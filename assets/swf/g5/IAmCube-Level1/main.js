///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 5, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var tween
var chpos = 0;
var question1, question2;
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var qno = [];
var choiceArr = [];
var qtype = [1, 2, 1, 2, 2, 1, 2, 1, 1, 2]
var ansArr = []
var choicePos = [1, 2, 1, 2, 1, 2, 1, 2, 1, 2]

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////


function init() {
    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "IAmCube-Level1/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(
            { id: "choice1", src: gameAssetsPath + "ChoiceImages1.png" },
            { id: "choice2", src: gameAssetsPath + "ChoiceImages2.png" },
            { id: "choice3", src: gameAssetsPath + "ChoiceImages3.png" },
            { id: "choice4", src: gameAssetsPath + "ChoiceImages4.png" },
            { id: "choice5", src: gameAssetsPath + "ChoiceImages5.png" },

            { id: "question", src: gameAssetsPath + "question.png" },
            { id: "question1", src: gameAssetsPath + "question1.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}
//=====================================================================//
function doneLoading1(event) {
    var event = assets[i];
    var id = event.item.id;
    console.log("assets ID= " + id)
    if (id == "questionText") {
        questionText = new createjs.Bitmap(preload.getResult('questionText'));
        container.parent.addChild(questionText);
        questionText.visible = false;
    }
    // Choice 1
    if (id == "choice1") {
        var spriteSheet3 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 172, "count": 0, "regY": 50, "width": 172 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        choice1 = new createjs.Sprite(spriteSheet3);
        container.parent.addChild(choice1);
        choice1.visible = false;
        // Choice 2
        var spriteSheet4 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice2")],
            "frames": { "regX": 50, "height": 172, "count": 0, "regY": 50, "width": 172 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });

        choice2 = new createjs.Sprite(spriteSheet4);
        container.parent.addChild(choice2);
        choice2.visible = false;
        // Choice 3
        var spriteSheet5 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice3")],
            "frames": { "regX": 50, "height": 172, "count": 0, "regY": 50, "width": 172 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });

        choice3 = new createjs.Sprite(spriteSheet5);
        container.parent.addChild(choice3);
        choice3.visible = false;
        // Choice 4
        var spriteSheet6 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice4")],
            "frames": { "regX": 50, "height": 172, "count": 0, "regY": 50, "width": 172 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });

        choice4 = new createjs.Sprite(spriteSheet6);
        container.parent.addChild(choice4);
        choice4.visible = false;
        // Choice 5
        var spriteSheet7 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice5")],
            "frames": { "regX": 50, "height": 172, "count": 0, "regY": 50, "width": 172 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        choice5 = new createjs.Sprite(spriteSheet7);
        container.parent.addChild(choice5);
        choice5.visible = false;

    }

    if (id == "question") {
        var spriteSheet3 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question")],
            "frames": { "regX": 50, "height": 357, "count": 0, "regY": 50, "width": 587 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        question = new createjs.Sprite(spriteSheet3);
        container.parent.addChild(question);
        question.visible = false;
    }

    if (id == "question1") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question1")],
            "frames": { "regX": 50, "height": 161, "count": 0, "regY": 50, "width": 160 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        question1 = new createjs.Sprite(spriteSheet1);
        question1.visible = false;
        container.parent.addChild(question1);

    };

    //=============================================================================
    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("9B187A3141F2F044B0356886562D636B");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }
}

function tick(e) {

    stage.update();
}

/////////////////////////////////////////////////////////////////=======GAME START========///////////////////////////////////////////////////////////////////
function handleClick(e) {
    qno = between(0, 49)
    qtype.sort(randomSort)
    toggleFullScreen()
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 65;

    bg.visible = true;

    container.parent.addChild(questionText);
    questionText.visible = true;

    container.parent.addChild(question)
    question.x = 400; question.y = 235;
    question.visible = true

    question1.visible = true;
    container.parent.addChild(question1)
    question1.scaleX = question1.scaleY = .7
    question1.x = 615;
    question1.y = 115;

    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i] = this["choice" + (i + 1)]
        choiceArr[i].visible = false
        container.parent.addChild(choiceArr[i])
        console.log("choice= " + choiceArr[i])
        choiceArr[i].x = 145 + (i * 230);
        choiceArr[i].y = 590;
    }

    pickques();
}

function helpDisable() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].mouseEnabled = true;
    }
}
//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    chpos = [];

    ansArr = []
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    boardMc.boardMc.openMc.mouseEnabled = true;
    //=================================================================================================================================//
    question.gotoAndStop(qno[cnt]);
    question1.gotoAndStop(qno[cnt]);
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].visible = true;
        choiceArr[i].gotoAndStop(qno[cnt]);
        choiceArr[i].name = "ch" + (i+1);
        chpos.push({ posx: choiceArr[i].x, posy: choiceArr[i].y })
    }

    ans = "ch1";
    chpos.sort(randomSort)
    console.log(ans)
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].x = chpos[i].posx
        choiceArr[i].y = chpos[i].posy
    }
    //======================================================================================================================================//
    enablechoices();
    rst = 0;
    gameResponseTimerStart();
    createjs.Ticker.addEventListener("tick", tick);
    stage.update();

}

function enablechoices() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].addEventListener("click", answerSelected);
        choiceArr[i].cursor = "pointer";
        choiceArr[i].visible = true;
    }
    question.visible = true
}

function disablechoices() {
    for (i = 0; i < choiceCnt; i++) {
        choiceArr[i].removeEventListener("click", answerSelected);
        choiceArr[i].cursor = "default";

        choiceArr[i].visible = false;

    }

    question.visible = false;
    boardMc.boardMc.openMc.mouseEnabled = false;
}


function answerSelected(e) {
    e.preventDefault();
    uans = e.currentTarget.name;

    gameResponseTimerStop();
    // pauseTimer();
    console.log(ans + " =correct= " + uans)
    disablechoices();
    if (uans == ans) {
        getValidation("correct");

    } else {
        getValidation("wrong");

    }

}
//===========================================================================================//
