 <header class="head">
                               
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Skill Score Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
			
 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Skill Score Report</h5>
            </header>
			<form id="srchreport" method="POST">
			<div class="row">
			<div class="col-sm-3">
			<label>School</label>
			<select  class="form-control input-sm" name="schoolid" id="schoolid">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			<div class="col-sm-3" style="display:none";>
			<label>Schools</label>
			<select  class="form-control input-sm" name="sklid" id="sklid">
			<option value="">Select</option>
			<?php foreach($skls as $skl) { ?>
			<option value="<?php echo $skl['sklname']; ?>"><?php echo $skl['sklname']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			<div class="col-sm-3">
			<label>Grade</label>
			<select  class="form-control input-sm" name="gradeid" id="gradeid" >
			<option value="">Select</option>
			<?php foreach($grades as $gd) { ?>
			<option value="<?php echo $gd['id']; ?>"><?php echo $gd['classname']; ?></option>
			<?php } ?>
			</select>
			<!--<input type="button" name="sbmtbtn" id="sbmtbtn" Value="Get result">-->
			</div>
			
			<div class="col-sm-2">
			<label>Section</label>
			<select  class="form-control input-sm sectionid" name="sectionid"  id="sectionid" >
			<option value="">Select</option>
			<?php if(isset($_POST['gradeid'])){   	 ?>
			<option value="<?php echo $data; ?>" selected='selected'><?php echo $data; ?></option>
			 <?php } ?>
			</select>
			</div>
			
			
			</div>
			</form>
			 <div id = "tabs-1">
         <ul>
            <li><a href = "javascript:;" onclick="report2('schoolid','gradeid',59);">Memory</a></li>
            <li><a href = "javascript:;" onclick="report2('schoolid','gradeid',60);">Visual Processing</a></li>
            <li><a href = "javascript:;" onclick="report2('schoolid','gradeid',61);">Focus And Attention</a></li>
			<li><a href = "javascript:;" onclick="report2('schoolid','gradeid',62);">Problem Solving</a></li>
			<li><a href = "javascript:;" onclick="report2('schoolid','gradeid',63);">Linguistics</a></li>
         </ul>
		 
      </div>
            <div id="collapse4" class="body">
                <div id="report2"></div>
				<div class="row" id='exportbtn' style='display:none;'>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<link href = "<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" rel = "stylesheet">
<script src = "//code.jquery.com/jquery-1.10.2.js"></script>
<script src = "//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(function() {
            $( "#tabs-1" ).tabs();
         });
	
var skillid_org = 59;	
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();	

$(document).ready(function(){
	
	
	$("#srchreport").validate({
		rules: {
			schoolid: {required: true},
            gradeid: {required: true}
 
		   },
		   messages: {
			schoolid: {required: "Please choose school"},
			gradeid: {required: "Please choose gradeid"}
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	
	$('#btnExcelExport').click(function(){
		
	excel_report2(schoolid,gradeid,skillid_org);
});
	
	
});

$('#gradeid').change(function(){
	
	ajaxsectionload(schoolid,gradeid);
	
	if($("#srchreport").valid()==true)
	{
report2(schoolid,gradeid,skillid_org);
	}
});


$('#sectionid').change(function(){
	if($("#srchreport").valid()==true)
	{
report2(schoolid,gradeid,skillid_org);
	}
});


$('#schoolid').change(function(){
	if($("#srchreport").valid()==true)
	{
report2(schoolid,gradeid,skillid_org);
	}
});

$('#sklid').change(function(){
	if($("#srchreport").valid()==true)
	{
report2(schoolid,gradeid,skillid_org);
	}
});

function excel_report2(schoolid,gradeid,skillid)
{
	var schoolid = 	$('#schoolid').val();
	var gradeid = $('#gradeid').val();
	var sklid = $('#sklid').val();
	var sectionid = $('#sectionid').val();
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/report2_excel",
    data: {schoolid:schoolid,gradeid:gradeid,skillid:skillid,sklid:sklid,sectionid:sectionid},
    
    success: function(result){
		//alert(result);
		 //console.log(result);
		window.location.href= "<?php echo base_url();?>"+result;
    }
});
}


function ajaxsectionload(schoolid,gradeid)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();

$.ajax({
		 url: "<?php echo base_url(); ?>index.php/admin/getsection",
		data:{schoolid:schoolid,gradeid:gradeid},
		success: function(result)
		{
		//alert(result);
		$('#sectionid').html(result);
		}
	});
	}




function report2(schoolid,gradeid,skillid)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();
var sklid = $('#sklid').val();
var sectionid = $('#sectionid').val();
skillid_org=skillid;
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/getreport2",
    data: {schoolid:schoolid,gradeid:gradeid,skillid:skillid,sklid:sklid,sectionid:sectionid},
    
    success: function(result){
		// alert(result);	 
		
		  if(result!='')
		 {
			 $('#exportbtn').show();
		 }
		 $('#report2').html(result);
		
    }
});
}	
		 
</script>
 