<?php //echo '<pre>'; print_r($report2); ?>

<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
					<th>s.no</th>
                        <th>Username</th>
                        <th>Grade</th>
						
						<?php if($gradesetid[0]['gradeid_set2']!='' && $gradesetid[0]['gradeid_set2']!='0') { ?>
                        <th>Skill score set1</th>
						 <th>AVG.Response time</th><?php } ?>
						 
                        <th>Skill score</th>
						 <th>AVG.Response time</th>
						 
						 <?php if($gradesetid[0]['gradeid_set3']!='' && $gradesetid[0]['gradeid_set3']!='0') { ?>
						<th>Skill score set3</th>
						<th>AVG.Response time</th><?php } ?>
                    </tr>
                    </thead>
                    <tbody>
					<?php $i=1; foreach($report2 as $r2) { ?>
                            <tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $r2['fname']; ?></td>
							 <td><?php echo $r2['gradename']; ?></td>
							  <?php if($gradesetid[0]['gradeid_set2']!='' && $gradesetid[0]['gradeid_set2']!='0') { ?>
							    <td><?php echo round($r2['score2'], 2); ?></td>
								<td><?php echo round($r2['rstime2'],2); ?></td><?php } ?>
							 
							  <td><?php echo round($r2['score'], 2); ?></td>
							   <td><?php echo round($r2['rstime'],2); ?></td>
							   
								<?php if($gradesetid[0]['gradeid_set3']!='' && $gradesetid[0]['gradeid_set3']!='0') { ?>
								  <td><?php echo round($r2['score3'], 2); ?></td>
								  <td><?php echo round($r2['rstime3'],2); ?></td><?php } ?></tr>
					<?php $i++; }  ?>
								 
                    </tbody>                
					</table>
<link href="<?php echo base_url(); ?>assets/admin/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/admin/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					$('#dataTable').DataTable( );
					</script>