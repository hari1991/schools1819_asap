<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arapi_model extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
				 $this->load->library('Multipledb');
        }
		public function typeofuser($username,$password)
        {
 			$query = $this->multipledb->db->query('select count(id) as type FROM school_parent_master WHERE username="'.$username.'" AND password=SHA1(CONCAT(salt1,"'.$password.'",salt2)) and status=1');
  			return $query->result();
        }
		public function checkParentLogin($username,$password,$deviceid)
        {
 			$query = $this->multipledb->db->query('select * FROM school_parent_master spm WHERE username="'.$username.'" AND password=SHA1(CONCAT(salt1,"'.$password.'",salt2)) and status=1');
			//echo $this->multipledb->db->last_query(); exit;
  			return $query->result();
        }
		public function checkChildLogin($username,$password,$deviceid)
        {
 			$query = $this->multipledb->db->query('select * FROM users a WHERE username="'.$username.'" AND password=SHA1(CONCAT(salt1,"'.$password.'",salt2))');
			//echo $this->multipledb->db->last_query(); exit;
  			return $query->result();
        }
		public function checkParentExist($username)
        {
 			$query = $this->multipledb->db->query('select * FROM school_parent_master a WHERE a.username="'.$username.'"  AND a.status=1');
  			return $query->result();
        }
		public function getInActiveParentDetails($ParentID)
        {
 			$query = $this->multipledb->db->query('select * FROM school_parent_master a WHERE a.id="'.$ParentID.'"  AND a.status=0');
  			return $query->result();
        }
		public function getChildList($ParentID)
        {
 			$query = $this->multipledb->db->query('select fname,gender,gp_id,grade_id,username,portal_type,startdate,enddate,org_id,parent_id,time_limit FROM users WHERE parent_id="'.$ParentID.'"  AND status=1 and visible=1');
  			return $query->result();
        }
		public function checkuserexist($username)
        {
 			$query = $this->multipledb->db->query('select a.*,(select replace(classname,"Grade","") from class where id=grade_id) as gradename FROM users a  WHERE a.username="'.$username.'"  AND a.status=1 and visible=1');
  			return $query->result();
        }
		public function updateDeviceID($username,$deviceid,$mtoken)
        {	
 			$query = $this->multipledb->db->query('UPDATE users SET deviceid="'.$deviceid.'",mtoken="'.$mtoken.'" where  username="'.$username.'"'); 
        }
		public function updateParentDeviceID($username,$deviceid,$mtoken)
        {	
 			$query = $this->multipledb->db->query('UPDATE school_parent_master SET deviceid="'.$deviceid.'",mtoken="'.$mtoken.'" where  username="'.$username.'"'); 
        }
 		public function updatePassword($userid,$username,$confirmpass,$shpassword,$salt1,$salt2)
        {			
			$qry=$this->multipledb->db->query("update change_password_history set updatedate=now(),status=1,newpwd='".$confirmpass."',device='APP' where userid='".$userid."'");
			
			$query = $this->multipledb->db->query("update users set password='".$shpassword."',salt1='".$salt1."',salt2='".$salt2."',otp='' where id = '".$userid."'");
        }
		public function update_Parent_Password($userid,$username,$confirmpass,$shpassword,$salt1,$salt2)
        { 
			
			$qry=$this->multipledb->db->query("update parent_change_password_history set updatedate=now(),status=1,newpwd='".$confirmpass."',device='APP' where userid='".$userid."'");
			
			$query = $this->multipledb->db->query("update school_parent_master set password='".$shpassword."',salt1='".$salt1."',salt2='".$salt2."',otp='' where id = '".$userid."'");
        }
 		 
		 public function getRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id,$userid)
		 {
			 
			$query = $this->multipledb->db->query("SELECT gid FROM rand_selection WHERE DATE(created_date) = '".$check_date_time."' AND gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and user_id='".$userid."' GROUP BY school_id, gs_id ORDER BY gs_id ASC");
			//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getAssignGames($game_plan_id,$game_grade,$uid,$catid)
		 {
			 
		$query = $this->multipledb->db->query("SELECT a.id, a.grade_id, d.skill_id FROM users AS a JOIN g_plans AS b ON a.gp_id = b.id AND b.id = '".$game_plan_id."' JOIN class_plan_game AS c ON b.id = c.plan_id AND b.grade_id = c.class_id JOIN class_skill_game AS d ON c.class_id = d.class_id AND c.game_id = d.game_id AND d.class_id = '".$game_grade."' JOIN category_skills AS e ON e.id = d.skill_id WHERE a.id = '".$uid."' AND e.category_id = '".$catid."' GROUP BY d.skill_id limit 1");
		//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getActualGames($game_plan_id,$game_grade,$uid,$catid,$where,$schoolid)
		 {
		$query = $this->multipledb->db->query("SELECT j.id, (select count(*) as tot_game_played from game_reports where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id AND lastupdate = '".date('Y-m-d')."') as tot_game_played ,(select MAX(game_score)  from game_reports where gu_id =  '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id AND lastupdate = '".date('Y-m-d')."') as tot_game_score , e.skill_id, j.name AS skill_name, g.gid, g.gname, g.appimg_path as img_path,g.game_html, j.icon 
		FROM users AS a
		JOIN g_plans AS b ON a.gp_id = b.id AND b.id = '".$game_plan_id."'
		JOIN class AS c ON a.grade_id = c.id AND c.id = '".$game_grade."'
		JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id
		JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id
		JOIN category_skills AS j ON e.skill_id = j.id 
		JOIN skl_class_plan AS f ON a.sid = f.school_id
		JOIN games AS g ON d.game_id = g.gid
		JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$schoolid."'
		WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' $where
		GROUP BY skill_id,g.gid");
	//	echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		 }	 
		 
		 public function getCurrentDaySkill($userid)
	{
		$query = $this->multipledb->db->query("select CASE 
WHEN playedcount%5=0 THEN 59
WHEN playedcount%5=1 THEN 60
WHEN playedcount%5=2 THEN 61
WHEN playedcount%5=3 THEN 62
WHEN playedcount%5=4 THEN 63 
END as currentdayskillid from(select count(DISTINCT lastupdate) as playedcount from game_reports where gu_id=".$userid.")a2");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	 public function getSkillsRandom($catid,$curdayskillid)
		 {
		$query = $this->multipledb->db->query("SELECT a.id AS category_id, b.id AS skill_id FROM g_category AS a JOIN category_skills AS b ON a.id = b.category_id WHERE a.id = '".$catid."' ORDER BY rand() limit 3 ");
		//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		 }
		
		public function assignRandomGame($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id)
		 {
			 
		$query = $this->multipledb->db->query("SELECT j.id, e.skill_id, j.name AS skill_name, g.gid, g.gname FROM users AS a
				JOIN g_plans AS b ON a.gp_id = b.id AND b.id = '".$game_plan_id."' JOIN class AS c ON a.grade_id = c.id AND c.id = '".$game_grade."' JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id JOIN category_skills AS j ON e.skill_id = j.id JOIN skl_class_plan AS f ON a.sid = f.school_id JOIN games AS g ON d.game_id = g.gid JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$school_id."' WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' and j.id = '".$skill_id."' and g.gid not in (SELECT gid FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and user_id='".$uid."' and gs_id = '".$skill_id."') and g.gid not in(243,283,23,65,100,146,140,179,186,226,266,307,233) GROUP BY g.gid ORDER BY RAND() LIMIT 1");
		//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		 }
		 
		public function deleteRandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$del_where)
		 {
			 
		$query = $this->multipledb->db->query("delete from rand_selection where gc_id = '".$catid."' and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' and user_id='".$uid."' ".$del_where);
		//echo $this->multipledb->db->last_query(); exit;
			 
		 }
		 
		 public function insertRandomGames($catid,$game_plan_id,$game_grade,$skill_id,$school_id,$section,$gameid,$check_date_time,$uid)
		 { 
			$query = $this->multipledb->db->query("INSERT INTO rand_selection SET gc_id = '".$catid."', gs_id = '".$skill_id."', gid = '".$gameid."', gp_id = '".$game_plan_id."', grade_id = '".$game_grade."', section = '".$section."', school_id = '".$school_id."',user_id='".$uid."',created_date = '".$check_date_time."'"); 
		 } 
		 
		 
		 public function deleteSPLRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id)
		 {
			$query = $this->multipledb->db->query("delete FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."'");
		 }
	
		 public function updateAppKey($username,$appkey)
		 {
 			$query = $this->multipledb->db->query('UPDATE users SET appkey="'.$appkey.'",appkey_set_datetime=NOW() where  username="'.$username.'"'); 
		 }
		 
		public function getgameid($gamename)
		{
			$query = $this->multipledb->db->query("select gid as gid from games where game_html='".$gamename."' limit 1 ");
			//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		}
	public function checkgame($gameid,$schoolid,$gradeid,$section,$userid)
	{
		$curdate=date('Y-m-d');
		$query = $this->multipledb->db->query("select count(distinct gid) as gameexist,(select game_html from games where gid='".$gameid."' limit 1) as gpath,(select ntimes from game_limit where g_id='".$gameid."' limit 1) as game_limit,(select count(id) from game_reports where g_id='".$gameid."' and gu_id='".$userid."' and lastupdate='".$curdate."') as played_time from rand_selection where gid = '".$gameid."' and created_date='".$curdate."' and grade_id='".$gradeid."' and user_id='".$userid."'  and school_id='".$schoolid."'  ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	 public function getresultGameDetails($userid,$gameid)
	{
		 //echo 'hello'; exit;
	$query = $this->multipledb->db->query("select (select gs_id from games where gid='".$gameid."') as gameskillid,(select count(gu_id) from game_reports where gu_id=".$userid." and lastupdate=CURDATE() and gs_id IN(59,60,61,62,63)) as playedgamescount");
	 
		return $query->result_array();
	}
		
	 public function insertone($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val)
		 {
			 //echo 'hello'; exit;
		$query = $this->multipledb->db->query("insert into gamedata (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate,Is_schedule) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."','".$schedule_val."')");
		//echo $this->multipledb->db->last_query(); exit;
			 
		 }	
		 
		  public function insertlang($gameid,$userid,$userlang,$skillkit)
		 {
			 //echo 'hello'; exit;
		$query = $this->multipledb->db->query("INSERT INTO game_language_track( gameID, userID, languageID, skillkit, createddatetime) VALUES ('".$gameid."','".$userid."','".$userlang."','".$skillkit."',NOW())");
		//echo $this->multipledb->db->last_query(); exit;
			 
		 }	
		 
		  public function insertthree($userid,$gameid,$acid,$lastup_date,$st)
		 {
			 //echo 'hello'; exit;
		$query = $this->multipledb->db->query("insert into user_games (gu_id,played_game,last_update,date,status,academicyear) values('".$userid."','".$gameid."','".$lastup_date."','".$lastup_date."','".$st."','".$acid."')");
		 
		 }	
		 
		 public function insertsparkies($arrofinput)	 
	{  //echo "<pre>";print_r($arrofinput);exit;
		/* $stored_procedure = "CALL insertsparkies(?,?,?,?,?,?,?,?,?) ";
		$result = $this->multipledb->db->query($sp,$arrofinput); 	 */
		$inDatetime=date("Y-m-d H:i:s");
	
		$query = $this->multipledb->db->query("CALL insertsparkiesnew(".$arrofinput['inSID'].",".$arrofinput['inGID'].",".$arrofinput['inUID'].",'".$arrofinput['inScenarioCode']."','".$arrofinput['inTotal_Ques']."','".$arrofinput['inAttempt_Ques']."','".$arrofinput['inAnswer']."','".$arrofinput['inGame_Score']."','".$arrofinput['inPlanid']."','".$arrofinput['inGameid']."','".$inDatetime."')");
		mysqli_next_result($this->multipledb->db->conn_id);
		return $query->result_array();
	}
	public function insertnewsfeeddata($arrofinput)	 
	{
		$inDatetime=date("Y-m-d H:i:s");
		
		$query = $this->multipledb->db->query("CALL insertnewsfeeddata('".$arrofinput['inSID']."','".$arrofinput['inGID']."','".$arrofinput['inUID']."','".$arrofinput['inScenarioCode']."','".$arrofinput['inTotal_Ques']."','".$arrofinput['inAttempt_Ques']."','".$arrofinput['inAnswer']."','".$arrofinput['inGame_Score']."','".$arrofinput['inPlanid']."','".$arrofinput['inGameid']."','".$inDatetime."')");
		
		//return $query->result_array();
	}
	/* Sundar */
	public function updatetempotp($otp,$userid)
	{
		$query = $this->multipledb->db->query('UPDATE users SET otp="'.$otp.'",otp_datetime=NOW() where id="'.$userid.'"');
	}
	public function clearTempotp($userid)
	{
		$query = $this->multipledb->db->query('UPDATE users SET otp="" where  id="'.$userid.'" '); 
	}
	public function getUserPuzzlesDetails($userid)
	{
		$query = $this->multipledb->db->query("SELECT COALESCE(ROUND((SUM(gtime)/60),0),0) as MinutesTrained  , 
		COALESCE(SUM(answer),0) as PuzzlesSolved, COALESCE(SUM(attempt_question),0) as PuzzlesAttempted,
		DATEDIFF(u.enddate,u.startdate) as TotalSession,COALESCE(count(distinct lastupdate),0) as AttendedSession,(select count(gu_id) from gamedata_cq where gu_id='".$userid."') as cqplayedcount	FROM game_reports gr join users u on gr.gu_id=u.id	WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate");
		return $query->result_array();
	}
	public function getUserPlayedDates($userid)
	{
		$query = $this->multipledb->db->query("Select GROUP_CONCAT(DISTINCT lastupdate) as playeddate from gamedata where gu_id='".$userid."' ");
		return $query->result_array();
	}
	public function getUserPlayedDetails($userid,$curdate)
	{
		$query = $this->multipledb->db->query("SELECT COALESCE(ROUND((SUM(gtime)/60),0),0) as MinutesTrained  , 
		COALESCE(SUM(answer),0) as PuzzlesSolved, COALESCE(SUM(attempt_question),0) as PuzzlesAttempted,(select gname from games where gid=gr.g_id) as Gname,COALESCE(SUM(game_score),0) as TotalScore,(select COALESCE(SUM(Points),0) from user_sparkies_history where U_ID='".$userid."' and date(Datetime)='".$curdate."') as Crownies FROM game_reports gr join users u on gr.gu_id=u.id	WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate and lastupdate='".$curdate."' ");
		return $query->result_array();
	}
	
	public function update_loginDetails($userid,$session_id)
	{ 
		$query = $this->multipledb->db->query("update users set pre_logindate = login_date,login_date = CURDATE(),login_count=login_count+1,session_id=".$session_id.",islogin=1,last_active_datetime=NOW() WHERE id =".$userid);
	}
	public function update_parent_loginDetails($userid,$session_id)
	{ 
		$query = $this->multipledb->db->query("update school_parent_master set pre_logindate = login_date,login_date = CURDATE(),login_count=login_count+1,session_id=".$session_id.",islogin=1,last_active_datetime=NOW() WHERE id =".$userid);
	}
	public function insert_login_log($userid,$sessionid,$ip,$country,$region,$city,$isp,$browser,$status)
	{
		$query = $this->multipledb->db->query('INSERT INTO user_login_log(userid,sessionid,created_date,lastupdate,logout_date,ip,country,region,city,browser,isp,status)VALUES("'.$userid.'","'.$sessionid.'",now(),now(),now(), "'.$ip.'","'.$country.'","'.$region.'","'.$city.'","'.$browser.'","'.$isp.'","'.$status.'")');
		return $query;
	}
	public function insert_parent_login_log($userid,$sessionid,$ip,$country,$region,$city,$isp,$browser,$status)
	{
		$query = $this->multipledb->db->query('INSERT INTO user_parent_login_log(userid,sessionid,created_date,lastupdate,logout_date,ip,country,region,city,browser,isp,status)VALUES("'.$userid.'","'.$sessionid.'",now(),now(),now(), "'.$ip.'","'.$country.'","'.$region.'","'.$city.'","'.$browser.'","'.$isp.'","'.$status.'")');
		return $query;
	}
	/*... User Total Score ...*/
	public function getUserCurrentBspi($userid)
	{
		$query = $this->multipledb->db->query("Select sum(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=".$userid.") as bspi from game_reports where gu_id=".$userid." and lastupdate between (select startdate from users where id=".$userid.") and  (select enddate from users where id=".$userid.")");
		return $query->result_array();
	}
	/*... User PercentileRank ...*/
	public function MyPercentileRank($userid,$gradeid,$org)
	{
		if($org!=''){$gwhere="org_id=".$org;}else{$gwhere="1=1";}
		
		$query = $this->multipledb->db->query("select 
(select count(gu_id) from  sc_usertotgamescore where grade_id=".$gradeid." and ".$gwhere.") as totusercount,
(select score from  sc_usertotgamescore where gu_id=".$userid." and grade_id=".$gradeid." and ".$gwhere." ) as Mytotscore,
(select count(gu_id) from  sc_usertotgamescore where score < Mytotscore and gu_id!=".$userid." and grade_id=".$gradeid." and ".$gwhere.") as belowusercount,
(select count(gu_id) from  sc_usertotgamescore where score=Mytotscore and grade_id=".$gradeid." and ".$gwhere.") as samescoreusercount");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	/*... User MyCurrentSparkie ...*/
	public function getMyCurrentSparkies($school_id,$grade_id,$userid,$startdate,$enddate)
    {
		$query = $this->multipledb->db->query("CALL getMyCurrentSparkies(".$school_id.",".$grade_id.",".$userid.",'".$startdate."','".$enddate."')");
		mysqli_next_result($this->multipledb->db->conn_id);
		return $query->result_array();

	}
	/*...... Line Chart ........*/
	public function datewisescore($userid)
	{
		$query = $this->multipledb->db->query("Select CASE WHEN group_concat(TotScore) IS NULL THEN 0 ELSE group_concat(TotScore) END as TotScore,CASE WHEN group_concat(Playeddate) IS NULL THEN 0 ELSE group_concat(Playeddate) END as Playeddate from (SELECT sum(gr.game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=".$userid.") as TotScore,DATE_FORMAT(gr.lastupdate, '%d-%m-%Y')  as Playeddate FROM game_reports gr join users u on gr.gu_id=u.id WHERE u.id=".$userid." and gr.lastupdate between u.startdate and u.enddate group by lastupdate) as a1 ORDER BY Playeddate ASC");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	/*..... Logout Log & Status Update ......*/
	public function update_logout_log($userid,$sessionid)
	{
		$query = $this->multipledb->db->query('update user_login_log set lastupdate=now(),logout_date=now() where userid="'.$userid.'" and sessionid="'.$sessionid.'"');
		return $query;
	}
	public function update_parent_logout_log($userid,$sessionid)
	{
		$query = $this->multipledb->db->query('update user_parent_login_log set lastupdate=now(),logout_date=now() where userid="'.$userid.'" and sessionid="'.$sessionid.'"');
		return $query;
	}
	public function updateuserloginstatus($userid,$login_session_id)
	{
		$query = $this->multipledb->db->query('Update users set islogin=0 WHERE id="'.$userid.'" AND status=1;');
	}
	public function update_parent_loginstatus($userid,$login_session_id)
	{
		$query = $this->multipledb->db->query('Update school_parent_master set islogin=0 WHERE id="'.$userid.'" AND status=1;');
	}
	/*....... Forget Password Log .........*/
	public function forgetpwdlog($userid,$otp)
	{
		$qry=$this->multipledb->db->query("insert into change_password_history(userid,randid,requestdate,status,device)values('".$userid."','".$otp."',NOW(),0,'APP')");
	}
	public function parent_forgetpwdlog($userid,$otp)
	{
		$qry=$this->multipledb->db->query("insert into parent_change_password_history(userid,randid,requestdate,status,device)values('".$userid."','".$otp."',NOW(),0,'APP')");
	}
	/*..... Rand Selection tbl....*/
	public function deleteMisMatchRandomGames($catid,$game_plan_id,$game_grade,$uid,$school_id,$del_where)
	{ 
		$query = $this->multipledb->db->query("delete from rand_selection where gc_id = '".$catid."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' and user_id='".$uid."' ".$del_where);
	}
	/*....... CQ Visibility .......*/
	public function getChallengeQuestionVisible($userid)
	{
		$query = $this->multipledb->db->query("Select (select max(cast(game_score as unsigned)) from gamedata where gu_id='".$userid."' and lastupdate=CURDATE() group by lastupdate) as topscore,(select count(id) from gamedata where gu_id='".$userid."' and lastupdate=CURDATE() group by lastupdate) as playedcount,(select count(id) as playedstatus from gamedata_cq where gu_id='".$userid."' and date(lastupdate)=CURDATE()) as playedstatus ");
		return $query->result_array();
	}
	public function getCrwoniesofGame($gameid,$userid)
	{
		$query = $this->multipledb->db->query("SELECT SUM(Points) as points FROM user_sparkies_history WHERE U_ID=".$userid." and Gameid=".$gameid." and ScenarioCode='GAME_END' and date(Datetime)=CURDATE() ");
		return $query->result_array();
	}
	/*......... CQ Insert ............*/
	public function insertone_CQ($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val)
	{
		$query = $this->multipledb->db->query("insert into gamedata_cq(gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."')");
	}
	public function InsertCQGames($userid,$gameid)
	{
		$query = $this->multipledb->db->query("Insert into user_challenge_table(userid,challengeid)values('".$userid."','".$gameid."') "); 
	}
	
	/*..... Badges Concept ........*/
	public function getTopScorePosition($userid,$org_id,$grade_id)
	{	$date=date('Y-m-d');
		
		$query = $this->multipledb->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,score,position from (select gu_id,grade_id,gp_id,sid,section,username,fname,score,lastupdate,FIND_IN_SET(score,(SELECT GROUP_CONCAT( score ORDER BY score DESC )FROM sc_topscorer where org_id='".$org_id."' and grade_id='".$grade_id."' )) AS position FROM sc_topscorer where org_id='".$org_id."' and grade_id='".$grade_id."') as a1 where gu_id='".$userid."' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getMaxPuzzlesAttempted($userid,$org_id,$grade_id)
	{	$date=date('Y-m-d');
		
		$query = $this->multipledb->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,PuzzlesAttempted,position from (select gu_id,grade_id,gp_id,sid,section,username,fname,lastupdate,PuzzlesAttempted,FIND_IN_SET(PuzzlesAttempted,(SELECT GROUP_CONCAT( PuzzlesAttempted ORDER BY PuzzlesAttempted DESC )FROM sc_maxpuzzlesattempted where org_id='".$org_id."' and grade_id='".$grade_id."')) AS position  FROM sc_maxpuzzlesattempted where org_id='".$org_id."' and grade_id='".$grade_id."')as a1 where gu_id='".$userid."' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getMaxPuzzlesSolved($userid,$org_id,$grade_id)
	{	$date=date('Y-m-d');
		$query = $this->multipledb->db->query("select gu_id,grade_id,gp_id,sid,section,username,fname,PuzzlesSolved,position from (select gu_id,grade_id,gp_id,sid,section,username,fname,lastupdate,PuzzlesSolved,FIND_IN_SET(PuzzlesSolved,(SELECT GROUP_CONCAT( PuzzlesSolved ORDER BY PuzzlesSolved DESC )FROM sc_maxpuzzlessolved where org_id='".$org_id."' and grade_id='".$grade_id."')) AS position FROM sc_maxpuzzlessolved where  org_id='".$org_id."' and grade_id='".$grade_id."') as a1 where gu_id='".$userid."' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	/*............Challenge Question Functions Start............*/
	
	public function getQuesList($userid)
	{
		$query = $this->multipledb->db->query("SELECT * FROM playedQues WHERE userid = $userid");
		return $query->result_array();
	}
	
	public function getQuesInfo($sno)
	{
		$query = $this->multipledb->db->query("SELECT * FROM challenge WHERE sno = $sno");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function gradewiseList($grade,$userid)
	{
		$query = $this->multipledb->db->query("SELECT group_concat(sno) as qnos FROM challenge  WHERE grade <= $grade and sno NOT IN(SELECT challengeid FROM user_challenge_table WHERE userid = $userid)");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	/*............Challenge Question Functions End............*/
	
	/*............Leader Board............*/
	public function maxbspiscore($org_id)
	{
		$query = $this->multipledb->db->query("select min(DISTINCT(bspi)) as val from (Select u.id, sum(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=u.id) as bspi, CONCAT(fname,'',lname) as name, (select classname from class where id=u.grade_id) as grade from game_reports gr 
		JOIN users u ON gr.gu_id=u.id where status=1 and visible=1 and isdemo=0 and u.org_id='".$org_id."' group by u.id order by bspi DESC) z1");
		//echo $this->multipledb->db->last_query();  exit;
		return $query->result_array();
	}
	
	public function homepagetopperslist($minscore,$org_id)
	{
		$query = $this->multipledb->db->query("select * from (Select sum(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id=u.id) as bspi,CONCAT(fname,' ',lname) as name,avatarimage,(select classname from class where id=u.grade_id) as grade from game_reports gr 
		JOIN users u ON gr.gu_id=u.id where status=1 and visible=1 and isdemo=0 and u.org_id='".$org_id."' group by u.id order by bspi DESC limit 50)z1 where bspi>='".$minscore."'");
		//echo $this->multipledb->db->last_query();  exit;
		return $query->result_array();
	}
	/*AVATAR IMAGE UPDATE*/
	public function avatarupdate($userid,$avatarimg)
	{
		$query = $this->multipledb->db->query("update users set avatarimage='assets/images/avatarimages/avatar/".$avatarimg."'  WHERE id ='".$userid."'");
		//echo $this->multipledb->db->last_query(); exit;
	}
	
	/*........... FB Sharing .........*/
	public function getTrainingCalendarData($userid,$curdate)
	{
		$query = $this->multipledb->db->query("SELECT ROUND((SUM(gtime)/60),0) as MinutesTrained  , SUM(answer) as PuzzlesSolved, SUM(attempt_question) as PuzzlesAttempted,(select gname from games where gid=gr.g_id) as Gname,count(gu_id) as playedcount, SUM(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id='".$userid."' and lastupdate='".$curdate."') as TotScore,(select count(gu_id) from gamedata_cq where gu_id='".$userid."' and lastupdate='".$curdate."') as Ischallengeplayed,(select SUM(Points) from user_sparkies_history where U_ID='".$userid."' and date(Datetime)='".$curdate."') as Crownies FROM game_reports gr join users u on gr.gu_id=u.id WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate and lastupdate='".$curdate."' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	/*.............. FB Sharing ..................*/
	public function getFBSharingData($userid,$curdate)
	{
		$query = $this->multipledb->db->query("SELECT SUM(answer) as PuzzlesSolved,SUM(game_score)+(select COALESCE(SUM(game_score),0) as score from gamedata_cq where gu_id='".$userid."' and lastupdate='".$curdate."') as TotScore,(select SUM(Points) from user_sparkies_history where U_ID='".$userid."' and date(Datetime)='".$curdate."') as Crownies FROM game_reports gr join users u on gr.gu_id=u.id WHERE gtime IS NOT NULL AND answer IS NOT NULL and u.id='".$userid."' and lastupdate between u.startdate and u.enddate and lastupdate='".$curdate."' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	/*.............. FB Sharing ..................*/
	public function getPrevUserPosition($userid)
	{
		$query = $this->multipledb->db->query("SELECT sbb_position,sgb_position,sab_position from users where id='".$userid."' and status=1 and visible=1 ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function InsertUserBadgeStatusLog($userid,$type,$score,$status,$badge_type,$position)
	{
		$query = $this->multipledb->db->query("Insert into user_badgestatus(userid,badge_type,score,status,dateontopper,created_on)
		values('".$userid."','".$type."','".$score."',".$status.",CURDATE(),NOW())");
		
		$this->multipledb->db->query("Update users SET ".$badge_type."='".$position."' Where id='".$userid."' ");
		
	}
	
	public function updateMtoken($username,$mtoken)
	{	
		$query = $this->multipledb->db->query('UPDATE users SET mtoken="'.$mtoken.'" where  username="'.$username.'"'); 
	}
	public function update_parent_Mtoken($username,$mtoken)
	{	
		$query = $this->multipledb->db->query('UPDATE school_parent_master SET mtoken="'.$mtoken.'" where  username="'.$username.'"'); 
	}
	
/* ....................... ASAP Concept code Begins ........................*/
	
	public function getMultiAssesGameList($grade_id,$org_id)
	{
		$query = $this->multipledb->db->query('SELECT  name, sid, grade_id, ME, VP, FA, PS, LI,mg1.gs_id as s1,mg2.gs_id  as s2,mg3.gs_id as s3,mg4.gs_id as s4,mg5.gs_id as s5,
mg1.gname as MEgname,mg1.path as MEpath,mg1.img_path as MEimgpath,mg1.game_html as MEgamehtml,
mg2.gname as VPgname,mg2.path as VPpath,mg2.img_path as VPimgpath,mg2.game_html as VPgamehtml,
mg3.gname as FAgname,mg3.path as FApath,mg3.img_path as FAimgpath,mg3.game_html as FAgamehtml,
mg4.gname as PSgname,mg4.path as PSpath,mg4.img_path as PSimgpath,mg4.game_html as PSgamehtml,
mg5.gname as LIgname,mg5.path as LIpath,mg5.img_path as LIimgpath,mg5.game_html as LIgamehtml
FROM asap_game_mapping as m
join games as mg1 on mg1.gid=m.ME
join games as mg2 on mg2.gid=m.VP
join games as mg3 on mg3.gid=m.FA
join games as mg4 on mg4.gid=m.PS
join games as mg5 on mg5.gid=m.LI
WHERE org_id='.$org_id.' and grade_id='.$grade_id.' and status=1 ');
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function getGameScore($userid,$org_id)
	{
		$query = $this->multipledb->db->query('select ME,VP,FA,PS,LI,ROUND(((ME+VP+FA+PS+LI)/5),2) as bspi from asap_gamescore where gu_id="'.$userid.'" and org_id="'.$org_id.'" ');
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function checkagame($gameid,$schoolid,$gradeid,$section,$userid,$org_id)
	{
		$curdate=date('Y-m-d');
		$query = $this->multipledb->db->query("SELECT count(*) as gameexist,(select game_html from games where gid='".$gameid."' limit 1) as gpath,1 as game_limit,(select count(id) from asap_gamedata where g_id='".$gameid."' and gu_id='".$userid."' and org_id=".$org_id.") as played_time FROM asap_game_mapping WHERE org_id=".$org_id." and ".$gameid." in (ME,VP,FA,PS,LI) and grade_id='".$gradeid."' ");
		
		return $query->result_array();
	}
	
/*....................... Registration .......................*/	
	public function getStateList()
	{
		$query = $this->multipledb->db->query('select id,state_name from  state where status="1" order by state_name asc');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getCityList($stateid)
	{
		$query = $this->multipledb->db->query('select id,city_name from city where stateid="'.$stateid.'" and status=1 order by city_name asc');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function getSchooLlist()
	{
		$query = $this->multipledb->db->query('select id,school_name,school_code from  school_master where status="1" order by school_name asc');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function gradelist()
	{
		$query = $this->multipledb->db->query('select group_concat(id) as grade_id,group_concat(classname) as grade_name from  class where id in(11,1,2) order by sortby ASC');		
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function IsMobilenounique($mobileno)
	{
		$query = $this->multipledb->db->query("SELECT count(id) as isexist from school_parent_master where mobileno='".$mobileno."' and status=1");
		return $query->result_array();
	}
	public function IsEmailunique($username)
	{
		$query = $this->multipledb->db->query("SELECT count(id) as isexist from school_parent_master where username='".$username."' and status=1");
		return $query->result_array();
	}
	public function getPlan($grade)
	{
		$query = $this->multipledb->db->query('select id from g_plans where grade_id="'.$grade.'" ');
		return $query->result_array();
	}
	public function addRegistration($firstname,$mobile,$email,$shpassword,$salt1,$salt2,$org_pwd,$created_on,$State,$City,$Pincode,$Address)
	{
		$query = $this->multipledb->db->query('INSERT INTO school_parent_master(name,username,salt1,password,salt2,mobileno,state,city,pincode,address,portal_type,orgpwd,status,createddate)VALUES("'.$firstname.'","'.$email.'","'.$salt1.'","'.$shpassword.'","'.$salt2.'","'.$mobile.'","'.$State.'","'.$City.'","'.$Pincode.'","'.$Address.'","B2C","'.$org_pwd.'","0",NOW())');
		//echo $this->multipledb->db->last_query(); exit;
		return $this->multipledb->db->insert_id();
	}
	public function updateParentConfirmUser($ParentID)
	{
		$query = $this->multipledb->db->query('UPDATE school_parent_master SET status="1" where id="'.$ParentID.'"');
	}
	public function updateParentRegOtp($otp,$regid)
	{
		$query = $this->multipledb->db->query('UPDATE school_parent_master SET otp="'.$otp.'",otp_datetime=NOW() where id="'.$regid.'"');
	}
	public function updateRegOtp($otp,$regid)
	{
		$query = $this->multipledb->db->query('UPDATE users SET otp="'.$otp.'",otp_datetime=NOW() where id="'.$regid.'"');
	}
	public function checkRegexist($otp,$regid)
	{
		$query = $this->multipledb->db->query('select *,(select replace(classname,"Grade","") from class where id=grade_id) as gradename FROM registration WHERE id="'.$regid.'" ');
		return $query->result();
	}
	public function getconfigdetails($couponcode)
	{
		$query = $this->multipledb->db->query('select id,validity,price,CURDATE() as startdate,DATE_ADD(CURDATE(), INTERVAL validity-1 DAY) as enddate,isasapenabled from organization_master where id IN(select organization_id from couponmaster where couponcode="'.$couponcode.'" and (CURDATE() between valid_from and valid_to) and coupon_valid_times>coupon_used_times and status=1)');
		//echo $this->multipledb->db->last_query(); 
		return $query->result_array();
	}
	public function getDefaultPlandetails()
	{
		$query = $this->multipledb->db->query('select id,validity,price,CURDATE() as startdate,DATE_ADD(CURDATE(), INTERVAL validity DAY) as enddate,isasapenabled from organization_master where id=2');
		//echo $this->multipledb->db->last_query(); 
		return $query->result_array();
	}
	public function GetCouponCodeDetails($couponcode)
	{
		$query = $this->multipledb->db->query("select count(c.id) as iscouponvalid,coupon_valid_times,discount_percentage,coupon_used_times,validity,price,org.id as orgid from couponmaster as c join organization_master as org on org.id=organization_id where couponcode='".$couponcode."' and c.status=1 and org.status=1 ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function applycoupon($couponcode)
	{
		$query = $this->multipledb->db->query("select count(id) as iscouponvalid from couponmaster where couponcode='".$couponcode."' and (CURDATE() between valid_from and valid_to) and coupon_valid_times>coupon_used_times and status=1");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}	
	public function updateCouponStatus($RegId,$couponcode,$planamount,$paidamount)
	{
		$query = $this->multipledb->db->query('UPDATE registration SET status="P", couponcode="'.$couponcode.'",originalamount="'.$planamount.'",paidamount="'.$paidamount.'", processdate="'.date("Y-m-d H:i:s").'" where id= "'.$RegId.'" ');
		//echo $this->multipledb->db->last_query(); exit;
		
	}
	public function getresponsedetails($RegId)
	{
		$query = $this->multipledb->db->query('select * from  registration r where id="'.$RegId.'" and id not in(select subscriberid from users) ');		
		return $query->result_array();
	}
	public function checkIsValidUSer($RegId)
	{
		$query = $this->multipledb->db->query('select count(id) as isuser from  registration r where id="'.$RegId.'" and status!="C" ');		
		return $query->result_array();
	}
	public function subscribeusers($subscriberid,$salt1,$salt2,$email,$password,$planid,$gradeid,$firstname,$gender,$mobile,$couponcode,$orgpwd,$startdate,$enddate,$orgid,$profileimage,$portal_type)
	{
		$query = $this->multipledb->db->query('INSERT INTO users(subscriberid,email, salt1, password, salt2, fname, gender, mobile,  status, visible,gp_id,grade_id,username,sid, section,academicyear,creation_date,org_pwd,couponcode,portal_type,startdate,enddate,org_id,avatarimage,agreetermsandservice)
		VALUES("'.$subscriberid.'","'.$email.'","'.$salt1.'","'.$password.'","'.$salt2.'","'.$firstname.'","'.$gender.'","'.$mobile.'",1,1,"'.$planid.'","'.$gradeid.'","'.$email.'",2,"A",20,"'.date("Y-m-d").'","'.$orgpwd.'","'.$couponcode.'","'.$portal_type.'","'.$startdate.'","'.$enddate.'","'.$orgid.'","'.$profileimage.'",1)');
		
		$clplastid=$this->multipledb->db->insert_id();
		
		$query = $this->multipledb->db->query("INSERT INTO user_academic_mapping(id, grade_id, gp_id, sid, section, academicid, status, visible)
		VALUES(".$clplastid.",'".$gradeid."','".$planid."',2,'A',20,1,1)");

		return $clplastid;
	}
	public function UpdateCouponCount($couponcode)
	{
		$query = $this->multipledb->db->query("UPDATE couponmaster SET coupon_used_times=coupon_used_times+1 WHERE couponcode='".$couponcode."' ");
	}
	public function completestatus($RegId)
	{
		$query = $this->multipledb->db->query('UPDATE registration SET status="C",paymentstatus="Y",completeddate="'.date("Y-m-d H:i:s").'" where id= "'.$RegId.'" ');		
		//echo $this->multipledb->db->last_query(); exit;
	}
	public function getOrganization_PortalType($org_id)
	{
		$query = $this->multipledb->db->query("select asaponly from organization_master where id='".$org_id."'");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	/* ...... Oranization based Leader Board ............. */
	public function maxscore_byorg($org_id,$grade_id)
	{
		$query = $this->multipledb->db->query('select min(DISTINCT(bspi)) as val from (select ROUND(((ME+VP+FA+PS+LI)/5),2) as bspi from asap_gamescore as ag JOIN users u ON ag.gu_id=u.id where status=1 and visible=1 and isdemo=0 and ag.org_id="'.$org_id.'" and grade_id="'.$grade_id.'" group by u.id order by bspi DESC) as z1');
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function Topperlist_byorg($minscore,$org_id,$grade_id)
	{
		$query = $this->multipledb->db->query('select * from (select ROUND(((ME+VP+FA+PS+LI)/5),2) as bspi,username,CONCAT(fname," ",lname) as name,avatarimage,(select classname from class where id=u.grade_id) as grade from asap_gamescore as ag JOIN users u ON ag.gu_id=u.id where status=1 and visible=1 and isdemo=0 and ag.org_id="'.$org_id.'" and grade_id="'.$grade_id.'" group by u.id order by bspi DESC limit 50) as z1 where bspi>="'.$minscore.'" ');
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	
	public function IscouponcodeValid($couponcode)
	{
		$query = $this->multipledb->db->query("select count(id) as iscouponvalid,discount_percentage,coupon_valid_times from couponmaster where couponcode='".$couponcode."' and (CURDATE() between valid_from and valid_to) and status=1");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function getGameNameOfGrade($gradeid,$planid)
	{
		$query = $this->multipledb->db->query("select group_concat(g.game_html) as gamename from class_plan_game AS d JOIN class_skill_game AS e ON e.class_id = d.class_id AND d.game_id = e.game_id JOIN games AS g ON d.game_id = g.gid where d.class_id='".$gradeid."' and d.plan_id='".$planid."' and skill_id IN(59,60,61,62,63)");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	

	public function ChildLimitExpired($parentid)
	{
		$query = $this->multipledb->db->query("select count(id) as noofchild from users where parent_id='".$parentid."' and status=1 and portal_type='B2C' ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	public function addChildReg($Name,$planid,$Grade,$Gender,$Address,$ParentID,$password,$salt1,$salt2,$created_on,$orgpwd,$State,$City,$Pincode,$Schoolname,$couponcode,$Bookneed,$validity,$orgid,$SpecialChild)
	{
		$query = $this->multipledb->db->query('INSERT INTO users(salt1,password,salt2,fname,gender,status,visible,gp_id,grade_id,sid, section,academicyear,creation_date,org_pwd,portal_type,org_id,avatarimage,agreetermsandservice,state,city,pincode,parent_id,time_limit,school_name,couponcode,isbookneed,startdate,enddate,isspecialchild)
		VALUES("'.$salt1.'","'.$password.'","'.$salt2.'","'.$Name.'","'.$Gender.'",0,0,"'.$planid.'","'.$Grade.'",2,"A",20,"'.date("Y-m-d").'","'.$orgpwd.'","B2C","'.$orgid.'","'.$profileimage.'",1,"'.$State.'","'.$City.'","'.$Pincode.'","'.$ParentID.'","40","'.$Schoolname.'","'.$couponcode.'","'.$Bookneed.'",CURDATE(),DATE_ADD(CURDATE(), INTERVAL '.$validity.'  DAY),"'.$SpecialChild.'" )');
		
		$clplastid=$this->multipledb->db->insert_id();
		
		$this->multipledb->db->query("Update users SET email='".$clplastid."',username='".$clplastid."' where id='".$clplastid."' ");
		
		return $clplastid;
		
	}
	public function getInActiveChildIDDetails($ChildID)
	{
		$query = $this->multipledb->db->query('select * FROM users a WHERE a.id="'.$ChildID.'"  AND a.status=0 and a.visible=0');
		return $query->result();
	}
	public function updateChildConfirmUser($childId,$paymentstatus,$PaymentId,$grade_id,$gp_id)
	{
		$query = $this->multipledb->db->query('UPDATE users SET status=1,visible=1,paymentstatus="P",payment_id="'.$PaymentId.'" where id="'.$childId.'"');
		
		$this->multipledb->db->query("INSERT INTO user_academic_mapping(id, grade_id, gp_id, sid, section, academicid, status, visible)VALUES(".$childId.",'".$grade_id."','".$gp_id."',2,'A',20,1,1)");
	}
	public function updateChildFailureUser($childId,$paymentstatus)
	{
		$query = $this->multipledb->db->query('UPDATE users SET paymentstatus="'.$paymentstatus.'" where id="'.$childId.'"');
	}
	
}