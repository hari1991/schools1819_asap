<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<title>Skillangels</title>
<!-- Bootstrap -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slick.css">
 
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stylenew.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/counter.css">
  <link href="<?php echo base_url(); ?>assets/fonts/Roboto.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  

    <!-- Custom styles for this template -->

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<style>
.modal{z-index: 9999;}
</style>
</head>

<body>
<button onclick="topFunction()" id="myBtn" title="Go to top"><span class=""></span></button>

<header>
<div class="section_three" id="header">
  <div class="container">
     <div class="col-md-12 col-sm-12 col-xs-12 text-center">
      <div class="logo col-md-3 col-sm-12 col-xs-12"><a  href="index.php"><img src="<?php echo base_url(); ?>assets/images/logo.png" width="150"></a></div>
	  <div class="col-md-9 col-sm-12 col-xs-12 pull-right" style="">
	   
         
        <div class="" id="primary-menu">
		<form  class="cmxform form-inline" method="POST" id="form-login" accept-charset="utf-8" >
          <ul class="nav navbar-nav" <?php if(isset($this->session->user_id)) { echo "style='float:right'"; } ?>>
		<?php if(!isset($this->session->user_id))
			{ ?>	
			<li style="padding-bottom:5px"><label style="margin-right: 5px;    color: #000;" for="email">Username :</label> <input style="margin-right: 10px;height:31px;width: 150px;" required="required" type="text" name="email" value="" class="input-small logTxt" id="email"></li><li><label style="margin-right: 5px;    color: #000;" for="pwd">Password :</label> <input style="margin-right: 10px;height:31px;width: 150px;" required="required" type="password" name="pwd" value="" class="input-small logTxt" id="pwd"></li>
			<li class="login-help" style="display:none;"  id="termschkbox">
				<input type="checkbox" value="1" id="termscondition" name="termscondition"><span class="spanRememberme1"><a href="<?php echo base_url(); ?>index.php/home/termsofservice" style="color: #fff; text-decoration: underline;font-weight: bold;" class="termslink" target="_blank">Accept Terms and Conditions</a></span>
				<div id="terrormsg" style="text-align: center;"></div>
			</li>
            <li><a href="javascript:;" id="submit" style="text-align: center;color: #fff;" name="submit" class="loginLink logSubmit"><?php echo $this->lang->line("login"); ?></a></li> <?php 
			} else { ?>
				
				<li><a href="<?php echo base_url('index.php/mypuzzleset1/dashboard#View') ?>" class="loginLink">Dashboard</a></li>
			<?php } ?>
			<li style="float: none;clear:both"><div id="errormsg" style="text-align: center;"></div></li>
			</ul>
			</form>
        </div>
       </div>
    </div>
  </div>
  </div>
</header>
<div class="modal fade"  id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    	  <div class="modal-dialog">
		 
				<div class="loginmodal-container">
				 <button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url();?>/assets/images/close.png" /></button>
					<h1>Login to Your Account</h1><br>
					
					<form  class="cmxform form-inline" method="POST" id="form-login" accept-charset="utf-8" > <ul><li><div id="errormsg"></div></li><li><label for="email">Email or Username</label><input required="required" type="text" name="email" value="" class="input-small logTxt" id="email"></li><li><label for="pwd">Password</label><input required="required" type="password" name="pwd" value="" class="input-small logTxt" id="pwd"></li>
					<li style="display:none">
					<select style="height:28px" name="ddlLanguage" class="input-small logTxt ddlHLanguage" id="ddlHLanguage">
								<option value="1">English</option>
								</select>
					</li>
					<li class="login-help" style="display:none;"  id="termschkbox">
						<input type="checkbox" value="1" id="termscondition" name="termscondition"><span class="spanRemembermepart"><a href="<?php echo base_url(); ?>index.php/home/termsofservice" style="color: #fff; text-decoration: underline;font-weight: bold;" class="termslink" target="_blank">Accept Terms & Conditions</a></span>
					</li>
					
					<li><input type="button" class="btn btn-primary logSubmit" id="submit" name="submit" value="Login"></li> </ul></form>
					
				 
				  <div class="login-help">
					<label><input type="checkbox" id="remember" name="remember"><span class="spanRememberme">Remember me</span></label>
				  </div>
				</div>
			</div>
		  </div>
		  
<script>
    $("#email").keyup(function(event){
     //alert('hai');
    if(event.keyCode == 13){
		$("#errormsg").hide();
        $("#submit").click();
    }
});

 $("#pwd").keyup(function(event){
     //alert('hai');
    if(event.keyCode == 13){
		$("#errormsg").hide();
        $("#submit").click();
    }
});
    
</script>
<!-- Sweet Alert -->
<!-- This is what you need -->
<script src="<?php echo base_url(); ?>assets/js/sweetalert/sweetalert2.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert/sweetalert2index.min.css">
<!--.......................-->		 

<div id="loginloadimage" style="display: none;" class="loader"></div>