<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['myprofile'] = 'My Profile';
$lang['personalinfo'] = 'Personal Information';
$lang['name'] = 'Name';
$lang['username'] = 'User Name';
$lang['fathersname'] = "Father's Name";
$lang['mothersname'] = "Mother's Name";
$lang['newpassword'] = "New Password";
$lang['conformpassword'] = "Confirm Password";
$lang['gender'] = "Gender";
$lang['dob'] = "Date of birth";
$lang['plandetails'] = "Plan Details";
$lang['planname'] = "Plan Name";
$lang['grade'] = "Grade";
$lang['validity'] = "Validity";
$lang['schoolname'] = "School Name";
$lang['expiry'] = "Expiry";
$lang['emailid'] = "Email ID";
$lang['phonenumber'] = "Phone Number";
$lang['address'] = "Address";
$lang['mandatory'] = "* Indicates required fields";


?>
