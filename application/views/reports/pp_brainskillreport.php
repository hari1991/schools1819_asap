     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="contentInner">
 <div class="container">



<div class="bspiCalender">
 <h2>Puzzle Performance Brain Skills</h2> 

 
<div class="bspiCalenderTop col-md-10 col-md-offset-1">
<span class="leftText"></span>

<div class="month">
 <div class="box">
     <form class="cmxform" method="POST" id="commentForm" accept-charset="utf-8"> 
	 <input type="hidden" name="planid" value="<?php echo $query[0]['gp_id']; ?>">
      <select name="game" id="game1">
	 <?php foreach($query1 as $gamename)

	 { ?>
		 
	<option value="<?php echo $gamename->gid; ?>"> <?php echo $gamename->gname; ?></option>
		  		  		 
	<?php } ?>
        
      </select>
	  </form>
    </div>
</div>
</div> 
<div class="clearfix"></div>
  
  
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
  
 <script type="text/javascript">


function Actualchart(gdate,gscore)
{
	 
    var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container',
			backgroundColor:'transparent',
			options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 30 
            },
            type: 'column'
        },
        title: {
            text: ''
        },
		tooltip: {enabled: false},exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Score'
            },
			max: 100 
        },xAxis: {
            categories: gdate,
			gridLineWidth: 0,
  minorGridLineWidth: 0
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: gscore
        }]
    });
	
}
function ajaxGraph(form)
{
	 
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/reports/brainskill_report_ajax') ?>",
data:form.serialize(),
dataType: 'json',
success:function(result)
{
//alert(result);
if(result!='')
{ 
	var gdate=[];
	var gscore=[];
	var v1=[];
	var k1=[];
	var k2=[];
	var v2=[];
	var arrgamedate = ((result));
	
$.each(arrgamedate, function(k1, v1) {
  //gdate=gdate+","+'"'+k+'"';

  $.each(v1, function(k2, v2) {
	    
	  if(k2=="lastupdate"){gdate.push(v2);}
	  if(k2=="game_score"){gscore.push(parseInt(v2));}
  });
   
});
//alert(gdate);
	Actualchart(gdate,gscore);
	 
}
}
});


}
 
$(document).ready(function(){
	ajaxGraph($("#commentForm")); 
$('#game1').change(function(){
var form=$("#commentForm");	
ajaxGraph(form);

	});
		});

</script>
 <div class="myBrainProfile">
 <h2 id="MonthID"></h2>
 <br/>
 <div id="container">
</div>
 
 
 
 </div> 

 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 .nice-select span.current{font-size: 20px}
 .nice-select .option {font-size: 10px}
 .nice-select ul{height:200px;overflow-y:scroll !important}
 </style>