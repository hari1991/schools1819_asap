<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Schools admin</title>
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/admin/img/metis-tile.png" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.css">   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css">   
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/metisMenu.css">    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/animate.css">
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/theme.less">
	
	


	
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>


</head>

 <body class="  ">
<div class="bg-dark dk" id="wrap" style="margin-bottom: 25px;">
	<div id="top">
	<!-- .navbar -->
		<nav class="navbar navbar-inverse navbar-static-top">
			<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
				<header class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a href="javascript:;" class="navbar-brand"><img src="<?php echo base_url(); ?>assets/images/logo.png" width="150" alt=""></a>
				</header>
			</div>
		</nav>
	</div>
</div>


<div class="container">
<div class="row">
<h2> Demo User List </h2>
<?php if(count($NithyaRefUsers>0)){?>
<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
	<thead>
	<tr>
		<th>S.No.</th>
		<th>Username</th>
		<th>Password</th>
		<th>Grade Name</th>
		<th>Memory</th>
		<th>Visual Processing</th>
		<th>Focus & Attention</th>
		<th>Problem Solving</th>
		<th>Linguistics</th>
		<th>BSPI</th>
		<th>Creation Date</th>
		<!-- <th>Last Login Date</th> -->
		<th>Last Played Date</th>						
	</tr>
</thead>
<tbody>
	<?php $i=1; foreach($NithyaRefUsers as $r1) {
		
		
	?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo $r1['username']; ?></td>
			<td><?php echo $r1['org_pwd']; ?></td>
			<td><?php echo $r1['gradename']; ?></td>
			<td><?php if($r1['skillscorem']==''){echo '-';$flag=1;}else{echo round($r1['skillscorem'], 2);} ?></td>
			<td><?php if($r1['skillscorev']==''){echo '-';$flag=1;}else{echo round($r1['skillscorev'], 2);} ?></td>
			<td><?php if($r1['skillscoref']==''){echo '-';$flag=1;}else{echo round($r1['skillscoref'], 2);} ?></td>
			<td><?php if($r1['skillscorep']==''){echo '-';$flag=1;}else{echo round($r1['skillscorep'], 2);} ?></td>
			<td><?php if($r1['skillscorel']==''){echo '-';$flag=1;}else{echo round($r1['skillscorel'], 2);} ?></td>
			<td><?php if($r1['avgbspiset1']==''){echo '-';}else{echo round($r1['avgbspiset1'], 2);}
			$var1+=round($r1['avgbspiset1'], 2); $var2+=1; ?></td>
			<td><?php  if($r1['creation_date']!=''){echo date('d-m-Y',strtotime($r1['creation_date']));}else{echo '-';} ?></td> 
			<!-- <td><?php if($r1['login_date']!=''){echo date('d-m-Y',strtotime($r1['login_date']));}else{echo '-';}  ?></td> -->
			
			<td><?php if($r1['status']=='') { echo $compdate=''; } else { echo date('d-m-Y',strtotime($r1['completeddate'])); } ?></td>  
		</tr>
							
					<?php $i++; }  ?>	 
                    </tbody>                
					</table>
<?php } else{ ?>
	<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
				<thead>
                    <tr>
						<th>s.no</th>
                        <th>Name</th>
						<th>Username</th>
						<th>Password</th>						
						<th>Grade Name</th>
						<th>Memory</th>
						<th>Visual processing</th>
						<th>Focus and attention</th>
						<th>Problem solving</th>
						<th>Linguistics</th>
						<th>BSPI</th>
						<th>Completed Date</th>
					</tr>
				</thead>
				<tbody>
					<tr>No record found</tr>
				</tbody>               
</table>

<?php } ?>
</div>
</div>
<link href="<?php echo base_url(); ?>assets/admin/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/admin/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.tableTools.js" type="text/javascript"></script>
					<script>
					
					$('#dataTable').dataTable();
					</script>
					