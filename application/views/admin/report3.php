 <header class="head">
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
           Performance Report
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
	 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Skill</h5>
            </header>
			<form id="srchreport" action="<?php echo base_url(); ?>index.php/admin/report3" method="POST">
			<div class="row">
			<div class="col-sm-3">
			<label>Schools</label>
			<select  class="form-control input-sm" name="schoolid" id="schoolid">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"  <?php if($_POST['schoolid']== $sc['id']) { ?>  selected='selected' <?php } ?>     ><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			<div class="col-sm-2">
			<label>Grade</label>
			<select  class="form-control input-sm" name="gradeid" id="gradeid" >
			<option value="">Select</option>
			<?php foreach($grades as $gd) { ?>
			<option value="<?php echo $gd['id']; ?>"    <?php if($_POST['gradeid']== $gd['id']) { ?>  selected='selected' <?php } ?>    ><?php echo $gd['classname']; ?></option>
			<?php } ?>
			</select>
			<!--<input type="button" name="sbmtbtn" id="sbmtbtn" Value="Get result">-->
			</div>
			
			<div class="col-sm-2">
			<label>Section</label>
			<select  class="form-control input-sm sectionid" name="sectionid"  id="sectionid" >
			<option value="">Select</option>
			<?php if(isset($_POST['gradeid'])){   	 ?>
			<option value="<?php echo $data; ?>" selected='selected'><?php echo $data; ?></option>
			 <?php } ?>
			</select>
			 
			</div>
			
			<div class="col-sm-2">
			<label>Bspi Range From</label>
			<input type="text" class="form-control" value="<?php echo isset($_POST['rangefrom']) ? $_POST['rangefrom'] : '' ?>" name="rangefrom" id="rangefrom" />
			</div>
			<div class="col-sm-2">
			<label>Bspi Range To</label>
			<input type="text" class="form-control" value="<?php echo isset($_POST['rangeto']) ? $_POST['rangeto'] : '' ?>" name="rangeto" id="rangeto" /></div>
			<!--<select  class="form-control input-sm" name="bspirange" id="bspirange" >
			<option value="">Select</option>
			<option value="0 AND 10">0 - 10</option>
			<option value="10 AND 20">10 - 20</option
			<option value="20 AND 30">20 - 30</option>
			<option value="30 AND 40">30 - 40</option>
			<option value="40 AND 50">40 - 50</option>
			<option value="50 AND 60">50 - 60</option>
			<option value="60 AND 70">60 - 70</option>
			<option value="70 AND 80">70 - 80</option>
			<option value="80 AND 90">80 - 90</option>
			<option value="90 AND 100">90 - 100</option>
			</select>-->
			</div>
			
			<div class="col-sm-2">
			<input type="submit" name="sbmtbtn" id="sbmtbtn" Value="Get result">
			</div>
			
			</div>
			
			</form>
			
			<!--<div id="collapse4" class="body">
 <?php if(isset($GradewiseScore_data2)){   	 ?>
 <h4>Report for puzzle set - 1</h4><?php } ?>
 <div id="container2">
</div></div>-->
			
			
            <div id="collapse4" class="body">
			<?php if(isset($GradewiseScore_data1)){   	 ?>
			<h4>Performance Report</h4><?php } ?>
                <div id="container1">		
</div></div>
 

<!--<div id="collapse4" class="body">
<?php if(isset($GradewiseScore_data3)){   	 ?>
<h4>Report for puzzle set - 3</h4><?php } ?>
 <div id="container3">
</div></div>-->

<!--<div id="collapse4" class="body">
<?php if(isset($GradewiseScore_data_overall)){   	 ?>
<h4>Report for Overall Sets</h4><?php } ?>
 <div id="container4">
</div></div>-->
            
        </div>
    </div>
</div>
</div>
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
 <script type="text/javascript">

<?php //print_r($GradewiseScore_data1); ?> 
$(document).ready(function(){
 
// $('.sectionid').val() = "<?php echo $_POST['sectionid'];?>";	
	
<?php if(isset($GradewiseScore_data1)){   	 ?>
	bubblereport1('container1'); 
<?php } ?>

<?php if(isset($GradewiseScore_data2)){   	 ?>
	bubblereport2('container2'); 
<?php } ?>

<?php if(isset($GradewiseScore_data3)){   	 ?>
	bubblereport3('container3'); 
<?php } ?>

<?php if(isset($GradewiseScore_data_overall)){   	 ?>
	bubblechart_overall('container4'); 
<?php } ?>

	$("#srchreport").validate({
		rules: {
			schoolid: {required: true},
            gradeid: {required: true},
			sectionid: {required: true},
			rangefrom: {required: true},
			rangeto: {required: true}
 
		   },
		   messages: {
			schoolid: {required: "Please choose school"},
			gradeid: {required: "Please choose gradeid"},
			sectionid:{required: "Please choose sectionid"},
			rangefrom:{required: "Please enter from range"},
			rangeto:{required: "Please enter to range"}
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	
});

$('#gradeid').change(function(){
	ajaxsectionload(schoolid,gradeid);
	
});

$('#sectionid').change(function(){
	//bubblereport1(schoolid,gradeid,sectionid);
});

$('#schoolid').change(function(){
	if($("#srchreport").valid()==true)
	{
	}
});


$('#sbmtbtn').click(function()
{
	if($("#srchreport").valid()==true)
	{
		//$('#sbmtbtn').click();
	}
});

function ajaxsectionload(schoolid,gradeid)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();

$.ajax({
		 url: "<?php echo base_url(); ?>index.php/admin/getsection",
		data:{schoolid:schoolid,gradeid:gradeid},
		success: function(result)
		{
		//alert(result);
		$('#sectionid').html(result);
		}
	});
	}
	
	function bubblereport1(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		
		  events: {
            load: function(){
				        this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  

				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
         
		gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false
}
    },

    yAxis: {
		title: {text: 'Score'},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.point.name +'</b>';
            }
        }
    }
},
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradewiseScore_data1 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
		}
		?>
            
        ] 
    }]

});
	
}

function bubblereport2(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		
		  events: {
            load: function(){
				        this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  

				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
         
		gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false
}
    },

    yAxis: {
		title: {text: 'Score'},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.point.name +'</b>';
            }
        }
    }
},
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradewiseScore_data2 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
		}
		?>
            
        ] 
    }]

});
}

function bubblereport3(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		
		  events: {
            load: function(){
				        this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  

				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
         
		gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false
}
    },

    yAxis: {
		title: {text: 'Score'},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.point.name +'</b>';
            }
        }
    }
},
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradewiseScore_data3 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
		}
		?>
            
        ] 
    }]

});
}


function bubblechart_overall(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		  events: {
            load: function(){
				this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  
				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false
}
    },

    yAxis: {
		title: {text: 'Score'},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '<b>'+ this.point.name +'</b>';
            }
        }
    }
},	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradewiseScore_data_overall as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",marker: {width: 40,height: 40,symbol: 'url(".base_url().$myavatarimage.")'}},";
			}
		}
		?>
            
        ]
         
    } ]

});
	
}



		 
</script>
 <style>
 body{min-height:0 !important;}
 .nice-select span.current{font-size: 20px}
 .nice-select .option {font-size: 10px}
 .nice-select ul{height:200px;overflow-y:scroll !important}
 </style>