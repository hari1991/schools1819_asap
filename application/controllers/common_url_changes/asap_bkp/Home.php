<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Assessment_model');
				$this->load->model('Assessment_model_set2');
				$this->load->model('Assessment_model_set3');
				$this->load->library('session');			
				//$this->lang->load("menu_lang","french");		
			
        }
		
	public function index()
	{		
	 
if($this->session->user_id=="" || !isset($this->session->user_id)){ }else{redirect('index.php/mypuzzleset1/dashboard');}	
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	
	public function asapuserlogin()
	{
		//print_r($_POST); 
		
		/* if (!empty($_POST))
		{
			 
			$data['query'] = $this->Assessment_model->checkUser($this->input->post('email'),$this->input->post('pwd'), $this->input->post('ddlLanguage')); */
			
			$username = $this->uri->segment(3); 
			$key = $this->uri->segment(4);
			$password = $this->uri->segment(5);			
	 
	 if((isset($username) && $username!='' ) && (isset($key) && $key!=''))
		
		{
			
		if($key==date("Ymd"))
		
		{
			$data['query'] = $this->Assessment_model->checkUser($username,$password);
			 
			if(isset($data['query'][0]->id)){
				
				$r=$array = json_decode(json_encode($data['query'][0]), True);
				
	
			/* Creating unique login ID */
				$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);				
				
				$this->Assessment_model->update_loginDetails($data['query'][0]->id,$uniqueId);
				
				
				$login_count=$data['query'][0]->login_count;
				$prelogin_date=$data['query'][0]->pre_logindate;
				$fname=$data['query'][0]->fname;
				 $todaydate=date('Y-m-d');
 $login_date=$data['query'][0]->login_date;
 $start_ts = strtotime($login_date);
 $end_ts = strtotime($todaydate);
 $diff = $end_ts - $start_ts;
 $datediff = round($diff / 86400);
 
				if((isset($login_count) && $login_count<=1) || $prelogin_date == "0000-00-00")
{
$greetings_content = 'Thanks for subscribing to SkillAngels. I am sure you will enjoy the learning process.<br>
Good Luck
';
}

elseif((isset($login_count) && $login_count>1) && $datediff <3 )
{
$greetings_content ='You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.
<br> Nice to see you pay so much attention';
}

elseif((isset($login_count) && $login_count>1) && ($datediff >= 3 && $datediff < 7 ))
{
$greetings_content =' You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.</br>
You were missing for the last '.$datediff.'</br>
You missed '.$datediff.' training </br>
Everything Okay?';
}

elseif((isset($login_count) && $login_count>1) && ($datediff >= 7 && $datediff < 14 ))
{
$greetings_content ='You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.
<br>You were missing for the last '.$datediff.' 
<br>Please make it a habit to use SkillAngels regularly';
}
elseif((isset($login_count) && $login_count>1) && ($datediff > 30))
{
$greetings_content ='You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.
<br>You were missing for the last '.$datediff.' 
<br>That seems to be a long break. Please resume your training
<br>Good Luck
';
}

$data['SetDetails2']=$this->Assessment_model_set2->getSetDetails($data['query'][0]->gp_id,$data['query'][0]->grade_id);

$data['SetDetails3']=$this->Assessment_model_set3->getSetDetails($data['query'][0]->gp_id,$data['query'][0]->grade_id);
		//$pid =  $this->session->gp_id; 
		 //$pid=$data['SetDetails'][0]['planid'];
		 
		// print_r($data['SetDetails2']);
			session_start();
			
if($data['query'][0]->grade_id==2)
{
	$gamepath = 'ukg';
}
else if(($data['query'][0]->grade_id==3) || ($data['query'][0]->grade_id==4)  || ($data['query'][0]->grade_id==5)  || ($data['query'][0]->grade_id==6)  || ($data['query'][0]->grade_id==7)  || ($data['query'][0]->grade_id==8) || ($data['query'][0]->grade_id==9) || ($data['query'][0]->grade_id==10))
{
	$gamepath = '2';
}
else
{
	$gamepath = '1';
}	
		
			$this->session->set_userdata(array(
                            'userlang'       => $gamepath,
                            'gp_id'       => $data['query'][0]->gp_id,
                            'id'       => $data['query'][0]->id,
                            'user_id'       => $data['query'][0]->id,
                            'fname'      => $data['query'][0]->fname,
                            'username'      => $data['query'][0]->username,
                            'login_count'      => $data['query'][0]->login_count,
                            'ltime'      => time(),
                            'fullname'      => $data['query'][0]->fname.' '.$data['query'][0]->lname,
                            'game_plan_id'      => $data['query'][0]->gp_id,
                            'game_grade'      => $data['query'][0]->grade_id,
                            'school_id'      => $data['query'][0]->sid,
                            'section'      => $data['query'][0]->section,
                            'greetings_content'      => $greetings_content,
							'set2planid' =>  $data['SetDetails2'][0]['planid'],
							'set2gradeid' => $data['SetDetails2'][0]['gradeid'],
							'set3planid' =>  $data['SetDetails3'][0]['planid'],
							'set3gradeid' => $data['SetDetails3'][0]['gradeid'],
							'login_session_id'=>$uniqueId					
							
                    ));
		
					if($this->session->login_count==0){$this->session->set_userdata('showIntro', 1);}
					else{$this->session->set_userdata('showIntro', 0);}
		$language = $data['query'][0]->languagekey;
		$language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
					
		$this->Assessment_model->termsandcondition(1,$data['query'][0]->id);	// Terms & Condition			

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
 $ip=$_SERVER['HTTP_CLIENT_IP'];}
 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
 $ip=$_SERVER['REMOTE_ADDR'];}		
$this->Assessment_model->insert_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);			
				

					 //redirect('index.php/home/profile');
			 echo	$result='ASAP'; exit;
		//redirect('index.php/home/dashboard');
		
		
			}
		}
		}
	}
	
	public function dashboard()
	{
		//echo 'ai'; exit;
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		$data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		
		$data['assignGames']=$this->Assessment_model->getAssignGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid);
		
		
		
		$cur_day_skills = count($data['randomGames']);
		$assign_count = count($data['assignGames']);
		if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) {
				$this->fn_Rand_games($userid, $check_date_time, $cur_day_skills, $assign_count,$catid);
			}
		$data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$where = "";
		foreach($data['randomGames'] as $brainData )
		{
			$brainIds[] = $brainData['gid'];
			$active_game_ids = @implode(',',$brainIds);
			$where = " and g.gid in ($active_game_ids)";
		}
		
		/*$data['query1'] = $this->Assessment_model->getsetdatas($this->session->gp_id,$this->session->game_grade);
		
		$this->session->set_userdata(array(
                            'gradeid_set2'       => $data['query1'][0]->gradeid_set2,
                            'gradeid_set3'       => $data['query1'][0]->gradeid_set3
							)); */
	//echo $this->session->gradeid_set3; exit;
			 
		$data['actualGames'] = $this->Assessment_model->getActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
 
	 $restrainCalendar= $this->Assessment_model->getTrainCalendar($userid,date('Y-m-01'),date('Y-m-d'),$catid);
	 $data['arrtrainCalendar']=explode(',',$restrainCalendar[0]['updateDates']);
	 $data['trophies']=array();
	 $data['trophies']= $this->Assessment_model->getMyCurrentTrophies($userid);
	 
	 $data['controllername'] = "home";

	/* echo "<pre>";
	 print_r($data['arrtrainCalendar'] );
	 exit;*/
		//$data['query'] = $this->Assessment_model->getleftbardata($userid);
		
		//$data['mytrophy'] = $this->Assessment_model->gettrophy($userid);
		//$this->in_array_r();
		 
		 
	
		//print_r($data['mytrophy']);  
		$this->load->view('headerinner', $data);
        $this->load->view('home/dashboard', $data);
		$this->load->view('footerinner');
	
	//redirect('index.php/home/dashboard');
	
	}
	
public function fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid) {
//echo "fn_Rand_games";

$arrSkills=$this->Assessment_model->getSkillsRandom($catid);
	
	  foreach($arrSkills as $gs_data)
	  {
		  $rand_sel = $this->Assessment_model->assignRandomGame($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
		   
		  $rand_count = count($rand_sel);	
		  if($rand_count <=0) {
					$del_where = "";
					if($assign_count <> $cur_day_skills && $cur_day_skills > 0)
						$del_where = " and created_date = '$check_date_time'";
					 $this->Assessment_model->deleteRandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$del_where);
					 
				} 
				if($rand_count > 0) {
					$rand_data = $this->Assessment_model->insertRandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$gs_data['skill_id'],$this->session->school_id,$this->session->section,$rand_sel[0]['gid'],$check_date_time);
					 
				}
	  }
	  $data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$cur_day_skills = count($data['randomGames']);
		if($cur_day_skills == 0)
				$this->fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count);
		 
			
	 
}
	
	public function language()
	{
	
	 $email = $this->input->post('Lemail'); 
		
		$data['query'] = $this->Assessment_model->languagechange($email);
		
		//print_r($data['query']);
		$str_language='';
		foreach($data['query'] as $data)
		{
			
			$str_language.='<option value="'.$data->ID.'">'.$data->name.'</option>';
	
		}	

	
	 if($str_language==''){$str_language='<option value="1">English</option>';}
		echo $str_language;exit;

	}
	
	public function logout()
	{	$data['disablemenus'] = $this->Assessment_model->menudisable($this->session->user_id,date('Y-m-d'));	
		$data['disablemenus2'] = $this->Assessment_model_set2->menudisable2($this->session->user_id,date('Y-m-d'));
		$data['disablemenus3'] = $this->Assessment_model_set3->menudisable3($this->session->user_id,date('Y-m-d'));
		
		$this->load->view('headerinner',$data);
		$this->load->view('logout/logout');
		//$this->load->view('footerinner'); 
		
		
	}
	public function mainlogout()
	{	
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		$this->Assessment_model->update_logout_log($this->session->user_id,$this->session->login_session_id);
		$exeislogin = $this->Assessment_model->updateuserloginstatus($this->session->user_id,$this->session->login_session_id);
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
		unset($_SESSION);
		$this->session->sess_destroy();
		redirect("https://html5devschools.skillangels.com/");
	}
	public function gamesajax()
	{
		$gameurl =  $_POST['gameurl']; 
		 $gname = substr($gameurl, strrpos($gameurl, '/') + 1);
	     $gamename = str_replace('.html','', $gname); 
		
		$data['gameid'] = $this->Assessment_model->getgameid($gamename);
		$gameid = $data['gameid'][0]['gid']; 
		$data['checkgame'] = $this->Assessment_model->checkgame($gameid);
		
		$gid = $data['checkgame'][0]['gameid']; 
		
		if($gid==1)
		{
			
		$this->session->set_userdata(array( 'currentgameid'=> $gameid ));
		$this->session->set_userdata(array( 'isskillkit'=> $_POST['skillkit'] ));
		$this->session->set_userdata(array( 'controllername'=> $_POST['controllername'] ));
		echo $this->session->currentgameid;exit;
		}
		else{
			
			echo 'IA'; exit;
		}
		
		 
	}
	public function result()
	{
			if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');exit;}
	if(!isset($_POST)){redirect('index.php'); exit;}
	if(empty($_POST)){redirect('index.php'); exit;}
		$total_ques=$_POST["tqcnt1"];
$attempt_ques=$_POST["aqcnt1"];
$answer=$_POST["cqcnt1"];
$score=$_POST["gscore1"];
$a6=$_POST["gtime1"];
$a7=$_POST["rtime1"];
$a8=$_POST["crtime1"];
$a9=$_POST["wrtime1"];	
$gameid=$this->session->currentgameid;
$isskillkit=$this->session->isskillkit;
if($isskillkit=="Y"){$skillkit=1;}
else{$skillkit=0;}
/*
echo "<pre>";
print_r($_SESSION);
exit;

		/*echo 'hai'; exit;*/
		
		$userlang = $this->session->userlang;
		$userid = $this->session->user_id; 
		$lastup_date = date("Y-m-d");
		$cid = 1;
		
		$contname = $this->session->controllername;
		if($contname=='mypuzzleset2')
		{
			$data['gameDetails'] = $this->Assessment_model_set2->getresultGameDetails($userid,$gameid);

		$skillid =$data['gameDetails'][0]['gameskillid'] ; 
		  
		//$createddate = NOW();
	$data['SetDetails']=$this->Assessment_model_set2->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
		//$pid =  $this->session->gp_id; 
		 $pid=$data['SetDetails'][0]['planid'];
		
		 
		if($skillkit==0){
		$data['insert1'] = $this->Assessment_model_set2->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		else if($skillkit==1){
		$data['insert1'] = $this->Assessment_model_set2->insertone_SK($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		$data['insert2'] = $this->Assessment_model_set2->insertlang($gameid,$userid,$userlang,$skillkit);
 		
		$acid = $data['gameDetails'][0]['academicid'];
		$st = 1;
		$data['insert3'] = $this->Assessment_model_set2->insertthree($userid,$gameid,$acid,$lastup_date,$st);
		
		echo "1";exit;
		//$this->load->view('gameresult/Result');
		}
		elseif($contname=='mypuzzleset3')
		{
			
			$data['gameDetails'] = $this->Assessment_model_set3->getresultGameDetails($userid,$gameid);

		$skillid =$data['gameDetails'][0]['gameskillid'] ; 
		  
		//$createddate = NOW();
		 
	$data['SetDetails']=$this->Assessment_model_set3->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
		//$pid =  $this->session->gp_id; 
		 $pid=$data['SetDetails'][0]['planid'];
		if($skillkit==0){
		$data['insert1'] = $this->Assessment_model_set3->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		else if($skillkit==1){
		$data['insert1'] = $this->Assessment_model_set3->insertone_SK($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		$data['insert2'] = $this->Assessment_model_set3->insertlang($gameid,$userid,$userlang,$skillkit);
 		
		$acid = $data['gameDetails'][0]['academicid'];
		$st = 1;
		$data['insert3'] = $this->Assessment_model_set3->insertthree($userid,$gameid,$acid,$lastup_date,$st);
		
		 echo "1";exit;
		//$this->load->view('gameresult/Result');
			
		}
		else
		{
	
		$data['gameDetails'] = $this->Assessment_model->getresultGameDetails($userid,$gameid);

		$skillid =$data['gameDetails'][0]['gameskillid'] ; 
		  $data['SetDetails']=$this->Assessment_model->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
		//$createddate = NOW();
		 
		//$pid =  $this->session->gp_id; 
		$pid=$data['SetDetails'][0]['planid'];
		//$curdate = date("Y-m-d");
		$data['getcurdayskillid'] = $this->Assessment_model->getcurdayskillid($userid,$skillid);
		$skillidcount = $data['getcurdayskillid'][0]['skillcount'];
		 
		//$array = $this->Assessment_model->getskillid($userid,$curdate);
		
		
		if(($skillid!=0) && ($skillidcount==0)) {
		if($skillkit==0){
		$data['insert1'] = $this->Assessment_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		/*Function to move users from ASAP to CLP*/
			$data['getuserscount']  = $this->Assessment_model->checkandUpgradeUserStatus($userid,$this->session->username);
		}
		else if($skillkit==1){
		$data['insert1'] = $this->Assessment_model->insertone_SK($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		$data['insert2'] = $this->Assessment_model->insertlang($gameid,$userid,$userlang,$skillkit);
 		
		$acid = $data['gameDetails'][0]['academicid'];
		$st = 1;
		$data['insert3'] = $this->Assessment_model->insertthree($userid,$gameid,$acid,$lastup_date,$st);
		
		}
		echo "1";exit;
		//$this->load->view('gameresult/Result');
		}
	}
	
	
	public function profile()
	{
		 
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
				
				if(isset($_FILES["file"]["type"]))
{
	
$validextensions = array("jpeg", "jpg", "png");
$temporary = explode(".", $_FILES["file"]["name"]);
$file_extension = end($temporary);
 $size = getimagesize($files);
$maxWidth = 150;
$maxHeight = 150;
if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
) && in_array($file_extension, $validextensions)) {

$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
$targetPath = "assets/images/avatarimages/".$_FILES['file']['name']; // Target path where file is to be stored
//$targetPath = "upload/".$_FILES['file']['name'];

move_uploaded_file($sourcePath,$targetPath) ; 

}
}
/*
		$data['updatemsG']="";
		if($_REQUEST)
		{
		$sfname =  $this->input->post('sfname');
	$gender = $this->input->post('gender');
	$dob = $this->input->post('dob');
	$fathername = $this->input->post('fathername');
	$mothename = $this->input->post('mname');
	$address = $this->input->post('saddress');
	$emailid = $_POST['emailid'];
	$sphoneno = $this->input->post('sphoneno');
	$id = $this->input->post('id');
	$newpass = $this->input->post('newpass');
	$confirmpass = $this->input->post('cpass');
		
		
		$userid = $this->session->user_id;
		$data['updateprofile'] = $this->Assessment_model->updateprofile($sfname,$gender,$dob,$fathername,$mothename,$address,$sphoneno,$id,$newpass,$confirmpass,$targetPath);
		$data['updatemsG']="Updated Successfully !!!";
		}
		*/
		
		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$userid = $this->session->user_id;
		//$data['query'] = $this->Assessment_model->getleftbardata($userid);
		//$data['mytrophy'] = $this->Assessment_model->gettrophy($userid);
		$data['myprofile'] = $this->Assessment_model->getmyprofile($userid);
		$planid = $data['myprofile'][0]['gp_id'];
		$schoolid = $data['myprofile'][0]['sid'];
		$gradeid = $data['myprofile'][0]['grade_id']; 
		$data['getplandetails'] = $this->Assessment_model->getplandetais($planid);
		$data['getgrade'] = $this->Assessment_model->getgradedetais($gradeid);
		
		
		/*BSPI COMPARISON*/
			$data['bspicomparison'] = $this->Assessment_model->getbspicomparison($this->session->school_id,$this->session->game_grade,$userid);	
		/*BSPI COMPARISON*/
		/* Menu Hide and show */
		$data['disablemenus'] = $this->Assessment_model->menudisable($userid,date('Y-m-d'));	
		$data['disablemenus2'] = $this->Assessment_model_set2->menudisable2($userid,date('Y-m-d'));
		$data['disablemenus3'] = $this->Assessment_model_set3->menudisable3($userid,date('Y-m-d'));
		/* Menu Hide and show */
		$this->load->view('headerinner', $data);
        $this->load->view('myprofile/newmyprofile', $data);
		$this->load->view('footerinner');
		
	}
	
	
	public function myprofile()
	{

		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
		
		if(isset($_FILES["file"]["type"]))
{
	
$validextensions = array("jpeg", "jpg", "png");
$temporary = explode(".", $_FILES["file"]["name"]);
$file_extension = end($temporary);
 $size = getimagesize($files);
$maxWidth = 150;
$maxHeight = 150;
if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
) && in_array($file_extension, $validextensions)) {

$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
$targetPath = "assets/images/avatarimages/".$_FILES['file']['name']; // Target path where file is to be stored
//$targetPath = "upload/".$_FILES['file']['name'];

move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
//echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
//echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
//echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
//echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
//echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";

}
}


		
		

		$data['updatemsG']="";
		if($_REQUEST)
		{ //echo "<pre>";print_r($_REQUEST);exit;
		$sfname =  $this->input->post('sfname');
	$gender = $this->input->post('gender');
	$dob = $this->input->post('dob');
	$fathername = $this->input->post('fathername');
	$mothename = $this->input->post('mname');
	$address = $this->input->post('saddress');
	$emailid = $this->input->post('email');
	$sphoneno = $this->input->post('sphoneno');
	$id = $this->input->post('id');
	$newpass = $this->input->post('newpass');
	$confirmpass = $this->input->post('cpass');
	
	//$password = $_POST['txtOPassword'];
				 $hashpass = $this->salt_my_pass($newpass);
				 
				$shpassword = $hashpass['Hash']; 
				$salt1 = $hashpass['Salt1']; 
				$salt2 = $hashpass['Salt2']; 
				
				$saltedpass = $salt1 . $shpassword . $salt2;
		
		$userid = $this->session->user_id;
		$data['updateprofile'] = $this->Assessment_model->updateprofile($sfname,$gender,$emailid,$dob,$fathername,$mothename,$address,$sphoneno,$id,$shpassword,$salt1,$salt2,$confirmpass,$targetPath);
		$data['updatemsG']="Updated Successfully !!!";
		redirect("index.php/home/profile");
		}
		
		
		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$userid = $this->session->user_id;
		//$data['query'] = $this->Assessment_model->getleftbardata($userid);
		//$data['mytrophy'] = $this->Assessment_model->gettrophy($userid);
		$data['myprofile'] = $this->Assessment_model->getmyprofile($userid);
		$planid = $data['myprofile'][0]['gp_id'];
		$schoolid = $data['myprofile'][0]['sid'];
		$gradeid = $data['myprofile'][0]['grade_id']; 
		$data['getplandetails'] = $this->Assessment_model->getplandetais($planid);
		$data['getgrade'] = $this->Assessment_model->getgradedetais($gradeid);
		
		
		 $data['disablemenus'] = $this->Assessment_model->menudisable($userid,date('Y-m-d'));	
		 $data['disablemenus2'] = $this->Assessment_model_set2->menudisable2($userid,date('Y-m-d'));
		 $data['disablemenus3'] = $this->Assessment_model_set3->menudisable3($userid,date('Y-m-d'));
		 
		
		$this->load->view('headerinner', $data);
        $this->load->view('myprofile/myprofile', $data);
		$this->load->view('footerinner');
	
	}
	public function mybrainprofile()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
	
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	
	
		$this->load->view('headerinner', $data);
        $this->load->view('mybrainprofile/mybrainprofile', $data);
		$this->load->view('footerinner');
		
	}
	public function ajaxcalendar()
	{
				 

		$yearMonthQry=$_REQUEST['yearMonth'];
		$userid = $this->session->user_id;
		
	
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	$bspicalendardays= $this->Assessment_model->mybspicalendar($this->session->school_id,$userid,$yearMonthQry);
	$mybspiCalendar=array();	
	foreach($bspicalendardays as $days)
	{
		$mybspiCalendar[$days['playedDate']]=$days['game_score'];
	}
	 $data['mybspiCalendar']=$mybspiCalendar;
	  
	$data['yearMonthQry']=$yearMonthQry;
	
		 
        $this->load->view('mybrainprofile/ajaxcalendar', $data);
 
		
	}
	public function ajaxcalendarSkillChart()
	{
				 

		$yearMonthQry=$_REQUEST['yearMonth'];
		$userid = $this->session->user_id;
		
 
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart("",$userid,$yearMonthQry);
  
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{
		$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);
	}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
	  
	$data['yearMonthQry']=$yearMonthQry;
	
		 
        $this->load->view('mybrainprofile/ajaxcalendarSkillChart', $data);
 
		
	}
	
	public function ajaxcalendarSkillChart1()
	{
		$userid = $this->session->user_id;
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart1("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
        $this->load->view('mybrainprofile/ajaxcalendarSkillChart', $data);
	}
	
	public function ajaxcalendarSkillChart2()
	{
		$userid = $this->session->user_id;
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart2("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
        $this->load->view('mybrainprofile/ajaxcalendarSkillChart', $data);
	}
	
	public function ajaxcalendarSkillChart3()
	{
		$userid = $this->session->user_id;
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart3("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
        $this->load->view('mybrainprofile/ajaxcalendarSkillChart', $data);
	}
	
	 
	 public function mytrophies()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$mytrophies= $this->Assessment_model->myTrophiesAll($userid,$startdate,$enddate);
	$arrmyTrophies=array();
	 foreach($mytrophies as $trophies)
	 {
		 $arrmyTrophies[str_pad($trophies['month'], 2, '0', STR_PAD_LEFT)][$trophies['category']]=$trophies['totstar'];
	 }
	 $data['arrmyTrophies'] =$arrmyTrophies;
		$this->load->view('headerinner', $data);
        $this->load->view('mytrophies/mytrophies', $data);
		$this->load->view('footerinner');
		
	}
	
	public function skillkit()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		 $this->fn_SK_Rand_games($userid, $check_date_time, '',100,$catid);
		$brain_rs = $this->Assessment_model->getAssignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$userid,$this->session->school_id);
		$where = "";
		$brainIds='';
		$data['actualGames']='';
			foreach( $brain_rs as $brainData)
			{
					$brainIds[] = $brainData['gid'];
			}
			if($brainIds)
			{			
					$active_game_ids = @implode(',',$brainIds);
					$where = " and g.ID in ($active_game_ids)";

			}
	if($brainIds)
			{				
			$data['actualGames'] = $this->Assessment_model->getSK_ActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
			}
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
  
		$this->load->view('headerinner', $data);
        $this->load->view('home/skillkit', $data);
		$this->load->view('footerinner');
	
	//redirect('index.php/home/dashboard');
	
	}
	public function fn_SK_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid) {
//echo "fn_Rand_games";

$arrSkills=$this->Assessment_model->getSK_SkillsRandom($uid);
 
	  foreach($arrSkills as $gs_data)
	  {
		  $rand_sel = $this->Assessment_model->assignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
		  
		  $cur_day_skills = count($rand_sel);	
		   
			  		  $rand_sel_limit = $this->Assessment_model->assignSK_assignGameCount($this->session->game_plan_id,$gs_data['skill_id'],$this->session->school_id);
$assign_count = $rand_sel_limit[0]['gameCount']; 


if($cur_day_skills<$assign_count){
	$rand_sel_rs=$this->Assessment_model->getSK_randomGames($this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$assign_count,$cur_day_skills);
 
 

$rand_count = count($rand_sel_rs);	

if($rand_count <=0) {
					$del_where = "";
					if($assign_count <> $cur_day_skills && $cur_day_skills > 0)
						$del_where = " and created_date = '$check_date_time'";
					 $this->Assessment_model->deleteSK_RandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$del_where);
} 
				 
				if($rand_count > 0) {
					foreach($rand_sel_rs as $rand_data1) { 
					$rand_data = $this->Assessment_model->insertSK_RandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$this->session->section,$rand_data1['gid'],$check_date_time);
					}
					 
				}
	  }
	  
	 		  $rand_sel = $this->Assessment_model->assignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
  $cur_day_skills = count($rand_sel);	
  if($cur_day_skills <$assign_count){
	  
	   $min_date_arr = $this->Assessment_model->getSK_mindatePlay($this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id ,$catid);
		$this->Assessment_model->deleteSK_OldGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$min_date_arr[0]['mindate']);
			
				
								$this->fn_SK_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,1);
				
			}
	  
	  
	  }
	   
			
	 
}

public function userfeedback($userid,$currentdate)
{
	
	$userid = $_REQUEST['userid'];
	$currentdate = $_REQUEST['currentdate'];
	
	$data['usercount'] = $this->Assessment_model->getusercount($userid,$currentdate);
	
	echo $data['usercount'][0]['usercount'];
	
}

public function userfeedbackinsert($userid,$currentdate,$star,$usercmnt)
{
	$userid = $_REQUEST['userid'];
	$currentdate = $_REQUEST['currentdate'];
	$star = $_REQUEST['stars'];
	$usercmnt = $_REQUEST['usercmnt'];
	
	$data['feedbackinsert'] = $this->Assessment_model->insertfeedback($userid,$currentdate,$star,$usercmnt);
	echo $result = 1;
}
public function islogin()
{ 
	if (!empty($_POST))
	{			 
		$exeislogin = $this->Assessment_model->islogin($this->input->post('email'),$this->input->post('pwd'),$this->config->item('ideal_time'));
		echo $exeislogin[0]->islogin;exit;
	}
}
public function checkuserisactive()
{
		$this->Assessment_model->update_login_log($this->session->user_id,$this->session->login_session_id);

	$exeislogin = $this->Assessment_model->checkuserisactive($this->session->user_id,$this->session->login_session_id);
	//echo $exeislogin[0]->isalive;exit;
	if($exeislogin[0]->isalive!=1)
	{ 	
//$exeislogin = $this->Assessment_model->updateuserloginstatus($this->session->user_id,$this->session->login_session_id);
		$this->session->unset_userdata();
		$this->session->sess_destroy();
		echo 1;exit;
	}
	else
	{
		echo 0;exit;
	}
}
public function termscheck()
{
	//print_r($_POST); 
	
	if (!empty($_POST))
	{
		$data['query'] = $this->Assessment_model->checkUser($this->input->post('email'),$this->input->post('pwd'), $this->input->post('ddlLanguage'));
		
		echo $data['query'][0]->agreetermsandservice; exit;
		
	}
}
public function termsofservice()
{
  $this->load->view('header');
  $this->load->view('footerpages/termsofservice');
  $this->load->view('footer');
}
	
public function privacypolicy()
{
  $this->load->view('header');
  $this->load->view('footerpages/privacypolicy');
  $this->load->view('footer');
}
	
public function faq()
{
  $this->load->view('header');
  $this->load->view('footerpages/faq');
  $this->load->view('footer');
}
public function salt_my_pass($password)
{
// Generate two salts (both are numerical)
$salt1 = mt_rand(1000,9999999999);
$salt2 = mt_rand(100,999999999);

// Append our salts to the password
$salted_pass = $salt1 . $password . $salt2;

// Generate a salted hash
$pwdhash = sha1($salted_pass);

// Place into an array
$hash['Salt1'] = $salt1;
$hash['Salt2'] = $salt2;
$hash['Hash'] = $pwdhash;

// Return the hash and salts to whatever called our function
return $hash;

}
public function unsetshowintro()
{
	$this->session->set_userdata('showIntro',0);
}
	
}
