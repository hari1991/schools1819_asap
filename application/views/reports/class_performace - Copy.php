     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="contentInner">
 <div class="container">



<div class="bspiCalender">
 <h2></h2> 
 
<div class="clearfix"></div>
  
  
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
 <script type="text/javascript">

function bubblechart1(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'bubble',
        plotBorderWidth: 1,
		  events: {
            load: function(){
				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				 this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 1,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
		labels:
{
  enabled: false
}
    },

    yAxis: {
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport1 as $datascore)
		{
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".$datascore['bspi']."},";
			}
		}
		?>
            
        ],
         marker: {
                symbol: 'url(<?php echo base_url();?>assets/images/avatar.png)'
            },
    }, {showInLegend: false, 
        data: [
             <?php 
			 if($GradeWiseReport1[$this->session->user_id]['name']==''){ echo "{name: '".$this->session->fname."',y:0},";}
else{
			 echo "{name: '".$GradeWiseReport1[$this->session->user_id]['name']."',y:".$GradeWiseReport1[$this->session->user_id]['bspi']."},";
}			 ?>
             
        ],
        marker: {
			symbol: 'url(<?php echo base_url();?>assets/images/avatar_blue.png)' 
        }
    }]

});
	
}

 function bubblechart2(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'bubble',
        plotBorderWidth: 1,
		  events: {
            load: function(){
				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				 this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 1,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
		labels:
{
  enabled: false
}
    },

    yAxis: {
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport2 as $datascore)
		{
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".$datascore['bspi']."},";
			}
		}
		?>
            
        ],
         marker: {
                symbol: 'url(<?php echo base_url();?>assets/images/avatar.png)'
            },
    }, {showInLegend: false, 
        data: [
             <?php
if($GradeWiseReport2[$this->session->user_id]['name']==''){ echo "{name: '".$this->session->fname."',y:0},";}
else{
			 echo "{name: '".$GradeWiseReport2[$this->session->user_id]['name']."',y:".$GradeWiseReport2[$this->session->user_id]['bspi']."},";
}
			 ?>
             
        ],
        marker: {
			symbol: 'url(<?php echo base_url();?>assets/images/avatar_blue.png)' 
        }
    }]

});
	
}

function bubblechart3(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'bubble',
        plotBorderWidth: 1,
		  events: {
            load: function(){
				//this.tooltip.refresh();
				// this.tooltip.refresh(this.series[0].data[0]);
 				 this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 1,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
		labels:
{
  enabled: false
}
    },

    yAxis: {
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b>';
    }
},
 
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport3 as $datascore)
		{
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".$datascore['bspi']."},";
			}
		}
		?>
            
        ],
         marker: {
                symbol: 'url(<?php echo base_url();?>assets/images/avatar.png)'
            },
    }, {showInLegend: false, 
        data: [
             <?php 
			 if($GradeWiseReport3[$this->session->user_id]['name']==''){ echo "{name: '".$this->session->fname."',y:0},";}
else{
			 echo "{name: '".$GradeWiseReport3[$this->session->user_id]['name']."',y:".$GradeWiseReport3[$this->session->user_id]['bspi']."},";
}
			 ?>
             
        ],
        marker: {
			symbol: 'url(<?php echo base_url();?>assets/images/avatar_blue.png)' 
        }
    }]

});
	
}

$(document).ready(function(){
	bubblechart1('container1'); 
<?php if(isset($GradeWiseReport2)){ ?>
	bubblechart2('container2');
<?php } ?>
<?php 	if(isset($GradeWiseReport3)){ ?>
	bubblechart3('container3'); 
<?php } ?>
 
		});

</script>

<div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID1">Puzzle set 1</h2>
 <br/>
 <div id="container1">
</div>
 </div> 
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID2">Puzzle set 2</h2>
 <br/>
 <div id="container2">
</div>
 </div> 
  <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID3">Puzzle set 3</h2>
 <br/>
 <div id="container3">
</div>
 </div> 
 </div> 
 
 
 
 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 .nice-select span.current{font-size: 20px}
 .nice-select .option {font-size: 10px}
 .nice-select ul{height:200px;overflow-y:scroll !important}
 </style>