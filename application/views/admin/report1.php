<header class="head">
<!-- /.search-bar -->
<div class="main-bar">
<h3>
<i class="fa fa-dashboard"></i>&nbsp;
BSPI Report
</h3>
</div>
<!-- /.main-bar -->
</header>
</div>
<div id="content">
<div class="outer">
<div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>BSPI Report</h5>
            </header>
			<form id="srchreport" method="POST">
			<div class="row">
			<div class="col-sm-3">
			<label>School</label>
			<select  class="form-control input-sm" name="schoolid" id="schoolid">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			<div class="col-sm-3" style="display:none";>
			<label>Schools</label>
			<select  class="form-control input-sm" name="sklid" id="sklid">
			<option value="">Select</option>
			<?php foreach($skls as $skl) { ?>
			<option value="<?php echo $skl['sklname']; ?>"><?php echo $skl['sklname']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			<div class="col-sm-2">
			<label>Grade</label>
			<select  class="form-control input-sm" name="gradeid" id="gradeid" >
			<option value="">Select</option>
			<?php foreach($grades as $gd) { ?>
			<option value="<?php echo $gd['id']; ?>"><?php echo $gd['classname']; ?></option>
			<?php } ?>
			</select>
			<!--<input type="button" name="sbmtbtn" id="sbmtbtn" Value="Get result">-->
			</div>
			
			<div class="col-sm-1">
			<label>Section</label>
			<select  class="form-control input-sm sectionid" name="sectionid"  id="sectionid" >
			<option value="">Select</option>
			<?php if(isset($_POST['gradeid'])){   	 ?>
			<option value="<?php echo $data; ?>" selected='selected'><?php echo $data; ?></option>
			 <?php } ?>
			</select>
			</div>
			
			<div class="col-sm-1">
					<input type="button"   name="sbmtbtn" id="sbmtbtn" Value="Generate Report">
				</div>
				
				<div class="col-sm-3 dwnbtn">
				</div>
			
			
			
			</div>
			</form>
			<div class="col-sm-3">
				<div style="display:none;" id="iddivLoading" class="loading">Loading&#8230;</div>
			</div>
            <div id="collapse4" class="body">
                <div id="report1"></div>
				<div class="row" id='exportbtn' style='display:none;'>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
				
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(document).ready(function(){
	
	
	$("#srchreport").validate({
		rules: {
			schoolid: {required: true},
            gradeid: {required: true}
 
		   },
		   messages: {
			schoolid: {required: "Please choose school"},
			gradeid: {required: "Please choose gradeid"}
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	$('#btnExcelExport').click(function(){
	excel_report1();
});
	
});

$('#gradeid').change(function(){
	
	ajaxsectionload(schoolid,gradeid);
	if($("#srchreport").valid()==true)
	{
report1(schoolid,gradeid);
	}
});

$('#sectionid').change(function(){
	if($("#srchreport").valid()==true)
	{
report1(schoolid,gradeid);
	}
});

$('#sklid').change(function(){
	if($("#srchreport").valid()==true)
	{
report1(schoolid,gradeid);
	}
});

$('#schoolid').change(function(){
	if($("#srchreport").valid()==true)
	{
report1(schoolid,gradeid);
	}
});

$('#sbmtbtn').click(function(){
	if($("#srchreport").valid()==true)
	{
generatereport(schoolid,gradeid);

	}
	
});


function ajaxsectionload(schoolid,gradeid)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();

$.ajax({
		 url: "<?php echo base_url(); ?>index.php/admin/getsection",
		data:{schoolid:schoolid,gradeid:gradeid},
		success: function(result)
		{
		//alert(result);
		$('#sectionid').html(result);
		}
	});
	}

function report1(schoolid,gradeid)
{
$("#iddivLoading").show();
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();
var sklid = $('#sklid').val();
var sectionid = $('#sectionid').val();
$.ajax({
    type: "POST",
	//dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/getreport1",
    data: {schoolid:schoolid,gradeid:gradeid,sklid:sklid,sectionid:sectionid},
    
    success: function(result){
		$("#iddivLoading").hide();
		//$("#sbmtbtn").show();
		// $("#pleasewait").html("");	 
		 if(result!='')
		 {
			 $('#exportbtn').show();
		 }
		 $('#report1').html(result);
		
    }
});
}	

function generatereport(schoolid,gradeid)
{
	
	$("#iddivLoading").show();
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();
var sklid = $('#sklid').val();
var sectionid = $('#sectionid').val();
$.ajax({
    type: "POST",
	dataType: "json",
    url: "<?php echo base_url(); ?>index.php/admin/generatereport",
    data: {schoolid:schoolid,gradeid:gradeid,sklid:sklid,sectionid:sectionid},
    
    success: function(result){
		$("#iddivLoading").hide();
	
	if($.trim(result.response)=='success')
		 {
		
		//	alert(result.gradeid);
			
		$(".dwnbtn").html('<a href="<?php echo base_url(); ?>report_image/'+result.schoolid+'_'+result.gradeid+'.zip" name="downloadzip" id="downloadzip" target="_blank"  class="btn btn-warning downloadbtn" style="float: right;">Download Report</a>');
			
		 }
		
    }
});
	
	
}


function excel_report1()
{
	var schoolid = 	$('#schoolid').val();
	var gradeid = $('#gradeid').val();
	var sklid = $('#sklid').val();
	var sectionid = $('#sectionid').val();
	$.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>index.php/admin/report1_excel",
    data: {schoolid:schoolid,gradeid:gradeid,sklid:sklid,sectionid:sectionid},
    
    success: function(result){
		
		window.location.href= "<?php echo base_url();?>"+result;
    }
});
}

		 
</script>
 
	  <style>
	   #error { color: red;}
         #sbmtbtn{padding: 10px 22px;margin: 5px auto;background: #e9e610;}
		 .dwnbtn{padding: 10px 22px;margin: 5px auto; }
		 
		 
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<script>
$( document ).ready(function() {
	$("#report1").on("click",'.downloadbtn',function(){
		var uid=$(this).attr('data-id'); 
		var fname=$(this).attr('data-fname'); 
		var gurl='<?php echo base_url('index.php/admin/downloadreport/') ?>'+uid;
		
		swal({
		  title: 'Student Name',
		  input: 'text',
		  inputValue: fname,
		  showCancelButton: true,
		  confirmButtonText: 'Submit',
		  showLoaderOnConfirm: true,
		  allowOutsideClick: false
		}).then(function (email) {
			$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/admin/getreportcard') ?>",
				data:{sname:email,userid:uid},
				success:function(result)
				{
					window.open(result);//document.location=result;
				}
			});
		});
		
		
	});
});
</script>