<footer>
<div class="container" id="footerpart">
<div class="row">
   
	  <div class="col-sm-6 col-lg-6 col-md-6">
	 <div class="row">
		<div class="col-sm-6 col-lg-6 col-md-6">
			<p style="text-align:center;color: #92278f;font-size: 20px;margin-bottom:10px"><strong>EdSix BrainLab<sup>TM</sup> Pvt Ltd</strong></p>
			<p style="text-align:center;margin-bottom:0px;margin-bottom:10px">Module #1, 3rd Floor, D Block,</p> 
			<p style="text-align:center;margin-bottom:0px;margin-bottom:10px">Phase 2, IITM Research Park,</p> 
			<p style="text-align:center;margin-bottom:0px;margin-bottom:10px">Kanagam Road, Taramani, Chennai - 600113 </p> 
		</div>
		<div class="col-sm-6 col-lg-6 col-md-6">
		 
			<p style="text-align:center;color: #92278f;font-size: 20px;margin-bottom:10px" class="callicon">044-66469877<br/> +91 95695 65454</p> 
			<p style="text-align:center;color: #92278f;font-size: 20px;margin-bottom:10px" class="msgicon"><a style="color: #92278f;" href="mailto:angel@skillangels.com">angel@skillangels.com</a></p>
  

		</div>
		</div>
		  
		  
     </div>    
				  
				 
               
				 
                <div class="col-sm-4 col-lg-6 col-md-6 ">
     <p style="text-align:center; margin-bottom:0px">  <a class="" href="javascript:;"><img src="<?php base_url(); ?>assets/images/index_logo.png" width="150px" alt="logo"></a></p> 
	 <p style="text-align:center; margin-bottom:0px">EdSix Brain Lab<sup>TM</sup> Pvt Ltd</p> 
     <p style="text-align:center; margin-bottom:0px">Incubated by IIT Madras' RTBI</p> 
     <p style="text-align:center; margin-bottom:0px">Supported by IIM Ahmedabad's CIIE and Village Capital, U.S.A</p> 
                      
                </div>
            </div>
</div>

</footer>



  <script src="<?php echo base_url(); ?>assets/js/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('ready', function() {
   /*   $(".regular").slick({
        dots: false,
		arrows: true,
		infinite: true,
		 autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1
      });
	  */
$('.medialogo').slick({
  dots: true,
  arrows: false,
  autoplay: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
    });
  </script>
<script type="text/javascript">
  $(document).ready(function(e) { 
   $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 6000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
});
</script>

<script>
window.odometerOptions = {
  format: '(ddd).dd'
};

$(document).ready(function(){	  	 
doAjax();	
});	

function doAjax()

	{   

 //alert(";;");
	   $.ajax({
		url: "answertime.php",
dataType:"json",
		success: function(data) {	
setTimeout(function(){
  odo1.innerHTML = data.tot_att;
    odo2.innerHTML =data.tot_ans;
    odo3.innerHTML =data.gtime
}, 1000);		  	
		}
	})
/*
setTimeout(function(){
    odo1.innerHTML = 909893;
    odo2.innerHTML =2839302;
}, 1000);	
*/
	}
	setInterval(function(){doAjax();}, 10000);  // 1000 = 1 second, 3000 = 3 seconds	
	
/* ****************************** User Login *********************************** */	
$('#submit').click(function(){ 
var form=$("#form-login");

$(".loader").show();

/* Avoid Multiple Login */	
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/home/islogin') ?>",
data:form.serialize(),
success:function(isloginval)
{ //alert(isloginval);
	if(isloginval==0){
		if(($('#termscondition').is(':checked')) )
		{
			userlogin(form);
		}
		else
		{
			termscheck(form);
		}
	}
	else
	{
			swal({
			  title: 'Are you sure?',
			  text: "You are logging into another system.would you like to continue.",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, continue!',
			  cancelButtonText: 'No, cancel!',
			  confirmButtonClass: 'btn btn-success',
			  cancelButtonClass: 'btn btn-danger',
			  buttonsStyling: false
			}).then(function () {
					if(($('#termscondition').is(':checked')) )
					{
						userlogin(form);
					}
					else
					{
						termscheck(form);
					}
			  
			}, function (dismiss) {
			  if (dismiss === 'cancel') {
				swal(
				  'Cancelled',
				  'You are continuing with your previous login :)',
				  'error'
				);
				$(".loader").hide();
			  }
			});
		
	}
}
});
});	

function userlogin(form)
{
var country = "";
var city = "";
var region = "";
var isp = "";
/*
$.getJSON("http://ip-api.com/json/?callback", function(data) {
$.each(data, function(k, v) {
	
		 if(k=='country') { country = v; }
		 else if(k=='city') { city = v; }
		 else if(k=='regionName') { region = v;  }
		 else if(k=='isp') { isp = v; }
		 else if(k=='query') {}
		 });
		
	
		}); 
		*/
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/asapuserlogin') ?>",
				data:form.serialize()+ "&txcountry=" + country+ "&txcity=" + city+ "&txregion=" + region+ "&txisp=" + isp,
				success:function(result)
				{
					 
					if(result=='ASAP')
					{
						 
							location.href= "<?php echo base_url();?>index.php/mypuzzleset1/dashboard#View";  
						  
						  
							$(".loader").hide();
					}
					else
					{						
						$("#errormsg").html('Invalid Credentials');$("#errormsg").show();
						$(".loader").hide();
					}
					
				}
		}); 
	
}
function termscheck(form)
{
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/termscheck') ?>",
				data:form.serialize(),
				success:function(result)
				{
				//alert(result);
					if(result==0  && $.trim(result)!='')
					{
						$("#termschkbox").show();
						//$("#terrormsg").html('Please check terms and conditions');
						$(".loader").hide();
					}

					else
					{
						userlogin(form);
					}

				}
		});
}
</script>
<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
</body>
</html>