
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-media.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" media="screen">

 <div class="clear_both"></div>
 <div class="section_four">
 <div class="container">
 <h2>Personalized Skill Kit </h2>
 <div class="panel panel-default">

<div class="panel-body">

<div class="tab-content responsive">
<div class="tab-pane active" id="BrainSkillsOn">
<div class="tab_sec_c1">
 <h3> </h3>
<div class="row">
<?php
 $myskillpie=array("59"=>"#cdcdcd","60"=>"#cdcdcd","61"=>"#cdcdcd","62"=>"#cdcdcd","63"=>"#cdcdcd");
 //$myskillpie=array("59"=>"#ff6600","60"=>"#067d00","61"=>"#ffbe00","62"=>"#be0000","63"=>"#1e9ddf");
 $myskillpie_orginal=array("59"=>"#ff6600","60"=>"#067d00","61"=>"#ffbe00","62"=>"#be0000","63"=>"#1e9ddf");
?>
<?php if($actualGames!=''){ ?>
<?php foreach($actualGames as $games)
{
	?>
	<div class="col-md-3 col-sm-3 col-xs-12 mB50">

<div class="box_c1">
<h3><?php echo $actualGameCategory[$games['skill_id']]; ?></h3>
 
<?php if($games['tot_game_played']<5){ ?>	
<a class="fancybox fancybox.iframe" id="<?php echo $games['gid']; ?>" href="<?php echo base_url()."assets/swf/".$this->session->userlang."/".$games['game_html'].".html"; ?>"><img src="<?php echo base_url(); ?>assets/<?php echo $games['img_path']; ?>"/></a>
<?php  } else {?>        
<a  href="javascript:;"><img src="<?php echo base_url(); ?>assets/<?php echo $games['img_path']; ?>"/></a>  
<?php } ?>

<p><?php echo $games['gname']; ?></p>
<p>
 

	

</p>
<?php if($games['tot_game_played']<5){ ?>
<a class="btn btn-default in_btn fancybox fancybox.iframe" id="<?php echo $games['gid']; ?>"  href="<?php echo base_url()."assets/swf/".$this->session->userlang."/".$games['game_html'].".html"; ?>"><?php if($games['tot_game_played']==0){ echo "Play"; } else{echo "Re-play";}?></a>         
<?php  } else {?>        
<a class="btn btn-default in_btn gameBtnInactive" href="javascript:;">Limit Expired</a>  
<?php } ?>
</div>
</div>
	
<?php 
}
}
else{?>
	
	<div class="alert alert-warning fade in"><strong>No Personalized Skill Kit</strong></div>
	<?php
}
?>




</div><!--/row -->

</div><!--/tab_sec_c1 -->

</div><!--/tab-pane -->



</div><!-- tab-content -->
</div><!-- panel-body -->
</div><!-- panel-default -->
 </div><!-- container -->
 </div><!-- section_four -->
 
 
 <div class="clear_both"></div>	
<script type="text/javascript">
$(document).ready(function(){
	$("a.fancybox").each(function() {
		var tthis = this;
  $(this).fancybox({
			'transitionIn'    :    'elastic',
'transitionOut'    :    'elastic',
'speedIn'     :    600,
'speedOut'     :    200,
'overlayShow'    :    false,
'width'  : 750,           // set the width
    'height' : 500,           // set the height
    'type'   : 'iframe',       // tell the script to create an iframe
    'scrolling'   : 'no',
	 'afterClose': function () {  
        parent.location.reload(true);
    },
	beforeShow : function(){
		 
 $(".fancybox-inner").addClass("fancyGameClass");
 $.ajax({
    type: "POST",
    url: "<?php echo base_url()."index.php/home/gamesajax"; ?>",
    data: {gameid:tthis.id,skillkit:'Y'},
     
    success: function(result){
		 
    }
});
  
}

});
});
 
}); 
</script>
<style>
 body{min-height:0 !important;}
 </style>