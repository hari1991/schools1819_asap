 <header class="head">
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            Upload Users
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
	 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>User list upload</h5>
            </header>
			<?php	if(!isset($_POST['sbmtbtn'])) 
{   ?>
			<form name="frmUploadusers" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/admin/userupload_list" id="frmUploadusers" method="post">
			<div class="row">
			<div class="col-sm-3">
			<label>School</label>
			<select  class="form-control input-sm" name="schoolid" id="schoolid">
			<option value="">Select</option>
			<?php foreach($schools as $sc) { ?>
			<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
			<?php } ?>
			</select>
			</div>
			
			<div class="col-sm-2">
			<label>Grade</label>
			<select  class="form-control input-sm" name="ddlGradeType" id="ddlGradeType" >
			<option value="">Select</option>
			<?php foreach($grades as $gd) { ?>
			<option value="<?php echo $gd['id']; ?>"><?php echo $gd['classname']; ?></option>
			<?php } ?>
			</select>
			
			</div>
			
			<div class="col-sm-2">
			<input type="file" name="txtUpload" id="txtUpload" class="form-control col-lg-6" />
			
			</div>
			
			<div class="col-sm-2">
			<a  href="<?php echo base_url(); ?>userlist.csv"  class="btn btn-info">Sample data</a></div>
			
			<div class="col-sm-2">
			<input type="submit" class="btn btn-success" name="sbmtbtn" id="sbmtbtn" style="dispaly:none;" Value="Upload"></div>
			
			
			</div>
			</form>
<?php } ?>
		<?php	if(isset($_POST['sbmtbtn'])) 
{   ?>
            <div class="col-lg-12">
        <div class="alert alert-warning" style="text-align:center" role="alert">
<a  href="<?php echo base_url(); ?>index.php/admin/userupload"  style="float:left; margin-top:-6px; "  class="btn btn-info">Back to user upload</a>
		<strong>User Data Uploaded Successfully</strong> <a  href="<?php echo base_url(); ?><?php echo $filename; ?>"  style="float:right; margin-top:-6px; "  class="btn btn-info">Click here to download</a></div>
</div> <?php } ?>
        </div>
    </div>
</div>
</div>
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
<script>
$(document).ready(function(){
	
	
	$("#frmUploadusers").validate({
		rules: {
			schoolid: {required: true},
            ddlGradeType: {required: true}
 
		   },
		   messages: {
			schoolid: {required: "Please choose school"},
			ddlGradeType: {required: "Please choose gradeid"}
		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},

	});
	
});

$('#ddlGradeType').change(function(){
	if($("#frmUploadusers").valid()==true)
	{

	}
});

$('#schoolid').change(function(){
	if($("#frmUploadusers").valid()==true)
	{

	}
});


</script>
 