<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet" type="text/css">
<!--<link href="<?php echo base_url(); ?>assets/css/bootstrap.min_index.css" rel="stylesheet">-->
<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" rel="stylesheet">

<link href="<?php echo base_url(); ?>assets/css/banner/parallax_mouseinteraction.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/testimonial/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/testimonial/owl.theme.css">
<div class="headerTop">
<div class="carousel-inner">
                <div class="item active" >
                   <img src="<?php echo base_url(); ?>assets/images/banner.jpg" width="100%" alt="Banner">
                </div>
            <!--/.item-->
              <!--/.item-->
            </div>
</div>



<div class="skillPuzzle">
	<div class="container">
		<h1><?php echo $this->lang->line("centertext"); ?></h1>
		<div class="row">
			<div class="col-md-4 col-sm-12">
<h2>Puzzles Attempted</h2>
<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s"><div class="odometer"  id="odo1" ></div></div>
</div>
<div class="col-md-4 col-sm-12">
<h2>Puzzles Solved</h2>
<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" ><div class="odometer" id="odo2" ></div></div>
</div>
<div class="col-md-4 col-sm-12">
<h2>Minutes Trained</h2>
<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" ><div class="odometer" id="odo3" ></div></div>
</div>
		</div>
	</div>
</div>

<style>
	
	.textsize16{font-size:16px; color:black; }
	.student-participate-block div.media{
	text-align: center;
    border: 2px solid #fff;
    padding: 20px;
	min-height: 263px;
	}
	.service-block div.media{
		text-align: center;
	}
	.service-block div.media-body{
		text-align: center;margin-top: 20PX;
	}
	p.service-block{
		padding-top: 20px;
	}
	.student-participate-block div.media-body{
		    clear: both;    margin-top: 20PX;
	}
	.taJustify{text-align:justify;}

	
	@media only screen and (max-width : 768px){
    .DetailSection h2{font-size: 20px;}
    .DetailSection h3{font-size: 14px;}
}

	</style>

<section id="services"  class="service-block white">
<center>
<font color="#ff6600">
<h3>Aptitude & Skill Assessment Program (ASAP)</h3>
<br/></font></center><font color="black">
        <div class="container">

            <div class="row">
			     <div class="col-md-1 col-sm-0 hdndiv">
			</div>
                <div class="col-md-2 col-sm-6 minheg">
                    <div class="media">
                        <div class=""><img src="<?php echo base_url(); ?>assets/images/icon_what_we_do.png" width="100"></div>
                         <div class="media-body">
                            <h4 class="media-heading" style="color: #ff9933;">What is ASAP?</h4>
                            <p class="textsize16">An Unique Aptitude & Skill Assessment program </p>
						</div>
					</div><!--/.col-md-4-->
				</div>
                <div class="col-md-2 col-sm-6 minheg">
                   <div class="media">
                       <div class=""><img src="<?php echo base_url(); ?>assets/images/Whom.png" width="100"></div>
                        <div class="media-body">
							
                            <h4 class="media-heading" style="color: #00ccff;">For Whom?</h4>
                            <p class="textsize16">Uniquely Designed for every Grade from Prekg - 12</p>
						
						</div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-2 col-sm-6 minheg">
                    <div class="media">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/icon-how-we-do-it.png" width="100">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading" style="color: #99cc00;">Why?</h4>
                            <p class="textsize16">To know student’s performance in core 5 skills</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
				<div class="col-md-2 col-sm-6 minheg">
                    <div class="media">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/schools.png" width="100">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading" style="color: #0dbbbb;">Where?</h4>
                            <p class="textsize16">At your School</p>
                        </div>
                    </div>
                </div>
				<div class="col-md-2 col-sm-6 minheg">
                    <div class="media">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/How.png" width="100">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading" style="color: #fd5252;">How?</h4>
                            <p class="textsize16">Online Through SkillAngels ASAP Platform</p>
                        </div>
                    </div>
                </div>
				
				<div class="col-md-1 col-sm-0 hdndiv">
			</div>
            </div>
        </div>
</font>


    </section>
	
	<section id="services" class="white student-participate-block">
<center>
<font color="white">
<h3>Why Should A Student Participate in ASAP?</h3>
<br/></font></center><font color="white">
        <div class="container">

            <div class="row">
			<div class="col-md-1 col-sm-0 hdndiv">
			</div>
                <div class="col-md-2 col-sm-6 bounceIn animated">
                    <div class="media bounce">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/Challenge.png" width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="student-participate-block-content textsize16">Opportunity to Challenge oneself and excel </p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-2 col-sm-6 bounceIn animated">
                    <div class="media bounce">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/Identify.png" width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="student-participate-block-content textsize16">Identify strengths and areas of improvement</p>
            </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-2 col-sm-6 bounceIn animated">
                    <div class="media bounce">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/Reports.png" width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="student-participate-block-content textsize16">Aptitude Ranking with performance report</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
				<div class="col-md-2 col-sm-6 bounceIn animated">
                    <div class="media bounce">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/rewards.png"  width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="student-participate-block-content textsize16">Exciting Rewards</p>
                        </div>
                    </div>
                </div>
				<div class="col-md-2 col-sm-6 bounceIn animated">
                    <div class="media bounce">
                        <div class="">
                            <img src="<?php echo base_url(); ?>assets/images/Certificate.png" width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="student-participate-block-content textsize16">Participation Certificates</p>
                        </div>
                    </div>
                </div>
				<div class="col-md-1 col-sm-0 bounceIn animated hdndiv"></div>
            </div>
        </div>
		
</font>


    </section>

	<section id="services" class="white encourage">
<center>
<font color="black">
<h3>Why Should A School Encourage ASAP?</h3>
<br/></font></center><font color="black">
        <div class="container">

            <div class="row">
                <div class="col-md-4 col-sm-12 tagline1">
                    <div class="media">
                        <div class="pull-left">
                            <img src="<?php echo base_url(); ?>assets/images/21st.png" width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="textsize16 taJustify">Be a pioneer in creating an awareness on 21st century skills including critical thinking, problem solving, decision making and many more </p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-12 tagline2">
                    <div class="media">
                        <div class="pull-left">
                            <img src="<?php echo base_url(); ?>assets/images/Identifysw.png" width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="textsize16 taJustify">Identify the strengths and areas of improvement in each student And assist in personalized learning</p>
            </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-12 tagline3">
                    <div class="media">
                        <div class="pull-left">
                            <img src="<?php echo base_url(); ?>assets/images/improve.png"  width="100" class="circle">
                        </div>
                        <div class="media-body">
                            <p class="textsize16 taJustify">Potential for improving the Cognitive Foundation, enabling students to better grasp input from institution & surrounding</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->

            </div>
        </div>
</font>
    </section>


<section id="" class="white awardsrecognition">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
				
                    <div class="center">
                        <h3>Awards &amp; Recognition</h3>
                        </div>
                    <div class="gap"></div>
								<center><img src="<?php echo base_url(); ?>assets/images/awardslogo.png" width="100%"></center>
                    <div class="gap"></div>

                    <div class="row awards">
                        <div class="col-md-6">
                            <blockquote>
                                <p>Featured in Startup India – with Prime Minister Shri. Narendra Modi</p>                                
                            </blockquote>
                            <blockquote>
                                <p>Startupp Challenge Winners</p>
                            </blockquote>
                            <blockquote>
                                <p>Received MSME Grant</p>
                            </blockquote> 
							<blockquote>
                                <p>Winners of Hot 100 Technology Awards</p>
                            </blockquote>
							<blockquote>
                                <p>NSDC Innovations for Skills Challenge –Finalist</p>
                            </blockquote>
							<blockquote>
                                <p>Assist World Record Largest Computer Based Skill Competition</p>
                            </blockquote>
                        </div>
                        <div class="col-md-6">
                            <blockquote>
                                <p>SILVER Winners in International Innovators Fair</p>
                            </blockquote>
                            <blockquote>
                                <p>Recognized ”Startup” by DIPP - Govt. of India</p>
                            </blockquote>
							<blockquote>
                                <p>Best Startup Award (TIDE Scheme) -Ministry of Communications & IT, Govt of India </p>
                            </blockquote>
							<blockquote>
                                <p>CII Startupreneurs - Best Startup, People’s Choice Award</p>
                            </blockquote>
							<blockquote>
                                <p>Qualcomm Qprize Finalist</p>
                            </blockquote>
							<blockquote>
                                <p>Winners of Tech4Impact Accelerator Program</p>
                            </blockquote>
							 
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

<section id="" class="white">
<hr/>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="center">
                        <h3>Featured In ...</h3>
                        </div>
                    <div class="gap"></div>
                    
                </div>
            </div>
<section class="regular slider">
    <div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/hindu.png">
    </div>
    <div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/et.png">
    </div>
    <div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/village.png">
    </div>
    <div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/business.png">
    </div>
    <div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/epaper.png">
    </div>
    <div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/live.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/telegraph.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/tech.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/wire.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/vccircle.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/quallcomm.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/smallbiz.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/yourstory.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/inc.png">
    </div>
	<div>
      <img src="<?php echo base_url(); ?>assets/images/featuredin/vikatan.png">
    </div>
</section> 
</div>
  
   </section>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slick-theme.css">
<style type="text/css">
    html, body {
      margin: 0;
      padding: 0;
    }

    * {
      box-sizing: border-box;
    }

    .slider {
        width: 90%;
        margin: auto auto 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
        color: black !important;
    }
	
.slick-slide img {
	

    padding: 10px 0;
	margin:10px;
    background: #fff;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, .3);
    -moz-box-shadow: 0 0 5px rgba(0, 0, 0, .2);
    box-shadow: 0 0 5px rgba(0, 0, 0, .3);
    text-align: center;
	
}
  </style>
    

			<!--<center><img src="images/featured.png" width="100%"></center>-->
       

   
<script src="<?php echo base_url(); ?>assets/js/counter.js" type="text/javascript" charset="utf-8"></script>
<script>
window.odometerOptions = {
  format: '(ddd).dd'
};
$(document).ready(function(){	  

$('.regular').slick({
  dots: false,
  infinite: true,
   autoplay: true,
   autoplaySpeed: 5000,
  speed: 3000,
  slidesToShow: 5,
  slidesToScroll: 2,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true
		}
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
		}
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
		}
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
	 
doAjax();	
});	
function doAjax()
{   
setTimeout(function(){
    odo1.innerHTML = 891565;
    odo2.innerHTML = 2791766;
}, 1000);		  		
}
setInterval(function(){doAjax();}, 10000);  // 1000 = 1 second, 3000 = 3 seconds	
$(".txthEmail").blur(function(){
$.ajax({
		type		: "POST",
		cache	: false,
		url		: '<?php echo base_url('index.php/home/language') ?>',
		data		: {Lemail:$(".txthEmail").val()},
		success: function(result) {
		//	alert(result);
			 $(".ddlHLanguage").html(result);
		}
});			
});
	
if (localStorage.chkbx && localStorage.chkbx != '') {
	$('#remember').attr('checked', 'checked');
	$('#email').val(localStorage.usrname);
	$('#pwd').val(localStorage.pass);
} else {
	$('#remember').removeAttr('checked');
	$('#email').val('');
	$('#pwd').val('');
}

 $('#remember').click(function() {

	if ($('#remember').is(':checked')) {
		// save username and password
		localStorage.usrname = $('#email').val();
		localStorage.pass = $('#pwd').val();
		localStorage.chkbx = $('#remember').val();
	} else {
		localStorage.usrname = '';
		localStorage.pass = '';
		localStorage.chkbx = '';
	}
});
</script>
<!-- Full Image Fade OUT --><script src="<?php echo base_url(); ?>assets/js/imagefade/parallax.min.js"></script>
<script>
jQuery(function() {
	jQuery('#parallax_mouseinteraction_thumbs').parallax_mouseinteraction({
		skin: 'bullets',
		width: 1920,
		height: 750,
		width100Proc:true,
		defaultEasing:'easeOutElastic',
		autoPlay:10,
		responsive:true,
		autoHideBottomNav:false,
		showPreviewThumbs:true,
		autoHideNavArrows:true,
		myloaderTime:1,
		scrollSlideDuration:1.8,
		scrollSlideEasing:'easeInQuint',
		maxInteractionYmove:37,
		thumbsOnMarginTop:12,				
		thumbsWrapperMarginBottom:-35	
	});			
});
</script>


<!-- Media Slider ---->
<link href="<?php echo base_url(); ?>assets/css/media/media.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/media/jquery.flexisel.js"></script>
<script type="text/javascript">
$("#flexiselDemo3").flexisel({
        visibleItems: 3,
        itemsToScroll: 1,         
        autoPlay: {
            enable: true,
            interval: 5000,
            pauseOnHover: true
        },
		  responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1,
                itemsToScroll: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2,
                itemsToScroll: 1
            },
            tablet: { 
                changePoint:768,
                visibleItems: 2,
                itemsToScroll: 1
            },
			landscape: { 
                changePoint:1024,
                visibleItems: 3,
                itemsToScroll: 1
            }
        },       
});
</script>
<!-- Banner JS Files Start--->
<script src="<?php echo base_url();?>assets/js/banner/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/banner/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/banner/jQueryRotateCompressed.2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/banner/parallax_mouseinteraction.js" type="text/javascript"></script>
<!-- Banner JS Files END--->
<!-- Testimonial  Slider Plugin -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/testimonial/owl.carousel.min.js"></script>
<script>
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        slideSpeed:1000,
        singleItem:true,
        autoPlay:true
    });
});
</script>

<style>
.tagline1{
background-color: rgba(146,39,143, .9);
color: #fff;
 padding: 10px; 
/*width: 32.333333%;
margin: 0px 5px;*/
}
.tagline2{
background-color: rgba(241,109,124, .9);
color: #fff;
padding: 16px;
/* width: 32.333333%; 
margin: 0px 5px;*/
}
.tagline3{
background-color: rgba(141,198,63, .9);
color: #fff;
padding: 16px;
/* width: 32.333333%; 
margin: 0px 5px;*/
}
.student-participate-block
{
background:url('<?php echo base_url(); ?>assets/images/Testimonial2.jpg');
background-position:center center;
background-size:cover;	
}
.encourage{border-bottom:1px solid #ccc;}
.encourage h3{color: #ff6600;}
.awards{background-color: #fafafa;}
blockquote{border-left: 5px solid rgba(146,39,143, .9);}
#edsix{background-color: #fafafa;}
#edsix h2,#edsix h4{color:#000;}

.awardsrecognition h3{color: #ff6600;}

.odometer-formatting-mark{display:none}
#footer {
    background-image: url('<?php echo base_url(); ?>assets/images/footer2.png');
    background-color: #92278f;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center bottom;
    padding: 40px 0 160px;
    color: #fff;
    font-size: 16px;
}
.skillPuzzle{background-color: #f4f4f4;}
.skillPuzzle h1{color: #ff6600;}
#header{background-color:#77bdff}
.btn-success {
    color: #fff;
    background-color: #f92c8b;
    border-color: #fff;
	padding: 6px 25px;
}
.btn-success:hover{color: #fff !important;background-color: #f92c8b !important;border-color: #fff !important;}

</style>
