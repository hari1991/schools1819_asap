<footer>
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6">

  <ul>
<li>EdSix Brain Lab Pvt Ltd<sup>TM</sup></li>
<li># 1 H, Module no. 8,</li>
<li>IIT Madras Research Park First Floor,</li>
<li>Kanagam Road ,Taramani Chennai - 600113</li>
  </ul>
 
  </div>
<div class="col-md-3 col-sm-6">
<ul>
<li class="callicon"><?php echo $this->lang->line("ftphonenumber"); ?></li>
<li class="msgicon"><a href="mailto:angel@skillangels.com"><?php echo $this->lang->line("ftemail"); ?></a></li>
</ul>
<div class="socialmedia">
<span><?php echo $this->lang->line("ftjoin"); ?></span>
<a href="https://www.facebook.com/skillangels" target="_blank"><img src="<?php echo base_url(); ?>assets/images/fb.png" width="33" height="33"></a> <a href="https://www.linkedin.com/company/edsix-brain-lab-pvt-ltd?trk=company_logo" target="_blank"><img src="<?php echo base_url(); ?>assets/images/icon_LinkedIn.png" width="33" height="33"></a>
</div>

</div>
<div class="col-md-3 col-sm-6">
<ul>
<li><a href=""><?php echo $this->lang->line("fthome"); ?></a></li>
<li><a href="<?php echo base_url(); ?>index.php/home/termsofservice" target="_blank"><?php echo $this->lang->line("ftterms"); ?></a></li>
<li><a href="<?php echo base_url(); ?>index.php/home/privacypolicy" target="_blank" ><?php echo $this->lang->line("ftprivacy"); ?></a></li>
<li><a href="<?php echo base_url(); ?>index.php/home/faq" target="_blank"><?php echo $this->lang->line("ftfaq"); ?></a></li>
</ul>
</div>
<div class="col-md-3 col-sm-6">

  <img src="<?php echo base_url(); ?>assets/images/sklogo-web.png" class="img-responsive" width="193" height="67">
  <br/>
<img src="<?php echo base_url(); ?>assets/images/logo_RTBI.png"  > <img src="<?php echo base_url(); ?>assets/images/logo_CJE.png"  ></div>
</div>
</div>
</footer>
<div class="footerBottom"><p>&copy; 2017 Skillangels. All rights reserved</p></div>

  <script src="<?php echo base_url(); ?>assets/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('ready', function() {
      $(".regular").slick({
        dots: false,
		arrows: true,
		infinite: true,
		 autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1
      });
	  
$('.medialogo').slick({
  dots: true,
  arrows: false,
  autoplay: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
    });
  </script>
<script type="text/javascript">
  $(document).ready(function(e) { 
   $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 6000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
});
</script>
<script>

setInterval(LoginAjaxCall, 1000*60*2); //300000 MS == 5 minutes
LoginAjaxCall();
function LoginAjaxCall(){ //alert("KKKKKKKKKK");
   $.ajax({
			type:"POST",
			url:"<?php echo base_url('index.php/home/checkuserisactive') ?>",
			success:function(result)
			{	//alert(result);
				if(result==1)
				{ 
					
					window.location.href= "<?php echo base_url();?>index.php";
				}		
			}
		});
}
</script>

	 <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/commonscript.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
	 
</body>
</html>