(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.info = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.helpTxt = new cjs.Text("", "bold 30px 'Veggieburger'", "#673401");
	this.helpTxt.name = "helpTxt";
	this.helpTxt.textAlign = "center";
	this.helpTxt.lineHeight = 31;
	this.helpTxt.lineWidth = 540;
	this.helpTxt.parent = this;
	this.helpTxt.setTransform(2,-55);

	this.timeline.addTween(cjs.Tween.get(this.helpTxt).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FCF1DC","#EED8AF"],[0,1],6,-125,0,6,-125,330.1).s().p("EgsRAZCQhGAAAAhGMAAAgv3QAAhGBGAAMBYjAAAQBGAAAABGMAAAAv3QAABGhGAAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B87A2D").s().p("EgsRAZgQhkAAAAhkMAAAgv3QAAhkBkAAMBYjAAAQBkAAAABkMAAAAv3QAABkhkAAg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#E5BA69","#D7A84F"],[0,1],0,0,0,0,0,344.1).s().p("EgsRAaIQiMAAAAiMMAAAgv3QAAiMCMAAMBYjAAAQCMAAAACMMAAAAv3QAACMiMAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.info, new cjs.Rectangle(-297.4,-167.2,594.8,334.5), null);


(lib.hitMc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,255,255,0.749)").s().p("Ehj/A4QMAAAhwfMDH/AAAMAAABwfg");
	this.shape.setTransform(240,60);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hitMc, new cjs.Rectangle(-400,-300,1280,720), null);


(lib.closebtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AA/BxIg6hNIAAghIAVAAIBDBYIAAAWgAhcBxIAAgWIBDhYIAWAAIAAAhIg8BNgAAFgCIAAgfIA8hQIAcAAIAAAXIhDBYgAgZgCIhDhZIAAgWIAdAAIA7BQIABAfg");
	this.shape.setTransform(0.3,0.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// Layer 5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["rgba(67,0,0,0.8)","rgba(79,0,0,0)"],[0,0.6],11.8,7.1,0,11,7.1,35.7).s().p("AiCBUQg1g0AAhIQAAgrATgmIAJgNIAAAuQAAA/AuArQAuAsBBAAIABAAQBBAAAtgsQAvgrAAg/IAAgoQAYAoAAAwQAABIg2A0Qg2AzhMABQhLgBg3gzg");
	this.shape_1.setTransform(0,5.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["rgba(255,153,0,0.8)","rgba(255,255,153,0)"],[0,0.6],11.8,7.1,0,11,7.1,35.7).s().p("AiCBUQg1g0AAhIQAAgrATgmIAJgNIAAAuQAAA/AuArQAuAsBBAAIABAAQBBAAAtgsQAvgrAAg/IAAgoQAYAoAAAwQAABIg2A0Qg2AzhMABQhLgBg3gzg");
	this.shape_2.setTransform(0,5.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).wait(1));

	// Layer 2
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#FF0000","#CC0000","#990000"],[0,0.765,1],0,0,0,0,0,19.9).s().p("AiKCLQg5g6AAhRQAAhQA5g6QA6g5BQAAQBRAAA6A5QA5A6AABQQAABRg5A6Qg6A5hRAAQhQAAg6g5g");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#FFFF33","#FFCC00","#FF9900"],[0,0.765,1],0,0,0,0,0,19.9).s().p("AiKCLQg5g6AAhRQAAhQA5g6QA6g5BQAAQBRAAA6A5QA5A6AABQQAABRg5A6Qg6A5hRAAQhQAAg6g5g");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3}]}).to({state:[{t:this.shape_4}]},1).wait(1));

	// Layer 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#FFFFFF","#6E6E6E"],[0,1],-0.3,21,0.3,-20.9).s().p("AiUCUQg9g9AAhXQAAhWA9g9QA+g+BWAAQBXAAA9A+QA+A9AABWQAABXg+A9Qg9A+hXAAQhWAAg+g+g");

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(2));

	// Layer 1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FFFFFF","#6E6E6E"],[0,1],-0.3,-24.5,0.4,24.5).s().p("AitCtQhHhHAAhmQAAhkBHhJQBJhHBkAAQBmAABHBHQBIBJAABkQAABmhIBHQhHBIhmAAQhkAAhJhIg");

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.5,-24.5,49,49);


(lib.HelpScreen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.closeMc = new lib.closebtn();
	this.closeMc.name = "closeMc";
	this.closeMc.parent = this;
	this.closeMc.setTransform(206,243.5);

	this.contentMc = new lib.info();
	this.contentMc.name = "contentMc";
	this.contentMc.parent = this;
	this.contentMc.setTransform(206,74);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.contentMc},{t:this.closeMc}]}).wait(1));

	// Layer 3
	this.hitMc = new lib.hitMc();
	this.hitMc.name = "hitMc";
	this.hitMc.parent = this;
	this.hitMc.setTransform(-34,14);

	this.timeline.addTween(cjs.Tween.get(this.hitMc).wait(1));

}).prototype = getMCSymbolPrototype(lib.HelpScreen, new cjs.Rectangle(-434,-286,1280,720), null);


// stage content:
(lib.helpPanel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.helpMc = new lib.HelpScreen();
	this.helpMc.name = "helpMc";
	this.helpMc.parent = this;
	this.helpMc.setTransform(434.1,285.4);

	this.timeline.addTween(cjs.Tween.get(this.helpMc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(640.1,359.4,1280,720);
// library properties:
lib.properties = {
	id: 'CCEB8AB16488514283EDB755E5DFA164',
	width: 1280,
	height: 720,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['CCEB8AB16488514283EDB755E5DFA164'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;