<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appapi extends CI_Controller {
var $info;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		
		// Call the CI_Model constructor
		parent::__construct();
		//$this->lang->load("menu_lang","english");
		$this->load->model('Appapi_model');
		$this->load->library('session');	
	//	$this->load->library('My_PHPMailer');
		$this->info=array("secret_key"=>"Ed6S@2018","default_pwd"=>"skillangels","default_play_time"=>1); 
		//$this->lang->load("menu_lang","french");		
		
    }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}
	
	public function checkapplogin()
	{
		$username = $this->input->post('Username');
		$password = $this->input->post('Password');
		$deviceid = $this->input->post('DeviceId');
		$Key = $this->input->post('Key');
		$timestamp = $this->input->post('ts');  
		
		 /* $username = 'demoapp2';
		$password = 'skillangels';
		$deviceid = 'ABCD12345ABCD';
		$Key = 123456;
		$timestamp = 123456; */ 
		
		if(($username!='') && ($password!='') && ($deviceid!='') && ($Key!='') && ($timestamp!=''))
		{			 
			$token=$this->encMethod($timestamp,$deviceid,$this->info['secret_key']); 
			 //echo $token['Hash']; exit;
			if($Key==$token['Hash'])
			//if(1==1)
			{
			
				
				$data['query'] = $this->Appapi_model->checkUser($username,$password);
				
				if(isset($data['query'][0]->id))
				{
					
						$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);
						
						$this->Appapi_model->update_loginDetails($data['query'][0]->id,$uniqueId);
						
						
						if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
							 $ip=$_SERVER['HTTP_CLIENT_IP'];}
							 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
							 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
							 $ip=$_SERVER['REMOTE_ADDR'];}
						 
						$this->Appapi_model->insert_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
						
						$data['query1'] = $this->Appapi_model->updateDeviceID($username,$deviceid);
						
						$resparr=array("status"=>"success","Schoolid"=>$data['query'][0]->sid,"Gradeid"=>$data['query'][0]->grade_id,"Planid"=>$data['query'][0]->gp_id,"msg"=>"User Login successfully");
						echo json_encode($resparr);exit;
				
				}
				else 
				{
					$resparr=array("status"=>"Failed","Schoolid"=>'-',"Gradeid"=>"-","Planid"=>'-',"msg"=>"Username or Password Wrong");
					echo json_encode($resparr);
				}
			}
			else
			{
				$resparr=array("status"=>"Failed","Schoolid"=>'-',"Gradeid"=>"-","Planid"=>'-',"msg"=>"Key Mismatch");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"Failed","Schoolid"=>'-',"Gradeid"=>"-","Planid"=>'-',"msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}
	
	
	public function gamedetails()
	{
	    $username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');  

	     /* $username = 'demoapp2';
		$Key = 123456;
		$timestamp = 123456;  */ 


		
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Appapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				//if(1==1)
				{	
					$check_date_time = date('Y-m-d');
					$catid=1;
					$userid=$data['query'][0]->id;
					$planid=$data['query'][0]->gp_id;
					$gradeid=$data['query'][0]->grade_id;
					$schoolid=$data['query'][0]->sid;
					$section=$data['query'][0]->section;
					
					$data['randomGames']=$this->Appapi_model->getRandomGames($planid,$gradeid,$schoolid);
					
					$data['assignGames']=$this->Appapi_model->getAssignGames($planid,$gradeid,$userid,$catid);
					
					$cur_day_skills = count($data['randomGames']);
					$assign_count = count($data['assignGames']);
									
					/* if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) 
					{						
						$this->fn_Rand_games($userid,$check_date_time,$cur_day_skills,$assign_count,$catid,$planid,$gradeid,$schoolid,$section);
					} */
					//$data['randomGames']=$this->Arapi_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					$where = "";
					foreach($data['randomGames'] as $brainData )
					{
						$brainIds[] = $brainData['gid'];
						$active_game_ids = @implode(',',$brainIds);
						$where = " and g.gid in ($active_game_ids)";
					}
						 
					$data['actualGames'] = $this->Appapi_model->getActualGames($planid,$gradeid,$userid,$catid,$where,$schoolid);
					
					//echo '<pre>'; print_r($data['actualGames']);
					
					$resparr=array("status"=>"success","gamedetails"=>$data['actualGames'],"total_play_times"=>$this->info['default_play_time'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","gamedetails"=>"-","total_play_times"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","gamedetails"=>"-","total_play_times"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"Failed","gamedetails"=>"-","total_play_times"=>"-","msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
		
	}
	
	public function result()
	{
		 $username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		
		$gamename=$this->input->post('Gname'); 
		
		if($gamename=='ArithmeticChallenge'){
			
			$gamename='Equate-Level2';
		}
		
		/* $username = 'demoapp2';
		$Key = 123456;
		$timestamp = 123456; 		
		$gamename='WhatsInStore-Level1'; */		
		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($gamename!=''))
		{
			$data['query'] = $this->Appapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				//if(1==1)
				{
					$data['gameid'] = $this->Appapi_model->getgameid($gamename);
					$gameid = $data['gameid'][0]['gid'];
					if($gameid!='')
					{
						$data['checkgame'] = $this->Appapi_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);

						$game_limit=$this->info['default_play_time'];
						
						if($game_limit>$data['checkgame'][0]['played_time'])
						{ 
							$postdata = $_POST;
							$total_ques=$postdata["total_ques"];
							if($postdata["attempt_ques"]>10){
								$attempt_ques=10;
							}
							else{
								$attempt_ques=$postdata["attempt_ques"];
							}
							$answer=$postdata["answer"];
							$score=$postdata["gamescore"];
							$a6=$postdata["gtime1"];
							$a7=$postdata["rtime1"];
							$a8=$postdata["crtime1"];
							$a9=$postdata["wrtime1"];
							/* $answer=10;
							$score=100;
							$a6=20;
							$a7=20;
							$a8=20;
							$a9=20;	 */
							
							$skillkit=0;
							$userlang = 1;
							$userid = $data['query'][0]->id; 
							$lastup_date = date("Y-m-d");
							$cid = 1;
							$data['gameDetails'] = $this->Appapi_model->getresultGameDetails($userid,$gameid);
							$skillid =$data['gameDetails'][0]['gameskillid'] ; 
							$schedule_val = 0;
							$pid =  $data['query'][0]->gp_id; 
							
							$data['getcurdayskillid'] = $this->Appapi_model->getcurdayskillid($userid,$skillid);
							$skillidcount = $data['getcurdayskillid'][0]['skillcount'];
							
							if(($skillid!=0) && ($skillidcount==0)) {
							if($skillkit==0){
								
							$data['insert1'] = $this->Appapi_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
							
							}
							}
							
							$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);

							/*--- Sparkies ----*/
							//$sparkies_output=$this->Arapi_model->insertsparkies($arrofinput);
							//echo $sparkies_output[0]['OUTPOINTS'];exit; 
							
							$resparr=array("status"=>"success","response"=>1,"msg"=>"-");
							echo json_encode($resparr);
						}
						else
						{
							$resparr=array("status"=>"Failed","response"=>"-1","msg"=>"Game limit expired");
							echo json_encode($resparr);
						}
					}
					else
					{
						$resparr=array("status"=>"Failed","response"=>"-2","msg"=>"Game not avialable");
						echo json_encode($resparr);
					}
				}
				else 
				{
					$resparr=array("status"=>"Failed","response"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{			
				$resparr=array("status"=>"Failed","response"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr= array("status"=>"Failed","response"=>"-","msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
	}
	
	
	public function getreport()
	{
		 $username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 

		/* $username = 'demoapp2';
		$Key = 123456;
		$timestamp = 123456; */


		
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Appapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				//if(1==1)
				{	
					
					$userid=$data['query'][0]->id;
					
					$data['skillscore']=$this->Appapi_model->getSkillscores($userid);
																						
					//echo '<pre>'; print_r($data['actualGames']);
					$bspi = ($data['skillscore'][0]['memory']+$data['skillscore'][0]['visual']+$data['skillscore'][0]['focus']+$data['skillscore'][0]['problem']+$data['skillscore'][0]['ling'])/5;
					
					$resparr=array("status"=>"success","scores"=>$data['skillscore'],"bspi"=>$bspi,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","scores"=>"-","bspi"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","scores"=>"-","bspi"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr= array("status"=>"Failed","response"=>"-","msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
	}
	
	
	public function logout()
	{
		$username = $this->input->post('Username');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Appapi_model->checkuserexist($username);
			
			if(isset($data['query'][0]->id))
			{
			
					$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
					//if($Key==$token['Hash'])
					if(1==1)
					{	
						$this->Appapi_model->updateDeviceID($username,'','');
						$this->Appapi_model->update_logout_log($data['query'][0]->id,$data['query'][0]->session_id);
						$this->Appapi_model->updateuserloginstatus($data['query'][0]->id,$data['query'][0]->session_id);
						$resparr=array("status"=>"success","msg"=>"Device Id cleared successfully");
						echo json_encode($resparr);
					}
					else 
					{
						$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
						echo json_encode($resparr);
					}
				
					
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr= array("status"=>"Failed","msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
	}
	
	public function encMethod($salt1,$password,$salt2)
	{
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	}
	
	
	
	
	
	
}