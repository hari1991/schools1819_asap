<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styleinner.css">
 <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">


     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="" style=" ">
 <div class="">


<div class="">

 <?php //print_r($skillwiseaverage); 

//echo $total= $var1 + $var2;

 $totalsets;

//echo $skillwiseaverage[0]['skillscorem'] + $skillwiseaverage[0]['skillscore2m'] + $skillwiseaverage[0]['skillscore3m'] 

//echo $set1avg_M = ($skillwiseaverage[0]['skillscorem'] + $skillwiseaverage[0]['skillscore2m'] + $skillwiseaverage[0]['skillscore3m']) / $totalsets;
//echo '<br/>';
//echo $set1avg_V = ($skillwiseaverage[0]['skillscorev'] + $skillwiseaverage[0]['skillscore2v'] + $skillwiseaverage[0]['skillscore3v']) / $totalsets;

//echo round($set1avg_M, 2); echo '<br/>';
//echo round($set1avg_V, 2); echo '<br/>';
//echo round($set1avg_F, 2); echo '<br/>';
//echo round($set1avg_P, 2);  echo '<br/>';
//echo round($set1avg_L, 2);  echo '<br/>';

$memory = round($set1avg_M, 2);
$visual = round($set1avg_V, 2);
$focus = round($set1avg_F, 2);
$problem = round($set1avg_P, 2);
$linguistics = round($set1avg_L, 2);


$test = array("Memory"=>$memory, "Visual Processing"=>$visual, "Focus and Attention"=>$focus, "Problem Solving"=>$problem, "Linguistics"=>$linguistics);
arsort($test);
//print_r($test);


?>
 
  <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
 
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>



<script>
$(document).ready(function()
{
	bspichart();
});
 function bspichart()
 {
	 
	 
	
	 var chart = Highcharts.chart('container1', {

    chart: {
        type: 'bar',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: true},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: [<?php foreach($test as $key=>$value){ 
	 echo "'".$key."',";
		}
	?> ],
		gridLineWidth: 0,
  minorGridLineWidth: 0,
  labels: {
            style: {
                fontSize: '25px',
				color: '#FF6600',
				fontFamily: 'Phenomena-Regular'
            }
        }
       
    },
  
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Score',style: {fontSize: '25px',color: '#000',fontFamily: 'Phenomena-Regular'}
        },
		 max: 100 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0,
  labels: {
            style: {
                fontSize: '20px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        }
		
    },
	
 plotOptions: {
	  
    bar: {
        dataLabels: {
            enabled: true,
			formatter: function() {
 
    var Greeting0='Needs Attention !!!';
    var Greeting1='Needs Improvement !!!';
    var Greeting2='Can Do Better !!!';
    var Greeting3='Average !!!';
    var Greeting4='Fair !!!';
    var Greeting5='Good !!!';
    var Greeting6='Very Good !!!';
    var Greeting7='Excellent !!!';
    var Greeting8='Extraordinary !!!';
    var Greeting9='Genius !!!';
    var Greeting10='Prodigy !!!';

    if(this.y<1){ return Greeting0;}
    else if(this.y<11){ return Greeting1;}
    else if(this.y<21){ return Greeting2;}
    else if(this.y<31){ return Greeting3;}
    else if(this.y<41){ return Greeting4;}
    else if(this.y<51){ return Greeting5;}
    else if(this.y<61){ return Greeting6;}
    else if(this.y<71){ return Greeting7;}
    else if(this.y<81){ return Greeting8;}
    else if(this.y<91){ return Greeting9;}
    else if(this.y<101){ return Greeting10;}
   },
                        inside: true,
			style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
             
        }
    }
},	

   
    series: [
	
	{
		showInLegend: false,
        name:"Skill Wise Average",
        data: [<?php foreach($test as $key=>$value){  if($key=='Memory'){$color='#da0404';}
	else if($key=='Visual Processing'){$color='#ffc000';}
	else if($key=='Focus and Attention'){$color='#92d050';}
	else if($key=='Problem Solving'){$color='#ff6600';}
	else if($key=='Linguistics'){$color='#00b0f0';} echo'{y:'.$value.',color:"'.$color.'"},'; }?>]
	}
	 ] 

});
 }
	 
	 
	 
 
</script>
 
  <div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 
 <div><h2 id="MonthID1"><span class="col-lg-10">Skill Wise Report</span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>
 <div id="container1"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" >
</div>
 </div>
  
 </div> 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 body{overflow: hidden;}

 
 </style>