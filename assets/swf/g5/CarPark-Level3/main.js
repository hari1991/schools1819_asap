///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 3, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var buttons, bt1, bt2, bt3, bt4, bg1
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var qno = [];
var chpos = []
var tweenMcArr = []

//////////////////////////////////////////////////
//register key functions
window.onload = function (e) {
    checkBrowserSupport();
}
//////////////////////////////////////////////////
function init() {
    console.log("innt")

    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "CarPark-Level3/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(
            { id: "choice1", src: gameAssetsPath + "CarImages.png" },
            { id: "bg1", src: gameAssetsPath + "Background1.png" },
            { id: "Buttons", src: gameAssetsPath + "question.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
            { id: "question1", src: gameAssetsPath + "red.png" },
            { id: "question2", src: gameAssetsPath + "yellow.png" },
            { id: "question3", src: gameAssetsPath + "blue.png" },
            { id: "question4", src: gameAssetsPath + "green.png" },
            { id: "car", src: gameAssetsPath + "questiontext1.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}
//=================================================================DONE LOADING=================================================================//
function doneLoading1(event) {
    var event = assets[i];
    var id = event.item.id;

    if (id == "car") {
        car1 = new createjs.Bitmap(preload.getResult('car'));
        container.parent.addChild(car1);
        car1.visible = false;
    }

    if (id == "bg1") {
        bg1 = new createjs.Bitmap(preload.getResult('bg1'));
        container.parent.addChild(bg1);
        bg1.visible = false;
    }

    if (id == "questionText") {
        var spriteSheet5 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("questionText")],
            "frames": { "regX": 50, "height": 96, "count": 0, "regY": 50, "width": 396 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        questionText = new createjs.Sprite(spriteSheet5);
        container.parent.addChild(questionText);
        questionText.visible = false;
    }
    //
    if (id == "choice1") {
        var spriteSheet2 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 170, "count": 0, "regY": 50, "width": 170 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        choice1 = new createjs.Sprite(spriteSheet2);
        choice1.visible = false;
        mc = new createjs.MovieClip()
        container.parent.addChild(choice1);

        choice2 = new createjs.Sprite(spriteSheet2);
        choice2.visible = false;
        mc1 = new createjs.MovieClip()
        container.parent.addChild(choice2);
        /////
        choice3 = new createjs.Sprite(spriteSheet2);
        choice3.visible = false;
        mc2 = new createjs.MovieClip()
        container.parent.addChild(choice3);


        choice4 = new createjs.Sprite(spriteSheet2);
        choice4.visible = false;
        mc3 = new createjs.MovieClip()
        container.parent.addChild(choice4);
    };

    if (id == "question") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question")],
            "frames": { "regX": 50, "height": 231, "count": 0, "regY": 50, "width": 231 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        question = new createjs.Sprite(spriteSheet1);
        question.visible = false;
        container.parent.addChild(question);
    };

    if (id == "question1") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question1")],
            "frames": { "regX": 50, "height": 47, "count": 0, "regY": 50, "width": 181 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        q1 = new createjs.Sprite(spriteSheet1);
        q1.visible = false;
        container.parent.addChild(q1);
    };

    if (id == "question2") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question2")],
            "frames": { "regX": 50, "height": 47, "count": 0, "regY": 50, "width": 181 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        q2 = new createjs.Sprite(spriteSheet1);
        q2.visible = false;
        container.parent.addChild(q2);
    };

    if (id == "question3") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question3")],
            "frames": { "regX": 50, "height": 47, "count": 0, "regY": 50, "width": 181 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        q3 = new createjs.Sprite(spriteSheet1);
        q3.visible = false;
        container.parent.addChild(q3);
    };

    if (id == "question4") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question4")],
            "frames": { "regX": 50, "height": 47, "count": 0, "regY": 50, "width": 181 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        q4 = new createjs.Sprite(spriteSheet1);
        q4.visible = false;
        container.parent.addChild(q4);
    };

    if (id == "Buttons") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("Buttons")],
            "frames": { "regX": 50, "height": 162, "count": 0, "regY": 50, "width": 158 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):

        });
        buttons = new createjs.Sprite(spriteSheet1);
        buttons.visible = false;
        container.parent.addChild(buttons);
    };
    //
    if (id == "chHolder") {
        chHolderMc = new createjs.Bitmap(preload.getResult('chHolder'));
        container.parent.addChild(chHolderMc);
        chHolderMc.visible = false;
    }

    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("9B187A3141F2F044B0356886562D636B");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }
}

function tick(e) {
    stage.update();
}
/////////////////////////////////////////////////////////////////=======HANDLE CLICK========//////////////////////////////////////////////////////////////
function handleClick(e) {
    qno = between(0, 3);
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Observe the direction of the cars and answer the questions";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 50;

    bg.visible = true;
    bg1.visible = true;

    questionText.gotoAndStop(0);
    questionText.x = 490;
    questionText.y = 300;
    questionText.visible = true;

    q1.x = 595; q1.y = 410;
    q2.x = 595; q2.y = 410;
    q3.x = 595; q3.y = 410;
    q4.x = 595; q4.y = 410;
    q1.visible = false;
    q2.visible = false;
    q3.visible = false;
    q4.visible = false;

    container.parent.addChild(car1);
    car1.visible = true;
    // car1.x = 370;
    // car1.y = 350;

     var carX = [,,250, 950]
    var carY = [,,330, 330]
    //question.x = 340; question.y = 150;
    for (i = 1; i <= 1; i++) {
        this["choice" + i].x = 610
        this["choice" + i].y = 600;
        this["choice" + i].visible = true;
        this["choice" + i].scaleX = this["choice" + i].scaleY = .75;
        chpos.push({ posx: this["choice" + i].x, posy: this["choice" + i].y })
    }

    for (i = 2; i <= 3; i++) {
        // this["choice" + i].x = 110 + ((i - 2) * 500)
        this["choice" + i].x = carX[i]
        this["choice" + i].y = carY[i];
        this["choice" + i].visible = true;
        this["choice" + i].scaleX = this["choice" + i].scaleY = .75;
        chpos.push({ posx: this["choice" + i].x, posy: this["choice" + i].y })
    }

    //chHolderMc.visible = true;
    var btnX = [,100, 300, 900, 1100]
    for (i = 1; i <= 2; i++) {
        this["bt" + i] = buttons.clone();
        // this["bt" + i].x = 580 + ((i - 1) * 160);
        this["bt" + i].x = btnX[i]
        this["bt" + i].y = 590;
        this["bt" + i].scaleX = this["bt" + i].scaleY = 0.85;
        this["bt" + i].visible = true;
        container.parent.addChild(this["bt" + i]);
    }
    for (i = 3; i <= 4; i++) {
        this["bt" + i] = buttons.clone();
        this["bt" + i].x = btnX[i];
        this["bt" + i].y = 590
        this["bt" + i].visible = true;
        this["bt" + i].scaleX = this["bt" + i].scaleY = 0.85;
        container.parent.addChild(this["bt" + i]);
    }
    bt1.gotoAndStop(1);
    bt1.name = "left";
    bt2.gotoAndStop(0);
    bt2.name = "right";
    bt3.gotoAndStop(2);
    bt3.name = "up";
    bt4.gotoAndStop(3);
    bt4.name = "down";
    container.parent.addChild(choice1, choice2, choice3, choice4)
    // mc2.timeline.addTween(createjs.Tween.get( this["choice"+3]).to({ scaleX: 0.95, scaleY: 0.95 }, 19).to({ scaleX: 1, scaleY: 1 }, 20).wait(1));
    for (i = 1; i <= 4; i++) {
        tweenMcArr[i] = new createjs.MovieClip()
        container.parent.addChild(tweenMcArr[i])
        //tweenMcArr[i].timeline.addTween(createjs.Tween.get(this["bt" + i]).to({ scaleX: 0.8, scaleY: 0.8 }, 19).to({ scaleX: .85, scaleY: .85 }, 20).wait(1));

    }
    pickques();
}
//==============================================================HELP ENABLE/DISABLE===================================================================//
function helpDisable() {
    for (i = 1; i <= 4; i++) {
        this["bt" + i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 1; i <= 4; i++) {
        this["bt" + i].mouseEnabled = true;
    }
}
//==================================================================PICKQUES==========================================================================//
function pickques() {
    var j = [];
    var m = [];
    cnt++;
    quesCnt++;
    chpos = []
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    //////////////////////////////////////////////////////////////////////////////////////////
    boardMc.boardMc.openMc.mouseEnabled = true;
    boardMc.boardMc.openMc.alpha = 1;
    var qno1 = between(0, 3);
    console.log("qno1 : " + qno1);
    var ranpos = [0, 90, 180, 270];
    var dirname = ["up", "right", "down", "left"];
    ranpos.sort(randomSort);
    console.log(ranpos);

    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].rotation = 0;
    }

    for (i = 1; i <= choiceCnt; i++) {

        this["choice" + i].visible = true;
        this["choice" + i].scaleX = this["choice" + i].scaleY = .75;
        //  this["choice"+i].rotation=ranpos[i];

        this["choice" + i].name = dirname[(ranpos[i] / 90)];
        // this["choice"+i].cursor = "pointer";
        if (this["choice" + i].name == "up") {
            var ran = [0, 1, 2, 3];

            j[i] = ran[ranpos[i] / 90];

            console.log("i:" + j);
        }
        else if (this["choice" + i].name == "right") {
            var ran = [4, 5, 6, 7];

            j[i] = ran[ranpos[i] / 90];

            console.log("i:" + j);
        }
        else if (this["choice" + i].name == "down") {
            var ran = [8, 9, 10, 11];

            j[i] = ran[ranpos[i] / 90];
            console.log("i:" + j);
        }
        else if (this["choice" + i].name == "left") {
            var ran = [12, 13, 14, 15];

            j[i] = ran[ranpos[i] / 90];
            console.log("i:" + j);
        }
        m[i] = i;
        console.log("m:" + m)
        this["choice" + i].gotoAndStop(j[i]);
    }
    q1.visible = false;
    q2.visible = false;
    q3.visible = false;
    q4.visible = false;
    var pos = between(1, 3);
    console.log("pos " + pos)
    var ps = (ranpos[pos[1]] / 90);
    console.log("ps : " + ps);
    console.log("rpos" + (ranpos[1] / 90))
    if ((ranpos[1] / 90) == 0) {
        q1.gotoAndStop(j[pos[1]] / 4);
        q1.visible = true;

    }
    else if ((ranpos[1] / 90) == 1) {
        q2.gotoAndStop(j[pos[1]] / 4);
        q2.visible = true;
    }
    else if ((ranpos[1] / 90) == 2) {
        q3.gotoAndStop(j[pos[1]] / 4);
        q3.visible = true;
    }
    else {
        q4.gotoAndStop(j[pos[1]] / 4);
        q4.visible = true;
    }



    ans = this["choice" + m[pos[1]]].name;
    console.log("ans:" + ans)
    //////////////////////////////////////////////////////////////////////////////////////
    rst = 0;
    gameResponseTimerStart();
    enablechoices()
    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
}
//====================================================================CHOICE ENABLE/DISABLE==============================================================//
function enablechoices() {
    for (i = 1; i <= 4; i++) {
        this["bt" + i].addEventListener("click", answerSelected);
        this["bt" + i].addEventListener("mouseover", onRoll_over);
        this["bt" + i].addEventListener("mouseout", onRoll_out)
        this["bt" + i].cursor = "pointer";
        this["bt" + i].visible = true;
        this["bt" + i].alpha = 1;
    }
}

function disablechoices() {
    for (i = 1; i <= 4; i++) {
        this["bt" + i].removeEventListener("click", answerSelected);
        this["bt" + i].removeEventListener("mouseover", onRoll_over);
        this["bt" + i].removeEventListener("mouseout", onRoll_out)
        this["bt" + i].visible = false;
        this["bt" + i].alpha = .5;
    }
    boardMc.boardMc.openMc.mouseEnabled = false;
    boardMc.boardMc.openMc.alpha = .5;
}
//===================================================================MOUSE ROLL OVER/ROLL OUT==============================================================//
function onRoll_over(e) {
    e.currentTarget.alpha = .5;
    stage.update();
}

function onRoll_out(e) {
    e.currentTarget.alpha = 1;
    stage.update();
}
//=================================================================ANSWER SELECTION=======================================================================//
function answerSelected(e) {
    e.preventDefault();
    uans = e.currentTarget.name;
    console.log("answer" + uans);
    console.log(ans + " =correct= " + uans)
    gameResponseTimerStop();
    if (ans == uans) {
        getValidation("correct");
    } else {
        getValidation("wrong");
    }
    disablechoices();
}
