<div class="col-12 col-md-12 col-sm-12 col-xs-12">
  <div><h2 id="MonthID1"><span class="col-lg-10">Rank : <?php echo array_search($cus1['bspi'],$su1_r)+1; ?></span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>

 
 <div id="containerone" style="padding-top:20px;border:1px solid #c5811a;background:url('<?php echo base_url(); ?>assets/images/chart_patterns/acp1.png')">
</div>
 </div> 
<script>
	
	var chart4 = new Highcharts.chart({

    chart: {
		 renderTo: "containerone",
        type: 'scatter',
         
		
		  events: {
            load: function(){
			this.tooltip.refresh(this.series[0].data[<?php echo array_search($cus1['bspi'],$su1); ?>]);
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
         
		gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false,style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
}
    },

    yAxis: {
		labels: {
            style: {
                fontSize: '20px',
				color: '#fff',
				fontFamily: 'Phenomena-Regular'
            }
        },
		title: {text: 'Score',style: {fontSize: '25px',color: '#ccc',fontFamily: 'Phenomena-Regular'}},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
	useHTML: true,
   backgroundColor: 'rgba(0, 0, 0, 0.16)',
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><img width="40px" height="40px" src="'+this.point.myData+'" /><br/><b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b></div>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '';
            }
        }
    }
},
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport1 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",myData : '".base_url().$myavatarimage."',marker:{symbol: 'url(".base_url()."assets/images/starsmall.png)' } },";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).", myData : '".base_url().$myavatarimage."',marker:{ symbol: 'url(".base_url()."assets/images/starani2.gif)',radius:5} },";
			}
		}
		?>
            
        ] 
    }]

});
$('#loadingimage1').css("display", "none");	

</script>