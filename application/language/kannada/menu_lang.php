<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['welcome_message'] = 'Welcome to CodexWorld';

$lang['home'] = 'ಮನೆ';
$lang['brainwave'] = 'ಮಿದುಳಿನ ತರಂಗ';
$lang['Whyskillangels'] = 'ಏಕೆ ಕೌಶಲ ಏಂಜೆಲ್';
$lang['ourstory'] = 'ನಮ್ಮ ಕಥೆ';
$lang['corevideo'] = 'ನಮ್ಮ ಕಥೆ...';
$lang['howitworks'] = 'ನಮ್ಮ ಕಥೆ...';
$lang['partOfrevolution'] = 'ಕೋರ್ ವೀಡಿಯೊ...';
$lang['mustknowfacts'] = '50 ಫ್ಯಾಕ್ಟ್ಸ್ ತಿಳಿದಿರಬೇಕು';
$lang['innews'] = 'ಸುದ್ದಿ';
$lang['inaction'] = 'ಕಾರ್ಯದಲ್ಲಿರುವುದನ್ನು';
$lang['superbrain'] = 'ಸೂಪರ್ ಮಿದುಳಿನ';
$lang['hearaboutus'] = 'ನಮ್ಮ ಬಗ್ಗೆ ಕೇಳಲು';
$lang['research'] = 'ಸಂಶೋಧನೆ';
$lang['industrylinks'] = 'ಉದ್ಯಮ ಕೊಂಡಿಗಳು';
$lang['experttalk'] = 'ತಜ್ಞ ಚರ್ಚೆ';

$lang['msgdashboard'] = 'Welcome to dashboard';

?>
