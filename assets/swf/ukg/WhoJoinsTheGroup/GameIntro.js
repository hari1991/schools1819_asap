(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("ATIkXQgUILlqFoQliFgm0KUQoaqUlilgQlqlogUoLQgBgdAAgcQgBn4FQl0QCZirC4hhIBfgvIDchDQBygVB6AAQB8AAByAVIDbBDIBgAvQC3BhCaCrQFPF0AAH4QAAAcgBAdg");
	this.shape.setTransform(0,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ArMLNQkpkpAAmkQAAmjEpkpQEpkpGjAAQGkAAEpEpQEpEpAAGjQAAGkkpEpQkpEpmkAAQmjAAkpkpg");
	this.shape_1.setTransform(0,-46.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC3333").s().p("AtIIZQlqlpgUoKIgCg6QAAn4FQlzQCZirC3hiQiLBOh6B6QlSFSAAHdIABA0QARG8FAE9QFRFSIPKNQGqqNFSlSQE/k9ARm8IABg0QAAndlRlSQh6h6iMhOQC3BiCZCrQFQFzAAH4IgCA6QgTIKlqFpQliFfm1KUQoaqUlhlfg");
	this.shape_2.setTransform(0,6.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AsuHoQk/k/gRm7IgBg0QAAndFRlRQB7h6CLhOIBggvIDbhDQBzgYB6AAQB8AAByAYQhygVh8AAQh6AAhzAVQBzgVB6AAQB8AAByAVIDbBDIBgAvQCMBOB5B6QFSFRAAHdIgBA0QgRG7lAE/QlRFSmqKNQoPqNlSlSgArMwSQkpEpAAGjQAAGjEpEpQEpEpGjAAQGkAAEokpQEpkpAAmjQAAmjkpkpQkokpmkAAQmjAAkpEpg");
	this.shape_3.setTransform(0,-13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-123.9,-162.9,247.9,326.2);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("AgRDXQgIgDgGgGQgGgGgEgIQgDgJgBgIQABgKADgIQAEgJAGgGQAGgGAIgDQAIgEAJAAQAKAAAIAEQAIADAHAGQAFAGAEAJQAEAIgBAKQABAIgEAJQgEAIgFAGQgHAGgIADQgIAEgKAAQgJAAgIgEgAgXA+QgEgegDgaQgDgZAAgcIAAirIBFAAIAACrQAAAcgDAZQgDAagEAeg");
	this.shape.setTransform(69,4.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("ABOCaQgKAAgEgNIg3iuIgFgSIgEgRIgDASIgFARIg4CuQgEANgMAAIg5AAIhhkzIA7AAQAIABAGADQAFAEACAGIAsClIAGAaIAEAaIAHgaIAIgaIAzimQACgFAGgFQAFgDAHAAIAgAAQAIAAAFADQAGAFABAFIAzCoIAHAZIAGAZIAFgaIAGgaIAuilQABgGAGgEQAGgDAHgBIA4AAIhhEzg");
	this.shape_1.setTransform(34.5,10.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Ag9CUQgcgMgVgUQgTgVgLgdQgMgeABgkQgBgkAMgdQALgdATgVQAVgUAcgMQAcgLAiAAQAiAAAcALQAcAMAUAUQATAVALAdQALAdAAAkQAAAkgLAeQgLAdgTAVQgUAUgcAMQgcALgiAAQgiAAgcgLgAg4hKQgTAaAAAwQAAAyATAZQATAbAmAAQAmAAATgaQASgaAAgyQAAgxgSgaQgTgagmAAQgmAAgTAbg");
	this.shape_2.setTransform(-6.3,10.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CCFF").s().p("ABnDZIhfkjIgEgMIgEgOIgCAOIgEAMIhgEjIhIAAIiHmxIBDAAQALAAAHAFQAHAFACAIIBKEEIAFAVIAEAYIAFgYIAHgVIBVkEQADgGAGgGQAHgGAKAAIAYAAQAKAAAHAFQAHAFADAIIBUEEQAHAUAEAXIAFgWIAEgVIBLkEQABgHAIgGQAHgFAKAAIBAAAIiHGxg");
	this.shape_3.setTransform(-54.7,4.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88.2,-34.9,170.2,76);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF9900").ss(3,1,1).p("ABKiMIAGCpIAEBeIACA9IAEB5IoZkeIBYg2IGskNIAFCkIFyA6IABAyIADA1IlqBm");
	this.shape.setTransform(655.9,203.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF99").s().p("Am/hyIBYg4IG9DcIAEB4gABUgKIgDhfIFsg9IACA2IlpBmg");
	this.shape_1.setTransform(655.9,216.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AmSAYIGskMIAGClIAGCoIgGioIFxA5IACAyIltA9IAEBfIABA9gAAghPg");
	this.shape_2.setTransform(660.1,197.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(609.6,171.2,92.5,64);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.qtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AgKBhIgEgDIgDgEIgBgFIABgFIADgFIAEgDIAFgBIAFABIAEADIADAFIABAFIgBAFIgDAEIgEADIgFABIgFgBgAgRA3IgCgJQAAgHADgGQACgGAEgFIAJgIIAJgJIAKgHIAJgJIAGgLQACgGAAgIQAAgIgCgHQgDgHgFgGQgFgGgHgDQgGgEgIAAQgIAAgGAEQgGADgEAFQgFAFgCAHQgCAHAAAIIAAAGIABAGIgVADIgCgIIgBgIQAAgMAFgKQAEgLAHgHQAIgIALgFQAKgFAMAAQAMAAALAFQALAEAHAIQAIAIAFALQAEALAAANIgBAMIgDAMQgDAGgEAGQgEAEgHAGIgMAJQgGAEgFAFQgGAFgDAHQgDAHACAJg");
	this.shape.setTransform(206.5,0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgaA/QgMgEgIgJQgIgJgFgMQgEgMAAgPQAAgNAFgOQAFgMAJgKQAIgKAMgFQAMgFANAAQAMAAALAFQALAEAJAJQAHAJAGALQAFALACANIhlAPQAAAIAEAHQADAHAGAEQAEAFAHADQAHABAHAAQAGAAAGgBQAGgCAGgEQAFgEAEgGQAEgFABgHIAXAEQgEALgFAJQgGAJgJAGQgHAGgKADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgGAEQgFAFgFAHQgEAHgBAKIBGgIIgBgDQgEgMgJgGQgHgHgMAAQgEAAgGACg");
	this.shape_1.setTransform(186.6,3.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#660000").s().p("AgVA/QgMgEgKgKQgKgJgGgNQgGgMAAgPQAAgIADgIQACgJAFgIQAEgHAHgGQAGgHAIgFQAHgEAJgDQAJgDAJAAQAKAAAKADQAJACAIAFQAIAFAGAGQAGAHAEAIIABABIgUAMIAAgBQgDgGgFgFIgJgIIgMgFQgGgCgHAAQgJAAgIAEQgJADgGAHQgGAGgEAJQgEAJAAAIQAAAKAEAIQAEAJAGAGQAGAHAJADQAIAEAJAAQAGAAAGgCQAGgBAFgEQAGgDAEgEQAEgEADgFIABgBIAUAMIAAAAQgEAIgHAGQgHAGgIAFQgIAEgJACQgJADgJAAQgMAAgNgGg");
	this.shape_2.setTransform(172.1,3.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#660000").s().p("AAhBBIACgmQABgTgBgTIgBgFIgBgHIgDgIQgBgEgDgCQgDgDgEgCQgEgBgGABQgJABgJANQgJAMgLAWIAAAfIABARIAAAJIgZADIgBglIgCgdIAAgWIgBgRIgBgZIAbgBIABAiQAEgHAFgFQAFgHAFgEQAGgFAGgDQAGgCAIgBQAJAAAGACQAHACAFAEQAEADADAGQAEAFABAFIADALIABAKQABATgBAVIgBArg");
	this.shape_3.setTransform(157.2,3.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#660000").s().p("AgaA/QgMgEgIgJQgIgJgFgMQgEgMAAgPQAAgNAFgOQAFgMAJgKQAIgKAMgFQAMgFANAAQAMAAALAFQALAEAJAJQAHAJAGALQAGALABANIhlAPQABAIADAHQADAHAFAEQAGAFAGADQAHABAHAAQAGAAAHgBQAFgCAGgEQAFgEAEgGQAEgFABgHIAXAEQgEALgFAJQgHAJgIAGQgHAGgKADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgFAEQgGAFgEAHQgFAHgBAKIBGgIIgBgDQgEgMgJgGQgHgHgMAAQgEAAgGACg");
	this.shape_4.setTransform(142.7,3.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#660000").s().p("AAgAgIgIANQgFAGgGAFQgGAFgGACQgGADgHAAQgKgBgIgDQgIgEgFgGQgFgGgDgIIgGgRIgBgSIgBgRIABgZIADgaIAZABIgDAaIgBAVIABAKIAAAMIADAOQABAHAEAFQADAFAEADQAFADAGgBQAJgBAKgMQAJgLALgWIgBgeIAAgRIAAgKIAZgDIABAlIABAcIABAXIAAARIABAZIgbABg");
	this.shape_5.setTransform(127.8,3.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#660000").s().p("AApBkIABhQIgHAFIgHAFIgIADIgHACQgHACgGAAQgOAAgMgFQgMgEgIgJQgJgJgFgMQgGgMAAgQQABgQAFgNQAGgNAJgJQAJgIALgEQAMgFAMgBQAGABAIADQAHABAIAEQAJADAHAHIAAgTIAWACIAADFgAgRhHQgHADgFAGQgFAHgDAIQgDAIAAAKQAAAKADAIQADAIAFAFQAFAGAIADQAHADAJABQAGgBAHgCQAHgDAHgEQAGgFAEgFQAEgHACgIIAAgSQgCgIgEgHQgFgHgFgFQgHgEgHgDQgHgCgHgBQgJABgHADg");
	this.shape_6.setTransform(111.9,6.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#660000").s().p("AgaA/QgMgEgIgJQgIgJgEgMQgFgMAAgPQAAgNAFgOQAFgMAJgKQAJgKALgFQAMgFANAAQAMAAALAFQALAEAIAJQAJAJAFALQAFALACANIhlAPQABAIADAHQADAHAFAEQAGAFAGADQAHABAHAAQAGAAAHgBQAGgCAFgEQAFgEAEgGQADgFACgHIAXAEQgEALgGAJQgFAJgJAGQgHAGgKADQgJAEgLAAQgOAAgNgFgAgJgtQgGABgFAEQgGAFgEAHQgFAHgCAKIBHgIIgBgDQgEgMgJgGQgHgHgMAAQgEAAgGACg");
	this.shape_7.setTransform(97.4,3.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#660000").s().p("AgQBDIgLgCIgKgEIgLgFIgKgHIALgSIAOAHIANAGIAPAEIAOAAIALAAIAIgDIAEgFIACgEIABgFIgCgEIgCgFIgHgEIgJgDIgNgCIgUgCQgJgCgIgDQgIgDgFgGQgFgGgBgIQgBgJADgHQABgHAEgFQAEgGAGgFQAFgEAIgCIAOgFQAHgBAHAAIALAAIAOACIANAFQAHACAGAFIgIAVQgHgEgIgCIgMgEIgMgBQgSgCgKAFQgLAFAAAKQABAIADADQAFADAGABIAQACIASACQALACAJADQAHADAFAEQAFAGACAFQADAGgBAGQAAAMgEAHQgFAIgIAEQgHAFgKACQgJADgMAAQgJAAgLgCg");
	this.shape_8.setTransform(83.3,3.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#660000").s().p("AgaA/QgMgEgIgJQgJgJgDgMQgFgMAAgPQAAgNAFgOQAFgMAJgKQAJgKAMgFQALgFANAAQAMAAALAFQALAEAIAJQAJAJAFALQAGALABANIhmAPQACAIADAHQADAHAGAEQAFAFAGADQAHABAHAAQAGAAAHgBQAFgCAFgEQAGgEADgGQAEgFACgHIAXAEQgDALgHAJQgGAJgHAGQgJAGgJADQgJAEgLAAQgOAAgNgFgAgJgtQgGABgFAEQgGAFgEAHQgFAHgCAKIBGgIIAAgDQgFgMgHgGQgJgHgLAAQgEAAgGACg");
	this.shape_9.setTransform(63.4,3.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#660000").s().p("AAgBaQADgLACgLIACgTIAAgTQgBgNgDgJQgEgIgGgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgHAGQgHAFgFAKIgBBPIgXAAIgCi1IAbgBIgBBIQAHgHAHgEIAOgGQAHgDAGgBQANAAALAFQALAFAIAJQAIAJAEAMQAFAMAAAQIAAARIgBARIgBAQIgDAOg");
	this.shape_10.setTransform(48.3,0.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#660000").s().p("AgNgNIgqABIABgVIApgCIABg0IAWgBIgBA1IAugCIgBAWIgtABIgBBnIgYAAg");
	this.shape_11.setTransform(34.5,1.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#660000").s().p("AAhBBIACgmQABgTgBgTIgBgFIgBgHIgDgIQgBgEgDgCQgDgDgEgCQgEgBgGABQgJABgJANQgJAMgLAWIAAAfIABARIAAAJIgZADIgBglIgCgdIAAgWIgBgRIgBgZIAbgBIABAiQAEgHAFgFQAFgHAFgEQAGgFAGgDQAGgCAIgBQAJAAAGACQAHACAFAEQAEADADAGQAEAFABAFIADALIABAKQABATgBAVIgBArg");
	this.shape_12.setTransform(14.9,3.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#660000").s().p("AgJBSIgEijIAZABIACCig");
	this.shape_13.setTransform(3.9,0.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#660000").s().p("AgNgNIgqABIABgVIApgCIABg0IAWgBIgBA1IAugCIgBAWIgtABIgBBnIgYAAg");
	this.shape_14.setTransform(-11.5,1.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#660000").s().p("AABASIgjAuIgYgHIAtg5Igvg5IAZgFIAjAsIAkgtIAWAJIgrA2IAuA3IgWAJg");
	this.shape_15.setTransform(-23.9,3.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#660000").s().p("AgaA/QgMgEgIgJQgIgJgFgMQgEgMAAgPQAAgNAFgOQAFgMAJgKQAIgKAMgFQAMgFANAAQAMAAALAFQALAEAJAJQAHAJAGALQAFALACANIhlAPQAAAIAEAHQADAHAFAEQAFAFAHADQAHABAHAAQAGAAAGgBQAGgCAGgEQAFgEAEgGQAEgFABgHIAXAEQgEALgFAJQgHAJgIAGQgIAGgJADQgKAEgKAAQgPAAgMgFgAgJgtQgGABgFAEQgGAFgFAHQgEAHgBAKIBGgIIgBgDQgEgMgJgGQgIgHgLAAQgEAAgGACg");
	this.shape_16.setTransform(-37.4,3.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#660000").s().p("AAhBBIACgmQABgTgBgTIgBgFIgBgHIgDgIQgBgEgDgCQgDgDgEgCQgEgBgGABQgJABgJANQgJAMgLAWIAAAfIABARIAAAJIgZADIgBglIgCgdIAAgWIgBgRIgBgZIAbgBIABAiQAEgHAFgFQAFgHAFgEQAGgFAGgDQAGgCAIgBQAJAAAGACQAHACAFAEQAEADADAGQAEAFABAFIADALIABAKQABATgBAVIgBArg");
	this.shape_17.setTransform(-52.1,3.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#660000").s().p("AgQBDIgKgCIgLgEIgLgFIgLgHIAMgSIAOAHIANAGIAPAEIAOAAIALAAIAIgDIAEgFIADgEIAAgFIgCgEIgDgFIgGgEIgJgDIgNgCIgTgCQgKgCgIgDQgIgDgFgGQgFgGgBgIQAAgJACgHQABgHAEgFQAEgGAGgFQAFgEAIgCIAOgFQAIgBAGAAIALAAIAOACIANAFQAHACAGAFIgIAVQgHgEgIgCIgMgEIgMgBQgRgCgLAFQgLAFAAAKQAAAIAFADQAEADAGABIAQACIASACQALACAJADQAHADAFAEQAFAGACAFQACAGAAAGQABAMgFAHQgFAIgIAEQgHAFgKACQgKADgLAAQgJAAgLgCg");
	this.shape_18.setTransform(-72.7,3.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#660000").s().p("AgaA/QgMgEgIgJQgJgJgEgMQgEgMAAgPQAAgNAFgOQAFgMAJgKQAJgKAMgFQALgFANAAQAMAAALAFQALAEAIAJQAJAJAFALQAGALABANIhmAPQABAIAEAHQADAHAGAEQAEAFAHADQAHABAHAAQAGAAAGgBQAHgCAEgEQAGgEADgGQAFgFABgHIAXAEQgDALgHAJQgGAJgHAGQgJAGgJADQgJAEgLAAQgPAAgMgFgAgJgtQgGABgGAEQgFAFgFAHQgEAHgCAKIBGgIIAAgDQgFgMgHgGQgJgHgLAAQgEAAgGACg");
	this.shape_19.setTransform(-86.3,3.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#660000").s().p("AA5AcIAAgaIgBgSQAAgKgDgEQgDgFgEAAQgDAAgDACIgGAFIgGAGIgGAHIgFAIIgEAGIABAKIAAAPIAAASIAAAVIgVACIgBglIgBgaIgBgSQAAgKgDgEQgDgFgEAAQgDAAgEACIgGAGIgGAHIgGAIIgGAIIgEAFIABA+IgXACIgEh+IAZgDIABAjIAIgKIAJgKQAFgEAFgCQAGgDAHAAQAFAAAFACQAEABADADQAEAEACAFQADAGABAGIAHgJQAEgFAFgEQAFgEAGgCQAFgDAHAAQAFAAAFACQAFABAEAEQAEAEACAGQADAGAAAIIABAVIABAcIABApIgZACIAAglg");
	this.shape_20.setTransform(-103.4,3.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#660000").s().p("AgYBAQgLgEgJgKQgIgIgFgOQgGgMAAgQQAAgOAGgNQAFgNAIgKQAJgIALgFQAMgGAMAAQANABAMAFQALAFAIAJQAJAKAGANQAEAMAAAOQAAAPgEANQgGANgJAIQgIAKgLAFQgMAFgNABQgMgBgMgFgAgRgrQgGAFgFAGQgFAHgCAJQgCAJAAAHQAAAJACAIQADAJAEAGQAFAIAHAEQAHAFAJgBQAKABAHgFQAIgEAEgIQAFgGACgJQACgIAAgJQAAgHgCgJQgCgJgFgHQgEgGgIgFQgHgFgKAAQgJAAgIAFg");
	this.shape_21.setTransform(-120.4,3.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#660000").s().p("AgVA/QgMgEgKgKQgKgJgGgNQgGgMAAgPQAAgIADgIQACgJAFgIQAEgHAHgGQAGgHAIgFQAHgEAJgDQAJgDAJAAQAKAAAKADQAJACAIAFQAIAFAGAGQAGAHAEAIIABABIgUAMIAAgBQgDgGgFgFIgJgIIgMgFQgGgCgHAAQgJAAgIAEQgJADgGAHQgGAGgEAJQgEAJAAAIQAAAKAEAIQAEAJAGAGQAGAHAJADQAIAEAJAAQAGAAAGgCQAGgBAFgEQAGgDAEgEQAEgEADgFIABgBIAUAMIAAAAQgEAIgHAGQgHAGgIAFQgIAEgJACQgJADgJAAQgMAAgNgGg");
	this.shape_22.setTransform(-134.9,3.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#660000").s().p("AgOgNIgoABIAAgVIApgCIABg0IAWgBIgBA1IAugCIgBAWIgtABIgBBnIgYAAg");
	this.shape_23.setTransform(-154.3,1.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#660000").s().p("AAlBDIABgQQgOAJgOAEQgNAEgMAAQgIAAgJgCQgHgCgGgFQgHgEgDgHQgEgIAAgJQAAgKADgHQACgIAFgGQAGgEAHgEQAGgFAIgCIAQgEIAQgBIAPAAIAMACIgFgMQgBgGgEgEQgEgFgEgCQgFgDgIAAIgKABQgFABgHAEQgIADgIAHQgIAGgKAKIgOgSQALgKAKgHQALgHAJgEQAKgEAHgBIAOgBQAKAAAJADQAJAEAGAHQAGAGAFAJQAEAIADAKIAEAVIABAUIgBAZIgEAcgAgFAAQgKABgHAFQgHAFgEAHQgDAHACAIQACAHAEADQAFADAHAAQAGAAAJgCIAOgGIAPgHIAMgHIAAgMIgBgMIgLgBIgNgBQgLAAgJACg");
	this.shape_24.setTransform(-168,3.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#660000").s().p("AAgBaQADgLACgLIACgTIAAgTQgBgNgDgJQgEgIgGgFQgFgGgHgCQgGgDgHAAQgFABgHADQgGADgHAGQgHAFgFAKIgBBPIgXAAIgCi1IAbgBIgBBIQAHgHAHgEIAOgGQAHgDAGgBQANAAALAFQALAFAIAJQAIAJAEAMQAFAMAAAQIAAARIgBARIgBAQIgDAOg");
	this.shape_25.setTransform(-183,0.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#660000").s().p("AhGBbIghirIAZgEIAWCGIAsh7IAdADIAmB/IAYiSIAYAEIgfCvIgcgBIgriFIguCHg");
	this.shape_26.setTransform(-202.3,0.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#CC9900").ss(3,1,1).p("EggggDTMBBBAAAQA/AAAsA+QAuA+AABXQAABXguA+QgsA/g/AAMhBBAAAQg/AAgtg/Qgtg+AAhXQAAhXAtg+QAtg+A/AAg");
	this.shape_27.setTransform(1.6,2.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFCC00").s().p("EgggADUQg/AAgtg/Qgtg+AAhXQAAhXAtg+QAtg+A/AAMBBBAAAQA/AAAsA+QAuA+AABXQAABXguA+QgsA/g/AAg");
	this.shape_28.setTransform(1.6,2.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.qtext, new cjs.Rectangle(-223.2,-19.9,449.8,45.3), null);


(lib.Symbol1copy15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#85B022").s().p("AmeCEQgIhABShDQCxiNDvgTQC1gOCfBBQhogThtADQiSAHiFAsQiQAyhlBfQgsAmgmA1QgKgOgBgRg");
	this.shape.setTransform(291.5,122.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#769F1A").s().p("AlqCgQgjgQgPgaQAmg1AsgmQBlhfCQgyQCFgsCSgHQBtgDBoATIAGACQiHALh7AnQiRA2hoBbQgbAVgzAuQgwAmgoAMQgZAKgYAAQgbAAgagLg");
	this.shape_1.setTransform(292.4,126.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F7BE51").s().p("AhwHFQAThjgRhjQgMhNg/iOQgvhygOg6QgLhHARhJQAWhLAug9IgDABQAqg3A2giQA4gkA7gIQAAgBAAAAQAAAAAAAAQAAAAABAAQAAAAAAABIAGgDQApAhBUDKQBsECggC3QgQBXgxBSQgtBRhCA9QhEA+hSAgQgeAPgfAHQASgxANgyg");
	this.shape_2.setTransform(318.6,190.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F7AF28").s().p("Aj4KgQgqAAgLgQQgOgRAIgbQAGgQAQgcQAqhDARhNQAMhMgLhNQgLhHg4h9Qg3h+gLhEQgRhfAYhiQAahiA5hOQA1hHBIgvQBNgxBRgMQARgFATAAQBjAMB7EoQB6EpglDPQgVBqg4BjQg4BihSBIQhVBOhiAnQhjAphmAAIgHgBgAjxkdQgRBJALBHQAOA6AvByQA/COAMBNQARBjgTBjQgNAygSAxQAfgHAegPQBSggBEg+QBCg9AthRQAxhSAQhXQAgi3hskCQhUjKgpghIgGADQAAgBgBAAQAAAAAAAAQAAAAAAAAQAAAAAAABQg7AIg4AkQg2AigqA3IADgBQguA9gWBLg");
	this.shape_3.setTransform(318.7,190.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C88014").s().p("AAIAyQgKgGgLAAQADgOAAgOIAAgOQgEghAAgOQAMAAARgEIAABDIAAAaQAAAGgBAAg");
	this.shape_4.setTransform(332.9,114.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FBA910").s().p("AkZMxQikgEgzgaQg1gegRglQgEgNAAgLQgDgeANgmIAvhZQA5iGhLjSQgqhtgWg3QgqhigLhEQgVhnAZhsQAYhkA8hdQA6hbBUhCQAEAAABgCQA1gqA5gaQAigRAngLQBvghBxALQAAAOgDAOQAKAAALAFIAGAAQABAAAAgFIAAgbQBSAKBRAcQBYAgBFAxQBpBFBIBqQAtA6AeBDIAAAKQAyB7AQCQIAAAFQAODShZDAQhTC5igCHQhPBDhZAwQhZAshfAcQhkAZhkAAQgiAAgigDgAh6rfQhSAMhNAxQhIAvg1BHQg5BOgaBiQgYBiARBfQALBEA3B+QA4B9ALBHQALBNgMBMQgRBNgqBDQgQAcgGAQQgIAbAOARQALAQAqAAQBpACBngqQBjgnBUhOQBShIA4hiQA4hjAVhqQAljPh6kpQh7kohigMQgTAAgRAFg");
	this.shape_5.setTransform(332.5,197.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_6.setTransform(324.4,194.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer_2
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("AvfPfQmamaAApFQAApEGambQGcmaJDAAQJEAAGbGaQGbGbAAJEQAAJFmbGaQmbGbpEAAQpDAAmcmbg");
	this.shape_7.setTransform(324.4,194.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy15, new cjs.Rectangle(184.2,54.3,280.4,280.4), null);


(lib.Symbol1copy14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6B4332").s().p("AAsAqIgBgEQgFgMgJgGQgGgEgFgBIAAgJQACgEADgCQAEgDAJAAIACABIACAEIADAFIAAACIAEAHQADAGAAAJIgDAPQAAAAgBAAQAAgBgBAAQAAgBAAAAQgBgBAAgBgAB2ALIABgDQAEgHAFgFIAEgGIACgEQAHAAAJAGQANALAIAPQgQgGgSADQgFAAgHACIgJADIACgJgAiOgIIgHAAIgMgBIgJgBIACgCIAGgHQADgEAEgDQADgCAHAAQAIACAEADIAEADQgGAFgGAJIgBgCgAB/gdIgOABIgCgHIgBgKQAJAAAIAEQALAFAFAIQgHgCgJABg");
	this.shape.setTransform(347,262.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B19015").s().p("AAQCKQgDAAgFgDQgFAAgDgBQgEgDgFgGIgJgKQgHgJgJgIIgPAGIgEgEQgPgRgJgTIADgDIAHgIQgPgngKgmQgGgfgLgrIgJgoIAIAEQAFApANAoQASAzAdApQAHAKACAdQAOgIAEAEQAPAVAIARQAAgHAFgHQAGgGAHgEQALgEAVAEQAjAHAdAYIgCAIIgGAEQgMAAgJgEIgIgFIgJgFQgIgFgMAAQgCgDgDgCIgJgCIgCgBQgIAAgFADQgDADgBAEIgBAJIAAABQgBAHACAIIgEgBg");
	this.shape_1.setTransform(346.9,251.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D0A815").s().p("AA5BmQgVgFgLAFQgHADgGAHQgFAGAAAHQgIgQgPgVQgEgFgOAJQgCgdgHgLQgdgpgSgyQgNgogFgpQAHABAHgDQAHgDAFgEIAAgCIALAvQAUBPAfBPIADADQANAIARgCQAMgBAPgEIAagKQAXgFAXABQAAAGABAGIABAKIACAHIAPgBIAEADQADAFAAAEIAAACIgDAFIgEAFQgFAGgDAIIgCACQgdgYgjgGgAiCB2QgCgCgFgCQAFgJAGgFIACgEQACgDAFgDQAFgCAIgBIALgBIALgEIAHgEQAJATAPARIAEAEQgLgCgMgBQgaADgMABIgJAAIgNgBg");
	this.shape_2.setTransform(346.7,250.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#649FCD").s().p("AgPAoIgIgEIgDgCQgKgGgHgPQgFgLgCgLQgCgOABgQIAAAAIATAAIACAVQAEAQAGAOQADAIAGAAQADAAAEgHQAJgMABAAIALgMQAPgLAUgDQgSAWgPAWQgGAIgDACIAAACQgFAFgGACQgEACgFAAIgFAAg");
	this.shape_3.setTransform(337.4,234);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#73ACD9").s().p("AgmAZQgGgOgEgPIgCgWIAlAAIAUgBQAXgBAVgEIgRAVQgUACgOALIgLAMQgBAAgKANQgEAGgDAAIAAAAQgGAAgDgIg");
	this.shape_4.setTransform(339.2,232.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#37414D").s().p("AASgIIADAAIgpATQAJgcAdAJg");
	this.shape_5.setTransform(401.5,142,0.543,0.543,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F7FAFA").s().p("AgHASQgEgDgCgGQgBgGADgHQADgHAGgEQAFgDAEACQAFACABAGQACAHgDAGQgDAHgGAEQgDACgDAAIgEAAg");
	this.shape_6.setTransform(387.4,136.3,0.543,0.543,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(0.5,1,1).p("ABnAAQAABBgeAuQgeAvgrAAQgqAAgegvQgeguAAhBQAAhBAeguQAeguAqAAQArAAAeAuQAeAuAABBg");
	this.shape_7.setTransform(388.3,137.5,0.131,0.131,0,-23.2,156.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#36424B").s().p("AhIBwQgfgvAAhBQAAhBAfguQAeguAqAAQArAAAeAuQAfAugBBBQABBBgfAvQgeAugrAAQgqAAgegug");
	this.shape_8.setTransform(388.3,137.5,0.131,0.131,0,-23.2,156.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FDFAFE").s().p("AgvCGQgkgNgMgxQgLgwATg2QAUg4AogeQAngeAkANQAlANALAwQAMAxgUA2QgUA4gnAeQgcAVgZAAQgMAAgLgEg");
	this.shape_9.setTransform(387.9,138,0.306,0.306,0,-3.5,176.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_10.setTransform(282.7,210.9,0.289,0.289,0,-2.5,177.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_11.setTransform(297.1,209.4,0.356,0.356,0,-2.5,177.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_12.setTransform(309.9,207.2,0.44,0.44,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#CDAA1C").s().p("AjnDyQhPghhRhQQBAAyBCAcQBmAoBggSQAjgHALgUQAFgJAAgWIgBgSQgSgFgRgHQhBgahHhFQA0AmA2AWQAeAMAeAHIAVADQAwAHAvgIQAggHAKgSQAFgIAAgUIgDgaQgugDgwgTQg2gXg2g3QAnAbAnARQA8AWA4AAIAQgBIAcgEQAdgGAJgPQAEgHAAgSIgCgWQglgDgmgQQg4gXg9g+QArAgAsASQAyAUAwABIAQgBQAPAAAPgDQAagGAIgOQADgGAAgNQgggEghgNQgxgUg0g3QAjAZAkAPQAvASAtAAIATgBIATgCQAXgFAHgNQADgGAAgPQgCgggKgaQAoA1ACAoQAAAPgDAGQgHANgXAFQgTADgTAAQACAOABAQQAAAQgDAHQgIAOgaAGQgXAEgXAAQAHAZACAeQAAARgEAHQgJAQgdAGQgVAEgXABQAJAcACAgQAAAUgFAIQgKASggAHQg5AKg8gMQAFAWABAYQAAAWgFAJQgLAUgiAHQgeAGgeAAQhDAAhHgcg");
	this.shape_13.setTransform(324.3,202.7,0.543,0.543,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_14.setTransform(394.9,119.9,0.57,0.57,0,35.6,-144.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_15.setTransform(389.6,117.6,0.57,0.57,0,27.2,-152.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_16.setTransform(384.5,117.2,0.57,0.57,0,17.4,-162.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#D4AE13").s().p("Ag9CIQAvgmAfg2QAXgnAMgpQg7gEgWg3QgKgaAagKQAvgQATAwQAKAYgBAaQgLBIgvA7QgWAeggAYg");
	this.shape_17.setTransform(379.6,118.9,0.543,0.543,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#38660A").s().p("ACzBVQAIgRAMgRQgcAEgbAKQAegjAhgeIACABQAEAAADACIACABIAUAFIAKADQgcAegaAoQgLAQgPAZQADgTAIgTgAAMBhQAKgYARgTQADgGAEgDIAFgGQAagbAhgRIACgCIAJgFIgfABQgZAEgaAHIgfAIIgGAEIgiANQAhgcAmgZQATgNAUgKQAbAOAcALIAKADIARAIQhAAqgmAiIgTARIgXAWIgMAMIAIgPgAjjgnQAKgHAMgFIABgBIAJgFQA6ggAjgOQAigNAfgGIAUANIASAOIAOAJQg+ARglANQgnANghARIgBAAIABgCIAOgJQATgLAXgKIAfgOQAEAAAEgDIgOAAQgUACgVADIgSADQgaAHgZAKIgTAJQgXALgUAPQAJgMALgMg");
	this.shape_18.setTransform(264.6,222);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#4D8C0F").s().p("ACUCBQAPgaALgQQAagoAcgeIAHACIAuAMIgRAHQgOAGgNAIIgCABQgeATgYAXQgTARgPAVIABgEgAAZCAQAWgYAggmIAPgSIAPgSIgWALIgaARQgXAOgTAQIgUATIgPAPIABgEIAMgNIAWgVIATgRQAmgjBAgqIADABIAjALIAJADQghAegeAjIgQAGQguATgkAhIgFAEIAEgEgAjnAXIAHgHQAWgUAZgRIABAAQAhgRAngMQAlgNA/gSIAXAOIAQAKIAUAJIADACQgUALgTANQglAXgiAeIgfAQQgeAQgiAVQAUgcAYgYQAWgWAZgRQATgOAUgLIAYgOQgUADgUAEQgaAFgYAIQgWAHgWAJIgVAKQgoATgkAbQgeAWgaAbQARgnAgghgAjgg5IACAAQAKgLANgJQAMgJAOgIIAVgLQASgJAUgHIAWgIIAOgDIAVAPQgfAGgiAOQgjANg6AhIgJAEIgBABQgMAGgKAHIAXgYg");
	this.shape_19.setTransform(266.6,221.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#B19015").s().p("AAECOIgLgHIgUgMQgigRgngBQgLgBgJACIgSgEIgFgBIABgGIAEgKIADgFIAmgHQAQgCAGgJQADgGAAgNQgDgrgZhCIgfhXQAHAEAKABQAFAdAOAlIAWA3QAMAgAFAYQAEATgDANQgDAIgGAGQgEAGgHACQAZgHAaALQAZAMAOAXQAEgOANgFQANgIANAHQgHgFgBgIIgBgOQAAgEAMAAQAKABATAyIgBAAIgLgGQgQgEgKAEQgEABgDAEQgGAEgEANIgBAEQgCAGgBAGQgMgEgPgIgACMBAIADAAIABADIgEgDg");
	this.shape_20.setTransform(329.7,259.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#D0A815").s().p("AAkBZQgMgBAAAEIABAPQABAIAHAEQgNgGgNAHQgMAGgEAOQgNgYgbgLQgZgLgZAGQAGgCAEgGQAGgGADgHQADgNgDgUQgGgXgMggIgVg3QgOglgFgeIAEAAQANgBAMgGQAKAfAKAoIAXBfQAEASADAHQAFANAJAGIABABIAIAEQAGADAKABQAGACAUAAIAGABQAKABAHADQgCgEgDgDIAXAAIArgCQAOgBAMADIADADIADAOIADAAIAJACQAGACAAAEIgDAGIgBABQgKANgDAMIgRgDQgTgCgQAEIgGADQgTgygKgBg");
	this.shape_21.setTransform(332.2,257.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#6B4332").s().p("AAnAZQgGgGgHgEIABgEQAEgMAGgEQADgEAEAAQAKAYgEAcQgEgKgHgIgACLgFQAEgMAKgNQALAFAIAJQAMAMADAOQgXgKgZgFgAiugjQAHgFAKgBQAFgBAGACIgEAKIgBAFQgUgDgPAIQAFgJAHgGg");
	this.shape_22.setTransform(330.5,271.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#649FCD").s().p("AgHBMQgKAAgHgEIgHgGIgDgEQgJgJgEgTIgCgJIgEgVQgHgngCgoIAVABQgGAkAFAmIABAEQABAKAEAIQADAHAGADQAOANAigLQAXgIATgLIgBACIgPAaQgIAMgKAHIgEAEIgEADQgLAHgMAAIgFAAg");
	this.shape_23.setTransform(319,236.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#73ACD9").s().p("Ag5AyQgGgEgDgHQgEgHgBgKIgBgFQgFglAGgkIAfADIARABIBiAEIgeA4IgMAYQgTALgWAJQgQAFgLAAQgOAAgIgHg");
	this.shape_24.setTransform(322,234.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#478EC6").s().p("Ag9CgQAygbAggaQArghAYglQAshFgOhZQgJgzgagnQAMAHANALQA2AzAFBHQABAdgHAdQgHAggSAeIgGAJQglA7g/AkQhDAohPAOQgZAEgZABQA1gYA0gcg");
	this.shape_25.setTransform(358.2,208.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#2D7BB8").s().p("Ag4ClQArgoAQgyQALghACg7QADhHAFgYQAIgwAcgqIAQgZIAGBYQAGCeg5BjQghA9g5AjIgXAOQASgfAIggg");
	this.shape_26.setTransform(380,190.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E5BC17").s().p("ABJDjIgngDIgFAAIgZgCIgRgBQhVgKhMgRIgvgMIgHgBIgJgDIgVgGIgBAAQgEgCgEgBIgBgBIgKgDIgigLIgDgCIgRgHIgKgEQgcgLgcgOIgDgBIgTgKIgRgKIgXgOIgOgIIgUgOIgTgOIgWgPIgWgRIAegTIATgMIAMgIIA7AbQBKAiAyAWQF3CfD7hlQBOggAyg4QA5g+gChKQgDhAgxg1QASAIAQAKIAOALIAEADQAjAdAQAnQAUA0gTA4QgGAUgNAVIgLAQQgSAcgYAYIgBABQgYAXgdATIgJAGQgmAYgpARQghANgjAKQgbAHgbAGIgaAEQgvAGgzAAIgPAAg");
	this.shape_27.setTransform(312.3,205.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#5AA2DB").s().p("AAiDfIgVAAIgigBIhjgEIgRgBIgegDIgVgBIgwgHQg4gJhAgPIgdgHIARgHQBMARBVAJIASACIAZABIAFABIAnACQA7ABA2gHIAZgEQAbgFAbgHQAjgKAhgOQApgRAmgYIAJgFQAdgUAYgWIABgBQAYgYASgcIALgQQANgVAGgVQATg4gUgzQgQgngjgdIABgCQAWAFAVANIACADQAaAnAJAzQAOBZgsBFQgYAlgrAhQghAagyAbQg0Acg1AYIgWABIgqAAg");
	this.shape_28.setTransform(331.7,207.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#984233").s().p("AAmATQACABABABQABABAAAAQAAAAAAAAQgBAAgBAAIgCgBIgBgCIAAAAIgBgCQgIgIgJgFQgSgOgVgGQgOgGgPgDIATACQAXABAVAMQANAHAJAFIAJAJIAFAPQgCAAgKgHg");
	this.shape_29.setTransform(404.9,148.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F0C61E").s().p("AomE+QgygWhKgiIg6gcIAPgJIALgIIAggTIAIgFIAQgIQAigUAkgTIARgKIAugWIANgHIAggOIAOgHIADgBIAMgGQASgJATgHQASgIARgGIAYgIQARgHAQgEQAUgHATgEIAngLIAdgGIAsgHIAIgBQALgCALgBIAegDQAhgDAgAAIARABQAkAAAjAFIAFABIAhAFIAZAEIAaAHIAGABQAiALAcAOQAxA1ADBAQACBKg5A/QgyA4hOAgQhgAnh0AAQi3AAjnhhgALVkeQgKgFgNgDIgegHQgegGgYgIIgLgEIgGgCIgGgCQgKgFgFgGQgFgFAAgHQAAgFACgEIACgCIADgDQAFgCAJABIAUABQAMgDADgKIAEgMIAAgKQAAgIADgFQAEgFAFgBQAFgBAGAEIAHAGQAlAmAXA0IAJAWIgJgJQgJgGgNgGQgWgMgXgCIgTgCQAPAEAOAFQAWAGASAPQAJAGAIAIIABABIgBgBg");
	this.shape_30.setTransform(336,179);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#C2DCDE").s().p("AgSAqQgIgQgDgSQgFgZAIgVQADgIAFAAQAGAAACAHIAKAXQAEANAGAHQAIALAMAEIACABIgDACIgDACIgHAHIAAAAQgJAIgFAIIgXgFg");
	this.shape_31.setTransform(391.4,145.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#AFC7C9").s().p("AgXBKIgLgDIgDgOQgSg8AQg+QABgIAEgDQAGgDAIAGQAOAKAEAcIADAVQAEAMAFAGQAGAKAMAEIABAAQgCADAAAFQAAAHAFAGQAEAFALAGIgJAGIgCgBQgMgEgIgLQgGgIgFgNIgJgWQgCgHgGAAQgFAAgDAIQgIAUAFAaQADASAIAQIgLgEg");
	this.shape_32.setTransform(390.8,141.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#A4B9BB").s().p("AgSBPQgKgGgJgIQgIgLgGgMQgEgLgCgMQgDgZAIgbIABgEQAHgXAPgMQAKgIALgCQAMgDAKAFQAOAHAGATQAFALABAXQAFAhAOANIgDADIgCACIgBAAQgMgEgGgJQgFgHgEgMIgEgVQgEgcgOgKQgHgFgGADQgEADgBAIQgQA8ASA+IADANIgJgFg");
	this.shape_33.setTransform(388.3,140.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#3489CD").s().p("AhbFdQgEhHg3gzQgMgLgMgHIgCgDIAAgBQAFgVAQgVQAkgqAQgWQAog4AFhaQACgggCgwIgDhQQgBgqAEgfQAFgnAQgeQAQggAegYQATgDAWAFQAkAJAdAZQARAOAPAUQAVAdAJASQgFgDgFAAQgGABgDAGQgEAEAAAIIAAALIgDAMQgEAKgMACIgUgBQgIAAgGACQgOgNgEgjQgCgWgEgMQgHgTgOgHQgKgEgMACQgMACgJAIQgOANgHAWIgCAFQgIAaAEAaQABANAFAKQAFANAIAKQAIAJALAGIAJAFIAKADIAMAEIAXAFIABAAIgCACIgJAOIgKASQgGAOgEAOIgDAHIgEASQgHAmACA0IAAAEIgQAYQgbArgIAwQgFAYgDBGQgCA8gLAiQgRAygrAnQAHgdgCgdg");
	this.shape_34.setTransform(384,166.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#2E7EBD").s().p("AhyEgIAPgRQARgVAQgYQBfiPgHihQgDgtAAgWIACgYQgDgLgBgOQgBgvApgmQAagWAggEQgfAYgQAgQgQAegGAnQgDAfABAqIACBQQACAvgCAgQgEBbgpA4QgOAWglAqQgPAVgGAVIAAABQgVgNgWgFg");
	this.shape_35.setTransform(373.5,156.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_36.setTransform(324.4,194.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_36).wait(1));

	// Layer_2
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("rgba(255,255,255,0.02)").s().p("AvfPfQmamaAApFQAApEGambQGcmaJDAAQJEAAGbGaQGbGbAAJEQAAJFmbGaQmbGbpEAAQpDAAmcmbg");
	this.shape_37.setTransform(324.4,194.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy14, new cjs.Rectangle(184.2,54.3,280.4,280.4), null);


(lib.Symbol1copy13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F6FCF3").s().p("AgpA5QgRgYAAghQAAggARgYQARgXAYAAQAYAAASAXQARAYAAAgQAAAhgRAYQgSAXgYAAQgYAAgRgXgAgjggQgLARAAAVQAAAWALAOQALARAQAAQAPAAAKgRQAMgOAAgWQAAgVgMgRQgKgPgPAAQgQAAgLAPgAgWADQgDgDgBgFIAAgCQAAgGAEgFQADgFAFAAQAFAAADAFIABACQACAFAAAEQAAAHgDADQgDAFgFAAQgFAAgDgFg");
	this.shape.setTransform(303.6,158.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00051D").s().p("AgOATQgGgHAAgMQAAgIAEgHQABAFADADQACAFAFABQAFgBADgFQADgDAAgHQAAgFgCgEQAGABAEAGQAHAJAAAKQAAAMgHAHQgFAJgJAAQgIAAgGgJg");
	this.shape_1.setTransform(302.6,159.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E1AA35").s().p("AgaAlQgLgPAAgWQAAgVALgQQALgPAPAAQAPAAALAPQAMAQAAAVQAAAWgMAPQgLAQgPAAQgPAAgLgQgAgNgYQgEAFAAAHIAAACQgEAHAAAHQAAANAGAHQAGAIAJAAQAIAAAFgIQAIgHgBgNQABgJgIgJQgEgHgGgBIgBgCQgDgEgFAAQgEAAgDAEg");
	this.shape_2.setTransform(302.7,158.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_3.setTransform(289.9,154.5,0.368,0.368,140.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_4.setTransform(295.7,160.1,0.368,0.368,140.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_5.setTransform(298.4,157.4,0.368,0.368,140.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_6.setTransform(295.7,149.8,0.368,0.368,140.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_7.setTransform(304.9,160.6,0.298,0.298,140.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_8.setTransform(301.3,156.3,0.368,0.368,140.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_9.setTransform(298.4,147.8,0.368,0.368,140.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_10.setTransform(304.9,148.6,0.298,0.298,140.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_11.setTransform(301.3,144.4,0.368,0.368,140.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_12.setTransform(307.4,162.7,0.368,0.368,140.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_13.setTransform(310.1,160.1,0.368,0.368,140.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_14.setTransform(307.4,152.3,0.368,0.368,140.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_15.setTransform(316.7,163.3,0.298,0.298,140.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_16.setTransform(313,158.8,0.368,0.368,140.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_17.setTransform(310.1,150.5,0.368,0.368,140.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_18.setTransform(316.7,151.2,0.298,0.298,140.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_19.setTransform(313,147.1,0.368,0.368,140.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_20.setTransform(300.7,173.7,0.368,0.368,140.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_21.setTransform(306.5,181,0.368,0.368,140.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_22.setTransform(303.6,172.7,0.368,0.368,140.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_23.setTransform(308.5,175.7,0.298,0.298,140.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_24.setTransform(305.1,171.6,0.368,0.368,140.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_25.setTransform(320.4,191,0.298,0.298,140.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_26.setTransform(316.8,186.9,0.368,0.368,140.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_27.setTransform(313.9,178.4,0.368,0.368,140.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_28.setTransform(311,170.8,0.368,0.368,140.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_29.setTransform(320.4,182.6,0.298,0.298,140.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_30.setTransform(316.8,178.2,0.368,0.368,140.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_31.setTransform(313.9,169.6,0.368,0.368,140.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_32.setTransform(318.9,172.8,0.298,0.298,140.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_33.setTransform(315.2,168.7,0.368,0.368,140.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_34.setTransform(326.4,178,0.298,0.298,140.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_35.setTransform(322.9,173.7,0.368,0.368,140.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_36.setTransform(319.8,165.1,0.368,0.368,140.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_37.setTransform(317,157.5,0.368,0.368,140.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_38.setTransform(326.4,169.3,0.298,0.298,140.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_39.setTransform(322.9,165.1,0.368,0.368,140.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_40.setTransform(319.8,156.6,0.368,0.368,140.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_41.setTransform(384.2,220.5,0.368,0.368,117.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_42.setTransform(377.7,220.5,0.368,0.368,117.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_43.setTransform(369.2,219,0.368,0.368,99.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_44.setTransform(361.4,221.2,0.368,0.368,79);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_45.setTransform(324.9,159.7,0.298,0.298,140.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_46.setTransform(321.3,155.5,0.368,0.368,140.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_47.setTransform(396.2,219.1,0.368,0.368,140.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_48.setTransform(389.7,215.1,0.368,0.368,140.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_49.setTransform(382.8,210.9,0.368,0.368,117.9);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_50.setTransform(376.3,210.9,0.368,0.368,117.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_51.setTransform(367.7,209.3,0.368,0.368,99.2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_52.setTransform(360.1,211.7,0.368,0.368,79);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_53.setTransform(355.1,215.3,0.368,0.368,79);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_54.setTransform(350,218,0.368,0.368,79);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_55.setTransform(401.9,214.3,0.368,0.368,140.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_56.setTransform(396.2,208.9,0.368,0.368,140.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_57.setTransform(389.7,204.9,0.368,0.368,140.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_58.setTransform(382.8,200.6,0.368,0.368,117.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_59.setTransform(376.3,200.6,0.368,0.368,117.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_60.setTransform(367.7,198.8,0.368,0.368,99.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_61.setTransform(360.1,201.3,0.368,0.368,79);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_62.setTransform(355.1,204.9,0.368,0.368,79);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_63.setTransform(350,207.6,0.368,0.368,79);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_64.setTransform(347.9,212.9,0.368,0.368,79);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_65.setTransform(341.9,217.1,0.368,0.368,79);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_66.setTransform(406.8,210.5,0.368,0.368,140.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_67.setTransform(401.9,204.7,0.368,0.368,140.9);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_68.setTransform(396.2,199.3,0.368,0.368,140.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_69.setTransform(389.7,195.3,0.368,0.368,140.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_70.setTransform(382.8,190.9,0.368,0.368,117.9);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_71.setTransform(376.3,190.9,0.368,0.368,117.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_72.setTransform(367.7,189.4,0.368,0.368,99.2);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_73.setTransform(360.1,191.9,0.368,0.368,79);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_74.setTransform(355.1,195.1,0.368,0.368,79);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_75.setTransform(350,198,0.368,0.368,79);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_76.setTransform(347.9,203.4,0.368,0.368,79);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_77.setTransform(341.9,207.5,0.368,0.368,79);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_78.setTransform(334.7,208.8,0.368,0.368,79);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_79.setTransform(326.8,206.1,0.368,0.368,79);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_80.setTransform(409.5,206.5,0.368,0.368,140.9);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_81.setTransform(406.8,198.8,0.368,0.368,140.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_82.setTransform(401.9,193,0.368,0.368,140.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_83.setTransform(396.2,187.4,0.368,0.368,140.9);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_84.setTransform(389.7,183.4,0.368,0.368,140.9);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_85.setTransform(382.8,179.1,0.368,0.368,117.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_86.setTransform(376.3,179.1,0.368,0.368,117.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_87.setTransform(367.7,177.7,0.368,0.368,99.2);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_88.setTransform(360.1,179.8,0.368,0.368,79);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_89.setTransform(355.1,183.6,0.368,0.368,79);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_90.setTransform(350,186.1,0.368,0.368,79);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_91.setTransform(347.9,191.4,0.368,0.368,79);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_92.setTransform(341.9,195.7,0.368,0.368,79);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_93.setTransform(334.7,196.8,0.368,0.368,79);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_94.setTransform(326.8,194.2,0.368,0.368,79);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_95.setTransform(411.8,194.8,0.368,0.368,140.9);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_96.setTransform(408.9,186.9,0.368,0.368,140.9);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_97.setTransform(404.2,180.9,0.368,0.368,140.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_98.setTransform(398.3,175.7,0.368,0.368,140.9);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_99.setTransform(392,171.6,0.368,0.368,140.9);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_100.setTransform(385.1,167.3,0.368,0.368,117.9);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_101.setTransform(378.6,167.3,0.368,0.368,117.9);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_102.setTransform(369.9,165.9,0.368,0.368,99.2);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_103.setTransform(362.3,168.1,0.368,0.368,79);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_104.setTransform(357.4,171.5,0.368,0.368,79);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_105.setTransform(352.3,174.1,0.368,0.368,79);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_106.setTransform(350.2,179.8,0.368,0.368,79);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_107.setTransform(344.1,183.8,0.368,0.368,79);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_108.setTransform(337,185,0.368,0.368,79);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_109.setTransform(328.6,182.5,0.368,0.368,79);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_110.setTransform(420.6,196.6,0.298,0.298,140.9);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_111.setTransform(417,192.5,0.368,0.368,140.9);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_112.setTransform(414.1,184.2,0.368,0.368,140.9);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_113.setTransform(411.2,176.3,0.368,0.368,140.9);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_114.setTransform(406.7,170.3,0.368,0.368,140.9);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_115.setTransform(400.8,165.1,0.368,0.368,140.9);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_116.setTransform(394.3,160.9,0.368,0.368,140.9);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_117.setTransform(387.3,156.6,0.368,0.368,117.9);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_118.setTransform(380.8,156.6,0.368,0.368,117.9);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_119.setTransform(372.2,155.1,0.368,0.368,99.2);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_120.setTransform(364.7,157.5,0.368,0.368,79);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_121.setTransform(359.6,160.9,0.368,0.368,79);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_122.setTransform(354.7,163.6,0.368,0.368,79);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_123.setTransform(352.5,169,0.368,0.368,79);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_124.setTransform(346.4,173.2,0.368,0.368,79);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_125.setTransform(339.2,174.2,0.368,0.368,79);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_126.setTransform(330.8,171.7,0.368,0.368,79);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_127.setTransform(311.5,139.3,0.368,0.368,3.7);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_128.setTransform(310.1,137.9,0.368,0.368,-22.5);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_129.setTransform(285,153.2,0.255,0.255,-60.8);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#9F5E1A").s().p("AhTBCQgEgZAJgXQAIgTAQgPQAOgMAPgIQAQgKAQgHQATgJAVgEQAUgFASAEQgYACgYAGQgWAGgUALQgSAKgQAOQgTAQgJAWQgIAVABAWIAAAGg");
	this.shape_130.setTransform(283.6,153.5,0.255,0.255,-87);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#D47717").s().p("AiHBoIAAgDQgEgPgKgTIgFgJQgZAIgLAFIgNAIIACgDIABgSIgBgMIgCgCIAAgBQAMgQAUgGIgBAOQAAAKAGADQAFACAOgGQAOgFADAJQgCAVAEAIQACAHAHAEQAFAFAIgBQAHgDAKgLQANgJAUABQALAAAYAFQA1ANA6gIQAXgCAFgOQACgJgJgHQgIgGgKgCIgUADQgMACgHgBQgNAAgGgKQgEgKAGgMIAQgSQAYgZAFgjQADgQgCgPQAKgDANgHQgGAggVAoIgDAGIgNAWIgFAGQgIAMADAHQAFAFAMACIAwAKQAXAFAbASIABABQAZASAJASIABACQgeAEgzgCQg3gCgVgDQgWgDhPANQgZAEgUAAQgUAAgQgEg");
	this.shape_131.setTransform(360.3,240.3,1.022,1.022);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#201F1E").s().p("AAPAaIgagIIgDAAIgDgBQAFgHAHgFIALgHQAMgFAYgIIAFAJQAKASAEAPIAAADQgeAAgQgEgAgZADIgZgFQgHgDgDgCQAFgDAFgEQANgIAggHIABACIABAMIgBARIgBADQgJAAgLgCg");
	this.shape_132.setTransform(340.1,247.9,1.022,1.022);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#F18C24").s().p("AhcBgQgHgEgCgHQgEgIACgVQgDgJgOAFQgOAGgFgCQgGgDAAgKIABgOQALgEAOgBIBDAAQAmADAPgNIACgBQAKgIAKgBQAJgBACgKQACgLADgIIAohJQgBAEAMAGQAJADAMgDQABAPgCAQQgGAjgYAYIgPATQgHAMAFAKQAGAKAMAAQAHABANgCIATgDQALACAHAGQAJAHgCAJQgFAOgWACQg7AIg2gNQgWgFgMAAQgUgBgMAJQgKALgIADIgCAAQgGAAgFgEg");
	this.shape_133.setTransform(357.5,239.5,1.022,1.022);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#2E2D2C").s().p("AgRAHQgEgCgCgCIAJgDQAIgDANgDIAQgCIAAABQABADgBAHIgBAGIgngCg");
	this.shape_134.setTransform(337.3,235.5,1.022,1.022);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#2C2926").s().p("AgEACIgVgIIgDgBIgCgBIAAAAQAaABAbADIAGABIACAJIAAADQgYgCgLgFg");
	this.shape_135.setTransform(341.4,239.2,1.022,1.022);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#D47717").s().p("ABiBSQgsgHgRgFQgRgEhCADQgoACgZgIIAAgDIgCgLQgBgIgEgJQgCgGgCgCIgFABIgRAEIgIADIgKAFIABgDIABgHIABgGQACgIgCgDIAAgCIAAAAQAKgKAOgEQAaAOASAaIAIAMQAIgFADgOICaAhQAKACAIgCQADgBACgEQACgFgCgEQg1ALgQgWQgMgQAIgfQAJgoAWggIADADQAMAGAYgLQgHAZgVAfIgDAEIgNARIgDAFQgIAIACAGQADAEAJADIAnANQARAFAUARIABABQATARAFAPIABACIgHAAQgWAAgkgFg");
	this.shape_136.setTransform(356,233.7,1.022,1.022);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#201F1E").s().p("AAmAMQgdgDgYgBQAEgEAGgEIgQgCQgOgEgGgEIgCgBIAoACIgBAHIgBACIAJgEIAIgDIARgEIAFgBQACACACAGQAEAIACAJIgGgBg");
	this.shape_137.setTransform(340,237.5,1.022,1.022);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#D97C1B").s().p("ABhBRIiaggQgCANgJAGIgIgMQgRgagbgQQAKgDANABQAMABAqAGQAeAFAOgJIABgBQAKgFAHgJIAIgPIADgHIAZg7QAMALALAHQgVAggKAoQgIAgAMAPQARAXA0gLQACADgCAFQgBAEgEABIgIABIgKgBg");
	this.shape_138.setTransform(354.5,231.5,1.022,1.022);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#CDA12D").s().p("Al9BPQAJgHAKgFIgCACIgHAHQBFgsA5gYQBtgrB1gVQA1gKA3gFQBLgJBLgCIAXAAIBWAAIAjAAIABABQgSACgNAFQgZAHgJARQgCAFABAKQAAAGAGAIIgMgBIgugCQg3gEg2gBIhcAAIhQADQg0ADgyAMQglAKgmAMQgdALhFAeQhDAdgPgDQgGAEgFABIACgEg");
	this.shape_139.setTransform(256.9,176.7);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#EFBF41").s().p("AljBUQAhgcAlgRQBHgjBLgRQAugMAsgFQBkgSAtgSIAWgDQAngDAngGQAWgDAVgLIADgBQARgIAOgNIAJgZQAGAHAEAJIALAbQAIARAIAKIAYAUIAoAdIgkAAIhVAAIgXAAQhLAChLAIQg3AGg1AJQh1AVhtAsQg6AYhEAsIAHgHIACgCQgKAFgKAGQAIgfATgZg");
	this.shape_140.setTransform(256.9,170.6);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#B48F2E").s().p("AjWA+QAdgeAmgUIAtgVQAngSAqgLIAxgMIAPgCQAlgHAmgFIA9gFIAvgFQgtAShlASQgtAFgsAMQhMAShGAiQglARghAcIALgOg");
	this.shape_141.setTransform(244,171.4);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#884C0D").s().p("ABdHEQgQgHgUgPIgCgDIgTgSQgUgBgNgHIgXgHQgSgDgWgHQgUgIgMgMQgLgNgigWIgLgHQgYgRgJgNIgDgIIgLAAQgTABgIgBQgKgBgFgDIgFgFQgEgGgEgKQgIgPgMgOIgFAVIgOgMQgFgFgHgJIgKgRIgRgdQgDAOgEAOIgNgTQgMgQgDgLIgEgWQgFgSgUgVIgDAZQAAgBgBAAQAAAAAAgBQAAAAAAAAQAAAAAAAAIgRgmIgFgQQgEgSABgXQgBgTACgaQgyANglgCQg9gCg1glQgngcgVgmQgGgIAAgFQgBgLACgFQAJgQAYgIQAOgFASgBIgBgBIgogdIgYgUQgIgKgHgRIgMgbQgEgKgGgHIAFgMQAxh3CCgZQCNgdBOBwIALASQAvBQgMBHQAIAGAJAGQABABABAAQAAAAABAAQAAABABAAQAAAAABABQAbAMAcgDQAdgGAggcQASgPAhglQBDhEBigcQBcgbBeARIAHACQBgATBRA9QBHA2AvBNQAAAAABABQAAAAAAABQABAAAAABQAAAAAAABIABAAIAIALQAIAQAIAUIABAEIAXBXQANAwAYAhQAdAnAnAHQg/AWgeARQgyAagbAjIgBABIgWAjIgQAYIgJAMQgKANgSAOIgMAJIgTAOIgNAJQhAAogzAXIgdAMQggALgdAFQgqAHgygHQgUANgOAAQgFAAgFgCg");
	this.shape_142.setTransform(357.5,185.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("rgba(255,255,255,0.02)").s().p("AxiSGMAAAgkLMAjFAAAMAAAAkLg");
	this.shape_143.setTransform(324.2,192.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_143).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy13, new cjs.Rectangle(211.9,77,224.6,231.6), null);


(lib.Symbol1copy12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#95590F").s().p("AgbASQgbgCgUgKQgRgIgJgQIAFAAQAVAQAbAHQAmAIApgGQATgBAygHIgXAHQgnANgsAAIgWgBg");
	this.shape.setTransform(366.7,126.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D78C31").s().p("AhnALQgSgOgLgbQAEAAAFgDQAAAHADAHQAEAJAKAHQALAHAcAGQAvAIAeAAQArABAhgOQAKgEAUgLQAJgHAIgCIgPAKIgRAOQgpAdhMAAQg5AAgegXg");
	this.shape_1.setTransform(368.1,128.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B6721E").s().p("AhgALQgbgIgWgOIANgBQARALAXAFQAfAHAlgFQARgCAxgKQApgJAcAAIARAAQAJAAAJADIAAACIgSgCIgJAAQgVABgaAIQgLAFgKACQgyAJgSABQgRACgQAAQgYAAgWgFg");
	this.shape_2.setTransform(371.6,126);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#ECB570").s().p("AgYANQgHgCgCgEQAAAAAAAAQABAAAAAAQAAAAABgBQAAAAAAAAQAGAAAEgDQAFgDADgDQAGgFABgEQADACAGADQAFAAAIADQANAEAFAHIgIgBQgIAAgKADIgPAEIgJAAIgIAAg");
	this.shape_3.setTransform(373.8,120.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E9A857").s().p("AgwAYQgNgDgIgGIAHgBQAOgDAIgEIASgLQAIgLADgLQAIAHANAEIAYAHQAbAIAJAPQgJgCgIABQgMAAgWAFIggAGQgIACgIAAQgKAAgJgDgAgLgDQgDADgGADQgEADgFAAQgBAAAAAAQAAABAAAAQgBAAAAAAQAAAAgBAAQADAEAHACQAHABAJgBIAQgEQAJgDAIAAIAJABQgGgHgMgEQgIgDgGAAQgGgDgCgCQgCAEgFAFg");
	this.shape_4.setTransform(373.7,120.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E59B41").s().p("AgTBFQgfAAgvgIQgcgGgLgIQgKgHgEgJQgDgHAAgHIAJgCQAIAQASAJQATAKAbACQA5AEAxgQIAWgIQAKgDALgFQAagJAVgBIAJAAIASADIACAAIAAAEIABADQgDgDgFAAIgIgBIgFAAQgGAAgGACQgFACgHAEIgGADQgIACgJAHQgUAMgKAEQgeANglAAIgIAAgAhXAgQgXgHgRgLIAOgDQAZgFAQgHQAWgKAPgSQAQgTAEgUQARAMAcALQAPAEAgAHQAkALAUAQQALAHAHAOQgJgDgJAAIgRAAQgcAAgpAKQgxALgRABQgQADgPAAQgTAAgSgEgAACgdIgRANQgIAEgNADIgHABQAIAFAMADQARAFASgFIAhgEQAVgFANAAQAIgCAIADQgIgRgbgIIgZgHQgOgEgIgGQgDAKgIALg");
	this.shape_5.setTransform(371.1,123.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0D0005").s().p("AgRAcQgJgLAAgQQAAgRAJgMQAIgLAKAAQALAAAHALQgFAAgEAFQgCAGAAAFQAAAHACAFQAEADAFAAQAFAAADgCQAAAQgIALQgHAMgLAAQgKAAgIgMg");
	this.shape_6.setTransform(359.9,114.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F0E9DD").s().p("AgjAqQgPgRAAgZQAAgYAPgTQAPgQAVAAQAUAAAPAQQAPATAAAYQAAAZgPARQgPASgUAAQgVAAgPgSgAgPgaQgIAMAAARQAAAQAIALQAIAMAKAAQALAAAHgMQAIgLAAgQQgCACgGAAQgEAAgEgEQgDgEAAgHQAAgFADgGQAEgFAEAAQgHgLgLAAQgKAAgIALg");
	this.shape_7.setTransform(359.7,114.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#D06C61").s().p("AACgPQAAALAEANQgGADgFAEQAFgPACgQg");
	this.shape_8.setTransform(296.1,232.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#85352D").s().p("AjqA1QgDgGgEgCIAHAAQACgBAFgGIACAHQAEAKABAHIgFACQgDgJgGgCgAiQAtIgIgJQAAAAAAAAQABgBAAAAQAAAAAAgBQAAAAAAgBIAIgBIACAAQAEAMAAAJQgEAAgDgIgACXAmIgJgCQAAgDADgBQAFgCABgFQAFAEAAABQAIAIACAHIgCAEQgIgIgFgDgADpAIQgDgEgHgEIABAAQADgDAGgBIAAABQAHAHACAKQgCAAgHgGgAijgPIgHgDIgPgIIACgDQADgFgDgDIgEgEQACAAAFAEQAGADAFAFQAHAEAFAPIgGgFgADGgsQgBgBgGAAIgQgFIgCAAQABAAABAAQAAgBABAAQAAAAAAgBQABAAAAAAQAAgGgEgBQAAgBAAAAQgBgBAAAAQgBAAAAgBQgBAAgBAAQADgDAFADIANAEQAIAEAHALIgHgBg");
	this.shape_9.setTransform(327.8,284.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A34036").s().p("Ai6ExQgHgSgLgLQgPgLgdgHIgtgLQgOgBgGgLQAAgEAEgHQABgHAGgIQAMgTgCgZQAAgNgFgLQADgWgGgSQAnhAgNhBQgCgTgNgeIgUgzQgZhBAIgoIAAgDIAphWIAAAdQAAAVgEATQAHgZAMgWQAAgCADgFIAAAHIAAA4QASgWAIgUIAEA9QgFAGAAAEQAAAHACAMQAGAOAAADQABAFgCAMIgCAEIgMAYQgHAUgBAbIABASIAAAhQAGAkAGA9QgFAOAEARQAEBFAZA/QAFARAHAFQAEABAPAAIBcgBIgCADIAAABQgFABAAADQgEAEACAJQAFAIAIAEQADABAKAAIAPAEQAHADAGAFIABAAQADAJgDAHIgEAAIgBAAIgJABQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBAAQgFACgCAHIgBgFQgIgPgagNQgdgMghgLIgMAAIgBAKQAAAFAEADQAHAIAIAEQACADAGABQAIAGAHAJQgEAGgDABIgHAAIgHAAIgFACQgLgKgMgIgABlEiQgXgHgHgSQgEgIAAgOIADgWQAAgPgDgIQgBgMgJgIQgCgDgEgBQgCgKgEgLIgQgxIAAgGQABhEgdgyQgHgPgagiQgWgbgJgSQgSgmADgsQABgoASgfIAMAwQAHgZAMgaQACAVAAAUQACgLAFgLIAJBDQAEgGAWg9QAGALADAOIgPAOQgQATACAPQAAAIAFAIIALAPQAEAJgCANQAAAIgDARQgBAPACAQQAAAIAEAKQADAOALAiQAMAZASAsQgBArAPArQANAuAfAlQANAQAKADQAIADAMgGIAWgKQATgHAkAAIAWABQAAAGAEAFQAEAFAIAAIARAAIAPAAQAIAAAHAGQAGAHgBAHIgFAAQgGABgDADIgBABQgEAGAAAGQgIgQgigHIhBgDQgJACgDABQgBAGABACQABAGAGABIgIADQghAJgLAAQgKACgIAAQgOAAgMgEg");
	this.shape_10.setTransform(318.7,257.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#913B32").s().p("ADcEYQgegGghAGQgTADgEADQgOABgJgHQgOgKADgfQAAgrgGgZIgBgPQAEABADADQAIAIABANQADAHAAAPIgDAXQAAANAFAIQAGASAYAHQATAHAYgFQALAAAhgJIAIgDQAAAAABABQAAAAAAAAQAAAAAAAAQABABAAAAQAHAFAJADIAKAAQAIACAJAJQgBAFgFACQgDABAAADIgHABIgBACQgTgOgcgDgAimERQgbgQghgCQgSABgFgCQgPgEgHgIQgJgOAJgdQAOgqABgYQAAgGACgEQAFALAAANQABAZgMATQgFAIgCAIQgEAGAAAEQAHALAOABIAtAMQAcAGAPAMQALAKAHASQgIgIgOgGgAC3DsQgJgDgOgQQgegkgOgvQgPgrACgrQAJAYANAcQAjBIAQARQASAQAKAPQA4gNAXgKQAOgFABAAIAEAEQABAAAAAAQABAAAAAAQABAAAAABQABAAAAABQADABAAAGQAAAAAAABQAAAAgBAAQAAABgBAAQAAAAgBABIgBAHQgBAAgBAAQAAABAAAAQgBAAAAABQAAAAAAAAIgWgBQgjAAgTAHIgWALQgJADgGAAIgGgBgAiiDkQgGgFgGgQQgYhAgFhFQgEgRAGgNIADAfQANBRANATQAMAWAEAQQA8ABAWgBQAOAAACACQABAAAAAAQABAAABAAQAAAAAAAAQABABAAAAIADAEQAEADgEAFIhbABQgPAAgFgBgAA1AfQgQg7gfgkQgMgOgLgJIgJgMQgTgVgGgTQgEgUAEgaIAKgkIACgIIAGgVIAMgqIADgDIABAKQgRAfgCAoQgDAsASAmQAKASAVAbQAaAiAIAQQAdAxgCBFIAAAFIgSg3gAkHBEQAHgyAAgfQAAgogJgiIgLgfIgWgqQgLgZABgUQAAgVAKgVIASgdQgDAQgGASIAGgIQgIAnAYBCIAVAyQAMAeADAUQAMBAgmBAQgCgGgEgJg");
	this.shape_11.setTransform(314.5,259.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#6E5A35").s().p("Ag2CCQgQAQgRAYQgFgaAAgGQgKAZgEAEIgBg7QgiAagaAlQgCgigNgdQgRAIgIASIAAgPIAAgDIgBAAQgKAGgMACQAMgIAIgMIACgBIAIgPQALgQAEgTQADgIAAgLQgDALgFAIQgOATgVARIgMALQgWAQgbAIQAWgVAIgcQAFgQAAgRIAAgLQAOgFAMgOQAVgOAOgUQAFgIADgLQAAALgDAJQgEATgLAOQgEAJgGAIQgIAJgMAJQAOgCAJgHQARgHANgMQADgEAFgEQALgNAHgMQAIgPADgSQAGASgGASQAAAAAAAAQAAABAAAAQgBAAAAAAQAAAAAAAAQgGAOgIAKQgEAHgIAFQAQgHAMgIIANgNIAHgHQAMgNAFgWQADAWgDAUIAAACQgBADgDABQgFAbgRAVQALgBAKgGQABgHAEgHIACALQAQgIALgNQAHgHAEgHQAIgJAEgLQAEgMACgMQABAMgBALQgCAMgEANQgBAIgGAHQgMAegYAVIArgSQAEgLACgKQABAJgBAIQAbgPAPgRQAOgTADgVQgBAVgJAWQAAAEgCAEQgNAdgZAWQAOgCAMgEQAMgEALgIIABgIIAAAIIAFgFIAHgHQAKgIAHgLQAMgSAEgWQAIATgHAXIgBAOQgEAEgBAGQAJgGASgFQAFgEAGgCQAMgHAIgLQAHgIADgIQADASgGATQgDATgGASIAvggQAAAAABAAQAAAAAAAAQAAAAAAgBQABAAAAAAQABgFAGgDQAUgZAEgkQAEAggJApQAAAAAAAAQAAAAABAAQAAAAAAAAQAAAAAAAAQAGgEgGAHQAWgTADgKQAJgUgFgUQAJAZgJAtQAFgHAHgFQAWgTAHgLQAOgYAEgdQALAjgoA3QA3gcATgXQAUgVgBgaQAEAVgBAMQAAANgPAYQAPgNANggQANghgFgaQASAbgMA8IgGgMQAFAcgNAhQgNAhgPAMQAPgaAAgLQABgOgEgWQABAdgUAVQgTAWg3AbQAog1gLgjQgEAcgOAYQgHALgWAVQgHAEgFAHQAJgsgJgaQAFATgJAWQgDAKgWARIgBACQABAXgJAXIgag9IgWBMQgHgXgMghQgFAigRAgIgRhDQgMAjgFAkIgWhWIgTAiIAGgWQgPAMgOAHQgMAhguAWIgOAHQAPgkAMg1gAlPBEQAMgQADgSQAFgMAAgLQgFAMgHAIQgEANgKAIQgTAPgWAFQAMgRAHgYIACAAQAegFAXgaQAMgNAIgNQgKAegSAbQAbgJAQgZQALgPAGgRIADgMIAAgNQACAGAAAHIAAAOQAAAPgFAQQgIAcgWAVQAQgDAPgIIgBAEQgGATgLAMQgQAagbAMQASgcAKghQgIAPgMAOQgdAggpACQAdgRAOgagAnuAbIAQghIAOgpIAAgDIAKgiIAEgjIAAAcIAAAoIAAAEQgCAEAAAHQgCAhgKAdQgFAVgJAVQgIAVgLAUQAAgpADgpgAFxALQATghADgHIAFgKQAOAaAEAvQgFgYgNgUQAGARgCAMIgCAMQgEAHgDAJIgXAuQAJg1gIgdgAmRAbIADgBQACgWgBgVQAKgIAHgLIAIgLQALgUAEgZQABAXgEAWIgBACIAAABQgHAXgOATIAZgJQgGAVgJATIgEAFQgCAEgCACQgSAbgdAMQAVgXAFgdgAnCAaIAAgtIAUgcQAAAAABAAQAAAAAAgBQAAAAAAAAQABgBAAgBQARgcAKggQAIAegFAdQAAABgBAAQAAAAAAAAQAAAAgBAAQAAAAgBAAIAAADQgFAdgVAVQAIgFAHgCQgHAPgJAPQgKAQgOAOQAAgQACgOgAn8AdIAHg2QACgMACgKIADAAIgBAOQgCAfAAAfIgLAAgAllgLQAIgGAIgIQAKgIAEgLQABAAAAAAQABgBABAAQAAAAAAgBQABAAAAgBIAIgQIgBAQIgEAEQgDAVgMAQQgKARgMALQABgSgBgPgAGOgsIACgOQACgJgGgSQAIALAGAPIgGgDIAGAIQADADABAAQALAVAFAoQgMgmgUgQgAGdg6QgDgLAAgJQAZAXAEAQQAGAOACAHQgSgbgQgNgAGahOQgIghgRgSQAdARAQAuQgFg3gVgQQAOADARAYIgGADQAEADAHABQABACAEAAIAOABIgDAHQgFAFgIADIAFAJQAIARADAWgAG3h1QgGgQgagZIAwAnIgBABIgOAHIgBgGgAHAiYQgBgGgEgEIANgIIADgHQgSAAgMgGQALgGAOgEQAAgBAAAAQAAAAAAAAQAAAAAAAAQABAAAAAAQAQgNALgLQgEARgMALQAIAHAHgDQAPgIAIgOQgIAXgOARQAOgCAMgMQADAMgHAIQgHAGgLAOQgMAIgPANQgEgTgHgMg");
	this.shape_12.setTransform(312.7,207.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#564523").s().p("AmrCYIgDAAQAhiMBUhNQBJg9B/gZQBtgWBuAPQBxARBhA0QA9AhAXAIQAQAFAPADQgJAMgPAEQgeANglgLQgYgEgpgVQjOhbiXAkQiKAhh/CIQgUAUgRAVIAAgdIgEAkQgWAYgTAaIACgNg");
	this.shape_13.setTransform(306.1,188);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#5F4D2A").s().p("AAuD0QgEALgDALQAAgVgBgUQgNAagGAYIgNgvIgBgLIAPg3IATgiIAWBWQAGgkAMgjIAQBDQASggAEgiQAMAhAHAXIAWhMIAaA9QgLASgUATQgKAIgeAVIgWASQgDgOgFgLQgXA8gEAHIgKhDgAiuDQQgJAVgRAWIAAg4IAAgHQgDAEAAADQgMAWgHAYQAEgTAAgUIAAgdIgpBWIAAADIgGAIQAGgSADgQIAIgOQAeguASg3IAAAPQAIgSAQgIQAOAdABAiQAaglAigaIACA7QAEgEAJgZQAAAGAGAaQAQgYARgQQgNA1gPAkQgMAFgUAHQgdALgPAHQgBAAAAABQgBAAAAAAQAAABgBABQAAAAAAABQgDgOAAgLQgCARgGAPQgEAEgBAEIgEg+gAB/AyQAFgTgDgSQgCAIgHAIQgIALgNAHQgFACgGAEQgRAFgKAGQABgGAFgEIABgOQAHgXgIgTQgFAWgMASQgHALgJAIIgHAHIgGAFIAAgIIgBAIQgLAIgLAEQgNAEgNACQAYgWANgdQADgEAAgEQAIgWABgVQgCAVgPATQgOARgcAPQACgIgCgJQgBAKgEALIgsASQAZgVAMgeQAFgHACgIQAEgNABgMQACgLgCgMQgBAMgEAMQgEALgJAJQgEAHgGAHQgLANgRAIIgBgLQgEAHgCAHQgJAGgLABQAQgVAGgbQACgBACgEIAAgBQADgUgDgWQgGAWgMANIgHAHIgMANQgMAIgRAHQAIgFAFgHQAIgKAFgOQABAAAAAAQAAAAAAAAQABAAAAgBQAAAAAAAAQAFgSgFgSQgDASgIAPQgHAMgLANQgGAEgCAEQgOAMgQAHQgKAHgOACQANgJAIgJQAFgIAEgJQALgOAEgTQADgJAAgLQgDALgFAIQgOAUgUAOQgMAOgOAFQAAgIgDgIQAAALgBAKQgPAIgRADQAWgVAIgcQAGgQAAgPIAAgOQAAgHgDgGIAAANIgDAMQgFARgLAPQgQAZgcAJQASgbALgeQgIANgNAMQgXAbgeAFIAAgBIAEgIQANgLAJgRQAMgQADgVIAEgEIACgQIgJAQQAAABAAAAQAAABgBAAQAAAAgBABQgBAAgBAAQgEALgJAIQgJAIgIAGIgYAJQANgTAHgXIAAgBIACgCQAEgWgCgXQgEAZgLAUIgIALQgHALgJAIIgEgSQgFAQgGAPQgHACgIAFQAUgVAGgdIAAgDQAAAAABAAQAAAAABAAQAAAAAAAAQAAgBAAAAQAGgdgIgeQgKAggSAcQAAABAAABQAAAAAAAAQAAABgBAAQAAAAAAAAIgVAcIAAgWIgBAFQAAgHABgEIAAgEIAAgoQARgUAUgVQB/iICKghQCXglDOBcQApAUAYAEQAlALAegMQAPgEAJgNIAdAAIAMgCIACgCIgCAAQAEgCADAAQABAAAAAAQAAAAABgBQAAAAAAAAQAAAAABgBQAJgJAKgSQgEAUgMAPQAEACABAEQAOgMAFgOQgBAUgIAOIAEADQAQgPALgIQgDAWgFAYQAQgJAMgOIgCAFQgLALgQANQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAAAAAABQgOAEgKAGQAMAGASAAIgDAHIgOAIQAEAEACAGQAHAMAEATQgBAAgBAAQAAAAAAABQgBAAAAAAQAAAAAAABIgBAAIgwgnQAaAZAFAQIABAGIgFABQgQgYgOgDQAUAQAGA3QgQgugdgRQAQASAIAhQAAAJADALQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgFgPgIgLQAFASgBAJIgDAOQgFgHgHgBQAFAFAGAIIgGAKQgCAHgUAhIAAgBQANg8gSgbQAEAagMAhQgOAggPANQAPgYAAgNQABgMgEgVQACAagVAVQgTAXg3AcQAog4gLgiQgEAdgOAYQgHALgVATQgHAFgGAHQAKgtgKgZQAGAUgKAUQgDAKgVATQAFgHgFAEQAAAAAAAAQAAgBgBAAQAAAAAAABQAAAAgBAAQAKgpgEggQgEAkgVAZQgFADgCAFQAAAAAAAAQAAABAAAAQAAAAgBAAQAAAAAAAAIgwAgQAHgSADgTgAnpgfQATgaAWgZIgJAiIAAADIgOApIgQAhIgDACQAAgfABgfg");
	this.shape_14.setTransform(312.1,207.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#BDB8B8").s().p("ABABMQgMAAgOgJIgYgTQgvgmgegSIgLgKQgVgagNgfQAFAEAHAIQATASAZASQAGAFAJAEQAUAOAsAXQATALAJACQAQAIAOgBQALAAAJgFIAEAaQgUAQgTAAIgGAAg");
	this.shape_15.setTransform(348.1,131.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CCCCCC").s().p("ABhGmQgIAOgPAIQgHADgJgHQANgLAEgRIACgFQgMAOgQAJQAFgYADgWQgLAIgQAPIgEgDQAIgOACgUQgGAOgNAMQgCgEgEgCQAMgPAFgUQgKASgKAJQAAABgBAAQAAAAAAABQAAAAgBAAQAAAAgBAAQgCAAgDACQAYgcAMghIADgIQAIgkgFgqQgCgegJgnIgDgLQgbhcg0htQgOgegXgtQgQgKgMgNQAeATAvAmIAZATQAOAJALAAQAWACAXgSQADAUAGAVIADARQgCAiAFArQAEAmAaBkQAYBTAAA8QgCA7gWAsQgMAMgOACQAOgRAIgXgAAChjQgIgCgTgLQgtgYgUgOQgJgEgGgFQgUgWgPgaQgPgagIgcQALg4ALgVQAKgVAGgTQAGgQAWgWQASgYAegMQAigMAgAEQAhAFAYAZQAQASAPAiQAZA5AAADQAIAVARAJIAJADQgCBOgxA4QgRAWgWAMQgKAHgMAFIAAAIQgKAFgLAAIgDAAQgMAAgOgHg");
	this.shape_16.setTransform(353.5,144.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#C4C0C0").s().p("ABhHlQALgNAHgGQAHgIgDgMQAWgsABg7QAAg8gXhUQgahjgEgnQgGgqADgjQAMAnAYBCQAQAtAKAiIAMAuQAPBEgHA5QgFBCgnA1QgLAOgPANQgNAOgPAIQAPgNAMgJgAiEjFQgHgJgFgDQgIgUgDgXQgEgSAAgUIAAgNIAFgfQAJg0AignQgKgFgLgBIgWgEQALgDANAAQAPgBANAEIAOgPQgPgGgQgBQgLgFgGAAIASAAQAWADAWABQATgOAWgGQgPgGgRgDIgfgEQgkgHgxgBIAMgFIAOgBIBJgBQAMAAAIABQAtAGAgAPQBEAAAwA2QAiAmAKAyQAFASACAVIAAAMIAAADIgKgCQgQgJgIgVQAAgDgZg5QgPgjgRgSQgXgYgigFQghgFggANQgeAMgTAYQgWAWgFAQQgHATgJAUQgLAWgLA4QAIAbAPAbQAPAZATAWQgZgRgTgSg");
	this.shape_17.setTransform(351.7,145.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(255,255,255,0.02)").s().p("AxiSGMAAAgkLMAjFAAAMAAAAkLg");
	this.shape_18.setTransform(324.2,192.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy12, new cjs.Rectangle(211.9,77,224.6,231.6), null);


(lib.Symbol1copy11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#010101").s().p("AhkAgQgLgJgDgOQgEgMAGgMQADgLAKgCQAKgDAKAGIgEAAQgFAAgEADQgFAFAAAGQAAAGAFADQAEAEAFAAQAGAAAEgEQAEgDAAgGIAAgCIAEAKQADAMgDAMQgGAMgKACIgGABQgHAAgGgEgABcAEQgHgEgCgJQgDgJADgHQAEgIAHgCQAFgCAFAFIgBAAQgBAAgBAAQgBAAAAAAQgBABAAAAQgBAAAAABQgEADAAADQAAAEAEADQABACAEAAQADAAADgCQADgDAAgEIAEAGQABAJgBAIQgEAHgIACIgCAAQgFAAgFgEg");
	this.shape.setTransform(350.8,119.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DBDBDB").s().p("AhFBIQgegkAAgqQgCgqAZgjQAKgNAMgHIgCACQgXAfACAnQACAoAYAdQAZAeAiACQAkACAZgdQAYgdAAgjIAAgPQAIATAAAUQAAAdgPAZIgNALQggAggaACIgSAAQgJAAgHACQgcgIgWgYg");
	this.shape_1.setTransform(342.3,119.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DADADA").s().p("AAWAuQAIgDAHgJQAEgIAFgKIADgJIABAEIAHATQAEATABAUQgcgSgMgFgAg8gdQABgLADgIQAGgPAHgFIAAACIAAATQAEApAQAcQADAGAGAGIgDAAQgUgEgSAEQgJghAEgeg");
	this.shape_2.setTransform(361.7,118.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhJBcQgjgCgZgeQgZgdgCgoQgCgnAXgfIACgCQAWgNAdAEQAuAGAeAjQAOAQAGATIABAOQAAAkgYAdQgXAbghAAIgEAAgAhggJQgKADgEAKQgFALADAOQAEAOAKAIQAJAHAKgDQALgDAFgLQAEgMgEgOIgEgJIAAACQAAAFgDADQgFAGgFAAQgGAAgEgGQgEgDAAgFQAAgHAEgDQAEgDAGAAIADAAQgHgFgGAAIgGABgABhApQgGgGgDgGQgQgdgEgoIAAgTIAAgCIAIgCQAWgJAZAYQAXAYAPAmIgDAJQgFALgEAHQgHAJgIAEQgUgJgRgEgABhgPQgIACgDAIQgDAFADAKQACAJAGAEQAHAGAGgCQAHgCAEgHQACgIgCgKIgEgEQAAADgCACQgDADgEAAQgEAAAAgDQgEgCAAgDQAAgEAEgDQAAAAAAgBQAAAAABAAQABAAAAgBQABAAABAAIACAAQgEgDgEAAIgCABg");
	this.shape_3.setTransform(350.5,117.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#4C2022").s().p("AAAAPQgGgDgDgFQgEgHAAgFQAAgHAGgDQAEgBAEABQAGAEAEAHQAEAEgBAHQgBAGgEACIgFABQgBAAAAAAQgBAAgBAAQAAAAgBAAQAAgBAAAAg");
	this.shape_4.setTransform(360.7,132.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0B010A").s().p("AAAAVQgHgFgGgHQgFgJAAgHQAAgJAHgEQAFgDAJADQAGAEAGAJQAFAIgCAJQAAAHgFAEQgEACgDAAQgEAAgCgCgAgHgOQgGADAAAGQAAAGAEAGQADAGAGACQAEADAFgDQAEgBABgHQABgGgEgFQgEgGgGgEIgEgBIgEABg");
	this.shape_5.setTransform(360.7,132.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000300").s().p("AA8AiQgUgMgOgJQgNgJgVgIQgWgIgfgFQgWgCgUgJQAPgUAUgNQAUgLAWAGQAWAHASANQATAOAPASQAMAPAMARIAaAqIAGAJQgVgVgXgOg");
	this.shape_6.setTransform(354,106.2,0.552,0.552,-7.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000300").s().p("AA8AiQgUgMgOgJQgNgJgVgIQgWgIgfgFQgWgCgUgJQAPgUAUgNQAUgLAWAGQAWAHASANQATAOAPASQAMAPAMARIAaAqIAGAJQgVgVgXgOg");
	this.shape_7.setTransform(334,105.9,0.563,0.563,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("rgba(5,28,79,0.2)").ss(1,1,1,3,true).p("AGvqqQgCgKgCgJQgKgfgMgbQgnhOhJgoQhAgjhJgDQg1gCg7APQiJAigiCOQgjCNBDB6QBFB8gYBeQgKAwgQAqQg1ARglAvQgjAugGA2QAAACAAABQgVAjgLAPQgLAVgEAHQgIARgEANQgFATAAAaQAAACAAACAGvqqQAGADAGAFQBKA2BFBRQBEBQAeA4QAeA5gbAcQgLAMgVADQgLABgOgBQgXgDghgJQgjgJgngTQgNgFgKgGQgIgEgHgCQgLgHgMgFQgOgIgOgIQgcgPgcgRQgWgNgTgOQgYgSgWgQQAAgBgBAAQgXgVgUgSQgjglgVgmQgDgDgBgDAEClLQgHgDgJgDQgegPgMgSQgQgXgCgsQAAgzgHgSQgFgLgMgUQgMgUgFgLQgGgRAEgSQAAgCABgBIAAgBQAGgQAOgFQAHgEALAAQANACAFAAQAbgCAgghQAmglASgHQAXgKAYAFQAiAGAnAaAG3l0QAQAjAEAkQABAFAAAGQADAlgIAHIAAAAQgGAFgGAAQgKAAgGgEQgCgBgDgDQgEgCgDgEQgEgDgEgEQgBAAAAgCQgXgagTgbQgVgdgTgdQgbgtgYgwQgCgHgDgGAGojiQgbgIgdgYQgNgJgWgTQgIgGgGgGQgLgIgLgHQgEgCgGgCQgLgGgSgIQgFBWAtBdQAGALAGALQBCB5gKCjQgLChhkBkQgaAagbAVQgOALgQAKQgTALgSAHQgdAPgfAEQgJAAgJABQgIABgGAAQgSASgVANQgqAbgwADQgLABgMgBIgBAAQACAdAPAdQAOAaAYAiQAWAdAWAPQAGADAEACQAUAKArACQAXABASADQAOADAJAEQANAEAHAGQAHAFAEAJQAFALgEANQgBACgBADQgDAIgEAEQgCABgBACQgHAHgKAEAGojiQgJgEgKgIQgXgSgWgaQgFgGgIgLQgVgcgng7Qgng+gYhJAktGZQAGgBAFgCQA/gQA8gVQAjgOAhgOQAxgVAggTQAogZAdgfQA3g5AdhiQAXhVgSg4QgOgrgjgdQgOgLgSgJQgigSgggEQgTgBgSACQgJABgKADQgHABgGAEAHOloQASAmAAAtIAAACQgBARgEANQgCACgCACQgFALgJAEIgBAAQgBABAAAAQgGABgGABQgKAAgJgDABnFgQgCA/gRAsQgDADAAADQgCAHgEAEQgMAbgUATAHfLPQAAANgQAHQgIAGgLACQgHACgHAAQgVAAgSgIQgUgJgNgPQgDgEgQgaQgHgNgIgIQgFgEgFgEQgEgEgKgCQgDAAgEgBIgCAAQgGgCgJAAQgOACgbADIgCAAQgfACgZgIQgJgCgEgCQgHgDgDgGQgDgGACgKQADgHAIgGQAKgIAWgBQAMgBANACQAVABAVAHQgBgEgEgEQgMgOgWgMQgBAAgXgLQgOgHgJgHQgPgKgPgTQgCgCgEgGQgCgDgEgGADKHOQABAKAEAIQAOAkAdAcQAXAXAbALQAWAJAYADQAOACAdgBQAIAAAJgBQAbgCASACQAJAAAGABQADAAAEABQAfAGAKATQAEAKgDAJQgBAFgCADQgBABgBABQAHAFADADQACACACADQACAFAAAGQAAALgGAIQgFALgJAGQgJAHgNAEQgDACgCAAAFYKBQAmAMARAFQAQAGAMAGQAOAGAKAFQAPAIAFAJQAIAJAAAJQAAABAAACAgJMMQAzAKAaAMQAVAJAQAMQAOAMAKARIACAEQAEAHACAIQAEARgKALQgDAGgJADQgCAAgCACQgJACgNgCQgGgCgGgDQgNgEgLgIQgBgCgBgCQgKgIgJgNQgJgQgFgHQgHgNgKgJQgHgJgIgFQgQgMgQgBQgIgCgEAAAiTI6QgIAgANAlQAJAcAdAwQAJANgEAJQgBACgEACQgCAAgCACQgFACgHgCQgJgCgQgEQgRgDgGgCIgCAAQgcgGgTANQgCACgDAEQgFAFgCAKQAAACAAAEQAAAKAFAGQAFAGAKACQACACAEAAQAFAAAFACQAPAAASAAQANgCAOgCQAXgCAIAAQAJAAAGAAQAHACAFAEQALAFAIALQADAGADAHQAEAFACAGQAOAbAYAgQAGAIAGAFQAHAIAJACQAKABAIgDQACAAAAgCIABAAQAKgGADgJQABgCAAgCAFjJrQAXgDAaABQANAAANACQARAEATAHQAQAFAUAHQAEACAEACAikgrQgLAJgNAQQgHAJgMASAhZAmIgoAuIgEACQgQATgEAJQgEAGgDAJAhwgaQghAkgMAyQgCALgBALQgCAXACAYQgEALAAAMQAAAWALAZQgNAcACAXQAAAPAHALQAGAFAFAEQgJASgHATQgIAQAAAOAgyC0QglAbgdAjQgIALgJALQgGAKgHALAhKBrQgjAYgYAiQgJAMgHANQgCAFgCAEAitBJQgdANgbAcQgDAEgFAFIgBAAQgPAUgSAaQgSAZgEARQgIAUAAAiAjBCYQgvArgUAVQgEADgDAEQgTAVgPAVQgIANgGALQgLATgDASAk0HEQAAgEABgFQABgLADgOQACgFAAgEAjLECQgYAMhCAkQgDACgDACQgJAFgKAFQgGADgFADQgQAIgIAJQgMAPgHAdQgDANACAFQAEAKARAGAlVGlQAGgCAEgCQALgEACAAQAJgBAIgDAkZHdQg2AhgoAhQgBABgCABIgCAAIAAABQguAlggAXQgFAGgFACQgZASgPAJQgOAHgTAJAkZHdQgEgCgDgBQgIgEgFgEQgDgDgCgDQgBgEgBgEAp5KEQASgFAUgIQAJgCAIgEQACAAABgCQANgEANgHQAtgXA+gqQAagUAVgPQAhgYAVgOQARgMAQgKAjvHoIgBAAQgGgBgHgCQgEgBgDAAQgCgBgCAAIgCAAQgJgEgGgCAkKHjIAAABQgBACgEAEQgYAagyAxQgRAPgNANQgoAlgFABQgCAAgCADQgFAEgJAHQgMANgLAFQgLAEgMABQgOACgQgCQgFAAgHgDQgHgCgIgGQgGADgKAFQgFADgGACQgWAHgRgMQgNgHgJgJQgCgDgCgCAigH3QgJAAgJgCQgKAAgLgEQgHAAgHgBQgLgEgPgEQgcAqgcAhQgBACgDADQgbAigOAKQgOALgcAJQgEACgDABQgFABgFABQgNABgMgEAigH3QAAgCAAgCQAAgDgCgGQgEgbACgPQAEgWAOgRQAPgTAWgFAiTI6QgGgDgEgIQAAgGAAgIQAAgBAAgNQAAgHgCgMQAAgFgBgEAlZGnQhBAWhoAkQgMAEgOAFQgmANgMAFQgIAEgHAEQgRAHgOAJQgYARgPATQgRAXgGAaQgEATAKAJQAHAIAUgEQADAAAFAAQALgDANgCAhuJHQgEAAgGAAQgDgBgCgBQgIgBgEgCQgHgEgDgE");
	this.shape_8.setTransform(324.2,192.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#030917").s().p("AhNBJQADgDACAAQAEgBA2gzIAOgPQA2gwAWgbIACAAQgdApgcAgIgDAGQgbAhgOAKQgOALgbAKIgIACIgFAAg");
	this.shape_9.setTransform(292.5,249);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FAC02B").s().p("AiHDcQgLgFgKgQIgNgcQgKgSgIgHQgKgJgLgCIgHgNQgIgLgKgFQgGgEgHgCIgPAAIgeACIgcAEIggAAQgGgCgEAAIgBgMIgCgKIACgEQAAgEACgBQAEgEAIAAIAMACQAjAHAjgEQALgBAEgCQAHgCAEgGQADgCAAgEQACgIgCgLQgDgVgag6QgQglgCgcIAGACIAKAAQACAdAPAdQANAaAYAhQAXAdAWAPIAKAFQAUAKAsACQAXABARADQAPADAIAEQANAEAIAGQACAOgDAHQgDAHgNAIQgLAEgOAAQAPAMAKARIACAEQAEAHABAIQgBACgEAAQgGACgLACQgPACgLAAQgOgCgLgHQgFgDgGgIQgDgDgHgLQgNgTgNgDQgFgCgHACQgHgJgIgFQgQgMgRgBIgMgCIAMACQARABAQAMQAIAFAHAJQAKAJAIANIAOAXQAJANAJAIIADAEQAAAAAAABQAAABgBAAQAAABAAAAQAAABgBAAQgDAJgKAGIgBAAQAAAAAAABQAAAAAAAAQgBABAAAAQAAAAgBAAQgKgCgHgEgAhaByQAVAJAQAMQgQgMgVgJQgagMgzgKQAzAKAaAMgADqAZIgOgPQgIgKgFgEQgNgNgTgDIgJgBIgKgIQgFgEgJgCIgIgBIgBAAIgGgHQgJgGgLgCQgEgBgKABQgJACgGgCQgHgBgGgIQgFgIAEgDQAXAFAfACQAKAAAGgEQAGAAACgDQADgEAAgCQAAgFgDgFQgEgHgLgGQgtgYgdgZQgcgagMgdQAQgKAOgLQACAKAEAIQANAkAdAcQAXAXAcALQAWAJAXADQAPACAcgBIARgBQAcgCARACIAPABIAHABQAGAGgCAMQgCAJgJALQgSgHgSgEQgNgCgNAAIgIAAIgHAAIAAAAIAAAAQgSAAgQACQAQgCASAAIAAAAIAAAAIAHAAIAIAAQANAAANACQASAEASAHQARAFATAHIAIAEQAIAFADADIADAFIgHABIgRAHIgRAHIgLAAQABANgBAGIAAAEQgGgJgPgHQgKgFgOgGQgMgGgPgGIg4gRIA4ARQAPAGAMAGQAOAGAKAFQAPAHAGAJQgCAMgHAGQgMAMgSAAQgVAAgTgPg");
	this.shape_10.setTransform(340,261.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E2AA18").s().p("Ah9DYQgJgCgHgHIgNgOQgYgfgOgcQgCgGgEgEQAMACAKAJQAHAGAKATIANAcQAKAPALAGQAIADAKACQgGADgHAAIgFgBgAg2DTIgMgGQgNgEgLgIIgCgEQgKgIgJgNIgOgXQgHgNgKgIQAGgDAGADQANADANASQAGALAEAEQAFAHAGAEQALAGAOACQAKAAAQgCQALgBAFgCQAEAAACgCQAEAQgKAMQgDAFgJAEQAAAAgBAAQAAAAgBAAQAAABgBAAQgBAAAAABIgKABIgMgBgAgqB8QANAAALgFQANgHADgIQADgGgCgPQAGAGAEAJQAFAKgEANIgCAGQgDAHgEAFIgCACQgHAIgKADQgKgQgOgMgAllBbQgKgCgFgHQgFgFAAgKIAAgHQACgJAFgGIAFgFQATgNAcAFIACAAIAXAGIAZAGQAHABAFgBIAEgCIAFgEQAEgJgJgNQgdgvgJgdQgNglAIgfQADAEAHADQAEADAIABQACAcAPAlQAaA6AEAVQACALgCAHQAAAEgEADQgEAFgHACQgEACgLACQgjAEgigIIgNgCQgHAAgEAEQgDACAAAEIgCADIACAKIABAMQgEAAgCgBgADzAqQgUgKgNgOIgTgeQgHgNgIgHIAKABQATADANAMQAEAEAJALIAOAOQASAQAVAAQATAAAMgMQAGgHACgLQAIAJAAAJIAAADQAAAMgQAIQgIAFgLACQgHACgHAAQgVAAgSgHgAFLARQAAgJgIgJIAAgDQACgHgCgOIALAAIASgGIAQgIIAIgBQACAFAAAHQAAALgGAHQgFAKgJAHQgJAHgNAEQgDACgCAAIAAgDgAFDgBIAAAAgAAdgzQgJgBgEgCQgHgEgDgGQgDgGACgJQADgIAIgFQAKgIAWgBQAMgBANACQAVAAAVAIQgBgFgEgDQgMgOgWgMIgYgLQgOgIgJgGQgPgKgOgUIgGgHIgGgJQARgIATgLQAMAeAcAaQAcAYAuAYQALAHADAGQAEAFAAAGQAAABgEAEQgCAEgFAAQgHADgKAAQgegBgXgGQgFAEAGAHQAFAIAIABQAFACAJgCQALgBADABQALACAKAHIAFAGQgGgCgJAAIgpAGIgCAAIgMAAQgYAAgUgHgAFig4QgUgIgQgEQAJgLACgJQACgMgGgHQAfAHAKATQAEAJgDAJIgDAJIgCACIgIgEg");
	this.shape_11.setTransform(339,262.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F792BC").s().p("Aglg0QAdASAbAOQAPAjADAmQgwgtgag8g");
	this.shape_12.setTransform(363.4,156);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#480518").s().p("AgIBGIgFgEIgHgGQAHgHAEgIIABgEQAEgNgBgWIAAgGQgDglgQgkIAbARQAQAiAEAkIABALQADAlgIAHIAAAAQgGAFgGgBQgJABgGgEg");
	this.shape_13.setTransform(367.8,161.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0D162A").s().p("AkDGhQgFAAgHgDQgHgDgIgFIAhgRQAPgIAZgTQAFgCAFgFQAggXAuglIAAgBIACAAIADgDQAoggA2ghQAGABAJAEIAAABIgFAHQgtAbgjAcQgsAkgfAYIgDABQggAYgSAKIgfARQAPAKALABIgPABIgPgBgAipF/QACgDACAAQAFgBAoglIAegdQAygxAYgZIAFgHIAAgBIACAAIAEABIAGACIANADQgVAbg2AxIgPAPQg3AzgDABQgCAAgDADIAFAAIgKACIgGABQgKAAgJgDgAkmFoQAtgXA+gpQAagVAVgOQgoAUgaAKQg4AUgwAAQgVAAgLgHQgKgKAAgTIBdguICpg6IAEgCIAKgDQgJgFgDgHQgCgGADgNQAFgdANgOQAIgKAQgIIAHgTIAIgSIAOgYQAAgXAEgRIADgMQAFgSAQgZIAAgEQAAgYAEgRIABgBIAIgIQAbgcAdgOQgdAOgbAcIgIAIIABgDQAEgNAIgQIAPgdQALgPAUgjIABgDQAEgaAJgaQgLAIgNARIgTAbQAGg2AjguQAlgvA1gSQAGgEAHAAQAKgDAJgBQASgDATACQAhAEAiARQASAJAOALQAEAYACAZQAJBzg3BlQgyBahfBGQgUAQgXAPQABgGACgFQAIgTAJgRQgGgEgDgGQgEgHgEgFIARgVQAdgjAlgbQglAbgdAjIgRAVQgCgGAAgHQAAgXANgdQgJgVgCgMQAYgiAjgYQgjAYgYAiQgBgFABgHQAAgMAEgMQgEgYAEgVQAAgMAEgMQAJgsAEgrQghAlgMAyIgDAWQgCAXACAYQgEAMAAAMQAAAWALAZQgNAbACAXQAAAOAHALIALAKQgJARgHATQgIAQAAAOQg3AfhAAXIgLADQgIADgJACIgNAEIANgEQAJgCAIgDQAAAFgCAEQgDAOgBALIgBAKIACAHIAFAHIANAIIAHADQg2AhgoAgIgDADIgCAAIAAABIgcANQhVAvhSAQQANgDANgIgAhWDfIg2AmIA2gmIAhgWIghAWgAgmA3IBZgwIhZAwgAgFgjIgHAIIAHgIQATgVAvgqQgvAqgTAVgAB9inIAogugAgaDiIAAAAg");
	this.shape_14.setTransform(298.7,218);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F57BAE").s().p("AA6BqQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBQgXgagTgbQgUgdgTgcQgbgtgYgwIgFgNIAuAiQATAOAVANQAZA8AyAtIAAAFQABAXgEAMIgBAFQgEAHgHAHIgIgHg");
	this.shape_15.setTransform(359.1,155.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F0B244").s().p("ABICSQgJgEgKgIQgXgSgWgaIgMgRQgVgcgng6Qgng+gYhJIArAnIABABIAFANQAYAwAbAtQATAcAUAdQATAbAXAaQAAAAAAABQAAAAAAAAQAAABABAAQAAAAAAAAIAIAHIAHAGIAFAEQAGAEAKAAQAGAAAGgFIAAAAQAIgHgDglIgBgLQgEgkgQgjIAXAMQASAmAAAtIAAACQgBARgEANIgEAEQgFALgJAEIgBAAIgBABIgMACQgKAAgJgDg");
	this.shape_16.setTransform(359.4,155.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#182138").s().p("AnMMTQgNgHgIgJIgFgFQATgFATgIQAJgCAJgEQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQBSgRBVgvIAbgNQguAlgfAXQgGAGgEACQgZASgPAJIghAQIgQAIIgLAFQgIADgIAAQgNAAgLgIgAlrMLIAggQQARgLAggYIADgBQAggYAsgkQAigbAtgcQgYAagyAxIgdAcQgpAlgEABQgCAAgDADIgOALQgMANgLAFQgKAEgNABQgKgBgQgKgAohL/QgKgJAFgTQAFgaASgXQAPgTAYgRQANgJARgHIAQgIQAMgFAmgNIAZgJIhdAuQAAASAKALQALAHAWAAQAwAAA3gUQAbgLAogUQgWAPgaAUQg9AqguAXQgNAHgMAEQAAAAgBAAQAAABAAAAQgBAAAAAAQgBABAAAAQgJAEgJACQgTAIgTAFIgYAFIgIAAIgMABQgKAAgFgFgAmrLpIAAAAgAAmLBIgBAAIgKAAIgGgCQgHgBgFgCQgGgEgDgEQgFgDgEgIIAAgOIAAgOIgCgTQAAgFgCgEIAAgEQAAgDgCgGQgDgbABgPQAEgWAOgRQAPgTAWgFQAIAFgQATQgOARgEAWQgBAPAEAbIADANIABAcIAAADQAIAGAJACQASACAdgMQBKgbAogXQASgLARgMQgDADgBADQgCAHgEAEQA1gHAwgrQAtgnAhg/QA4hwAFh6QACg3gNh8IgFg/IALAWQBDB4gLCkQgLChhjBkQgaAagcAVQgOALgQAKQgSALgSAHQgdAPggAEIgSABIgOABQAUgTAMgbQgMAbgUATQgRASgWANQgrAbgvADIgLABIgMgBgAjNIfQgRgGgDgKQgCgFADgNQAGgdAMgPQAIgJAQgIIALgGIAUgKIAFgEIgFAEIgUAKIgLAGQAEgSALgTQAFgLAJgNQAPgVASgVQgSAVgPAVQAAgiAHgUQAFgRARgZIAiguIgiAuIAAgEQAAgaAGgTQAEgNAIgRIAPgcQALgPAUgjIAAgDIATgcQANgQALgJQgJAagEAbIAAADQgVAjgLAPIgPAcQgIARgEANIgBADIAAAAQgEASAAAYIAAAEQgSAZgEARIgEAMQgEASAAAYIgOAYIgIARIgGAUQgQAIgIAJQgNAPgGAdQgDANACAFQAEAHAIAFIgKAEgAgWHaQAAgOAHgQQAIgTAIgSIANgVQAEAEADAIQAEAFAFAEQgJASgHATQgDAFgBAFQAXgPAVgPQBfhHAxhbQA4hlgJhyQgCgZgFgXQAjAcAOArQATA5gXBVQgdBig4A5QgcAfgqAZQgfATgxAVIhDAcQg8AVg/AQQBAgXA4gfgAgJGOQgIgLAAgPQgBgXANgcQgMgZAAgWQAAgMAEgLQgCgYACgXIAEgWQALgyAgglQgEAqgJAtQgDAMAAAMIgEACQgQATgDAJQgEAGgEAJQAEgJAEgGQADgJAQgTIAEgCQgEAVAEAYQgEALAAAMQgBAHABAGQACAMAJAUQgOAeABAXQAAAHACAHIgNAVIgKgJgAgCE4IgDAJIADgJQAHgNAJgMQgJAMgHANgAAOGCIAAAAgAASDOIAAAAgABhh7QAXhehEh8QhDh6AiiNQAjiOCJgiQA7gPA2ACQBJADA/AjQBJAoAnBOQghgLgzgUQhDgagjgKQhMgThEAPQhOASgkA4QgcArgCA8QAAAvAOA/QAKAsAaBZQAVBPADA5QADA/gQAuQgSgBgTACQgJABgKADQgGABgHAEQARgqAKgwg");
	this.shape_17.setTransform(309.4,180.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#1D2841").s().p("AisKyQgJgCgHgGIAAgDIgCgcIgDgNQgDgbAAgPQAEgWAPgRQAQgTgJgFQgWAFgPATQgOARgEAWQgCAPAEAbQACAGAAADIAAAEIgSgCQgKAAgLgEIgOgBIgagIIgBAAIgNgDIgHgBIgEgBIgCAAQgJgEgGgCIgHgDIgNgIIgFgGIgCgIIABgJQABgLADgOQACgFAAgEIALgDQA/gQA8gVIBEgcQAxgVAggTQApgZAdgfQA2g5AdhiQAXhVgSg5QgOgrgjgdQgOgLgRgJQgigRghgEQAQgugDg/QgEg5gUhPQgahZgKgsQgOg/AAgvQACg8AcgrQAkg4BNgSQBDgPBMATQAjAKBEAaQAyAUAiALQAMAbAKAfIAEATQgngagigGQgYgFgXAKQgSAHgmAlQggAhgbACIgSgCQgLAAgHAEQgOAFgGAQIAAABIgBADQgEASAGARQAFALAMAUQAMAUAFALQAHASAAAzQACAsAQAXQAMASAeAPIAQAGQgFBWAtBdIAGA/QANB8gDA3QgEB6g5BwQggA/gtAnQgwArg1AHQAEgEACgHQAAgDADgDQgQAMgSALQgoAXhJAbQgZAKgQAAIgHAAgAAXJfQARgsACg/QgCA/gRAsg");
	this.shape_18.setTransform(330.3,178);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FEA810").s().p("AgRDBQgNgKgWgTIgOgLQgHgagLgLQgPgQgUgFIgXgFQgMgFgHgFQgOgMgCgYIACgqQAAgvgbgxQgOgagdgiIAAgBQAOABAKADQACAEADADQAVAmAiAkQAYBIAoA+QAnA7AVAcIANASQAVAaAWASQAKAHAKAEQgbgHgdgYgAC8CHQgigJgogSIgXgLIgPgHIgXgMIgbgQQgbgOgdgSQgWgNgSgOIgvghIAAgBIgsgnQgigkgVgmQgDgDgCgEIAYADQAhAEAfgHQAigJAWgUIATgTQALgLAJgFQAQgLAVADQASABASAKQAPAJAPANIAcAcIA4BEQAgAmATAiQA1BWgIBSIgBABQgXgCghgKgAjdiXIAAAAg");
	this.shape_19.setTransform(362.7,147.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#EE9E11").s().p("Ah5DBIgJgDQgLgHgTgHIgPgHQgfgPgMgSQgQgWgBgsQAAgzgIgTQgEgKgMgTQgMgVgFgLQgGgQADgTIABgCQAdAiAOAaQAbAwAAAwIgCAqQACAYAOAMQAHAFAMAFIAXAFQAUAFAPAQQALALAHAaIgWgQgADTDLIABgBQAIhSg1hXQgTghgggmIg4hEIgcgcQgPgNgPgJQgRgKgTgBQgVgDgQALQgJAFgLALIgTATQgWAUgiAJQgfAHghgEIgYgDQgKgDgOgBQAHgRANgFQAIgDALAAIARACQAbgCAhgiQAlgkATgIQAWgJAZAFQAhAFAnAbIALAHQBLA3BEBRQBEBOAeA5QAeA4gaAdQgLAMgWACIgMABIgNgBg");
	this.shape_20.setTransform(366,142.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(255,255,255,0.02)").s().p("AxiSGMAAAgkLMAjFAAAMAAAAkLg");
	this.shape_21.setTransform(324.2,192.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy11, new cjs.Rectangle(211.9,77,224.6,231.6), null);


(lib.Symbol1copy10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7C0BE").s().p("AhQBRQghgiAAgvQAAguAhgiQAighAuAAQAPAAANAEIgUAmIAJAFIgRAiIAAAAQgPAAgKAJQgKALAAANQAAAPAKAJQAFAFAEACQAIAEAIAAQANAAALgLQAJgJABgPQAAgKgGgIIgEgGIgBAAIAQggIAOAHIAUgmIADACIAFAEQAgAhABAuIAAABQAAAvghAiIgFAFQghAcgrAAQguAAgighg");
	this.shape.setTransform(247.9,229.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6E706F").s().p("Ah5B7QgzgzAAhIQAAhHAzgyQAzgzBGAAQAdAAAaAIIgIAOQgXgHgaAAQhAAAguAuQguAuAABAQAABAAuAuQAuAuBAAAQAqAAAjgTQARgLARgQQAuguAAhAQAAhAguguIgRgPIAIgOQAPAMAHAHQAzAyAABFIAAACQAABIgzAzQgRASgVALQglAVgvAAQhGAAgzgyg");
	this.shape_1.setTransform(248.1,229.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D74990").s().p("AhEBTIgEgDQAnhCAMgpQAIgbAGgLQAJgUARgDQAQgEAPAKQAPAKAEAQQAJAYgMAoQgIgLgMgLQgPgPgRgMIgUApIgIANIgUAnIgUAmg");
	this.shape_2.setTransform(258.7,215.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D95B9A").s().p("AgDCWIAAgIIAJgCQAggFAQgTQAMgVgFgkQgLhJg4hFIgHgIIgMgOQgcgdgqghIgWgQQAWAGAZAJIAFADQAQAKAQALQAcAVAeAbQAkAgANAfQAKAUAAAUQAjA3gBBhQAAAOgBADQgEAGgNAAIhqACQADgRAAgRg");
	this.shape_3.setTransform(270.2,215);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D45092").s().p("AAsBYQgNgegjggQgfgbgcgUQgCgSAGgRQAHgVAQgSQAEgGAcgbIACAAQACAbAHAbQATBGAlBhIACADIgLAhQAAgUgKgVg");
	this.shape_4.setTransform(273.2,202.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E5CB0E").s().p("AAJAxQghgSgCgwIACgVIAAgEIAAgFIADgIQADAVAFARQAOAzAaAWQgJgDgJgEg");
	this.shape_5.setTransform(273.6,145.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E97726").s().p("AAIBXQgZgWgOg1QgFgQgEgUIACgEQAGgRAGgNQAIgMAJgHQADgEADgBQARgMARAEQgJAHgEAGIgDACQgFAIgDAJQgFAOAAAQQgCAVAHATIACAKQAGATAPAdIALAWIgFAAQgPAAgNgFg");
	this.shape_6.setTransform(275.5,142.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#161F1E").s().p("AAaAmIgTgLIgHgDIgDgCIgagNIgGgDQgcgQgCgJQgEgOAKgIIAEgCQAyAxAnANIAhAKQgBAFgFAEIgEADIgDACIgHABQgHAAgOgGg");
	this.shape_7.setTransform(296.7,149.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#19201F").s().p("AAcAcQgngNgygxQAEgDAFAAIAEACIAeAQQADABADACIAGADIALAFQAaAMAPAKIAFAEIALAIQABABgBAIIgBADIghgKg");
	this.shape_8.setTransform(297.3,148.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#46260E").s().p("AhsAcQgMAAgIgJQgGgGgCgIIAAgFQgBgGADgGIAGgIQAIgHAMAAIAwAAICuAAQALAAAIAHIADAEIAAACIAAADIABAaIgEAEQgIAJgVAAg");
	this.shape_9.setTransform(349.8,231.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E56DA9").s().p("ACyHiQgtAAhCgnQhEgng6iIIgCgFQg5iFANhXQANhXAehiQASg6AFgqIABgPQAvhLgvAVQgVAIgUABIgLgWQgOgdgHgTIgCgKQgGgUABgVQABgQAEgOQAEgJAFgIIACgCQAEgGAJgHIAGACIAYALIAQAIIAWALIAKAHIAZAOQARAJAFAKQAFALADAEIAMAHQAGAEAHAFIAIAOIABAIIgdgPIgFgDQgEAAgFAEIgEACQgJAIAEAOQACAJAcARIAFADQgPACgfBbQgGARgFARQguBHgRBbQgYCAAwByQAtBzA1A6QA0A5CEAMQCDAKBVgYQBSgaAYgwQALAiAUAsQgSAOgUAMIgxAAQgLAAgJAHIgFAIQgwADgrAAQivgEgsAAgAjcFbIgBgDQALgpgIgYQgFgQgPgKQgOgKgQAEQgRADgKAUQgGALgJAbQgLAqgoBCIgSgIIgJgFIAVgmIAUgoIAIgNIAVgpIgBgBIgGgCQgdAVg0gFIgxgHIgagEIgigDQgMgBgVAAIgwgBIgDAAQAFgHAGgGQAUgVAUgRQAggcAjgSQA6ggBBgFQAPgBAYAEQANACAPAEIAXAQQAqAhAcAdIAMAOIAGAIQA6BFAKBKQAGAkgNAVQgPATggAFIgKACQgDhFgpg1gAjuChIgFgDQgLgHgDgIQgCgIADgbQACgaASgdQASgdAugbIAfgRIgBADQgIAvAEAtIgBABQgcAagFAGQgPASgHAWQgGARABARQgQgLgPgKg");
	this.shape_10.setTransform(287.6,182.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#979F9B").s().p("AgzBcQglgMgfgdQgtgrgEg7IgBgOQAAgRAEgQIAQAAQgDAQAAARIABASQAFAxAmAlQAuAsA+ABQA/gBAtgsQArgpABhDQAAgPgDgOIASAAQADAPAAARIAAABQAABDgyAxQgeAdgmAMQgYAIgcAAQgbAAgYgIgAhPALQghgfAAgtQAAgSAFgPIBWAAIgBAAQgLAMAAAOQAAAPALAKQALALANgBQAQABALgLQAKgKAAgPQAAgOgKgMIgCAAIBQAAQAFAPAAASIgBAQQgFAjgbAZQggAhgvgBQguABghghg");
	this.shape_11.setTransform(385.3,237.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#509340").s().p("Ah1BFIAAgLIDrAAIAAALIjrAAgAh1ApIAAgMIDrAAIAAAMIjrAAgAh1AHIAAgKIB2AAIB1AAIAAAKIjrAAgAABgWIh2AAIAAgMIB2AAIB1AAIAAAMIh1AAgAABg5Ih0AAIgCAAIAAgLIACAAIAAAAIB0AAIB1AAIAAALIh1AAg");
	this.shape_12.setTransform(387.2,212.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#092123").s().p("AIcCfIgIgHQg1g0gJhIIgBgbIAAgCIAAgBQAAgQADgNIAyAAQgEAQAAAQIABAOQAEA8AtArQAfAdAlAMQAYAIAcAAQAcAAAYgIQAmgMAegdQAygxAAhEIAAgBQAAgQgDgPIAxAAIACAHIABAZQAABYhAA/IgIAHQg+A4hVAAQhVAAg/g4gAtGCZIgEgEQhAhAAAhaQAAhcBAhAIAEgDQAegdAlgPIAaADIAxAHQA0AFAdgVIAGADIABAAIgVApQgagJgdAAQhHAAgzA0QgzAyAABIQAABGAzAzQAzAzBHAAQAvAAAmgVQAVgMAQgSQAzgzAAhGIAAgDQAAhFgzgyQgHgIgPgLIAVgqQARAMAPAPQAMAMAIAMIACACQAoA1ADBFIAAAIQAAARgDAQQgJBFg1A0IgKAKQg9A3hUAAQhZAAg/g9gAJDBqQgmglgFgyIgBgSQAAgQADgQIAqAAQgFAPAAARQAAAtAhAgQAhAhAvgBQAvABAgghQAbgaAFgjIABgQQAAgRgFgPIAqAAQADAOAAAPQgBBCgrAqQgtAsg/ABQg/gBgugsgAsdBnQguguAAhAQAAhAAuguQAugvBBABQAagBAXAIIgUAnQgOgEgOAAQgwAAghAhQghAiAAAvQAAAuAhAiQAhAhAwAAQArAAAhgcIAEgFQAigiAAguIAAgCQgBgughghIgEgDIgDgDIAUgnIARAQQAuAuAABAQAABAguAuQgQAQgSALQgjATgqAAQhBAAgugugAKZATQgLgKAAgOQAAgOALgMIABAAIAxAAIACAAQAKAMAAAOQAAAOgKAKQgLALgQgBQgOABgLgLgAq+AZQgFgDgEgFQgKgJAAgOQAAgOAKgLQAKgJAPAAIABAAIARghIASAIIAEACIgQAgIAAAAIAFAGQAFAJAAAKQAAAOgKAJQgKALgOAAQgIAAgIgDg");
	this.shape_13.setTransform(316.5,230.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#EC7BB3").s().p("AC0B8IgxAAIgSAAIgqAAIhPAAIgxAAIhWAAIgqAAIgQAAIgyAAIgwAAQgLgMgJgPQgKgRgFgTIH5AAQgEgWAPgUIASgPIAGgEIALgKQAHgHAEgKQAKgZgHgkQgCgRgFgSQAOAQAMATIAGAIQANAUANAeIACADQAQAkAQA0IATA/g");
	this.shape_14.setTransform(388.9,214.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#EA443C").s().p("AAOAjQgNgTgNgQQgOgRgQgQQAdgEAPAIQAZAKALAfIAFAQQgKAHgNAAg");
	this.shape_15.setTransform(412.6,202.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E0352D").s().p("AAjAXQgLgfgYgKQgQgIgdAEIgFgGIgJgHQAHgDAIAAIA9AAQARAAANAMQANANAAARQAAARgNAMIgHAGIgFgQg");
	this.shape_16.setTransform(412.9,201.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#F09CC6").s().p("AlxF/Qg7gEgZgLQgYgMgygrQgygsgkheIgCgDQglhigThGQgHgagDgbQgFguAIgwIABgDQAMhMArhQQAPgdAYgkIAKgQIgCAQQgFApgSA7QgeBhgNBZQgMBWA4CFIACAFQA6CIBFAnQBDAmAsAAQAsAACvAEQArAAAwgDQgDAGAAAHIABAFQgJADgGAAIiLAFIhfABIhHgBgACaFFQgggYgRgvQgTg1AIg/QAKhgA3g0QAegaAngNQAlgMAqAAQBngDBgA9QBZA3A6BdQAQAbAPAdIgGAEIgSAPQgPAUAEAWIn6AAQAFATAKARQAJAPALAMgAFXD1IDrAAIAAgMIjrAAgAFXDYIDrAAIAAgLIjrgBgAFXC3IDrAAIAAgMIh0AAIh3AAgAFXCYIB3AAIB0AAIAAgLIh0AAIh3AAgAFXB2IABAAIB2AAIB0AAIAAgMIh0AAIh2AAIAAABIgBAAg");
	this.shape_17.setTransform(341.1,194.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#EE89BC").s().p("AkqDKQgBgFgCgEQgUgrgMgjIgEgPQgEgQgCgNIgBgOQgBg1APgqQAHgUAKgTQAGgLAJgKQAUgbAZgUQAlgcAugNQBQgZCBAQQBZAMA/AaQAbALAWAOQAVANASARIAMAKIAJAHIAFAGQAPAQAPASQAFASACARQAHAjgKAZQgEAKgGAHIgMALQgPgdgQgbQg5hchag4Qhgg9hmADQgqAAgkAMQgoANgeAaQg3A1gKBfQgHA/ASA1QARAvAgAYg");
	this.shape_18.setTransform(377.8,207.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#161E21").s().p("AEvAmIpZAAQgXAAgSgOIgGgFQgNgOgFgRIAXgJQBAgTBTAEQArAABqANQDKAXDOAAQgFAKgJAJQgPAOgTAFIgNAAg");
	this.shape_19.setTransform(360,178);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#CD5390").s().p("AkJFCQiDgMg1g5Qg0g6gvhzQgvhxAXiBQARhbAvhHQgXBFgJAyQgbCWAwB6QAwB7BLA4QBKA4A8gHQA7gIBYABQBPAAAkg7IAGgLIACgDIALgZQASg0gFg7IgFgbQgJg0gOgbQgLgUgNgIQghgVgcgLQgbgMgMgXQgHgMAAgOQASAOAXAAIJZAAIAMAAQAIAHAJAKQAXAlAOApQg/gahagLQiBgQhQAYQgvAOgkAbQgZAUgVAbQgIAKgGALQgKASgHAUQgPAsABA1IABAOQACANAEAPIAEAQQgYAwhRAaQg7ARhTAAQgjAAgngDg");
	this.shape_20.setTransform(338.8,195.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#242D31").s().p("AguAXQhqgNgrAAQhTgDhAASIgXAJQgCgHABgIQgBgZAUgUQAUgUAbAAIJZAAQAcAAATAUQAUAUAAAZQAAAPgGANQjOAAjKgYg");
	this.shape_21.setTransform(360.2,173.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_1
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AuDODQl0l0AAoPQAAoOF0l0QF1l1IOAAQIOAAF1F1QF1F0AAIOQAAIPl1F0Ql1F1oOAAQoOAAl1l1g");
	this.shape_22.setTransform(324.4,194.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(1));

	// Layer_2
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(255,255,255,0.02)").s().p("AvfPfQmamaAApFQAApEGambQGcmaJDAAQJEAAGbGaQGbGbAAJEQAAJFmbGaQmbGbpEAAQpDAAmcmbg");
	this.shape_23.setTransform(324.4,194.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy10, new cjs.Rectangle(184.2,54.3,280.4,280.4), null);


(lib.Symbol1copy9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ai7JZIAAkhIFHAAIAAEhgAixDmIAAgfQABhOARgwQARgyAkgnQAjgoB6hlQBBg1AAgtQAAgsgagZQgbgZg1AAQg4AAglAnQgmAlgLBeIk4gmQAQitBuhoQBthqDgAAQCvAABsBKQCTBiAACmQAABFgmA/QglBAh2BbQhRBAgVAnQgVAngBA/g");
	this.shape.setTransform(321.5,195.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.02)").s().p("AxiSGMAAAgkLMAjFAAAMAAAAkLg");
	this.shape_1.setTransform(324.2,192.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy9, new cjs.Rectangle(211.9,77,224.6,231.6), null);


(lib.gridbox_mccopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0.4)").ss(3,1,1).p("AKXlWIKYAAIAAKtIqYAAIqXAAAKXlWIAAKtAqWlWIKWAAIAAKtAqWFXIKWAAAqWFXIAAqtAqWFXIqYAAIAAqtIKYAAAAAlWIKXAA");
	this.shape.setTransform(-8.7,-103.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AK1E5IAApxIJcAAIAAJxgAAeE5IAApxIJbAAIAAJxgAp4E5IAApxIJbAAIAAJxgA0QE5IAApxIJcAAIAAJxg");
	this.shape_1.setTransform(-8.7,-103.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.gridbox_mccopy, new cjs.Rectangle(-142.8,-138.9,268.4,71.6), null);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("AgbCFQgJgDgJgEQgzgYgUg1QgTg0AYgyQAYg0A1gTQA0gUAzAZQAkAQAUAg");
	this.shape.setTransform(451.1,97.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AghDKQgUgFgTgJQhMgkgdhQQgdhOAkhNQAkhMBPgdQANgEANgDQAsgLArALQAVAFAUAJQA9AcAfA3");
	this.shape_1.setTransform(450.8,97.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AgGj/QAGgGAIgFQAJgEAKgFQBGgdBXAMQBYAOBxEkQhJCRhHATQhYAWgqADQgQABgUAJQgUAJgtAiQgrAigcAfQgbAegOgJQgsggACg6QABgyAegsQAfgsAngNQAMgFAagIQANgEANgEQhahIhbglQgYgKgVgOQgOgIgNgKQgcgUgagaQgTgVgvg7Qgvg9AXg4QAYg4BIA0QBIAzA1AzQACACADACQAUATAUAOQAJAGAJAGQADABADADQAYAOAaAJQA6AVALABAhXiwQAHgMAKgMQAQgUAVgQQAMgKAPgIIAAgBQA6gPBLA6");
	this.shape_2.setTransform(485.8,125.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AhmFWQgsggACg6QABgyAegsQAfgsAngNQAMgFAagIIAagIQhahIhbglQgYgKgVgOQgOgIgNgKQgcgUgagaQgTgVgvg7Qgvg9AXg4QAYg4BIA0QBIAzA1AzIAFAEQAUATAUAOIASAMIAGAEQAYAOAaAJQA6AVALABQgLgBg6gVQgagJgYgOIgGgEIAFgJQAHgMAKgMQAQgUAVgQQAMgKAPgIIAAgBIAOgLIATgJQBGgdBXAMQBYAOBxEkQhJCRhHATQhYAWgqADQgQABgUAJQgUAJgtAiQgrAigcAfQgVAXgNAAQgEAAgDgCgAB/jUIgCgBIgDgDQg5gpgvgBIAAAAIAAAAQgNAAgLADQALgDANAAIAAAAIAAAAQAvABA5ApIADADIACABIAAAAg");
	this.shape_3.setTransform(485.8,125.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(466,124.8,1.2,1.2);
	this.instance.alpha = 0.852;
	this.instance.filters = [new cjs.BlurFilter(53, 53, 1)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(379,37.8,178,178);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(6.6,-8.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.95,scaleY:0.95,x:6.4,y:25.6},15).to({scaleX:1,scaleY:1,x:6.6,y:-8.1},19).wait(1));

	// Layer_3
	this.instance_1 = new lib.Tween10("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,35.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.95,scaleY:0.95,y:67.3},15).to({scaleX:1,scaleY:1,y:35.9},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122,-127.1,247.9,326.2);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Wow
	this.instance = new lib.Symbol6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(637.9,346.4,1.111,1.111);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(173).to({_off:false},0).wait(53));

	// hand
	this.instance_1 = new lib.Tween8("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(263.2,503.2);
	this.instance_1.alpha = 0.5;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(107).to({_off:false},0).to({alpha:1},7).to({startPosition:0},12).to({scaleX:1.1,scaleY:1.1},9).to({scaleX:1,scaleY:1},6).to({_off:true},26).wait(59));

	// arrow
	this.instance_2 = new lib.Tween7("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(806.8,465.2,1,1,-37.2,0,0,652.2,197.3);
	this.instance_2.alpha = 0.801;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(83).to({_off:false},0).to({x:787.8,y:489.2,alpha:1},11).to({x:806.8,y:465.2},9).to({x:787.8,y:489.2,alpha:0.801},11).to({_off:true},1).wait(111));

	// Layer_8
	this.instance_3 = new lib.Symbol1copy14();
	this.instance_3.parent = this;
	this.instance_3.setTransform(645.8,583.5,1,1,0,0,0,330.2,210.2);

	this.instance_4 = new lib.Symbol1copy10();
	this.instance_4.parent = this;
	this.instance_4.setTransform(996.3,583.5,1,1,0,0,0,330.2,210.2);

	this.instance_5 = new lib.Symbol1copy15();
	this.instance_5.parent = this;
	this.instance_5.setTransform(295.4,583.5,1,1,0,0,0,330.2,210.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_5},{t:this.instance_4},{t:this.instance_3}]},51).to({state:[]},121).wait(54));

	// Layer_7
	this.instance_6 = new lib.Symbol1copy9();
	this.instance_6.parent = this;
	this.instance_6.setTransform(950.1,311.2,0.81,0.81,0,0,0,330.3,210.2);

	this.instance_7 = new lib.Symbol1copy13();
	this.instance_7.parent = this;
	this.instance_7.setTransform(747,311.2,0.81,0.81,0,0,0,330.3,210.2);

	this.instance_8 = new lib.Symbol1copy12();
	this.instance_8.parent = this;
	this.instance_8.setTransform(543.8,311.2,0.81,0.81,0,0,0,330.3,210.2);

	this.instance_9 = new lib.Symbol1copy11();
	this.instance_9.parent = this;
	this.instance_9.setTransform(340.6,311.2,0.81,0.81,0,0,0,330.3,210.2);

	this.questiongrid = new lib.gridbox_mccopy();
	this.questiongrid.name = "questiongrid";
	this.questiongrid.parent = this;
	this.questiongrid.setTransform(539.4,297.1,3.058,2.972,0,0,0,-41.6,-103);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.questiongrid},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6}]},21).to({state:[]},151).wait(54));

	// Layer_2
	this.instance_10 = new lib.qtext();
	this.instance_10.parent = this;
	this.instance_10.setTransform(636.4,116.7,1.889,1.889);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(6).to({_off:false},0).to({alpha:1},5).to({_off:true},161).wait(54));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(556.8,303.8,1450,835);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;