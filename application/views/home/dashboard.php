<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fireworks.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css" media="screen">

<div class="container">
 
  <!-- Trigger the modal with a button -->
  
 
  <!-- Modal -->
  <div class="modal fade" data-easein="bounceIn" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" >
        
        <div class="modal-body"  style="border: 10px solid rgba(0, 0, 0, 0.41);"  >
	<?php	//if($this->session->set3gradeid!=0) { $var1 = 1; }
           // if($this->session->set2gradeid!=0) { $var2 = 1; } 
			//$totalsets = $var1 + $var2 + 1; ?>
			<h2 class="modal-title" style="text-align:center;padding-bottom:0">Welcome </h2>
			<div class="col-sm-6 popupimg"><img src="<?php echo base_url(); ?>assets/images/space_small.png" align="middle"/></div>
			<div>
          <h3><?php echo 'Hello Brightmind!!'; ?> </h3>
			<p style="letter-spacing: 0.5px;">	 <?php echo 'Are you ready for the launch? <br/>Lets go through  puzzles to launch our rocket in the learning orbit'; ?> </p>
				 <div style="text-align:center;"><button type="button" class="btn btn-warning blink_me" data-dismiss="modal">Start</button></div></div>
        </div>
         
      </div>
	  
	 
      
    </div>
  </div>
  
   
 
  
</div>



 
<div id="gamecontentAjax">
<div class="container">
 
  <!-- Trigger the modal with a button -->
  
   

  <!-- Modal -->
  <div class="modal fade" id="completepopupmodal" role="dialog">
    <div class="modal-dialog">
      <?php $Greeting0='Needs Attention';
    $Greeting1='Needs Improvement';
    $Greeting2='Can Do Better';
    $Greeting3='Average';
    $Greeting4='Fair';
    $Greeting5='Good';
    $Greeting6='Very Good';
    $Greeting7='Excellent';
    $Greeting8='Extraordinary';
    $Greeting9='Genius !!!';
    $Greeting10='Prodigy';

    if(round($bspi,2)<1){ $dispbspimsg=$Greeting0;}
    else if(round($bspi,2)<11){ $dispbspimsg=$Greeting1;}
    else if(round($bspi,2)<21){ $dispbspimsg=$Greeting2;}
    else if(round($bspi,2)<31){ $dispbspimsg=$Greeting3;}
    else if(round($bspi,2)<41){ $dispbspimsg=$Greeting4;}
    else if(round($bspi,2)<51){ $dispbspimsg=$Greeting5;}
    else if(round($bspi,2)<61){ $dispbspimsg=$Greeting6;}
    else if(round($bspi,2)<71){ $dispbspimsg=$Greeting7;}
    else if(round($bspi,2)<81){ $dispbspimsg=$Greeting8;}
    else if(round($bspi,2)<91){ $dispbspimsg=$Greeting9;}
    else if(round($bspi,2)<101){ $dispbspimsg=$Greeting10;} ?>
      <!-- Modal content-->
      <div class="modal-content" style="background:rgba(0, 0, 0, 0);box-shadow:none;border:0">
       
        <div class="modal-body" <!-- style="border: 10px solid rgba(0, 0, 0, 0.41);"-->>
	<h2 class="modal-title" style="text-align:center;font-weight:border;text-align:center;">Puzzle Completion</h2>
        <p style="font-weight:border;color:#fff;">Congrats, You have successfully completed the Puzzle set !!!.</p>
		  
		 <?php  if($controllername2=='reports') { ?>
		  <div style="text-align:center">
<h4 style="font-weight:border;color:#fff;">BSPI Score : <span class="updatedBSPI"><?php echo round($bspi,2); ?>%</span></h4>
<div class="progress">
  <div class="progress-bar progress-bar-striped active massive-font" role="progressbar"
  aria-valuenow="<?php echo round($bspi,2); ?>" aria-valuemin="0" aria-valuemax="100" style="background-color: #ff9854;width:<?php echo round($bspi,2); ?>%">
   <span>   <?php echo $dispbspimsg; ?> </span>

  </div>
</div>

<div id="chart-container"></div>
 
</div>
		  <div style="text-align:right;" ><a class="btn btn-success blink_me1 ViewReport" href="<?php echo base_url()."index.php/".$controllername2."/reportslist#View"; ?>" >Click here to see your <?php echo $setname; ?> </a></div>
		 
		 <?php } else {  ?>
		  
		  <div style="text-align:center">
		
<h4 style="font-weight:border;color:#fff;">BSPI Score : <span class="updatedBSPI"><?php echo round($bspi,2); ?>%</span></h4>
<div class="progress">
  <div class="progress-bar progress-bar-striped active massive-font" role="progressbar"
  aria-valuenow="<?php echo round($bspi,2); ?>" aria-valuemin="0" aria-valuemax="100" style="background-color: #ff9854;width:<?php echo round($bspi,2); ?>%">
   <span>   <?php echo $dispbspimsg; ?> </span>

  </div>
</div>
<div id="chart-container"></div>
 
</div>

		  <div style="text-align:right;" ><a  class="btn btn-success blink_me1 ViewReport" href="<?php echo base_url()."index.php/".$controllername2."/dashboard#View"; ?>" >Click here to play next puzzle set <?php //echo $setname; ?> </a></div>
		 <?php } ?>
        </div>
        
      </div>
	  
	 
      
    </div>
  </div>
  
</div>

 <div class="clear_both"></div>
 <div class="section_four">
 <div class="container">
 <h2>Puzzles </h2>
 <?php if (isset($this->session->fullname) && ($this->session->fullname!=''))
 { ?>
  <h4> Welcome, <?php echo $this->session->fullname; ?></h4>
 <?php } ?>
 <div class="panel panel-default">

<div class="panel-body">

<div class="tab-content responsive">
<div class="tab-pane active" id="BrainSkillsOn">
<div class="tab_sec_c1">
 <h3> </h3>
<div class="row">
<?php
 $myskillpie=array("59"=>"#cdcdcd","60"=>"#cdcdcd","61"=>"#cdcdcd","62"=>"#cdcdcd","63"=>"#cdcdcd");
 //$myskillpie=array("59"=>"#ff6600","60"=>"#067d00","61"=>"#ffbe00","62"=>"#be0000","63"=>"#1e9ddf");
 $myskillpie_orginal=array("59"=>"#da0404","60"=>"#ffc000","61"=>"#92d050","62"=>"#ff6600","63"=>"#00b0f0");
?>
<?php //print_r($this->session); 
$inCC=0;
foreach($actualGames as $games)
{
	
	?>
	<div class="col-md-3 col-sm-3 col-xs-3 bounce" >

<div class="box_c1 bounceIn animated" style="background-color:<?php echo $myskillpie_orginal[$games['skill_id']]; ?>">
<!--<div class="pricesaving btnactive"><?php //echo 5-$games['tot_game_played']; ?></div>-->
<h3 style="color:#fff;"><?php echo $actualGameCategory[$games['skill_id']]; ?></h3>
<?php
if($games['tot_game_score']==""){$games['tot_game_score']=0;}
if($games['tot_game_played']=="" || $games['tot_game_played']==0 ){$tot_game_played=1;}else{$myskillpie[$games['skill_id']]=$myskillpie_orginal[$games['skill_id']];$tot_game_played=$games['tot_game_played'];}
 
$avg_game_score=$games['tot_game_score']/$tot_game_played;
	if($avg_game_score < 20) $filled_stars = 0;
	if($avg_game_score >= 20 && $avg_game_score <= 40)	$filled_stars = 1;
	if($avg_game_score >= 41 && $avg_game_score <= 60)	$filled_stars = 2;
	if($avg_game_score >= 61 && $avg_game_score <= 80)	$filled_stars = 3;
	if($avg_game_score >= 81 && $avg_game_score <= 90)	$filled_stars = 4;
	if($avg_game_score >= 91 && $avg_game_score <= 100)	$filled_stars = 5;
	
	?>
<?php if($games['tot_game_played']<$games['ncount']){ ?>	
<a class="fancybox fancybox.iframe" id="<?php echo $games['gid']; ?>" href="javascript:;" data-href="<?php echo base_url()."assets/swf/".$this->session->userlang."/".$games['game_html'].".html"; ?>"><img src="<?php echo base_url(); ?>assets/<?php echo $games['img_path']; ?>"/></a>
<?php  } else {?>        
<a  href="javascript:;"><img src="<?php echo base_url(); ?>assets/<?php echo $games['img_path']; ?>"/></a>  
<?php } ?>

<p><?php echo $games['gname']; ?></p>
<p style="background-color: #fff;box-shadow: 2px 0px 8px 2px rgb(183, 180, 180);" id="stars<?php echo $games['gid']; ?>">
 
<?php

	 
	//echo $filled_stars;
 for($i=0;$i<$filled_stars;$i++){ //echo $filled_stars; ?>
	 <img class="staractive" src="<?php echo base_url(); ?>assets/images/icon_StarActive.png">
 <?php } ?>
  <?php for($i=0;$i<5-$filled_stars;$i++){  ?>
	 <img class="starinactive" src="<?php echo base_url(); ?>assets/images/icon_StarInActive.png">
 <?php } ?>
</p>
<?php if($games['tot_game_played']<$games['ncount']){ ?>
<a style="" class="btn btn-default in_btn fancybox fancybox.iframe btn-4 btnactive" id="<?php echo $games['gid']; ?>" href="javascript:;" data-href="<?php echo base_url()."assets/swf/".$this->session->userlang."/".$games['game_html'].".html"; ?>"><span><?php if($games['tot_game_played']==0){ echo "Play"; } else{echo "Re-play";}?></span></a>         
<?php  } else { $inCC++;?>        
<a style="" class="btn btn-default in_btn gameBtnInactive" href="javascript:;">Completed</a>  
<?php } ?>
</div>
</div>
<?php 
 $sum+= $filled_stars; } //echo $sum;
?>


<div class="demo">
<h1></h1>
	  </div>

</div><!--/row -->

</div><!--/tab_sec_c1 -->

</div><!--/tab-pane -->



</div><!-- tab-content -->
</div><!-- panel-body -->
</div><!-- panel-default -->
 </div><!-- container -->
 </div><!-- section_four -->
 </div>
<div class="section_five">
<div class="container">
<div class="row">


<div class="col-md-12 col-sm-12 col-xs-12">
 
<script src="<?php echo base_url(); ?>assets/js/createjs-2015.11.26.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/rockethtml.js"></script>
<script>
$(document).ready(function(){
	<?php 
	/*if($inCC==0){ $x11=0;$x12=0;} 
		 else if($inCC==1){$x11=0;$x12=49;}
		 else if($inCC==2){$x11=49;$x12=100;}
		 else if($inCC==3){$x11=100;$x12=150;}
		 else if($inCC==4){$x11=150;$x12=199;}
		 else if($inCC==5){$x11=199;$x12=249;}
	*/	 
		 if($inCC==0){ $x11=0;$x12=0;} 
		 else if($inCC==1){$x11=0;$x12=9;}
		 else if($inCC==2){$x11=9;$x12=19;}
		 else if($inCC==3){$x11=19;$x12=29;}
		 else if($inCC==4){$x11=20;$x12=39;}
		 else if($inCC==5){$x11=39;$x12=49;}
		 
		 
		 ?>
		 
		 
		 init(<?php echo $x11; ?>,<?php echo $x12; ?>);
});


/*
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init(gotx1,gotx2) {
	canvas = document.getElementById("canvas");
	anim_container = document.getElementById("animation_container");
	dom_overlay_container = document.getElementById("dom_overlay_container");
	handleComplete(gotx1,gotx2);
}
function handleComplete(gotx1,gotx2) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	exportRoot = new lib.RocketDesign_Canvas();
	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);	
	//Registers the "tick" event listener.
	fnStartAnimation = function() {
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage);
		setInterval(function(){ if(gotx2>=gotx1){exportRoot.gotoAndStop(gotx1);} gotx1++;}, 1000/70);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas);		
		resizeCanvas();		
		function resizeCanvas() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = $(".container").width(), ih='250';				
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas.width = w*pRatio*sRatio;			
			canvas.height = h*pRatio*sRatio;
			canvas.style.width = dom_overlay_container.style.width =   w*sRatio+'px';				
			canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
			stage.scaleX = pRatio*sRatio;			
			stage.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;		
		}
	}
	makeResponsive(true,'both',true,1);	
	fnStartAnimation();
}
*/

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init(gotx1,gotx2) {
	canvas = document.getElementById("canvas");
	anim_container = document.getElementById("animation_container");
	dom_overlay_container = document.getElementById("dom_overlay_container");
	handleComplete(gotx1,gotx2);
}
function handleComplete(gotx1,gotx2) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	exportRoot = new lib.RocketDesign_Canvas();
	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);	
	//Registers the "tick" event listener.
	fnStartAnimation = function() {
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage);
		setInterval(function(){ if(gotx2>=gotx1){exportRoot.gotoAndStop(gotx1);} gotx1++;}, 1000/70);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas);		
		resizeCanvas();		
		function resizeCanvas() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = $(".container").width(), ih='250';				
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas.width = w*pRatio*sRatio;			
			canvas.height = h*pRatio*sRatio;
			canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';				
			canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
			stage.scaleX = pRatio*sRatio;			
			stage.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;		
		}
	}
	makeResponsive(true,'both',true,1);	
	fnStartAnimation();
}
</script>
<!-- write your code here -->
<div id="animation_container" style="text-align:center;margin:0;">
		<canvas id="canvas" width="800" height="150" style=""></canvas>
		<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:800px; height:150px; position: absolute; left: 0px; top: 0px; display: block;">
		</div>
	</div>


 
</div>
 
</div><!-- row -->
</div> <!-- container -->	
</div>	<!-- section_five -->	

<style>
  #animation_container {
	width:100%; 
	margin:auto;
	left:0;right:0;
	top:0;bottom:0;
  }
</style>
 
 
 <div class="clear_both"></div>	
<style>


/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<div id="loadingimage1" style="display:none;" class="loading1">Loading&#8230;</div>
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/highcharts-3d.js"></script>
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>

<script src="<?php echo base_url(); ?>assets/fusioncharts/js/fusioncharts.js"></script>
<script src="<?php echo base_url(); ?>assets/fusioncharts/js/fusioncharts.theme.fint.js"></script>
<script src="<?php echo base_url(); ?>assets/fusioncharts/js/fusioncharts.widgets.js"></script>
<script>
 
$(function () {

/*	Highcharts.setOptions({
 colors: ['<?php echo $myskillpie['59']; ?>','#fff', '<?php echo $myskillpie['60']; ?>', '#fff','<?php echo $myskillpie['61']; ?>', '#fff','<?php echo $myskillpie['62']; ?>', '#fff','<?php echo $myskillpie['63']; ?>','#fff']
});
Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });
    Highcharts.chart('container', {
        chart: {
            type: 'pie',
			backgroundColor:'transparent',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
       tooltip: {enabled: false},exporting:false,credits: {
      enabled: false
  },
        plotOptions: {
            pie: {
				 
                 borderWidth:1,
                  dataLabels: {
                enabled: false,
            },
                depth: 50 
            }
        },
        series: [{
            type: 'pie',
            name: 'Skill Pie',
            data: [
                [ 19],
                [ 0.4],
                [ 20],
				 [ 0.4],
                [  20],
				 [ 0.4],
                [  20],
				 [ 0.4],
                [  20],
				[ 0.4]
            ]
        }]
    });*/
});


function guagechart()
			{
			
	FusionCharts.ready(function () {
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container","background":"transparent",
        "width": "300",
        "height": "150",
        "dataFormat": "json",
            "dataSource": {
    "chart": {
		 "baseFont": "Phenomena-Regular",
            "baseFontSize": "20",
            "baseFontColor": "#0c315b",
        "caption": "",
        "lowerlimit": "0",
        "upperlimit": "100",
	 "bgAlpha":'0',
            "gaugeFillMix": "",
            "gaugeFillRatio": "1",
            "theme": "fint",
			
        
    },
    "colorrange": {
        "color": [
            {
                "minvalue": "0",
                "maxvalue": "20",
                "code": "e44a00"
            },
            {
                "minvalue": "20",
                "maxvalue": "75",
                "code": "f8bd19"
            },
            {
                "minvalue": "75",
                "maxvalue": "100",
                "code": "6baa01"
            }
        ]
    },
    "dials": {
        "dial": [
            {
                "value": "<?php echo $bspi; ?>",
                "rearextension": "15",
                "radius": "100",
                "bgcolor": "333333",
                "bordercolor": "333333",
                "basewidth": "8"
            }
        ]
    }
}
      });

csatGauge.render();
});	
			}
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//$('.btnactive').sparkleHover(); 
	//$('#myModal').modal('show');
	var logincount = '<?php echo $this->session->showIntro; ?>';
	<?php $this->session->set_userdata('showIntro',0); ?>
if(logincount==1)
{
		$('#myModal').modal('show');
		$.ajax({
				type:"POST",
				url:"<?php echo base_url('index.php/home/unsetshowintro') ?>",
				success:function(result)
				{	
				}
		}); 
		
}
	
	fancyCall();
	//guagechart();
	}); 
	function fancyCall()
	{
		
		$("a.fancybox").each(function() { 
		var tthis = this;
  $(this).fancybox({
			'transitionIn'    :    'elastic',
'transitionOut'    :    'elastic',
'speedIn'     :    600,
'speedOut'     :    200,
'overlayShow'    :    false,
'width'  : 750,           // set the width
    'height' : 500,           // set the height
    'type'   : 'iframe',       // tell the script to create an iframe
    'scrolling'   : 'no',
	'href'          : $(this).attr('data-href'),
	helpers     : { 
	overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
},
keys : {
	// prevents closing when press ESC button
	close  : null
},
	 'afterClose': function () {  
       // parent.location.reload(true);
		
		//alert(tthis.id);
		$('#loadingimage1').show();
		
		$.ajax({
    type: "POST",
    url: "<?php echo base_url()."index.php/".$ajaxurl."/ajaxstarsupdate"; ?>",
    data: {gameid:tthis.id},
     
    success: function(result){
		 //alert('heyhai');
		 $('#loadingimage1').hide();
		 $('#gamecontentAjax').html(result);
		 //guagechart();
		 //$('.btnactive').sparkleHover(); 
		 fancyCall();
		 
		 
    }
});

	var currentdate = '<?php echo date('Y-m-d'); ?>';
	var userid = '<?php echo $this->session->user_id; ?>';
$.ajax({
	
    type: "POST",
    url: "<?php echo base_url()."index.php/".$ajaxurl."/gamecompletepopup"; ?>",
    data: {userid:userid,currentdate:currentdate},
     
    success: function(result){
		 
		var inCC=result;
		 
		 /*if(inCC==0){ init(0,0);} 
		 else if(inCC==1){init(0,49); }
		 else if(inCC==2){init(49,100); }
		 else if(inCC==3){init(100,150); }
		 else if(inCC==4){init(150,199); }
		 else if(inCC==5){init(199,249); }
		  */
		 if(inCC==0){ init(0,0);} 
		 else if(inCC==1){init(0,9); }
		 else if(inCC==2){init(9,19); }
		 else if(inCC==3){init(19,29); }
		 else if(inCC==4){init(29,39); }
		 else if(inCC==5){init(39,49); }
		 
		  
		 
		if(result==5)
		{
			
			$('body').fireworks({ sound: true });
			 
			window.setTimeout(function(){
			$('#completepopupmodal').modal({backdrop: 'static', keyboard: false});
			 }, 5000)
 			
			
		}
		 
		 
    }
});



$.ajax({
    type: "POST",
    url: "<?php echo base_url()."index.php/".$ajaxurl."/bspiajax"; ?>",
    data: {},
     
    success: function(result){
		 //alert(result);
		 $('#bspiid').html(result);
		 //fancyCall();
		 
    }
});

//var setonecount = '<?php echo $disablemenus[0]['countid']; ?>';
//if(setonecount==5)
//{
//	alert('hello');
//}
		
    },
	beforeShow : function(){ 
	/* Check User Login */
		LoginAjaxCall();
	/* Check User Login */	 
	//$('.btnactive').sparkleHover(); 
 $(".fancybox-inner").addClass("fancyGameClass");
 $.ajax({
    type: "POST",
    url: "<?php echo base_url()."index.php/home/gamesajax"; ?>",
    data: {gameid:tthis.id,skillkit:'N',controllername:'<?php echo $controllername; ?>',gameurl:$(tthis).attr('data-href')},
    success: function(result){
		  if($.trim(result)=='IA')
{
	$.fancybox.close();
}	 
    }
});
  
}

});
});
	}

</script>
<style>
.btn-4:hover {
  -webkit-transform: scale(1.1);
    background: #31708f;
}
#fireworksField{z-index:9999;}
.modal{z-index:99999;}
svg{    background-color:rgba(0, 0, 0, 0) !important; }  body{min-height:0 !important;}



body { width:100%; height:100%;}
.demo { margin:0 auto; width:100%; height:100%;}
h1 { margin:150px auto 30px auto; text-align:center; font-family:'Roboto';}

.blink_me {
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 1500ms;
    -webkit-animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;

    -moz-animation-name: blinker;
    -moz-animation-duration: 1500ms;
    -moz-animation-timing-function: linear;
    -moz-animation-iteration-count: infinite;

    animation-name: blinker;
    animation-duration: 1500ms;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
}

@-moz-keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 40px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}

@-webkit-keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 40px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}

@keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 40px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}
.massive-font{
font-size: 30px;
text-align: center;
}
  
.progress{
height:50px;
text-align:center;
}

.progress-bar {
  padding:15px;
}

.progress {
    position: relative;
}

.bar {
    z-index: 1;
    position: absolute;
}

.progress span {
    position: absolute;
    top: 12px;
    z-index: 2;
    text-align: center;
   
    color: black;
}
.ViewReport{background: #1713a5;font-size: 26px;border: 3px solid #fff;}
</style>
