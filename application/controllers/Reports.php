<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Assessment_model');
				$this->load->model('Assessment_model_set2');
				$this->load->model('Assessment_model_set3');
				$this->load->library('session');			
				//$this->lang->load("menu_lang","french");		
			
        }
		
		public function bubblechart()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
				
		$data['GradewiseScore_data1'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports');
		$su1=array_unique(array_column($data['GradewiseScore_data1'],'bspi'));
  
		$gu1=array_search($this->session->user_id,array_column($data['GradewiseScore_data1'],'id'));

		$data['GradeWiseReport1']=$data['GradewiseScore_data1'];

		$su1_r=array_reverse(array_merge($su1));

		$cus1=$data['GradewiseScore_data1'][$gu1];
 
		$data['gu1']=$gu1;
		$data['su1']=$su1;
		$data['su1_r']=$su1_r;
		$data['cus1']=$cus1;
		
		$this->load->view('reports/leaderboard',$data);
		
	}
		
	public function reportslist()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
		$userid = $this->session->user_id;
		$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart1("",$userid);
		
		$data['userbspiscore']= $this->Assessment_model->userbspiscore($userid);
		
		$mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
		
		foreach($bspicalendarskillScore as $score)
		{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
		
		$data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
		
		$this->load->view('headerinner');
		$this->load->view('reports/reports',$data);
		$this->load->view('footerinner');
		
	}
		
	public function reportslist_old()
	{			
	
	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

	 
		$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
		$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;
	
		$data['disablemenus'] = $this->Assessment_model->menudisable($userid,date('Y-m-d'));	
	//	$data['disablemenus2'] = $this->Assessment_model_set2->menudisable2($userid,date('Y-m-d'));
	//	$data['disablemenus3'] = $this->Assessment_model_set3->menudisable3($userid,date('Y-m-d'));
			
		
		/*SKILL COMPARISON REPORT*/ 
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	
	
	$userid = $this->session->user_id;
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart1("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
	 
	 /* $bspicalendarskillScore2= $this->Assessment_model->mybspicalendarSkillChart2("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore2 as $score)
	{$mybspiCalendarSkillScore2["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore2']=$mybspiCalendarSkillScore2;
	 
	 $bspicalendarskillScore3= $this->Assessment_model->mybspicalendarSkillChart3("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore3 as $score)
	{$mybspiCalendarSkillScore3["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore3']=$mybspiCalendarSkillScore3; */
		/*SKILL COMPARISON REPORT*/
	
		/*BSPI COMPARISON*/
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	$data['bspicomparison'] = $this->Assessment_model->getbspicomparison($this->session->school_id,$this->session->game_grade,$userid);	
		/*BSPI COMPARISON*/
		
		/*SKILL PERFORMANCE*/
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	
	
if($this->session->set3gradeid!=0) {
 $var1 = 1; }

if($this->session->set2gradeid!=0) {
 $var2 = 1; } 

$data['totalsets'] = $var1 + $var2 + 1;
	
$data['skillwiseaverage'] = $this->Assessment_model->getskillwise_avg($userid,$this->session->school_id,$this->session->game_grade);

	

$data['set1avg_M'] = ($data['skillwiseaverage'][0]['skillscorem'] + $data['skillwiseaverage'][0]['skillscore2m'] + $data['skillwiseaverage'][0]['skillscore3m']) / $data['totalsets'];

$data['set1avg_V'] = ($data['skillwiseaverage'][0]['skillscorev'] + $data['skillwiseaverage'][0]['skillscore2v'] + $data['skillwiseaverage'][0]['skillscore3v']) / $data['totalsets'];

$data['set1avg_F'] = ($data['skillwiseaverage'][0]['skillscoref'] + $data['skillwiseaverage'][0]['skillscore2f'] + $data['skillwiseaverage'][0]['skillscore3f']) / $data['totalsets'];

$data['set1avg_P'] = ($data['skillwiseaverage'][0]['skillscorep'] + $data['skillwiseaverage'][0]['skillscore2p'] + $data['skillwiseaverage'][0]['skillscore3p']) / $data['totalsets'];

$data['set1avg_L'] = ($data['skillwiseaverage'][0]['skillscorel'] + $data['skillwiseaverage'][0]['skillscore2l'] + $data['skillwiseaverage'][0]['skillscore3l']) / $data['totalsets'];
		/*SKILL PERFORMANCE*/
		
		/*Class Performance*/
	
		$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
		
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);

	
//	$data['SetDetails2']=$this->Assessment_model_set2->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
	 
//	$data['SetDetails3']=$this->Assessment_model_set3->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
	
	$data['GradewiseScore_data1'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports');
	//$data['GradewiseScore_data2'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports_set2');
	//$data['GradewiseScore_data3'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports_set3');
//	$data['GradewiseScore_data_overall'] = $this->Assessment_model->getClassPerformace_data_overall($this->session->school_id,$this->session->game_grade,$this->session->section,$data['SetDetails2'][0]['planid'],$data['SetDetails3'][0]['planid']);
//echo "ss";exit;
$data['GradewiseScore_data_overall']='';
  
  $su1=array_unique(array_column($data['GradewiseScore_data1'],'bspi'));
 // $su2=array_unique(array_column($data['GradewiseScore_data2'],'bspi'));
 // $su3=array_unique(array_column($data['GradewiseScore_data3'],'bspi'));
  $su_overall=array_unique(array_column($data['GradewiseScore_data_overall'],'bspi'));
  
  
 $gu1=array_search($this->session->user_id,array_column($data['GradewiseScore_data1'],'id'));
 //$gu2=array_search($this->session->user_id,array_column($data['GradewiseScore_data2'],'id'));
 //$gu3=array_search($this->session->user_id,array_column($data['GradewiseScore_data3'],'id'));
 $gu_overall=array_search($this->session->user_id,array_column($data['GradewiseScore_data_overall'],'id'));

  $data['GradeWiseReport1']=$data['GradewiseScore_data1'];
 // $data['GradeWiseReport2']=$data['GradewiseScore_data2'];
  //$data['GradeWiseReport3']=$data['GradewiseScore_data3'];
  $data['GradeWiseReport_overall']=$data['GradewiseScore_data_overall'];
  
  
  $su1_r=array_reverse(array_merge($su1));
 // $su2_r=array_reverse(array_merge($su2));
 // $su3_r=array_reverse(array_merge($su3));
  $su_overall_r=array_reverse(array_merge($su_overall));
  
  $cus1=$data['GradewiseScore_data1'][$gu1];
 // $cus2=$data['GradewiseScore_data2'][$gu2];
  //$cus3=$data['GradewiseScore_data3'][$gu3];
  $cus_overall=$data['GradewiseScore_data_overall'][$gu_overall];
  
	 
	 $data['gu1']=$gu1;
	 $data['gu2']=$gu2;
	 $data['gu3']=$gu3;
	 $data['gu_overall']=$gu_overall;
	 $data['su1']=$su1;
	 $data['su2']=$su2;
	 $data['su3']=$su3;
	 $data['su_overall']=$su_overall;
	 $data['su1_r']=$su1_r;
	 $data['su2_r']=$su2_r;
	 $data['su3_r']=$su3_r;
	 $data['su_overall_r']=$su_overall_r;
	 $data['cus1']=$cus1;
	// $data['cus2']=$cus2;
	// $data['cus3']=$cus3;
	 $data['cus_overall']=$cus_overall;
	 
	 /*Class Performance*/
	 
	 
	
	$data['bspi'] = $tot;
		$this->load->view('headerinner',$data);
		$this->load->view('reports/reports',$data);
		$this->load->view('footerinner');
		
	}
	
	public function brainskill_report()
	{			
	
	//print_r($_POST); 
	//echo $this->input->post('game'),

	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$data['query'] = $this->Assessment_model->getgameplanid($userid);
		$pid =$data['query'][0]['gp_id'];
		
		$data['query1'] = $this->Assessment_model->getgamenames($userid,$pid);
		//echo '<pre>';
		//print_r($data['query1']); exit;
		//foreach($data['query1'] as $gamedetails)
		//{ 
		//$gameid = $this->input->post('game');
		//$data['gamedetails'] = $this->Assessment_model->getgamedetails($userid,$gameid,$pid );
		//}
		$this->load->view('headerinner', $data);
		$this->load->view('reports/pp_brainskillreport', $data);
		$this->load->view('footerinner');
		
	}
	
	public function brainskill_report_ajax()
	{
		 $userid = $this->session->user_id;
		 $gameid = $this->input->post('game');
		 $pid = $this->input->post('planid');
		
		
		$data['gamedetails'] = $this->Assessment_model->getgamedetails($userid,$gameid,$pid ); 
		//print_r($data['gamedetails']);
	echo $data['json'] = json_encode($data['gamedetails']);	exit;

	}
	
	
	public function skill_performance_report()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		 
		$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	
	
if($this->session->set3gradeid!=0) {
 $var1 = 1; }

if($this->session->set2gradeid!=0) {
 $var2 = 1; } 

$data['totalsets'] = $var1 + $var2 + 1;
	
$data['skillwiseaverage'] = $this->Assessment_model->getskillwise_avg($userid,$this->session->school_id,$this->session->game_grade);

	

$data['set1avg_M'] = ($data['skillwiseaverage'][0]['skillscorem'] + $data['skillwiseaverage'][0]['skillscore2m'] + $data['skillwiseaverage'][0]['skillscore3m']) / $data['totalsets'];

$data['set1avg_V'] = ($data['skillwiseaverage'][0]['skillscorev'] + $data['skillwiseaverage'][0]['skillscore2v'] + $data['skillwiseaverage'][0]['skillscore3v']) / $data['totalsets'];

$data['set1avg_F'] = ($data['skillwiseaverage'][0]['skillscoref'] + $data['skillwiseaverage'][0]['skillscore2f'] + $data['skillwiseaverage'][0]['skillscore3f']) / $data['totalsets'];

$data['set1avg_P'] = ($data['skillwiseaverage'][0]['skillscorep'] + $data['skillwiseaverage'][0]['skillscore2p'] + $data['skillwiseaverage'][0]['skillscore3p']) / $data['totalsets'];

$data['set1avg_L'] = ($data['skillwiseaverage'][0]['skillscorel'] + $data['skillwiseaverage'][0]['skillscore2l'] + $data['skillwiseaverage'][0]['skillscore3l']) / $data['totalsets'];
	
	
	
		//$this->load->view('headerinner', $data);
		$this->load->view('reports/skill_performance_report', $data);
		//$this->load->view('footerinner');
		
	}
	
	public function skill_comparision()
	{			
	
	//print_r($_POST); 
	//echo $this->input->post('game'),

	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		 
		$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	
	
	$userid = $this->session->user_id;
	$bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart1("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
	 
	 $bspicalendarskillScore2= $this->Assessment_model->mybspicalendarSkillChart2("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore2 as $score)
	{$mybspiCalendarSkillScore2["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore2']=$mybspiCalendarSkillScore2;
	 
	 $bspicalendarskillScore3= $this->Assessment_model->mybspicalendarSkillChart3("",$userid);
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore3 as $score)
	{$mybspiCalendarSkillScore3["SID".$score['gs_id']]=round($score['gamescore'],2);}
	 $data['bspicalendarskillScore3']=$mybspiCalendarSkillScore3;
	
		//$this->load->view('headerinner', $data);
		$this->load->view('reports/skill_comparision', $data);
		//$this->load->view('footerinner');
		
	}
	
	public function bspi_comparision()
	{	
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		 
		$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model->getskills();	
	
	$data['bspicomparison'] = $this->Assessment_model->getbspicomparison($this->session->school_id,$this->session->game_grade,$userid);	
			
		//$this->load->view('headerinner', $data);
		$this->load->view('reports/bspi_comparision', $data);
		//$this->load->view('footerinner');
		
	}
	
	
	
	public function individual_skill_report()
	{			
	
	//print_r($_POST); 
	//echo $this->input->post('game'),

	if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$data['skills'] = $this->Assessment_model->getskills();
		$this->load->view('headerinner', $data);
		$this->load->view('reports/individual_skill_report', $data);
		$this->load->view('footerinner');
		
	}
	public function individualskill_report_ajax()
	{
		$skillname= $_REQUEST["months"];
		$userid = $this->session->user_id;
		$data['timelimit'] = $this->Assessment_model->getdates($userid); 
		//print_r($data['timelimit']); 
		$trophystartdate = $data['timelimit'][0]['startdate'];
		
		$monthdisparray = array("1"=>"JAN","2"=>"FEB","3"=>"MAR","4"=>"APR","5"=>"MAY","6"=>"JUN","7"=>"JUL","8"=>"AUG","9"=>"SEP","10"=>"OCT","11"=>"NOV","12"=>"DEC");
		
		$startdatesplit = explode("-",$trophystartdate);
		
		$startmonthdisp = $startdatesplit[1];
					
					$displaycatval = array();
					$displaydataval = array();
					$indskillarr = array();
					
		$data['getbspi'] = $this->Assessment_model->getbspireport($userid); 
		
		$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
		$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
		$month_array=$month_array_chart=array();
		 
		error_reporting(0);
		foreach($data['getbspi'] as $get_res)
		{
			 
			if(!isset($month_array[$get_res['playedMonth']][$get_res['gs_id']]['score'])){$month_array[$get_res['playedMonth']][$get_res['gs_id']]['score']=0;}
			if(!isset($month_array[$get_res['playedMonth']][$get_res['gs_id']]['count'])){$month_array[$get_res['playedMonth']][$get_res['gs_id']]['count']=0;}
			//if(!isset($month_array[$get_res[3]][$get_res[1]]['score'])){$month_array[$get_res[3]][$get_res[1]]['score']=0;}

				$month_array[$get_res['playedMonth']][$get_res['gs_id']]['score']+=$get_res['gamescore'];
			
			$month_array[$get_res['playedMonth']][$get_res['gs_id']]['score']+=$get_res[0];
	$month_array[$get_res['playedMonth']][$get_res['gs_id']]['count']+=1;
	$month_array_chart[$get_res['playedMonth']][$get_res['gs_id']]=$month_array[$get_res['playedMonth']][$get_res['gs_id']]['score']/$month_array[$get_res['playedMonth']][$get_res['gs_id']]['count'];
	
	 
		
		if(($get_res['gs_id']=='59')){
			$res_tot_memory_i++;
			  $res_tot_memory += $get_res['gamescore'];	
		}else{
		 	$res_tot_memory += 0.00;
			}
if(($get_res['gs_id']=='60')){
				$res_tot_vp_i++;

			 	$res_tot_vp += $get_res['gamescore'];	
		}else{
			 $res_tot_vp += 0.00;
			}
if(($get_res['gs_id']=='61')){
				$res_tot_fa_i++;

				 $res_tot_fa += $get_res['gamescore'];	
		}else{
			 $res_tot_fa += 0.00;
			}
if(($get_res['gs_id']=='62')){
				$res_tot_ps_i++;

			 $res_tot_ps += $get_res['gamescore'];	
		}else{
			 $res_tot_ps += 0.00;
			}
if(($get_res['gs_id']=='63')){
				$res_tot_lang_i++;

			 $res_tot_lang += $get_res['gamescore'];	
		}else{
		 	$res_tot_lang += 0.00;
			}
			
		}
		 
		$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;
		$skillQuotient_value = round($tot,2);
	
					 
					 for($i=0;$i<=11;$i++){
					 	$passmonth = date("M", mktime(0, 0, 0, $startdatesplit[1]+$i, $startdatesplit[2], $startdatesplit[0]));
						$passmonthnum = date("m", mktime(0, 0, 0, $startdatesplit[1]+$i, $startdatesplit[2], $startdatesplit[0]));
						$passyear = date("Y", mktime(0, 0, 0, $startdatesplit[1]+$i, $startdatesplit[2], $startdatesplit[0]));
						 
						
						$currmonthdayx= cal_days_in_month(0, $passmonthnum, $passyear) ; 
						
						$displaycatval[]= $passmonth."-".$passyear;
						$displaydataval[]= round($month_array[$passmonthnum][$skillname]["score"]/$month_array[$passmonthnum][$skillname]["count"],2);					
						//array_push($indskillarr,$skillkitdata["sc"]);	
					 }
					 
					
				 echo json_encode(array("data"=>$displaycatval,"values"=>$displaydataval)); exit;
				//echo $displaydataval."@@".$displaycatval; exit;
				
			

	}
	public function skillperformance_report_ajax()
	{			
	
			
$date = time(); 
$day = date('d', $date) ; 
$month = date('m', $date) ; 
$year = date('Y', $date);
$mth = date('M', $date) ; 

//$date1 = getdate();
//$dayCurrent= $date1['day']; 


$dateMonthYearArr = array();

$event_array[][] = "";
$uid = $this->session->user_id;
$user_id = $uid; 
$startyear = $this->session->start_year;
$startmonth = $this->session->start_month;

$start_date = date(''.$startyear.'-m-01'); 
$last_date = date(''.$startyear.'-m-t'); 

$maxDays=date('t');

$event_array = $this->range_date_curstudent($start_date, date('d-m-Y'), $uid);

$data['sql'] = $this->Assessment_model->getcalendar($uid,$start_date,$last_date);	

foreach($data['sql'] as $rsData)
{
	$played_date = $rsData['created_date'];	
	$event_array[$played_date][$uid] = "2";
}
	
$first_day = mktime(0,0,0,$startmonth, 1, $startyear) ;

//This gets us the month name
$title = date('F', $first_day) ;

//Here we find out what day of the week the first day of the month falls on 
$day_of_week = date('D', $first_day) ; 


 //Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero

 switch($day_of_week){ 

 case "Sun": $blank = 0; break; 

 case "Mon": $blank = 1; break; 

 case "Tue": $blank = 2; break; 

 case "Wed": $blank = 3; break; 

 case "Thu": $blank = 4; break; 

 case "Fri": $blank = 5; break; 

 case "Sat": $blank = 6; break; 

 }
	
 $days_in_month = cal_days_in_month(0, $startmonth, $year) ; 
	
echo "<TABLE class='myBrainTable'> ";

 echo "<tr><TD class='CalenderHeader' COLSPAN='7' ALIGN=center><h4>BSPI Calender</h4></TD> </tr>";

 echo "<tr><td width=42 style='color:#497096;'><b>SUN</td></b><td width=42 style='color:#497096;'><b>MON</b></td><td 
		style='color:#497096;' width=42><b>TUE</b></td><td width=42 style='color:#497096;'><b>WED</b></td><td width=42 style='color:#497096;'><b>THU</b></td><td 
		width=42 style='color:#497096;'><b>FRI</b></td><td width=42 style='color:#497096;'><b>SAT</b></td></tr>";



//This counts the days in the week, up to 7

 $day_count = 1;



 echo "<tr>";

//first we take care of those blank days

 while ( $blank > 0 ) 
 { 
	 echo "<td></td>"; 
	 $blank = $blank-1; 
	 $day_count++;
 } 
 
  //sets the first day of the month to 1 

 $day_num = 1;

$curr_day = '';



 //count up the days, untill we've done all of them in the month

 while ( $day_num <= $days_in_month ) 
 { 
    $curr_day = '';
	$val_day = '';
	
    /*if($day == $day_num) { $curr_day = "style='background-color:#FFFF00';"; 
	} */
	
	if(strlen($day_num) <= 1)
	{
	  $day_num = "0".$day_num;
	}
	
	$val_day =  $day_num."/".$month."/".$year;
	
	/*echo $val_day;
	echo "-----";
	echo $day_num."/".$month."/".$year;
	echo "<br/>";*/
	
	if(($day_num == $day)&&($month == $_SESSION['start_month']))
	{
	   $curr_day = "style='background:url(images/line.png) no-repeat right; background-size: 16px 16px;'";
	}
	
	$day_num_str=$day_num;

	if(isset($dayArrPlayedScore[$day_num_str]))
	{
		 $day_num_str = '<font>'.$day_num_str.'</font> / <b><font color="green">'.$dayArrPlayedScore[$day_num].'</font></b>';
		 $curr_day = "style='background:url(img/img/smiley2.png) no-repeat right; background-size: 26px 26px;'";
	}
	elseif((int)$dayCurrent >(int)$day_num)
	{
		//dayArrNotPlayed
		$curr_day = "style='background:url(img/img/cross.png) no-repeat right; background-size: 16px 16px;'";
		$day_num_str = '<font>'.$day_num_str.'</font>';
	}
	else
	{
		$day_num_str = '<font>'.$day_num_str.'</font>';
	}
	
	
	echo "<td ".$curr_day."> $day_num_str </td>"; 
	$day_num++; 
	$day_count++;
	
	//Make sure we start a new row every week
	if ($day_count > 7)
	{
		echo "</tr><tr>";
		$day_count = 1;
	}
  } 
  
  echo "</table>";
  
  echo "<table class='BSPIStatus'>
  <tr class='alert alert-success'><TD COLSPAN='7'><strong><i><img src='img/img/smiley2.png'style='width:26px;height:26;'></i></strong> Brain Trained </TD></tr></table>
  <h5 style='color:#1E991E;'><center><strong></strong></center></h5>"; 
	

	
	//print_r($_POST);  
	 $skillsid = str_replace("multiselect-all,",'',$_POST['hdnskillsid']); 
	// $skillsid = ($_POST['skill'][0].','.$_POST['skill'][1].','.$_POST['skill'][2].','.$_POST['skill'][3].','.$_POST['skill'][4]);exit;
	$userid = $this->session->user_id;
	$month = $_POST['months'];
	
	$data['skpreport'] = $this->Assessment_model->getskpreport($userid,$skillsid,$month); 
	//print_r($data['skpreport']); exit;
		echo $data['json'] = json_encode($data['skpreport']);	exit;
	}
	
	public function mybspi_report()
	{	
	$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);
	$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
		//print_r($data['academicyear']);
		$startdate = $data['academicyear'][0]['startdate'];
		$enddate = $data['academicyear'][0]['enddate'];
		$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
		$this->load->view('headerinner', $data);
	$this->load->view('reports/mybspi_report', $data);
	$this->load->view('footerinner');
	}
	
public function comma_separated_to_array($string, $separator = ',')
{
  //Explode on comma
  $vals = explode($separator, $string);
 
  //Trim whitespace
  foreach($vals as $key => $val) {
    $vals[$key] = trim($val);
  }
  
  return array_diff($vals, array(""));
}
	
	public function mybspi_report_ajax()
	{	
	
	$yeramonths = $this->comma_separated_to_array($_POST['hdnMonthID']);
	//print_r($yeramonths); exit;
	
	 $test = "'" . implode("','", $yeramonths) . "'";
	
	$userid = $this->session->user_id;

	
		//echo $mnths; exit;
		
		 
$data['bspireport'] = $this->Assessment_model->getbspireport1($userid,$test); 
		
//print_r($data['bspireport']);  exit;
//print_r($data['bspireport'][0]['gs_id']);

//echo $data['json'] = json_encode($data['bspireport'][0]);	exit;

$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;

foreach($data['bspireport'] as $get_res)
{
	//echo $get_res['gs_id'];
	//echo $get_res['gamescore'];
	if(($get_res['gs_id']=='59')){
			$res_tot_memory_i++;
			  $res_tot_memory += $get_res['gamescore'];	
		}else{
		 	$res_tot_memory += 0.00;
			}
if(($get_res['gs_id']=='60')){
				$res_tot_vp_i++;

			 	$res_tot_vp += $get_res['gamescore'];
		}else{
			 $res_tot_vp += 0.00;
			}
if(($get_res['gs_id']=='61')){
				$res_tot_fa_i++;

				 $res_tot_fa += $get_res['gamescore'];	
		}else{
			 $res_tot_fa += 0.00;
			}
if(($get_res['gs_id']=='62')){
				$res_tot_ps_i++;

			 $res_tot_ps += $get_res['gamescore'];	
		}else{
			 $res_tot_ps += 0.00;
			}
if(($get_res['gs_id']=='63')){
				$res_tot_lang_i++;

			 $res_tot_lang += $get_res['gamescore'];
		}else{
		 	$res_tot_lang += 0.00;
			}
		//$month_cnt =  count($_POST['option']);
}
if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

echo $bspi = $tot;
	
	}
	
	public function myutilization()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$this->load->view('headerinner',$data);
		$this->load->view('reports/myutilization',$data);
		$this->load->view('footerinner');
			
	}
	
	public function myutilization_ajax()
	{
		 date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST); exit;
		$userid = $this->session->user_id;
		$txtFDate=date("Y-m-d", strtotime($_REQUEST['txtFDate']));	
		$txtTDate=date("Y-m-d", strtotime($_REQUEST['txtTDate']));
		$begin = new DateTime($txtFDate);
		$end = new DateTime($txtTDate);
		$end = $end->modify( '+1 day' ); 

		$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
		
		$data['myutilization'] = $this->Assessment_model->getstudentplay($userid,$txtFDate,$txtTDate); 
		
		//print_r($data['myutilization']); exit;
		$this->load->view('reports/myutilization_ajax', $data); 
	}
	
	public function performance_report()
	{	
	$this->load->view('header');
	$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
		//print_r($data['academicyear']);
		$startdate = $data['academicyear'][0]['startdate'];
		$enddate = $data['academicyear'][0]['enddate'];
		$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);
	$this->load->view('reports/performance_report', $data);
	$this->load->view('footer');
	}
	
	
	
	public function class_performace()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		
		
		$userid = $this->session->user_id;
		$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	
	$data['academicmonths'] = $this->Assessment_model->getacademicmonths($startdate,$enddate);

	
	$data['SetDetails2']=$this->Assessment_model_set2->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
	 
	$data['SetDetails3']=$this->Assessment_model_set3->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
	
	$data['GradewiseScore_data1'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports');
	$data['GradewiseScore_data2'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports_set2');
	$data['GradewiseScore_data3'] = $this->Assessment_model->getClassPerformace_data($this->session->school_id,$this->session->game_grade,$this->session->section,'game_reports_set3');
	
	$data['GradewiseScore_data_overall'] = $this->Assessment_model->getClassPerformace_data_overall($this->session->school_id,$this->session->game_grade,$this->session->section,$data['SetDetails2'][0]['planid'],$data['SetDetails3'][0]['planid']);
	
	/*
	$data['GradewiseScore1'] = $this->Assessment_model->getGradewiseScore($this->session->school_id,$this->session->game_grade,$this->session->section);
	$data['GradewiseScore2'] = $this->Assessment_model->getGradewiseScore($this->session->school_id,$this->session->game_grade,$this->session->section);
	$data['GradewiseScore3'] = $this->Assessment_model->getGradewiseScore($this->session->school_id,$this->session->game_grade,$this->session->section);
		$ini=1;
	foreach($data['GradewiseScore1'] as $objGradeWiseReport)
		{
			//$GradeWiseReport[$ini]=array_merge($objGradeWiseReport,$this->userRowData($objGradeWiseReport['user_id'],'1'));
			$GradeWiseReport1[$objGradeWiseReport["user_id"]]=array("avatarimage"=>$objGradeWiseReport["avatarimage"],"id"=>$objGradeWiseReport["user_id"],"name"=>$objGradeWiseReport["fname"],"bspi"=>$this->userRowData($objGradeWiseReport['user_id'],'1'));
			$ini++;
		}
		$ini=1;
		foreach($data['GradewiseScore2'] as $objGradeWiseReport)
		{
			//$GradeWiseReport[$ini]=array_merge($objGradeWiseReport,$this->userRowData($objGradeWiseReport['user_id'],'1'));
			$GradeWiseReport2[$objGradeWiseReport["user_id"]]=array("avatarimage"=>$objGradeWiseReport["avatarimage"],"id"=>$objGradeWiseReport["user_id"],"name"=>$objGradeWiseReport["fname"],"bspi"=>$this->userRowData($objGradeWiseReport['user_id'],'2'));
			$ini++;
		}
		 
		$ini=1;
		foreach($data['GradewiseScore3'] as $objGradeWiseReport)
		{
			//$GradeWiseReport[$ini]=array_merge($objGradeWiseReport,$this->userRowData($objGradeWiseReport['user_id'],'1'));
			$GradeWiseReport3[$objGradeWiseReport["user_id"]]=array("avatarimage"=>$objGradeWiseReport["avatarimage"],"id"=>$objGradeWiseReport["user_id"],"name"=>$objGradeWiseReport["fname"],"bspi"=>$this->userRowData($objGradeWiseReport['user_id'],'3'));
			$ini++;
		}
		
		
		

$GradeWiseReport1=$this->aasort($GradeWiseReport1, 'bspi');
$GradeWiseReport2=$this->aasort($GradeWiseReport2, 'bspi');
$GradeWiseReport3=$this->aasort($GradeWiseReport3, 'bspi');
  */
  
  $su1=array_unique(array_column($data['GradewiseScore_data1'],'bspi'));
  $su2=array_unique(array_column($data['GradewiseScore_data2'],'bspi'));
  $su3=array_unique(array_column($data['GradewiseScore_data3'],'bspi'));
  $su_overall=array_unique(array_column($data['GradewiseScore_data_overall'],'bspi'));
  
  
 $gu1=array_search($this->session->user_id,array_column($data['GradewiseScore_data1'],'id'));
 $gu2=array_search($this->session->user_id,array_column($data['GradewiseScore_data2'],'id'));
 $gu3=array_search($this->session->user_id,array_column($data['GradewiseScore_data3'],'id'));
 $gu_overall=array_search($this->session->user_id,array_column($data['GradewiseScore_data_overall'],'id'));

  $data['GradeWiseReport1']=$data['GradewiseScore_data1'];
  $data['GradeWiseReport2']=$data['GradewiseScore_data2'];
  $data['GradeWiseReport3']=$data['GradewiseScore_data3'];
  $data['GradeWiseReport_overall']=$data['GradewiseScore_data_overall'];
  
  
  $su1_r=array_reverse(array_merge($su1));
  $su2_r=array_reverse(array_merge($su2));
  $su3_r=array_reverse(array_merge($su3));
  $su_overall_r=array_reverse(array_merge($su_overall));
  
  $cus1=$data['GradewiseScore_data1'][$gu1];
  $cus2=$data['GradewiseScore_data2'][$gu2];
  $cus3=$data['GradewiseScore_data3'][$gu3];
  $cus_overall=$data['GradewiseScore_data_overall'][$gu_overall];
  
   
  
	/*if(isset($data['GradewiseScore_data1'][$gu1+6])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1+6];}
	if(isset($data['GradewiseScore_data1'][$gu1+5])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1+5];}
	if(isset($data['GradewiseScore_data1'][$gu1+4])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1+4];}
	if(isset($data['GradewiseScore_data1'][$gu1+3])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1+3];}
	if(isset($data['GradewiseScore_data1'][$gu1+2])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1+2];}
	if(isset($data['GradewiseScore_data1'][$gu1+1])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1+1];}
	if(isset($data['GradewiseScore_data1'][$gu1])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1];$cus1=$data['GradewiseScore_data1'][$gu1];}
	if(isset($data['GradewiseScore_data1'][$gu1-1])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1-1];}
	if(isset($data['GradewiseScore_data1'][$gu1-2])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1-2];}
	if(isset($data['GradewiseScore_data1'][$gu1-3])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1-3];}
	if(isset($data['GradewiseScore_data1'][$gu1-4])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1-4];}
	if(isset($data['GradewiseScore_data1'][$gu1-5])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1-5];}
	if(isset($data['GradewiseScore_data1'][$gu1-6])){$data['GradeWiseReport1'][]= $data['GradewiseScore_data1'][$gu1-6];} 
	*/
	
  /*
	if(isset($data['GradewiseScore_data2'][$gu2+6])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2+6];}
	if(isset($data['GradewiseScore_data2'][$gu2+5])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2+5];}
	if(isset($data['GradewiseScore_data2'][$gu2+4])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2+4];}
	if(isset($data['GradewiseScore_data2'][$gu2+3])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2+3];}
	if(isset($data['GradewiseScore_data2'][$gu2+2])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2+2];}
	if(isset($data['GradewiseScore_data2'][$gu2+1])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2+1];}
	if(isset($data['GradewiseScore_data2'][$gu2])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2];$cus2=$data['GradewiseScore_data2'][$gu2];}
	if(isset($data['GradewiseScore_data2'][$gu2-1])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2-1];}
	if(isset($data['GradewiseScore_data2'][$gu2-2])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2-2];}
	if(isset($data['GradewiseScore_data2'][$gu2-3])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2-3];}
	if(isset($data['GradewiseScore_data2'][$gu2-4])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2-4];}
	if(isset($data['GradewiseScore_data2'][$gu2-5])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2-5];}
	if(isset($data['GradewiseScore_data2'][$gu2-6])){$data['GradeWiseReport2'][]= $data['GradewiseScore_data2'][$gu2-6];} 
	*/
	
  /*
	if(isset($data['GradewiseScore_data3'][$gu3+6])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3+6];}
	if(isset($data['GradewiseScore_data3'][$gu3+5])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3+5];}
	if(isset($data['GradewiseScore_data3'][$gu3+4])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3+4];}
	if(isset($data['GradewiseScore_data3'][$gu3+3])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3+3];}
	if(isset($data['GradewiseScore_data3'][$gu3+2])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3+2];}
	if(isset($data['GradewiseScore_data3'][$gu3+1])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3+1];}
	if(isset($data['GradewiseScore_data3'][$gu3])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3];$cus3=$data['GradewiseScore_data3'][$gu3];}
	if(isset($data['GradewiseScore_data3'][$gu3-1])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3-1];}
	if(isset($data['GradewiseScore_data3'][$gu3-2])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3-2];}
	if(isset($data['GradewiseScore_data3'][$gu3-3])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3-3];}
	if(isset($data['GradewiseScore_data3'][$gu3-4])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3-4];}
	if(isset($data['GradewiseScore_data3'][$gu3-5])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3-5];}
	if(isset($data['GradewiseScore_data3'][$gu3-6])){$data['GradeWiseReport3'][]= $data['GradewiseScore_data3'][$gu3-6];}
	 */
  /*
	if(isset($data['GradewiseScore_data_overall'][$gu_overall+6])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall+6];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall+5])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall+5];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall+4])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall+4];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall+3])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall+3];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall+2])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall+2];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall+1])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall+1];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall];$cus_overall=$data['GradewiseScore_data_overall'][$gu_overall];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall-1])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall-1];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall-2])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall-2];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall-3])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall-3];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall-4])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall-4];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall3-5])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall-5];}
	if(isset($data['GradewiseScore_data_overall'][$gu_overall-6])){$data['GradeWiseReport_overall'][]= $data['GradewiseScore_data_overall'][$gu_overall-6];}
	 */
	 
	 
	 $data['gu1']=$gu1;
	 $data['gu2']=$gu2;
	 $data['gu3']=$gu3;
	 $data['gu_overall']=$gu_overall;
	 $data['su1']=$su1;
	 $data['su2']=$su2;
	 $data['su3']=$su3;
	 $data['su_overall']=$su_overall;
	 $data['su1_r']=$su1_r;
	 $data['su2_r']=$su2_r;
	 $data['su3_r']=$su3_r;
	 $data['su_overall_r']=$su_overall_r;
	 $data['cus1']=$cus1;
	 $data['cus2']=$cus2;
	 $data['cus3']=$cus3;
	 $data['cus_overall']=$cus_overall;
	 
	  
	 
	 
	  
		//$data['GradeWiseReport1']=$GradeWiseReport1;
		//$data['GradeWiseReport2']=$GradeWiseReport2;
		//$data['GradeWiseReport3']=$GradeWiseReport3;
		//$this->load->view('headerinner',$data);
		$this->load->view('reports/class_performace',$data);
		//$this->load->view('footerinner');
			
	}
	function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
     $array=$ret;
	 return ($array);
	  
}

	
	public function userRowData($uid,$setID)
		{
			$gametable="game_reports";
			if($setID==1){$gametable="game_reports";}
			else if($setID==2){$gametable="game_reports_set2";}
			else if($setID==3){$gametable="game_reports_set3";}
			$req_academicMonths=1;
			{$hdnSkillID='59,60,61,62,63';}
			$monthcondition='';
			 
		$get_bspi_rows = $this->Assessment_model->getGradewiseData($hdnSkillID,$uid,$monthcondition,$gametable);

		 
		
		
			$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
			$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
			$month_array=$month_array_chart=array();
			 
			foreach($get_bspi_rows as $get_res)
			{
				 
				$month_array[$get_res['playedMonth']][$get_res['gs_id']]['score']+=$get_res['gscore'];
	if(!isset($month_array[$get_res['playedMonth']][$get_res['gs_id']]['count'])){$month_array[$get_res['playedMonth']][$get_res['gs_id']]['count']=0;}
	$month_array[$get_res['playedMonth']][$get_res['gs_id']]['count']+=1;
	$month_array_chart[$get_res['playedMonth']][$get_res['gs_id']]=$month_array[$get_res['playedMonth']][$get_res['gs_id']]['score']/$month_array[$get_res['playedMonth']][$get_res['gs_id']]['count'];
	
				if(($get_res['gs_id']=='59')){
					$res_tot_memory_i++;
					$res_tot_memory += $get_res['gscore'];	
					}
				else{$res_tot_memory += 0.00;}
				if(($get_res['gs_id']=='60')){
					$res_tot_vp_i++;
					$res_tot_vp += $get_res['gscore'];	
				}else{$res_tot_vp += 0.00;}
				if(($get_res['gs_id']=='61')){
					$res_tot_fa_i++;
					$res_tot_fa += $get_res['gscore'];	
				}else{$res_tot_fa += 0.00;}
				if(($get_res['gs_id']=='62')){
					$res_tot_ps_i++;
					$res_tot_ps += $get_res['gscore'];	
				}else{$res_tot_ps += 0.00;}
				if(($get_res['gs_id']=='63')){
					$res_tot_lang_i++;
					$res_tot_lang += $get_res['gscore'];	
				}else{$res_tot_lang += 0.00;}

			}
			$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;
			$skillQuotient_value = round($tot,2);
			/*
			$innwer_arr1=$innwer_arr2=$innwer_arr1_skill=array();
		foreach($month_array_chart as $key=>$month_arr_chart)
		{
			$innwer_arr1[$key]=0;
			if(!isset($innwer_arr1[$key])){$innwer_arr1[$key]=0;}
			
			foreach($month_arr_chart as $key11=>$chart)
			{if(!isset($innwer_arr1_skill[$key11])){$innwer_arr1_skill[$key11]=0;}
			
				$innwer_arr1[$key]+=$chart;
				$innwer_arr1_skill[$key11]+=$chart;
			}
		}
foreach($innwer_arr1 as $key=>$value)
{
	$innwer_arr2[$key]=round(($value/5),2);
}
$OverallPerformance_value=$innwer_arr2;
*/

			
			//$cccc=array('memory'=>round($innwer_arr1_skill['59']/count($req_academicMonths),2),'visualprocessing'=>round($innwer_arr1_skill['60']/count($req_academicMonths),2),'focusattention'=>round($innwer_arr1_skill['61']/count($req_academicMonths),2),'problemsolving'=>round($innwer_arr1_skill['62']/count($req_academicMonths),2),'linguistics'=>round($innwer_arr1_skill['63']/count($req_academicMonths),2),'bspi'=>($skillQuotient_value));
			
			 
			return $skillQuotient_value;
			
		}
	 function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( ! isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( ! isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
	public function totalstars()
		{
			$userid = $this->session->user_id;
			
				$data['Starcount1'] = $this->Assessment_model->getstarcount($userid);
			$data['Starcount2'] = $this->Assessment_model_set2->getstarcount($userid);
			$data['Starcount3'] = $this->Assessment_model_set3->getstarcount($userid);
			
			//echo '<pre>';
			//print_r($data['Starcount1']);
			foreach($data['Starcount1'] as $starscount1)
		{
			if($starscount1['game_score']==""){$starscount1['game_score']=0;}
			
	if($starscount1['game_score'] < 20) $filled_stars = 0;
	if($starscount1['game_score']>= 20 && $starscount1['game_score'] <= 40)	$data['starone'] = $filled_stars+= 1;
	if($starscount1['game_score']>= 41 && $starscount1['game_score'] <= 60)	$data['starone'] = $filled_stars+= 2;
	if($starscount1['game_score']>= 61 && $starscount1['game_score'] <= 80)	$data['starone'] = $filled_stars+= 3;
	if($starscount1['game_score']>= 81 && $starscount1['game_score'] <= 90)	$data['starone'] = $filled_stars+= 4;
	if($starscount1['game_score']>= 91 && $starscount1['game_score'] <= 100) $data['starone'] = $filled_stars+= 5;
		}
		
		foreach($data['Starcount2'] as $starscount2)
		{
			if($starscount2['game_score']==""){$starscount2['game_score']=0;}
			
	if($starscount2['game_score'] < 20) $filled_stars = 0;
	if($starscount2['game_score']>= 20 && $starscount2['game_score'] <= 40)	$data['startwo'] = $filled_stars_two+= 1;
	if($starscount2['game_score']>= 41 && $starscount2['game_score'] <= 60)	$data['startwo'] = $filled_stars_two+= 2;
	if($starscount2['game_score']>= 61 && $starscount2['game_score'] <= 80)	$data['startwo'] = $filled_stars_two+= 3;
	if($starscount2['game_score']>= 81 && $starscount2['game_score'] <= 90)	$data['startwo'] = $filled_stars_two+= 4;
	if($starscount2['game_score']>= 91 && $starscount2['game_score'] <= 100) $data['startwo'] = $filled_stars_two+= 5;
		}
		
		foreach($data['Starcount3'] as $starscount3)
		{
			if($starscount3['game_score']==""){$starscount3['game_score']=0;}
			
	if($starscount3['game_score'] < 20) $filled_stars = 0;
	if($starscount3['game_score']>= 20 && $starscount3['game_score'] <= 40)	$data['starthree'] = $filled_stars_three+= 1;
	if($starscount3['game_score']>= 41 && $starscount3['game_score'] <= 60)	$data['starthree'] = $filled_stars_three+= 2;
	if($starscount3['game_score']>= 61 && $starscount3['game_score'] <= 80)	$data['starthree'] = $filled_stars_three+= 3;
	if($starscount3['game_score']>= 81 && $starscount3['game_score'] <= 90)	$data['starthree'] = $filled_stars_three+= 4;
	if($starscount3['game_score']>= 91 && $starscount3['game_score'] <= 100) $data['starthree'] = $filled_stars_three+= 5;
		}

			$this->load->view('reports/totalstars',$data);
		}
	public function downloadimg()
	{ header('Content-Type: image/png');
		$data['academicyear'] = $this->Assessment_model->getacademicyear(); 
		$startdate = $data['academicyear'][0]['startdate'];
		$enddate = $data['academicyear'][0]['enddate'];
		$userid = $this->session->user_id;
		/* Basic Info start*/
			$arrgrade_name=$this->Assessment_model->getgradedetais($this->session->game_grade);
			$arrschool_name=$this->Assessment_model->getschooldetais($this->session->school_id);
			$name=$this->session->fullname;
			$grade_name=$arrgrade_name[0]['classname'];	
			$school_name=$arrschool_name[0]['school_name'];	
			//echo $name."==".$grade_name."==".$school_name;exit;
		/* Basic Info end */
		/* BSPI Score */
			$arrBSPIScore=$this->Assessment_model->getbspicomparison($this->session->school_id,$this->session->game_grade,$userid);
			$BSPIScore=$arrBSPIScore[0]['avgbspiset1'];
		/* BSPI Score ENDS */
		
/*Total STARS Starts*/
	 $userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		
	 $data['randomGames']=$this->Assessment_model->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$where = "";
		foreach($data['randomGames'] as $brainData )
		{
			$brainIds[] = $brainData['gid'];
			$active_game_ids = @implode(',',$brainIds);
			$where = " and g.gid in ($active_game_ids)";
		}
		$data['actualGames'] = $this->Assessment_model->getActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
	 
		foreach($data['actualGames'] as $games)
		{
			if($games['tot_game_score']==""){$games['tot_game_score']=0;}
		if($games['tot_game_played']=="" || $games['tot_game_played']==0 ){$tot_game_played=1;}else{$myskillpie[$games['skill_id']]=$myskillpie_orginal[$games['skill_id']];$tot_game_played=$games['tot_game_played'];}
		 
		$avg_game_score=$games['tot_game_score']/$tot_game_played;
			if($avg_game_score < 20) $filled_stars = 0;
			if($avg_game_score >= 20 && $avg_game_score <= 40)	$filled_stars = 1;
			if($avg_game_score >= 41 && $avg_game_score <= 60)	$filled_stars = 2;
			if($avg_game_score >= 61 && $avg_game_score <= 80)	$filled_stars = 3;
			if($avg_game_score >= 81 && $avg_game_score <= 90)	$filled_stars = 4;
			if($avg_game_score >= 91 && $avg_game_score <= 100)	$filled_stars = 5;
			
			$sum+= $filled_stars;
		}
/*STARS End*/
	
/* Skill Wise Scores */
			 $bspicalendarskillScore= $this->Assessment_model->mybspicalendarSkillChart1("",$userid);
			 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
			 foreach($bspicalendarskillScore as $score)
			{$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);}
			 $skillScore=$mybspiCalendarSkillScore;
			 
/* Skill Wise Scores */
$Greeting1='Wow !!!';
$Greeting2='Good !!!';
$Greeting3='Nice !!!';
$Greeting4='Super !!!';
$Greeting5='Great !!!';
$Greeting6='Excellent !!!';
$Greeting7='Wonderful !!!';
$Greeting8='Awesome !!!';
$Greeting9='Terrific !!!';
$Greeting10='Fantasticc !!!';
//$BackgroundImage= base_url().'assets/images/GTec/gtec'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';
$BackgroundImage= '/mnt/vol1/sites/gtec/assess/assets/images/GTec/gtec'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';

 $img = $this->LoadPNG($BackgroundImage,$name,$grade_name,$school_name,$sum,$BSPIScore,$skillScore['SID59'],$skillScore['SID60'],$skillScore['SID61'],$skillScore['SID62'],$skillScore['SID63'],$Greeting1,$Greeting2,$Greeting3,$Greeting4,$Greeting5,$Greeting6,$Greeting7,$Greeting8,$Greeting9,$Greeting10);

 imagepng($img);
imagedestroy($img);	
		
}	
	public function LoadPNG($fBackgroundImage,$name,$grade_name,$school_name,$fTotalScore,$fBSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$fGreeting1,$fGreeting2,$fGreeting3,$fGreeting4,$fGreeting5,$fGreeting6,$fGreeting7,$fGreeting8,$fGreeting9,$fGreeting10)
	{ //echo $fBSPIScore;exit;
		 /* Attempt to open */
    $im = @imagecreatefrompng($fBackgroundImage);
    /* See if it failed */
    if(!$im)
    {
      echo "Loading bg has issue ".fBackgroundImage;
    }
	 
	// Create some colors
	$white = imagecolorallocate($im, 255, 255, 255);
	$grey = imagecolorallocate($im, 128, 128, 128);
	$black = imagecolorallocate($im, 0, 0, 0);


	// The text to draw
	// Replace path by your own font path
	//$font = APPPATH."../assets/fonts/OpenSansBold.ttf";
	 $font = "/mnt/vol1/sites/gtec/assess/assets/fonts/OpenSansBold.ttf";
	// Add some shadow to the text
	$text_color = imagecolorallocate($im, 213, 91, 0);
	/* imagestring($im, 5, 140, 66, str_pad($name,50," ",STR_PAD_BOTH), $text_color);
	imagestring($im, 5, 140, 87, str_pad($grade_name,50," ",STR_PAD_BOTH), $text_color);
	imagestring($im, 5, 212, 105, str_pad($school_name,50," ",STR_PAD_BOTH), $text_color); */
	imagestring($im, 5,300, 75,"Name:", $black);
	imagestring($im, 5, 350, 75,$name, $text_color);
	imagestring($im, 5,292, 95,"Grade:", $black);
	imagestring($im, 5, 341, 95,str_replace('Grade','',$grade_name), $text_color);
	/* BSPIScore */ 
	$text_color = imagecolorallocate($im, 255, 255, 255);
	if (strlen($fBSPIScore) == 1)
    {
     imagettftext ($im, 25, 0, 190, 375, $text_color, $font, $fBSPIScore);
    }

	 elseif (strlen($fBSPIScore) > 2)
    {
     imagettftext ($im, 25, 0, 165, 375, $text_color, $font, $fBSPIScore);
    }
	
		elseif (strlen($fBSPIScore) > 3)
    {
     imagettftext ($im, 25, 0, 170, 375, $text_color, $font, $fBSPIScore);
    }
	else
    {
     imagettftext ($im, 25, 0, 180, 375, $text_color, $font, $fBSPIScore);
    }

/* TotalScore */ 
    if (strlen($fTotalScore) == 1)
    {
    imagettftext ($im, 50, 0, 570, 300, $text_color, $font, $fTotalScore);
    }
    elseif (strlen($fTotalScore) > 2)
    {
     imagettftext ($im, 50, 0, 530, 300, $text_color, $font, $fTotalScore);
    }
	else
    {
     imagettftext ($im, 50, 0, 550, 300, $text_color, $font, $fTotalScore);
    }
	
	
	
	/* MemoryScore */ 
	$text_color = imagecolorallocate($im, 255, 0, 0);
	
	if (strlen($fMemoryScore) == 1)
    {
      imagettftext ($im, 30, 0, 237, 505, $text_color, $font, $fMemoryScore);
    }
    elseif (strlen($fMemoryScore) > 2)
    {
     imagettftext ($im, 30, 0, 215, 505, $text_color, $font, $fMemoryScore);
    }
	else
    {
      imagettftext ($im, 30, 0, 227, 505, $text_color, $font, $fMemoryScore);
    }
	/* VisualProcessingScore */ 
	$text_color = imagecolorallocate($im, 255, 153, 51);
	
	if (strlen($fVisualProcessingScore) == 1)
	{
   
    imagettftext ($im, 30, 0, 350, 505, $text_color, $font, $fVisualProcessingScore);
   }
    elseif (strlen($fVisualProcessingScore) > 2)
    {
      imagettftext ($im, 30, 0, 327, 505, $text_color, $font, $fVisualProcessingScore);
    }
	else
    {
       imagettftext ($im, 30, 0, 337, 505, $text_color, $font, $fVisualProcessingScore);
    }
   /* FocusAndAttentionScore */ 
	$text_color = imagecolorallocate($im, 102, 204, 0);
	
	if (strlen($fFocusAndAttentionScore) == 1)
   {
    imagettftext ($im, 30, 0, 460, 505, $text_color, $font, $fFocusAndAttentionScore);
   }
    elseif (strlen($fFocusAndAttentionScore) > 2)
    {
      imagettftext ($im, 30, 0, 435, 505, $text_color, $font, $fFocusAndAttentionScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 448, 505, $text_color, $font, $fFocusAndAttentionScore);
    }
   /* ProblemSolvingScore */ 
	$text_color = imagecolorallocate($im, 230, 107, 25);
	
	if (strlen($fProblemSolvingScore) == 1)
  {
   imagettftext ($im, 30, 0, 570, 505, $text_color, $font, $fProblemSolvingScore);
   }
  elseif (strlen($fProblemSolvingScore) > 2)
    {
      imagettftext ($im, 30, 0, 545, 505, $text_color, $font, $fProblemSolvingScore);
    }
	else
    {
       imagettftext ($im, 30, 0, 558, 505, $text_color, $font, $fProblemSolvingScore);
    }
   /* LinguisticsScore */ 
	$text_color = imagecolorallocate($im, 51, 204, 255);
	
	if (strlen($fLinguisticsScore) == 1)
  {
  imagettftext ($im, 30, 0, 680, 505, $text_color, $font, $fLinguisticsScore);
  }
  elseif (strlen($fLinguisticsScore) > 2)
    {
      imagettftext ($im, 30, 0, 657, 505, $text_color, $font, $fLinguisticsScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 670, 505, $text_color, $font, $fLinguisticsScore);
    }
	/* Greetings Range */
	$text_color = imagecolorallocate($im, 255, 255, 255);
	
 if ($fBSPIScore < 11)
  {
  imagestring($im, 5, 490, 87, str_pad($fGreeting1,40," ",STR_PAD_BOTH), $text_color);
  }
  elseif ($fBSPIScore < 21)
    {
		imagestring($im, 5, 490, 87, str_pad($fGreeting2,40," ",STR_PAD_BOTH), $text_color);
    }
	elseif ($fBSPIScore < 31)
    {
		imagestring($im, 5, 490, 87, str_pad($fGreeting3,40," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 41)
    {
		imagestring($im, 5, 490, 87, str_pad($fGreeting4,40," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 51)
    {
		imagestring($im, 5, 490, 87, str_pad($fGreeting5,40," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 61)
    {
		imagestring($im, 5, 490, 87, str_pad($fGreeting6,40," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 71)
    {
		imagestring($im, 5, 490, 87, str_pad($fGreeting7,40," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 81)
    {
		imagestring($im, 5, 500, 87, str_pad($fGreeting8,40," ",STR_PAD_BOTH), $text_color);
    }
	elseif ($fBSPIScore < 91)
    {
		imagestring($im, 5, 500, 87, str_pad($fGreeting9,40," ",STR_PAD_BOTH), $text_color);
    }
	else
    {
		imagestring($im, 5, 500, 87, str_pad($fGreeting10,40," ",STR_PAD_BOTH), $text_color);
    }


	
    return $im;
	}

}
