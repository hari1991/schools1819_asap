var container1
var questionTxtR;
var attemptTxtR;
var correctTxtR;
var responseTxtR;
var scoreTxtR;
var getCorrectStr = "";
var removeSkillangesTweenMc
var rightCnt = -1, wrongCnt = -1;
var animEndCnt = 0,validCnt = 0;
var pickQuesInterval
var es1 = [2, 0, 3, 4, 1, 4, 2, 0, 1, 3]
var es2 = [5, 6, 7, 6, 5, 7, 7, 6, 5, 6]
es1.sort(randomSort);
//es2.sort(randomSort);


function startAnimationHandler(evt){
 // handleClick(null)
  //  console.log("checkAnim= "+evt.currentTarget.currentFrame+" checkTotal Frames - "+evt.currentTarget.totalFrames)
   if(evt.currentTarget.currentFrame == evt.currentTarget.totalFrames-45){
        gameIntroAnimMc.removeEventListener("tick",startAnimationHandler)
        introStartBtn.addEventListener("click", createDelayToStartGame);
        gameIntroAnimMc.cursor = "pointer";
        introStartBtn.visible = true;
        introStartBtn.cursor = "pointer";
      //  introStartBtn.x = 850;introStartBtn.y = 25; 
        container.parent.addChild(introStartBtn)
        customAnimation()
    }
}
//=============================================================================//
function createDelayToStartGame(){
    introStartBtn.removeEventListener("click", createDelayToStartGame);
    introStartBtn.mouseEnabled = false;
    introStartBtn.visible = false;

     gameIntroAnimMc.stop()
     setTimeout(handleClick, 1000);
}
//=============================================================================//

function customAnimation(){
   
    //Happy Birthday Theme
    /*
    removeSkillangesTweenMc = new confettiKit({
        confettiCount: 100,
        angle: 80,
        startVelocity: 100,
        colors: randomColor({hue: 'blue',count: 50}),
        elements: {
            'confetti': {
                direction: 'down',
                rotation: true,
            },
            'star': {
                count: 50,
                direction: 'down',
                rotation: true,
            },
            'ribbon': {
                count: 50,
                direction: 'down',
                rotation: true,
            },
            'custom': [{
                count: 2,
                width: 100,
                textSize: 80,
                content: 'assets/images/Balloon.png',
                contentType: 'image',
                direction: 'up',
                rotation: false,
            }]
        },
        position: 'bottomLeftRight',
    });

   // 
   console.log("removeSkillangesTweenMc= "+removeSkillangesTweenMc)
   */

  new confettiKit({
        confettiCount: 50,
        angle: 90,
        startVelocity: 100,
        colors: randomColor({hue: 'blue',count: 50}),
        elements: {
            'confetti': {
                direction: 'down',
                rotation: true,
            },
            'star': {
                count: 10,
                direction: 'down',
                rotation: true,
            },
            'ribbon': {
                count: 10,
                direction: 'down',
                rotation: true,
            },
            'custom': [{
                count: 2,
                width: 100,
                textSize: 40,
                content: 'assets/images/Balloon.png',
                contentType: 'image',
                direction: 'down',
                rotation: false,
            }]
        },
        position: 'bottomLeftRight',
    });
}
 

function animationEndHandler(e) {
    animEndCnt++
    
    if (animEndCnt == 1 ) {
        animEndCnt = 0
        if(getCorrectStr == ""){
            console.log("coming...")
            gameIntroAnimMc.addEventListener("tick",startAnimationHandler)
        }else{
            validCnt++
            console.log("get validation= "+validCnt)
            if(validCnt == 2){
                
                getCorrectValidation();
            }
        }
    }
   
}



//=========================================================================//
function getValidation(aStr) {
    boardMc.boardMc.mouseEnabled = false;
    console.log("es1 " + es1)
    console.log("es2 " + es2)
    getCorrectStr = aStr

    if (aStr == "correct") {
        rightCnt++;
        console.log(rightCnt)
        ccnt = ccnt + 1;
        calculatescore();
        crst = crst + rst;

        boardMc.boardMc.scoreMc.txt.text = score + "";
        customAnimation()
        createAnimationSound()

    } else {
        wrongCnt++;
        console.log("wrongSoundCnt " + wrongCnt)
        rst1 = rst1 + rst;
        wrst = wrst + rst;
        getWrongValidation()
      
        if (!isEffSound) {
             wrongSnd.play();
             wrongSnd.volume = 0;
        } else {
            wrongSnd.play();
            wrongSnd.volume = 1;
        }
       // wrongSnd.addEventListener("complete", handleComplete);
    }

    if (rst == 0) {
        rst = 1
    }
    responseTime += rst;
    console.log("responseTime= " + responseTime)
    answeredQuestions += 1;
}
//=============================================================================//
function createAnimationSound(){
    console.log("rightCnt " + rightCnt)
    if (!isEffSound) {
            correctSnd.play();
            correctSnd.volume = 0;
        } else {
            correctSnd.play();
            correctSnd.volume = 1;
        }
     //  correctSnd.addEventListener("complete", handleComplete);
}
//=============================================================================//
function getCorrectValidation(){
        console.log("getCorrectValidation")
        validCnt = 0
        if(time !=0){
            if (container.parent) {
                ParrotAnimationTesting.visible = true;
                container.parent.addChild(ParrotAnimationTesting)
                ParrotAnimationTesting["_mc"].gotoAndStop(es1[rightCnt]);
            }
        }else{
            ParrotAnimationTesting.visible = false;
        }

      
        stage.update()

        pickQuesInterval = setInterval(handleComplete, 1000)
        
}
//=============================================================================//
function getWrongValidation(){
    console.log("getWrongValidation")
    validCnt = 0;
     if(time !=0){
         if (container.parent) {
            ParrotAnimationTesting.visible = true;
            container.parent.addChild(ParrotAnimationTesting)
            ParrotAnimationTesting["_mc"].gotoAndStop(es2[wrongCnt]);
         }
     }else{
         ParrotAnimationTesting.visible = false;
     }
    stage.update()
    pickQuesInterval = setInterval(handleComplete, 1000)
}
//=================================================================================================================//
function handleComplete() {
    clearInterval(pickQuesInterval);
    console.log("answ ====== = "+answeredQuestions +" cnt = "+cnt )
    
    if (cnt < totalQuestions - 1) {
        console.log("get new questions")

        ParrotAnimationTesting.visible = false;
        //restartTimer();
        boardMc.boardMc.mouseEnabled = true;

        pickques();
        
    } else {
        console.log(" Game Finished ")
        clearInterval(interval);
        if (container.parent) {
            gameOverSnd.play();
            gameOverSnd.volume = 1;
            ParrotAnimationTesting.visible = true;
            container.parent.addChild(ParrotAnimationTesting)
            ParrotAnimationTesting["_mc"].gotoAndStop(8);
            stage.update()
            gameOverSnd.addEventListener("complete", handleComplete1);
        }
    }
}
 
function handleComplete1(e) {

    clearInterval(interval);
    gameResponseTimerStop();
    correctSnd.stop();
    wrongSnd.stop();
    gameOverSnd.stop();
    

    if (container.parent) {
        container.parent.removeAllChildren();
    }

    container1 = new createjs.Container();
    stage.addChild(container1)
    container1.parent.addChild(resultLoading)
    


    computeresult();
}

//=================================================================================================================//
function computeresult() {

    stage.mouseEnabled = false;

    helpMc.helpMc.visible = false;
    var responseValue = 1;

    tqcnt = 10;
    aqcnt = answeredQuestions;
    if (aqcnt > 10) {
        aqcnt = 10;
    }
    cqcnt = ccnt;
    gscore = score;

    if (time == 0)
        gtime = 180;
    else
        gtime = time;

    rtime = responseTime;
    crtime = crst;
    wrtime = wrst;


//    removeFullScreen()



    htmlRedirect(nav, url, tqcnt, aqcnt, cqcnt, gscore, gtime, rtime, crtime, wrtime)
/*
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    questionTxtR = new createjs.Text(tqcnt, "bold 35px Veggieburger-Bold", "white");
    container1.parent.addChild(questionTxtR);
    questionTxtR.x = 732; questionTxtR.y = 153;
    questionTxtR.textAlign = "center";
    questionTxtR.textBaseline = "middle";

    //
    attemptTxtR = new createjs.Text(aqcnt, "bold 35px Veggieburger-Bold", "white");
    container1.parent.addChild(attemptTxtR);
    attemptTxtR.x = 735; attemptTxtR.y = 267;
    attemptTxtR.textAlign = "center";
    attemptTxtR.textBaseline = "middle";

    correctTxtR = new createjs.Text(cqcnt, "bold 35px Veggieburger-Bold", "white");
    container1.parent.addChild(correctTxtR);
    correctTxtR.x = 732; correctTxtR.y = 375;
    correctTxtR.textAlign = "center";
    correctTxtR.textBaseline = "middle";
    //
    responseTxtR = new createjs.Text(rtime, "bold 35px Veggieburger-Bold", "#FF9900");
    container1.parent.addChild(responseTxtR);
    responseTxtR.x = 410; responseTxtR.y = 570;
    responseTxtR.textAlign = "center";
    responseTxtR.textBaseline = "middle";

    //
 
    scoreTxtR = new createjs.Text(gscore, "bold 35px Veggieburger-Bold", "#FF9900");
    container1.parent.addChild(scoreTxtR);
    scoreTxtR.x = 840; scoreTxtR.y = 560;
    scoreTxtR.textAlign = "center";
    scoreTxtR.textBaseline = "middle";

    

    questionTxtR.visible = false;
    attemptTxtR.visible = false;
    correctTxtR.visible = false;
    responseTxtR.visible = false;
    scoreTxtR.visible = false;
*/


    bitmap.visible = true;
    var container2 = new createjs.Container();
    stage.addChild(container2)
    container2.parent.addChild(bitmap)


    
    if(gscore==0){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(0)
    }
    if(gscore<=10){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(2)
    }
    if(gscore>10 && gscore<=20){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(4)
    }
    if(gscore>20 && gscore<=30){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(6)
    }

     if(gscore>30 && gscore<=40){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(8)
    }
    if(gscore>40 && gscore<=50){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(9)
    }
    if(gscore>50 && gscore<=60){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(9)
    }
    if(gscore>60 && gscore<=70){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(9)
    }
    if(gscore>70 && gscore<=80){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(9)
    }
    if(gscore>80 && gscore<=90){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(9)
    }
    if(gscore>90 && gscore<=100){
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(9)
    }
    if(gscore==100){    
        resultLoading.resultScreenMc.totalScoreMc.gotoAndStop(10)
    }


    console.log("gscore= "+gscore)
    resultLoading.resultScreenMc.sTxt.text = gscore;
    resultLoading.resultScreenMc.sTxt.font = "bold 80px Questrial-Regular";
  
    //
    resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(rtime*2)
    resultLoading.resultScreenMc.rTxt.text = rtime;
    resultLoading.resultScreenMc.rTxt.font = "bold 50px Questrial-Regular";
    //

    if(rtime == 0){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(0)
    }
  
    if(rtime>0 && rtime<=20 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(1)
    }
     
    if(rtime>20 && rtime<=40 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(2)
    }

    if(rtime>40 && rtime<=60 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(3)
    }

    if(rtime>60 && rtime<=80 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(4)
    }

    if(rtime>80 && rtime<=100 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(5)
    }

    if(rtime>100 && rtime<=120 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(6)
    }

    if(rtime>120 && rtime<=140 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(7)
    }
    if(rtime>140 && rtime<=160 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(8)
    }
    if(rtime>160 && rtime<=180 ){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(9)
    }
    if(rtime==180){
        resultLoading.resultScreenMc.responseTimeMc.gotoAndStop(10)
    }
    
    //
    resultLoading.resultScreenMc.totalQuestionsMc.gotoAndStop(tqcnt)
    resultLoading.resultScreenMc.qTxt.text = tqcnt;
    resultLoading.resultScreenMc.qTxt.font = "bold 50px Questrial-Regular";
    //
    resultLoading.resultScreenMc.correctScoreMc.gotoAndStop(cqcnt)
    resultLoading.resultScreenMc.cTxt.text = cqcnt;
    resultLoading.resultScreenMc.cTxt.font = "bold 50px Questrial-Regular";
    //
    resultLoading.resultScreenMc.attemptScoreMc.gotoAndStop(aqcnt)
    resultLoading.resultScreenMc.aTxt.text = aqcnt;
    resultLoading.resultScreenMc.aTxt.font = "bold 50px Questrial-Regular";




    //////////////////////////////////////////////////////////////////////////////////
}
//----------------------------------------------------------------------------------------------------------------//
function calculatescore() {
    switch (rst) {
        case 0:
        case 1:
        case 2:
        case 3:
            score = score + 10;
            break;

        case 4:
            score = score + 9;
            break;
        case 5:
            score = score + 8;
            break;
        case 6:
            score = score + 7;
            break;
        case 7:
            score = score + 6;
            break;
        case 8:
            score = score + 5;
            break;
        case 9:
            score = score + 4;
            break;
        case 10:
            score = score + 3;
            break;
        default:
            score = score + 2;
            break;

    }
    rst1 = rst1 + rst;
}


function randomSort(a, b) {
    if (Math.random() < 0.5) return -1;
    else return 1;
}