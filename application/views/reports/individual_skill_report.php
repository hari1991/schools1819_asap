     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="contentInner">
 <div class="container">


<div class="bspiCalender">
 <h2>Individual Skill Tracking</h2> 

 
<div class="bspiCalenderTop col-md-10 col-md-offset-1">
<span class="leftText"></span>

<div class="month">
 <div class="box">
 <form class="cmxform" method="POST" id="commentForm" accept-charset="utf-8"> 
      <select id="months" name="months" >
			 <?php foreach($skills as $sk){ ?>
					
				<option value="<?php echo $sk['id']; ?>"><?php echo $sk['name']; ?></option>
				
			<?php } ?>
			
	 </select>	
</form>	 
    </div>
</div>
</div> 
<div class="clearfix"></div>

  
  
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>




 
	 
	 <script type="text/javascript">

$(document).ready(function(){
	ajaxGraph($("#commentForm")); 
$('#months').change(function(){
var form=$("#commentForm");	
ajaxGraph(form);

	});
		});
		
		
		
		
		function ajaxGraph(form)
{
	 
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/reports/individualskill_report_ajax') ?>",
data:form.serialize(),
dataType: 'json',
success:function(result)
{
//alert(result);
 Actualchart(result);
}
});


}


 

function Actualchart(result)
{
	 
    var chart = new Highcharts.Chart({
        chart: {
			renderTo: 'BrainProfileChart',
			backgroundColor:'transparent',
			options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 30 
            },
            type: 'column'
        },
        title: {
            text: ''
        },
		tooltip: {enabled: false},exporting:false,credits: {
      enabled: false
  },
        yAxis: {
			gridLineWidth: 0,
  minorGridLineWidth: 0,
          title: {
                text: 'Score'
            },
			max: 100 
        },xAxis: {
            categories: result.data,
			gridLineWidth: 0,
  minorGridLineWidth: 0
        },
        credits: {
            enabled: false
        },
		plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
            showInLegend: false, 
            data: result.values
        }]
    });
	
}
</script>
 

 <div class="myBrainProfile">
 <h2 id="MonthID"></h2>
 <br/>
 <div id="BrainProfileChart">
</div>
 
 
 
 </div> 

 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 </style>