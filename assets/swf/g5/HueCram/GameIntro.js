(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("ATIkXQgUILlqFoQliFgm0KUQoaqUlilgQlqlogUoLQgBgdAAgcQgBn4FQl0QCZirC4hhIBfgvIDchDQBygVB6AAQB8AAByAVIDbBDIBgAvQC3BhCaCrQFPF0AAH4QAAAcgBAdg");
	this.shape.setTransform(0,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ArMLNQkpkpAAmkQAAmjEpkpQEpkpGjAAQGkAAEpEpQEpEpAAGjQAAGkkpEpQkpEpmkAAQmjAAkpkpg");
	this.shape_1.setTransform(0,-46.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC3333").s().p("AtIIZQlqlpgUoKIgCg6QAAn4FQlzQCZirC3hiQiLBOh6B6QlSFSAAHdIABA0QARG8FAE9QFRFSIPKNQGqqNFSlSQE/k9ARm8IABg0QAAndlRlSQh6h6iMhOQC3BiCZCrQFQFzAAH4IgCA6QgTIKlqFpQliFfm1KUQoaqUlhlfg");
	this.shape_2.setTransform(0,6.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AsuHoQk/k/gRm7IgBg0QAAndFRlRQB7h6CLhOIBggvIDbhDQBzgYB6AAQB8AAByAYQhygVh8AAQh6AAhzAVQBzgVB6AAQB8AAByAVIDbBDIBgAvQCMBOB5B6QFSFRAAHdIgBA0QgRG7lAE/QlRFSmqKNQoPqNlSlSgArMwSQkpEpAAGjQAAGjEpEpQEpEpGjAAQGkAAEokpQEpkpAAmjQAAmjkpkpQkokpmkAAQmjAAkpEpg");
	this.shape_3.setTransform(0,-13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-123.9,-162.9,247.9,326.2);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("AgRDXQgIgDgGgGQgGgGgEgIQgDgJgBgIQABgKADgIQAEgJAGgGQAGgGAIgEQAIgDAJAAQAKAAAIADQAIAEAHAGQAFAGAEAJQAEAIgBAKQABAIgEAJQgEAIgFAGQgHAGgIADQgIAEgKAAQgJAAgIgEgAgXA+QgEgegDgaQgDgZAAgcIAAirIBFAAIAACrQAAAcgDAZQgDAagEAeg");
	this.shape.setTransform(69,3.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("ABOCaQgKAAgEgNIg3iuIgFgSIgEgRIgDASIgFARIg4CuQgEANgMAAIg5AAIhhkzIA7AAQAIABAGADQAFAEACAGIAsClIAGAaIAEAaIAHgaIAIgaIAzimQACgFAGgFQAFgDAHAAIAgAAQAIAAAFADQAGAFABAFIAzCoIAHAZIAGAZIAFgaIAGgaIAuilQABgGAGgEQAGgDAHgBIA4AAIhhEzg");
	this.shape_1.setTransform(34.5,9.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Ag9CUQgcgMgVgUQgTgVgLgdQgMgeABgkQgBgkAMgdQALgdATgVQAVgUAcgMQAcgLAiAAQAiAAAcALQAcAMAUAUQATAVALAdQALAdAAAkQAAAkgLAeQgLAdgTAVQgUAUgcAMQgcALgiAAQgiAAgcgLgAg4hKQgTAaAAAwQAAAyATAZQATAbAmAAQAmAAATgaQASgaAAgyQAAgxgSgaQgTgagmAAQgmAAgTAbg");
	this.shape_2.setTransform(-6.3,9.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CCFF").s().p("ABnDZIhfkjIgEgMIgEgOIgCAOIgEAMIhgEjIhIAAIiHmxIBDAAQALAAAHAFQAHAFACAIIBKEEIAFAVIAEAYIAFgYIAHgVIBVkEQADgGAGgGQAHgGAKAAIAYAAQAKAAAHAFQAHAFADAIIBUEEQAHAUAEAXIAFgWIAEgVIBLkEQABgHAIgGQAHgFAKAAIBAAAIiHGxg");
	this.shape_3.setTransform(-54.7,3.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88.2,-36.3,170.2,76);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF9900").ss(3,1,1).p("AiMhRIBKluIAyAAIA1ABIBWFtAiMhRICoACIBfABIA8ABIB5ABIk0IMIgzhaIj4m4g");
	this.shape.setTransform(-280.7,-122.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("AjzgmICkADICoABIBeABIA9AAIjvGzgABZgigAhPgjIBKlvIAxAAIAtFwg");
	this.shape_1.setTransform(-286.7,-127.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF99").s().p("AizFlIDumzIB5ACIk0ILgAhfhPIgtlwIA1ABIBXFuIgBACg");
	this.shape_2.setTransform(-268.2,-122.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-312.6,-168.9,63.9,92.5);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.redcar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,3.4).ss(1,1,1,3,true).p("AgOghIAAgBQAwAvgTAVAgOghQgSARAuAzQABAAAAgB");
	this.shape.setTransform(117.6,49.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#403D50").s().p("AgLgiIAcBEIAAABQgvgzATgSg");
	this.shape_1.setTransform(117.4,49.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#637778").s().p("AgRghIAAAAQAwAugTAVg");
	this.shape_2.setTransform(117.9,49.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,3.4).ss(1,1,1,3,true).p("AAkANIAAgBQgygsgUAVAAkANQgRATg2gqQABAAAAgB");
	this.shape_3.setTransform(99.7,32.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#637778").s().p("AgigIQATgVAzAsIAAABg");
	this.shape_4.setTransform(99.8,32.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#403D50").s().p("AgjgOIABAAIBGAXQgFAGgJAAQgUAAglgdg");
	this.shape_5.setTransform(99.7,33.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().rs(["#700000","#4B0101"],[0,0.988],9.8,-0.8,0,9.8,-0.8,11.5).ss(1,1,1,3,true).p("AhFBIICLiP");
	this.shape_6.setTransform(84.7,24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12.7,8.9,0,-12.7,8.9,5.2).ss(1,1,1,3,true).p("AAbglQAIgKAIAGIAAAGAAbgUQAEgFgEgMAgaAeQAGAAAIgIIAngqAgqAsQgCgEAEgIIAOgC");
	this.shape_7.setTransform(91,110.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-16.7,-17.6,0,-16.7,-17.6,29.3).ss(1,1,1,3,true).p("AAZgEQgZABgZgHQAWAUAdAA");
	this.shape_8.setTransform(38.2,153.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-5.8,-4.2,0,-5.8,-4.2,12.6).ss(1,1,1,3,true).p("AluFSQAbgDAdgZQDAihCqijQBPhLBKhNQBUhVBOhX");
	this.shape_9.setTransform(77.5,117.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().rs(["#700000","#4B0101"],[0,0.988],10.5,-13.3,0,10.5,-13.3,5).ss(1,1,1,3,true).p("ADFi2QgOAPgOAPQj4D7h0BTQAAAAgBAB");
	this.shape_10.setTransform(67,133);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().rs(["#700000","#4B0101"],[0,0.988],17.9,-18.8,0,17.9,-18.8,6.6).ss(1,1,1,3,true).p("AAegMIAAAAQgVAZgdADQgFABgGAAAAegMIAAAAAAggQQgBACgBAC");
	this.shape_11.setTransform(44,153);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().rs(["#700000","#4B0101"],[0,0.988],31.6,-33.2,0,31.6,-33.2,6.2).ss(1,1,1,3,true).p("AAegGQABgBACgBAAdgGIABAAAggAJQAggBAdgO");
	this.shape_12.setTransform(44,152.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().rs(["#700000","#4B0101"],[0,0.988],3.7,-3.7,0,3.7,-3.7,28.2).ss(1,1,1,3,true).p("AAPATQAbAYAeAYAgSgMQgbgbgagb");
	this.shape_13.setTransform(24.7,131.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-5.9,-17.9,0,-5.9,-17.9,28.2).ss(1,1,1,3,true).p("AgJgGIAHAFQADACAFADQAAAAABABQABABABAB");
	this.shape_14.setTransform(37.5,142.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4,-4,0,4,-4,28.2).ss(1,1,1,3,true).p("AARAQQgRgQgQgP");
	this.shape_15.setTransform(24.5,131.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().rs(["#700000","#4B0101"],[0,0.988],39.2,-13.9,0,39.2,-13.9,30.7).ss(1,1,1,3,true).p("AAiCIQgZgcgWgcQAAgBgBgBQgCgCgBgCQgOgVgFgdQAAAAAAgBQgFgsBPhy");
	this.shape_16.setTransform(14.1,111.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().rs(["#700000","#4B0101"],[0,0.988],18.4,15.7,0,18.4,15.7,28.9).ss(1,1,1,3,true).p("AAMAeQgHgWgBgUQgBgCABAAAgLgdQgCAiAZAZ");
	this.shape_17.setTransform(1.7,119.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().rs(["#700000","#4B0101"],[0,0.988],17.6,-18.6,0,17.6,-18.6,6.6).ss(1,1,1,3,true).p("AgOAaIABABQAAABAAABAAPgcQgbAYgCAe");
	this.shape_18.setTransform(1.9,113.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().rs(["#700000","#4B0101"],[0,0.988],31.2,-34.2,0,31.2,-34.2,6.2).ss(1,1,1,3,true).p("AAHgjQgBACgBACQgMAiACAh");
	this.shape_19.setTransform(2.7,114);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().rs(["#700000","#4B0101"],[0,0.988],11.8,-40,0,11.8,-40,30.7).ss(1,1,1,3,true).p("ACIgqQhrBWgsgCQgBAAgBAAQgcgCgXgNQAAgBgBgBAiFgUQgBgBgBgBAhYAMIgtgg");
	this.shape_20.setTransform(45.4,140.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.2,5.9,0,4.2,5.9,12.5).ss(1,1,1,3,true).p("AkgFPQCSjMCXi0QBGhUBHhQQBRhbBShVAkhFRIABgCAk4GGQABgaAWgb");
	this.shape_21.setTransform(34.8,77.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().rs(["#700000","#4B0101"],[0,0.988],28.1,2,0,28.1,2,29.8).ss(1,1,1,3,true).p("AADABQAAABACAAAADABQgDgBgDgBQAAAAgBAA");
	this.shape_22.setTransform(64.3,56.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-6.9,7.1,0,-6.9,7.1,29.8).ss(1,1,1,3,true).p("AidiUIABABQDKBiBwDF");
	this.shape_23.setTransform(80.5,71.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-2.5,2.6,0,-2.5,2.6,25.2).ss(1,1,1,3,true).p("ABQDYQA8gDBMhNQABgCACgCQABgBABgBQAIgMgMgMQgBgBgBgBQi3iOiOilQgBAAgBgBQgYgXgTAZQgBABgCABQg9BVgEAzIABACQACAfAOAUIABACIAAABQBcB1CDBcIACACQAZAOAfgB");
	this.shape_24.setTransform(36.3,118.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().rs(["#700000","#4B0101"],[0,0.988],35.6,2.3,0,35.6,2.3,30.4).ss(1,1,1,3,true).p("AAEARQgBgBgBgBQAAAAgBgBQgJgLAKgT");
	this.shape_25.setTransform(25.2,89.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().rs(["#700000","#4B0101"],[0,0.988],14.2,26,0,14.2,26,29.9).ss(1,1,1,3,true).p("AjDD5QgMAEgGgDACIh5IgDAEQjCEdiGBRADWj7IhOCC");
	this.shape_26.setTransform(47,66);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().rs(["#700000","#4B0101"],[0,0.988],12.9,-11.9,0,12.9,-11.9,4.9).ss(1,1,1,3,true).p("ACpjQQgPAPgMAPQjrEIhKB6QAAABgBAA");
	this.shape_27.setTransform(20.3,89.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-8.1,13.3,0,-8.1,13.3,5.1).ss(1,1,1,3,true).p("AgUAPQgIAIAAAGIgBAOQgHAFgEgCAAggtIAHAAQAGAIgJAIQgMgDgFAEIgnAr");
	this.shape_28.setTransform(41.2,63.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().rs(["#700000","#4B0101"],[0,0.988],9,-9.9,0,9,-9.9,40.4).ss(1,1,1,3,true).p("AkjjQQAAgCACgEQABgEAIgHIAngqQAGgHAHgCQAEgBADgBIAHAAAkkjDIABgNAElDJQAAAEgBAEQgCAHgFAHIgnAqQgIAIgDACQgEABgCABIgNACAElDDIAAAG");
	this.shape_29.setTransform(66,87.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.8,34.9,0,4.8,34.9,32).ss(1,1,1,3,true).p("AgkAmIBJhL");
	this.shape_30.setTransform(72.1,37.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.5,-0.1,0,0.5,-0.1,4.2).ss(1,1,1,3,true).p("AgMgYQATgCAeAHQANAHgDAYQgBABAAABQAAABAAAAAgNgYIABAAAAdAZIAAAAQAMgEAFgGAAhATQAAACgCACQgBABgBABAAiAQQAAACgBABAguAAQAIgIBIAYAguAAQAGgYAbAA");
	this.shape_31.setTransform(63.5,36.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().rs(["#700000","#4B0101"],[0,0.988],25.2,12.5,0,25.2,12.5,29.8).ss(1,1,1,3,true).p("AgVgHQgCAAgBAAQgBAAgBAAAgVgHIAAAAAAbAIQgYgIgYgH");
	this.shape_32.setTransform(80.3,32);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().rs(["#700000","#4B0101"],[0,0.988],27.8,10.1,0,27.8,10.1,31.9).ss(1,1,1,3,true).p("AgCABQAAgBABgBQABgFAFgCQAAAAAAgBQACAAACAAAgIALIAGgK");
	this.shape_33.setTransform(76.8,32.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-9.3,3.7,0,-9.3,3.7,9.2).ss(1,1,1,3,true).p("AAnhpQgGBOhHCE");
	this.shape_34.setTransform(139.1,44.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.8,-5,0,4.8,-5,29.8).ss(1,1,1,3,true).p("AixilQD5BZBrDy");
	this.shape_35.setTransform(100.9,49.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.1,-9.9,0,0.1,-9.9,11.5).ss(1,1,1,3,true).p("ABJhRIiRCj");
	this.shape_36.setTransform(128,63.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-26.9,-12.1,0,-26.9,-12.1,30).ss(1,1,1,3,true).p("ADrjpIhvBLQkcDphJCKQAAABAAABQgBAMACAFQAAABABAAQAAABABAA");
	this.shape_37.setTransform(88.5,104.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-35.8,-1.9,0,-35.8,-1.9,31.9).ss(1,1,1,3,true).p("AAfgiIg9BF");
	this.shape_38.setTransform(115.9,76.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.4,-1.2,0,-0.4,-1.2,3.8).ss(1,1,1,3,true).p("AjsDpQAGAEAQgMQACgCABgBQDEi1D7kRQABAAAAgB");
	this.shape_39.setTransform(89.1,104.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.2,-0.3,0,0.2,-0.3,4).ss(1,1,1,3,true).p("AgQgoQABgBABAAQAPgJANALQADADACADIAGAkQAGAjgbAIAgQgoIAAAAAgZgdQADgGAGgFAgZgdQAFgFAQAkQAQAkgIAI");
	this.shape_40.setTransform(116.9,85.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-26.3,27.8,0,-26.3,27.8,29.8).ss(1,1,1,3,true).p("AA1EHQABAAgBABQABABABABQAHAHAIgDQABgBACgBIDOiLAkQgnQgJgFAFgNIB8jV");
	this.shape_41.setTransform(91.1,60.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-14.2,-24.1,0,-14.2,-24.1,29.8).ss(1,1,1,3,true).p("AgJgbQAKAYAJAZQAAACgBAE");
	this.shape_42.setTransform(119.7,68.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12,-27.1,0,-12,-27.1,31.9).ss(1,1,1,3,true).p("AAIgHQAAACAAABQgCACgCACQgEADgBABQAAABgBAAIgFAD");
	this.shape_43.setTransform(119.8,72.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-2.1,8.8,0,-2.1,8.8,9.2).ss(1,1,1,3,true).p("AhoAzQAPgLAPgKQBqhGBJgK");
	this.shape_44.setTransform(102.3,11.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.8,5.1,0,0.8,5.1,11.6).ss(1,1,1,3,true).p("Aj5CnQDAi9CVh4QACgBABgBQAdgWAXAAQABAAABABQAigCBDAi");
	this.shape_45.setTransform(93.4,17.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-10.9,13.3,0,-10.9,13.3,11.3).ss(1,1,1,3,true).p("AgYgpQAAgBABgBQAug5BiA+QAHAEAIAFAiHBHIBvhw");
	this.shape_46.setTransform(105.3,9.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().rs(["#700000","#4B0101"],[0,0.988],3.6,16.6,0,3.6,16.6,24.7).ss(1,1,1,3,true).p("AgdAAQAagDAVADQAGAAAGAC");
	this.shape_47.setTransform(115.8,6.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12.3,12.2,0,-12.3,12.2,10.6).ss(1,1,1,3,true).p("AApAWQABgBAAAAQAqg6hHhfAg5CGIBihw");
	this.shape_48.setTransform(141,41.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-3.3,-0.7,0,-3.3,-0.7,11.1).ss(1,1,1,3,true).p("AB+kDQAnBBABAiQAAABAAAAQACAXgUAgQhtCdi2DOIgWgB");
	this.shape_49.setTransform(132.4,55.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-16.4,-2.8,0,-16.4,-2.8,24.8).ss(1,1,1,3,true).p("AgCgbQAHAXgCAg");
	this.shape_50.setTransform(142.7,31.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.1,-0.3,0,-0.1,-0.3,20.4).ss(1,1,1,3,true).p("AhZhgIADADQAPAIAOAJQAeATAaAXQARAOAQARQAYAWAVAdQAPAUAOAXIACACQAFAHADAIAhzhrQANADANAI");
	this.shape_51.setTransform(130.8,17.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.rf(["#DA0E0D","#850700"],[0,0.988],2,1.4,0,2,1.4,6.4).s().p("AgqAqQgCgEAEgIIAOgCQAGAAAIgIIAngqQAEgFgEgMQAIgKAIAGIAAAGIAAAGIgBAIQgCAHgFAHIgmApQgIAIgDACIgGACIgNACIgFABIgEgFg");
	this.shape_52.setTransform(91,110.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.rf(["#FF9900","#CC2D17"],[0,0.988],0.1,0.8,0,0.1,0.8,4.4).s().p("Ag5gDQAaAFAZgBQAegBAegOIABAAIADgCIgCADIgBAAIAAABQgUAZgeADIgLABQgdAAgWgUg");
	this.shape_53.setTransform(41.5,153);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.lf(["#710A0B","#C80000"],[0,0.988],-4.6,4.6,0,-0.2).s().p("AA7CDIgCgCQhmhPhVhfIgBAAQhKhVgBhAIAGAAQgCAjAZAYQgHgWgBgVIAAgCIAJgGQgCAiAiA3QAlA9BCA+QBCBBBBAkQBIAoAtgKIgGAEQgDADADALQgZABgagHQAWAVAdAAIAAAFQg9gChRg+g");
	this.shape_54.setTransform(20.7,135.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.rf(["#FF9900","#CC2D17"],[0,0.988],-0.6,-0.3,0,-0.6,-0.3,4.9).s().p("AgNAAIAAgBIgBgBQACgfAbgYIgCAEQgNAiACAhIAAABQABAWAHAVQgZgYACgig");
	this.shape_55.setTransform(1.9,116.2);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.rf(["#700000","#4B0101"],[0,0.988],-1.6,5.4,0,-1.6,5.4,35.7).s().p("AjpDKIAAgFIALgBQAfgEAUgZIAAgBIABAAIACgDIABgBQB0hSD4j8IAcgdIAEAEIAFgBIiGCLQhdCQhtApIhAA2QgcAWgkAAIgDAAg");
	this.shape_56.setTransform(64.2,135);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.rf(["#700000","#4B0101"],[0,0.988],-5,0.2,0,-5,0.2,33.6).s().p("Ai4DyQgBgfASgbIAxhDQAihwCIhnICBiQIAAAGQACACADABIgbAeQjrEJhJB6IgBABQgcAYgCAfIABABIAAABg");
	this.shape_57.setTransform(18.5,91.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.rf(["#DA0E0D","#850700"],[0,0.988],-1.9,-2.3,0,-1.9,-2.3,6.7).s().p("AgmAuQgDgBgCgDIABgFIABgNIACgGQABgEAIgHIAmgpQAGgHAHgCIAHgCIAHAAIAGAAQAHAIgKAIQgMgDgFAEIgnArQgHAIAAAGIgBAOQgFADgEAAIgDAAg");
	this.shape_58.setTransform(41,63.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.lf(["#6C0202","#C80000"],[0,0.988],-2.1,-3.2,0,-1.5).s().p("Ak0FJIACgEIABgBQBKh6DqkJIAbgeQAFADAHgFIABgPQAAgGAHgHIAogsQAFgEAMADQAKgJgHgHIgGAAIDFjUIATAGQhSBUhSBbQhGBQhGBUQiXC1iSDLIgBACQgWAbgBAaIgCAAIgBgIIgCALIgJAGQgCghANgjg");
	this.shape_59.setTransform(34.1,77.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.rf(["#DD1600","#640101"],[0,0.988],-3.6,-0.9,0,-3.6,-0.9,8.7).s().p("AAdAZIACgCIACgEIABgDQhIgYgIAIQAGgYAbAAIABAAQATgCAeAHQANAHgDAYIgBACIAAABQgFAGgMAEg");
	this.shape_60.setTransform(63.5,36.1);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.lf(["#6C0202","#C80000"],[0,0.988],3,2.4,-0.2,-1).s().p("AlvFMQAbgEAcgZQDBihCpiiQBPhMBLhMQBUhWBNhWIAFATIjBDQIAAgHQgJgGgHAKQADAMgDAFIgoAsQgIAHgHABIgOACQgEAHADAFIgcAdQj4D8h0BSIgCABIgDACIAAAAQgeAPggABQgDgLAEgCg");
	this.shape_61.setTransform(77.6,118.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#6B0000").s().p("AgpG7QAHAEAQgMIACgCQDEi2D8kQIABgBIAPgBQhfBnhSBVIg6A8QhwByhlBeIgEAEQgOALgLAAQgGAAgGgFgAm4BJIgBgBQgLgLAMgUIAAgBIAAAAIADgDIAAgBIAEgFQBVhpBth6IA3g/QBQhYBehkIACAIIgBAIQkAEQioDQIgBABIgCACQgKAMAFAHIACAEIgBgCg");
	this.shape_62.setTransform(69.5,83.5);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#DD1600","#640101"],[0,0.988],1.4,2.8,0,1.4,2.8,7.6).s().p("AgEACQgQgkgFAFQADgGAGgFIAAAAIACgBQAPgJANALIAFAGIAGAkQAGAjgbAIQAIgIgQgkg");
	this.shape_63.setTransform(116.9,85.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.rf(["#C60203","#870101"],[0,0.988],-37.9,40.8,0,-37.9,40.8,122.9).s().p("Ai1I2QgrAAhCglQgXgNgYgQQgcgVgdgZIgbgaIgHgGQgdgcgWgbQgbgggTgfQgbgugIgeQgBgEABgEQACgaAWgbIAAgCQCTjKCXi2QBGhUBHhQQBRhbBShUIABgBIABAAIABAAQALgEAGgGIABgBIADAMQheBkhQBYIg3A/QhtB6hVBqIgEAFIAAABIgBAAIgBADIAAAAIgBABIg2BFIgHAGIgOAWQhLBsAAAuIAAAGIAAAAQAFAdAOAWIAEADIABACQAXAdAYAbIA1A1IAiAgIgiggIg1g1QgYgbgXgdIgBgCIgEgDQgOgWgFgdIAAAAIAAgGQAAguBLhsQhFByADArIAAABQAGAbANAVIAEAFIABABQAWAbAZAcQAYAaAbAaIAhAfQAZAXAiAaIABACIA9AsIABABIACABQAVALAaADIABABQApABBthOIANgKIAEgFQAigcAogmIACgBIAGgFIAFgEQBmhfBvhxIA7g9QBShTBehpIANAAIgCAGIgBABIgBAAQgGAFgDAFQgCAFgBAFIABADQhNBXhUBUQhLBMhPBMQiqCkjACgQgcAagbADIAFgFgAiXHyQApgCBWhDIAAAAIAEgDIABgBIALgIIABgBIAAAAIABgBIACgCIgCACIgBABIAAAAIgBABIgLAIIgBABIgEADIAAAAQhWBDgpACIgDABIAAAAIgCAAIgCAAQgcgDgXgNIgBgBIABABQAXANAcADIACAAIACAAIAAAAIADgBgAjWHfIABABIABAAIACACIgDgCIgBgBIgIgFIAIAFgAkSG0IAtAgIAHAGIgHgGIgtggIgBgDIABADgAkTGxQgegXgbgZQAbAZAeAXgAmfAcIAAAAg");
	this.shape_64.setTransform(59.5,94.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#6B0906").s().p("ADIDjQASgEALgJQALgJAEgLQAOgBgUAWIgMANIgBACIgNAAIgPABIADgEgAj2jAIgCgMIACgDIATgUQAQgIgHAQIAAACQgEgBgEABIgEADQgDADgGANIgFANg");
	this.shape_65.setTransform(93.1,58.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#AF0303").s().p("ADHDZIAogbIAGgBQgEAKgLAKQgLAJgSAEIgLACgAjzi+IAFgNQAGgOADgDIAFgCQAEgCADABIgWAjIgFAGg");
	this.shape_66.setTransform(93,58.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FF591D").s().p("AihiSQASgDAxAeQAuAdA1AvQA5AwApAxQAuA2ANAnQiCi2jBhvg");
	this.shape_67.setTransform(79.3,72.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.rf(["#C60203","#870101"],[0,1],31.5,-32.3,0,31.5,-32.3,92.5).s().p("Ak4IvQhBgkhDhBQhCg/glg9Qgig3ACgiIACgLIABAIIACAAQgBAEABAEQAHAeAbAuQAUAfAaAgQAXAcAcAbIAHAGIAbAaQAdAaAcAUQAYAQAYANQBBAlAsABQgJACgKAAQgoAAg6gggAioIAIgBgBQgbgDgVgLIgCgBIgBgBIg9gsIgBgCQghgagagXIghgfQgbgZgYgbQgZgbgWgcIgBgBIgDgFQgOgUgFgcIAAgBQgEgrBFhxIAOgXIAHgGIA2hFQgLAUAKALIABABIACACQAGADAMgEQCGhRDDkeIADgEIBNiCIBLhLIh9DVQgEANAIAFIAAABQgPgBgNASIAAACIAAABQiYDPiTCRIgBAAIgCADQgNAPAIARIACAFQBsCcCmBlIAFABQARAEANgOIAAAAQCeixCoiXQAYgSAAgLIABgBIAAABIACACQAHAHAHgDIADgCIDOiLIg9BGIgJAHIhvBLQkdDphJCKIAAACQgBAMACAFIABABIABABQAOAMAWgSIgGAFIgBACQgoAlgiAdIgEAFIgOAKQhsBMgoAAIgBAAgAmSA8IgCADQg+BUgEA0IABABQACAgAPATIAAADIABAAQBcB2CDBcIADADQAYANAggBIADAAQA8gDBMhNIAEgEIABgCQAJgMgMgMIgCgCQi3iOiQilIgBgBQgMgLgLAAQgLAAgKAMgAlhgzIABAAIgCADg");
	this.shape_68.setTransform(61,92.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#E71515").s().p("AgwFPIgEgBQinhlhsicIgCgFQgHgRANgPIACgDIAAAAQCTiRCYjPIACgCIAAgBQAMgSAOABIABABIAHABIACABIABABQDLBiBwDGIABADQAAALgYASQioCXieCxIABAAQgMALgNAAIgHgBgAAAlAQDBBvCCC3QgNgngug3Qgpgwg5gyQg1gugvgdQgsgcgTAAIgDABg");
	this.shape_69.setTransform(63.1,90.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#760101").s().p("AiZiNQA6ARAxAcQAhATAdAaQAWARATAWQAaAbAWAiQAcAqAVAzQhljGjOhVg");
	this.shape_70.setTransform(102.5,47.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.lf(["#AD0002","#FB1E1E"],[0,1],-11.9,-5.2,8.9,0.5).s().p("AjbB0IAAgBIgDABIgCAAICNiQIAegVQBqhHBKgKQA5AFApAbQhXgSk1D3QgXgJgZgGg");
	this.shape_71.setTransform(100.2,19.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.lf(["#5B0202","#720000"],[0,0.988],-6.3,-3.7,5.9,7.7).s().p("Aj5CnIB0h3QBLhrBZgsIAEgCIABgCQBZhRAtAWIACAAQAnANAnAaQhEgjghACIgCAAQgXAAgeAVIgCADQiVB4jAC8g");
	this.shape_72.setTransform(93.4,17.2);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-0.4,-2.1,0,-0.4,-2.1,10).s().p("AhTAiIALgMIAigiIACgCQAlgvBKAiIAAABIACABQAPAGgEABQgEABgFAAIgHgBIgOACQg4AHhJAqIgOAHIgDACQgCAAAHgIg");
	this.shape_73.setTransform(106.8,7.5);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],1,-4.3,0,1,-4.3,10).s().p("AgagpIACgCQAtg5BjA+IAOAJIACACIABABIgEgBIgLgDQgWgEgaAEQhKAKhqBGIgeAVgAgWgkIgCACIgiAiIgLAMQgLALAJgFIAOgHQBIgqA4gHIAPgCIAHABQAFAAAEgBQAEgBgPgGIgCgBIgBgBQgegOgZAAQghAAgWAbg");
	this.shape_74.setTransform(105.5,9.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.lf(["#6D0101","#D60C02","#771B1E"],[0,0.447,0.988],-3.8,-0.2,-7.8,-4.5).s().p("AkMC8IAVgjIABgCQAHgQgRAIIgSAUIgBgdQDAi8CVh4IACgCQAegWAXAAIACAAQAhgBBEAiIAGAEQAXAPAXATQgXgJgWgEIgCgCIgOgJQhjg+guA6IgCABIhtByIiNCPIgDABIgBABQgFABgCAGIgBACIgGAKIhKBLg");
	this.shape_75.setTransform(96,21);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.lf(["#AD0002","#FB0D0D"],[0,1],7.9,10.8,-3.5,-7.6).s().p("AhmDoIACgGQgJgagLgYQDclAgUhYQAhAsAJAvQgHBOhHCEIiRCkg");
	this.shape_76.setTransform(130.9,48.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.lf(["#5B0202","#720000"],[0,0.988],4.8,6.1,2.6,4.2).s().p("AitEFIACgGIACgBIAWACQC2jPBtidQAUgggCgXIAAgBQgBgignhAQAdAlARAlIAAABQAYAthLBeIgCACIgBAEQgnBbhlBTIh2CDQgMgLgRAJg");
	this.shape_77.setTransform(132.8,55.5);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],3.2,-2.9,0,3.2,-2.9,9.7).s().p("AAUhNQACgggJgYQBHBggqA5IgBACIhiBwQBHiGAGhNgAAYhiIAAAJIABAIIgBANQgDA5glBMIgGAOQgEAIAKgKIAMgNIAfgkIABgCQAsgogohKIgBgBQgFgKgCAAIAAABg");
	this.shape_78.setTransform(141,41.9);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],41.8,-22.1,0,41.8,-22.1,9.7).s().p("AB6D4IgFgTIgBgDQAAgFADgFQAFgEAQAlQARAkgIAHQgPgLgMghgAFrhxIAGgOQAmhNADg4IABgOIgBgHIAAgKQABgEAGAOIABABQAoBJgsAqIgBABIggAkIgMANQgGAGgBAAQAAAAAAAAQAAgBAAAAQAAgBAAAAQABgBAAgBgAl+jlIgOgEQgjgJgFgIQAJgJBJAaIgBACIgIAHIgTgFg");
	this.shape_79.setTransform(102.4,61.2);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.lf(["#6D0101","#D60C02","#771B1E"],[0,0.447,0.988],-5,8.1,1.2,13.2).s().p("AiXEXIAMgNQAUgWgOABIgGABIgpAcIA+hGIAFgEIAAAAIAGgFIAEgFIAAgCIABgBIAAAAICRikIBjhvIABgBQAqg7hHhfQgCgVgIgQIAgAlIAEAGQAoBBAAAiIAAABQACAXgTAfQhuCei1DOg");
	this.shape_80.setTransform(130.9,53);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FF3333").s().p("AkXgxQE0j4BXASIADAAIABABQAHADAGAEQAfATAaAXQASAOAPAQQAZAZAUAdIAHALIADAEIABAEQATBYjdE/Qhqjzj6hXgABFDuQgVg0gcgqQgVgigagcQgTgWgWgRQgegaghgSQgxgcg6gRQDPBUBkDIg");
	this.shape_81.setTransform(111,37.8);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#CD0C00").s().p("ABuAwIgBgEIgDgEIgHgLQgUgcgZgYQgPgQgSgPQgZgWgfgTQgGgFgHgDIgBgBIgDAAQgogbg6gFQAagEAWAEIAMADIADABQAOADAMAIIADADQAPAIAOAJQAdATAaAXQASAOAQAQQAYAYAVAcQAPAUAOAXIACACQAFAHADAIQAJAXgCAhQgJgvgggsg");
	this.shape_82.setTransform(127.9,20.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#3C4A58").s().p("AmNKPIgCgDQiEhchch2IAAAAIgBgDQgOgTgCggIgBgBQAEg0A9hUIADgDQATgYAYAXIACABQCPClC3COIACACQAMAMgIAMIgCACIgDAEQhMBNg8ADIgEAAIgEABQgdAAgXgNgAh/IeIgBgBIgBgBQgCgFABgMIABgCQBJiKEbjqIBvhLIAMgCIgDAEIgBABQj8ERjEC2IgCACQgMAJgHAAIgEgBgAoNCuIgCgEQgFgHAKgMIACgCIABgBQCojQEAkQIAFgGIgGALIhPCCIgDAEQjCEdiGBRQgHADgFAAQgEAAgDgCgAC5CGIgCgCIAAgBIgBgCQhwjFjKhiIgBgBIgCgBIgHgDIAAAAQgJgFAFgNIB8jVIAGgJIABgDQACgFAFgCIABgBIADgBIACAAIACAAIAAAAQAZAHAYAJQD6BYBrDzQALAZAJAaIgBAGIAAAAIAAABIgBAAIAAADIgEAFIgGAEIAAABIgFADIjOCKIgDACIgFABQgFAAgFgFgADSBxIABAAIASgOgAJ6nRIgCgCQgOgXgPgUQgVgcgYgYQgQgRgSgOQgagXgegSQgOgJgPgIIgDgDQgMgIgOgEIAAgBQAWAEAWAJQAwAUA2AsQAzAyAZA1QAIAQACAVQgDgIgFgHg");
	this.shape_83.setTransform(78.1,73.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.redcar, new cjs.Rectangle(-0.5,0,150.7,155.7), null);


(lib.qtxt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3F3302").s().p("AgdA7IAAhzIARAAIAAAdQAFgSASgJQAHgDALAAIABAAIAAARIgCAAQgSAAgLAPQgLAOAAATIAAAzg");
	this.shape.setTransform(220.2,-1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3F3302").s().p("AgpAqQgPgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAPARQAQARAAAYQAAAZgQARQgPARgaAAQgZAAgQgRgAgdgeQgKAMAAASQAAATAKAMQAMAMARAAQASAAAMgMQAJgMABgTQgBgSgJgMQgMgMgSAAQgRAAgMAMg");
	this.shape_1.setTransform(208.3,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3F3302").s().p("AgHBRIAAihIAQAAIAAChg");
	this.shape_2.setTransform(198.4,-3.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3F3302").s().p("AgpAqQgPgRAAgZQAAgYAPgRQAQgRAZAAQAZAAARARQAPARAAAYQAAAZgPARQgRARgZAAQgZAAgQgRgAgcgeQgLAMAAASQAAATALAMQAKAMASAAQATAAAKgMQAKgMAAgTQAAgSgKgMQgKgMgTAAQgSAAgKAMg");
	this.shape_3.setTransform(188.6,-0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3F3302").s().p("AgnAqQgPgRAAgZQAAgYAPgRQAQgRAYAAQAUgBAPALQAOALAFAQIgSAAQgEgKgJgGQgIgFgLAAQgKgBgIAEQgIADgFAGQgLAMAAASQAAATALAMQAMAMATAAQALAAAIgFQAJgGAEgKIASAAQgFAQgOALQgPALgUgBQgYAAgQgRg");
	this.shape_4.setTransform(174.9,-0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3F3302").s().p("AgfBJQgOgJgFgPIATAAQAIASAYAAQAVAAAJgKQAIgJABgSIAAgQQgOAVgbAAQgZAAgPgRQgPgRAAgYQAAgYAPgRQAQgRAZAAQAaAAAOAUIAAgSIARAAIAABuQAAAZgOANQgPANgXAAQgXAAgNgJgAgcg1QgLAMAAASQAAATALALQALAMASAAQAJAAAHgDQAIgEAFgFQALgLAAgTQAAgSgLgMQgLgMgSAAQgSAAgLAMg");
	this.shape_5.setTransform(154.5,1.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3F3302").s().p("AAhA7IAAhCQAAgQgJgJQgJgJgOAAQgNAAgKAJQgLAKAAAPIAABCIgRAAIAAhzIARAAIAAASQAHgIAJgFQAJgHAKABQAMAAAIADQAIADAGAGQAPAPAAAWIAABDg");
	this.shape_6.setTransform(140.6,-1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3F3302").s().p("AgIBPIAAhzIAQAAIAABzgAgIg3IAAgXIARAAIAAAXg");
	this.shape_7.setTransform(130.7,-3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3F3302").s().p("AgjAwQgMgKAAgQIASAAQABAPARAGQAFABAGAAIAKAAIAKgDQALgEAAgLQAAgGgGgFQgFgDgJgCIgSgFQgJgCgJgDQgIgDgGgGQgFgGgBgKQABgQAKgJQAMgIAWAAQAVAAAMAKQAKAIABAPIgRAAQgCgTgaABQgaAAgBAQQAAAHAHAEQAFADAIACIASAFQAKACAIADQAJACAGAHQAFAGABALQAAAQgOAJQgMAKgWgBQgVAAgPgLg");
	this.shape_8.setTransform(121.8,-0.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3F3302").s().p("AgiAwQgMgKgCgQIASAAQACAPAQAGQAGABAGAAIAKAAIAKgDQALgEAAgLQAAgGgFgFQgHgDgIgCIgRgFQgLgCgIgDQgJgDgFgGQgGgGABgKQgBgQALgJQAMgIAWAAQAUAAANAKQAKAIACAPIgSAAQgCgTgaABQgaAAAAAQQAAAHAFAEQAGADAJACIARAFQAKACAIADQAJACAGAHQAFAGAAALQABAQgNAJQgNAKgWgBQgWAAgNgLg");
	this.shape_9.setTransform(109.7,-0.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3F3302").s().p("AgIBPIAAhzIAQAAIAABzgAgIg3IAAgXIARAAIAAAXg");
	this.shape_10.setTransform(100.8,-3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3F3302").s().p("ABEA7IAAhFQAAgNgIgKQgIgJgNABQgMgBgJAJQgJAJAAAPIAABEIgQAAIAAhFQAAgNgJgKQgIgJgMABQgOgBgIAJQgKAJAAAPIAABEIgRAAIAAhzIARAAIAAASQAMgUAYABQALAAAKAGQAKAGAEALQAIgPARgGQAGgDAIABQAJAAAHADQAIAEAHAFQANAOgBAVIAABFg");
	this.shape_11.setTransform(87.5,-1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3F3302").s().p("AgpAqQgPgRAAgZQAAgYAPgRQAQgRAZAAQAZAAARARQAPARAAAYIAAAHIhfAAQACAQAKAKQALAKAQAAQAYABAKgUIATAAQgHAQgMAJQgOALgUgBQgZAAgRgRgAgaghQgKAKgCAPIBNAAQgCgPgKgKQgLgJgQAAQgPAAgLAJg");
	this.shape_12.setTransform(64.3,-0.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#3F3302").s().p("AAhBRIAAhDQAAgPgJgJQgJgJgOAAQgNAAgKAJQgLAKAAAOIAABDIgRAAIAAihIARAAIAABAQAHgIAJgFQAJgHAKABQAMAAAIADQAIADAGAGQAPAPAAAVIAABEg");
	this.shape_13.setTransform(50.4,-3.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#3F3302").s().p("AgJBOIAAhjIgXAAIAAgPIAXAAIAAgpIAQAAIAAApIAaAAIAAAPIgaAAIAABjg");
	this.shape_14.setTransform(39.1,-2.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3F3302").s().p("AgLBSIAAhjIgUAAIAAgQIAUAAIAAgKQAAgSALgKQALgJAUAAIABAAIAAAQIgCAAQgNAAgGAFQgGAGAAALIAAAJIAbAAIAAAQIgbAAIAABjg");
	this.shape_15.setTransform(24.8,-3.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#3F3302").s().p("AgpAqQgPgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAQARQAPARAAAYQAAAZgPARQgQARgaAAQgZAAgQgRgAgcgeQgLAMAAASQAAATALAMQAKAMASAAQATAAAKgMQALgMAAgTQAAgSgLgMQgKgMgTAAQgSAAgKAMg");
	this.shape_16.setTransform(14.1,-0.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#3F3302").s().p("AgdA7IAAhzIARAAIAAAdQAFgSASgJQAHgDALAAIABAAIAAARIgCAAQgSAAgLAPQgLAOAAATIAAAzg");
	this.shape_17.setTransform(-2.7,-1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#3F3302").s().p("AgoAqQgQgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAPARQAQARAAAYIAAAHIhgAAQACAQALAKQALAKARAAQAWABAKgUIATAAQgFAQgOAJQgMALgVgBQgZAAgQgRgAgaghQgKAKgDAPIBOAAQgCgPgLgKQgKgJgQAAQgPAAgLAJg");
	this.shape_18.setTransform(-14.5,-0.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#3F3302").s().p("AgJBOIAAhjIgYAAIAAgPIAYAAIAAgpIAQAAIAAApIAbAAIAAAPIgbAAIAABjg");
	this.shape_19.setTransform(-25.5,-2.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3F3302").s().p("AgJBOIAAhjIgYAAIAAgPIAYAAIAAgpIAQAAIAAApIAaAAIAAAPIgaAAIAABjg");
	this.shape_20.setTransform(-34,-2.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#3F3302").s().p("AgpAqQgPgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAPARQAQARAAAYIAAAHIhgAAQACAQALAKQALAKARAAQAWABAKgUIAUAAQgHAQgNAJQgNALgUgBQgZAAgRgRgAgaghQgKAKgDAPIBOAAQgDgPgJgKQgMgJgPAAQgPAAgLAJg");
	this.shape_21.setTransform(-44.9,-0.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#3F3302").s().p("AgIBRIAAihIAQAAIAAChg");
	this.shape_22.setTransform(-54.7,-3.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#3F3302").s().p("AgJBOIAAhjIgYAAIAAgPIAYAAIAAgpIAQAAIAAApIAaAAIAAAPIgaAAIAABjg");
	this.shape_23.setTransform(-67.7,-2.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#3F3302").s().p("AgjAwQgMgKAAgQIASAAQABAPARAGQAFABAGAAIAKAAIAKgDQALgEAAgLQAAgGgGgFQgFgDgJgCIgSgFQgJgCgJgDQgIgDgGgGQgFgGgBgKQABgQAKgJQAMgIAWAAQAVAAAMAKQAKAIABAPIgRAAQgCgTgaABQgaAAgBAQQAAAHAHAEQAFADAIACIASAFQAKACAIADQAJACAFAHQAGAGABALQAAAQgOAJQgMAKgWgBQgVAAgPgLg");
	this.shape_24.setTransform(-78,-0.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#3F3302").s().p("AgdA7IAAhzIARAAIAAAdQAFgSASgJQAHgDALAAIABAAIAAARIgCAAQgSAAgLAPQgLAOAAATIAAAzg");
	this.shape_25.setTransform(-88,-1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#3F3302").s().p("AgIBPIAAhzIAQAAIAABzgAgIg3IAAgXIARAAIAAAXg");
	this.shape_26.setTransform(-95.9,-3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#3F3302").s().p("AgLBSIAAhjIgUAAIAAgQIAUAAIAAgKQAAgSALgKQALgJAUAAIABAAIAAAQIgCAAQgNAAgGAFQgGAGAAALIAAAJIAbAAIAAAQIgbAAIAABjg");
	this.shape_27.setTransform(-102.8,-3.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#3F3302").s().p("AgoAqQgQgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAQARQAPARAAAYIAAAHIhfAAQABAQALAKQALAKAQAAQAXABAKgUIATAAQgFAQgOAJQgNALgUgBQgZAAgQgRgAgaghQgKAKgCAPIBNAAQgCgPgLgKQgLgJgPAAQgPAAgLAJg");
	this.shape_28.setTransform(-119.4,-0.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#3F3302").s().p("AAhBRIAAhDQAAgPgJgJQgJgJgOAAQgNAAgKAJQgLAKAAAOIAABDIgRAAIAAihIARAAIAABAQAHgIAJgFQAJgHAKABQAMAAAIADQAIADAGAGQAPAPAAAVIAABEg");
	this.shape_29.setTransform(-133.4,-3.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#3F3302").s().p("AgJBOIAAhjIgYAAIAAgPIAYAAIAAgpIAQAAIAAApIAaAAIAAAPIgaAAIAABjg");
	this.shape_30.setTransform(-144.7,-2.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#3F3302").s().p("AgJBOIAAhjIgYAAIAAgPIAYAAIAAgpIAQAAIAAApIAaAAIAAAPIgaAAIAABjg");
	this.shape_31.setTransform(-159.1,-2.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#3F3302").s().p("AgnAqQgPgRAAgZQAAgYAPgRQAQgRAYAAQAUgBAPALQAOALAFAQIgSAAQgEgKgJgGQgIgFgLAAQgKgBgIAEQgIADgFAGQgLAMAAASQAAATALAMQAMAMATAAQALAAAIgFQAJgGAEgKIASAAQgFAQgOALQgPALgUgBQgYAAgQgRg");
	this.shape_32.setTransform(-170,-0.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#3F3302").s().p("AgoAqQgQgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAPARQAQARAAAYIAAAHIhgAAQACAQALAKQALAKARAAQAXABAJgUIATAAQgGAQgNAJQgMALgVgBQgZAAgQgRgAgaghQgKAKgDAPIBOAAQgDgPgKgKQgKgJgQAAQgPAAgLAJg");
	this.shape_33.setTransform(-183.6,-0.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#3F3302").s().p("AgIBRIAAihIAQAAIAAChg");
	this.shape_34.setTransform(-193.4,-3.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#3F3302").s().p("AgoAqQgQgRAAgZQAAgYAPgRQAQgRAZAAQAaAAAPARQAQARAAAYIAAAHIhgAAQACAQALAKQALAKARAAQAWABAKgUIATAAQgFAQgOAJQgMALgVgBQgZAAgQgRgAgaghQgKAKgDAPIBOAAQgCgPgLgKQgKgJgQAAQgPAAgLAJg");
	this.shape_35.setTransform(-203,-0.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#3F3302").s().p("AgtBAQgPgMgCgWIASAAQABAQAOAJQAMAHAUAAQAqAAAAgbQAAgKgHgFQgIgFgMgEIgYgGQgNgDgLgDQgMgGgIgHQgIgIABgNQAAgTANgLQAPgMAcAAQAdAAAPAOQAPALABASIgSAAQgCgSgWgHQgIgCgKAAQgMAAgIACQgIAEgFADQgDAEgCAEIgBAKQAAAFADAFQAFAEAHADIARAGIASAEIAUAGQAJACAHAFQARAKAAASQAAAWgQAKQgQAMgdAAQgeAAgRgOg");
	this.shape_36.setTransform(-217.6,-2.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#9A5E05").ss(3.5,1,1).p("EAkVgAvQAAg7gzgoQg0gphKAAMhDHAAAQhKAAgzApQg0AoAAA7IAABfQAAA7A0AoQAzApBKAAMBDHAAAQBKAAA0gpQAzgoAAg7g");
	this.shape_37.setTransform(0,-2.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFCC00").s().p("EghjAC8QhKAAgzgpQg0goAAg7IAAhfQAAg7A0goQAzgpBKAAMBDHAAAQBKAAA0ApQAzAoAAA7IAABfQAAA7gzAoQg0AphKAAg");
	this.shape_38.setTransform(0,-2.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_38},{t:this.shape_37}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.qtxt, new cjs.Rectangle(-234.2,-22.7,468.6,41.1), null);


(lib.pinkcar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,2.5).ss(1,1,1,3,true).p("AAYgLQACARgyAGIAAgBAAYgLQgwACAAAUAAYgLIAAAA");
	this.shape.setTransform(-9.2,-34.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#637778").s().p("AAYgLIAAABIgvAWQgBgUAwgDg");
	this.shape_1.setTransform(-9.3,-34.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#403D50").s().p("AgYALIAwgWQACARgyAGg");
	this.shape_2.setTransform(-9.2,-34.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,2.5).ss(1,1,1,3,true).p("AgYgKQAAASAxADIAAgBQgBgUgwAAg");
	this.shape_3.setTransform(8.4,-35.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#637778").s().p("AgYgJIAAgBQAwABABAUg");
	this.shape_4.setTransform(8.4,-35.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#403D50").s().p("AgYgKIAxAUIAAABQgxgEAAgRg");
	this.shape_5.setTransform(8.4,-35.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.1,-3.2,0,-0.1,-3.2,21.4).ss(1,1,1,3,true).p("ACvAPQiwhIitBN");
	this.shape_6.setTransform(-0.2,-28.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.2,-4.1,0,-0.2,-4.1,20.3).ss(1,1,1,3,true).p("AARAAQgRAAgQAA");
	this.shape_7.setTransform(1.2,53.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.8,-33,0,-0.8,-33,4.4).ss(1,1,1,3,true).p("AgMgTQAAABAAABIABAAQAJAWAPAP");
	this.shape_8.setTransform(-19.2,55.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().rs(["#700000","#4B0101"],[0,0.988],17.5,-0.9,0,17.5,-0.9,21.1).ss(1,1,1,3,true).p("AATALQgVAAgQgNAgKgKQAMAMARAJ");
	this.shape_9.setTransform(-16.8,58.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.4,-18.7,0,-0.4,-18.7,4.7).ss(1,1,1,3,true).p("AgIgUQgCAWAOAQQACADADADAgIgVIAAABAgIgVIAAAAAgIgYQAAABAAAC");
	this.shape_10.setTransform(-19.7,55.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().rs(["#700000","#4B0101"],[0,0.988],11.8,-6.7,0,11.8,-6.7,20.3).ss(1,1,1,3,true).p("AACAAQgDAAgDAAQgBAAAAAAQgBAAgBAAAAIABIgGgB");
	this.shape_11.setTransform(-10.9,53.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().rs(["#700000","#4B0101"],[0,0.988],11.4,-25.3,0,11.4,-25.3,22.1).ss(1,1,1,3,true).p("ABCBHIgogEABEBGQgBABgBAAAAKBBQgBAAAAgBQgSgDgQgNQgBAAAAAAQgZgVgQhh");
	this.shape_12.setTransform(-12.7,46.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().rs(["#700000","#4B0101"],[0,0.988],5.2,0.6,0,5.2,0.6,9).ss(1,1,1,3,true).p("AggllQgBBUAEBWQADBNAGBPQANCoAYCzQADAbANAP");
	this.shape_13.setTransform(-20.6,21);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.8,-12.2,0,0.8,-12.2,3.6).ss(1,1,1,3,true).p("AgRjAQABAPABAOQALD/AWBkIAAAB");
	this.shape_14.setTransform(-22.3,34.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().rs(["#700000","#4B0101"],[0,0.988],2.5,10.8,0,2.5,10.8,3.8).ss(1,1,1,3,true).p("AADghQgHAFAAAEIABApQAAAIADAEIAGAIQgBAFgEACAgCgrQAGABgBAJAgFgoIADgD");
	this.shape_15.setTransform(-24.2,10.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().rs(["#700000","#4B0101"],[0,0.988],3.3,6.5,0,3.3,6.5,6.6).ss(1,1,1,3,true).p("AgdBLQAZhpAigs");
	this.shape_16.setTransform(-18.4,-47.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.8,-5.3,0,4.8,-5.3,8.2).ss(1,1,1,3,true).p("AABhOIgBCd");
	this.shape_17.setTransform(-21.4,-32.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().rs(["#700000","#4B0101"],[0,0.988],19.2,-6.1,0,19.2,-6.1,21.4).ss(1,1,1,3,true).p("AgSALQACgDAAgBQARgJASgI");
	this.shape_18.setTransform(-19.6,-25.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().rs(["#700000","#4B0101"],[0,0.988],19.3,6.5,0,19.3,6.5,21.6).ss(1,1,1,3,true).p("AgWjtIAWBeQAnEFgbBtQAAABAAABQgGAGgDACQAAABgBgBIgBAA");
	this.shape_19.setTransform(-19,8.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().rs(["#700000","#4B0101"],[0,0.988],20.1,16.3,0,20.1,16.3,23).ss(1,1,1,3,true).p("AAAggIAABB");
	this.shape_20.setTransform(-21.5,-19.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.9,-0.4,0,0.9,-0.4,2.7).ss(1,1,1,3,true).p("AAPDvQgGgBgDgOQAAgBAAgBQgRjAgDkLQAAgBAAAA");
	this.shape_21.setTransform(-20.4,8.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.1,0,0,0.1,0,2.9).ss(1,1,1,3,true).p("AAGgXIgUAQQgVAPALARAAcgMQgEgMgNAAQgCAAgDABAAcgMQAAABAAABQABAGgBAE");
	this.shape_22.setTransform(-26.3,-15.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().rs(["#700000","#4B0101"],[0,0.988],19.6,-8.7,0,19.6,-8.7,23).ss(1,1,1,3,true).p("AAAgHQAAABAAAAQAAACAAADQAAACAAACIAAABIABAE");
	this.shape_23.setTransform(-21.5,-23.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().rs(["#700000","#4B0101"],[0,0.988],10.2,6.4,0,10.2,6.4,17.8).ss(1,1,1,3,true).p("AAPgNQgPAJgOAS");
	this.shape_24.setTransform(-13.9,-56.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.4,11.4,0,-0.4,11.4,7.7).ss(1,1,1,3,true).p("AAthYQhTASgGAzQAAABAAAAIAABr");
	this.shape_25.setTransform(-16.9,-49.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().rs(["#700000","#4B0101"],[0,0.988],1.7,0.4,0,1.7,0.4,8).ss(1,1,1,3,true).p("AgPiuQgNAMgEAaQgSCIgCDGIAOAKAgPiuIAAAAAA1jQQg0AQgQAS");
	this.shape_26.setTransform(-19.3,-38);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.1,-3.9,0,-0.1,-3.9,20.3).ss(1,1,1,3,true).p("AARABQAbgBAbgBAgQACQgbAAgbgC");
	this.shape_27.setTransform(1.1,53.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-17.4,-0.4,0,-17.4,-0.4,20.8).ss(1,1,1,3,true).p("AgUAKQAOgIAKgKQABAAAAgBAAUgJQgPASgZAB");
	this.shape_28.setTransform(19.4,58.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12.3,-24.6,0,-12.3,-24.6,22.1).ss(1,1,1,3,true).p("AAcAvIgBABQgQANgRAFQgCAAgCAAQgBAAAAAAQgaAEgaADABAhIQgMBigYAV");
	this.shape_29.setTransform(14.6,46.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.5,-18.4,0,-0.5,-18.4,4.7).ss(1,1,1,3,true).p("AAGgVQADAZgNARIgBABIgBAA");
	this.shape_30.setTransform(22.1,54.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.2,-33.3,0,-0.2,-33.3,4.4).ss(1,1,1,3,true).p("AAOgWQAAACgBACQgJAXgRAS");
	this.shape_31.setTransform(21.3,54.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-20,-15.7,0,-20,-15.7,21.9).ss(1,1,1,3,true).p("AgGAJIAAgBIAAAAQABAAAAABQAKgBADgQAgHAJQAAAAABAA");
	this.shape_32.setTransform(21,30.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.1,0.1,0,0.1,0.1,18.1).ss(1,1,1,3,true).p("ACdhPIABAAQAZgBgCAXQAAABABABQgJBKgWAdIgBAAQgQAQgSAEIgBABIgBAAQhpAThzgOIgDAAQgTgEgRgQAiTA0QgegegDhNQgBgCAAgCQABAAAAgBQABgLAMgBQABAAABAAQCmAMCcgT");
	this.shape_33.setTransform(1,43.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-5.2,1.1,0,-5.2,1.1,9).ss(1,1,1,3,true).p("AgHE+QASizAHipQADhPAAhNQAAhYgDhUAgIE/IABgBAgUFnQALgOABga");
	this.shape_34.setTransform(21.8,20.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-1.1,-12.6,0,-1.1,-12.6,3.6).ss(1,1,1,3,true).p("AALjAQAAAPAAAOQgDD+gSBmIABAA");
	this.shape_35.setTransform(23.7,33.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,-9.6,0,0,-9.6,29.1).ss(1,1,1,3,true).p("AEDAPIAAgpQgBgGgCgFQgCgCgBgDIgEgCAD7AlIAFgIQACgCABgCQABgDgBgHAj6gjIgDADQgCACgBADQgDAFAAAGIABAoQAAAIABADQABACABABIAGAI");
	this.shape_36.setTransform(0.3,10.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-2,11,0,-2,11,3.7).ss(1,1,1,3,true).p("AAFgoIgEgDQgGABABAJQAHAEABAFIAAAqQAAAIgDADIgFAIQACAGACAB");
	this.shape_37.setTransform(24.7,9.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-3,5.8,0,-3,5.8,6.6).ss(1,1,1,3,true).p("AgfhNQAiApAXBYQADANADAN");
	this.shape_38.setTransform(17.2,-47.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-19.4,16.4,0,-19.4,16.4,23).ss(1,1,1,3,true).p("AgBglIADBL");
	this.shape_39.setTransform(21.1,-19.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#3A2201").ss(1,1,1,3,true).p("ADrgRQABgBAAgBQAzAVABAIQgHADgXgMQgFgDgEgCAjpgGQgCAEgCACQgBAAAAAAQgHAFgFADQgWAMgOgBQgBgHAbgMQAagLABAFg");
	this.shape_40.setTransform(0,-14.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,3).ss(1,1,1,3,true).p("AgNgYQgBAAAAAAQgFACgFAFQgCACgCADQAAABgBABIAAAAAgNgYQADgBADAAQASAMAKAKIAAABQAPALgJARAgZADQAAgBAAAAAgYgCQgBACAAABQAAABAAAAQgEgGAAgG");
	this.shape_41.setTransform(26,-16.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.7,25.3,0,0.7,25.3,21.4).ss(1,1,1,3,true).p("AClBYQAIABAEgJIAjitAibBdQgBAAgBABQgGAAgDgFQAAgBgBgCIgsisAiaBdIgBAA");
	this.shape_42.setTransform(-0.3,-14.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.3,8.2,0,0.3,8.2,21.4).ss(1,1,1,3,true).p("ACcAIIAAgBQicgsibAz");
	this.shape_43.setTransform(0,-6.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-15.9,-12.4,0,-15.9,-12.4,21.4).ss(1,1,1,3,true).p("AADABQAAgBAAAAQgCAAgBAAIgBAAQgBAAAAAA");
	this.shape_44.setTransform(15.9,-5.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-18.8,7.1,0,-18.8,7.1,21.5).ss(1,1,1,3,true).p("AAAiCIAAAFQgiD1AhBtQAEAIAFABAATjtIgTBr");
	this.shape_45.setTransform(19.4,7.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-4.8,-5.1,0,-4.8,-5.1,8.3).ss(1,1,1,3,true).p("AgChIIAFCR");
	this.shape_46.setTransform(20.7,-32.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-19.5,-5.4,0,-19.5,-5.4,21.4).ss(1,1,1,3,true).p("AAQAGQABABAAABQAAAAABAAAAPAGIABAAAgRgIQARAHAPAH");
	this.shape_47.setTransform(19.2,-26.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-19.8,-7.9,0,-19.8,-7.9,23).ss(1,1,1,3,true).p("AABgHIAAABQACADgCADQAAABgBABIgBAIAAAgJQAAABABAB");
	this.shape_48.setTransform(21,-24.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.4,11.3,0,0.4,11.3,8.1).ss(1,1,1,3,true).p("AgvhbQAGAAAGACQBSAOgDA1QAAABAAABIAEBw");
	this.shape_49.setTransform(15.5,-49.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-9.8,7.1,0,-9.8,7.1,17.8).ss(1,1,1,3,true).p("AgQgMQAEABAEADQANAIAMAN");
	this.shape_50.setTransform(12.3,-56.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-2.1,1.2,0,-2.1,1.2,8.3).ss(1,1,1,3,true).p("AA+DLQgNjHgViHQgBgBgBgCQgFgagMgKQgBgBAAAAQgQgRg1gOAA9DLIABAA");
	this.shape_51.setTransform(18.3,-39.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.3,0.7,0,0.3,0.7,14.7).ss(1,1,1,3,true).p("ABcAAQgMgCgLgBQgagFgZgBQgQgBgQACQgYABgZAFQgRACgUAGIgBAAQgHACgFACABfAAIgDAAAByAGQgIgEgLgC");
	this.shape_52.setTransform(-1,-59);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.rf(["#FF9900","#CC2D17"],[0,0.988],-0.8,0.9,0,-0.8,0.9,3.1).s().p("AgJARIgFgFQgPgQACgXIAAgBIAAAAIAAgCIABACIAAAAQAJAXAQAOQAMANARAKQgWgBgPgOg");
	this.shape_53.setTransform(-17.8,56.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.rf(["#6D0779","#320338"],[0,0.988],-1.6,3.6,0,-1.6,3.6,25.7).s().p("AAECwIgHg8QgohLAUh5IgFiLIADADIAEAAIABAdQALD+AWBkIAAABIgBACIABABIgBAAQgBAYAPAQIAFAGIgCADQgVgSgEgag");
	this.shape_54.setTransform(-21.8,36.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.rf(["#C11BD5","#72087F"],[0,0.988],-1.4,-0.1,0,-1.4,-0.1,4.6).s().p("AACAsIgCgDIgGgIIgCgDIgBgLIgBgoQAAgGADgFIADgEIADgEIACgDQAIABgBAJQgIAFAAAEIAAApQABAIADAEIAHAIQgCAFgDACIgCAAIgCAAg");
	this.shape_55.setTransform(-24.6,10.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#5E0668","#920CA2"],[0,0.988],-2.5,-0.2,0.9,-0.5).s().p("AAIFDIgBAAIAAgCIAAgBQgWhkgLj/IgBgdQADgBACgGIgHgIQgDgEgBgHIAAgrQAAgDAIgGQABgIgIgBIgDADIgDjNIAMgIQgBBUAEBWQADBOAGBOQANCoAYCzQADAbANAPQAAADgGAEQgQgPgJgXg");
	this.shape_56.setTransform(-21.2,21.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.rf(["#67798D","#4F5D6F","#313C46"],[0,0.91,0.988],-0.9,1,0,-0.9,1,22.5).s().p("AAFDxIgBAAQgFgBgDgOIABgDQgTi/gDkLIAAgBIABgEIAFAHIAWBdQAnEGgbBtIAAABQgGAHgDABIgBABIAAAAg");
	this.shape_57.setTransform(-19.3,8.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.rf(["#ED99F7","#D42CE8","#850894"],[0,0.259,0.988],-2.1,1.2,0,-2.1,1.2,5.5).s().p("AgOgHIAUgQIAFgBQANAAAEAMIAAACIAAAKQgBgFgaALQgZAMAAAHQgLgRAVgPg");
	this.shape_58.setTransform(-26.3,-15.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.lf(["#870896","#D127E4"],[0,1],-9.9,1.6,5,-2.8).s().p("Ag+CoIABieQAZhoAjgtQAagVAmgIQg1AmgiEVQgSAIgRAKIgCAEg");
	this.shape_59.setTransform(-15.2,-41.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],-1.5,-4.1,0,-1.5,-4.1,7).s().p("AgsgSIABgBQAFgzBTgRQgQAIgOATQgiAsgZBogAAOhJIgBABQg3ATADAsIAAABIAAAhIABAOQgBAKADgHIADgKQARg6AZggIAGgIIAEgDIAEgFQABAAAAAAQAAgBAAAAQgBAAAAAAQAAAAAAAAIgJACg");
	this.shape_60.setTransform(-16.9,-49.1);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.lf(["#500558","#840793"],[0,0.988],-5.7,-0.1,-3.6,-0.1).s().p("AgzDHIgBh/QgOhdAYhEIABgCIAAgCQAFhWAjgNQgBAAABAAQAagMAhgGQg0AQgPARIgBABQgMALgFAbQgSCJgBDFIANAKIAAACIgBADQgFgMgMAAg");
	this.shape_61.setTransform(-19.9,-37.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.lf(["#60046A","#C820DB","#7F058D"],[0,0.447,0.988],-1.8,5.2,-7.6,5).s().p("AgyC3IgCgDQgHgHAAAVIAAAMIgNgKQABjFASiJQAFgaAMgMIABAAQAQgSAzgQIAGgBIAjgFQgMAFgMAKQhTARgFA0IgBABIAABrIgBCdIAAABIAAAAIgBACIAAAFIABAFIgBAAIACAFIgBBCg");
	this.shape_62.setTransform(-17.3,-37.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.lf(["#9AAEBA","#DBE3E8"],[0,0.988],-0.1,-3.4,0.1,3.5).s().p("AhEAgIgChAQAbACAbAAIABBBQgcAAgZgDgAARgfQAbgBAbgDIAABBQgaAEgcABg");
	this.shape_63.setTransform(1.1,57.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#580363").s().p("AhoBmQg6gNgTgbQAYAUA1AMQATAFAVADQAYACAcABIAagBIAHABQAbgCAagEQAegEAZgIQAlgLASgMIAEgFIABABIAEgEIgEAGQgPAVgtANQgyAOhBACIgVAAQg1AAgsgKgAhCAlIgCAAIgogEIgGgBIgHgBIgBAAIgCAAIgBAAQgTgEgQgMIgBgBQgZgUgQhiQAWBeAWASIAAAAQAQALARAFIACAAIABAAIA2AGIABgBQAeACAZAAQAPABARgBQAagBAbgCIAzgHIABgBIAEAAQARgFAQgNIABAAQAWgTASheQgMBjgZAUIAAABQgRANgRAFIgEAAIgCAAIg0AHQgaADgbABIghABQgbAAgbgCg");
	this.shape_64.setTransform(0.8,50.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.lf(["#60046A","#A606B9"],[0,0.988],0.1,3.3,0,-1.5).s().p("AhdAbIgCAAQhJgGgigbIACgDQAQANAWAAQgRgIgNgNQAGgEAAgDIgBgFQATAbA6AMQA1AMBBgCQBBgCAygPQAtgMAPgUIACAIQAAABgBAAQgLALgOAIQAagBAPgTIADADQgfAghRAKQg2AGg1AAQgmAAgngDg");
	this.shape_65.setTransform(1.4,59.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.rf(["#FF9900","#CC2D17"],[0,0.988],1.1,0.7,0,1.1,0.7,3.5).s().p("AgBANQABAAgBAAQARgRAJgYIABgDQADAagNAQIgCABIAAAAQgPAUgZAAQAOgIALgLg");
	this.shape_66.setTransform(20,55.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.rf(["#C60203","#870101"],[0,1],-22.6,-46.7,0,-22.6,-46.7,66.6).s().p("AAAAAIAAAAIAAABIAAgBg");
	this.shape_67.setTransform(21.9,29.6);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.lf(["#8499AC","#657A8D"],[0,0.988],0.1,4.8,0,-4.7).s().p("AhcAkQgmgHgJgWQgJgWAogNQAngNBBgBQBBAAAqAHQAqAJgEAZQgEAZgoAJQgoAIg3ACQg4AAgmgHg");
	this.shape_68.setTransform(0.8,45.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.lf(["#788AA0","#486575"],[0,0.988],0.2,5.9,-0.1,-5.7).s().p("AgKBGQhcgBghgUQgXgagHhAIAAgEQAAgMAMgFQCdALCVgSQANADAAARQAABKggAYQgvAVhYAAIgJAAgAgEgXQhBABgnAMQgoAMAJAXQAJAWAmAHQAmAHA4AAQA3gBAogIQAogJAEgaQAEgYgqgJQgngHg8AAIgIAAg");
	this.shape_69.setTransform(0.8,44.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#424056").s().p("AhqBKIgDAAQgTgEgRgQIgCgCQgegegDhNIgBgEIABgBQABgLAMgBIACAAQCmAMCcgTIABAAQAYgBgBAXIABACQgJBKgXAdIgBAAQgQAQgRAEIgCABIAAAAQg9ALg+AAQgwAAgxgGgAingqIAAAEQAIA/AWAaQAhAVBcAAQBeABAygVQAggYAAhKQAAgSgNgDQiUASiegKQgMAEAAANg");
	this.shape_70.setTransform(1,43.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.rf(["#6D0779","#320338"],[0,0.988],2.2,2.4,0,2.2,2.4,24.2).s().p("AgYDXIABAAIABgBQAOgRgDgbIgBAAQAShlADj/IAAgdQAAABABAAQAAAAABAAQAAAAABAAQAAAAABgBIACgCIABCKQAXB5glBNIgEA8QgDAVgQASg");
	this.shape_71.setTransform(23.9,35.6);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.rf(["#C11BD5","#72087F"],[0,0.988],1.8,-0.3,0,1.8,-0.3,4.8).s().p("AgEAsQgDgCgCgFIAGgIQADgDAAgJIAAgpQgBgFgIgEQgBgJAHgBIADADIADADIADAFQADAEAAAHIABAoQAAAIgBADIgCADIgFAIIgCADIgCAAIgCAAg");
	this.shape_72.setTransform(25.2,9.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.lf(["#5E0668","#920CA2"],[0,0.988],2.1,-0.8,0.2,-0.9).s().p("AgaFlIAEgGIgEADIgBgBQAMgOABgZIABgCQASizAGioQADhQABhMQAAhZgDhUIAMAGIAEDQIgEgDQgGABAAAJQAIAFABAEIAAArQAAAIgDADIgGAIQACAGAEABIAAAdQgDD/gTBkIABABIgBADQgJAZgRARIgCgIg");
	this.shape_73.setTransform(22.4,20.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#5E0668").s().p("AjNDhIgBgEQgIhkgFhzIgDg7QgDhVAAhlIAHAIIAAABQADELATC/IgBADQADAOAGABQgNgBgEgUgADKDwIABAAIAAAAIgBABIAAgBgADLDwIABAAQAGgCABgMIAAgCIAAgBQAKjAgGkMIAEgFIAEgDQADBjAABVIgBA9QgBB1gFBhQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAIAAABIAAACIAAABIgBABQgDAQgLABIgBAAg");
	this.shape_74.setTransform(0,7.6);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-18.3,-18.4,0,-18.3,-18.4,7).s().p("AkeDbQgBgHAbgMQAagMABAFIgEAHIgBAAIgMAIQgVAMgNAAIgCgBgAECDJIgJgGIgNgGIAAgBIgBgGIABgCQAzAWABAIIgDABQgIAAgTgKgAjPhoIgBgNIAAgiIAAgBQgDgsA5gUIABgBQAKgDgBACIgFAFIgEAEIgGAHQgaAggRA7IgDAKIgCADIAAgGg");
	this.shape_75.setTransform(0,-34.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.rf(["#B915CD","#850894"],[0,0.988],0.9,35.8,0,0.9,35.8,88.4).s().p("AgEFEIAhgBIAABDIgHgBIgaABgAhgF7Qg1gMgYgTIABAEQgNgPgDgbQgZiygNioQgGhPgDhNQgEhWABhUIABAAIAEgHIAAgKIAAgCIABgEIAHAHQAABlADBVIADA8QAFBzAIBiIABAEIABAHIAAABIAIBHIAAAFIACALQAQBiAZAVIABABQAQAMATAEIABAAIACAAIABAAIAHABIAGABIAoAFIACgBIACBBQgVgDgTgFgABSE/IA0gGIACgBIAEAAQARgEARgOIAAAAQAZgWAMhjIADgUIAAgGIAEhAIABAAIAAgBIAAgBIAAgBIAAgBQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAAAgBQAFhgABh3IABg8QAAhVgDhjIAHgFIAAABQAAAFAEAHIABABIABABIAAABQADBUAABZQgBBMgDBQQgGCogTCzIgBACQgBAZgMAOIgEAFQgSANglAKQgZAIgeAFg");
	this.shape_76.setTransform(0,21.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.rf(["#ED99F7","#D42CE8","#850894"],[0,0.259,0.988],2.5,1.6,0,2.5,1.6,5.3).s().p("AgYgDIAAABIgBADIAAABQgEgGAAgGIAAAAIABgCIAEgFIAJgGIABgBIABABIAAgBIAGgBQASAMAKAKIAAABQAPALgJARQAAgJgzgUg");
	this.shape_77.setTransform(26,-16.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#5C0966").s().p("AjfAQIgHgGIAAgCIgBgMQAAgVAHAHQgDAIACAKQABAJAIALIAAAEgADgAEIADgMIgCgEQgBgDgDgBIABgBQAEgMAFAMIABAWIgIAFIgEADIAEgJg");
	this.shape_78.setTransform(-0.2,-18);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#800C8D").s().p("AjYAQQgIgLgBgJQgCgKADgIIADAEIAJAhIACAIIgGgHgADYAIIAFgcQADABABADIACAEIgDAMIgEAJIgEAFIAAgGg");
	this.shape_79.setTransform(-0.2,-17.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.rf(["#5A6F82","#475863"],[0,1],-0.1,-1.4,0,-0.1,-1.4,6.3).s().p("AgdgMQAcgLAbAJIAaAfQg0gDgzAFg");
	this.shape_80.setTransform(0.2,-10.9);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#E883F3").s().p("AhQgHQAtgGA1ABQA0AAAmAHQApAGAIALQidghicAiQAZgNAzgHg");
	this.shape_81.setTransform(0.2,-5.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.lf(["#E638FA","#A404B7"],[0,0.988],-0.5,-20.2,1.1,20.3).s().p("AiDDIIgDgCQgLgFgBgPQAAirgQihQgFgVAGgGIACgCQCbgzCbAtIABAAIACAAIAAAAIAEADIAAAAQAJAHgCAPIAAACQgTC3AJCVIAAABIAAACQgBAOgMAGQAAAAgBAAQAAAAAAABQgBAAAAAAQgBAAAAAAQhKARhKAAQg9AAg9gLgAhSjDQgzAHgZAOQCdgjCcAiQgIgLgpgHQgmgHg0AAIgOgBQgtAAgnAGg");
	this.shape_82.setTransform(0.4,13.5);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.rf(["#C720DB","#7D058B"],[0,1],-0.9,-31.9,0,-0.9,-31.9,66.6).s().p("AgGF/QgYAAgfgCIgBABIg2gGIgBAAIgCAAQgRgFgQgLIAAAAQgWgTgWheIgCgLIAAgFIgIhHIAAgBIgBgHQAEAVANAAIABAAQABABAAgBQADgCAGgGIAAgCQAbhtgnkFIgXheIgCgIIABhCIAsCtIAAACQAEAFAFAAIADAAIAAgBIAAABQgGAGAFAWQAQChAACqQABAPALAGIADACQCIAZCGgfQAAAAABgBQAAAAABAAQAAAAABAAQAAgBAAAAQAMgGABgNIABgDIgBAAQgJiWATi2IAAgCQACgQgJgHIABAAQAHABAEgJIAjiuIAEBMIgUBrIAAAEQgiD1AhBtQAFAIAFABIABAAIAAgBIABAAIAAABQALgBADgRIgEBAIABAGIgEAUQgSBegWAUIAAAAQgRANgRAFIgDAAIgCABIgzAHQgbACgaABIgUAAIgMAAgAB7FmIAAABIABgCQASgDAQgQIABAAQAWgeAJhKIgBgDQACgWgYAAIgBAAQicATimgLIgCAAQgNAAgBALIAAACIAAADQAEBPAeAeIACACQARAPATAFIACgBQBzAPBqgUg");
	this.shape_83.setTransform(0.2,14.9);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.rf(["#67798D","#4F5D6F","#313C46"],[0,0.91,0.988],1.4,2.2,0,1.4,2.2,22.5).s().p("AgEDpQgghtAhj1IAAgEIAThrIACgJIAAAGQAHELgLDBIAAABIAAACQAAAMgGABIgBABIAAAAIgCAAQgFgBgEgIg");
	this.shape_84.setTransform(19.6,7.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#74899C").s().p("AiiA6IgDgRIgQhAQCwhMC7BJIgRBOQgCAGgHgCQgzgPgzgFIgZggQgdgIgbAKIgVAgQg2AHg1ARIgCABQgEAAgBgFg");
	this.shape_85.setTransform(-0.1,-12.8);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#4C6174").s().p("AjGAbQgDgNAKgFQDAhNDABGQAGAEABAJIgBAFIAAABIgBADQjHgLjEATg");
	this.shape_86.setTransform(-0.2,-25.3);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#596E81").s().p("AjFgcQDEgSDHALIgPBIQi7hIiwBLg");
	this.shape_87.setTransform(-0.2,-19.2);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#E754F8").s().p("AhOgHQAcgHAbgCQAUgDATABQAdABAbAGQAoAIAnASQiXg1iWA5QAkgRAkgJg");
	this.shape_88.setTransform(-0.1,-30.5);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#3E414A").s().p("AinB9IAAgDIgsisIgCgEIABgBIgBgFIAAgFIABgBIAAgBIAAAAIABAAIACgEQARgJASgIQCuhPCwBJIAhAPIAAAAIABACIACAAIABADIAAABQACADgBADIgBACIgCAIIgjCtQgEAJgHgBQAAgBgBABIgEgCIAAAAIgCAAIgBAAQicgtiaAzIgCABIAAAAIgDABQgGAAgDgFgAi/hCQgKAFADANIABAFIARBEIAQBBIADAQIABAAQABAGAFgBQA2gSA1gGQAzgGAzAEQAzAFAzAOQAHADACgHIARhOIAQhJIAAgDIABgBIAAgFQAAgJgGgEQhcgihdAAQhjAAhlApg");
	this.shape_89.setTransform(-0.2,-17.7);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.lf(["#870896","#D127E4"],[0,1],9.4,2.5,-4.5,-4.4).s().p("ABDClIgBgCIAAAAIghgPQgtkYg4ghQAjAFAhAZQAiAqAXBYIAGAZIAGCRIgCAAg");
	this.shape_90.setTransform(14.1,-42);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],1.7,-1.5,0,1.7,-1.5,7.2).s().p("AAfA5IgDgLQgUg5gZgfIgGgIIgEgDIgFgFQgCgCALAEIACAAIABAAQA2ARgCArIAAACIABAjIAAAMQABAGgBAAIgCgCg");
	this.shape_91.setTransform(16.3,-51.6);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],2.6,-3.9,0,2.6,-3.9,7.2).s().p("AAsBCQgXhYgigpQgMgOgNgJQgEgDgEgBIgDgBIABAAIACAAIAMACQBSAOgDA1IAAACIAFBwIgGgagAgXhSIAFAFIAEADIAGAIQAZAfAUA5IADALQADAGgBgKIAAgMIgBgiIAAgCQACgsg2gRIgBAAIgCAAIgJgDQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAg");
	this.shape_92.setTransform(15.4,-49.2);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.lf(["#500558","#840793"],[0,0.988],6.2,-0.3,-5.8,-0.1).s().p("AA9DKQgMjGgWiHIgBgDQgFgagNgKIgBgCQgQgRg1gOQAiAGAbAMIABAAQAiAKAIBXIAAABIABACQAbBEgLBdIADB+IgBAAIABAAIAAABgAA+DKgAA+DKIAAAAg");
	this.shape_93.setTransform(18.4,-39);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.lf(["#60046A","#C820DB","#7F058D"],[0,0.447,0.988],2.3,-0.1,6.5,-0.4).s().p("AAxCQIACgIIAAgCQACgDgCgDIAAgBIgBgDIgHiQIgEhxIAAgCQADg1hSgOIgMgCIgCAAQgNgJgRgGIApAEIAFABQA0AOARARIABABQAMAKAGAaIABADQAWCIAMDGQgEABgFAFIgFAGIgBABIAAABIAAgBIgBgXQgEgMgFAMIAAABIgGAdIgCAJgABMDFIAAAAgABVC/IAAAAIgJAGQAFgFAEgBgABVC/g");
	this.shape_94.setTransform(16,-37.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.lf(["#9AAEBA","#DBE3E8"],[0,0.988],0.3,13.5,-0.6,-13.6).s().p("AhEiEQAYgGAagCIgCEQQgbACgcAHgAAUCCIgHkOQAZAAAZAFIANEPQgbgGgdAAg");
	this.shape_95.setTransform(-0.5,-45.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-0.9,-2.6,0,-0.9,-2.6,9.9).s().p("AhaABQAogPAygCQAyADAiAMQARAFAOAJIgBAAQgJgFgLgBIgDAAIgXgFQgZgDgZgBQgQgBgQABQgYACgZAEQgRADgUAFIgBABIgNAEQAMgLAMgFg");
	this.shape_96.setTransform(-0.9,-59.7);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.lf(["#A505B8","#E75EF7"],[0,1],0.2,13.2,-0.1,-15.2).s().p("AhWiXIADgCIADgBIAJgBIgHERQgjAJgkARQCVg6CYA2QgogSgngJIgNkPQAGAAAFADIABAAIABABQA4AgAuEZQiwhJitBOQAjkVA1gmgAgVijQAQgBAPABIAHEOQgTgBgVADg");
	this.shape_97.setTransform(-0.2,-43);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#A909BB").s().p("Ah0gCIAMgEIABgBQAUgFARgDQAZgFAZgCQAQgBAPABQAZABAZAEIAYAFIADAAQAKABAJAFIACABQAFABADADQAOAIAMAOQgigYgjgFIgBgBIgBAAQgFgCgGgBQgZgEgZAAQgPgBgQABQgaABgYAGIgJACIgDABIgDACQgmAHgbAVQAOgTAQgHg");
	this.shape_98.setTransform(-0.7,-57.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.pinkcar, new cjs.Rectangle(-30.1,-61.3,60.2,123.7), null);


(lib.yellowcar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC6600").ss(1,1,1,3,true).p("AhnptQAMgEAMgCQAbgFAbgCQARgCAQABQAaABAbAEQATADAUAGIACAAQAGABAHACQARAIAQAUQAmAuAfBvIAHCoADPpTIAAAAQAOAMAGAcQAYCRAJDTIgOALADVojQAAgBgBAAQgHg3hagPACEp0QA4APATASAC3keQATAHAUAKQAAABACADADgkIQABABABAAQAAADAAACQAAAEgBACIAAAAIgBAFACrg1QABgBAAABQABAAABAAQAHAAADgGQAAAAABgCIAni6IAEBHAEKjFQADAAADABIAWARQAWAQgKATAD4i1IAAAAQAAgBABgBQAEgNANgBADbFRQAGgBACgPQAAgCAAgBQALjMgGkcQAAAAAAgBAD4iqQABgFAdALQAcALAAAJAD4iqQAAgEAAgHADiinIgVBkQggEXAhBzQAAABABAAQAGAHADACQABAAAAAAQABAAABAAADZmxIgEhyAEMA6IADAEIADADQACACACADQADAFAAAHIABArQAAAJAAADQgCACAAACIgGAIAEOBOQAAgEgJgFQgBgJAIgCAEQB7IgCgtAEFCQIAHgIQADgEABgJAELCXQAAARAAAOQgDEQgUBrQAAABgBAAQABACAAABIAAAAIAAABQADAZgQASQgCADgDADQgQAQgXAAAELCXQgEgBgCgGADSJXQAOgQACgdQAUi/AHi1QAEhTAAhSQABhagDhaACpIGQAggiABhTQgBgBAAgCQAAgBAAgBQgCgMgNAAQgBAAgBAAQiwATimgPQgBABAAAAQgaAAADAYQAAABgBACQALBPAaAeIABAAQASARASADIACABIAAAAQBxAQB5gTIADAAQAUgGARgRADzI1IAAAAQAAgCAAgBADaJfQAQgSAJgYADjGgQgNBogaAXQAAABAAAAQgRAOgUAFQAAAAgBAAQgBAAgBAAACEI2IAHgCQAEAAAEgBIABAAABaI7QgBAAgBAAQgdADgcABQgSABgRgBQgcAAgdgBQgcgCgcgEQAAAAgBAAQgCAAgCAAQgTgEgSgOIAAAAQgbgXgRhoACEI2IgqAFADaJfQgOAOgRAKAkFjBQAFjOATiRQAAgCAAgBQAFgbANgNQAAAAABgBQASgTA3gQAjhoXQAAgBAAgBQgFg4BYgSQAFgBAHgBAhqptIADAAAh/plQAKgGALgCAjhmeQACgOADgOQAVhfAkgtQAMgQAOgJQAEgDAEgCAjgkFQgBAAAAABQgBAAAAABIABibIAAh5Ajjj2QgBgBAAgCQgBgDABgEQAAAAAAgBQABgBABgBAjgkGIAAABAkrijIAAAAQALgNATgMQALgDAMAQQAAABABAAQAAABAAAAAkxiEQAAgJA2gZQABABAAABQABABAAACQAAABAAACIAAAAQAFgJgBgGAjiidIABhRIgCgIAjIgsIAAAEQAtEEgeB0QgFAJgFABIAAAAQgBAAgBAAIgBAAQgMgBgEgRAkxiEQgJgRAPgOAjiidIAaBxAiogvIgBABQgHABgEgKIgti3AiigxIABgBQCkg1CnAxAijgxQgCABgDABAijgxQABAAAAAAAi8kXQgSAIgSAJAkHBKIADgDIAEgEQAIACgBAJQgIAFgBAFIABAtQAAAIADADIAIAJQgCAGgEACQABAQABAOQALEQAZBqIgBABQABACABABQALAaATASAkNBbQAAgGADgFQACgEABgCAkJCXQgBgCgBgDQgBgCAAgJIgBgsAkCCeIgHgHAjFIyQgbi+gNi1QgGhTgEhSQgEhcAAhaAjEIzIgBgBAibJ9QgQgIgMgLQAAgBAAAAAi1JcQgMgOgDgbAjIJpQAAgBAAABQASAUAbAAAjJJoIABABAjXI6QgCAcAQASAi8kXQC4hUC7BN");
	this.shape.setTransform(-0.3,-0.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#C60203","#870101"],[0,1],22.3,-50.4,0,22.3,-50.4,70.8).s().p("AAAAAIAAAAIAAABg");
	this.shape_1.setTransform(-21.9,31.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-1.8,-1.5,0,-1.8,-1.5,7.7).s().p("AggA5IAAgMIAAgmIAAgBQgEguA5gUIABAAIACgBQALgEgBADQgCADgDACIgEADIgHAJQgaAigSA9IgEAMIgBACQgBAAAAgHg");
	this.shape_2.setTransform(-19.1,-54.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],-2.9,-4,0,-2.9,-4,7.7).s().p("AgwgUIAAgCQgFg4BXgSIAMgCIACgBIABABIgCABIgIAEQgOAKgMAPQgjAtgVBfQgDANgCAPgAANhWIgCAAIgBABQg5AUAEAuIAAACIAAAlIAAAMQAAAMACgHIAEgMQASg+AaghIAHgJIAEgEQADgCACgCQAAgBAAAAQAAAAAAAAQAAAAAAAAQgBAAAAAAIgJACg");
	this.shape_3.setTransform(-18,-52.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A25A00").s().p("AiBARQgvgNgSgTIgEgGIAEADIABgBIAFAFQAUANAnAKQAbAGAgAEQAbADAeAAIAHAAQAOAAAOgBQAdgBAagEQAWgEAUgFQA4gNAYgXQgTAdg9APQg3APhGAAIgFAAQhDAAgzgNg");
	this.shape_4.setTransform(0.9,61.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC0000").s().p("AjWARIAAAAIgBAAQgQgSACgbIACADQALAZATARIAAABQAMAMAQAHIgCAAQgZAAgSgUgADMAIQAQgRAJgYIAAAAIAAgDIABADIAAAAIAAAAQADAZgQASIgFAGQgQAPgXABQARgLAOgNg");
	this.shape_5.setTransform(1.1,59.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFB500").s().p("AhdD/IAAAAQhWgHgjgjQgRgRgEgYIgIg/QgqhQAUiCIgEiTIACADIAEAAIACAeQALEPAZBqIgBABQgCAcAQASIABABIAAAAQASAUAbAAQgQgIgMgLIAAgBIABgJQASAUAvANQA1AOBGgBQBGAAA3gPQA9gQATgdIgBAFQAAAEAIAEQgOAOgRAKQAXAAAQgQIAFgGQAQgSgDgZIAAgBIAAAAIgBgDIABgBQAUhrADkPIAAgfIAEABIADgDIgBCTQAaCAgnBSIgGA/QgEAcgVAUQgjAfhOAJIgCAAQg8AHg6AAQgnAAgngDg");
	this.shape_6.setTransform(0.8,40.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFD400").s().p("AjZFbIgBgDIAAgBQgZhqgLkPIgBgfQADgBACgHIgHgIQgDgDAAgJIgBgtQAAgFAIgEQABgKgIgBIgDADIgEjdIANgHQAABaADBeQAEBRAGBUQANCzAcC+IABABQACAbANAPIgBABIgFgDIAFAGIgCAIQgTgRgLgagADPF1QANgRACgdQAVi+AHi0QADhUABhRQABhcgEhZIANAIIAEDZIgEgDQgHABABAKQAIAEABAFIABAtQAAAIgEAEIgHAJQACAGAEABIAAAfQgCEPgVBqIAAACIAAADIgBAAQgJAYgQASQgIgFABgDg");
	this.shape_7.setTransform(0,22);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFD000").s().p("Ah0gDQApgJA3gDQA5gDAwAFQA2AGAcANQingeimApQAIgMAqgIg");
	this.shape_8.setTransform(-0.1,-5.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#760101").s().p("AhNgKQAdgHAegCQAUgCAWACQAdACAfAGQAlAJAoAQQiig4ifA/QApgVAqgKg");
	this.shape_9.setTransform(-0.8,-32.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#6F3B00").s().p("AgCJaQgcAAgdgCQgcgCgcgDIgBAAIgEgBQgTgEgSgNIAAgBQgbgXgRhoQAWBjAZAVIABABQARAMATAFIAEAAIACAAQAaAEAcACQAcACAcAAIAigBQAagBAggDIACAAIA5gIIABAAIACgBQARgFAQgMIABAAQAXgVAThlQgNBpgaAXIAAABQgRANgUAFIgBAAIgCABIgBAAIgIABIgHABIgqAGIgCgBQgdAEgcABIgSAAIgRAAgAjDF0IgBAAQgMAAgEgRIAAgBIAAgCIgBgBIAAgBIgBgFQgIhngGh+IgDg/QgDhagBhpIAFADIADAFQAEEdATDMIAAABIAAACQABAMAHACIADAAIgCAAgADlFbIAAgCQALjNgGkbIAAgBIAHgJQAEBsAABZIAABAQgBB6gGBrIAAAEIAAACIgBACIAAABIAAABIAAAAQgFAPgLACQAGgCACgPgAD5igIgBgCIAOgLQgJjSgYiSQgGgcgOgLIAAgBQgTgSg4gOQAkAFAeALIAAABQAlALAJBcIAAACIABADQAbBHgLBkIAFCHQgNAAgEAOgAkFipIAAh/QgQhjAahIIABgDIAAgCQAFhcAlgMIABAAQAdgOAjgHQg3ARgSATIgBABQgNAMgFAbIAAADQgTCSgFDOg");
	this.shape_10.setTransform(-0.5,-3.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],1.4,-4.4,0,1.4,-4.4,7.4).s().p("AgRhAQgQgTgRgJQBZAQAHA3IABABIAEBxQgfhuglgvgAgahOQABADAEACIAEAEIAHAIQAbAhAVA9IAEALQACAHAAgLIAAgOIgCgkIAAgBQACgug8gTIgBAAIgJgDQAAAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAg");
	this.shape_11.setTransform(16.3,-53.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],18,-20,0,18,-20,7.4).s().p("AkxDtQAAgJA2gZIABACIgBAIIgNAHIgKAFQgUAMgIAAIgDAAgAELDWIgMgIIgCAAIgFgHQABgFAdALQAcALAAAJIgDAAQgOAAgWgLgADQhwIgEgLQgVg+gcghIgHgIIgEgDQgEgDgBgDQgCgCALAEIABAAQA9ATgCAvIAAABIACAlIAAANQAAAHgBAAIgBgDg");
	this.shape_12.setTransform(-0.3,-37.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFDF0D").s().p("AiCHnIgDgCQgOgFAAgPIgBgDIABAAQADiegbjDIAAgCIAAgBQgDgQAJgHIAAAAIAFgDIABgBIABAAQCkg2CnAyIABABQAGAGgDAXQgMCtAHC1IAAAAQgBAPgLAHIgDACQhOARhNAAQhCAAhCgNgAgPA+Qg2ADgpAIQgrAKgIALQClgqCoAgQgbgPg3gFQgegDgiAAIgpABgAhannIACgBIABAAQAGgDAFAAQAbgGAbgCIAhAAQAbAAAaAFIAKADIAEAAIACACQA6AmAwEmQi7hMi4BUQAmkuA5gkgAgTjSQgeABgdAHQgqALgpAVQCfg/ChA4QgngQgmgJQgegHgdgBIgWgBIgUABg");
	this.shape_13.setTransform(-0.6,-13.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFC204").s().p("Ag6J+QgggEgbgGQgngLgUgNIgFgFQgMgOgDgbIgBgBQgbi+gNi1QgGhTgEhSQgEhcAAhaIABgBIABgBIAAAAQAEgIAAgGIAAgBIAAgBIAIAFQABBqADBZIADBAQAGB9AIBoIABAEIAAABIAAABIABABIAAABIAAABIAHBEIAAAGIAEAVQARBoAbAXIAAAAQASAOATAEIAEAAIABAAQAcAEAcACQAdABAcAAIAjAAQAcgBAdgDIACAAIAqgFIAHgCIAIgBIABAAIACAAIABAAQAUgFARgOIAAgBQAagXANhoIACgNIAAgFQADgiADgqIAAgBIAAAAIAAgBIAAgCIABgBIAAgDIAAgEQAGhqABh6IAAhBQAAhZgEhrIAIgIIACAEIgBACIAAAAIAAAAIABgCQAEgNANgBIAGABIAWARQAWAQgKATQAAgJgcgLQgdgLgBAFIAFAHIACAAQADBagBBaQAABSgEBTQgHC1gUC/QgCAdgOAQIABgFQgYAXg4AOQgUAFgWAEQgaAEgdABQgOABgPAAIgGAAQgeAAgbgDgAD4ivIAAgLIAAALgAkrioIAAAAQALgNATgMQALgDAMAQIABABIAAABIAAABQAAAGgEAIIAAAAIAAgDQAAAAAAgBQAAAAAAgBQAAAAAAAAQgBgBAAAAIgBgCQg2AZAAAJQgJgRAPgOgAjkirIgHgfIgBgCQgFgMgEANIAAAVIgQgQQAFjOATiRIAAgDQAFgbANgNIABgBQASgTA3gQIAGgCQAUgDAXgCQgRAHgPAJIgCABIgMACQhYASAFA4IAAACIAAB5QACgOADgOQAVhfAkgtQAMgQAOgJIAIgFIACgBQAKgGALgCIADAAIAYgGQAbgFAbgCQARgCAQABQAaABAbAEQATADAUAGIACAAIANADQARAIAQAUQAmAuAfBvIAHCoIAAAAIgCgEQgUgKgTgHQgwkng6gmIgCgBIgEgBIgKgCQgagFgbgBIghABQgbABgbAGQgFABgGACIgBAAIgCACQg5AkgmEtQgSAIgSAJIAAABIgBABIgBABIABibIgBCbIgCACIAAABQgBAEABADIABADIACAIIgBBRg");
	this.shape_14.setTransform(-0.3,-0.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3C4A58").s().p("AhxJTIAAgBIgCgBQgSgDgSgQIgBgBQgagegLhPIABgDQgDgYAaAAIABAAQCmAOCwgSIACAAQANAAACALIAAACIABAEQgBBTggAhIgCACQgRARgUAGIgDAAQhDALg/AAQg1AAgzgHgAjKGKIgCAAQgHgCgBgMIAAgCIAAgBQgTjMgEkdIABgGIACAKIAaBwIAAADQAtEFgeB0QgFAJgFABgADTGCIgBgBQgDgBgGgHIgBgCQghhyAgkXIAVhlIAFgHIABAEIAAABQAGEbgLDNIAAACQgCAPgGACIgCAAgAkGDSIgCgEIgHgHIgCgEQgBgDAAgIIgBgsQAAgHADgFIADgFIADgEIAEgDQAIABgBAKQgIAEgBAFIABAtQAAAJADADIAIAIQgCAHgEABIgDABIgBAAgAEFDIQgEgBgCgGIAHgJQADgEABgIIgCgtQAAgFgJgEQgBgKAIgBIADADIADAEIAEAEQADAFAAAHIABAsIAAALIgCAEIgGAIIgDADIgCAAIgCAAgAi6gGIgti3IgCgIIgBgDQgBgEABgDIAAgBIACgDIABgBIABgBIAAAAQASgKASgHQC4hUC7BMQATAIAUAJIACAEIAAAAIAAABIACACIAAAFIgBAFIAAABIgBAFIgnC6IgBACQgDAGgHgBIgCABQAAgBgBAAIgBAAQimgyilA2IgBAAIgBABIgFABIgBAAIgBAAQgHAAgDgHgACqgdIABgBIADgRgAiGo1QAPgKARgHQAlgOA1gFQA1ABAqAQQANAEANAKIgNgDIgCAAQgUgFgTgEQgbgEgagBQgQAAgRACQgbABgbAGIgYAFIgDABQgLACgKAFg");
	this.shape_15.setTransform(0.3,-5.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFA300").s().p("AhCJXQgcgCgagEIgBAAIgFAAQgTgFgRgMIgBgBQgYgUgXhjIgEgVIABgGIgHhEQAEARAMAAIABAAIABAAIAAAAQAFAAAFgKQAehzgtkFIAAgDIgahxIAChRIAsC3QAEAKAIgBIAAAAQgJAIADAOIAAADIAAAAQAbDDgDCfIAAAAIAAACQAAAPAOAFIAEADQCNAcCSghIACgBQALgIABgPIAAAAQgGi1ALitQAEgWgGgGIAAAAQAAgBAAABIADAAQAGAAADgGIABgCIAoi6IABgFIAAAAIABgGIgBgFIgBgCIAAgBIgHinIgFhyIgBgBQgHg3hagQQgMgKgNgEIAlAEIAFABQA4APATASIABAAQANAMAGAbQAYCSAJDTIgOALIABABIgIAIIgHAIIgBgEIgEAHIgWBlQggEXAhByIABACQAGAHAEABIABABIABAAQAMgBAEgQIAAACQgCAqgEAiIAAAFIgBANQgUBkgXAVIgBABQgQALgRAFIgCABIgBAAIg5AIIgCAAQgfADgbABIgiABQgcAAgcgCgAimGmQgagBADAZIgBADQAMBPAZAdIABABQASAQASADIACABIABABQBxAQB4gUIADAAQAUgFARgRIACgDQAgghABhTIgBgEIAAgBQgCgMgNAAIgCAAQiwATimgPIgBABgADdiPIgDhHgAjviFIgFgDIgIgGIAAgDIAAgUQAEgOAFANIABACIAHAeIgBAHg");
	this.shape_16.setTransform(0.4,-3.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellowcar, new cjs.Rectangle(-32.2,-65.6,63.9,131.6), null);


(lib.yellowcar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#3A2201").ss(1,1,1,3,true).p("ADokJQABABABAAQCXCMBpCGADokJQgMgMgOgMQgWgWgXgUAFcgUQAFgIAJAGIC1CKQABABABABQACABABABQABABAAABQACABABABIAAABQAGAHACAHQAAADAAADQAJBJgiBQQgiBShEBFQhFBEhPAhQhDAag8gEQAAAAgBAAQgIgCgIgEQgCgBgCgCQgGgFgFgHIiTi+QgBgBAAgCQgCgFAGgEIAAAAQD0h2B6j7gADAjWIgDgEQiMiMh1iTQgFgGgFgHQgIgKgIgKQARALASANQAOAKAOAKQAnAeAsAlQA0ArA5A1AFpATQAFgHAHAEIB2BZIAhAZIABAAQACACABABQAIAHACAJQAAACAAADQAEA/ggBGQghBJg+A+Qg/A+hFAgQg8AagzgBQAAAAgBgBQgKgBgHgFQgEgEgDgEQgBgBgBgCIgdgmIhbh3QgBgCgBgBQAAgFAFgEIAAABQDZhxB0jfgAH5A1QAAgBgBgBQjHiXhxhyALMHHQAKhdhPiAQhPiAgBgBQgBgDgDgIQAAAAgBgCIAAABAIlB6QACABABACAITBCQAFAHAGAHQgTgOgSgNAITBCQgUgcgVgcAjhrDQABABABAAQgBgBgBAAQgZgGgZACQheAEhpBgIgBABQhLBGhEBLQgCACgDADQhPBagMBTQgCALAAAMQgBAZAGAYQAAABAAAAQAAABABABAjfrCQgBAAgBgBIAAAAAmFqZIgBABQgyAchuByQhyB1gWAzQAAABAAABIgSAvAptlgQAAgBAAAAQBWikCrhhQABgBACgBQBagtBRAmArBkxIABAAArLkIQAAgLAEgMQABgEABgEQACgFACgFApvlcIABgBQAAgBABgCIAAAAIAAAAAnwgXQgLgOgJgNAoIhFQAHAEAGAGQCUByCPCLIADACQByBtCYDCQACABABACQAPATAQAUQgJgGgIgFQgBgBgBAAQgagSgbgUQiHhniOiVQAAgBgBgBArHjWIAAAAQABABAAABArGjUIAAAAQgBgBAAgBIAAgBArGjUQAQAuAnAqAlKCnQg2g5gsgzAoIhFQgLgJgKgIQAMASANASAnwgXQAeAmAmAsAlKCnQAVAWAWAXQAMANAMAMAlAq5QghAKgkAWApzi0QgmhPAqhZABfIwQABABACAAQAKAEAEACQABABCBBNQCBBNBdgL");
	this.shape.setTransform(73.6,73);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EFE0A9").s().p("AC0i2QhtDsj6CBQDpiQB+jdg");
	this.shape_1.setTransform(89.5,88.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#EECA5C","#EBD98F"],[0,1],-0.5,-2.3,-3.5,4.3).s().p("Ah/geQAsAMAcgJQBVBOBihAQgsAsg6AAQhCAAhXg9g");
	this.shape_2.setTransform(114.4,139.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#EBD78A","#F7AE28"],[0,1],3.8,7.7,0,3.8,7.7,7.2).s().p("AiLhhQClCBByAgQgoAIgKAaQh+g4hniLg");
	this.shape_3.setTransform(93.8,138.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#FCF4E0","#F1C775","#E49807","#AE6904"],[0,0.745,0.898,1],0,0.9,0,0,0.9,6.1).s().p("AgjAcQgGgBgFgDQgIgFgCgHQgCgMAPgKQAOgMAYgFQAMgDAMABQAIAAAIACIAEABQAPAFACAKQACAIgHAHIgHAHIgEADQgHAFgJADIgGACIgIADIgEABQgLACgLAAQgKAAgJgCg");
	this.shape_4.setTransform(110.6,148.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#E7C954","#ECDA93"],[0,1],-9.8,7.2,6.1,-5.9).s().p("AhnAaQBGhQAXh1QCyDjhlB0QAThLi9hHg");
	this.shape_5.setTransform(133.2,110.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#EBD78A","#F7AE28"],[0,1],6.3,2.5,0,6.3,2.5,5.9).s().p("AhkiKQCPBmA5B7QgaAMgIAnQgihwiEikg");
	this.shape_6.setTransform(139.3,92.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FCF4E0","#F1C775","#E49807","#AE6904"],[0,0.745,0.898,1],0.8,0.3,0,0.8,0.3,5.9).s().p("AgKA4QgKgCgFgOIgCgEQgCgIAAgJQAAgLACgNQAFgXALgPQALgPALACQAIACAEAIQADAEACAHQAEASgEAWIgBAEIgDAJIgBAGQgEAIgFAIIgCAEIgIAIQgEAFgGAAIgEgBg");
	this.shape_7.setTransform(149.2,109.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#D27E02","#F4AE0B"],[0,1],7.9,8.4,0,7.9,8.4,37.7).s().p("AjyDzQgKgEADgRIAQAEQAFADAGABQASAFAYgFIAEgBIAIgCQBngUB2h0IACgBIAQgRIACgCQCCiCAVhvIABgJIABgEQAFgXgFgSQgBgHgDgEIgEgQQARgEADAKQAXBAgVBBQgOAvgoA0IgCACQhVBvhwBaQg0ApgwAQQggALghAAQggAAgggLg");
	this.shape_8.setTransform(129.3,128);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#F7ECC4","#C8C8D9"],[0,1],0.3,0.3,0,0.3,0.3,4).s().p("AgkAEQANgGAXgFQAYgFAFACIAIANQgdAIgQAAQgSAAgKgHg");
	this.shape_9.setTransform(72.9,124.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FACF20","#FBA913"],[0,1],-2.9,0,0,-2.9,0,7.7).s().p("AABAQQgeAAgFgKIgIgNIgDgHIABAAQAJAHASAAQARgBAdgHIARAeQgKABgRAAIgSAAg");
	this.shape_10.setTransform(73.7,126.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#F0C44F","#D27E02"],[0,1],2.1,0.9,0,2.1,0.9,13.1).s().p("ABpCKQhHgShShJIgEgDQg+g7gphDIgQggIgIgOIgHgOIA1AmIABABQAMATAOAMQgPgHgOgIQDGCUB8AbQgMgBgNADQgYAFgOAMQgPAMACALQACAIAIAFIgQgFgABaB4QAKgaAogIQhyggiliBQBnCLB+A4g");
	this.shape_11.setTransform(93.8,136.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#E9DA7E","#CAA202"],[0,1],-1.4,1.2,0,-1.4,1.2,5.5).s().p("AgCAaQgPgKgLgUQgMgUAKgKIAqArIAYAaQgHgBgIABIgBAAQgIAAgOgJg");
	this.shape_12.setTransform(43.6,93.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#EFCE4B","#FDFDA4"],[0,1],2.9,-2.1,-2.6,1.9).s().p("AhLhJQAdgDARgZQAeB3BLBUQhehIg5hng");
	this.shape_13.setTransform(10.6,58.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#FBCF1D","#CA7902"],[0,1],6.4,6.2,0,6.4,6.2,19.3).s().p("AASA/IgDgDQg8g7gogwQgngqgQguIAAAAQAtBJA3A3IASARIAKAIQAsAnApgEQAeAnAmAsQg4gHhDhCg");
	this.shape_14.setTransform(16.6,65.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#6DB5F2","#217EBC"],[0,1],18.2,-28.8,0,18.2,-28.8,46.2).s().p("AAAAAIAAAAIAAABg");
	this.shape_15.setTransform(2.4,51.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#956140","#3C291A"],[0,0.988],2.6,2,0,2.6,2,4.4).s().p("AEpEvIgCgBIg0gmQiJhniMiVIgCgCIgYgYIgrgtQg1g5gsgzQgmgsgegnIgVgcIgYgjIAVAQIANAKQCUB0CNCLIAEACQByBsCZDCIADADIAeAnIgRgLg");
	this.shape_16.setTransform(50.8,95.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#FDA624","#FFFFFF"],[0.58,1],1.1,-3.7,0,1.1,-3.7,27.4).s().p("AiMBdQAVgyAjgtQAzhFBKguQBKguAtABQjiBKhdD7QgBgTAUgzg");
	this.shape_17.setTransform(28.8,30.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#5B3702").s().p("AgBAMIABgXIACAAQgCAMAAALg");
	this.shape_18.setTransform(2.1,45.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FA761F").s().p("AgXAkQgCgDgBgHIAAgIIADgHIAEgJQAJgTALgLQAMgMAHAEQAIADgBARIgBACQgEAPgFAJQgEAJgHAIQgIAIgJACIgFABQgEAAgDgCg");
	this.shape_19.setTransform(5.4,42.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.rf(["#A66504","#744603"],[0,1],6.4,-17.2,0,6.4,-17.2,38.1).s().p("Ah9DvIhKhJQAglXDVg/ICXCUQAIAHgLAJQisB4iHDBQgEAEgDAAIgFgCg");
	this.shape_20.setTransform(43.6,40.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#EDD768").s().p("AhXEQIixivQgNgKAEgMIAGgRQBdj7DjhKIARgFIABgBQAQgEAKALICsCpQAMAMgMAJQjJB7iMDgQgEAEgEAAQgDAAgEgDgABHkKIgBAAIgHADQjrBKheEIQgEALAMALICpCmQAIAHAHgHQCJjbDFh6QALgJgKgLIiliiQgHgIgKAAIgIACg");
	this.shape_21.setTransform(39.6,41.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.lf(["#AF6A03","#291801"],[0,1],11.5,4.5,-21.1,-11.2).s().p("AhZEAIifidQgMgKAEgLQBhkDDphLIABAAQAOgFAKALICbCYQAKAKgMAJQi+B5iJDVQgEAEgEAAQgDAAgDgDgAidCgIBKBJQAGAEAGgGQCHjBCsh4QALgJgIgHIiXiUQjVA/ggFXg");
	this.shape_22.setTransform(39.4,40.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.lf(["#EFCE4B","#FDFDA4"],[0,1],-0.2,1,1.7,-2.7).s().p("AhmgbQAagRACgeQBoA4BJBdQhVhKh4gcg");
	this.shape_23.setTransform(57.7,10.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#F57205").s().p("AgkAVQgDgHALgNQALgLATgKIAJgDIAGgDQAFgBAEABQAHAAACADQAEAEgCAIQgCAJgIAHQgIAIgJAFQgIAEgPAFIgDAAIgFAAQgMAAgDgGg");
	this.shape_24.setTransform(41.8,5.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#754602").s().p("AghFGIipimQgMgLAEgLQBdkIDrhKIAIgDIABAAQAPgEAKAKICkCiQALALgMAJQjEB6iKDbQgEAEgDAAQgEAAgDgEgACBjDIgCABQjpBKhgEEQgFALAMAJICgCdQAHAGAHgHQCIjVC/h4QALgJgKgKIibiZQgHgHgIAAIgIABgAk4CNQgJgEABgTQABgLADgNIgDAAQAMhSBQhaIAFgFQBDhKBLhGIACgBQBnhhBegEIABABQATgBAEAJQAEAJgNAOQgNAOgWALQgVAJgSACIgDAAQgTABgEgIQgEgJANgOQAMgOAUgKQgiALgjAWIgCABQgxAbhuByQhxB1gWAzIgBABIgSAvQALgWANgOQAOgNAJAEQAJAFgBASIAAADQgBASgKAVQgKAXgOANQgLAKgIAAIgEgBgAkdAvQgMALgJAUIgEAJIgCAHQgEAKgBAKQAAARAHADQAHAEAMgMQAMgLAKgUQAIgSACgQIAAgCQABgRgHgDIgEgBQgGAAgKAJgAk5BXIgDAHIADgHIAEgLIAAAAIAAAAIgEALgAB0lEQgKABgLADIgGADIgJADQgUAKgLAMQgLANADAHQAEAIAQgCIADAAQAPgCATgJQATgKALgMQAMgMgEgHQgDgGgMAAIgFAAg");
	this.shape_25.setTransform(34,34.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FDFDA4").s().p("AjbDlQgIgDABgRQABgKADgKIAAAIQABAHACADQAEADAIgCQAJgCAJgIQAHgIAEgJQAFgJAEgQQgBAQgJASQgJAUgMALQgKAJgGAAIgDgBgACzi0QAJgFAHgIQAJgIACgJQACgIgEgEQgDgDgGAAQgFgBgEABQAKgDAKgBQARgCAEAIQADAHgLAMQgMAMgTAKQgSAJgQACQAQgFAJgEg");
	this.shape_26.setTransform(25,25.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.rf(["#FDA008","#FBCE1C"],[0,1],-12,-11.6,0,-12,-11.6,43.7).s().p("AkoDJIgBgIQgJg7AghEQAfhBAsgrIABgCIAKgKQA6hGBLhAIAHgIIAGgGQAqgrA/geQBDgiA7AJIAJABQAvAHA3BDQgrglgtgRIgDgBQgYgFgaABQheAEhnBhIgCABQhLBGhDBKIgFAFQhQBagMBSIgCAYQAAAZAFAYIAAAAIABADIAAAAQARAtAmArQhEg3gIgvg");
	this.shape_27.setTransform(30,30.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.rf(["#FCCE16","#E1C248"],[0,1],-15.5,-13.5,0,-15.5,-13.5,46.2).s().p("AjQE1IgJgIIgTgSQg3g3gshJIgBgCIgBgBQgFgYAAgZIABAAQgBAUAJAEQAJAEAOgOQAOgMAKgXQAKgWABgRIAAgDQABgTgJgEQgJgEgOANQgNANgLAXIASgvIABgCQAWgyBxh1QBuhyAygcIACgBQAigWAigKQgUAKgMANQgNAOAEAJQAEAJATgCIADAAQASgBAVgKQAWgLANgNQANgPgEgJQgEgJgTACIgBgBQAagCAYAGIAAAAIACABQBKAsA4A3IASARIAJAKQAnAtgEApIgbgUQhOhRg1gcQhSgmhaAtIgDACQiqBhhVCjIAAABIgBAAIAAAAIgBADIAAABQgrBZAmBPQAdA1BSBNIAVAbIgIABQglAAgqgkgAlLCRQA6BoBfBIQhLhUgfh4QgSAZgdADgABwkeQB5AdBVBKQhJhehpg4QgCAegaARg");
	this.shape_28.setTransform(36.1,36.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#CA7902").s().p("AkSCmQgmhQAqhYQgiBQAmBQQAaAxBJBFIgVgQIAZAjQhThMgcg1gAECjAIAQAVQhGhIgygaQhRglhRAlQBZguBRAmQA2AcBNBSIgjgZg");
	this.shape_29.setTransform(38.3,38.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.rf(["#FBCF1D","#CA7902"],[0,1],8.5,7.8,0,8.5,7.8,20.1).s().p("AA2BIQAEgpgogrIgJgKIgRgRQg4g3hJgsIAAAAQAuAQAqAmQAxAnA8A8IADADQBDBBAIA5Qgsgmgogeg");
	this.shape_30.setTransform(65,16.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FCF8E9").s().p("Ai+DhIgHgBQgGgCgIgHQAHAFAJABIABAAQA7AFBDgbQBPghBEhEQBFhFAihRQAihPgJhJIgBgGQgBgIgGgHQAJAJACAKIABAGQAIBJghBPQgiBRhFBFQhEBEhPAhQg5AXg0AAIgRgBg");
	this.shape_31.setTransform(108.9,108.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.rf(["#010D31","#022770"],[0,1],-12.8,30.3,0,-12.8,30.3,69.8).s().p("AABABIgBgBIABACg");
	this.shape_32.setTransform(128.7,85.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#AE6904").s().p("AgiFmIAGgCQAIgEAIgFQBTgaBlheIAJgIIADgDIAPgOIABgCQB2h6AcheQAGgIADgIIABgHIACABQgVBtiCCEIgCACIgQAQIgCABQh3B1hmATgAgBFRQAGgHgCgJQgCgJgOgFIgEgBQgIgDgIAAQh8gajIiVQAOAHAPAHIADABIAOAFICDBPQCABNBcgMIgrAKIAAAAQADADAEAGQAFAFgBAIQgBAHgGAGIgBABIgBgBgADgi3IhQiBIgFgLIgBgCQgIgSgIgOQCYDEAcB8QABAJACAHIABAFQAFAOAKACQAIABAHgGQABABAAAAQAAABAAAAQAAABgBAAQAAAAAAAAQgGAGgHAAQgHABgGgEQgFgDgEgEIgJArQALhdhPh/g");
	this.shape_33.setTransform(116,114.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#EAD976","#FF9900"],[0,1],5.6,2.5,0,5.6,2.5,51.8).s().p("AABFiIAHgIQABABABAAQAAAAABAAQAAAAAAgBQABAAAAAAQAGgGAAgHQABgIgEgFQgEgGgEgDIAAAAIAsgKQhdAMiAhNIiChPIgPgFIgCgBQgOgLgMgTIASALIgfgnQAmAoAqAVIAFACQAIAIAHABIAGABQA7AFBDgbQBQggBEhFQBEhEAihRQAhhQgIhJIAAgGQgDgKgJgIIgDgDIgCgCIgCgDQgJgagfgnIAkAcIgKgPQAMAFASAVIABACIAAgBIABACIAEALIBQCBQBPB/gKBdIAJgsQAEAEAFAEQAGAEAGgBQAHAAAHgHQAAAAAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBIAIgHIACgDQgcBeh2B6IgBACIgPAOIgEADIgJAIQhlBehSAaIACgCgAB7DzQhiBAhVhOQgcAJgsgMQCgBzBfhigABPgFQC+BFgTBMQBlh0izjjQgWB1hHBRg");
	this.shape_34.setTransform(114.9,114);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.lf(["#9D6004","#5B3702"],[0,1],23.2,-15.9,-15.3,23.1).s().p("AieDWIgBgBQgJAAgHgFIgCgDIgdgnQDth6CJkBIAhAZIABABQADAEABAGIAAAFQAFA/ghBGQghBIg+A+Qg+A+hFAgQg4AZgxAAIgFAAg");
	this.shape_35.setTransform(105.9,104.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.lf(["#6B4103","#291801"],[0,1],21.3,-17.6,-17.2,21.4).s().p("AipDNIgBAAQgKgBgIgGIgHgHQAIAFAIAAIABABQAzABA8gaQBEggA/g+QA+g+AhhIQAghGgEg/IAAgFQgCgGgDgEIADACQAIAHACAJIAAAFQAFA/ghBHQghBHg+A/Qg+A+hFAfQg4AZgxAAIgFAAg");
	this.shape_36.setTransform(107.8,106.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#F0C44F","#D27E02"],[0,1],5.2,6.5,0,5.2,6.5,10.1).s().p("AhYiGQAIAOAIASIABABIgCgCQgRgVgMgEIgqg4IAPAGIAOAHIAgARQBEApA7A7IAEAFQBJBQAUBIIAEAQQgFgIgHgCQgMgCgLAPQgLAPgFAXQgDAOAAALQgch7iXjEgABZCKQAHgoAbgLQg6h8iOhlQCECjAiBxg");
	this.shape_37.setTransform(137,92.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.rf(["#F7ECC4","#C8C8D9"],[0,1],-0.2,1.6,0,-0.2,1.6,5.2).s().p("AgJAeQgCgGAEgXQAFgXAGgNQAHAIAAATQAAARgHAcIgNgHg");
	this.shape_38.setTransform(125,71.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.rf(["#FACF20","#FBA913"],[0,1],-0.1,-3.7,0,-0.1,-3.7,8.4).s().p("AgPAcQAHgbAAgSQAAgSgHgJIAAgBIAHADIAOAHQAJAGABAdQABAegBAPIgfgRg");
	this.shape_39.setTransform(127,72.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FBB751","#242B61"],[0,1],4.3,-8.2,-33.2,29.8).s().p("AgQgLQACg+gWgpQBLAyADAEQARAQgfA7QgaAzgXAYIgaAZQAdg6AChEg");
	this.shape_40.setTransform(114.4,87.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.lf(["#E08805","#6B4103"],[0,1],19.4,-18.4,-18.1,19.6).s().p("AjnB0IgCgCQAAgFAFgEIAAAAQDZhvBzjgQAFgHAHAFIB2BYQiJEBjtB6gACHhxQgCBFgdA5IAagYQAXgYAbgzQAeg8gQgQQgEgEhMgzQAXApgCA/g");
	this.shape_41.setTransform(99.3,98);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.rf(["#3E2501","#955A04"],[0,1],15.3,15.3,0,15.3,15.3,69.8).s().p("AhuEqIgBAAQgJgBgHgFIgFgDQgGgFgEgGIiUi+IgCgDQgBgGAFgEIABAAQD0h1B5j8QAGgIAJAGIC1CMIABABIADADIACABIACADIABAAQAGAHABAIIABAGQAJBJgiBOQgiBShFBFQhEBEhQAhQg4AXg0AAIgRgBgAj1BaIACACIBaB3IAdAnIACADIAHAHQAIAGAKABIABAAQAzACA6gbQBGgfA+g+QA+g/AhhIQAhhGgFg/IAAgFQgCgJgIgHIgDgCIAAgBIgigZIh2hYQgHgFgFAHQhzDgjZBvIAAAAQgFAEABAFg");
	this.shape_42.setTransform(100.5,100.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.lf(["#E9DB82","#FDCE13"],[0,0.988],16.9,17.5,-18.9,-19.5).s().p("AgnFBQieiTh0icIgBgBQgWgfANgfIADgHQCDjJCRhHQAYgOAYAOQB/BXCvCzIAAABQAgAhgNAdIgDAGIgBACIgMATQhbCOjOCWIgHAEQgIAEgIAAQgOAAgOgLg");
	this.shape_43.setTransform(70.3,69.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.lf(["#F4D344","#FF9900"],[0,1],35.7,33.9,-42.2,-42.4).s().p("ABoIaIgDgDQiYjChyhtIgEgCQiOiLiUhyIgNgKQhJhGgbgxQgmhPAjhRIAAgBIABgDIAAAAIABAAIAAgBQBVikCrhhIADgCQBSglBSAmQAxAaBHBHIAKANQB0CTCNCMIADAEQBwByDHCXIACACQAfAmAIAbIgBgCIi1iLQgJgGgGAIQh6D8j0B2IgBAAQgFAEABAFIACADICUC+QAEAHAGAFQgqgVgmgogAAbF4QD6iBBtjtQh9DejqCQgAgelBQiRBHiCDJIgDAHQgOAfAWAfIACABQBzCcCfCTQAVARAWgKIAHgEQDPiWBbiOIAMgTIABgCIADgGQAMgdggghIAAgBQiuizh/hXQgNgHgLAAQgMAAgNAHgAlmn5QhLAtgzBFQgjAugVAyQgUAzABAUIgGARQgEALANALICxCvQAIAGAHgHQCMjhDJh7QAMgKgLgLIisiqQgLgKgPAEIgBAAIgSAFIgBAAQgtAAhJAug");
	this.shape_44.setTransform(68.8,69.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.rf(["#E9DA7E","#CAA202"],[0,1],0.8,-1.7,0,0.8,-1.7,5.9).s().p("AAJAMIgsgqQAKgKAVAMQAUALAKAPQAKAPgBAHQgBAIABAHIgagXg");
	this.shape_45.setTransform(93.3,42.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#956140","#3C291A"],[0,0.988],1.9,1.7,0,1.9,1.7,3.9).s().p("AEaEaIgCgBQjHiYhvhzIgDgDQiNiLh1iTIgKgNIgQgVIAjAZIAcATQAnAeAtAmQAzAqA6A1IAtArIAZAYIABABQCYCLBpCGIApA4IAKAPIgkgcg");
	this.shape_46.setTransform(95.9,50.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellowcar_1, new cjs.Rectangle(0,0,154.4,153.4), null);


(lib.redcar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,3.4).ss(1,1,1,3,true).p("AgOghIAAgBQAwAvgTAVAgOghQgSARAuAzQABAAAAgB");
	this.shape_84.setTransform(117.6,49.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#403D50").s().p("AgLgiIAcBEIAAABQgvgzATgSg");
	this.shape_85.setTransform(117.4,49.6);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#637778").s().p("AgRghIAAAAQAwAugTAVg");
	this.shape_86.setTransform(117.9,49.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0,0,0,0,0,3.4).ss(1,1,1,3,true).p("AAkANIAAgBQgygsgUAVAAkANQgRATg2gqQABAAAAgB");
	this.shape_87.setTransform(99.7,32.8);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#637778").s().p("AgigIQATgVAzAsIAAABg");
	this.shape_88.setTransform(99.8,32.5);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#403D50").s().p("AgjgOIABAAIBGAXQgFAGgJAAQgUAAglgdg");
	this.shape_89.setTransform(99.7,33.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().rs(["#700000","#4B0101"],[0,0.988],9.8,-0.8,0,9.8,-0.8,11.5).ss(1,1,1,3,true).p("AhFBIICLiP");
	this.shape_90.setTransform(84.7,24);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12.7,8.9,0,-12.7,8.9,5.2).ss(1,1,1,3,true).p("AAbglQAIgKAIAGIAAAGAAbgUQAEgFgEgMAgaAeQAGAAAIgIIAngqAgqAsQgCgEAEgIIAOgC");
	this.shape_91.setTransform(91,110.3);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-16.7,-17.6,0,-16.7,-17.6,29.3).ss(1,1,1,3,true).p("AAZgEQgZABgZgHQAWAUAdAA");
	this.shape_92.setTransform(38.2,153.6);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-5.8,-4.2,0,-5.8,-4.2,12.6).ss(1,1,1,3,true).p("AluFSQAbgDAdgZQDAihCqijQBPhLBKhNQBUhVBOhX");
	this.shape_93.setTransform(77.5,117.9);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().rs(["#700000","#4B0101"],[0,0.988],10.5,-13.3,0,10.5,-13.3,5).ss(1,1,1,3,true).p("ADFi2QgOAPgOAPQj4D7h0BTQAAAAgBAB");
	this.shape_94.setTransform(67,133);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().rs(["#700000","#4B0101"],[0,0.988],17.9,-18.8,0,17.9,-18.8,6.6).ss(1,1,1,3,true).p("AAegMIAAAAQgVAZgdADQgFABgGAAAAegMIAAAAAAggQQgBACgBAC");
	this.shape_95.setTransform(44,153);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().rs(["#700000","#4B0101"],[0,0.988],31.6,-33.2,0,31.6,-33.2,6.2).ss(1,1,1,3,true).p("AAegGQABgBACgBAAdgGIABAAAggAJQAggBAdgO");
	this.shape_96.setTransform(44,152.2);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().rs(["#700000","#4B0101"],[0,0.988],3.7,-3.7,0,3.7,-3.7,28.2).ss(1,1,1,3,true).p("AAPATQAbAYAeAYAgSgMQgbgbgagb");
	this.shape_97.setTransform(24.7,131.3);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-5.9,-17.9,0,-5.9,-17.9,28.2).ss(1,1,1,3,true).p("AgJgGIAHAFQADACAFADQAAAAABABQABABABAB");
	this.shape_98.setTransform(37.5,142.2);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4,-4,0,4,-4,28.2).ss(1,1,1,3,true).p("AARAQQgRgQgQgP");
	this.shape_99.setTransform(24.5,131.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().rs(["#700000","#4B0101"],[0,0.988],39.2,-13.9,0,39.2,-13.9,30.7).ss(1,1,1,3,true).p("AAiCIQgZgcgWgcQAAgBgBgBQgCgCgBgCQgOgVgFgdQAAAAAAgBQgFgsBPhy");
	this.shape_100.setTransform(14.1,111.1);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().rs(["#700000","#4B0101"],[0,0.988],18.4,15.7,0,18.4,15.7,28.9).ss(1,1,1,3,true).p("AAMAeQgHgWgBgUQgBgCABAAAgLgdQgCAiAZAZ");
	this.shape_101.setTransform(1.7,119.1);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().rs(["#700000","#4B0101"],[0,0.988],17.6,-18.6,0,17.6,-18.6,6.6).ss(1,1,1,3,true).p("AgNAbQAAABAAABAgOAaIABABAAPgcQgbAYgCAe");
	this.shape_102.setTransform(1.9,113.2);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().rs(["#700000","#4B0101"],[0,0.988],31.2,-34.2,0,31.2,-34.2,6.2).ss(1,1,1,3,true).p("AAHgjQgBACgBACQgMAiACAh");
	this.shape_103.setTransform(2.7,114);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().rs(["#700000","#4B0101"],[0,0.988],11.8,-40,0,11.8,-40,30.7).ss(1,1,1,3,true).p("ACIgqQhrBWgsgCQgBAAgBAAQgcgCgXgNQAAgBgBgBAiFgUQgBgBgBgBAhYAMIgtgg");
	this.shape_104.setTransform(45.4,140.3);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.2,5.9,0,4.2,5.9,12.5).ss(1,1,1,3,true).p("AkgFPQCSjMCXi0QBGhUBHhQQBRhbBShVAkhFRIABgCAk4GGQABgaAWgb");
	this.shape_105.setTransform(34.8,77.7);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-6.9,7.1,0,-6.9,7.1,29.8).ss(1,1,1,3,true).p("AidiUIABABQDKBiBwDF");
	this.shape_106.setTransform(80.5,71.7);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().rs(["#700000","#4B0101"],[0,0.988],28.1,2,0,28.1,2,29.8).ss(1,1,1,3,true).p("AADABQgDgBgDgBQAAAAgBAAAADABQAAABACAA");
	this.shape_107.setTransform(64.3,56.6);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-2.5,2.6,0,-2.5,2.6,25.2).ss(1,1,1,3,true).p("ABQDYQA8gDBMhNQABgCACgCQABgBABgBQAIgMgMgMQgBgBgBgBQi3iOiOilQgBAAgBgBQgYgXgTAZQgBABgCABQg9BVgEAzIABACQACAfAOAUIABACIAAABQBcB1CDBcIACACQAZAOAfgB");
	this.shape_108.setTransform(36.3,118.8);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().rs(["#700000","#4B0101"],[0,0.988],35.6,2.3,0,35.6,2.3,30.4).ss(1,1,1,3,true).p("AAEARQgBgBgBgBQAAAAgBgBQgJgLAKgT");
	this.shape_109.setTransform(25.2,89.3);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().rs(["#700000","#4B0101"],[0,0.988],14.2,26,0,14.2,26,29.9).ss(1,1,1,3,true).p("AjDD5QgMAEgGgDACIh5IgDAEQjCEdiGBRADWj7IhOCC");
	this.shape_110.setTransform(47,66);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().rs(["#700000","#4B0101"],[0,0.988],12.9,-11.9,0,12.9,-11.9,4.9).ss(1,1,1,3,true).p("ACpjQQgPAPgMAPQjrEIhKB6QAAABgBAA");
	this.shape_111.setTransform(20.3,89.4);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-8.1,13.3,0,-8.1,13.3,5.1).ss(1,1,1,3,true).p("AgUAPQgIAIAAAGIgBAOQgHAFgEgCAAggtIAHAAQAGAIgJAIQgMgDgFAEIgnAr");
	this.shape_112.setTransform(41.2,63.9);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f().rs(["#700000","#4B0101"],[0,0.988],9,-9.9,0,9,-9.9,40.4).ss(1,1,1,3,true).p("AkjjQQAAgCACgEQABgEAIgHIAngqQAGgHAHgCQAEgBADgBIAHAAAkkjDIABgNAElDJQAAAEgBAEQgCAHgFAHIgnAqQgIAIgDACQgEABgCABIgNACAElDDIAAAG");
	this.shape_113.setTransform(66,87.2);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.8,34.9,0,4.8,34.9,32).ss(1,1,1,3,true).p("AgkAmIBJhL");
	this.shape_114.setTransform(72.1,37.1);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.5,-0.1,0,0.5,-0.1,4.2).ss(1,1,1,3,true).p("AgMgYQATgCAeAHQANAHgDAYQgBABAAABQAAABAAAAAgNgYIABAAAAdAZIAAAAQAMgEAFgGAAhATQAAACgCACQgBABgBABAAiAQQAAACgBABAguAAQAIgIBIAYAguAAQAGgYAbAA");
	this.shape_115.setTransform(63.5,36.1);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().rs(["#700000","#4B0101"],[0,0.988],27.8,10.1,0,27.8,10.1,31.9).ss(1,1,1,3,true).p("AgCABQAAgBABgBQABgFAFgCQAAAAAAgBQACAAACAAAgIALIAGgK");
	this.shape_116.setTransform(76.8,32.3);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().rs(["#700000","#4B0101"],[0,0.988],25.2,12.5,0,25.2,12.5,29.8).ss(1,1,1,3,true).p("AgVgHQgCAAgBAAQgBAAgBAAAgVgHIAAAAAAbAIQgYgIgYgH");
	this.shape_117.setTransform(80.3,32);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-9.3,3.7,0,-9.3,3.7,9.2).ss(1,1,1,3,true).p("AAnhpQgGBOhHCE");
	this.shape_118.setTransform(139.1,44.7);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().rs(["#700000","#4B0101"],[0,0.988],4.8,-5,0,4.8,-5,29.8).ss(1,1,1,3,true).p("AixilQD5BZBrDy");
	this.shape_119.setTransform(100.9,49.4);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.1,-9.9,0,0.1,-9.9,11.5).ss(1,1,1,3,true).p("ABJhRIiRCj");
	this.shape_120.setTransform(128,63.5);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-26.9,-12.1,0,-26.9,-12.1,30).ss(1,1,1,3,true).p("ADrjpIhvBLQkcDphJCKQAAABAAABQgBAMACAFQAAABABAAQAAABABAA");
	this.shape_121.setTransform(88.5,104.4);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-35.8,-1.9,0,-35.8,-1.9,31.9).ss(1,1,1,3,true).p("AAfgiIg9BF");
	this.shape_122.setTransform(115.9,76.9);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.4,-1.2,0,-0.4,-1.2,3.8).ss(1,1,1,3,true).p("AjsDpQAGAEAQgMQACgCABgBQDEi1D7kRQABAAAAgB");
	this.shape_123.setTransform(89.1,104.6);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.2,-0.3,0,0.2,-0.3,4).ss(1,1,1,3,true).p("AgQgoQABgBABAAQAPgJANALQADADACADIAGAkQAGAjgbAIAgQgoIAAAAAgZgdQADgGAGgFAgZgdQAFgFAQAkQAQAkgIAI");
	this.shape_124.setTransform(116.9,85.8);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-26.3,27.8,0,-26.3,27.8,29.8).ss(1,1,1,3,true).p("AA1EHQABAAgBABQABABABABQAHAHAIgDQABgBACgBIDOiLAkQgnQgJgFAFgNIB8jV");
	this.shape_125.setTransform(91.1,60.4);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-14.2,-24.1,0,-14.2,-24.1,29.8).ss(1,1,1,3,true).p("AgJgbQAKAYAJAZQAAACgBAE");
	this.shape_126.setTransform(119.7,68.8);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12,-27.1,0,-12,-27.1,31.9).ss(1,1,1,3,true).p("AAIgHQAAACAAABQgCACgCACQgEADgBABQAAABgBAAIgFAD");
	this.shape_127.setTransform(119.8,72.6);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-2.1,8.8,0,-2.1,8.8,9.2).ss(1,1,1,3,true).p("AhoAzQAPgLAPgKQBqhGBJgK");
	this.shape_128.setTransform(102.3,11.7);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f().rs(["#700000","#4B0101"],[0,0.988],0.8,5.1,0,0.8,5.1,11.6).ss(1,1,1,3,true).p("Aj5CnQDAi9CVh4QACgBABgBQAdgWAXAAQABAAABABQAigCBDAi");
	this.shape_129.setTransform(93.4,17.7);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-10.9,13.3,0,-10.9,13.3,11.3).ss(1,1,1,3,true).p("AgYgpQAAgBABgBQAug5BiA+QAHAEAIAFAiHBHIBvhw");
	this.shape_130.setTransform(105.3,9.7);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f().rs(["#700000","#4B0101"],[0,0.988],3.6,16.6,0,3.6,16.6,24.7).ss(1,1,1,3,true).p("AgdAAQAagDAVADQAGAAAGAC");
	this.shape_131.setTransform(115.8,6.6);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-12.3,12.2,0,-12.3,12.2,10.6).ss(1,1,1,3,true).p("AApAWQABgBAAAAQAqg6hHhfAg5CGIBihw");
	this.shape_132.setTransform(141,41.9);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-3.3,-0.7,0,-3.3,-0.7,11.1).ss(1,1,1,3,true).p("AB+kDQAnBBABAiQAAABAAAAQACAXgUAgQhtCdi2DOIgWgB");
	this.shape_133.setTransform(132.4,55.2);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-16.4,-2.8,0,-16.4,-2.8,24.8).ss(1,1,1,3,true).p("AgCgbQAHAXgCAg");
	this.shape_134.setTransform(142.7,31.4);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f().rs(["#700000","#4B0101"],[0,0.988],-0.1,-0.3,0,-0.1,-0.3,20.4).ss(1,1,1,3,true).p("AhZhgIADADQAPAIAOAJQAeATAaAXQARAOAQARQAYAWAVAdQAPAUAOAXIACACQAFAHADAIAhzhrQANADANAI");
	this.shape_135.setTransform(130.8,17.8);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.rf(["#DA0E0D","#850700"],[0,0.988],2,1.4,0,2,1.4,6.4).s().p("AgqAqQgCgEAEgIIAOgCQAGAAAIgIIAngqQAEgFgEgMQAIgKAIAGIAAAGIAAAGIgBAIQgCAHgFAHIgmApQgIAIgDACIgGACIgNACIgFABIgEgFg");
	this.shape_136.setTransform(91,110.5);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.rf(["#FF9900","#CC2D17"],[0,0.988],0.1,0.8,0,0.1,0.8,4.4).s().p("Ag5gDQAaAFAZgBQAegBAegOIABAAIADgCIgCADIgBAAIAAABQgUAZgeADIgLABQgdAAgWgUg");
	this.shape_137.setTransform(41.5,153);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.lf(["#9AAEBA","#DBE3E8"],[0,0.988],3.2,-3.3,-3.5,3.7).s().p("AgQA2IA9hDQAbAXAeAYIg+BCQgcgUgcgagAhlggIA8hDIA0A2Ig9BDQgcgagXgcg");
	this.shape_138.setTransform(21.7,134.6);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.lf(["#710A0B","#C80000"],[0,0.988],-4.6,4.6,0,-0.2).s().p("AA7CDIgCgCQhmhPhVhfIgBAAQhKhVgBhAIAGAAQgCAjAZAYQgHgWgBgVIAAgCIAJgGQgCAiAiA3QAlA9BCA+QBCBBBBAkQBIAoAtgKIgGAEQgDADADALQgZABgagHQAWAVAdAAIAAAFQg9gChRg+g");
	this.shape_139.setTransform(20.7,135.6);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.rf(["#FF9900","#CC2D17"],[0,0.988],-0.6,-0.3,0,-0.6,-0.3,4.9).s().p("AgNAAIAAgBIgBgBQACgfAbgYIgCAEQgNAiACAhIAAABQABAWAHAVQgZgYACgig");
	this.shape_140.setTransform(1.9,116.2);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.rf(["#700000","#4B0101"],[0,0.988],-1.6,5.4,0,-1.6,5.4,35.7).s().p("AjpDKIAAgFIALgBQAfgEAUgZIAAgBIABAAIACgDIABgBQB0hSD4j8IAcgdIAEAEIAFgBIiGCLQhdCQhtApIhAA2QgcAWgkAAIgDAAg");
	this.shape_141.setTransform(64.2,135);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.lf(["#8499AC","#657A8D"],[0,0.988],-4.6,5.2,4.4,-4.6).s().p("AA9B/Qgtgdg5g0Qg5g0ghguQghguATgeQATgfA0AfQAzAfBCA9QBDA9AdAyQAdAygfAPQgKAFgNAAQgXAAgegSg");
	this.shape_142.setTransform(32.3,123.3);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.lf(["#788AA0","#486575"],[0,0.988],-7.4,8,3.7,-3.7).s().p("ABNDFQg0gJhfhWQhjhXgghCQgKg3BFhOQARgSAPAJQCJCdCuCHQAHAPgLAOIgDAEQhEA8gwAFgAiyhcQgTAeAhAvQAhAsA5A1QA6A1AsAcQAuAcAegPQAfgPgdgyQgdgxhDg+QhCg9gzgfQgagPgSAAQgRAAgKAPg");
	this.shape_143.setTransform(35.6,119.7);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#424056").s().p("AAUDLIgCgCQiDhchch1IAAgBIgBgCQgOgUgCgfIgBgCQAEgzA9hVIADgCQATgZAYAXIACABQCOClC3COIACACQAMAMgIAMIgCACIgDAEQhMBNg8ADIgEAAIgEAAQgdAAgXgNgAiVivQhFBOAKA3QAgBCBjBXQBfBWA0AJIABAAQAwgFBEg8IADgEQALgOgHgPQiuiHiJidQgFgDgFAAQgKAAgMAMg");
	this.shape_144.setTransform(36.3,118.8);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.rf(["#700000","#4B0101"],[0,0.988],-5,0.2,0,-5,0.2,33.6).s().p("Ai4DyQgBgfASgbIAxhDQAihwCIhnICBiQIAAAGQACACADABIgbAeQjrEJhJB6IgBABQgcAYgCAfIABABIAAABg");
	this.shape_145.setTransform(18.5,91.9);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.rf(["#DA0E0D","#850700"],[0,0.988],-1.9,-2.3,0,-1.9,-2.3,6.7).s().p("AgmAuQgDgBgCgDIABgFIABgNIACgGQABgEAIgHIAmgpQAGgHAHgCIAHgCIAHAAIAGAAQAHAIgKAIQgMgDgFAEIgnArQgHAIAAAGIgBAOQgFADgEAAIgDAAg");
	this.shape_146.setTransform(41,63.9);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.lf(["#6C0202","#C80000"],[0,0.988],-2.1,-3.2,0,-1.5).s().p("Ak0FJIACgEIABgBQBKh6DqkJIAbgeQAFADAHgFIABgPQAAgGAHgHIAogsQAFgEAMADQAKgJgHgHIgGAAIDFjUIATAGQhSBUhSBbQhGBQhGBUQiXC1iSDLIgBACQgWAbgBAaIgCAAIgBgIIgCALIgJAGQgCghANgjg");
	this.shape_147.setTransform(34.1,77.9);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.rf(["#67798D","#4F5D6F","#313C46"],[0,0.91,0.988],-2.1,1.9,0,-2.1,1.9,31.2).s().p("AjXD/IgCgDQgFgHAKgNIACgCIABgBQCojRD/kPIAFgFIgGALIhPCCIgDADQjBEdiGBSQgHACgFAAQgEAAgDgCg");
	this.shape_148.setTransform(47.1,65.5);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.rf(["#DD1600","#640101"],[0,0.988],-3.6,-0.9,0,-3.6,-0.9,8.7).s().p("AAdAZIACgCIACgEIABgDQhIgYgIAIQAGgYAbAAIABAAQATgCAeAHQANAHgDAYIgBACIAAABQgFAGgMAEg");
	this.shape_149.setTransform(63.5,36.1);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#596E81").s().p("Ajih8IA0hbQDYCvC5DIIhSA4QhujykFhig");
	this.shape_150.setTransform(94.1,56.7);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.lf(["#6C0202","#C80000"],[0,0.988],3,2.4,-0.2,-1).s().p("AlvFMQAbgEAcgZQDBihCpiiQBPhMBLhMQBUhWBNhWIAFATIjBDQIAAgHQgJgGgHAKQADAMgDAFIgoAsQgIAHgHABIgOACQgEAHADAFIgcAdQj4D8h0BSIgCABIgDACIAAAAQgeAPggABQgDgLAEgCg");
	this.shape_151.setTransform(77.6,118.6);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#6B0000").s().p("AgpG7QAHAEAQgMIACgCQDEi2D8kQIABgBIAPgBQhfBnhSBVIg6A8QhwByhlBeIgEAEQgOALgLAAQgGAAgGgFgAm4BJIgBgBQgLgLAMgUIAAgBIAAAAIADgDIAAgBIAEgFQBVhpBth6IA3g/QBQhYBehkIACAIIgBAIQkAEQioDQIgBABIgCACQgKAMAFAHIACAEIgBgCg");
	this.shape_152.setTransform(69.5,83.5);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.rf(["#67798D","#4F5D6F","#313C46"],[0,0.91,0.988],-0.9,0.9,0,-0.9,0.9,31.3).s().p("AjsDrIgBgBIgBgBQgCgFABgMIAAgDQBJiJEcjpIBvhMIALgCIgDAEIgBABQj7ERjEC1IgDADQgMAJgGAAIgEgBg");
	this.shape_153.setTransform(89.1,104.4);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.rf(["#DD1600","#640101"],[0,0.988],1.4,2.8,0,1.4,2.8,7.6).s().p("AgEACQgQgkgFAFQADgGAGgFIAAAAIACgBQAPgJANALIAFAGIAGAkQAGAjgbAIQAIgIgQgkg");
	this.shape_154.setTransform(116.9,85.8);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.rf(["#C60203","#870101"],[0,0.988],-37.9,40.8,0,-37.9,40.8,122.9).s().p("Ai1I2QgrAAhCglQgXgNgYgQIA+hDIABADIgBgDQgegXgbgZIgiggIg1g1QgYgbgXgdIgBgCIgEgDQgOgWgFgdIAAAAIAAgGQAAguBLhsQhLBsAAAuIAAAGIAAAAQAFAdAOAWIAEADIABACQAXAdAYAbIg8BDQgbgggTgfQgbgugIgeQgBgEABgEQACgaAWgbIAAgCQCTjKCXi2QBGhUBHhQQBRhbBShUIABgBIABAAIABAAQALgEAGgGIABgBIADAMQheBkhQBYIg3A/QhtB6hVBqIgEAFIAAABIgBAAIgBADIAAAAIgBABIg2BFIgHAGIgOAWQhFByADArIAAABQAGAbANAVIAEAFIABABQAWAbAZAcQAYAaAbAaIAhAfQAZAXAiAaIABACIA9AsIABABIACABQAVALAaADIABABQApABBthOIANgKIAEgFQAigcAogmIACgBIAGgFIAFgEQBmhfBvhxIA7g9QBShTBehpIANAAIgCAGIgBABIgBAAQgGAFgDAFQgCAFgBAFIABADQhNBXhUBUQhLBMhPBMQiqCkjACgQgcAagbADIAFgFgAiXHyQApgCBWhDIAAAAIAEgDIABgBIALgIIABgBIAAAAIABgBIACgCIgCACIgBABIAAAAIgBABIgLAIIgBABIgEADIAAAAQhWBDgpACIgDABIAAAAIgCAAIgCAAQgcgDgXgNIgBgBIABABQAXANAcADIACAAIACAAIAAAAIADgBgAjWHfIABABIABAAIACACIgDgCIgBgBIgIgFIAIAFgAjlHUIAHAGIgHgGIgtgggAmlGsIgHgGIA+hFIAiAgIg+BFIgbgag");
	this.shape_155.setTransform(59.5,94.7);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#6B0906").s().p("ADIDjQASgEALgJQALgJAEgLQAOgBgUAWIgMANIgBACIgNAAIgPABIADgEgAj2jAIgCgMIACgDIATgUQAQgIgHAQIAAACQgEgBgEABIgEADQgDADgGANIgFANg");
	this.shape_156.setTransform(93.1,58.1);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#AF0303").s().p("ADHDZIAogbIAGgBQgEAKgLAKQgLAJgSAEIgLACgAjzi+IAFgNQAGgOADgDIAFgCQAEgCADABIgWAjIgFAGg");
	this.shape_157.setTransform(93,58.6);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FF591D").s().p("AihiSQASgDAxAeQAuAdA1AvQA5AwApAxQAuA2ANAnQiCi2jBhvg");
	this.shape_158.setTransform(79.3,72.7);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.lf(["#EB2502","#A90101"],[0,0.988],19,-20,-20.3,20.3).s().p("AgwFPIgEgBQinhlhsicIgCgFQgHgRANgPIACgDIAAAAQCTiRCYjPIACgCIAAgBQAMgSAOABIABABIAHABIACABIABABQDLBiBwDGIABADQAAALgYASQioCXieCxIABAAQgMALgNAAIgHgBgAAAlAQDBBvCCC3QgNgngug3Qgpgwg5gyQg1gugvgdQgsgcgTAAIgDABg");
	this.shape_159.setTransform(63.1,90.1);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.rf(["#C60203","#870101"],[0,1],27.3,-28.4,0,27.3,-28.4,92.5).s().p("AjSInIgBAAQgbgDgVgLIgCgCIgBgBIg9gsIgBgCQghgZgagXIghgfQgbgagYgbQgZgbgWgbIgBgCIgDgEQgOgVgFgcIAAgBQgEgqBFhyIAOgXIAHgGIA2hEQgLASAKAMIABABIACABQAGAEAMgEQCGhRDDkeIADgDIBNiCIBLhLIh8DUQgEANAIAGIAAABQgPgBgNASIgBACIAAAAQiYDPiTCSIgBAAIgCACQgNAPAIARIACAFQBsCcCmBmIAFABQARADAOgOIAAAAQCdiwCoiYQAYgRAAgMIABAAIAAABIACACQAHAGAHgDIADgBIDOiLIg9BGIgJAGIhvBLQkdDqhICJIAAADQgBAMABAFIABABIABABQAOALAWgRIgGAFIgBABQgnAlgjAdIgEAFIgOAKQhrBMgpAAIgBAAgAm8BkIgCACQg+BVgEAzIABACQACAfAPAUIAAACIABABQBcB2CDBcIADACQAYAOAggBIADAAQA8gDBMhNIAEgEIABgCQAJgMgMgMIgCgCQi3iPiQilIgBgBQgMgLgKAAQgMAAgKANgAmLgLIABgBIgCADg");
	this.shape_160.setTransform(65.2,88.5);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.rf(["#5A6F82","#475863"],[0,1],2.9,-2.7,0,2.9,-2.7,8.7).s().p("Ag4grIA4gKQAmARATAkIgJA2Qgvg2g5grg");
	this.shape_161.setTransform(84.4,66.9);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#74899C").s().p("ABsDKQgnhFgxg4IAIg2QgTgkglgRIg4AKQg5gqhBggQgLgEAFgJIA3hhQEFBiBuDzIhMA0IgTAOQgDACgCAAQgDAAgDgDg");
	this.shape_162.setTransform(87.2,64.6);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#3E414A").s().p("AAvEVIgCgCIAAgBIgBgCQhvjGjMhhIgBgBIgCgBIgGgDIgBAAQgIgFAEgNIB9jVIAFgJIACgDQACgFAEgCIABgBIAEgBIACAAIACAAIABAAQAZAHAXAJQD5BYBrDyQALAZAKAaIgCAGIABAAIAAABIgBAAIAAADIgEAFIgGAEIgBABIgFADIjOCLIgDACIgFAAQgFAAgEgEgAiSj8IgEAFIAAACIgDAEIAAAAIg0BaIg3BhQgEAJAKAEQBCAgA4ApQA6AsAvA2QAxA4AmBFQAFAFAGgEIABAAIATgOIBMg0IBSg4IAGgEQAPgLgFgOQiAkEkIhoIgEgBQgIAAgHAIg");
	this.shape_163.setTransform(92,59.3);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#4C6174").s().p("AjRiyIAAAAIADgEIAAgBIAEgGQAIgIALABQEIBpCAEDQAFAPgPAKIgGAEQi5jIjZivg");
	this.shape_164.setTransform(97.6,53);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#760101").s().p("AiZiNQA6ARAxAcQAhATAdAaQAWARATAWQAaAbAWAiQAcAqAVAzQhljGjOhVg");
	this.shape_165.setTransform(102.5,47.4);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.lf(["#AD0002","#FB1E1E"],[0,1],-11.9,-5.2,8.9,0.5).s().p("AjbB0IAAgBIgDABIgCAAICNiQIAegVQBqhHBKgKQA5AFApAbQhXgSk1D3QgXgJgZgGg");
	this.shape_166.setTransform(100.2,19.7);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.lf(["#5B0202","#720000"],[0,0.988],-6.3,-3.7,5.9,7.7).s().p("Aj5CnIB0h3QBLhrBZgsIAEgCIABgCQBZhRAtAWIACAAQAnANAnAaQhEgjghACIgCAAQgXAAgeAVIgCADQiVB4jAC8g");
	this.shape_167.setTransform(93.4,17.2);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-0.4,-2.1,0,-0.4,-2.1,10).s().p("AhTAiIALgMIAigiIACgCQAlgvBKAiIAAABIACABQAPAGgEABQgEABgFAAIgHgBIgOACQg4AHhJAqIgOAHIgDACQgCAAAHgIg");
	this.shape_168.setTransform(106.8,7.5);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],1,-4.3,0,1,-4.3,10).s().p("AgagpIACgCQAtg5BjA+IAOAJIACACIABABIgEgBIgLgDQgWgEgaAEQhKAKhqBGIgeAVgAgWgkIgCACIgiAiIgLAMQgLALAJgFIAOgHQBIgqA4gHIAPgCIAHABQAFAAAEgBQAEgBgPgGIgCgBIgBgBQgegOgZAAQghAAgWAbg");
	this.shape_169.setTransform(105.5,9.7);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.lf(["#6D0101","#D60C02","#771B1E"],[0,0.447,0.988],-3.8,-0.2,-7.8,-4.5).s().p("AkMC8IAVgjIABgCQAHgQgRAIIgSAUIgBgdQDAi8CVh4IACgCQAegWAXAAIACAAQAhgBBEAiIAGAEQAXAPAXATQgXgJgWgEIgCgCIgOgJQhjg+guA6IgCABIhtByIiNCPIgDABIgBABQgFABgCAGIgBACIgGAKIhKBLg");
	this.shape_170.setTransform(96,21);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.lf(["#AD0002","#FB0D0D"],[0,1],7.9,10.8,-3.5,-7.6).s().p("AhmDoIACgGQgJgagLgYQDclAgUhYQAhAsAJAvQgHBOhHCEIiRCkg");
	this.shape_171.setTransform(130.9,48.4);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.lf(["#5B0202","#720000"],[0,0.988],4.8,6.1,2.6,4.2).s().p("AitEFIACgGIACgBIAWACQC2jPBtidQAUgggCgXIAAgBQgBgignhAQAdAlARAlIAAABQAYAthLBeIgCACIgBAEQgnBbhlBTIh2CDQgMgLgRAJg");
	this.shape_172.setTransform(132.8,55.5);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],3.2,-2.9,0,3.2,-2.9,9.7).s().p("AAUhNQACgggJgYQBHBggqA5IgBACIhiBwQBHiGAGhNgAAYhiIAAAJIABAIIgBANQgDA5glBMIgGAOQgEAIAKgKIAMgNIAfgkIABgCQAsgogohKIgBgBQgFgKgCAAIAAABg");
	this.shape_173.setTransform(141,41.9);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],41.8,-22.1,0,41.8,-22.1,9.7).s().p("AB6D4IgFgTIgBgDQAAgFADgFQAFgEAQAlQARAkgIAHQgPgLgMghgAFrhxIAGgOQAmhNADg4IABgOIgBgHIAAgKQABgEAGAOIABABQAoBJgsAqIgBABIggAkIgMANQgGAGgBAAQAAAAAAAAQAAgBAAAAQAAgBAAAAQABgBAAgBgAl+jlIgOgEQgjgJgFgIQAJgJBJAaIgBACIgIAHIgTgFg");
	this.shape_174.setTransform(102.4,61.2);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.lf(["#6D0101","#D60C02","#771B1E"],[0,0.447,0.988],-5,8.1,1.2,13.2).s().p("AiXEXIAMgNQAUgWgOABIgGABIgpAcIA+hGIAFgEIAAAAIAGgFIAEgFIAAgCIABgBIAAAAICRikIBjhvIABgBQAqg7hHhfQgCgVgIgQIAgAlIAEAGQAoBBAAAiIAAABQACAXgTAfQhuCei1DOg");
	this.shape_175.setTransform(130.9,53);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.lf(["#CC0005","#FA7437"],[0,1],-15.2,15.6,11.5,-13.4).s().p("AiRB1IECkQQASAOAPAQIj7EZQgSgWgWgRg");
	this.shape_176.setTransform(118.8,30.4);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.lf(["#9AAEBA","#DBE3E8"],[0,0.988],-12.2,13,13.9,-14.2).s().p("AhfCSID7kZQAYAZAUAdIj3EhQgWgigagcgAjHA+IEJkNQAeATAbAXIkDEQQgegaghgTg");
	this.shape_177.setTransform(117.8,31.4);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.lf(["#CC0005","#FF6600"],[0,1],-7.5,8.3,19.2,-20.7).s().p("AkXgxQE0j4BXASIADAAIABABQAHADAGAEIkIEOQgxgcg6gRQDPBUBkDIQgVg0gcgqID4khIAHALIADAEIABAEQATBYjdE/Qhqjzj6hXg");
	this.shape_178.setTransform(111,37.8);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#CD0C00").s().p("ABuAwIgBgEIgDgEIgHgLQgUgcgZgYQgPgQgSgPQgZgWgfgTQgGgFgHgDIgBgBIgDAAQgogbg6gFQAagEAWAEIAMADIADABQAOADAMAIIADADQAPAIAOAJQAdATAaAXQASAOAQAQQAYAYAVAcQAPAUAOAXIACACQAFAHADAIQAJAXgCAhQgJgvgggsg");
	this.shape_179.setTransform(127.9,20.3);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],4.8,-3.3,0,4.8,-3.3,13.7).s().p("ABsBeIgCgDQgOgWgPgUQgVgdgYgXQgQgRgRgOQgagWgegTQgOgJgPgIIgDgDQgMgIgOgEIAAgBQAWAEAWAKQAwATA1AtQAzAwAZA1QAIARACAUQgDgHgFgHg");
	this.shape_180.setTransform(130.7,17.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.redcar_1, new cjs.Rectangle(-0.5,0,150.7,155.7), null);


(lib.Question_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{r1:0});

	// timeline functions:
	this.frame_0 = function() {
		/* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// QuestionMark
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag4DGQgMgMAAgTQAAgUANgMQANgNAUAAQAUAAAMANQANAMgBATQABATgNANQgMAMgVAAQgUAAgNgMgAg9BFQAAgeAPgfQARgfAYgTQAcgWAHgJQANgQAAgRQAAgTgPgNQgQgMgYAAQgRAAgUAOQgWAPgKAAQgLAAgJgKQgIgJAAgNQAAgXAagNQAhgUApAAQA2AAAgAcQAhAcAAAuQAAAtgrAmQgPAMgIAIQgRAPgKAQQgFAKgHAlQgFAkgdAAQggAAAAgpg");
	this.shape.setTransform(42.3,42.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// QuestionImages
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AxqU+IAAusIJXAAIAAF3IZ+AAIAAI1gAxql1IAAvIIWFAAIAAJOINQAAIAAF6g");
	this.shape_1.setTransform(-112.2,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AN8VuIAAquItmAAIAAo1MAirAAAIAATjgEgjAgDqIAAsJMAjWAAAIAAl6MAirAAAIAAL9IkRAAIAAkLIxLAAIAAEiMgnOAAAIAAFvg");
	this.shape_2.setTransform(-1.2,64.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("EgjAAaVIAAquMAjWAAAINmAAIAAKugAAWxGItQAAIAApOMAv7AAAIAAJOg");
	this.shape_3.setTransform(-1.2,34.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AkVIEI5/AAIAAl3IAAltMAnOAAAIAAkjIRKAAIAAELIERAAIAAL8g");
	this.shape_4.setTransform(28.7,26.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer 7
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#042727").s().p("EgpVAh6QisAAAAitMAAAg+aQAAisCsAAMBSrAAAQCsAAAACsMAAAA+aQAACtisAAg");
	this.shape_5.setTransform(-2.2,33.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(0,0,0,0.02)").s().p("EApWAjAMhSrAAAQh5AAg8g+Qg9g8AAh5MAAAg+aQAAjyDyAAMBSrAAAQDyAAAADyMAAAA+aQAAB5g9A8Qg8A+h5AAIAAAAg");
	this.shape_6.setTransform(-2.2,33.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Question_mc, new cjs.Rectangle(-291,-190.6,577.7,448), null);


(lib.pinkcar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#333300").ss(1,1,1,3,true).p("AhYpIQALgDAKgCQAXgFAXgBQAOgCAOABQAWAAAXAEQAQADARAFIACAAQAFABAGACQAPAJANASQAhArAaBoIAGCeAC1oCQAAgBAAAAQgHgzhMgPABxpOQAvAOAQARQAMALAFAbQAVCIAHDGIgLAKACckNQAQAHARAJQAAABACADAC/j4QAAABABAAQABADAAACQgBADAAACIAAABIgBAEIADBDADTifQAAgFAAgGIAAAAQAAgBABgBQADgNALAAQADAAADAAIATARQASAPgJASAC6E8QAFgBACgOQAAgBAAgBQAKjAgGkKQAAgBAAAAACSgyQAAgBAAABQABAAABAAQAGAAACgFQABgBAAgBIAiivADBidIgTBeQgbEGAcBsQABAAAAABQAFAGADACQABAAAAAAQABAAAAAAADTifQABgGAZALQAXALAAAIAC5mXIgEhrADeBBQAAgJAGgBIADADIADADQABACACACQACAFAAAGIABApQAAAJAAACQgBADgBABIgFAHADmBJQAAgEgIgEADnBzIgBgqADeCHIAGgIQADgDAAgJADjCOQgDgBgCgGADjCOQAAAQAAANQgCD/gRBkQAAABgBABQABABAAACIAAAAQACAXgNASQgCACgDADQgNAPgUABADAIHQASiyAGiqQADhOAAhNQABhVgDhUACQHmQAbggABhOQgBgBAAgCQAAgBAAgBQgBgKgLAAQgBAAgBAAQiWARiNgOQgBABAAAAQgWAAACAWQAAABAAACQAJBKAWAdIABAAQAPAPAQADIABACQBgAPBngTIADAAQARgFAOgQADPITIAAAAQAAgCAAgBADCGGQgMBigWAWQAAAAAAABQgPANgQAEQgBAAAAAAQgBABgBAAABxITIAGgBQADAAADgBIABAAAC5I6QgLAMgPALAC5I6QAOgRAIgWACzIzQAMgQABgcABNIYQgBAAgBAAQgYADgYABQgQAAgOAAQgYAAgYgCQgYgBgYgEQgBAAAAAAQgCAAgCAAQgQgEgPgNIAAAAQgXgWgOhhABxITIgkAFAjei1QAFjBAQiJQAAgCAAgBQAEgZALgMQAAgBABAAQAPgSAvgPAi/n2QAAgBAAgBQgEg1BKgQQAFgCAGgBAhspAQAIgFAKgCIACgBAi/mFQABgNADgNQAShZAegrQALgPALgIQAEgDADgCAi/jfIgCgIQgBgBAAgCQgBgDABgDIAAgBQABgBABgBIABiSIAAhxAi+j2QgBABAAABQgBAAAAABAjkiwQAJgDAKAOQAAABABABQAAAAAAABQAAAFgDAIAkDh8QAAgJAugXQAAABAAABQACABAAACQAAABgBABIABAAAj+iZQAJgMARgLAjAiTIABhMAkDh8QgIgQANgNAi+j2IAAAAAiPgsIAAABQgHABgDgKIgmirAiqgpIAAADQAmD0gZBtQgFAJgEABIAAAAQgBAAAAAAIgBAAQgKgBgEgQAiJgvIAAAAQCMgyCOAuAiLguQABgBABAAAiLguQgBAAgDACAigkGQgPAHgPAJAjAiTIAWBqAjaA/QAHABgBAJQgHAEAAAFIABArQAAAIACACIAHAIQgCAGgDACQABAPAAANQAKEAAVBjIgBABQABACABABQAJAZAQAQAjgBGIADgEIADgDAjkBVQgBgGADgEQABgEABgBAjhCNQgBgBgBgDQgBgCAAgIIAAgqAjcCUIgFgHAinIPQgYiygLiqQgFhOgDhMQgDhXAAhVAimIQIgBgBAirJCIABABQAAgBAAABQAQATAWAAQgNgHgKgLQgBgBAAAAAi3IXQgBAaANARAiaI3QgKgOgCgZAigkGQCdhOCfBH");
	this.shape_99.setTransform(-1.1,0.5);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#CC0000").s().p("Ai2AQIAAAAIgBgBQgNgQABgaIACADQAJAYAQAQIABABQAKALANAHIgBAAQgWAAgPgTgACtAHQAOgQAIgWIAAAAIAAgDIABADIAAAAQACAXgNARIgFAFQgNAPgUABQAPgLALgMg");
	this.shape_100.setTransform(0.1,56.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.rf(["#C60203","#870101"],[0,1],19,-47.3,0,19,-47.3,66.5).s().p("AAAAAIAAAAIAAABg");
	this.shape_101.setTransform(-19.5,31);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-1.6,-1.3,0,-1.6,-1.3,7).s().p("AgbA1IgBgLIABgjIgBgCQgDgqAxgTIACgBQAKgEgCADQgBACgDACIgDADIgGAIQgWAggQA6IgDALIAAACIgBgHg");
	this.shape_102.setTransform(-17.1,-50.2);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],-2.5,-3.7,0,-2.5,-3.7,7).s().p("AgpgTIAAgBQgEg1BKgRIAKgCIACgBIABABIgCABIgHAEQgMAJgKAPQgeAqgRBYQgDANgCAOgAALhRIgCABQgxATADAsIABABIgBAiIABAMQAAALABgHIADgKQAQg6AWggIAGgIIADgDQADgCABgDQABAAAAAAQAAAAgBgBQAAAAAAAAQAAAAAAAAIgIACg");
	this.shape_103.setTransform(-16.2,-47.9);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#336600").s().p("AhmHHQgogMgPgTIgEgFIAEADIABgBIAEAEQARAMAhALQAXAGAcADQAWADAaAAIAFAAIAYgBQAZgBAXgEQASgDARgFQAwgOAUgVQgQAbg0AQQgvANg8AAIgDABQg5AAgsgNgAgFGJQgZAAgYgBQgYgCgYgDIgBAAIgDgBQgQgDgPgNIgBgBQgXgVgOhiQATBdAVAUIABAAQAPAMAPAEIAEABIABAAQAXAEAXABIAwACIAdgBQAWgBAbgDIACAAIAwgIIABAAQABABAAAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAPgFANgLIABAAQAUgUAQheQgLBhgWAWIgBABQgOANgRAFIgBAAIgCAAIAAAAIgHABIgGACIgkAFIgBgBQgZADgYABIgPABIgOgBgAiqCyIgBAAQgKAAgDgRIgBAAIAAgCIAAgBIAAgBIgBgEQgHhigFh1IgCg7QgDhVgBhjIAEADIADAFQADEMARC+IAAABIAAADQAAALAGACIADAAIgCAAgAC/CaIAAgCQAJi/gFkLIAAgBIAGgIQADBlAABUIAAA9QgBBygEBkIgBADIAAADIAAABIAAACIgBAAIABAAIgBABIAAABQgEAMgJACQAFgCACgOgAhHnJQAYgHAagCQARgCASACQAZACAaAGQAgAJAiAPQiKg1iHA7QAjgUAkgJg");
	this.shape_104.setTransform(-0.9,15.1);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],1.2,-4.1,0,1.2,-4.1,6.8).s().p("AgPg8QgNgRgPgJQBMAOAGA0IABABIAEBqQgbhngggsgAgWhJQABADADACIAEAEIAGAHQAXAfARA5IADAKQADAHgBgLIAAgMIgCgiIABAAQACgsg0gSIgBgBIgHgCQAAAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAg");
	this.shape_105.setTransform(13,-49);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],15.3,-18.8,0,15.3,-18.8,6.8).s().p("AkDDeQAAgIAugXIAAABIAAAHIgLAHIgJAGQgRAKgHAAIgCAAgADkDJIgLgIIgCAAIgEgGQABgFAZALQAXAKAAAIIgCAAQgLAAgTgKgACxhpIgDgKQgSg7gYgfIgGgHIgEgDQgDgCgBgDQgBgDAJAEIABABQA0ARgCAsIAAABIACAjIAAAMQAAAGgBAAIgBgCg");
	this.shape_106.setTransform(-1.1,-34.3);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#006600").s().p("ADUDMIALgLQgHjEgViJQgFgagMgLQgQgRgvgOQAeAFAZALIABAAQAfALAIBWIAAACIAAADQAYBDgKBdIAEB+QgLAAgDANgAjeDFIAAh3QgNhcAWhEIABgDIAAgBQAEhWAggMIABAAQAYgNAegGQgvAPgPASIgBABQgLAMgEAZIAAADQgQCHgFDCg");
	this.shape_107.setTransform(-1.3,-37.7);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#00CC00").s().p("Ai1IgIgCgCIABgBQgVhkgKj/IgBgdQADgBACgGIgHgIQgCgDAAgIIgBgqQAAgFAHgEQABgJgHgBIgDADIgDjPIALgHQAABVADBXIAICbQALCpAYCzIABABQACAZAKAOIAAABIgEgDIAEAFIgCAIQgQgQgJgZgACzI5QAMgQABgbQASizAGiqQADhOAAhNQABhUgDhVIALAIIADDLIgDgDQgGACAAAIQAIAFAAAEIABAqQAAAIgDAEIgGAIQACAFADABIAAAdQgCEAgRBkIgBABIAAADIAAAAQgIAWgOARQgGgEAAgDgAhxFWIgDgCQgLgEgBgPIAAgCIAAAAQADiVgXi2IAAgCIAAgBQgDgOAIgHIAEgEIACAAIAAgBQCMgyCOAvIABABQAFAGgDAVQgKChAGCrIAAgBQgBAPgJAHIgDABQhCARhBAAQg4AAg5gNgAj+iTQAJgMARgLQAJgDAKAPIABABIAAABQAAAGgDAIIgBAAIABgDQAAAAAAgBQAAAAgBgBQAAAAAAAAQAAgBgBAAIAAgBQguAXAAAIQgIgQANgNgADtiTQgZgLgBAFIAAgKIAAAAIABgCQADgNALAAIAGAAIATARQASAOgJASQAAgIgXgKgAhPo8IACgCIABAAQAFgCAEgBQAXgFAXgBIAcgBQAXABAWAEIAIACIAEABIACABQAxAkApEVQifhIidBPQAhkbAwgigAgTk5QgZACgZAHQgkAJgjAUQCIg7CJA1QgigPgfgJQgagGgZgCIgSgBIgSABg");
	this.shape_108.setTransform(-1.1,-0.2);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#009900").s().p("AhPJgIAAgBQhJgGgeghQgPgQgDgWIgHg8QgkhLASh6IgEiLIACAEIADgBIACAdQAJD/AVBkIAAABQgCAaANARIABAAIABAAQAPAUAXgBQgOgHgKgLIAAgBIABgIQAPATAoAMQAtANA8gBQA7AAAvgNQA0gQAQgbQgUAVgwAOQgRAFgSADQgXAEgZABIgXABIgGAAQgaAAgWgDQgcgDgXgGQghgLgRgMIgEgEQgLgOgCgZIgBgBQgXizgLipIgJibQgDhXAAhVIABAAIABgBIAAAAQAEgIgBgGIAAgBIAAgDIgNgPQAEjBAQiIIAAgEQAEgZALgMIABgBQAPgRAvgQIAFgBIAlgFQgPAGgMAJIgCABIgKACQhLARAEA1IAAABIAAByQACgOADgNQARhZAfgqQAKgPAMgJIAHgEIACgBQAIgGAJgCIADAAIAVgFQAWgFAXgCQAPgBAPAAQAVABAXAEIAhAIIACAAIAKADQAPAIANASQAhAsAbBoIgEhrIgBgCQgGgzhNgOQgKgJgLgFIAfAEIAFABQAwAOAQARQALALAGAaQAUCJAIDFIgMALIACAFIAAACIgBAAIABAKIAEAGIABAAQADBVAABUQgBBNgDBOQgGCqgRCzQgCAbgLAQQgBADAHAEQgMANgOAKQATAAAOgPIAEgGQAOgRgDgXIAAgBIAAgDIAAgBQAShkACkAIAAgdQAAABABAAQAAAAAAAAQABAAAAAAQABAAAAAAIADgDIgBCKQAWB6ghBMIgFA8QgDAagTATQgdAdhDAIIgBAAQgyAHgxAAQgiAAgigDgAhtIIQAYADAYACQAYABAZAAIAdAAQAYgBAZgDIABABIAkgFIAGgCIAHgBIAAAAIACAAIABAAQARgFAOgNIABgBQAWgWALhhQgQBegUAUIgBAAQgNALgPAFQAAAAAAAAQgBABAAAAQAAAAAAAAQAAAAgBgBIgBAAIgwAIIgCAAQgbADgWABIgdABIgwgCQgXgBgXgEIgBAAIgEgBQgPgEgPgMIgBAAQgVgUgThdQAOBiAXAVIABABQAPANAQADIADABIABAAIAAAAgAiRFfQgVAAACAXIgBADQAKBKAVAcIACABQAOAPAQADIACABQBgAPBmgSIADAAQARgGAPgQIACgCQAagfABhOIAAgDIAAgCQgCgLgLAAIgCAAQiVARiOgNIgBAAgAiJEtQAAAPAMAEIADACQB5AbB7gfIACgBQAKgHABgPIgBABQgFirAJihQADgVgFgGIAAgBQABAAAAABIACgBQAGABACgGIABgCIAhiuIABgFIAAAAIABgFIAAgFIgCgCIAAgBIgFidIAFCdIgCgEIgggPQgqkVgxgkIgCgBIgDgBIgIgCQgXgEgVgBIgeABQgWABgXAFQgFABgFACIgBAAIgBACQgxAiggEbQgQAHgPAIIAAABIgBABIgBABIABiRIgBCRIgBADIAAAAQgBAEABADIAAADIACAHIgBBMIgCgJIAAAGIgDgFIgEgDQABBjADBUIACA7QAFB2AHBiIABAEIAAABIAAABIAAAAIAAACIABAAQADARAKAAIABAAIACAAIAAAAQAEgBAEgJQAahsgnj0IABgDIgXhrIABhMIAmCsQAEAJAGgBIABABQgIAHACAOIAAADIABAAQAWC2gCCVIgBAAIABACgAC3irIgSBfQgbEGAcBrIABABQAFAHADABIABABIABAAQAJgCAEgMIAAgBIABgBIgBAAIABAAIAAgCIAAgBIAAgDIABgDQAEhkABhzIAAg9QAAhTgDhlIgGAIIgBgEQAAADgEAEgAC4izIgDhCg");
	this.shape_109.setTransform(-0.2,1.8);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#3C4A58").s().p("AhgIuIgCgBQgQgDgOgPIgCgBQgVgcgKhKIABgDQgCgXAVAAIABAAQCNANCWgRIACAAQALAAACALIAAACIAAADQgBBOgaAfIgCACQgPAQgRAGIgDAAQg5AKg2AAQgsAAgrgHgAirFyIgDAAQgGgCAAgLIAAgDIAAgBQgRi/gDkLIAAgGIACAJIAXBqIgBADQAnD1gaBsQgEAJgEABgAC0FqIgBgBQgDgBgFgHIgBgBQgchrAbkGIAShfQAEgEAAgDIABAEIAAABQAFEKgJDAIAAACQgCAOgFACIgBAAgAjhDBIgGgGIgBgEQgCgDABgIIgBgpQAAgGADgFIACgFIADgDIADgDQAGABAAAJQgHAEgBAFIABAqQAAAIADADIAGAIQgCAGgDABIgDABgADeC7QgEgBgBgFIAFgIQADgEABgIIgBgqQgBgEgHgFQgBgIAGgCIADADIADAEIADAEQADAFAAAGIABApIgBALIgBAEIgFAHIgDADIgBAAIgCgBgAifgGIgmisIgCgHIAAgDQgBgDABgEIAAAAIABgDIABgBIABgBIAAgBQAPgIAQgHQCdhPCfBIIAgAPIACAEIAAABIACACIAAAFIgBAFIAAAAIgBAFIghCuIgBACQgCAGgGgBIgCABQAAgBgBAAIgBAAQiNgviMAyIgBABIgBAAIgEACIgBAAIgBAAQgGAAgDgHgACRgbIAAgBIADgQgAhyoTQAMgJAPgGQAfgOAtgEQAuABAkAOQALAFAKAJIgKgDIgCAAIghgIQgXgEgWgBQgOAAgPABQgXACgWAFIgVAFIgDAAQgJACgIAGg");
	this.shape_110.setTransform(-0.6,-4.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.pinkcar_1, new cjs.Rectangle(-28.4,-60.5,54.7,123.5), null);


(lib.bluecar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000033").ss(1,1,1,3,true).p("ABkptIACAAQAGABAHACQARAIAQAUQAmAuAfBvIAHCoADPpTIAAAAQAOAMAGAcQAYCRAJDTIgOALAhnptQAMgEAMgCQAbgFAbgCQARgCAQABQAaABAbAEQATADAUAGACEp0QA4APATASADVojQAAgBgBAAQgHg3hagPAC3keQATAHAUAKQAAABACADADgkIQABABABAAQAAADAAACQAAAEgBACIAAAAIgBAFIAEBHACrg1QABgBAAABQABAAABAAQAHAAADgGQAAAAABgCIAni6AEKjFQADAAADABIAWARQAWAQgKATAD4i1IAAAAQAAgBABgBQAEgNANgBAD4iqQAAgEAAgHADbFRQAGgBACgPQAAgCAAgBQALjMgGkcQAAAAAAgBAD4iqQABgFAdALQAcALAAAJADiinIgVBkQggEXAhBzQAAABABAAQAGAHADACQABAAAAAAQABAAABAAADZmxIgEhyAEFBFQgBgJAIgCIADAEIADADQACACACADQADAFAAAHIABArQAAAJAAADQgCACAAACIgGAIAEOBOQAAgEgJgFAEMCIQADgEABgJIgCgtAELCXQgEgBgCgGIAHgIAELCXQAAARAAAOQgDEQgUBrQAAABgBAAQABACAAABIAAAAIAAABQADAZgQASQgCADgDADQgQAQgXAAADSJXQAOgQACgdQAUi/AHi1QAEhTAAhSQABhagDhaACpIGQAggiABhTQgBgBAAgCQAAgBAAgBQgCgMgNAAQgBAAgBAAQiwATimgPQgBABAAAAQgaAAADAYQAAABgBACQALBPAaAeIABAAQASARASADIACABIAAAAQBxAQB5gTIADAAQAUgGARgRADaJfQAQgSAJgYIAAAAQAAgCAAgBADjGgQgNBogaAXQAAABAAAAQgRAOgUAFQAAAAgBAAQgBAAgBAAACEI2IAHgCQAEAAAEgBIABAAABaI7QgBAAgBAAQgdADgcABQgSABgRgBQgcAAgdgBQgcgCgcgEQAAAAgBAAQgCAAgCAAQgTgEgSgOIAAAAQgbgXgRhoACEI2IgqAFADaJfQgOAOgRAKAjhoXQAAgBAAgBQgFg4BYgSQAFgBAHgBAkFjBQAFjOATiRQAAgCAAgBQAFgbANgNQAAAAABgBQASgTA3gQAhqptIADAAAh/plQAKgGALgCAjhmeQACgOADgOQAVhfAkgtQAMgQAOgJQAEgDAEgCAjjj2QgBgBAAgCQgBgDABgEQAAAAAAgBQABgBABgBIABibIAAh5AjgkFQgBAAAAABQgBAAAAABAjgkGIAAABAjhjuIgCgIAkxiEQAAgJA2gZQABABAAABQABABAAACQAAABAAACIAAAAQAFgJgBgGAkNi8QALgDAMAQQAAABABAAQAAABAAAAAjiidIABhRAkxiEQgJgRAPgOIAAAAQALgNATgMAjIgsIAAAEQAtEEgeB0QgFAJgFABIAAAAQgBAAgBAAIgBAAQgMgBgEgRAjiidIAaBxAijgxQABAAAAAAIABgBQCkg1CnAxAiogvIgBABQgHABgEgKIgti3AijgxQgCABgDABAi8kXQgSAIgSAJAkHBKIADgDIAEgEQAIACgBAJQgIAFgBAFIABAtQAAAIADADIAIAJQgCAGgEACQABAQABAOQALEQAZBqIgBABQgCAcAQASIABABQAAgBAAABQASAUAbAAQgQgIgMgLQAAgBAAAAAkNBbQAAgGADgFQACgEABgCAkJCXQgBgCgBgDQgBgCAAgJIgBgsAkCCeIgHgHAjFIyQgbi+gNi1QgGhTgEhSQgEhcAAhaAjXI6QABACABABQALAaATASAjEIzIgBgBAi1JcQgMgOgDgbAi8kXQC4hUC7BN");
	this.shape.setTransform(-0.9,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#C60203","#870101"],[0,1],22.3,-50.4,0,22.3,-50.4,70.8).s().p("AAAAAIAAAAIAAABg");
	this.shape_1.setTransform(-22.4,33.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],-1.8,-1.5,0,-1.8,-1.5,7.7).s().p("AggA5IAAgMIAAgmIAAgBQgEguA5gUIABAAIACgBQALgEgBADQgCADgDACIgEADIgHAJQgaAigSA9IgEAMIgBACQgBAAAAgHg");
	this.shape_2.setTransform(-19.7,-53.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],-2.9,-4,0,-2.9,-4,7.7).s().p("AgwgUIAAgCQgFg4BXgSIAMgCIACgBIABABIgCABIgIAEQgOAKgMAPQgjAtgVBfQgDANgCAPgAANhWIgCAAIgBABQg5AUAEAuIAAACIAAAlIAAAMQAAAMACgHIAEgMQASg+AaghIAHgJIAEgEQADgCACgCQAAgBAAAAQAAAAAAAAQAAAAAAAAQgBAAAAAAIgJACg");
	this.shape_3.setTransform(-18.6,-50.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC0000").s().p("AjWARIAAAAIgBAAQgQgSACgbIACADQALAZATARIAAABQAMAMAQAHIgCAAQgZAAgSgUgADMAIQAQgRAJgYIAAAAIAAgDIABADIAAAAIAAAAQADAZgQASIgFAGQgQAPgXABQARgLAOgNg");
	this.shape_4.setTransform(0.5,60.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0097FF").s().p("Ag9J7QgggDgbgHQgogLgUgNIgEgEQgNgPgCgbQACAbANAPIgBABIgFgDIAFAGIgCAIQgTgRgLgaIgBgDIAAgBQgZhqgLkQIgBgfQADgBACgHIgHgIQgDgDAAgJIgBgtQAAgFAIgEQABgKgIgBIgDADIgEjcIANgHIABgBIABAAIAAgBQAEgIAAgGIAAgFIgQgPQAFjOATiSIAAgDQAEgbANgMIACgBQARgTA3gRIAGgBQAVgEAWgCQgRAHgOAKIgCAAIgNADQhXASAFA4IAAACIgBB5IgBCaIgBADIAAABQgCADACAEIAAADIADAIIgCBRIgCgKIAAAGIgEgFIgEgDQABBpADBaIACA/QAGB+AJBnIABAFIAAABIAAABIAAAAIAAACIABABIAHBDIgBAHIAFAUQAQBoAbAXIABABQASANASAEIAEABIACAAQAcADAcACQAcACAdAAIAjAAQAcgBAdgEIACABIAqgGIAHgBIAHgBIABAAIACgBIACAAQATgFARgNIABgBQAagXANhpIACgNIgBgEQAEgjACgpIAAgBIABgBIgBAAIABgBIAAgBIAAgCIAAgCIABgEQAFhrABh6IAAhAQAAhZgDhsIAHgHIACAEIAAACIgBAAIABALIAEAGIACABIANAIIAEDYIgEgDQgHABABAKQAIAEABAFIABAtQAAAIgEAEIgHAJQACAGAEABIAAAfQgCEQgVBqIAAACIAAADIgBAAQgJAYgQASQgIgFABgDIABgGQgZAXg4AOQgTAGgWAEQgaAEgeABQgOABgPgBIgGABQgegBgagDgAD+AKQgBBRgDBUQgHC1gVC+QgCAdgNARQANgRACgdQAVi+AHi1QADhUABhRIAAgnQAAhHgDhGQADBGAABHIAAAngAj7ATQAEBRAGBUQANC0AcC+IABABIgBgBQgci+gNi0QgGhUgEhRQgDhdAAhaQAABaADBdgAi4JVIAAAAg");
	this.shape_5.setTransform(-0.5,1.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFFFFF","#8B919D"],[0,0.988],18,-20,0,18,-20,7.4).s().p("AkxDtQAAgJA2gZIABACIgBAIIgNAHIgKAFQgUAMgIAAIgDAAgAELDWIgMgIIgCAAIgFgHQABgFAdALQAcALAAAJIgDAAQgOAAgWgLgADQhwIgEgLQgVg+gcghIgHgIIgEgDQgEgDgBgDQgCgCALAEIABAAQA9ATgCAvIAAABIACAlIAAANQAAAHgBAAIgBgDg");
	this.shape_6.setTransform(-0.9,-36.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#BFBFBF","#6B727A"],[0,0.988],1.4,-4.4,0,1.4,-4.4,7.4).s().p("AgRhAQgQgTgRgJQBZAQAHA3IABABIAEBxQgfhuglgvgAgahOQABADAEACIAEAEIAHAIQAbAhAVA9IAEALQACAHAAgLIAAgOIgCgkIAAgBQACgug8gTIgBAAIgJgDQAAAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAg");
	this.shape_7.setTransform(15.7,-51.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#001C94").s().p("AhZKEIAAAAQhWgHgjgjQgRgRgEgYIgIg/QgqhQAUiDIgEiTIACADIAEAAIACAeQALEQAZBqIgBABQgCAcAQASIABABIAAAAQASAUAbAAQgQgIgMgLIAAgBIABgJIgEgGIAEADIABgBIAFAFQAUANAnALQAbAGAgAEQAbADAeAAIAHAAQAOAAAOgBQAdgBAagEQAWgEAUgFQA4gOAYgXIgBAFQAAAEAIAEQgOAOgRAKQAXAAAQgQIAFgGQAQgSgDgZIAAgBIAAAAIgBgDIABgBQAUhrADkQIAAgfIAEABIADgDIgBCTQAaCBgnBSIgGA/QgEAcgVAUQgjAfhOAJIgCAAQg7AHg7AAQgnAAgngDgAgLItQgcAAgdgBQgcgCgcgEIgBAAIgEAAQgTgEgSgOIAAAAQgbgXgRhoQAWBjAZAVIABAAQARANATAEIAEABIACAAQAaAEAcABQAcACAcAAIAigBQAaAAAggEIACAAIA5gIIABAAIACAAQARgFAQgMIABgBQAXgVAThkQgNBogaAXIAAABQgRAOgUAFIgBAAIgCAAIgBAAIgIABIgHACIgqAFIgCAAQgdADgcABIgSAAIgRAAgAjMFIIgBAAQgMgBgEgRIAAgBIAAgBIgBgCIAAgBIgBgEQgIhogGh9IgDhAQgDhZgBhqIAFAEIADAFQAEEcATDMIAAABIAAADQABAMAHABIADABIgCAAgADcEvIAAgDQALjMgGkcIAAgBIAHgIQAEBrAABaIAABAQgBB6gGBqIAAAEIAAADIgBABIAAACIAAABIAAAAIAAABIAAAAQgFAOgLABQAGgBACgPgADwjNIgBgBIAOgLQgJjTgYiRQgGgcgOgMIAAAAQgTgSg4gPQAkAFAeAMIAAAAQAlAMAJBcIAAACIABACQAbBIgLBkIAFCGQgNABgEANgAkOjVIAAh/QgQhkAahIIABgDIAAgCQAFhbAlgMIABgBQAdgNAjgHQg3AQgSATIgBABQgNANgFAbIAAADQgTCRgFDOg");
	this.shape_8.setTransform(-0.2,2.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3C4A58").s().p("AhxJTIAAgBIgCgBQgSgDgSgQIgBgBQgagegLhPIABgDQgDgYAaAAIABAAQCmAOCwgSIACAAQANAAACALIAAACIABAEQgBBTggAhIgCACQgRARgUAGIgDAAQhDALg/AAQg1AAgzgHgAjKGKIgCAAQgHgCgBgMIAAgCIAAgBQgTjMgEkdIABgGIACAKIAaBwIAAADQAtEFgeB0QgFAJgFABgADTGCIgBgBQgDgBgGgHIgBgCQghhyAgkXIAVhlIAFgHIABAEIAAABQAGEbgLDNIAAACQgCAPgGACIgCAAgAkGDSIgCgEIgHgHIgCgEQgBgDAAgIIgBgsQAAgHADgFIADgFIADgEIAEgDQAIABgBAKQgIAEgBAFIABAtQAAAJADADIAIAIQgCAHgEABIgDABIgBAAgAEFDIQgEgBgCgGIAHgJQADgEABgIIgCgtQAAgFgJgEQgBgKAIgBIADADIADAEIAEAEQADAFAAAHIABAsIAAALIgCAEIgGAIIgDADIgCAAIgCAAgAi6gGIgti3IgCgIIgBgDQgBgEABgDIAAgBIACgDIABgBIABgBIAAAAQASgKASgHQC4hUC7BMQATAIAUAJIACAEIAAAAIAAABIACACIAAAFIgBAFIAAABIgBAFIgnC6IgBACQgDAGgHgBIgCABQAAgBgBAAIgBAAQimgyilA2IgBAAIgBABIgFABIgBAAIgBAAQgHAAgDgHgACqgdIABgBIADgRgAiGo1QAPgKARgHQAlgOA1gFQA1ABAqAQQANAEANAKIgNgDIgCAAQgUgFgTgEQgbgEgagBQgQAAgRACQgbABgbAGIgYAFIgDABQgLACgKAFg");
	this.shape_9.setTransform(-0.3,-4.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#008FFF").s().p("AiFHnIgDgCQgOgFAAgPIgBgDIABAAQADiegbjDIAAgCIAAgBQgDgQAJgHIAAAAIAFgDIABgBIABAAQCkg2CnAyIABABQAGAGgDAXQgMCtAHC1IAAAAQgBAPgLAHIgDACQhOARhMAAQhDAAhCgNgAgSA+Qg2ADgpAIQgrAKgIALQClgqCoAgQgbgPg3gFQgegDgiAAIgpABgAkrghIAAgBQALgMATgMQALgDAMAPIABACIAAABQABAGgFAIIAAAAIAAgCQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBIgBgBQg2AYAAAJQgJgRAPgNgAEWgiQgdgMgBAGIAAgLIAAAAIABgCQAEgOANAAIAGAAIAWASQAWAQgKASQAAgIgcgLgAhdnnIACgBIABAAQAGgDAFAAQAbgGAbgCIAhAAQAbAAAaAFIAKADIAEAAIACACQA6AmAwEmQi7hMi4BUQAmkuA5gkgAgWjSQgeABgdAHQgqALgpAVQCfg/ChA4QgngQgmgJQgegHgdgBIgVgBIgVABg");
	this.shape_10.setTransform(-0.9,-12.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#005ADB").s().p("AhMJYQgbgBgbgEIgBAAIgEgBQgTgEgRgNIgBAAQgZgVgWhjIgEgVIAAgGIgHhEQAEARAMABIABAAIABAAIAAAAQAGgBAEgJQAfh0gtkEIAAgEIgahxIABhRIAtC3QADAKAIgBIAAAAQgIAIADAPIAAACIAAAAQAaDDgDCfIAAAAIAAADQABAPAOAFIADACQCOAcCRghIADgBQALgHABgQIAAABQgHi2ALitQAEgWgGgGIAAAAQABgBgBABIADAAQAHAAACgGIACgCIAni6IABgFIAAAAIABgGIgBgFIgBgBIAAgBIgHioIgEhyIgBgBQgIg3hZgPQARAIAPAUQAnAuAfBvIAHCoIAAAAIgCgEQgUgKgTgHQgxkng5gmIgDgBIgDgBIgKgCQgbgFgZgBIgjABQgaABgbAGQgGABgFACIgBAAIgCACQg5AkgnEtQgRAIgSAJIAAABIgBABIgBABIABibQACgOADgOQAVhfAkgtQAMgQAOgJIAIgFIACgBQAJgGALgCIADAAIAZgGQAbgFAagCQARgCASABQAZABAbAEQATADATAGIADAAIANADQgNgKgNgFIAlAEIAGABQA4APASASIABAAQAOAMAFAcQAYCRAKDTIgOALIAAABIgHAIIgHAIIgBgEIgFAIIgVBkQghEXAiBzIAAABQAGAHAEACIABAAIABAAQAMgBAEgOQgCAqgEAiIABAFIgCANQgTBkgXAVIgBABQgQAMgRAFIgCAAIgCAAIg5AIIgBAAQggAEgbAAIghABQgdAAgcgCgAiwGnQgZAAADAYIgBADQALBPAaAeIABAAQASARASADIABABIABAAQBxAQB4gTIADAAQAVgGARgRIACgCQAggiAAhTIAAgDIAAgCQgCgMgNAAIgCAAQivATiogPIgBABgADTiOIgDhHgAiCgVQApgJA3gDQA5gDAwAFQA2AGAcAOQiogfilAqQAIgMAqgJgAhhkpQAcgHAegCQAVgCAVACQAdACAfAGQAmAJAnARQiig5ieBAQApgWAqgKg");
	this.shape_11.setTransform(0.8,-2.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bluecar, new cjs.Rectangle(-32.8,-64.3,63.9,131.6), null);


(lib.Bluecar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF0000").ss(0.5,1,1).p("ADxjRQgBALgEAMQgCAEgBADQgDAFgCAGIgVAuQAAAAgBABQgYAyh4BuQh0BrgzAYIgCACQglATgiAJQgCABgBAAQgGACgFACQgEABgDABQgNADgMAAAjHDMQAEgBADgBQAFgCAEgCQAUgJAMgLQAMgLgDgIQgDgHgRAAQgBAAgBAAQgQABgSAIQgVAIgLAMQgNALAEAIQADAIARgBQAJgBALgCg");
	this.shape.setTransform(124.5,134);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#171A39").ss(0.5,1,1).p("AEPi8QgBgCgBgBAEPi8IAAAAAEOi/QAAABABACAERiLQACgZgEgYAC9jkQAiBUgxBXQgBABgBABAALDBIABgBQBPhCBIhGQADgDACgCQBhhhAIhdAhlC/QACAAABgBQCwhWBeifAkRC9QBQArBcgpAjyETIAAAAQAYAHAaAAQBeABBthaAj0ESQACABAAAAAj0ESQACABAAAAAj0ERIAAABAEOi/QgPgugjgt");
	this.shape_1.setTransform(121.4,127);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#070A12").ss(0.5,1,1).p("Akik3QgEgIgFgIQARAPARAPQABAAABACQC+CjBpB4IACAEQCECUBuCZQAEAGAFAHQAHALAIALQgRgNgRgOQgNgJgNgMQgmgggrgnQgxgug2g5Akik3QASAdAUAdAgLAkQAMANAMAMQAWAYAVAVAgLAkQAAgBgBAAQiPiThhiN");
	this.shape_2.setTransform(55,104.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000511").ss(0.5,1,1).p("ACVjwQABACABABIAbApIBTB8QACABAAACQAAAEgFAEQAAAAAAAAQjfBjh/DYQgGAGgHgEIhwhfIgggcQAAAAAAAAACVjwQgDgEgDgDQgHgGgKgCIgBgBQgzgEg9AXQhGAbhCA7QhCA7glBGQgkBEABA/QgBACABADQABAJAHAHQABABADAC");
	this.shape_3.setTransform(52,53.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#132D53").ss(0.5,1,1).p("AgtkxQBdgGB8BVQB8BUACABQADACALAFQABABACAAAiyEyIAAAAIAAgBIAAAAAk3hAQgQBcBICEQBICEABABQABADACAIQAAABABAAQAAABAAAA");
	this.shape_4.setTransform(40.9,39.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(0.5,1,1).p("ABkBCQgTgXgVgYQgLgNgMgNQAAgBgBAAQiEieiDhvAgOAeIACADQCGCTCOB8QAGAFAGAGQAKAIAKAJQgLgSgMgTQgJgOgJgOQgcgqgjgtABkBCQAzA8ApA2AknlLQAJAHAIAGQAAAAABACQAZATAZAUAkKkiQgOgUgPgVAkKkiQABACABABQCPDLBrBy");
	this.shape_5.setTransform(102.6,61.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#010C21").ss(0.5,1,1).p("AhdEiQCGj0D7hnQABAAAAgBQAGgDgBgFQgBgCgBgCIiJjGQgEgHgFgFAkhCFQgGgIgBgHQAAgDgBgDQgEhJAmhNQAnhQBJhBQBIhABQgcQBEgYA8AIQAAABABAAQAIACAHAEQACACADACAkbCMQgBgBgBgCQgBAAgBgBQgBgCgBgBQAAAAAAAAAkbCMQABABABAAICtCWQAIAHAHgI");
	this.shape_6.setTransform(53.2,53.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B6E3FE").s().p("AC+irQjxCBiKDWQB7jmEAhxg");
	this.shape_7.setTransform(63.6,65.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#62B0F1","#D2E5FE"],[0,1],0.1,-0.9,-2,2.7).s().p("AhjhPQBRBOB2AkQgbAQgDAdQhlg+hEhhg");
	this.shape_8.setTransform(91,145.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#62B0F1","#FFFFFF"],[0.675,1],0,3.5,0,0,3.5,27.4).s().p("AioCaQDng8Bqj3QAAAUgXAyQgYAxglArQg3BBhNAqQhFAmgsAAIgIAAg");
	this.shape_9.setTransform(120.8,127.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F57205").s().p("AjWDOQgHgBgCgDQgEgEADgIQACgJAJgIQAIgHAJgEQAJgEAQgEIADAAQAQAAAEAHQADAIgMALQgMALgUAJIgJAEIgHACIgFAAIgEAAgACviEQgHgDACgRIABgCQAFgQAFgJQAEgIAIgIQAJgIAKgBQAIgCADAEQADADAAAHIgBAJIgCAGIgFAJQgKATgNALQgJAHgGAAIgFgBg");
	this.shape_10.setTransform(125.1,133.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F4FA0F").s().p("AjuDTQgDgIAMgMQAMgLAUgJQASgHAQgBQgQADgJAEQgJAEgIAHQgJAIgCAJQgDAIAEAEQACADAHABIAJAAIgVAEIgCAAQgPAAgDgHgAC/izQAKgTANgLQAMgLAIAFQAHADgCARQgCAJgEALIABgJQAAgGgDgDQgDgEgIABQgKACgJAIQgIAHgEAJQgFAIgFAQQACgPAKgSg");
	this.shape_11.setTransform(124.3,133);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#62B0F1","#D2E5FE"],[0,1],-1,0.3,2.8,-1.4).s().p("AhGhoQBaBNAzBrQgeABgSAYQgXh5hGhYg");
	this.shape_12.setTransform(140.7,100.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#1073B4","#B9DFF9"],[0,1],11.5,12.1,0,11.5,12.1,43.8).s().p("AjMEhIgIgDQgvgJgzhHQAoAoAtATIABAAIACACQAYAGAZABQBfABBthaIABgBQBOhCBIhGIAGgGQBghhAIhcQACgZgEgYIAAgBIgBgDQgOgugkgsQBCA6AEAvIABAJQAGA7gkBCQgjA+guAqIgBABIgKAJQg/BEhOA7IgIAIIgGAFQgsAohBAbQgyAVgtAAQgRAAgQgCg");
	this.shape_13.setTransform(119.4,127.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#217EBC","#053250"],[0,1],-8,-8.2,0,-8,-8.2,20.2).s().p("AAsBYQgugqg4g+IgDgEQg/hFgFg5QArAoAmAgQgHAoAlAuIAJALIAPASQA2A6BGAwQgtgTgpgog");
	this.shape_14.setTransform(83.9,139.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#57B1F4","#0D5A8C"],[0,1],-0.9,1.7,0,-0.9,1.7,5.9).s().p("AADAeQgTgNgJgPQgJgPABgIQABgIgBgHIAZAZIAqAsQgFAEgGAAQgIAAgMgHg");
	this.shape_15.setTransform(57.2,111.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#D1E9F1","#31A7F6"],[0,1],-6,-2.9,0,-6,-2.9,5.9).s().p("AhchdQAbgLAKgmQAbByB5CrQiHhtgyh/g");
	this.shape_16.setTransform(13.9,59.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#3D81DA","#164FBA"],[0,1],-0.2,3.7,0,-0.2,3.7,8.4).s().p("AAGArIgNgIQgJgGAAgeQAAgeADgOIAeASQgJAbgBASQgBASAHAKIgBAAIgGgDg");
	this.shape_17.setTransform(25.2,79.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.rf(["#57B1F4","#0D5A8C"],[0,1],0.3,-1.5,0,0.3,-1.5,5.2).s().p("AgLAJQABgRAJgcIANAIQABAGgGAXQgGAXgGANQgHgKABgSg");
	this.shape_18.setTransform(27.1,80.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#A6ABBF","#242B61"],[0,1],-4.9,7.7,34.7,-28.1).s().p("AguA4QgQgRAjg5QAbgxAagXIAbgYQghA5gGBEQgFA+AUAqQhIg3gDgEg");
	this.shape_19.setTransform(38.8,65.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.rf(["#3C4277","#1D1D38"],[0,0.988],-1.7,-1.8,0,-1.7,-1.8,4).s().p("AEKEtQgNgKgNgLQgmgggrgoQgxgtg2g5IgrgtIgYgZIgBgCQiPiShhiNIgmg6IgJgPIAiAdIACABQC+CkBpB5IACADQCECTBuCaIAJANIAPAWIgigbg");
	this.shape_20.setTransform(55,104.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.lf(["#3496F6","#ADCFEE"],[0,1],5.8,-6.5,-10.8,5.6).s().p("Ag7itQgXBKC4BSQhKBNgdByQiljsBrhvg");
	this.shape_21.setTransform(21.4,41.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.rf(["#2F9AE1","#0C408D"],[0,1],-4.5,-6.8,0,-4.5,-6.8,10.1).s().p("AB6C5IgNgIIgfgTQhCgsg4g/IgDgFQhFhVgQhIIgDgQQAFAIAHACQAMADAMgOQAMgPAGgWQADgOABgMQAVB9CLDNQgHgPgHgSIAAgBIAAAAIAAgBIAAAAIABADQAQAWAMAFIAmA6IgOgHgAhyheQAyB/CIBtQh6irgbhyQgKAngbAKg");
	this.shape_22.setTransform(16.1,59.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.rf(["#FCF4E0","#8CC4D0","#2891BD"],[0,0.831,1],-1.2,-0.6,0,-1.2,-0.6,5.9).s().p("AgNA4QgIgCgEgJIgEgLQgDgSAGgWIABgFIADgIIACgFQAEgJAFgIIADgDQAEgFAEgCQAHgGAIACQAKADAEAOIABAEQABAIAAAJQAAAMgDAMQgHAXgMAOQgJAMgKAAIgDAAg");
	this.shape_23.setTransform(5.1,42.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.lf(["#8A90AC","#242B61"],[0,1],-20.3,17.2,19.3,-18.6).s().p("Ah/DiIhxhfQCXj5D0hsIBUB8IABADQABAEgFAEIgBAAQjeBjh/DYQgDADgEAAQgDAAgDgBgAiAAEQgZAXgdAxQgjA6AQARQADAEBJA3QgTgqAFg+QAGhFAgg4g");
	this.shape_24.setTransform(54.2,56.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.lf(["#424657","#1D2338"],[0,1],-22.2,16.5,18.4,-20.2).s().p("AjEDBQgIgIgBgJIAAgFQAAg/AkhEQAlhGBBg7QBCg6BHgbQA8gXA0AEIAAAAQAKACAHAGIAHAIQgHgFgJgCIAAAAQg0gEg8AWQhIAchBA6QhBA7glBGQgkBEAAA/IAAAFQABAFADAFIgDgCg");
	this.shape_25.setTransform(46.3,46.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.lf(["#69718D","#2B3355"],[0,1],-24,14.7,16.6,-22.1).s().p("AjSCxIgBAAQgDgFgBgFIAAgFQAAg/AjhFQAmhGBBg6QBCg7BHgbQA8gXA0AEIAAABQAJABAHAFIABADIAbApQj0BsiXD5g");
	this.shape_26.setTransform(48.1,48.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#3496F6","#ADCFEE"],[0,1],0.6,2.1,3.9,-4.3).s().p("AA3AcQhPhShnA6QBmhcCZB8QgsgPgdAHg");
	this.shape_27.setTransform(41.5,13.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.rf(["#D1E9F1","#31A7F6"],[0,1],-3.2,-8,0,-3.2,-8,7.1).s().p("AiGhHQAogHANgaQB6A/BeCSQieiKhvgmg");
	this.shape_28.setTransform(62,15.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#204C90").s().p("AkxAXQAAgJgCgIIgBgDQgEgOgKgDQgIgCgIAGQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAHgGAHAAQAIAAAFAEQAEAEAEAFIALgsQgPBbBICFIBJCFIADALIAAABIABABIAAAAQAHATAHAOQiNjNgUh9gAlpAVQAbhsCKh8IACgCIARgPIABgCQB9htBogOIAAACIgGABQgJADgIAEQhUAWhqBYIgKAIIgDACIgQAOIgBACQh9ByghBdQgGAHgEAJIgCAFgAFPh3IgDgBIgOgHIh+hVQh9hVhcAGIAsgHQgFgEgDgGQgEgFABgIQABgGAGgGQAAAAABAAQAAgBAAAAQABAAAAABQABAAABAAQgHAIABAHQABALAPAFIADABIARAEQB6AhC/CgIgbgPg");
	this.shape_29.setTransform(38.9,38);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.rf(["#FCF4E0","#8CC4D0","#2891BD"],[0,0.831,1],0.1,-1,0,0.1,-1,6.1).s().p("AgUAdIgQgDIgEgBQgOgFgCgLQgBgIAHgGQADgEAFgEIAEgCQAHgFAKgDIAFgBIAJgCIAEgBQAXgEARAGQAHACAEADQAIAFABAIQACAKgQALQgPAKgXAEIgRABIgIAAg");
	this.shape_30.setTransform(45.9,5.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.rf(["#0A5BD3","#5C9BF8"],[0,1],-7.5,-8.6,0,-7.5,-8.6,37.7).s().p("Aj9DpQgThBAYg/QASgvArgxIACgCQBbhqB1hTQA2gnAygMQBCgSA+AZQAKAEgEARIgQgFQgFgDgGgBQgRgGgZAEIgDAAIgJACQhpAOh8BtIgBABIgSAPIgBACQiKB7gbBtIgCAJIgBAEQgGAXAEATIADALIADAQIgGABQgLAAgDgJg");
	this.shape_31.setTransform(26.2,24.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.rf(["#62B4EA","#055B94"],[0,1],-5.7,-2.5,0,-5.7,-2.5,67.7).s().p("AiVFEIAKAQQgMgGgQgWIgCgCIAAAAIABABIAAgBIgBAAIAAgBIgDgMIhJiEQhIiFAPhbIgLArQgEgFgEgEQgFgEgIABQgHgBgHAHQAAAAAAAAQAAAAAAABQAAAAAAAAQAAABAAAAQgEADgEAFIgCADQAhhdB9hzIABgBIAQgPIADgCIAKgIQBqhXBUgXIgDADQgGAEgCADQgBAAgBAAQAAAAgBAAQAAAAAAAAQgBAAAAAAQgFAGgBAHQgBAIADAFQADAGAFADIgsAIQBcgHB9BWIB+BUIAOAIIADABQAMALAKAUIgQgNIAdAqQgmgrgogXIgEgDQgHgIgHgBIgHgCQg6gJhFAYQhRAchHBBQhJA/gnBQQglBOADBJIAAAFQADALAIAJIABAAIACADIACACIACACQAHAcAdAoIgjgegAi6DEQAehyBKhNQi5hRAYhLQhrBvCkDsgACQjWQiZh8hmBdQBng7BPBSQAdgHAsAPIAAAAg");
	this.shape_32.setTransform(39.9,39);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#5A9BFA").s().p("AjeDEIAAgGQgEhJAlhOQAohPBIhAQBIhBBQgcQBFgXA7AIIAGACQAHABAHAIQgHgFgIgCIgBAAQg8gIhEAXQhQAchIBBQhJBAgnBPQgmBOAEBJIABAGQABAIAGAHQgJgJgCgKg");
	this.shape_33.setTransform(45.4,45.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#2F9AE1","#0C408D"],[0,1],-1.8,-1.2,0,-1.8,-1.2,13.1).s().p("ACCBvIgCgBQgKgVgMgLIAbAPQi+ifh6ghQAMABANgDQAYgEAPgKQAQgLgCgLQgBgIgIgFIAQAFQBHAXBNBNIAEADQA5A+AlBGQAJAQAHAQIAHAPIAFAOIgygogAiEheQBvAmCeCLQheiSh6hAQgNAagoAHg");
	this.shape_34.setTransform(61.8,18.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#161A38").s().p("AiBFDIgBgBQAMAAANgDIAHgDIALgDIADgCQAigJAlgTIABgBQAzgZB1hrQB3huAZgxIABgCIAUguIAGgKIADgHQADgNACgLQgCALgDANIgDAHIgGAKQgMAWgOAMQgPANgIgFQgJgEACgTIABgDQACgSAKgUQANgXAOgLQAOgNAJAEQAJAFgCATIABAAQgJBdhgBhIgGAFQhHBHhPBBIgBABQhsBZhdABIgCgBgAEyhsQgMALgKATQgLASgCAPIgBADQgCAQAHAEQAIAEAMgLQANgLALgTIAEgIIADgHQAEgLABgJQACgRgHgDIgFgBQgGAAgJAHgAiZE5QgDgKAOgMQANgOAYgJQAVgJASAAIACAAQAUAAADAIQAEAKgNANQgNAMgVAJIgDACIgLADIgHADQgNADgMAAIgCAAQgRAAgEgJgAhoEQQgVAJgMALQgMAMADAIQADAHARAAIAVgEIAHgCIAJgDQAUgJAMgLQAMgMgDgIQgEgHgQABIgDAAQgQABgRAHgAiCFCIAAAAgAjSDmQg0gfhJhWIAjAbIgPgWQBCBMAwAcQBPArBUghQgtAUgqAAQgsAAgpgWgADzizQgYgzhEhJIATARIgWglQBNBRAaA3QAiBUgwBXIgCACQAqhRgihUg");
	this.shape_35.setTransform(115.1,122.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.rf(["#217EBC","#053250"],[0,1],-6.1,-6.6,0,-6.1,-6.6,19.3).s().p("AAoAKIgRgRIgJgKQgqgpgpACQgcgpgjguQA3AKA/BGIADAEQA5A9AlAzQAjAtAPAtIAAABQgphLg0g7g");
	this.shape_36.setTransform(135.1,93.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#6DB5F2","#217EBC"],[0,1],14.8,14.3,0,14.8,14.3,46.2).s().p("AimFLIAAgBIgCgBIAAAAQhGgwg2g6IgQgSIgJgLQglgvAHgoQANALANAKQBIBVA0AgQBQAqBcgoIADgCQCvhWBfifIACgBQAxhYgihTQgag3hOhRIgSgcQApgCArApIAJAKIARASQA0A6AoBMIACACIAAABQAEAYgCAZIgBAAQACgTgJgFQgJgFgOANQgOAMgMAWQgLAVgCARIgBADQgCATAJAEQAJAEAPgLQAOgMAMgWIgVAtIgBABQgYAyh4BvQh1BrgzAYIgBACQglATgiAJQAVgJANgNQANgNgEgJQgDgJgTAAIgDAAQgSABgVAIQgXAJgOAOQgOANAEAJQADAKATgBIABACQgagBgYgGgAidFFQADgdAbgRQh3gkhRhPQBEBjBmA+gAEkhkQATgYAdgBQgzhshbhNQBGBYAYB6g");
	this.shape_37.setTransform(113.8,121.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#252B52").s().p("AhvD+IibirQgKgMAMgIQDLhuCWjTQAIgHAHAHICfCwQAMALgGALQhsECjuA9IgIACIgBAAIgHABQgKAAgIgIgABbj5QiVDNjFBuQgMAIAKALICSChQAJAKAOgDIABAAQDug9Bvj+QAEgKgKgLIiXimQgDgDgDAAQgEAAgEADg");
	this.shape_38.setTransform(110.8,116.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.rf(["#68738A","#304778"],[0,1],-7.6,16.6,0,-7.6,16.6,38.1).s().p("AjMBQQgGgJAKgIQCzhuCSi4QAHgGAFAEIBGBOQgzFUjZAyg");
	this.shape_39.setTransform(106.9,117.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#778DE6","#06061A"],[0,1],-11.1,-5.1,20.5,12.4).s().p("AhwD2IiRihQgKgLAMgIQDFhuCVjNQAHgHAHAHICWCmQALALgFAKQhvD+jtA9IgBAAIgHABQgKAAgHgIgABWjhQiTC5izBuQgKAIAHAIICOCcQDZgzA0lUIhHhNQAAgBgBAAQAAgBgBAAQAAAAgBAAQgBAAAAAAQgDAAgEADg");
	this.shape_40.setTransform(110.8,116.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#3AA3E7").s().p("AhvEFIiiizQgKgMAMgJQDQhvCYjYQAHgHAIAHICnC5QAMAMgFALIgHAQQhrD2jmA8IgSAFIgCAAIgHABQgKAAgIgJgABbkBQiWDTjMBuQgLAIAKAMICaCrQAKALAPgEIABAAIAIgCQDvg9BskCQAFgLgLgLIigiwQgEgDgDAAQgEAAgDADg");
	this.shape_41.setTransform(110.6,116.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.lf(["#6FBBEE","#148BDA"],[0,0.988],-15.7,-18.6,17.9,20.4).s().p("AgUFDQh7heiji9IAAgBQgfgjAPgdIADgFIABgCIAOgSQBiiIDWiLIAIgDQAYgJAUASQCVCdBrCiIABAAQAUAhgPAeIgEAHQiNDBiVA/QgLAGgLAAQgNAAgNgJg");
	this.shape_42.setTransform(81.5,86.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.rf(["#57B1F4","#0D5A8C"],[0,1],1.5,-1.2,0,1.5,-1.2,5.5).s().p("AgLgJIgWgbQAHACAIgBQAIAAAOAKQAOALALAVQAKAVgKAJIgogug");
	this.shape_43.setTransform(109.7,64);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.rf(["#414883","#1D1D38"],[0,0.988],-2.4,-2.1,0,-2.4,-2.1,4.4).s().p("AEUE7IgMgLQiOh8iGiTIgCgDQhrhyiPjKIgCgDIgdgqIARANIABACIAyAnQCDBvCECeIABABIAXAaIAoAvQAzA8ApA2QAjAtAcApIASAdIAXAlIgUgRg");
	this.shape_44.setTransform(102.6,61.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.lf(["#1F9FF5","#0A65A0"],[0,1],-33.4,-36,40,44.6).s().p("AB4JKQgwgchChMIgIgNQhuiZiEiVIgCgDQhqh4i+ijIgCgCQgdgngHgcIACABICtCWQAIAHAHgIQCHj0D7hoIABgBQAFgDgBgFIgBgEIiJjGQgEgHgFgFQAoAXAlAqIACADQCODLBrBzIACADQCHCTCOB6IAMALQBEBJAYAzQAiBUgqBRQheCfixBXIgDABQgkAOgjAAQgvAAgtgYgAFuAgQiZDYjQBwQgLAJAJALICiC0QALALAPgDIABgBIASgEQAtAEBNgqQBNgqA3hBQAmgsAXgxQAXgyAAgUIAHgQQAFgLgMgMIini4QgDgEgEAAQgEAAgEAEgAAClMIgGADQjXCLhjCIIgNASIgBACIgEAFQgOAdAeAjIAAABQCkC9B6BeQAZAPAXgMQCWg/CNjBIAEgHQAOgegUggIgBgBQhqiiiWidQgNgMgQAAQgHAAgIADgAl+ghQCKjXDyiBQkBBxh7Dng");
	this.shape_45.setTransform(82.9,86.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#010D31","#022770"],[0,1],-14.4,-16.1,0,-14.4,-16.1,69.8).s().p("AhsEjIitiWIgCgBIgCgDIgCgBIgCgDIAAAAQgGgIgBgHIgBgGQgEhJAmhNQAnhQBJhBQBIhABQgcQBEgYA8AIIABABQAIACAHAEIAFAEQAFAFAEAHICJDGIACAEQABAFgGADIgBABQj7BniGD0QgEAEgEAAQgDAAgEgDgAABjxQhGAbhCA6QhCA7glBHQgkBDABA/IAAAFQABAJAHAIIAEACIAAABIAgAbIBwBfQAHAFAGgGQB/jZDfhjIAAAAQAFgDAAgFIgCgDIhTh8IgbgpIgCgCIgGgIQgHgGgKgCIgBAAIgPgBQgtAAg0AUg");
	this.shape_46.setTransform(53.2,53.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.rf(["#57B1F4","#0D5A8C"],[0,1],-1.4,0.2,0,-1.4,0.2,5.1).s().p("AgdAKIgHgOQAdgGARABQASAAAJAIQgOAFgXAEQgPADgHAAIgHgBg");
	this.shape_47.setTransform(82.3,30.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.rf(["#3D81DA","#164FBA"],[0,1],3,0.2,0,3,0.2,7.7).s().p("AAsASQgJgIgRAAQgSgBgcAGQgHgPgJgQQAPgCAdADQAfACAEAKIAIAOIACAHg");
	this.shape_48.setTransform(81.5,28.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Bluecar, new cjs.Rectangle(0,0,150.6,157), null);


(lib.answercopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ah4DOQgbAAgLgMQgMgNAAgaIAAk1QAAgbAMgMQALgMAbAAICFAAQAdAAAWAEQAWADARALQAOAJAMANQAKANAHARQAFAQAAARQAAA+g+AdQBSAZAABKQAAAjgSAcQgSAbgeANQgSAIgZAEQgZADggAAgAhXCPIBZAAQBUAAAAg9QAAgfgVgOQgXgOgqAAIhXAAgAhXgkIBMAAQAfAAARgGQARgGAIgQQAIgMAAgPQAAgfgWgKQgXgKgsAAIhEAAg");
	this.shape.setTransform(0.9,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00223A").s().p("AiRGQQhpAAhLhKQhKhLAAhpIAAkjQAAhpBKhLQBLhKBpAAIEjAAQBpAABLBKQBKBLAABpIAAEjQAABphKBLQhLBKhpAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BDE4FF").s().p("AiYGjQhvAAhNhOQhOhNAAhvIAAkxQAAhvBOhOQBNhNBvAAIExAAQBvAABOBNQBNBOAABvIAAExQAABvhNBNQhOBOhvAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AmzGzIAAtlINnAAIAANlg");
	this.shape_3.setTransform(0.4,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.answercopy5, new cjs.Rectangle(-43.2,-43.3,87.3,87.1), null);


(lib.answercopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeDIQgMgMAAgaIAAiDIhkiaQgNgVgGgMQgFgMAAgJQAAgOALgKQALgLAQAAQARAAAJAKQAJAKASAcIBMB9IBNh9IAMgTIALgPQAGgHAHgDQAHgEAKAAQAPAAALALQAKAKAAANQAAALgFALQgFALgNATIhnCdIAACDQAAAagMAMQgMANgRAAQgSAAgMgNg");
	this.shape.setTransform(0,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00223A").s().p("AiRGQQhpAAhLhKQhKhLAAhpIAAkjQAAhpBKhLQBLhKBpAAIEjAAQBpAABLBKQBKBLAABpIAAEjQAABphKBLQhLBKhpAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BDE4FF").s().p("AiYGjQhvAAhNhOQhOhNAAhvIAAkxQAAhvBOhOQBNhNBvAAIExAAQBvAABOBNQBNBOAABvIAAExQAABvhNBNQhOBOhvAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AmzGzIAAtlINnAAIAANlg");
	this.shape_3.setTransform(0.4,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.answercopy4, new cjs.Rectangle(-43.2,-43.3,87.3,87.1), null);


(lib.answercopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhIDGQgngOgbgcQgcgcgPgoQgPgnAAgxQAAgvAPgoQAOgoAcgcQAcgbAngPQAogPAyAAQAqAAAfALQAhALATARQAVARAJATQALATAAAOQAAAQgMALQgMALgQAAQgJAAgIgEQgIgEgGgIQgQgYgKgMQgLgNgSgIQgSgJgcAAQgcAAgXALQgWAJgRATQgQATgIAcQgJAbAAAiQAABHAhAnQAiAnA6AAQAdAAAagHQAZgIAagOIAAhDIhBAAQgXAAgMgHQgLgHgBgRQAAgNAKgIQAKgJARAAIBfAAQASAAALADQAMADAIAJQAHALABAVIAABPQgBAQgCAKQgEAJgIAHQgJAIgNAHQglAUgiAKQgiAJgpAAQgvAAgngPg");
	this.shape.setTransform(-0.3,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00223A").s().p("AiRGQQhpAAhLhKQhKhLAAhpIAAkjQAAhpBKhLQBLhKBpAAIEjAAQBpAABLBKQBKBLAABpIAAEjQAABphKBLQhLBKhpAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BDE4FF").s().p("AiYGjQhvAAhNhOQhOhNAAhvIAAkxQAAhvBOhOQBNhNBvAAIExAAQBvAABOBNQBNBOAABvIAAExQAABvhNBNQhOBOhvAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AmzGzIAAtlINnAAIAANlg");
	this.shape_3.setTransform(0.4,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.answercopy3, new cjs.Rectangle(-43.2,-43.3,87.3,87.1), null);


(lib.answercopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABrDLQgJgHgHgKIgTgeIghg3QgSgfgOgQQgNgQgPgGQgPgGgWAAIgdAAIAACEQAAAbgMAMQgLAMgTAAQgTAAgLgNQgLgNAAgZIAAk7QAAgbAMgMQAMgMAbAAICGAAQAcAAAUACQAUACAQAIQAUAIAPAPQAOAPAIAUQAIAUAAAXQAAAugaAbQgaAbg1ALQAWAMAVAXQAUAXAQAaQAPAaAJAVQAJAVAAAIQAAAIgFAIQgFAIgJAFQgJAEgMAAQgOAAgKgGgAhXghIBLAAQAdAAAUgFQAVgFALgMQAKgNAAgWQAAgRgIgNQgJgNgQgHQgOgGgqAAIhNAAg");
	this.shape.setTransform(0.9,0.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00223A").s().p("AiRGQQhpAAhLhKQhKhLAAhpIAAkjQAAhpBKhLQBLhKBpAAIEjAAQBpAABLBKQBKBLAABpIAAEjQAABphKBLQhLBKhpAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BDE4FF").s().p("AiYGjQhvAAhNhOQhOhNAAhvIAAkxQAAhvBOhOQBNhNBvAAIExAAQBvAABOBNQBNBOAABvIAAExQAABvhNBNQhOBOhvAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("AmzGzIAAtlINnAAIAANlg");
	this.shape_3.setTransform(0.4,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.answercopy2, new cjs.Rectangle(-43.2,-43.3,87.3,87.1), null);


(lib.yellowcarcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.yellowcar_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0.5,0.72,0.72,-135,0,0,77.2,76.7);
	this.instance.filters = [new cjs.ColorFilter(1, 1, 1, 1, 30, 10, -108, 0)];
	this.instance.cache(-2,-2,158,157);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.353)").s().p("AhBKHIgJAAIgGAAQgugBgzgQQg2gUgbgkIgEgFQgWgdAGhCQgFgwABhBIAAgEQgChHAbgjQgFgzgDg7QgKgBgFgSQgFgRACgNQAEgOAEgDIAIgJIAAgCQgGicAQiBIgEgMQgDAFgPAKQgPAKgLAEIAAgBIgDgEIgDgMQgDgIAQgRIAZgZQgPg6gBhBIABgEQAEhTAcgyQgLgGAEgIQAWgvAugYQAhgSAygGIACAAQBrgPBsANQAzAGAiARQAvAXAXAuQAEAIgLAHQAcAxAGBUIAAAEQABBBgOA7QAIAHARAQQAPASgBAHIgEAMIgCAFIgBAAQgLgDgPgKQgQgKgCgEIgEALQATCBgFCdIABABQACAFAFAEQAFADADAOQAEANgGASQgEARgLAAQgCA8gEAzQAbAiAABIIAAAEQABBBgFAwQAIBCgVAdIgEAGQgbAkg2AUQg0ASgwABIgBAAIgKAAQglADgmAAQghAAgkgCgAkCIpIAAgDIgBABIABACIAAAAgAEOIlIABgCIgBAAIAAACgAjbpAIAFgCIgBAAIgEACgADZpBIgGgEIAAAAIAGAEIAAAAg");
	this.shape.setTransform(0.4,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellowcarcopy2, new cjs.Rectangle(-78.3,-77.8,156.7,156.7), null);


(lib.yellowcarcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.yellowcar_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0.5,0.72,0.72,-135,0,0,77.2,76.7);
	this.instance.filters = [new cjs.ColorFilter(1, 1, 1, 1, 30, 10, -108, 0)];
	this.instance.cache(-2,-2,158,157);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.353)").s().p("AhBKHIgJAAIgGAAQgugBgzgQQg2gUgbgkIgEgFQgWgdAGhCQgFgwABhBIAAgEQgChHAbgjQgFgzgDg7QgKgBgFgSQgFgRACgNQAEgOAEgDIAIgJIAAgCQgGicAQiBIgEgMQgDAFgPAKQgPAKgLAEIAAgBIgDgEIgDgMQgDgIAQgRIAZgZQgPg6gBhBIABgEQAEhTAcgyQgLgGAEgIQAWgvAugYQAhgSAygGIACAAQBrgPBsANQAzAGAiARQAvAXAXAuQAEAIgLAHQAcAxAGBUIAAAEQABBBgOA7QAIAHARAQQAPASgBAHIgEAMIgCAFIgBAAQgLgDgPgKQgQgKgCgEIgEALQATCBgFCdIABABQACAFAFAEQAFADADAOQAEANgGASQgEARgLAAQgCA8gEAzQAbAiAABIIAAAEQABBBgFAwQAIBCgVAdIgEAGQgbAkg2AUQg0ASgwABIgBAAIgKAAQglADgmAAQghAAgkgCgAkCIpIAAgDIgBABIABACIAAAAgAEOIlIABgCIgBAAIAAACgAjbpAIAFgCIgBAAIgEACgADZpBIgGgEIAAAAIAGAEIAAAAg");
	this.shape.setTransform(0.4,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellowcarcopy, new cjs.Rectangle(-78.3,-77.8,156.7,156.7), null);


(lib.yellowcar_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.yellowcar_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0.5,0.72,0.72,-135,0,0,77.2,76.7);
	this.instance.filters = [new cjs.ColorFilter(1, 1, 1, 1, 30, 10, -108, 0)];
	this.instance.cache(-2,-2,158,157);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(0,0,0,0.353)").s().p("AhBKHIgJAAIgGAAQgugBgzgQQg2gUgbgkIgEgFQgWgdAGhCQgFgwABhBIAAgEQgChHAbgjQgFgzgDg7QgKgBgFgSQgFgRACgNQAEgOAEgDIAIgJIAAgCQgGicAQiBIgEgMQgDAFgPAKQgPAKgLAEIAAgBIgDgEIgDgMQgDgIAQgRIAZgZQgPg6gBhBIABgEQAEhTAcgyQgLgGAEgIQAWgvAugYQAhgSAygGIACAAQBrgPBsANQAzAGAiARQAvAXAXAuQAEAIgLAHQAcAxAGBUIAAAEQABBBgOA7QAIAHARAQQAPASgBAHIgEAMIgCAFIgBAAQgLgDgPgKQgQgKgCgEIgEALQATCBgFCdIABABQACAFAFAEQAFADADAOQAEANgGASQgEARgLAAQgCA8gEAzQAbAiAABIIAAAEQABBBgFAwQAIBCgVAdIgEAGQgbAkg2AUQg0ASgwABIgBAAIgKAAQglADgmAAQghAAgkgCgAkCIpIAAgDIgBABIABACIAAAAgAEOIlIABgCIgBAAIAAACgAjbpAIAFgCIgBAAIgEACgADZpBIgGgEIAAAAIAGAEIAAAAg");
	this.shape_17.setTransform(0.4,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellowcar_2, new cjs.Rectangle(-78.3,-77.8,156.7,156.7), null);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("AghCnQgMgEgMgFQg/gfgZhBQgYhCAfg/QAdhABCgYQBBgZBAAfQAuAUAZAo");
	this.shape.setTransform(-140.8,40.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AgqD8QgYgGgYgMQhfgsgkhkQgkhiAshgQAthfBjgkQARgGAPgDQA4gOA2AOQAaAFAZAMQBMAjAnBG");
	this.shape_1.setTransform(-141.1,40.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AgIk/QAIgHAKgGQAMgGAMgGQBYgkBsAPQBuARCNFuQhbC1hYAXQhuAcg2AEQgUABgZAMQgYAKg4ArQg2ArgjAnQgjAlgRgMQg2gnABhJQACg/Amg2QAng4AwgQQAPgGAhgKQAQgFAQgFQhxhahygvQgdgMgagSQgSgJgRgNQgigZggghQgZgZg7hLQg6hMAchGQAehFBaBAQBbBABCA/QADADADACQAaAYAZASQAKAIAMAHQADACAEADQAfASAgALQBJAaAOACAhtjcQAJgPAMgPQAVgZAagUQAPgMASgLIAAgBQBJgSBeBI");
	this.shape_2.setTransform(-97.4,76.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AiAGrQg2gnABhJQACg/Amg2QAng4AwgQQAPgGAhgKIAggKQhxhahygvQgdgMgagSQgSgJgRgNQgigZggghQgZgZg7hLQg6hMAchGQAehFBaBAQBbBABCA/IAGAFQAaAYAZASQAKAIAMAHIAHAFQAfASAgALQBJAaAOACQgOgChJgaQgggLgfgSIgHgFIAGgLQAJgPAMgPQAVgZAagUQAPgMASgLIAAgBQAIgHAKgGIAYgMQBYgkBsAPQBuARCNFuQhbC1hYAXQhuAcg2AEQgUABgZAMQgYAKg4ArQg2ArgjAnQgaAcgRAAQgFAAgEgDgACfkJQhLg5g+gBIAAAAIAAAAQgQAAgOAEQAOgEAQAAIAAAAIAAAAQA+ABBLA5IAAAAg");
	this.shape_3.setTransform(-97.4,76.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-141.2,39.8,0.797,0.797,0,0,0,0.3,-0.1);
	this.instance.alpha = 0.852;
	this.instance.filters = [new cjs.BlurFilter(53, 53, 1)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-208.3,-27,160.5,147.7);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.ans3 = new lib.answercopy5();
	this.ans3.name = "ans3";
	this.ans3.parent = this;
	this.ans3.setTransform(315.1,-0.3,1.35,1.35);

	this.ans3_1 = new lib.answercopy4();
	this.ans3_1.name = "ans3_1";
	this.ans3_1.parent = this;
	this.ans3_1.setTransform(104.7,-0.3,1.35,1.35);

	this.ans3_2 = new lib.answercopy3();
	this.ans3_2.name = "ans3_2";
	this.ans3_2.parent = this;
	this.ans3_2.setTransform(-105.8,-0.3,1.35,1.35);

	this.ans3_3 = new lib.answercopy2();
	this.ans3_3.name = "ans3_3";
	this.ans3_3.parent = this;
	this.ans3_3.setTransform(-316.2,-0.3,1.35,1.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ans3_3},{t:this.ans3_2},{t:this.ans3_1},{t:this.ans3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-374.5,-58.7,749.1,117.5);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(6.6,-8.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.95,scaleY:0.95,x:6.4,y:25.6},15).to({scaleX:1,scaleY:1,x:6.6,y:-8.1},19).wait(1));

	// Layer_3
	this.instance_1 = new lib.Tween10("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,35.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.95,scaleY:0.95,y:67.3},15).to({scaleX:1,scaleY:1,y:35.9},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122,-127.1,247.9,326.2);


(lib.redcar_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.redcar_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0.5,0.719,0.719,-42.9,0,0,75.1,77.5);
	this.instance.filters = [new cjs.ColorFilter(1, 1, 1, 1, 35, 0, -31, 0)];
	this.instance.cache(-2,-2,155,160);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.353)").s().p("AhgKHIAAABQhVgJgigjQgRgSgEgYIgHg+QgnhQAWiBIgCiTIgGgHQAAAAgBgBQAAAAAAgBQAAAAgBgBQAAgBAAAAQgBgEAAgIIAAgrQAAgHADgFIADgEIAEgEIgBjaIgJAFQgYANgHgCQgJgSAPgMIAAgBQALgLAUgMQALgDALAPIABACIAAAAIAAgEIgRgSIACh+QgNhjAahHIABgDIAAgCQAHhaAlgMIABAAQAdgNAjgGIAFgCIArgEQAmgOAzgDQA2ABAqARQATABARADIAGACQAkAFAcAMIABAAQAkANAHBbIABACIABADQAZBIgNBiIACCGIAGAAIAWASQAVAQgLATQgNABgZgMIAADWIAEAEIADAEQADAFAAAHIAAArQAAAJgBACIgCAFIgGAHIgDCTQAXCAgoBQIgIA/QgEAcgWATQgjAehOAIIgBAAQgwAEgwAAQgxAAgxgFgAivgeIABgCIgBAAIAAACgACpg4IAAgBIgCgBIACACgAiog6IgBABIAAABIABAAIAEgDIgEABgACuhRIAAAAIAEgRgAj4inIABgBQAAAAAAAAQAAAAAAAAQAAAAABgBQAAAAAAAAIAAgCIgBgEg");
	this.shape.setTransform(-0.3,3.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.redcar_2, new cjs.Rectangle(-77.5,-77.1,154.9,155.1), null);


(lib.redcar_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance_1 = new lib.redcar();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0.5,0.719,0.719,-42.9,0,0,75.1,77.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.redcar_3, new cjs.Rectangle(-77.5,-77.1,154.9,155.1), null);


(lib.bluecar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bluecar();
	this.instance.parent = this;
	this.instance.setTransform(0,0.5,0.72,0.72,41,0,0,75.2,78.5);
	this.instance.filters = [new cjs.ColorFilter(1, 1, 1, 1, -255, -31, 40, 0)];
	this.instance.cache(-2,-2,155,161);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(0,0,0,0.353)").s().p("Ag2KIIgJAAIgGAAQguAAgzgQQg3gSgcglIgDgFQgXgcAGhDQgGgwAAhAIgBgEQgBhIAZgiIgIhvQgKgBgGgRQgFgRACgNQAEgOAEgEQAFgEACgFIAAgBQgIidAPiBIgEgLQgDAEgPAKQgPALgLADIAAABIgCgFIgEgMQgDgIAPgRQAQgSAJgHQgQg6gBhBIABgEQADhUAbgyQgLgGAEgIQAVgvAugZQAhgSAxgHIADgBQBqgPBtALQAyAFAjARQAvAXAXAuQAEAIgLAHQAdAxAHBUIAAADQACBCgNA7QAJAGAQARQAQAQgCAJIgDALIgCAGIgBAAQgLgDgQgKQgPgKgCgFIgEAMQAUCBgCCcIAAABQADAFAFAEQAFADADAOQADAOgEARQgGARgKABQgBA8gDAzQAbAiABBIIAAADQACBBgEAwQAJBCgVAeIgEAFQgbAmg2ATQg0ATgvACIgCAAIgKAAQgsAEgtAAIg2gBgAEAI/IgBgBIAAABIABAAgAj5IsIABAAIgBgCIAAACgAEXIiIABgCIAAgBIgBADgACfo5IAAgBIAAAAIAAABIAAAAgAjho7IAGgEIgBAAIgFAEgADUpEIgGgEIAAABIAGADIAAAAg");
	this.shape_12.setTransform(0.2,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bluecar_1, new cjs.Rectangle(-77.9,-77.7,156,156.4), null);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Wow
	this.instance = new lib.Symbol6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(637.9,346.4,1.111,1.111);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(173).to({_off:false},0).wait(59));

	// hand
	this.instance_1 = new lib.Tween8("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(488,585.8,1.111,1.111);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(120).to({_off:false},0).to({alpha:1},7).to({regX:0.1,regY:0.1,scaleX:1.23,scaleY:1.23,x:512,y:574.2},17).to({regX:0,regY:0,scaleX:1.11,scaleY:1.11,x:488,y:585.8},9).to({startPosition:0},6).to({_off:true},14).wait(59));

	// arrow
	this.instance_2 = new lib.Tween7("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(626,388.1,1.111,1.111,0.3,0,0,-5,-224.6);
	this.instance_2.alpha = 0.699;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(83).to({_off:false},0).to({y:418.1,alpha:1},14).to({y:388.1},9).to({y:418.1,alpha:0.699},13).to({_off:true},1).wait(112));

	// Layer_1
	this.instance_3 = new lib.Tween1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(640,647.3);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(63).to({_off:false},0).to({alpha:1},5).to({startPosition:0},101).to({alpha:0},3).to({_off:true},1).wait(59));

	// Layer_6
	this.question = new lib.Question_mc();
	this.question.name = "question";
	this.question.parent = this;
	this.question.setTransform(642.3,335.7,0.946,0.946,0,0,0,0.2,0.3);
	this.question.alpha = 0;
	this.question._off = true;

	this.timeline.addTween(cjs.Tween.get(this.question).wait(37).to({_off:false},0).to({alpha:1},4).wait(128).to({alpha:0},3).to({_off:true},1).wait(59));

	// Layer_5
	this.instance_4 = new lib.qtxt();
	this.instance_4.parent = this;
	this.instance_4.setTransform(640,116.8,1.862,1.862);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(15).to({_off:false},0).to({alpha:1},4).wait(150).to({alpha:0},3).to({_off:true},1).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(593.8,320.8,1370,766.3);
// library properties:
lib.properties = {
	id: '9B187A3141F2F044B0356886562D636B',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#333333",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9B187A3141F2F044B0356886562D636B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;