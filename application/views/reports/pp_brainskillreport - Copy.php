<div id="main">
<?php //echo "<pre>";

//print_r($query); ?>
  
    <h2>Puzzle Performance Brain Skills</h2>
     <form class="cmxform" method="POST" id="commentForm" accept-charset="utf-8"> 
	 <input type="hidden" name="planid" value="<?php echo $query[0]['gp_id']; ?>">
	 <table align="center" border="0" cellpadding="0" cellspacing="0"> 
	 <tr>
	 <td class="selectGame">Select Puzzle : </td>
	 <td> 
	 <select name="game" id="game1">
	 <option value="">Select Puzzle </option>
	 <?php foreach($query1 as $gamename)

	 { ?>
		 
	<option value="<?php echo $gamename->gid; ?>"> <?php echo $gamename->gname; ?></option>
		  		  		 
	<?php } ?>
		 
	 </select>	 
	 </td>
	 </tr>	 
	 </table>
	 </form> 
  

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto; display:none;"></div>
<script type="text/javascript">

$(document).ready(function(){
	 
$('#game1').change(function(){ 
var form=$("#commentForm");
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/reports/brainskill_report_ajax') ?>",
data:form.serialize(),
dataType: 'json',
success:function(result)
{
alert(result);
if(result!='')
{
	var val = parseFloat($.trim(result.game_score));
	var gamedate = $.trim(result.lastupdate);
	//window.location.href= "index.php/home/dashboard";
	  Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Puzzle Performance Report'
        },
        xAxis: {
            categories: [gamedate]
        },
        credits: {
            enabled: false
        },
        series: [{
           
            data: [val]
        }]
    });
	$('#container').show();
}
}
});

	});
	});


$(function () {
   
});
</script>
<center>  
<b>X-Axis :</b>  Date Played <br> 
<b>Y-Axis:</b> Scored Points   
</center>
</div>
</div>

    
    
    
    
    
    
    
    

