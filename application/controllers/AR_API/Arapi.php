<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arapi extends CI_Controller {
var $info;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		
		// Call the CI_Model constructor
		parent::__construct();
		//$this->lang->load("menu_lang","english");
		$this->load->model('Arapi_model');
		$this->load->library('session');	
		$this->load->library('My_PHPMailer');
		$this->info=array("secret_key"=>"Ed6S@2018","default_pwd"=>"skillangels","default_play_time"=>$this->config->item('game_limit')); 
		//$this->lang->load("menu_lang","french");		
		
    }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}
	
	public function checklogin()
	{
		$username = $this->input->post('UserId');
		$password = $this->input->post('Password');
		$deviceid = $this->input->post('DeviceId');
		$Key = $this->input->post('Key');
		$timestamp = $this->input->post('ts');
		
		if(($username!='') && ($password!='') && ($deviceid!='') && ($Key!='') && ($timestamp!=''))
		{			 
			$token=$this->encMethod($timestamp,$deviceid,$this->info['secret_key']); 
			 
			if($Key==$token['Hash'])
			//if(1==1)
			{
				/* $isparent= $this->Arapi_model->typeofuser($username,$password);
				
				if($isparent[0]->type==1)
				{
					$data['query'] = $this->Arapi_model->checkParentLogin($username,$password,$deviceid);
				}
				else
				{
					$data['query'] = $this->Arapi_model->checkChildLogin($username,$password,$deviceid);
				} */
				
				$data['query'] = $this->Arapi_model->checkParentLogin($username,$password,$deviceid);
				
				if(isset($data['query'][0]->id))
				{
					/* if($isparent[0]->type=='1')
					{ */ // PARENT
						
						$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);
						$this->Arapi_model->update_parent_loginDetails($data['query'][0]->id,$uniqueId);
						
						if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
							 $ip=$_SERVER['HTTP_CLIENT_IP'];}
							 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
							 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
							 $ip=$_SERVER['REMOTE_ADDR'];}
						 
						$this->Arapi_model->insert_parent_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
						
						$data['query1'] = $this->Arapi_model->updateParentDeviceID($username,$deviceid,'');
						
						$resparr=array("status"=>"success","UserId"=>$data['query'][0]->id,"Username"=>$data['query'][0]->username,"Mobileno"=>$data['query'][0]->mobileno,"portal_type"=>$data['query'][0]->portal_type,"startdate"=>'-',"enddate"=>'-',"ParentID"=>$data['query'][0]->id,"msg"=>"User Login successfully");
						echo json_encode($resparr);exit;
					/* }
					else
					{ // CHILD
						
						if(($data['query'][0]->deviceid!='') && ($data['query'][0]->deviceid!=$deviceid))
						{
							$resparr=array("status"=>"Failed","UserId"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"msg"=>"User Already Login in some Devices");
							echo json_encode($resparr);exit;
						}
						$uniqueId = $data['query'][0]->id."".date("YmdHis")."".round(microtime(true) * 10);
						$this->Arapi_model->update_loginDetails($data['query'][0]->id,$uniqueId);
						
						if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
							 $ip=$_SERVER['HTTP_CLIENT_IP'];}
							 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
							 $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];} else {
							 $ip=$_SERVER['REMOTE_ADDR'];}
							 
							$this->Arapi_model->insert_login_log($data['query'][0]->id,$uniqueId,$ip,$this->input->post('txcountry'),$this->input->post('txregion'),$this->input->post('txcity'),$this->input->post('txisp'),$_SERVER['HTTP_USER_AGENT'],1);
							
							$data['query1'] = $this->Arapi_model->updateDeviceID($username,$deviceid,'');
							
							$resparr=array("status"=>"success","UserId"=>$data['query'][0]->id,"portal_type"=>$data['query'][0]->portal_type,"startdate"=>$data['query'][0]->startdate,"enddate"=>$data['query'][0]->enddate,"ParentID"=>$data['query'][0]->parent_id,"msg"=>"User Login successfully");
							echo json_encode($resparr);
					} */
				}
				else 
				{
					$resparr=array("status"=>"Failed","UserId"=>'-',"Username"=>'-',"Mobileno"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"msg"=>"Username or Password Wrong");
					echo json_encode($resparr);
				}
			}
			else
			{
				$resparr=array("status"=>"Failed","UserId"=>'-',"Username"=>'-',"Mobileno"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"msg"=>"Key Mismatch");
				echo json_encode($resparr);
			}
		}
		else
		{
			$resparr=array("status"=>"Failed","UserId"=>'-',"Username"=>'-',"Mobileno"=>'-',"portal_type"=>"-","startdate"=>'-',"enddate"=>'-',"ParentID"=>'-',"msg"=>"Please provide required field values");
			echo json_encode($resparr);
		}
	}
	
	public function childlist()
	{
		$ParentID = $this->input->post('ParentID');		 
		if(($ParentID!=''))
		{
			$data['query'] = $this->Arapi_model->checkParentExist($ParentID);
			if(isset($data['query'][0]->id))
			{
				$arrchildlist=$this->Arapi_model->getChildList($data['query'][0]->id);
				//echo "<pre>";print_r($arrchildlist);exit;
				$resparr=array("status"=>"success","childlist"=>$arrchildlist,"msg"=>"-");
				echo json_encode($resparr);
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}	
	public function fpvalidate()
	{
		$username = $this->input->post('UserId');
		
		if(($username!=''))
		{
			//$data['query'] = $this->Arapi_model->checkuserexist($username);
			$isparent= $this->Arapi_model->checkParentExist($username);				
			if(isset($isparent[0]->id))
			{
				$data['query'] = $this->Arapi_model->checkParentExist($username);
			}
			else
			{
				$data['query'] = $this->Arapi_model->checkuserexist($username);
			}			
			if(isset($data['query'][0]->id))
			{	
				$OTP=mt_rand(100000, 999999);
				$MSG="Dear ".$data['query'][0]->fname.", OTP : ".$OTP." is to reset your password";
				$this->sendsms($data['query'][0]->mobile,$MSG);
				
				if(isset($isparent[0]->id))
				{
					$this->Arapi_model->updateParentRegOtp($OTP,$data['query'][0]->id);
					$this->Arapi_model->parent_forgetpwdlog($data['query'][0]->id,$OTP);
				}
				else
				{
					$this->Arapi_model->updateRegOtp($OTP,$data['query'][0]->id);
					$this->Arapi_model->forgetpwdlog($data['query'][0]->id,$OTP);					
				}
				$resparr=array("status"=>"success","OTP"=>$OTP,"msg"=>"-");
				echo json_encode($resparr);exit;
			}
			else
			{
				$resparr=array("status"=>"Failed","OTP"=>"-","msg"=>"Enter Valid Username");
				echo json_encode($resparr);
			}
		}
	}
	
	
	public function fpupdate()
	{
		$username = $this->input->post('UserId');
		$password = $this->input->post('Password');
		$OTP = $this->input->post('OTP');
 		 
		if(($username!='') && ($password!='') && ($OTP!=''))
		{
			//$data['query'] = $this->Arapi_model->checkuserexist($username);
			$isparent= $this->Arapi_model->checkParentExist($username);				
			if(isset($isparent[0]->id))
			{
				$data['query'] = $this->Arapi_model->checkParentExist($username);
			}
			else
			{
				$data['query'] = $this->Arapi_model->checkuserexist($username);
			}	
			if(isset($data['query'][0]->id))
			{
				if($data['query'][0]->otp==$OTP)
				{
					$shpassword = $this->salt_my_pass($password);
					if(isset($isparent[0]->id))
					{
						$data['query1'] = $this->Arapi_model->update_Parent_Password($data['query'][0]->id,$username,$password,$shpassword['Hash'],$shpassword['Salt1'],$shpassword['Salt2']);
					}
					else
					{
						$data['query1'] = $this->Arapi_model->updatePassword($data['query'][0]->id,$username,$password,$shpassword['Hash'],$shpassword['Salt1'],$shpassword['Salt2']);
					}
					$resparr=array("status"=>"success","msg"=>"Password Updated Successfully");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"OTP Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}

 
 	public function flushlogin()
	{
		$username = $this->input->post('UserId');
		if(($username!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
					$OTP=mt_rand(100000, 999999);
					$MSG="Dear ".$data['query'][0]->fname.", OTP : ".$OTP." is to confirm to flush logins in other device";
					$this->sendsms($data['query'][0]->mobile,$MSG);
					$this->Arapi_model->updatetempotp($OTP,$data['query'][0]->id);
					$resparr=array("status"=>"success","OTP"=>$OTP,"msg"=>"-");
					echo json_encode($resparr);
				
			}
			else 
			{
				$resparr=array("status"=>"Failed","OTP"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}

	
 	public function flushloginupdate()
	{
		$username = $this->input->post('UserId');
		$deviceid = $this->input->post('DeviceId');
		$OTP = $this->input->post('OTP');
		if(($username!='') && ($deviceid!='') && ($OTP!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{ 
				if($data['query'][0]->otp==$OTP)
				{
					$data['query1'] = $this->Arapi_model->updateDeviceID($username,$deviceid,'');
					$this->Arapi_model->clearTempotp($data['query'][0]->id);
					$resparr=array("status"=>"success","UserId"=>$data['query'][0]->username,"msg"=>"Login in other device has been flushed successfully");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","UserId"=>'-',"msg"=>"OTP Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","UserId"=>'-',"msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	
 
 	public function userprofile()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					if($data['query'][0]->show_grade=='Y')
					{
						$gradename=$data['query'][0]->gradename;
					}
					else
					{
						$gradename='';
					}
					$expiry_date=date("d-m-Y", strtotime($data['query'][0]->enddate));
					$resparr=array("status"=>"success","first_name"=>$data['query'][0]->fname,"last_name"=>$data['query'][0]->lname,"mailid"=>$data['query'][0]->email,"mobile"=>$data['query'][0]->mobile,"expiry_date"=>$expiry_date,"grade"=>$gradename,"start_date"=>$data['query'][0]->startdate,"avatarimage"=>$data['query'][0]->avatarimage,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","first_name"=>"-","last_name"=>"-","mailid"=>"-","mobile"=>"-","expiry_date"=>"-", "grade"=>"-","start_date"=>"-","avatarimage"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","first_name"=>"-","last_name"=>"-","mailid"=>"-","mobile"=>"-","expiry_date"=>"-", "grade"=>"-","start_date"=>"-","avatarimage"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
 	public function gamedetails()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{	
					$check_date_time = date('Y-m-d');
					$catid=1;
					$userid=$data['query'][0]->id;
					$planid=$data['query'][0]->gp_id;
					$gradeid=$data['query'][0]->grade_id;
					$schoolid=$data['query'][0]->sid;
					$section=$data['query'][0]->section;
					$data['randomGames']=$this->Arapi_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					
					$data['assignGames']=$this->Arapi_model->getAssignGames($planid,$gradeid,$userid,$catid);
					
					$cur_day_skills = count($data['randomGames']);
					$assign_count = count($data['assignGames']);
									

					if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) 
					{						
						$this->fn_Rand_games($userid,$check_date_time,$cur_day_skills,$assign_count,$catid,$planid,$gradeid,$schoolid,$section);
					}
					$data['randomGames']=$this->Arapi_model->getRandomGames($check_date_time,$planid,$gradeid,$schoolid,$userid);
					$where = "";
					foreach($data['randomGames'] as $brainData )
					{
						$brainIds[] = $brainData['gid'];
						$active_game_ids = @implode(',',$brainIds);
						$where = " and g.gid in ($active_game_ids)";
					}
						 
					$data['actualGames'] = $this->Arapi_model->getActualGames($planid,$gradeid,$userid,$catid,$where,$schoolid);
					
					$data['checkgame'] = $this->Arapi_model->checkgame($data['actualGames'][0]['gid'],$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
 					if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}
					
					if($gradeid==8 || $gradeid==9 || $gradeid==10)
					{	//Grade VI,VII & VIII
						$cqvisible=$this->Arapi_model->getChallengeQuestionVisible($userid);
						if(($cqvisible[0]['topscore']>=70 || $cqvisible[0]['playedcount']>=5) && ($cqvisible[0]['playedstatus']==0))
						{
							$CQvisible=1;
						}
						else if($cqvisible[0]['playedstatus']==1)
						{
							$CQvisible=2;
						}
						else
						{
							$CQvisible=0;
						}
					}
					else
					{
						$CQvisible='-1';
					}
					
					$resparr=array("status"=>"success","gamedetails"=>$data['actualGames'],"image_path"=>base_url('assets/'),"total_play_times"=>$this->info['default_play_time'],"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","gamedetails"=>"-","image_path"=>"-","total_play_times"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function logout()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
 		 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$isparent= $this->Arapi_model->checkParentExist($username);
				
			if(isset($isparent[0]->id))
			{
				$data['query'] = $this->Arapi_model->checkParentExist($username);
			}
			else
			{
				$data['query'] = $this->Arapi_model->checkuserexist($username);
			}
			if(isset($data['query'][0]->id))
			{
				if(isset($isparent[0]->id))
				{ // Parent
					$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']);
					if($Key==$token['Hash'])
					//if(1==1)
					{
						$this->Arapi_model->updateParentDeviceID($username,'','');
						$this->Arapi_model->update_parent_logout_log($data['query'][0]->id,$data['query'][0]->session_id);
						$this->Arapi_model->update_parent_loginstatus($data['query'][0]->id,$data['query'][0]->session_id);
						$resparr=array("status"=>"success","msg"=>"Device Id cleared successfully");
						echo json_encode($resparr);
					}
					else 
					{
						$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
						echo json_encode($resparr);
					}
				}
				else
				{ // Child
					$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
					//if($Key==$token['Hash'])
					if(1==1)
					{	
						$this->Arapi_model->updateDeviceID($username,'','');
						$this->Arapi_model->update_logout_log($data['query'][0]->id,$data['query'][0]->session_id);
						$this->Arapi_model->updateuserloginstatus($data['query'][0]->id,$data['query'][0]->session_id);
						$resparr=array("status"=>"success","msg"=>"Device Id cleared successfully");
						echo json_encode($resparr);
					}
					else 
					{
						$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
						echo json_encode($resparr);
					}
				}
					
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
	public function result()
	{
		$data['query'] = $this->Arapi_model->checkuserexist($this->session->app_userid);
		if(isset($data['query'][0]->id))
		{
			$gameid=$this->session->currentgameid;
			
			if($gameid==0)
			{
				echo '-2';exit;
			}
			
			$data['checkgame'] = $this->Arapi_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);
			if($data['checkgame'][0]['game_limit']==''){ $data['checkgame'][0]['game_limit']=$this->info['default_play_time'];}		
 			if($data['checkgame'][0]['game_limit']>$data['checkgame'][0]['played_time'])
			{ 

				$postdata = $this->session->flashdata('app_post_data');
				$total_ques=$postdata["tqcnt1"];
				if($postdata["aqcnt1"]>10){
					$attempt_ques=10;
				}
				else{
					$attempt_ques=$postdata["aqcnt1"];
				}

				$answer=$postdata["cqcnt1"];
				$score=$postdata["gscore1"];
				$a6=$postdata["gtime1"];
				$a7=$postdata["rtime1"];
				$a8=$postdata["crtime1"];
				$a9=$postdata["wrtime1"];	
				
				$skillkit=0;
				$userlang = 1;
				$userid = $data['query'][0]->id; 
				$lastup_date = date("Y-m-d");
				$cid = 1;
				$data['gameDetails'] = $this->Arapi_model->getresultGameDetails($userid,$gameid);
				$skillid =$data['gameDetails'][0]['gameskillid'] ; 
				$schedule_val = 0;
				$pid =  $data['query'][0]->gp_id; 
				
				$data['insert1'] = $this->Arapi_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val);
			
				$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);

/*........................... Push Notification ..........................................................*/
			$TopScore = $this->Arapi_model->getTopScorePosition($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
			$PuzzlesAttempted= $this->Arapi_model->getMaxPuzzlesAttempted($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
			$PuzzlesSolved= $this->Arapi_model->getMaxPuzzlesSolved($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
			
			
			if($TopScore[0]['position']!='' && $TopScore[0]['position']==1)
			{$b1position=1;}
			else{$b1position=0;}
			$arrbadgeposition= $this->Arapi_model->getPrevUserPosition($userid);
			if($b1position==$arrbadgeposition[0]['sbb_position'])
			{
				// Same 
			}
			else if($b1position==1 && $arrbadgeposition[0]['sbb_position']==0)
			{
				// New Badge
				$this->Arapi_model->InsertUserBadgeStatusLog($userid,'SBB',$TopScore[0]['score'],1,'sbb_position',$b1position);
				
				// CALL API
				$this->sendPush('You Got a Super Brain Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
			else if($b1position==0 && $arrbadgeposition[0]['sbb_position']==1)
			{
				// Loose the Badge
				$this->Arapi_model->InsertUserBadgeStatusLog($userid,'SBB',$TopScore[0]['score'],0,'sbb_position',$b1position);
				
				// CALL API
				//$this->sendPush('You Got a Super Brain Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperBrain-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
/*....................................... Push Notification SGB ..............................................*/
			if($PuzzlesAttempted[0]['position']!='' && $PuzzlesAttempted[0]['position']==1)
			{$b2position=1;}
			else{$b2position=0;}
			$arrbadgeposition= $this->Arapi_model->getPrevUserPosition($userid);
			if($b2position==$arrbadgeposition[0]['sgb_position'])
			{
				// Same 
			}
			else if($b2position==1 && $arrbadgeposition[0]['sgb_position']==0)
			{
				// New Badge
				$this->Arapi_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],1,'sgb_position',$b2position);
				
				// CALL API
				$this->sendPush('You Got a Super Goer Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperGoer-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
			else if($b2position==0 && $arrbadgeposition[0]['sgb_position']==1)
			{
				// Loose the Badge
				$this->Arapi_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],0,'sgb_position',$b2position);
				
				// CALL API
				//$this->sendPush('You Got a Super Goer Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperGoer-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
/*....................................... Push Notification SAB ..............................................*/
			if($PuzzlesSolved[0]['position']!='' && $PuzzlesSolved[0]['position']==1)
			{$b3position=1;}
			else{$b3position=0;}
			$arrbadgeposition= $this->Arapi_model->getPrevUserPosition($userid);
			if($b3position==$arrbadgeposition[0]['sab_position'])
			{
				
			}
			else if($b3position==1 && $arrbadgeposition[0]['sab_position']==0)
			{
				// New Badge
				$this->Arapi_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],1,'sab_position',$b3position);
				
				// CALL API
				$this->sendPush('You Got a Super Angels Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperAngel-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
			else if($b3position==0 && $arrbadgeposition[0]['sab_position']==1)
			{
				// Loose the Badge
				$this->Arapi_model->InsertUserBadgeStatusLog($userid,'SGB',$TopScore[0]['score'],0,'sab_position',$b3position);
				
				// CALL API
				//$this->sendPush('You Got a Super Angels Badge',$data['query'][0]->mtoken,'https://summerchamps.com/dev/assets/images/new/SuperAngel-Badge.png','https://summerchamps.com/dev/assets/swf/3/assets/wow_s.ogg','N');
			}
/*....................................... Push Notification END ..............................................*/

			/*--- Sparkies ----*/
			$sparkies_output=$this->Arapi_model->insertsparkies($arrofinput);
			echo $sparkies_output[0]['OUTPOINTS'];exit; 
				
			}
			$this->session->unset_userdata('app_userid');
		}
	}

	public function fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid,$planid,$gradeid,$schoolid,$section) 
	{
		if($cur_day_skills!=$assign_count)
		{ // MisMatch Skill Count => Delete mismateched games for that user in todays date.
			$del_where = " and created_date = '$check_date_time'";
			$this->Arapi_model->deleteMisMatchRandomGames($catid,$planid,$gradeid,$uid,$schoolid,$del_where);
		}
		//$CurrentDaySkill=$this->Arapi_model->getCurrentDaySkill($uid);
		$arrSkills=$this->Arapi_model->getSkillsRandom($catid,$CurrentDaySkill[0]['currentdayskillid']);
		foreach($arrSkills as $gs_data)
		{
		  $rand_sel = $this->Arapi_model->assignRandomGame($catid,$planid,$gradeid,$uid,$gs_data['skill_id'],$schoolid);
			$rand_count = count($rand_sel);
			if($rand_count<=0)
			{ 
			/*--- No Unique games => delete previous all games of user( in particular skill) and insert new game for that skill (for that date only) ---*/
		
				$del_where = " ";
				$this->Arapi_model->deleteRandomGames($catid,$planid,$gradeid,$uid,$gs_data['skill_id'],$schoolid,$del_where);
				
				$rand_sel = $this->Arapi_model->assignRandomGame($catid,$planid,$gradeid,$uid,$gs_data['skill_id'],$schoolid);
				
				$this->Arapi_model->insertRandomGames($catid,$planid,$gradeid,$gs_data['skill_id'],$schoolid,$section,$rand_sel[0]['gid'],$check_date_time,$uid);
			}
			else
			{ //If New Games available => insert the new game for user for that date
				$this->Arapi_model->insertRandomGames($catid,$planid,$gradeid,$gs_data['skill_id'],$schoolid,$section,$rand_sel[0]['gid'],$check_date_time,$uid);
			}
		}
	} 
	public function sendsms($tonum,$message)
	{
		/* $ch = curl_init("http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=Ac79406bbeca30e25c926bad8928bcc17&to=".$tonum."&sender=SBRAIN&message=".urlencode($message));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response=curl_exec($ch);       
		curl_close($ch); */
	}
	public function confirmation_email($toemailid,$subject,$message)
	{
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = "smtp.falconide.com";
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "";
		$mail->Username = "skillsangelsfal";
		$mail->Password = "SkillAngels@123";
		$mail->setFrom('angel@skillangels.com', 'Kinder Angels');
		$mail->addReplyTo('angel@skillangels.com', 'Kinder Angels');
		$mail->addAddress($toemailid, ''); //to mail id
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		/* if (!$mail->send()) {
		   //echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		   //echo "Message sent!";
		}   */
	}
	public function salt_my_pass($password)
	{
		$salt1 = mt_rand(1000,9999999999);
		$salt2 = mt_rand(100,999999999);
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	} 
	public function encMethod($salt1,$password,$salt2)
	{
		$salted_pass = $salt1 . $password . $salt2;
		$pwdhash = sha1($salted_pass);
		$hash['Salt1'] = $salt1;
		$hash['Salt2'] = $salt2;
		$hash['Hash'] = $pwdhash;
		return $hash;

	} 
	
	
	/*......... Badges Concept ...........*/
	public function superbrainbadge()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$position = $this->Arapi_model->getTopScorePosition($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					if($position[0]['position']!='')
					{
						$position=$position[0]['position'];
					}
					else
					{
						$position=0;
					}
					$resparr=array("status"=>"success","Position"=>$position,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function supergoerbadge()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$position= $this->Arapi_model->getMaxPuzzlesAttempted($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					if($position[0]['position']!='')
					{
						$position=$position[0]['position'];
					}
					else
					{
						$position=0;
					}
					$resparr=array("status"=>"success","Position"=>$position,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	public function superangelbadge()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$position= $this->Arapi_model->getMaxPuzzlesSolved($data['query'][0]->id,$data['query'][0]->org_id,$data['query'][0]->grade_id);
					if($position[0]['position']!='')
					{
						$position=$position[0]['position'];
					}
					else
					{
						$position=0;
					}
					$resparr=array("status"=>"success","Position"=>$position,"msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","Position"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
/*......... Badges Concept End...........*/

	
	
	/*....... Leader Board ........*/
	
	/*....... Profile Avatar List ........*/
	public function avatarlist()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 
		if(($username!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{	
					$baseurl = "assets/images/avatarimages/avatar";
					$imagename='';$visible='';
					if ($dh = opendir($baseurl))
					{	$i=1;
						while (($file = readdir($dh)) !== false)
						{ 
							if($file!='.' && $file!='..')
							{	
								$imagename.="'".$file."',";
								if($i<=5)
								{
									$visible.="'1',";
								}
								else
								{
									$visible.="'0',";
								}
								$i=$i+1;
							}
							
						}
						closedir($dh);
						
						$resparr=array("status"=>"success","avatarurl"=>$baseurl,"avatarname"=>rtrim($imagename,','),"visible"=>rtrim($visible,','),"msg"=>"-");
					}
					else
					{
						$resparr=array("status"=>"Failed","avatarurl"=>$baseurl,"avatarname"=>rtrim($imagename,','),"visible"=>"-","msg"=>"No avatar image available ");
					}
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","avatarurl"=>"-","avatarname"=>"-","visible"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","avatarurl"=>"-","avatarname"=>"-","visible"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
		
	public function avatarupdate()
	{
		$username = $this->input->post('UserId');
		$avatarimg = $this->input->post('avatarimg');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts');
		if(($username!='') && ($avatarimg!='') && ($Key!='') && ($timestamp!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
				if($Key==$token['Hash'])
				{
					$avatarimg=$avatarimg; 
					$userid=$data['query'][0]->id;
					$this->Arapi_model->avatarupdate($userid,$avatarimg);
					$resparr=array("status"=>"success","msg"=>"-");
					echo json_encode($resparr);
				}
				else 
				{
					$resparr=array("status"=>"Failed","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
	}
	
public function sendPush($body,$mtoken,$icon,$sound,$imgflag)
{
	#API access key from Google API's Console
	define( 'API_ACCESS_KEY', 'AIzaSyDCUVLX4XRQrGV98uxSwl1Oq41ty042LdU' );
	//$registrationIds = 'fLoWjWYwig8:APA91bHl736oFoJrklskahkQE5rMY0Zb5FsqExflhbfcsezrGUkFz_qXZ6_fkvHSwMKjQIp7Rgs58hik_h9qiUxbzPNgdj-6fHAMYGV0ToLS6fSct38Vx1EsVOzNlegNVmDxAGEQ9I8P';
	$registrationIds=$mtoken;
	#prep the bundle
	$msg = array
	  (
	'body' 	=> $body,
	'title'	=> 'Summer Champ - Notification',
	'icon'	=>$icon,
	'sound' =>$sound,
	'tag'=>$imgflag
	);
	$fields = array
	(
		'to'		=> $registrationIds,
		'notification'	=> $msg
	);


	$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
	#Send Reponse To FireBase Server	
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );
	#Echo Result Of FireBase Server
	//echo $result;
}

/*....... M Token Update ........*/
	public function mtoken()
	{
		$username = $this->input->post('UserId');
		$mtoken = $this->input->post('mtoken');
		if(($username!='') && ($mtoken!=''))
		{
			$isparent= $this->Arapi_model->checkParentExist($username);				
			if(isset($isparent[0]->id))
			{
				$data['query'] = $this->Arapi_model->checkParentExist($username);
			}
			else
			{
				$data['query'] = $this->Arapi_model->checkuserexist($username);
			}	
			
			if(isset($data['query'][0]->id))
			{
				if(isset($isparent[0]->id))
				{
					$this->Arapi_model->update_parent_Mtoken($username,$mtoken);
				}
				else
				{
					$this->Arapi_model->updateMtoken($username,$mtoken);
				}
				
				$resparr=array("status"=>"success","msg"=>"mtoken Updated Successfully");
				echo json_encode($resparr);
			}
			else 
			{
				$resparr=array("status"=>"Failed","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr=array("status"=>"Failed","msg"=>"Invalid User");
			echo json_encode($resparr);
		}
	}
	
	
	
	
	
/* ....................... Registration code Begins .......................*/
	public function statelist()
	{
		$arrStateList=$this->Arapi_model->getStateList();
		$resparr=array("status"=>"success","StateList"=>$arrStateList);
		echo json_encode($resparr);
	}
	public function citylist()
	{
		$stateid = $this->input->post('stateid');		
		$arrCityList=$this->Arapi_model->getCityList($stateid);
		$resparr=array("status"=>"success","CityList"=>$arrCityList);
		echo json_encode($resparr);
	}
	public function schoollist()
	{
		$stateid = $this->input->post('stateid');		
		$arrSchoolList=$this->Arapi_model->getSchooLlist($stateid);
		$resparr=array("status"=>"success","SchoolList"=>$arrSchoolList);
		echo json_encode($resparr);
	}
	public function gradelist()
	{
		$arrGradeList=$this->Arapi_model->gradelist();
		$resparr=array("status"=>"success","GradeId"=>$arrGradeList[0]['grade_id'],"GradeName"=>$arrGradeList[0]['grade_name']);
		echo json_encode($resparr);
	}
	public function ismobilenounique()
	{
		$mobileno = $this->input->post('txtMobileno');	
		if($mobileno!='')
		{
			$arrGradeList=$this->Arapi_model->IsMobilenounique($mobileno);
			$resparr=array("status"=>"success","uniquestatus"=>$arrGradeList[0]['isexist']);
		}
		else
		{
			$resparr=array("status"=>"success","msg"=>'Invalid Access');
		}
		echo json_encode($resparr);
	}
	public function isemailunique()
	{
		$isusername = $this->input->post('txtEmailid');	
		if($isusername!='')
		{
			$arrGradeList=$this->Arapi_model->IsEmailunique($isusername);
			$resparr=array("status"=>"success","uniquestatus"=>$arrGradeList[0]['isexist']);
		}
		else
		{
			$resparr=array("status"=>"success","msg"=>'Invalid Access');
		}
		echo json_encode($resparr);
	}
	
	public function parentreg()
	{
		$Name = $this->input->post('txtName');
		$Emailid = $this->input->post('txtEmailid');
		$Mobileno = $this->input->post('txtMobileno');
		$State = $this->input->post('txtState');
		$City = $this->input->post('txtCity');
		$Pincode = $this->input->post('txtPincode');
		$Address = $this->input->post('txtAddress');
		$Password = $this->input->post('txtPassword');
		
		
		$org_pwd = $this->input->post('txtpassword');
		
		$hashpass = $this->salt_my_pass($Password);
		 
		$shpassword = $hashpass['Hash']; 
		$salt1 = $hashpass['Salt1']; 
		$salt2 = $hashpass['Salt2']; 
		
		$saltedpass = $salt1 . $shpassword . $salt2; 		
		//echo "<pre>";print_r($_POST);exit;
		if($Emailid!='' && $Mobileno!='')
		{
			$ismobile =$this->Arapi_model->IsMobilenounique($Mobileno);			
			$isusername = $this->Arapi_model->IsEmailunique($Emailid);
			if($isusername[0]['isexist']==0 && $ismobile[0]['isexist']==0)
			{
				$created_on=date("Y-m-d");
				$data['insertreg'] = $this->Arapi_model->addRegistration($Name,$Mobileno,$Emailid,$shpassword,$salt1,$salt2,$org_pwd,$created_on,$State,$City,$Pincode,$Address);
				$RegId = $data['insertreg'];
				
				//$OTP=mt_rand(100000, 999999);
				$OTP='123456';
				$MSG="Dear ".$Name.", OTP : ".$OTP." is to confirm your mobile no";
				$this->sendsms($Mobileno,$MSG);
				$this->Arapi_model->updateParentRegOtp($OTP,$RegId);
				 
				$arrResult=array("status"=>"success","ParentID"=>$RegId,"OTP"=>$OTP,"msg"=>"-");
				echo json_encode($arrResult); exit;
			}
			else
			{
				$arrResult=array("status"=>"failure","ParentID"=>"-","OTP"=>"-","msg"=>"Emailid and Mobile no is already registred.");
				echo json_encode($arrResult);exit;
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","ParentID"=>"-","OTP"=>"-","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	public function resendotp()
	{
		$ParentID = $this->input->post('ParentID');		
		if($ParentID!='')
		{	
			$data['query'] = $this->Arapi_model->getInActiveParentDetails($ParentID);
			if(isset($data['query'][0]->id))
			{			
				$OTP=mt_rand(100000, 999999);
				$Name=$data['query'][0]->name;
				$Mobileno=$data['query'][0]->mobileno;
				$MSG="Dear ".$Name.", OTP : ".$OTP." is to confirm your mobile no";
				$this->sendsms($Mobileno,$MSG);
				$this->Arapi_model->updateParentRegOtp($OTP,$ParentID);
				 
				$arrResult=array("status"=>"success","ParentID"=>$ParentID,"OTP"=>$OTP,"msg"=>"-");
				echo json_encode($arrResult); exit;
			}
			else 
			{
				$resparr=array("status"=>"failure","ParentID"=>"-","OTP"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","RegId"=>"-","OTP"=>"-","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	public function parentconfirmuser()
	{
		$ParentID = $this->input->post('ParentID');		
		if($ParentID!='')
		{	
			$data['query'] = $this->Arapi_model->getInActiveParentDetails($ParentID);
			if(isset($data['query'][0]->id))
			{			
				
				$this->Arapi_model->updateParentConfirmUser($ParentID);
				 
				$arrResult=array("status"=>"success","Username"=>$data['query'][0]->username,"Mobileno"=>$data['query'][0]->mobileno,"ParentID"=>$ParentID,"msg"=>"Registered successfully");
				echo json_encode($arrResult); exit;
			}
			else 
			{
				$resparr=array("status"=>"failure","Username"=>"-","Mobileno"=>"-","ParentID"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","Username"=>"-","Mobileno"=>"-","ParentID"=>"-","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	public function childreg()
	{
		$Name = $this->input->post('txtName');
		$Grade = $this->input->post('txtGrade');
		$Gender = $this->input->post('txtGender');
		$Address = $this->input->post('txtAddress');
		$ParentID = $this->input->post('ParentID');
		$State = $this->input->post('txtState');
		$City = $this->input->post('txtCity');
		$Pincode = $this->input->post('txtPincode');
		$Schoolname = $this->input->post('txtSchoolname');
		$Couponcode = $this->input->post('txtCouponcode');
		$Bookneed = $this->input->post('txtBookneed');
		$SpecialChild = $this->input->post('txtSpecialChild');
		
		if($SpecialChild=='Y')
		{
			$Grade='11';
		}
		else
		{
			$Grade=$Grade;
		}
		
		$hashpass = $this->salt_my_pass($this->info['default_pwd']);
		$orgpwd =$this->info['default_pwd'];
		 
		$shpassword = $hashpass['Hash']; 
		$salt1 = $hashpass['Salt1']; 
		$salt2 = $hashpass['Salt2']; 
		
		$saltedpass = $salt1 . $shpassword . $salt2; 
		
		if($Grade!='' && $ParentID!='')
		{		
			$arrcoupon = $this->Arapi_model->GetCouponCodeDetails($Couponcode);
			//echo "<pre>";print_r($arrcoupon);exit;
			if($arrcoupon[0]['iscouponvalid']==1)
			{
				if($arrcoupon[0]['coupon_valid_times']==0)
				{ // User can manyh time
					$isallow =1;
				}
				else
				{ // Restricted Use
					$couponcheck=$this->Arapi_model->applycoupon($Couponcode);
					if($couponcheck[0]['iscouponvalid']==1)
					{
						$isallow =1;
					}
					else
					{
						$isallow =0;
					}
				}
				if($isallow==1)
				{
					$arrplan=$this->Arapi_model->getPlan($Grade);
					$noofchild = $this->Arapi_model->ChildLimitExpired($ParentID);
					$ChildLimit=$this->config->item('NoOfChild');
					if($ChildLimit > $noofchild[0]['noofchild'])
					{
											
						$created_on=date("Y-m-d");
						$data['insertreg'] = $this->Arapi_model->addChildReg($Name,$arrplan[0]['id'],$Grade,$Gender,$Address,$ParentID,$shpassword,$salt1,$salt2,$created_on,$orgpwd,$State,$City,$Pincode,$Schoolname,$Couponcode,$Bookneed,$arrcoupon[0]['validity'],$arrcoupon[0]['orgid'],$SpecialChild);
						$RegId = $data['insertreg'];
						
						$arrResult=array("status"=>"success","ChildID"=>$RegId,"Discount"=>$arrcoupon[0]['discount_percentage'],"Price"=>$arrcoupon[0]['price'],"msg"=>"-");
						echo json_encode($arrResult); exit;
					}
					else
					{
						$arrResult=array("status"=>"failure","ChildID"=>"-","Discount"=>'-',"Price"=>'-',"msg"=>"User limit was expired");
						echo json_encode($arrResult);exit;
					}
				}
				else
				{
					$arrResult=array("status"=>"failure","ChildID"=>"-","Discount"=>'',"Price"=>'',"msg"=>"Couponcode limit was expired");
					echo json_encode($arrResult);exit;
				}
			}
			else
			{
				$arrResult=array("status"=>"failure","ChildID"=>"-","Discount"=>'-',"Price"=>'-',"msg"=>"Invalid Couponcode");
				echo json_encode($arrResult);exit;
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","ChildID"=>"-","Discount"=>'-',"Price"=>'-',"msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
		
	public function childregsuc()
	{
		$ChildID = $this->input->post('ChildID');		
		$PaymentStatus = $this->input->post('PaymentStatus');		
		$PaymentId = $this->input->post('PaymentId');		
		if($ChildID!='' && $PaymentStatus!='' && $PaymentId!='')
		{	
			$data['query'] = $this->Arapi_model->getInActiveChildIDDetails($ChildID);
			if(isset($data['query'][0]->id))
			{			
				if($PaymentStatus=='Y')
				{
					$this->Arapi_model->updateChildConfirmUser($ChildID,$PaymentStatus,$PaymentId,$data['query'][0]->grade_id,$data['query'][0]->gp_id);
					
					 $this->Arapi_model->UpdateCouponCount($data['query'][0]->couponcode);
					 
					$arrResult=array("status"=>"success","ChildID"=>$ChildID,"msg"=>"Registered successfully");
					echo json_encode($arrResult); exit;
				}
				else
				{
					$this->Arapi_model->updateChildFailureUser($ChildID,$PaymentStatus);
					$arrResult=array("status"=>"failure","ChildID"=>'-',"msg"=>"Payment failure");
					echo json_encode($arrResult); exit;
				}
			}
			else 
			{
				$resparr=array("status"=>"failure","ChildID"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else
		{
			$arrResult=array("status"=>"failure","ChildID"=>"-","msg"=>"Please fill the mandatory fields");
			echo json_encode($arrResult);exit;
		}
	}
	
	public function arresult()
	{
		$username = $this->input->post('UserId');
		$Key = $this->input->post('Key');
 		$timestamp = $this->input->post('ts'); 		
		$gamename=$this->input->post('gameID');		
		if(($username!='') && ($Key!='') && ($timestamp!='') && ($gamename!=''))
		{
			$data['query'] = $this->Arapi_model->checkuserexist($username);
			if(isset($data['query'][0]->id))
			{
				$token=$this->encMethod($timestamp,$data['query'][0]->deviceid,$this->info['secret_key']); 
	 
				if($Key==$token['Hash'])
				{
					$data['gameid'] = $this->Arapi_model->getgameid($gamename);
					$gameid = $data['gameid'][0]['gid'];
					if($gameid!='')
					{
						$data['checkgame'] = $this->Arapi_model->checkgame($gameid,$data['query'][0]->sid,$data['query'][0]->grade_id,$data['query'][0]->section,$data['query'][0]->id);

						$game_limit=$this->info['default_play_time'];
						
						if($game_limit>$data['checkgame'][0]['played_time'])
						{ 
							$postdata = $_POST;
							$total_ques=$postdata["total_ques"];
							if($postdata["attempt_ques"]>10){
								$attempt_ques=10;
							}
							else{
								$attempt_ques=$postdata["attempt_ques"];
							}

							$answer=$postdata["answer"];
							$score=$postdata["gamescore"];
							$a6=$postdata["gtime1"];
							$a7=$postdata["rtime1"];
							$a8=$postdata["crtime1"];
							$a9=$postdata["wrtime1"];	
							
							$skillkit=0;
							$userlang = 1;
							$userid = $data['query'][0]->id; 
							$lastup_date = date("Y-m-d");
							$cid = 1;
							$data['gameDetails'] = $this->Arapi_model->getresultGameDetails($userid,$gameid);
							$skillid =$data['gameDetails'][0]['gameskillid'] ; 
							$schedule_val = 0;
							$pid =  $data['query'][0]->gp_id; 
							
							$data['insert1'] = $this->Arapi_model->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date,$schedule_val);
						
							$arrofinput=array("inSID"=>$data['query'][0]->sid,"inGID"=>$data['query'][0]->grade_id,'inUID'=>$userid,'inScenarioCode'=>'GAME_END','inTotal_Ques'=>$total_ques,'inAttempt_Ques'=>$attempt_ques,'inAnswer'=>$answer,'inGame_Score'=>$score,"inPlanid"=>$pid,'inGameid'=>$gameid);

							/*--- Sparkies ----*/
							$sparkies_output=$this->Arapi_model->insertsparkies($arrofinput);
							//echo $sparkies_output[0]['OUTPOINTS'];exit; 
							
							$resparr=array("status"=>"success","crownypoints"=>$sparkies_output[0]['OUTPOINTS'],"response"=>1,"msg"=>"-");
							echo json_encode($resparr);
						}
						else
						{
							$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-1","msg"=>"Game limit expired");
							echo json_encode($resparr);
						}
					}
					else
					{
						$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-2","msg"=>"Game not avialable");
						echo json_encode($resparr);
					}
				}
				else 
				{
					$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Key Mismatch");
					echo json_encode($resparr);
				}
			}
			else 
			{			
				$resparr=array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Invalid User");
				echo json_encode($resparr);
			}
		}
		else 
		{
			$resparr= array("status"=>"Failed","crownypoints"=>"-","response"=>"-","msg"=>"Please provide all required parameters");
			echo json_encode($resparr);
		}
	}
	
	
	
	
}