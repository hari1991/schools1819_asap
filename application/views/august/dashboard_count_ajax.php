<table  class="table table-bordered table-condensed table-hover table-striped">
		<thead>
			<tr>
				<th>S.No</th>
				<th>Grade</th>
				<th>Section</th>
				<th>Total Users</th>
				<th>User Taken Assessment</th>
				<th>User Completed Assessment</th>
				<th>M</th>
				<th>VP</th>
				<th>FA</th>
				<th>PS</th>
				<th>LI</th>
				<th>User Completed Assessment BSPI</th>
			</tr>
		</thead>  
		<tbody>
		<?php $val1=0;$val2=0;$val3=0;$i=1; 
		
$m1=0;$vp1=0;$fa1=0;$ps1=0;$li1=0;
$m=0;$vp=0;$fa=0;$ps=0;$li=0;
$a=0;$b=0;$c=0;$d=0;$e=0;

foreach($gradesection as $row){
if(!empty($RegisteredUserCount[$row['rowval']])){
if(isset($RegisteredUserCount[$row['rowval']])){$rcount=$RegisteredUserCount[$row['rowval']];}else{$rcount='-';}
if(isset($AssessmentTakenCount[$row['rowval']])){$tcount=$AssessmentTakenCount[$row['rowval']];}else{$tcount='-';}
if(isset($AssessmentTakenBSPI[$row['rowval']])){$tbcount=$AssessmentTakenBSPI[$row['rowval']];$k++;}else{$tbcount='-';}
if(isset($FullyAssessmentTakenCount[$row['rowval']])){$fcount=$FullyAssessmentTakenCount[$row['rowval']];}else{$fcount='-';}
			$val1+=$rcount;
			$val2+=$tcount;
			$val3+=$fcount;
			$val4+=$tbcount;

if(isset($ClassSkillScore[$row['rowval']."-M"])){$m=$ClassSkillScore[$row['rowval']."-M"];$a++;}else{$m='-';}
if(isset($ClassSkillScore[$row['rowval']."-VP"])){$vp=$ClassSkillScore[$row['rowval']."-VP"];$b++;}else{$vp='-';}
if(isset($ClassSkillScore[$row['rowval']."-FA"])){$fa=$ClassSkillScore[$row['rowval']."-FA"];$c++;}else{$fa='-';}
if(isset($ClassSkillScore[$row['rowval']."-PS"])){$ps=$ClassSkillScore[$row['rowval']."-PS"];$d++;}else{$ps='-';}
if(isset($ClassSkillScore[$row['rowval']."-LI"])){$li=$ClassSkillScore[$row['rowval']."-LI"];$e++;}else{$li='-';}

$m1+=$m;
$vp1+=$vp;
$fa1+=$fa;
$ps1+=$ps;
$li1+=$li;

			?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $row['gradename']; ?></td>
				<td><?php echo $row['section']; ?></td>
				<td><?php echo $rcount; ?></td>
				<td><?php echo $tcount; ?></td>
				<td><?php echo $fcount; ?></td>
				
				<td><?php echo $m; ?></td>
				<td><?php echo $vp; ?></td>
				<td><?php echo $fa; ?></td>
				<td><?php echo $ps; ?></td>
				<td><?php echo $li; ?></td>
				
				<td><?php echo $tbcount;?></td>
			</tr>
			<?php $i++; }} ?>
			<tr >
				<td colspan="3" class="boldcls">Total</td>
				<td class="boldcls"><?php echo $val1; ?></td>
				<td class="boldcls"><?php echo $val2; ?></td>
				<td class="boldcls"><?php echo $val3; ?></td>
				
				<td class="boldcls"><?php echo round(($m1/$a),2); ?></td>
				<td class="boldcls"><?php echo round(($vp1/$b),2); ?></td>
				<td class="boldcls"><?php echo round(($fa1/$c),2); ?></td>
				<td class="boldcls"><?php echo round(($ps1/$d),2); ?></td>
				<td class="boldcls"><?php echo round(($li1/$e),2); ?></td>
				
				<td class="boldcls"><?php echo "Avg BSPI: ".round(($val4/$k),2); ?></td>
			</tr>
		</tbody>
</table>
<!--<br/>
<table  class="table table-bordered table-condensed table-hover table-striped">
		<thead>
			<tr>
				<th>s.no</th>
				<th>Grade</th>
				<th>Best Assessment 1</th>
				<th>Best Assessment 2</th>
				<th>Best Assessment 3</th>
			</tr>
		</thead>  
		<tbody>
		<?php $i=1; foreach($gradesection as $row1){ //
				$arr=$BestPuzzleSet[$row1['gradename']];
				arsort($arr);
		//echo "<pre>";print_r($arr);exit;
		if(array_filter($arr)){
		?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $row1['gradename']; ?></td>
				<?php foreach($arr as $key=>$val){
				if($val!=''){?>
					<td><?php echo $key."(".$val.")"; ?></td>
				<?php } else { ?>
					<td><?php echo '-'; ?></td>
				<?php } ?>
				<?php } ?>
			</tr>
		<?php  $i++;}} ?>
			

		</tbody>
</table>-->