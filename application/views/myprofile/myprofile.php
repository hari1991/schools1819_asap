<div class="my_profile">
 <div class="container">
 <h2>My Profile:</h2>
 <div class="form_sec">
 <form action="<?php echo base_url(); ?>index.php/home/myprofile#View" class="cmxform" method="POST" id="profilefrm" enctype="multipart/form-data" name="addstudent" accept-charset="utf-8" >
<?php if($updatemsG!=''){ ?> <div class="alert alert-success fade in"  ><strong><?php echo $updatemsG; ?></strong></div><?php } ?>
<input type="hidden" name="id" id="id" value="<?php echo $myprofile[0]['id']; ?>" />
<input type="hidden" name="oldpass" id="oldpass" value="<?php //echo $password;?>" />
<div class="row">
 
  <div class="col-md-6 col-sm-6 col-xs-12">
   <div class="plan_top"> Personal Information </div>
   <div class="fieldlabel">Name<span style="color:red">*</span></div>
    <div class="form-group">
   
    <div class="col-sm-10"> 
      <input type="text" name="sfname" value="<?php echo $myprofile[0]['fname'];?>" class="form-control" id="name"  placeholder="Name">
    </div>
  </div>
  <div class="fieldlabel">Father name</div>
    <div class="form-group">
   
    <div class="col-sm-10"> 
      <input type="text" class="form-control" value="<?php echo $myprofile[0]['father'];?>" name="fathername" id="fathername"  placeholder="Father Name">
    </div>
  </div>
 <div class="fieldlabel">Mother name</div>
  <div class="form-group">
    <div class="col-sm-10">
       <input type="text" class="form-control" name="mname" id="mname" value="<?php echo $myprofile[0]['mother'];?>"  placeholder="Mother's Name">
    </div>
  </div>
  <div class="form-group" id="radioval">
  
    <div class="col-sm-4" > 
	 <div class="heading_c1">Gender</div>
      <label class="radio-inline"><input type="radio" id="gender" value="male" name="gender" <?php if($myprofile[0]['gender']!='' && $myprofile[0]['gender']=='male'){ ?> checked="checked" <?php } ?>>Male</label>
<label class="radio-inline"><input type="radio" id="gender" value="female" name="gender" <?php if($myprofile[0]['gender']!='' && $myprofile[0]['gender']=='female'){ ?> checked="checked" <?php } ?> >Female</label>

    </div>
  </div>
  <div class="fieldlabel">Date of birth<span style="color:red">*</span></div>
  <div class="form-group">   
    <div class="col-sm-10">
       <input type="text" class="form-control" value="<?php echo  $myprofile[0]['dob'];?>" name="dob" id="dob"  placeholder="Date Of Birth">
    </div>
  </div>
  <div class="fieldlabel">Grade</div>
    <div class="form-group">   
    <div class="col-sm-10">
  <select class="form-control" name="grade" id="sel1">
    <option value="<?php echo $getgrade[0]['id']; ?>"><?php echo $getgrade[0]['classname'];?></option>
  </select>

    </div>
  </div>
   <div class="fieldlabel">School</div>
    <div class="form-group">   
    <div class="col-sm-10">
       <input type="text" name="schoolname" value="<?php echo $myprofile[0]['schoolname']; ?>" readonly class="form-control" id="schoolname"  placeholder="School name">
    </div>
  </div>
   <div class="fieldlabel">Email ID<span style="color:red">*</span></div>
    <div class="form-group">   
    <div class="col-sm-10">
        <input type="email" class="form-control" value="<?php echo  $myprofile[0]['email'];?>"  name="email" id="email" placeholder="Email ID">
    </div>
  </div>
  <div class="fieldlabel">Mobile Number<span style="color:red">*</span></div>
      <div class="form-group">   
    <div class="col-sm-10">
       <input type="text" class="form-control" name="sphoneno" value="<?php echo  $myprofile[0]['mobile'];?>" id="sphoneno" placeholder="Phone number">
    </div>
  </div>
  <div class="fieldlabel">Address<span style="color:red">*</span></div>
        <div class="form-group">   
    <div class="col-sm-10">
       <textarea class="form-control" rows="5" name="saddress" id="comment" placeholder="Address"><?php echo $myprofile[0]['address']; ?></textarea>
    </div>
  </div>
  


  </div>
  
  <div class="col-md-4 col-sm-4 col-xs-12">
  
  
  <div class="plan_top">Profile Image</div>
  <div class="form-group">
    <div class="col-sm-10"> 
	
	<div class="">
 
  <div class="sec_in_L">
  <ul>
  <?php 
  if($myprofile[0]['avatarimage']==""){$myprofile[0]['avatarimage']="assets/images/avatar.png";} 
  if(getimagesize(base_url()."".$myprofile[0]['avatarimage'])== false){  $myprofile[0]['avatarimage']="assets/images/avatar.png";} 
  ?>
  <li class="avatar"> <img src="<?php echo base_url(); ?><?php echo $myprofile[0]['avatarimage']; ?>" id="previewing" alt="Profile image" width=89px; height=89px; />
   <a id="pImage" href="javascript:;" title="Upload Profile Image"><img src="<?php echo base_url(); ?>assets/images/photo-icon.png" width="21" height="21" class="photoicon" alt="Upload Profile Image"></a>
   <input type="file" accept="image/*" class="form-control" value="" id="fileUpload" style="display:none;" name="file" >
	 
   </li> 
   <li id="message"></li>
   </ul>

  </div>

  </div>
 
    </div>
  </div>
  
  <div class="plan_top"> Login Detail </div>
   <br/>
  <div class="fieldlabel">User Name</div>
    <div class="form-group">
   
    <div class="col-sm-10"> 
      <input type="text" class="form-control" value="<?php echo $myprofile[0]['username']; ?>" id="username" readonly name="emailid"  placeholder="User Name">
    </div>
  </div>
  <div class="fieldlabel">New Password</div>
    <div class="form-group">
   
    <div class="col-sm-10"> 
      <input type="password" class="form-control" id="newpass" name="newpass"  placeholder="New Password">
    </div>
  </div>
  <div class="fieldlabel">Confirm Password</div>
  <div class="form-group">
     
    <div class="col-sm-10">
       <input type="password" class="form-control" id="cpass" name="cpass"   placeholder="Confirm Password">
	   <label id="pwdmsg"></label>
    </div>
  </div>
 <!--
   <div class="form-group">
    <div class="col-sm-10">
      <div class="plan_bold"> Plan Details </div>
    </div>
  </div>
      
<div class="fieldlabel">Plan Name</div>
    <div class="form-group">   
    <div class="col-sm-10">
       <input type="text" class="form-control" id="planname" value="<?php echo $getplandetails[0]['name']; ?>" name="planname" readonly  placeholder="Plan name">
    </div>
  </div>
    
	<div class="fieldlabel">Validity in days</div>
      <div class="form-group">   
    <div class="col-sm-10">
       <input type="number" class="form-control" value="<?php echo $getplandetails[0]['validity']; ?>" id="phonenumber" readonly  placeholder="Validity">
    </div>
  </div>
  <div class="fieldlabel">Expiry date</div>
        <div class="form-group">   
    <div class="col-sm-10">
       <input type="text" class="form-control" value="<?php echo date("d-m-Y", strtotime($myprofile[0]['planenddate'])); ?>" id="expiry" readonly  placeholder="Expiry">
    </div>
  </div>
        
-->




  </div>
 <div class="form-group"> 
    <div class="col-sm-12">
      <button type="submit" id="studentsubmit" class="btn btn-default Submit">Save Your Details</button>
	  <input type="hidden" id="studentsubmite" name="studentsubmite" value="Save">
    </div>
  </div>
 
  </form>
  </div>
  </div><!--/form_sec -->
 </div><!-- container -->
 </div>
 <style>
 body{min-height:0 !important;} #radioval .error{padding-top: 32px;}
 </style>
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
 <script>
 $("#pImage").click(function(){
	$("#fileUpload").click(); 
 });
 
 $('#fileUpload').change(function(){
$("#message").empty();
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
//
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{

$("#message").html("<p id='error' style=color:red; >Please select a valid image file</p>");
return false;
}
else if(file.size>1024*1024){
	/*
	var image = new Image();
image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (height > 150 || width > 150) {
          $("#message").html("<p id='error' style=color:red; >Please upload image below 150*150</p>");

                        return false;
                    }
}
*/
					
$("#message").html("<p id='error' style=color:red; >Please select a file less than 1 MB</p>");
return false;
	
}
else
{
$('#previewing').attr('src','schools_admin_student_report.png');
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}


 });
 
 function imageIsLoaded(e) {
$("#file").css("color","green");
$('#image_preview').css("display", "block");
$('#previewing').attr('src', e.target.result);
$('#previewing').attr('width', '89px');
$('#previewing').attr('height', '89px');
};

dob = $( "#dob" )
        .datepicker({
			 dateFormat: 'dd-mm-yy' ,maxDate: "31-12-2013" ,minDate: "01-01-1991" ,yearRange: "1991:2013",
			 onSelect: function(value, ui) {
        var today = new Date(),
            dob = new Date(value),
            age = new Date(today - dob).getFullYear() - 1970;
			},
          changeMonth: true, changeYear: true
        });
		
		jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 7 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "Please specify a valid mobile number");
		jQuery.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-z]+$/i.test(value);
		}, "Letters only please"); 

		$("#profilefrm").validate({
        rules: {
            "sfname": {required: true,minlength: 3},
			"fathername":{lettersonly:true},
			"mname":{lettersonly:true},
			"email": {required: true,email: true},
			"newpass": {minlength: 8},
			"cpass": {equalTo: "#newpass"},
            "dob": {required: true},
            "gender": {required: true},
            "sphoneno": {required: true,phoneno:true,minlength:8},
            "saddress": {required: true}           
        },
        messages: {
            "sfname": {required: "Please enter first name"},
			"email": {required: "Please enter email",email: "Please enter valid email"},
			"newpass": {required: "Please enter password"},
			"cpass": {required: "Please confirm password",equalTo: "Please enter valid confirm password"},
			"dob": {required: "Please select date of birth"},
			"sphoneno": {required:"Please enter mobile",
					minlength:"Enter minimum 8 number"
			},
            "gender": {required: "Please select gender"},
            "saddress": {required: "Please enter address"}
			
          
        },
		errorPlacement: function(error, element) {
    if (element.attr("type") === "radio") {
        error.insertAfter(element.parent().parent());
    } 
	else if (element.attr("id") === "txtMobile") {
        error.insertAfter(element.parent());
    } else {
        error.insertAfter(element);
    }
},
		highlight: function(input) {
            $(input).addClass('error');
        } 
});
	
$("#studentsubmit").click(function(){
if( $("#profilefrm").valid())
{
}
});


 </script>