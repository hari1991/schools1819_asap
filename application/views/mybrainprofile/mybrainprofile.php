     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="my_profile noPHeight">
 <div class="container">



<!--
<div class="skills">
   
    <div class="skillsLeft">Skills:</div> 
  <div class="skillsRight">
   <form>
<ul>
 
   <li><label class="rad">
  <input type="checkbox" name="r1" value="a" class="radiomemory" />
  <i></i> <span>Memory</span>
</label></li>
   <li><label class="rad">
  <input type="checkbox" name="r1" value="b" class="radiovisual"/>
  <i></i> <span>Visual Processing</span>
</label></li>
   <li><label class="rad">
  <input type="checkbox" name="r1" value="b" class="radiofocus"/>
  <i></i> <span>Focus and Attention</span>
</label></li>
   <li><label class="rad">
  <input type="checkbox" name="r1" value="b" class="radioproblem"/>
  <i></i> <span>Problem Solving</span>
</label></li>
   <li><label class="rad">
  <input type="checkbox" name="r1" value="b" class="radiolinguistics"/>
  <i></i> <span>Linguistics</span>
</label></li>
</li>
</ul>
</form> 
  </div>
  </div>
  -->
  <div class="form_sec">
  <div class="row">
<div class="bspiCalender">
 <h2>BSPI Calender</h2> 

 
<div class="bspiCalenderTop">
<span class="leftText">Know your Everyday BSPI score in the below calendar</span>

<div class="month">
 <div class="box">
      <select name="ddlMonth" id="ddlMonth">
	  <?php foreach($academicmonths as $months){ ?>
	  <option <?php if(date("n")==$months['monthNumber']) { ?> selected="selected" <?php } ?> value="<?php echo $months['yearNumber']."-".$months['monthNumber'];?>"><?php echo date("F", mktime(null, null, null, $months['monthNumber']));; ?></option>

	  <?php } ?>
        
      </select>
    </div>
</div>
</div> 
<div class="clearfix"></div>
  
 <div  id="myBspiCalendar">
 </div> 
  
  
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>



<script>
 
	 
	 function bspichart(memory,visualpprocessing,focusattention,problemsolving,linguistics)
	 {
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });
    // Set up the chart
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'BrainProfileChart',
            type: 'column',
			backgroundColor:'transparent',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 30 
            }
        },
        title: {
            text: ''
        },
		tooltip: {enabled: false},exporting:false,credits: {
      enabled: false
  },
        subtitle: {
            text: ''
        },
		 
		yAxis:{
		  title: {
                text: 'Score'
            },
		 max: 100 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0
},
       xAxis:{ 
	    categories: [
                'Memory',
                'Visual Processing',
                'Focus and Attention',
				'Problem Solving',
				'Linguistics'],
	   
  gridLineWidth: 0,
  minorGridLineWidth: 0
},
        plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
        series: [{
 showInLegend: false, 
            data: [{y:memory,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#ff6600"],[1, Highcharts.Color("#ff6600").brighten(-0.3).get('rgb')]]}}, {y:visualpprocessing,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#067d00"],[1, Highcharts.Color("#067d00").brighten(-0.3).get('rgb')]]} },{y:focusattention,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#ffbe00"],[1, Highcharts.Color("#ffbe00").brighten(-0.3).get('rgb')]]}},{y:problemsolving,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#be0000"],[1, Highcharts.Color("#be0000").brighten(-0.3).get('rgb')]]}},{y:linguistics,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#1e9ddf"],[1, Highcharts.Color("#1e9ddf").brighten(-0.3).get('rgb')]]}}]
        }]
    });

	 }
	 
 
</script>
<script>
getMonthName = function (v) {
    var n = ["","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return n[v]
}

    bspiCalendar("<?php echo date('Y-m');?>");
  $("#ddlMonth").change(function(){
	   bspiCalendar($(this).val()); 
  });
  bspiCalendar("<?php echo date('Y-m');?>");
  function bspiCalendar(yearMonthData)
  {
  $.ajax({
	  url: "<?php echo base_url(); ?>index.php/home/ajaxcalendar",
	  data:{yearMonth:yearMonthData},
	  success: function(result){
        $("#myBspiCalendar").html(result);
		}
	});
	$.ajax({
	  url: "<?php echo base_url(); ?>index.php/home/ajaxcalendarSkillChart",
	  dataType:"json",
	  data:{yearMonth:yearMonthData},
	  success: function(result){
		  //alert(result.SID59);
		  var monthYear=[];
		  monthYear=(yearMonthData.split("-"));
		  $("#MonthID").html("My Brain Profile "+getMonthName(parseInt(monthYear[1]))+" - "+monthYear[0]);
         bspichart(result.SID59,result.SID60,result.SID61,result.SID62,result.SID63);
		}
	});
  }
  </script>
 <div class="myBrainProfile">
 <h2 id="MonthID"></h2>
 <br/>
 <div id="BrainProfileChart">
</div>
 
 
 
 </div> 

 
 </div>
 </div>    
 </div>   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 </style>