(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#993399").s().p("AAgA5IgggSIggASQgBAAAAAAQgBABgBAAQAAAAgBAAQAAAAgBgBIgBgDIAAgCIAHgkIgbgYIgCgFIAAgBQABgCAEAAIAkgFIAQghQAAAAAAgBQAAAAAAAAQAAgBABAAQAAAAABgBIABAAIACAAIACADIAPAhIAkAFQAFAAABACIAAABQAAACgDADIgaAYIAHAkIAAACIgCADIgCABIgDgBg");
	this.shape.setTransform(-265.1,37.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCC00").s().p("AAcAzIgcgQIgcAQQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAgBQAAAAAAAAQgBgBAAAAIAAgCIAGggIgYgVIgBgEIAAgBQAAAAAAgBQABAAAAAAQABgBAAAAQABAAABAAIAggEIAPgdQAAgBAAAAQAAgBAAAAQAAgBAAAAQABAAAAAAIABAAIACAAIACADIANAdIAhAEQABAAAAAAQABAAABABQAAAAABAAQAAABAAAAIAAABIgCAEIgYAVIAGAgIAAACIAAADIgCAAIgEAAg");
	this.shape_1.setTransform(-141.1,-4.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("AAkA/IgkgTIgjATQgDACgCgBQgBgBAAAAQgBgBAAAAQAAgBAAAAQgBgBAAAAIABgCIAHgoIgegbQAAgBAAgBQgBAAAAgBQAAAAAAgBQAAAAAAgBIAAgBQABgCADgBIApgFIASglIABgDIACAAIADAAIACADIARAlIAoAFQAFABABACIAAABQgBACgCADIgeAbIAIAoIAAACQAAAAAAABQAAAAAAABQAAAAAAABQgBAAAAABIgCAAIgEgBg");
	this.shape_2.setTransform(-236.9,-48.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#997B33").s().p("AAgA5IgggSIggASQgBAAAAAAQgBABgBAAQAAAAgBAAQAAAAgBgBIgBgDIAAgCIAHgkIgbgYIgCgFIAAgBQABgCAEAAIAkgFIAQghQAAAAAAgBQAAAAAAAAQABgBAAAAQAAAAABgBIABAAIACAAIACADIAPAhIAkAFQAFAAABACIAAABQAAACgDADIgaAYIAHAkIAAACIgCADIgCABIgDgBg");
	this.shape_3.setTransform(-365.6,-49.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#6666CC").s().p("AA1BfIg1gdIg2AdQgEADgDgCQgDgDAAgDIABgDIALg8IgtgpQgDgEAAgDIAAgCQABgDAGgBIA8gIIAbg3QAAgDADgCIADAAIAEAAIADAFIAZA3IA8AIQAIABABADIAAACQAAADgDAEIgtApIAMA8IAAADQAAADgDADIgDAAIgGgBg");
	this.shape_4.setTransform(256.6,45.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#993399").s().p("AAgA5IgggSIggASQgBAAAAAAQgBABgBAAQAAAAgBAAQAAAAgBgBIgBgDIAAgCIAHgkIgbgYIgCgFIAAgBQABgCAEAAIAkgFIAQghQAAAAAAgBQAAAAAAAAQABgBAAAAQAAAAABgBIABAAIACAAIACADIAPAhIAkAFQAFAAABACIAAABQAAACgDADIgaAYIAHAkIAAACIgCADIgCABIgDgBg");
	this.shape_5.setTransform(290.2,-23);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF9933").s().p("A8QG6IgWgMIgVAMQgBAAAAAAQgBAAAAAAQAAAAgBAAQAAAAAAAAIgCgCIABgCIAEgYIgSgQIgBgDIAAgBIADgCIAYgDIALgWIABgCIABAAIACAAIABACIAKAWIAYADIAEACIAAABIgBADIgSAQIAEAYIABACIgCACIgBAAIgCAAgA4XkvIgkgUIgkAUQgDACgCgCQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAAAgBIAAgCIAIgoIgegcQgBAAAAgBQAAgBgBAAQAAgBAAAAQAAgBAAAAIAAgCQABgCAEAAIAogGIASgkQAAgBAAAAQAAgBAAAAQABgBAAAAQAAAAABgBIACAAIADAAIACAEIARAkIAoAGQAFAAABACIAAACQAAACgDACIgdAcIAHAoIABACQAAABAAAAQgBABAAAAQAAABAAAAQgBABAAAAIgCABIgEgBgAc3laIgbgOIgbAOQgBABAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAIgCgDIABgCIAFgeIgWgVIgCgDIAAgBQABgBAAAAQAAAAABgBQAAAAABAAQAAAAABAAIAegEIAOgcQAAAAAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIACAAIACAAIABACIANAcIAeAEQABAAABAAQABAAAAAAQABABAAAAQABAAAAABIAAABIgCADIgWAVIAFAeIABACIgCADIgBAAIgDgBg");
	this.shape_6.setTransform(184.7,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-371.7,-55.3,743.5,110.8);


(lib.nplnts = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1CC68D").s().p("AhHBTQgKgKAAgOQAAgOAKgLQAKgKAOABQAOgBAKAKQALALAAAOQAAAOgLAKQgKAKgOAAQgOAAgKgKgAArg2QgGgHAAgJQAAgJAGgHQAHgGAJAAQAKAAAGAGQAHAHAAAJQAAAJgHAHQgGAHgKAAQgJAAgHgHg");
	this.shape.setTransform(-443.2,-0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#49E8B3").s().p("AhzBmQgPgPAAgWQAAgWAPgQQAQgQAWAAQAWAAAQAQQAQAQAAAWQAAAWgQAPQgQAQgWAAQgWAAgQgQgAhkAoQgKALAAAOQAAAOAKAKQAKAKAOAAQAOAAAKgKQAKgKAAgOQAAgOgKgLQgKgKgOAAQgOAAgKAKgAA7BUQgMgMAAgSQAAgRAMgNQANgMARAAQASAAAMAMQAMANAAARQAAASgMAMQgMAMgSAAQgRAAgNgMgABGAjQgIAIAAAMQAAALAIAHQAIAIALAAQALAAAIgIQAIgHAAgLQAAgMgIgIQgIgHgLAAQgLAAgIAHgAAFgnQgJgKAAgOQAAgPAJgLQALgKAOAAQAPAAAKAKQAKALAAAPQAAAOgKAKQgKALgPAAQgOAAgLgLgAAOhQQgHAHAAAJQAAAJAHAHQAHAHAJAAQAJAAAHgHQAGgHAAgJQAAgJgGgHQgHgGgJAAQgJAAgHAGgAhchTQgFgGAAgIQAAgJAFgGQAGgFAJAAQAIAAAGAFQAGAGAAAJQAAAIgGAGQgGAGgIAAQgJAAgGgGgAhXhqQgDAEAAAFQAAAGADADQAEAEAGAAQAFAAAEgEQADgDAAgGQAAgFgDgEQgEgDgFAAQgGAAgEADg");
	this.shape_1.setTransform(-440.3,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#17AF7C").s().p("AihCJQAoAVAwAAQBQAAA6g6QA5g5AAhQQAAhRg5g5IgKgJQAaANAXAXQA6A6AABRQAABQg6A5Qg4A5hQAAQhLAAg2gwgAiAhqQgEgDABgGQgBgFAEgEQAEgDAFAAQAFAAAEADQAEAEAAAFQAAAGgEADQgEAEgFAAQgFAAgEgEg");
	this.shape_2.setTransform(-436.1,1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#33CC99").s().p("AhrChIgKgJQg5g5gBhRQAAgmAOghQAOgkAcgdIACgCQA5g5BQAAQAwAAAoAVIAKAJQA5A5AABRQAABQg5A5Qg5A6hQAAQgwAAgogVgAhnAhQgPAQAAAWQAAAWAPAPQARAQAWAAQAVAAAQgQQAQgPAAgWQAAgWgQgQQgQgQgVAAQgWAAgRAQgABIAeQgMANAAARQAAASAMAMQAMAMARAAQASAAAMgMQANgMAAgSQAAgRgNgNQgMgMgSAAQgRAAgMAMgAARhTQgJALAAAPQAAAOAJAKQALALAPAAQAOAAAKgLQALgKAAgOQAAgPgLgLQgKgKgOAAQgPAAgLAKgAhQhqQgFAGAAAJQAAAIAFAGQAHAGAIAAQAIAAAGgGQAGgGAAgIQAAgJgGgGQgGgFgIAAQgIAAgHAFgABSBPQgHgHgBgLQABgMAHgIQAIgHALAAQAMAAAHAHQAIAIAAAMQAAALgIAHQgHAIgMAAQgLAAgIgIg");
	this.shape_3.setTransform(-441.5,-1.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#8CBB01").s().p("AihCJQAoAVAwAAQBQAAA6g6QA5g5AAhQQAAhRg5g5IgKgJQAaANAXAXQA6A6AABRQAABQg6A5Qg4A5hQAAQhLAAg2gwg");
	this.shape_4.setTransform(-134.2,1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B1E80E").s().p("AAfB+QgKgDAAgFQAAgFAKgDQAKgEAPAAQAOAAAKAEQAKADAAAFQAAAFgKADQgKADgOABQgPgBgKgDgAhXBHQgVgFAAgJQAAgIAVgHQAVgFAeAAQAeAAAUAFQAVAHAAAIQAAAJgVAFQgUAHgegBQgeABgVgHgABiAoQgGgBAAgBQAAgBAGgCIAQgBIAPABQAGACAAABQAAABgGABIgPABIgQgBgAAsgdQgJgDAAgDQAAgDAJgDQAKgDANAAQANAAAJADQAJADAAADQAAADgJADQgJADgNAAQgNAAgKgDgAhGgsQgDgDAAgFQAAgGADgDQAEgEAFAAQAFAAAEAEQAEADAAAGQAAAFgEADQgEAEgFAAQgFAAgEgEgAALhJQgIgDAAgEQAAgEAIgDQAHgEALAAQAKAAAIAEQAHADAAAEQAAAEgHADQgIAEgKAAQgLAAgHgEgAh5hvQgNgDAAgDQAAgDANgDQAOgDAUAAQATAAAOADQAOADAAADQAAADgOADQgOADgTAAQgUAAgOgDgABQh2QgIgBAAgDQAAgDAIgCQAHgCALAAQALAAAIACQAHACAAADQAAADgHABQgIACgLABQgLgBgHgCg");
	this.shape_5.setTransform(-139,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#99CC00").s().p("AhrChIgKgJQg5g5gBhRQABhQA5g6QA5g5BQAAQAwAAAoAVIAKAJQA5A5AABRQAABQg5A5Qg5A6hQAAQgwAAgogVgAAlB7QgKAEAAAFQAAAFAKADQAKADAPAAQAOAAAKgDQAKgDAAgFQAAgFgKgEQgKgDgOAAQgPAAgKADgAhRA4QgVAGAAAJQAAAIAVAGQAVAGAeAAQAeAAAUgGQAVgGAAgIQAAgJgVgGQgUgGgeAAQgeAAgVAGgABoAxQgGABAAACQAAABAGABIAPABIAQgBQAGgBAAgBQAAgCgGgBIgQgBIgPABgAAygcQgJADAAAEQAAADAJADQAKADAMAAQAOAAAIgDQAKgDAAgDQAAgEgKgDQgIgCgOAAQgMAAgKACgAhAgwQgEAEAAAFQAAAFAEAEQAEAEAFAAQAFAAAEgEQADgEAAgFQAAgFgDgEQgEgDgFAAQgFAAgEADgAARhKQgIADAAAFQAAAEAIADQAHADALAAQAKAAAIgDQAHgDAAgEQAAgFgHgDQgIgDgKAAQgLAAgHADgAhzhuQgNADAAAEQAAADANADQAOADAUAAQATAAANgDQAOgDAAgDQAAgEgOgDQgNgCgTAAQgUAAgOACgABVhxQgHACAAADQAAACAHACQAIACALAAQALAAAIgCQAHgCAAgCQAAgDgHgCQgIgCgLAAQgLAAgIACg");
	this.shape_6.setTransform(-139.5,-1.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00B1FF").s().p("AihCJQAoAVAxAAQAcAAAagHIAUABQAfAAAWgIQAWgHAAgLQAAgKgVgHIAJgJQANgNAKgOIADAAQAJAAAHgDQAHgCAAgEQAAgDgHgDIgIgCQAMgXAGgZQAVgEAIAAIAQACQgGBGgzA0Qg5A5hQAAQhKAAg3gwgABBivIgKgJQAbANAXAXQAqAqALA3QgPAFgVADQgChNg3g3g");
	this.shape_7.setTransform(-271.2,1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#85E7FF").s().p("AAACYQgWgIAAgKQAAgLAWgHQAVgIAfAAQAfAAAWAIIABAAQgiAfgpAMQgSgCgNgFgAB8BOQgHgDAAgDQAAgDAHgDQAHgDAKAAIAIAAIgLASQgIAAgGgDgAARAsQgOAAgDgBIAAgBQAPgEAIgIQAGgFAAgGQAAgGgvABQgvAAAAgIQAAgOAxgIQAWgEBVgGQAwgDAhgGIAAAGQAAAXgFAVIhHANIhOAQgAirg1QAIgiAWgeQAtAKAkgDQA3gFAjAEQAjAEAAAJQAAAHg6AJIg8AKQgLADgCAEQgBADgCABQgIAHhTACIgLgBgAgliSQgIgCAAgDQAAgCAIgCQAIgCALAAQALAAAHACQAIACAAACQAAADgIACQgHABgLAAQgLAAgIgBg");
	this.shape_8.setTransform(-276.2,0.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#00CCFF").s().p("Ah/ChIgKgJQg6g5AAhRQAAgaAHgYIALAAQBTgCAIgGQACgCABgDQACgEALgDIA+gJQA4gKAAgGQAAgKgjgEQgjgEg3AFQgkADgtgJQAKgMALgMQA5g5BQAAQAxAAAoAVIAJAJQA3A3ACBNQghAGgwADQhUAHgXADQgxAHAAAPQAAAIAvAAQAvgBAAAHQAAAFgGAFQgHAIgPAEIAAACQAEABAMAAIABAAIBPgQIBHgNQAEgWAAgXIAAgGQAWgDAOgFQAFAUAAAVIgBAQIgQgCQgIAAgUAEQgHAZgMAXIAJACQAGADAAADQAAAEgGACQgIADgJAAIgDAAIALgRIgIgBQgJAAgHADQgIADAAADQAAAEAIACQAFADAIAAQgKAOgNANIgIAJQAUAHAAAKQAAALgVAHQgXAIgeAAIgWgBQApgMAjgeIgBgBQgXgHgeAAQgfAAgWAHQgWAIABAKQgBALAWAHQAPAFAQACQgZAHgcAAQgxAAgngVgAg2iLQgIACAAACQAAADAIACQAIACALAAQALAAAIgCQAIgCAAgDQAAgCgIgCQgIgCgLAAQgLAAgIACgAB5BhIAAAAg");
	this.shape_9.setTransform(-274.5,-1.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#4343EC").s().p("AiVCTIgMgKQAKAGALAEQAgALAkAAQBQAAA4g6QA6g5AAhQQAAhRg6g5IgJgJQAbANAWAXQA6A6AABRQAABQg6A5Qg5A5hQAAQhCAAgygmg");
	this.shape_10.setTransform(278,1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#6666FF").s().p("AhWCpQA2gEAkgaQAigZAAgaQAAgRgEgFQgEgEgMAAIgeAEQgRAAgBgbQgCgbgLAAQghAAgUArQgVArgUAAIgUgEQgRglAAgtQAAhQA4g5QAtgtA7gJQAOAIAJAMQAPASAAAUQABAOgZATQgYASAAAZQAAAPAPAJQAMAHAXABQAAgBAngMQAngTAAgrQAAgQgMghQgJgWgDgOQAOAEAOAHIAKAJQA5A6ABBRQgBBQg5A5Qg5A5hQAAQgkAAgfgLgAB6gaIgIAQIAAABQAAAKAJAJQAIAJALAAQAIAAACgCQABgDAAgMQAAgdgYAAQgHAAAAABgAhrg3QgGAFAAAJQAAAEAIAHQAJAIALAAQAMAAAFgJQADgHAAgOQABgHgDgEIgFgGIgMAAIgXAOg");
	this.shape_11.setTransform(272.6,-1.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#8B8BFB").s().p("AhpCnIgKgKQgagZgOgfIAUAEQAUAAAVgrQAUgrAhAAQALAAACAbQACAbAQAAIAegEQANAAADAEQAEAFAAARQAAAagiAZQgjAag3AEQgLgEgKgFgAB9ARQgIgJAAgKIAAgBIAHgQQABgBAGAAQAZAAAAAdQAAAMgCADQgBACgJAAQgLAAgIgJgAgHgOQgPgJAAgPQAAgZAYgSQAZgTAAgOQAAgUgQgSQgJgMgOgIQAQgDASAAQAgAAAcAJQADAOAJAWQAMAhAAAQQAAArgnATQgnAMAAABQgWgBgNgHgAhngXQgIgHAAgEQAAgJAHgFIAWgOIAMAAIAFAGQADAEAAAHQAAAOgEAHQgFAJgMAAQgLAAgJgIg");
	this.shape_12.setTransform(272.4,-1.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF6600").s().p("AihCJQAoAVAwAAQAuAAAlgSQAgAMAbAEQgtAdg4AAQhLAAg2gwgAA/BmIACgCQA5g5AAhQIgBgWIAAAAIAhgIQAIAbAAAfQAABDgpAzQgbgBgfgGgAB1hWQgNgygngnIgKgJQAbANAWAXQAcAcAPAiIgOABIgQgBg");
	this.shape_13.setTransform(141,1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF9900").s().p("Ah7ChIgKgJQgLgLgJgMIACgCQANgFAoAAQA7AAA9AeIAaAMQgmASgtAAQgwAAgogVgAAwCkQAcgOAZgYQgdgFgggJQhRgVgaAAQgmAAgUAEQgVAFgUAJQgOgZgGgdIAegBQAqAAAiAMQAiAMAgAAQAvAAAPgPQAagZADgCQgIADgjAGQgiAGgEAAQgXAAgtgSQgpgRgfgCIgCAAQgZABgTADIAAgEQAAguATgmIAhgCQBQAAAwAYQAwAYAiAAQAmAAAxgLQgCgOgDgNIARABIAOgBIAGATIggAIIAAAAIABAWQAABQg6A5IgBACQAfAGAbABIgRATQgQAQgTAMQgcgEgfgMgABAhSQg9gUgiAAQgdAAghAKQghALgRAAQgIAAgNgDQAMgVATgTQA5g5BQAAQAxAAAoAVIAJAJQAnAnANAyQgogDgygRg");
	this.shape_14.setTransform(137.2,-1.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFCC00").s().p("AAnB6Qg+geg7AAQgnAAgNAFIgDACQgHgKgFgKQATgKAVgEQAUgFAnABQAaAABRAVQAfAJAdAFQgZAYgcAOIgZgMgAhAAkQgigMgqAAIgeABQgDgSgBgTQATgCAZgBIACAAQAfACApAPQAtATAXAAQAEAAAigGQAjgGAIgDQgDACgZAYQgQAQgwAAQgfAAgigMgAAFhOQgvgYhQAAIghACIAHgPQANADAIAAQARAAAigKQAggLAeAAQAhAAA+AVQAxARApADQACANACAOQgwALgnAAQgigBgxgXg");
	this.shape_15.setTransform(135.6,1.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FD90B4").s().p("AAcB3QgKgJAAgTQAAgZAaAAIARACQAKAHABAEQABACAAAPQAAAegSAAQgSAAgJgHgAAAA8QAAAAgBAAQAAAAgBAAQAAAAgBAAQAAAAAAAAQgYgEgHgZQgNgxgdgfQgGgGgUAAQgZAAgNgBQgNgBgJgEQAOgjAcgdQAZAPAPABQAQABBAgEQAhAAAJAGQAJAGAAAXQAAAGgNAEQgNADAAAJQAAAFASATQASASAAAgQAAAighAHIgZABgAB8gRQgBgDAAgMQAAgFACgFQAEgHAEAAIABAAQABgBAHAAQANAAAFAIQADAFAAANQAAAFgKAFQgJAFgHAAQgKAAgDgIg");
	this.shape_16.setTransform(-2,-1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF6699").s().p("AhrChIgKgJQg6g5AAhRQABgmANghQAJAEAMABQANABAagBQAUABAGAGQAdAfANAxQAHAYAXAFQABAAAAgBQAAAAABAAQABAAAAAAQAAAAABAAIACABIAagBQAhgGAAgiQAAghgSgSQgSgSAAgFQAAgJANgEQANgDAAgHQAAgXgJgFQgJgGgiAAQg/AEgQgCQgPgBgZgPIACgCQA5g5BQAAQAxAAAoAVIAJAJQA6A5gBBRQABBQg6A5Qg5A6hQAAQgxAAgngVgAATBfQAAATAKAIQAJAIASAAQASAAAAgeQAAgPgBgCQgCgFgJgGIgRgCQgaAAAAAZgAB+gnQgDAGAAAFQAAAMACADQACAHAKAAQAIAAAJgFQAKgFAAgEQAAgNgDgFQgFgIgNAAQgIAAAAABIgBAAQgFAAgDAGg");
	this.shape_17.setTransform(-2,-1.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#EE3271").s().p("AihCJQAoAVAwAAQBQAAA5g6QA6g5AAhQQAAhRg6g5IgJgJQAbANAXAXQA5A6AABRQAABQg5A5Qg5A5hRAAQhJAAg3gwg");
	this.shape_18.setTransform(3.3,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.nplnts, new cjs.Rectangle(-459,-19.5,753.2,39.1), null);


(lib.fire = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC00").s().p("AgrB6Qgbg5gHg/IgXAVQgEgcgGgcQgShUAwg/QANgqAngUQARgIAPgBQgLAGgDAnIAAAAQgYAMgIAaQgCAHgEAGQglAxAPBAIAAABIABAFQAKACAIAHQAMAKACAPQAGA3AYA0IAJAYQAIgXAFgZIAAABQAEgbgFgbQgDgPAIgNQAHgMAPgFIAIgCQACgeAMgeQAIgXAEgaQAGgwgrg3QgOgSgKgDQAbAAAaARQA6AngLBKQgEAegKAbQgVA4AOA4IgHgCQgLAAgJgIQgIgIgFgJQAHAigGAjQgRBag3BKQAEhFgdg+g");
	this.shape.setTransform(12.8,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("AgHCnQgYg0gGg4QgCgPgMgKQgIgHgKgCIgBgFIAAgBQgPg/AlgxQAEgGACgHQAIgaAYgMIAAAAQADgnALgGIABgBQgDAFgBANQgCAugGAHQgGAGgFAPQgDAKgHAKQgcAlAJAvQAGADAGAGIABAAQASAQAEAZQAAAJACAIQACgMAHgLIAAgBQAIgRATgJQAEgYAKgZIgBABQAIgVADgXQAEgggZg6QgLgZgGgGIACAAQAKADAOASQArA3gGAwQgEAagIAXQgMAdgCAeIgIACQgPAFgHANQgIANADAPQAFAbgEAbIAAgBQgFAZgIAXIgJgYg");
	this.shape_1.setTransform(12.8,-5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6600").s().p("AgMBzQgEgZgSgQIgBAAQgGgGgGgDQgJgwAcgkQAHgKADgKQAFgPAGgGQAGgHACguQABgNADgFIAHAAQAGAGALAZQAZA6gEAgQgDAWgIAVIABgBQgKAZgEAZQgTAJgIARIAAABQgHALgCAMQgCgIAAgJg");
	this.shape_2.setTransform(12.8,-10.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF9900").s().p("AgHCkQgYg0gGg4QgCgPgMgKQgIgHgKgCIgBgFIAAgBQgPg/AlgxQAEgGACgHQAIgaAYgMIAAAAQAGglANgDQgDAFgBAMQgFAugBABQgMAHgFAPQgDAKgHAKQgcAlAJAvQAGADAGAGIABAAQASAQAEAZQAAAJACAIQACgMAHgLIAAgBQAIgRATgJQAEgYAKgZIgBABQAIgVADggQAEghgWgzQgGgNgFgHQAJAFALANQAoAygGAwQgEAagIAXQgMAdgCAeIgIACQgPAFgHANQgIANADAPQAFAbgEAbIAAgBQgFAZgIAXIgJgYg");
	this.shape_3.setTransform(-12.8,-7.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF6600").s().p("AgMBxQgEgZgSgQIgBgBQgGgFgGgDQgJgxAcgkQAHgJADgLQAFgOAMgIQABAAAFguQABgNADgFQAFgCAHAFQAFAGAGAOQAWAzgEAgQgDAggIAUIABAAQgKAZgEAZQgTAIgIASIAAAAQgHAMgCAMQgCgJAAgIg");
	this.shape_4.setTransform(-12.8,-13);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFCC00").s().p("AgrB6Qgbg5gHg/IgXAVQgEgcgGgcQgShUAwg/QANgqAngUQAxgYAuAfQA6AngLBKQgEAegKAbQgVA4AOA4IgHgCQgLAAgJgIQgIgIgFgJQAHAigGAjQgRBag3BKQAEhFgdg+gAgqioQgCAHgEAGQglAxAPBAIAAABIABAFQAKACAIAHQAMAKACAPQAGA3AYA0IAJAYQAIgXAFgZIAAABQAEgbgFgbQgDgPAIgNQAHgMAPgFIAIgCQACgeAMgeQAIgXAEgaQAGgwgogyQgLgNgJgFQgHgEgFACQgNADgGAlIAAAAQgYAMgIAag");
	this.shape_5.setTransform(-12.8,-1.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-26.5,48.8,53.1);


(lib.strs = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0.211},9).to({alpha:1},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-371.7,-55.3,743.5,110.8);


(lib.rkt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B0B29D").s().p("AgCAGIgCgBIAAAAIgCgBIAAgBIgBAAIABgDQAAgBAAgBQAAgBAAAAQABgBAAAAQAAAAABAAIAAgBIAJAAIADACIAAAFIAAACIgBABIgBAAQgBAAAAABQAAAAAAAAQgBAAAAAAQAAgBAAAAIgBABIgDAAIgCAAg");
	this.shape.setTransform(398.9,179.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#909281").s().p("AgMAOQgHgGAAgIQAAgHAHgGQAFgFAHAAQAIAAAGAFQAFAGABAHQgBAIgFAGQgGAFgIAAQgHAAgFgFgAgFgFQgBAAAAAAQgBABAAAAQAAABAAAAQgBABAAABIgBADIACAAIAAABIACABIAAAAIACABIAFAAIABgBQAAABAAAAQAAAAAAAAQABAAAAAAQAAAAABgBIABAAIAAgBIABgCIgBgFIgCgCIgJAAg");
	this.shape_1.setTransform(399.1,179.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AgVAWQgIgKAAgMQAAgLAIgJQAKgJALAAQAMAAAJAJQAJAJAAALQAAAMgJAKQgJAIgMAAQgLAAgKgIgAgMgNQgHAGAAAHQAAAIAHAGQAFAFAHAAQAIAAAGgFQAFgGABgIQgBgHgFgGQgGgFgIAAQgHAAgFAFg");
	this.shape_2.setTransform(399.1,179.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B0B29D").s().p("AgCAGIgCgBIAAAAIgCgBIAAgBIgBAAIABgDQAAgBAAgBQAAgBAAAAQABgBAAAAQAAgBABAAIAAAAIAJAAIADACIAAAFIAAACIgBABIgBAAQgBABAAAAQAAAAAAAAQgBAAAAAAQAAAAAAgBIgBABIgDAAIgCAAg");
	this.shape_3.setTransform(398.9,262.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#909281").s().p("AgMAOQgHgGAAgIQAAgHAHgFQAFgHAHABQAIgBAGAHQAFAFABAHQgBAIgFAGQgGAGgIgBQgHABgFgGgAgFgGQgBABAAAAQgBABAAAAQAAABAAAAQgBABAAABIgBADIACAAIAAABIACACIAAAAIACAAQADABACgBIABAAQAAAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAIABgBIAAgBIABgCIgBgFIgCgCIgJAAg");
	this.shape_4.setTransform(399.1,262.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#666666").s().p("AgVAWQgIgKAAgMQAAgLAIgJQAKgJALAAQAMAAAJAJQAJAJAAALQAAAMgJAKQgJAIgMAAQgLAAgKgIgAgMgNQgHAGAAAHQAAAIAHAGQAFAFAHAAQAIAAAGgFQAFgGABgIQgBgHgFgGQgGgGgIAAQgHAAgFAGg");
	this.shape_5.setTransform(399.1,262.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#666666").s().p("AgUAVQgJgJAAgMQAAgLAJgKQAJgIALAAQAMAAAJAIQAJAKAAALQAAAMgJAJQgJAJgMAAQgLAAgJgJgAgMgNQgGAGAAAHQAAAIAGAGQAFAFAHAAQAIAAAGgFQAGgGAAgIQAAgHgGgGQgGgFgIAAQgHAAgFAFg");
	this.shape_6.setTransform(440.6,220.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B0B29D").s().p("AgDAHIAAAAIgBgCQgBAAAAAAQAAgBAAAAQAAAAAAAAQAAAAABAAIgBgBQgBgDABgCIABgCIAAAAIABgCIABAAIAAgBIADAAQAEAAABADIAAAAIAAAJIgCACIgFABg");
	this.shape_7.setTransform(440.7,220.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#909281").s().p("AgMAOQgHgGABgIQgBgHAHgGQAFgFAHAAQAIAAAGAFQAFAGAAAHQAAAIgFAGQgGAFgIAAQgHAAgFgFgAgDgFIAAAAIgBACQgBADABACIABABQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAABAAIABACIAAAAIACABIAFgBIACgCIAAgJIAAAAQgBgDgEAAIgDAAIAAABIgBAAg");
	this.shape_8.setTransform(440.6,220.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B0B29D").s().p("AgDAHIgBAAIAAgCQAAAAgBAAQAAgBAAAAQAAAAAAAAQABAAAAAAIgBgBIAAgFIABgCIAAAAIABgCIABAAIAAgBIADAAQAEAAAAADIABAAIAAAJIgCACIgGABg");
	this.shape_9.setTransform(357.6,220.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#909281").s().p("AgNAOQgFgGAAgIQAAgHAFgGQAGgFAHAAQAIAAAGAFQAFAGABAHQgBAIgFAGQgGAFgIAAQgHAAgGgFgAgDgFIAAAAIgBACIAAAFIABABQAAAAgBAAQAAAAAAAAQAAAAAAABQABAAAAAAIAAACIABAAIACABIAFgBIADgCIAAgJIgCAAQAAgDgEAAIgDAAIAAABIgBAAg");
	this.shape_10.setTransform(357.5,220.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#666666").s().p("AgVAVQgIgJAAgMQAAgLAIgKQAKgIALAAQAMAAAJAIQAJAKAAALQAAAMgJAJQgJAJgMAAQgLAAgKgJgAgNgNQgGAGAAAHQAAAIAGAGQAGAFAHAAQAIAAAGgFQAFgGAAgIQAAgHgFgGQgGgFgIAAQgHAAgGAFg");
	this.shape_11.setTransform(357.5,220.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#666666").s().p("AlOFQQiLiLAAjFQAAjDCLiMQCLiKDDAAQDFAACKCKQCLCMAADDQAADFiLCLQiKCKjFAAQjDAAiLiKgAlClEQiHCGAAC9QAAC9CHCGQCFCFC9AAQC9AACGiFQCFiGAAi9QAAi9iFiGQiGiFi9AAQi9AAiFCFg");
	this.shape_12.setTransform(399.1,222.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#909281").s().p("AlBFEQiHiHAAi9QAAi9CHiGQCEiFC9AAQC9AACGCFQCGCGAAC9QAAC9iGCHQiGCFi9AAQi9AAiEiFgAkFkHQhtBugBCZQABCaBtBtQBtBtCYAAQCbAABthtQBthtAAiaQAAiZhthuQhthtibAAQiZAAhsBtg");
	this.shape_13.setTransform(399.1,222.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#003366").s().p("Aj8D9QhohpAAiUQAAiTBohpQBphoCTAAQCUAABpBoQBoBpAACTQAACUhoBpQhpBpiUgBQiTABhphpg");
	this.shape_14.setTransform(399.2,222.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#B0B29D").s().p("AkGEIQhthuAAiaQAAiZBthtQBthtCZAAQCaAABtBtQBtBtAACZQAACahtBuQhtBsiaAAQiZAAhthsgAj7j8QhpBpAACTQAACUBpBpQBoBoCTAAQCUAABphoQBphpAAiUQAAiThphpQhphpiUAAQiTAAhoBpg");
	this.shape_15.setTransform(399.1,222.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#72A4A8").s().p("Ag7ARIALghIBsAAIgMAhg");
	this.shape_16.setTransform(428.6,322.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CBEBEE").s().p("AiDNbQhUkVgih/QjFrcCnpFIJOAAQBzIWiwKPQgjCBh4GPg");
	this.shape_17.setTransform(398.6,235.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#99C7CB").s().p("AjrNsIAKgiICcAAIgLAigAhFNKQBWj7Agh1QDLrzi4pSIA1AAQDhJvjZMmQgmCNg0CTgAhFNKg");
	this.shape_18.setTransform(430.7,236.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#B8E3E7").s().p("AklNsIgKgiIDjAAIgKAigAhMNKQB3mPAjiBQCwqPhzoWIBOAAQC4JSjLLzQggB1hXD7gAhMNKg");
	this.shape_19.setTransform(415.9,236.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#EB5124").s().p("ABXEyQhHlIi0kbIAJAEQDaEYBmFHg");
	this.shape_20.setTransform(421,118.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E5FCFE").s().p("AAPNsIgMgiQgziTgmiNQjZsmDhpvIA7AAQjOJiDTMPQAfB3BGDNIBgAAIALAig");
	this.shape_21.setTransform(367.1,236.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AA4NbQhFjNggh3QjTsPDOpiIA2AAQimJFDELcQAjB/BTEVg");
	this.shape_22.setTransform(370.2,235.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FB641E").s().p("AknE1QBhlNDYkcQAMABANAGQC1EbBIFHg");
	this.shape_23.setTransform(400.2,118.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FD8545").s().p("Ai3E2QBsk+DckQIADgEIAIgKIAPgPIANABQjYEchgFOg");
	this.shape_24.setTransform(383.6,118.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F09F85").s().p("AjOE2QBrkrDQj/IADgEIAJgKQArgxArgCIgOAPIgJAKIgCAEQjdEQhsE+g");
	this.shape_25.setTransform(379.9,118.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#9E403B").s().p("ACGEwQhmlIjakXQAbAPAcAiQDSEBBsEtg");
	this.shape_26.setTransform(424.1,118.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).wait(1));

	// Layer 3
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FC8F5F").s().p("ABPF2QAflljulwIAAAAIA2gbIgCAPIAEAEQDgFdgZGCQgKADgLAAQgNAAgOgFg");
	this.shape_27.setTransform(318.3,345.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#AD2606").s().p("AhEANQA7gpBOgIIgHAxQhEgHg0AfIgKgYg");
	this.shape_28.setTransform(362.5,309.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FB6420").s().p("Ajcg7IgEgEIACgQQAHg2AdgvIAQgbQAWgjAUgkQAmhEAshBQAog8Arg6QApg1AtgxIApgsQgZGeBWDPQi/CEATHbQgaBpgvARQAYmDjglbg");
	this.shape_29.setTransform(333.1,316);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFB064").s().p("AjbFKIgEgDQAFg/Ahg3IAQgaQAWgjAUgkQAmhEAshAQAog8Arg6QApg1AtgyQArguAugqIALABIgDAkIgpArQgtAygpA1QgsA6goA8QgrBAgmBEQgUAkgWAjIgQAaQgdAwgHA2Ig2Abg");
	this.shape_30.setTransform(327.4,277.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#D9340C").s().p("AipMNQAgAKAPgJQAwgSAahpQgTnbC+iFQhWjOAZmdIAwguQAggeAigcQA1gsA+ghQArgXAtgRIh1MWQhQAIg7AqIAKAYQipBkAUH8QgbBrhMAAQgXAAgbgJgAl6A2IAAAAIAAAAg");
	this.shape_31.setTransform(343.3,305.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#F04D25").s().p("AibBPIABgMQAagZAcgWQA1grA+ghQAYgNAYgLQAtgUAvgOIAEAJQguASgqAXQg/Ahg0ArQgiAcggAeIgwAtIADgkg");
	this.shape_32.setTransform(365.4,236.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E6B279").s().p("AAHEHQAUn7iphkIAKgYQC+CFgTHaQAaBpAwASQAOAFAQgCIgCAAIgJACQgMACgKAAQhNAAgahqg");
	this.shape_33.setTransform(461.7,347.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FB6420").s().p("AAQIpQAUnci+iDQBVjPgZmeIApAsQAtAxAoA2QAsA5ApA9QAsBAAmBFQAUAjAVAjIAQAbQAeAvAHA2IABAQIgEAEQjhFcAaGDQgwgSgbhpgAkeg2IgHgyQBPAIA8AqIgKAXQg0gehGAHg");
	this.shape_34.setTransform(464,315.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#D9340C").s().p("AB7MQQgZmDDhlcIAEgEIgCgQIA2AbIAAABQjuFwAeFlIAAABQgJADgJABIgJABQgLAAgKgEgAkFAEIh1sXQAtARArAYQA+AhA1AsQAiAcAgAeQAYAWAYAXQAZGdhWDOQg7gphQgIg");
	this.shape_35.setTransform(460.7,305.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E95733").s().p("ACmEvQgHg2gdgwIgQgaQgWgjgUgkQgmhEgrhAQgog8gsg6Qgpg1gtgyIgqgrIgCgkIALgBQAuAqArAuQAtAyApA1QArA6AoA8QAsBAAmBEQAUAkAWAjIAQAaQAhA3AFA/IgEADg");
	this.shape_36.setTransform(476.6,277.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#F04D25").s().p("ABvBGQgggegigcQg0grg+ghQgrgXgugSIAEgJQAwAOAsAUQAZALAYANQA9AhA1ArQAcAXAaAYIABAMIACAkQgXgXgYgWg");
	this.shape_37.setTransform(438.6,236.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27}]}).wait(1));

	// Layer 4
	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#4A4841").s().p("Ag2ASIAAgiIBtAAIAAAig");
	this.shape_38.setTransform(418.6,324.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#262A1C").s().p("AgiASIAAgiIBFAAIAAAig");
	this.shape_39.setTransform(427.6,324.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#64625C").s().p("Ag2AaIAAgzIBtAAIAAAzg");
	this.shape_40.setTransform(418.6,329.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#414634").s().p("AgiAaIAAgzIBFAAIAAACQAAAxgxAAg");
	this.shape_41.setTransform(427.6,329.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#758670").s().p("Ah0AaIAAgzIDpAAIAAAzg");
	this.shape_42.setTransform(401.5,329.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#566651").s().p("Ah0ASIAAgiIDpAAIAAAig");
	this.shape_43.setTransform(401.5,324.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#90A697").s().p("AgfASIAAgiIA/AAIAAAig");
	this.shape_44.setTransform(374.5,324.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#6D6B63").s().p("Ag7ASIAAgiIB3AAIAAAig");
	this.shape_45.setTransform(383.8,324.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#B2C8B9").s().p("AASAaQgxAAAAgxIAAgCIA/AAIAAAzg");
	this.shape_46.setTransform(374.5,329.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#908E86").s().p("Ag7AaIAAgzIB3AAIAAAzg");
	this.shape_47.setTransform(383.8,329.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38}]}).wait(1));

	// Layer 1
	this.instance = new lib.fire("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(402.3,351.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.rkt, new cjs.Rectangle(305,87.3,194,297.1), null);


// stage content:
(lib.RocketDesign_Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}
	this.frame_9 = function() {
		this.stop()
	}
	this.frame_19 = function() {
		this.stop()
	}
	this.frame_29 = function() {
		this.stop()
	}
	this.frame_39 = function() {
		this.stop()
	}
	this.frame_49 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(9).call(this.frame_9).wait(10).call(this.frame_19).wait(10).call(this.frame_29).wait(10).call(this.frame_39).wait(10).call(this.frame_49).wait(1));

	// Layer 6
	this.rocketMc = new lib.rkt();
	this.rocketMc.parent = this;
	this.rocketMc.setTransform(70.8,75.3,0.391,0.391,90,0,0,402.6,235.4);
	this.rocketMc.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(-44, 19, 0, 0))];
	this.rocketMc.cache(303,85,198,301);

	this.timeline.addTween(cjs.Tween.get(this.rocketMc).to({x:205.8},9).to({x:342.8},10).to({x:480.8},10).to({x:618.8},10).to({x:730.8},10).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B1FF").s().p("AihCJQAoAVAxAAQAcAAAZgHIAVABQAfAAAWgIQAWgHAAgLQAAgKgVgIIAJgIQANgNAKgOIADAAQAJAAAHgDQAHgCAAgEQAAgDgHgDIgIgCQAMgXAGgZQAVgEAIAAIAQACQgGBGgzA0Qg5A5hQAAQhKAAg3gwgABBivIgKgJQAbANAXAXQAqAqALA3QgPAFgVADQgChNg3g3g");
	this.shape.setTransform(214.2,76.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#85E7FF").s().p("AAACYQgWgHAAgLQAAgKAWgIQAVgHAfgBQAfABAWAHIABAAQgiAfgpAMQgSgCgNgFgAB8BOQgHgCAAgEQAAgEAHgDQAHgCAKAAIAIAAIgLASQgIAAgGgDgAARAtIgRgBIAAgCQAPgEAIgIQAGgGAAgFQAAgGgvAAQgvABAAgIQAAgOAxgIQAWgEBVgGQAwgEAhgFIAAAGQAAAYgFAUIhHANIhOARgAirg0QAJgjAVgdQAtAIAkgCQA3gFAjADQAjAFAAAKQAAAGg6AKIg8AJQgLADgCAEQgBADgCABQgIAGhTADIgLAAgAgliSQgIgDAAgCQAAgDAIgBQAIgDALAAQALAAAHADQAIABAAADQAAACgIADQgHACgLAAQgLAAgIgCg");
	this.shape_1.setTransform(209.2,76);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Ah/ChIgLgJQg5g5AAhRQABgaAGgYIALAAQBTgCAIgGQACgCABgDQACgEALgDIA+gJQA4gKAAgGQAAgKgjgEQgjgEg3AFQgkADgtgJQAJgMALgMQA6g5BQAAQAwAAAoAVIAKAJQA3A3ACBNQAVgDAPgFQAEAUABAVIgBAQIgQgCQgIAAgUAEQAEgWAAgXIAAgGQghAGgwADQhUAHgXADQgxAHAAAPQAAAIAwAAQAugBAAAHQAAAFgGAFQgHAIgPAEIAAACIARABIAAAAIBPgQIBHgNQgGAZgNAXIgIgBQgKAAgGADQgIADAAADQAAAEAIACQAFADAIAAQgKAOgNANIgJAIIAAAAQgXgHgeAAQggAAgUAHQgXAIAAAKQAAALAXAHQANAFARACQApgMAigfQAVAIAAAKQAAALgVAHQgXAIgeAAIgWgBQgZAHgcAAQgwAAgogVgAg2iLQgIACAAACQAAADAIACQAIACALAAQALAAAIgCQAIgCAAgDQAAgCgIgCQgIgCgLAAQgLAAgIACgAB5BhIALgRIAIACQAIADgBADQABAEgIACQgGADgKAAIgDAAg");
	this.shape_2.setTransform(210.9,74.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B1E80E").s().p("AAfB+QgKgDAAgFQAAgFAKgEQAKgCAPAAQAOAAAKACQAKAEAAAFQAAAFgKADQgKAEgOAAQgPAAgKgEgAhXBHQgVgFAAgJQAAgJAVgFQAVgHAeAAQAeAAAUAHQAVAFAAAJQAAAJgVAFQgUAHgeAAQgeAAgVgHgABiApQgGgBAAgCQAAgBAGgCIAQgBIAPABQAGACAAABQAAACgGABIgPABIgQgBgAAsgdQgJgDAAgDQAAgEAJgDQAKgCANAAQANAAAJACQAJADAAAEQAAADgJADQgJADgNAAQgNAAgKgDgAhGgsQgDgEAAgFQAAgEADgFQAEgDAFAAQAFAAAEADQAEAFAAAEQAAAFgEAEQgEAEgFAAQgFAAgEgEgAALhIQgIgDAAgFQAAgEAIgEQAHgDALAAQAKAAAIADQAHAEAAAEQAAAFgHADQgIACgKAAQgLAAgHgCgAh5hvQgNgCAAgEQAAgDANgEQAOgCAUAAQATAAAOACQAOAEAAADQAAAEgOACQgOADgTAAQgUAAgOgDgABQh1QgIgDAAgCQAAgCAIgDQAHgBALAAQALAAAIABQAHADAAACQAAACgHADQgIABgLAAQgLAAgHgBg");
	this.shape_3.setTransform(346.5,75.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#99CC00").s().p("AhrChIgKgJQg6g5AAhRQAAhQA6g6QA5g5BQAAQAwAAApAVIAJAJQA6A5gBBRQABBQg6A5Qg5A6hQAAQgxAAgngVgAAlB7QgKAEAAAFQAAAFAKADQAKADAPAAQAOAAAKgDQAKgDAAgFQAAgFgKgEQgKgDgOAAQgPAAgKADgAhRA4QgVAGAAAJQAAAIAVAGQAVAGAeAAQAeAAAUgGQAVgGAAgIQAAgJgVgGQgUgGgeAAQgeAAgVAGgABoAxQgGABAAACQAAABAGABIAPABIAQgBQAGgBAAgBQAAgCgGgBIgQgBIgPABgAAygcQgJADAAAEQAAADAJADQAKADANAAQANAAAIgDQAKgDAAgDQAAgEgKgDQgIgCgNAAQgNAAgKACgAhAgwQgDAEgBAFQABAFADAEQAEAEAFAAQAFAAAEgEQADgEAAgFQAAgFgDgEQgEgDgFAAQgFAAgEADgAARhKQgIADAAAFQAAAEAIADQAHADALAAQAKAAAIgDQAHgDAAgEQAAgFgHgDQgIgDgKAAQgLAAgHADgAhzhuQgOADAAAEQAAADAOADQAOADAUAAQATAAANgDQAOgDABgDQgBgEgOgDQgNgCgTAAQgUAAgOACgABVhxQgHACAAADQAAACAHACQAIACALAAQALAAAHgCQAIgCAAgCQAAgDgIgCQgHgCgLAAQgLAAgIACg");
	this.shape_4.setTransform(345.9,74.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#8CBB01").s().p("AihCJQAoAVAwAAQBQAAA5g6QA6g5AAhQQAAhRg6g5IgJgJQAaANAYAXQA5A6AABRQAABQg5A5Qg5A5hQAAQhLAAg2gwg");
	this.shape_5.setTransform(351.3,76.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FD90B4").s().p("AAcB3QgKgJAAgTQAAgZAaAAIARACQAKAHABAEQABACAAAPQAAAegSAAQgSAAgJgHgAAAA8IgEAAQgXgEgHgZQgNgxgdgfQgGgGgUAAQgZAAgNgBQgNgBgJgEQAOgjAcgdQAZAPAPABQAQABBAgEQAhAAAJAGQAJAGAAAXQAAAGgNAEQgNADAAAJQAAAFASATQASASAAAgQAAAighAHIgZABgAB8gRQgBgDAAgMQAAgFACgFQAEgHAEAAIABAAQABgBAHAAQANAAAFAIQADAFAAANQAAAFgKAFQgJAFgHAAQgKAAgDgIg");
	this.shape_6.setTransform(483.5,74.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF6699").s().p("AhrChIgLgJQg5g5AAhRQABgmANghQAJAEANABQANABAZgBQAUABAGAGQAdAfANAxQAHAYAXAFIAEgBIADABIAZgBQAhgGAAgiQAAghgSgSQgSgSAAgFQAAgJANgEQANgDAAgHQAAgXgJgFQgJgGgiAAQg/AEgQgCQgPgBgZgPIABgCQA6g5BQAAQAxAAAoAVIAJAJQA5A5AABRQAABQg5A5Qg5A6hQAAQgxAAgngVgAATBfQAAATAKAIQAJAIASAAQASAAAAgeQAAgPgBgCQgBgFgLgHIgQgBQgaAAAAAZgAB+gnQgDAGAAAFQAAAMABADQAEAHAKAAQAGAAAKgFQAKgFAAgEQAAgNgDgFQgFgIgOAAQgGAAgBABIgBAAQgEAAgEAGg");
	this.shape_7.setTransform(483.4,74.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EE3271").s().p("AihCJQAoAVAxAAQBQAAA4g6QA6g5AAhQQAAhRg6g5IgJgJQAbANAXAXQA5A6AABRQAABQg5A5Qg6A5hQAAQhJAAg3gwg");
	this.shape_8.setTransform(488.8,76.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF6600").s().p("AihCJQAoAVAwAAQAuAAAlgSQAfAMAcAEQgtAdg4AAQhLAAg2gwgAA/BmIACgCQA5g5AAhQIgBgWIAAAAIAhgJQAIAcAAAfQAABDgpAzQgbgBgfgGgAB0hWQgMgygngnIgKgJQAaANAXAXQAcAcAPAiIgOABIgRgBg");
	this.shape_9.setTransform(626.4,76.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF9900").s().p("Ah7ChIgKgJQgLgLgJgMIACgCQANgFAoAAQA7AAA9AeIAaAMQgmASgtAAQgwAAgogVgAAwCkQAcgOAYgYQAgAGAbABIgRATQgQAQgTAMQgcgEgfgMgAAwCkIAAAAgAAoBwQhRgVgaAAQgmAAgUAEQgVAFgUAJQgOgZgGgdIAegBQAqAAAiAMQAiAMAgAAQAvAAAPgPQAagZADgCQgIADgjAGQgiAGgEAAQgXAAgtgSQgpgRgfgCIgCAAQgZABgTADIAAgEQAAguATgmIAhgCQBQAAAwAYQAwAYAiAAQAmAAAxgLIABAWQAABQg6A5IgCACQgcgFgggJgACfgjQgCgOgDgNIARABIAOgBIAGASIggAJIAAAAIAAAAgABAhSQg9gUgiAAQgdAAghAKQghALgRAAQgIAAgNgDQAMgVATgTQA5g5BQAAQAxAAAoAVIAJAJQAnAnANAyQgogDgygRg");
	this.shape_10.setTransform(622.6,74.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFCC00").s().p("AAnB6Qg+geg7AAQgnAAgNAFIgDABIgNgTQAUgKAVgEQAVgEAmAAQAaAABRAVQAgAJAcAFQgYAYgdANIgZgLgAhAAkQgigMgqAAIgeABQgEgSAAgSQATgDAZgBIACAAQAfACAqAQQAsASAXAAQAFAAAhgGQAjgGAJgDQgEACgZAZQgQAPgwAAQgfAAgigMgAAGhOQgwgYhPAAIgiACIAHgOQANADAJAAQAQAAAigLQAhgKAdAAQAiAAA9AUQAxARAoADQAEANABAOQgxALgmAAQghAAgxgYg");
	this.shape_11.setTransform(621,77.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF9900").s().p("Ah7ChIgKgJQgLgLgJgMIACgCQANgFAoAAQA7AAA9AeIAaAMQgmASgtAAQgwAAgogVgAAwCkQAcgOAYgYQgcgFgggJQhRgVgaAAQgmAAgUAEQgVAFgUAJQgOgZgGgdIAegBQAqAAAiAMQAiAMAgAAQAvAAAPgPQAagZADgCQgIADgjAGQgiAGgEAAQgXAAgtgSQgpgRgfgCIgCAAQgZABgTADIAAgEQAAguATgmIAhgCQBQAAAwAYQAwAYAiAAQAmAAAxgLQgCgOgDgNIARABIAOgBIAGASIggAJIAAAAIABAWQAABQg6A5IgCACQAgAGAbABIgRATQgQAQgTAMQgcgEgfgMgAAwCkIAAAAgABAhSQg9gUgiAAQgdAAghAKQghALgRAAQgIAAgNgDQAMgVATgTQA5g5BQAAQAxAAAoAVIAJAJQAnAnANAyQgogDgygRg");
	this.shape_12.setTransform(622.6,74.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#4343EC").s().p("AiVCTIgMgKIAVAJQAgAMAkAAQBQAAA5g6QA5g5AAhQQAAhRg5g5IgKgJQAbANAWAXQA6A6AABRQAABQg6A5Qg5A5hQAAQhCAAgygmg");
	this.shape_13.setTransform(763.4,76.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#6666FF").s().p("AhWCpQA2gEAkgaQAigZAAgaQAAgRgEgFQgEgEgMAAIgeAEQgRAAgBgbQgCgbgLAAQgiAAgUArQgUArgUAAIgUgEQgRglAAgtQAAhQA4g5QAtgsA7gKQAOAIAJAMQAQASAAAUQAAAOgZATQgYASAAAZQAAAPAPAJQAMAIAXAAQAAgBAngMQAngTAAgrQAAgQgNghQgIgWgDgOQAPAEANAHIAKAJQA5A6ABBRQgBBQg5A5Qg5A5hQAAQgkAAgfgLgAB6gaIgHAQIAAABQAAAKAIAJQAIAJALAAQAIAAACgCQABgDAAgMQAAgdgYAAQgGAAgBABgAhrg3QgGAFAAAJQAAAEAHAHQAKAIALAAQAMAAAFgJQADgHABgOQgBgHgCgEIgFgGIgMAAIgXAOg");
	this.shape_14.setTransform(758.1,74.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#8B8BFB").s().p("AhpCnIgKgKQgagZgOgfIAUAEQAUAAAVgrQAUgrAhAAQALAAACAbQACAbAQAAIAegEQANAAADAEQAEAFAAARQAAAagiAZQgjAag3AEIgVgJgAB9ARQgIgJAAgKIAAgBIAHgQQABgBAGAAQAZAAAAAdQAAAMgCADQgBACgJAAQgLAAgIgJgAgHgOQgPgJAAgPQAAgZAYgSQAZgTAAgOQAAgUgQgSQgJgMgOgIQAQgDASAAQAgAAAcAJQADAOAJAWQAMAhAAAQQAAArgnATQgnAMAAABQgWAAgNgIgAhngXQgIgHAAgEQAAgJAHgFIAWgOIAMAAIAFAGQADAEAAAHQAAAOgEAHQgFAJgMAAQgLAAgJgIg");
	this.shape_15.setTransform(757.8,73.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},9).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},10).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},10).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},10).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_11},{t:this.shape_12},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},10).wait(1));

	// Layer 2
	this.instance = new lib.nplnts();
	this.instance.parent = this;
	this.instance.setTransform(485.5,75.8);
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(26, 0, -100, 0))];
	this.instance.cache(-461,-21,757,43);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50));

	// Layer 7
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF3333").s().p("EA1rAAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEAzLAAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEAwrAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEAuLAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEArrAAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEApLAAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEAmrAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEAkLAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEAhrAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAfLAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAcrAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgAaLAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgAXrAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAVLAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgASrAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgAQLAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgANrAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgALLAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAIrAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAGLAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgADrAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgABLAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAhUAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAj0AHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgAmUAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgAo0AHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgArUAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAt0AHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgAwUAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgAy0AHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgA1UAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgA30AHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgA6UAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgA80AHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgA/UAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEgh0AAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEgkUAAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEgm0AAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEgpUAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEgr0AAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEguUAAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAgEgw0AAHQgDAAgCgCQgCgCAAgDQAAgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEgzUAAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACABACQgBADgCACQgCACgDAAgEg10AAHQgDAAgCgCQgCgCgBgDQABgCACgCQACgCADgBIAKAAQADABACACQACACAAACQAAADgCACQgCACgDAAg");
	this.shape_16.setTransform(413.9,76.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(50));

	// Layer 5
	this.instance_1 = new lib.strs();
	this.instance_1.parent = this;
	this.instance_1.setTransform(417,66);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50));

	// Layer 8
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhEA4IAAhvICJAAIAABvgAgKAcIAZAAIAAg7IgZAAg");
	this.shape_17.setTransform(24.2,139.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC2321").s().p("AiJAbQABgTAEgQIEDgSQALAYAAAdg");
	this.shape_18.setTransform(40.3,128.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#144662").s().p("AkABNIAAgSIAcAAIAAACIHTAAIAAgCIASAAIAAASgAjWA7IAAg2IAgAAIAAA2gAhGAfIAAg7IAaAAIAAA7gAB3gYIAAgKIBiAAIAAAKgAjwg2IAAgWIDMAAIETAAIAQAAIAAAWg");
	this.shape_19.setTransform(30.2,139.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#DCDCDC").s().p("AgICTIAAhxIDlAAIAABxgABlA+IBiAAIAAgKIhiAAgAjHCTIAAg2IggAAIAAA2IgOAAIAAhxIBjAAIAABxgAC1hUIgSgQIAtguIAmAnIguAvQgIgNgLgLg");
	this.shape_20.setTransform(31.9,130.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF4341").s().p("Ah3AIQAognA5AAQAsAAAjAZIASAOQALALAIANQAGAIAEAKIkDASQAKgiAagagABmg0IAQgPIAmAmIgQAQg");
	this.shape_21.setTransform(42.5,121);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]}).wait(50));

	// Layer 3
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#001E3C").s().p("Eg+fAF3IAArtQDOHUFRg+IJ2AAQGpCQI8hkQI3hiI7BnQIjBkIohOQF3g4F9gDIghALIAnABIR5BXQJCAsI0h5QENg7ESgKIAAF6g");
	this.shape_22.setTransform(400,112.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#003366").s().p("EAkLAJ0Ix5hXIgngBIAhgKQl9ADl3A3QooBOojhkQo7hno3BiQo8BkmpiQIp2AAQlRA+jOnVIAArtMB8/AAAIAARhQkSALkNA6QmfBZmmAAQiYAAiZgMg");
	this.shape_23.setTransform(400,64);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22}]}).wait(50));

	// bg
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(255,255,255,0.008)").s().p("Eg+fALuIAA3bMB8/AAAIAAXbg");
	this.shape_24.setTransform(400,75);

	this.timeline.addTween(cjs.Tween.get(this.shape_24).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(400,75,800,150);
// library properties:
lib.properties = {
	width: 800,
	height: 150,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;