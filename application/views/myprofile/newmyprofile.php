<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>assets/css/newprofile.css" rel="stylesheet" media="screen">
<div class="my_profile">

<div class="container">
    <div class="row user-menu-container square">
        <div class="col-md-12 user-details">
            <div class="row coralbg white">
                <div class="col-md-12 no-pad">
                    <div class="user-pad" style="font-weight: 400!important; font-size: 16px!important;">
                        <p style="color:#fff; font-size:22px">Welcome <?php echo $myprofile[0]['fname']; ?> <?php echo $myprofile[0]['lname']; ?><br/>
                      <!-- <i class="fa fa-check-circle-o"></i> -->  <?php //echo str_replace('Grade','Grade :',$getgrade[0]['classname']);?>     <?php //echo $myprofile[0]['section'];?>
					   <?php if($myprofile[0]['school_name']!=''){$schoolname=$myprofile[0]['school_name'];}else{$schoolname=$myprofile[0]['schoolname'];} ?>
					   
                     <i class="fa fa-university"></i>    <?php echo $schoolname; ?>
					 <br/> <br/><strong style="color: #ffc000;"><a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard" class="btnactive playbtn">Click Here</a> to take your assessment</strong>
                      </p>
                    </div>
                </div>
               
            </div>
			 
        
        </div>
        
        
    </div>
	
	
	<div class="row">
      			<div class="col-lg-12 landingContainer">
        			<div class="col-lg-4 bounceIn animated">
                    	<a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeGameIcon.png" width="123" height="123" alt="# Games"></a>
                    	<div class="landingPageBox homeGameContainer">
                   		  <h4># Puzzles</h4>
                       	  <p>5 Brain Puzzles to complete</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                    	<a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeQuestionsIcon.png" width="123" height="123" alt="# Questions"></a>
                    	<div class="landingPageBox homeQuestionContainer">
                    		<h4># Questions</h4>
                        	<p>10 questions in Each Brain Puzzle</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                            <a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeGame-ScoresIcon.png" width="123" height="123" alt="Game Scores"></a>
                    	<div class="landingPageBox homeGamescoreContainer">
                    		<h4>Puzzle Scores</h4>
                        	<p>Speed & Accuracy decide your Scores</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                            <a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeReportsIcon.png" width="123" height="123" alt="# Reports"></a>
                    	<div class="landingPageBox homeReportContainer">
                    		<h4># Reports</h4>
                        	<p>Know your BSPI in "Reports"</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                            <a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeTime-DurationIcon.png" width="123" height="123" alt="Time Duration for Games"></a>
                    	<div class="landingPageBox homeTimeContainer">
                    		<h4>Time Duration for Puzzles</h4>
                        	<p>Each puzzle has a time limit. Finish before the Timer goes off!</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                            <a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeGamePlayIcon.png" width="123" height="123" alt="Game Play"></a>
                    	<div class="landingPageBox homePlayContainer">
                    		<h4>Puzzles Play</h4>
                        	<p>Each Puzzle challenge can be taken only once</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                            <a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeTime-SlotIcon.png" width="123" height="123" alt="Time Slot"></a>
                    	<div class="landingPageBox timeSlotContainer">
                    		<h4>Time Slot</h4>
                        	<p>You have to  complete all the 5 pzzles within the time slot</p>
                        </div>
                    </div>
                    <div class="col-lg-4 bounceIn animated">
                            <a href="<?php echo base_url(); ?>index.php/mypuzzleset1/dashboard"><img class="landingIcon circle" src="<?php echo base_url(); ?>assets/images/HomeHow-to-PlayIcon.png" width="123" height="123" alt="How to Play?"></a>
                    	<div class="landingPageBox homeHowtoplayContainer">
                    		<h4>How to Play?</h4>
                        	<p>Read the instructions before you begin to play each puzzle</p>
                        </div>
                    </div>
      			</div>
 			</div>
	
	
</div>
</div>
</div>
<style>

.landingContainer { text-align:center; padding-top:30px; }
.landingPageBox { background:#ececec; padding: 64px 15px 5px 15px; margin-top: -62px; margin-bottom:25px; }
.landingIcon { position:relative; z-index:999; }
.round { background:#fff; width:120px; height:120px; border-radius:50%; margin:0 auto; border:5px solid #000; padding-top:14px; }
.homeGameContainer { background: #ff0000; }
.homeQuestionContainer { background: #ffc000; }
.homeGamescoreContainer { background: #92d050; }
.homeReportContainer { background: #00b0f0; }
.homeTimeContainer { background: #ff6600; }
.homePlayContainer { background: #7030a0; }
.timeSlotContainer { background: #a21d7e; }
.homeHowtoplayContainer { background: #538134; }
.landingPageBox h4{ color:#fff; font-weight:bold; font-size:18px; }
.landingPageBox p { font-size:16px; min-height:42px; color:#fff;}
.playbtn{text-decoration: none;color: #fff;border-radius: 5px;padding: 2px;}
</style>
<script>
$(document).ready(function() {
    var $btnSets = $('#responsive'),
    $btnLinks = $btnSets.find('a');
 
    $btnLinks.click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.user-menu>div.user-menu-content").removeClass("active");
        $("div.user-menu>div.user-menu-content").eq(index).addClass("active");
    });
});

$( document ).ready(function() {
    //$("[rel='tooltip']").tooltip();    
 
    $('.view').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});
$( document ).ready(function() {
<?php if($this->session->flashdata('flag')==1){ ?>
	swal({
	  title: 'Profile Updation',
	  html:'You have successfully updated the <br/> profile details.',
	  allowOutsideClick: false,
	  confirmButtonColor: '#3085d6',
	  confirmButtonText: 'Ok'
	}).then(function () {
	});
<?php } ?>
});
</script>