///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 4, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////
var tween
var chpos = 0;
var question1, question2;



var choiceArr = [];
var choiceMcArr = []
var posArr = []
var qno = [];
var anss = [,0,1,2,3];

var namePos = [0,1,2,3,0,2,3,1,0,1,2,3,3,0,1,2,1,2,0,3,0,1,3,2,2,1,0,3,1,3,2,0,1,2,3,0,2,0,1,3,2,0,3,1,0,1,3,2,1,2,3,0,1,2,3,0,1,0,3,2,1,2,3,0,2,3,0,1,0,2,1,3,
                3,0,1,2,1,2,3,0,0,1,2,3,0,2,3,1,0,2,3,1,0,1,2,3,1,2,3,0]
var qno = [0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96]
var nameArr = [];
var btnX = ["178.3","258.3","338.3","418.3","498.3","578.3","658.3","258.3","338.3","418.3","498.3","578.3"];
var btnY = ["497.1","497.1","497.1","497.1","497.1","497.1","497.1","567.1","567.1","567.1","567.1","567.1"];

var btnPaddArr = ["","","","365","335","305","275","245","215","185"]

var btnPadding = 50;
///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
// var qno = [];
var choiceArr = [];
var xArr = [];
var yArr = [];
var tweenMcArr = [];
var choicePos = [1, 2, 1, 2, 1, 2, 1, 2, 1, 2]
var qno1 = []
var qno2 = []

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////


function init() {
      canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "ArrangeTheWords-Level4/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(


        {id: "choice1", src: gameAssetsPath+"question.png"},
		{id: "position", src: gameAssetsPath+"positionImage1.png"},
        {id: "questionText", src: gameAssetsPath+"questiontext.png"},
         {id: "chHolder", src: gameAssetsPath+"chHolder.png"},
 { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
        
}
//=====================================================================//

function doneLoading1(event) {
  var event = assets[i];
    var id = event.item.id;
    console.log(" doneLoading ")
    loaderBar.visible = false;
    stage.update();

      if(id == "chHolder"){
				chHolderMc  = new createjs.Bitmap(preload.getResult('chHolder'));
                chHolderMc.x=80;chHolderMc.y=120;
				container.parent.addChild(chHolderMc);    
				chHolderMc.visible = false;
                 
			}
       
        if(id == "choice1")
        {
            var spriteSheet1 = new createjs.SpriteSheet({
                framerate: 60,
                "images": [preload.getResult("choice1")],
                "frames": {"regX": 50, "height": 92, "count": 100, "regY": 50, "width": 175}
            });
            
            choice1 = new createjs.Sprite(spriteSheet1);
            choice1.visible = false;
            container.parent.addChild(choice1);
            
        }
        
        if(id == "position"){
            var spriteSheet3 = new createjs.SpriteSheet({
                framerate: 60,
                "images": [preload.getResult("position")],
                "frames": {"regX": 50, "height": 63, "count": 64, "regY": 50, "width": 63}
            });
            numPosition = new createjs.Sprite(spriteSheet3);
            numPosition.visible = false;
            container.parent.addChild(numPosition);
            
        }
		 
       if(id == "questionText" ){
           
            questionText = new createjs.Bitmap(preload.getResult('questionText'));
            container.parent.addChild(questionText);
            questionText.visible = false;
             
        }
       if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("9B187A3141F2F044B0356886562D636B");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = true;
        gameIntroAnimMc.addEventListener("tick",startAnimationHandler);
    }
}

function tick(e) {

    stage.update();
}


function handleClick(e) {
    qno.sort(randomSort)
     toggleFullScreen();
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements(){
    helpMc.helpMc.contentMc.helpTxt.text = "Select the words in the alphabetical order";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";  
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y +40
    bg.visible=true
    container.parent.addChild(boardMc);
    container.parent.addChild(questionText);
    questionText.visible = true;
    questionText.scaleX = questionText.scaleY = 1
    questionText.x= 470;questionText.y = 80;
    container.parent.addChild(choice1);
    container.parent.addChild(numPosition);
    numPosition.visible = false;
    var j;
    for(i=0;i<2;i++){
         j=i+1;
        choiceArr[j] = choice1.clone();
        choiceArr[j].visible = true;
        choiceArr[j].x = (i*370)+420;
        choiceArr[j].y = 240
        console.log(choiceArr[j].x)
         console.log(choiceArr[j].y)
        choiceArr[j].scaleX =.8;
        choiceArr[j].scaleY =.8;
        container.parent.addChild(choiceArr[j]);
        console.log("xpos= "+ choiceArr[j].x)
        posArr[j] = numPosition.clone()
     posArr[j].scaleX=posArr[j].scaleY=.7;
        posArr[j].x = (i*360)+470;
        posArr[j].y = 330;
        
        //   posArr[j].y=200
        posArr[j].visible = false;
        container.parent.addChild(posArr[j]);
    }

     for(i=0;i<2;i++){
         j=i+3;
        choiceArr[j] = choice1.clone();
        choiceArr[j].visible = true;
        choiceArr[j].x = (i*370)+420;
        choiceArr[j].y = 460
         console.log(choiceArr[j].x)
         console.log(choiceArr[j].y)
        choiceArr[j].scaleX =.8;
        choiceArr[j].scaleY =.8;
        container.parent.addChild(choiceArr[j]);
        console.log("xpos= "+ choiceArr[j].x)
        posArr[j] = numPosition.clone()
        posArr[j].scaleX=posArr[j].scaleY=.7;
        posArr[j].x = (i*360)+470;
        posArr[j].y = 550;
         
        posArr[j].visible = false;
        container.parent.addChild(posArr[j]);
    }
   
    j=0;
    for(i=1;i<=choiceCnt;i++)
    {
          choiceArr[i].x = choiceArr[i].x-30;
           posArr[i].x = posArr[i].x-30;
            choiceArr[i].y = choiceArr[i].y+17;
           posArr[i].y = posArr[i].y+17;
    }
    pickques();
}
  function helpEnable(){
      for(i=1;i<=4;i++){
        choiceArr[i].mouseEnabled = true;
      }
  }

  function helpDisable(){
      for(i=1;i<=4;i++){
        choiceArr[i].mouseEnabled = false;
      }
  }

//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    clk=0;
    correctCnt = 0;
    chpos = [];
    console.log("count "+namePos.length)
    ansArr = [];
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions; 
     boardMc.boardMc.openMc.mouseEnabled = false;
     boardMc.boardMc.openMc.alpha = .5;
     qno.sort(randomSort)
     
     var qn=qno[cnt];
     
    for(i=1;i<=4;i++){
       console.log("qnoj:"+qn);
        choiceArr[i].gotoAndStop(qn);
       choiceArr[i].name = "ch"+namePos[qn];
       console.log("ch name "+choiceArr[i])
       nameArr[i]=namePos[qn];
       console.log(nameArr[i])
        choiceArr[i].alpha = 1;
        choiceArr[i].scaleX =1;
        choiceArr[i].scaleY =1;
        posArr[i].visible = false;
        chpos.push({posx:choiceArr[i].x,posy:choiceArr[i].y})
        qn++;
        ansArr.push(choiceArr[i].name);
    } 
     chpos.sort(randomSort);
     boardMc.boardMc.openMc.mouseEnabled = true;
     boardMc.boardMc.openMc.alpha = 1;    
    var vCnt = 0;
    for(i=1;i<=4;i++)
     {
         console.log("swap working");
         choiceArr[i].x = chpos[i-1].posx;
         choiceArr[i].y = chpos[i-1].posy;
         console.log("xpos= "+ choiceArr[i].x)
         console.log("ypos= "+ choiceArr[i].y)
    }
    rst = 0;
    gameResponseTimerStart();
    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
    enablechoices();
}

function enablechoices() {
    helpMc.helpMc.visible = false;
     boardMc.boardMc.openMc.mouseEnabled = true
    boardMc.boardMc.openMc.alpha = 1
      for(i=1;i<=4;i++){
        choiceMcArr[i] = new createjs.MovieClip()
        container.parent.addChild(choiceMcArr[i] )
        choiceMcArr[i].addChild(choiceArr[i])
        choiceArr[i].addEventListener("click", answerSelected);
        choiceArr[i].addEventListener("mouseover", onRoll_over);
        choiceArr[i].addEventListener("mouseout", onRoll_out)
        choiceArr[i].cursor = "pointer";
        choiceArr[i].scaleX =1.1;
        choiceArr[i].scaleY =1.1;
        choiceMcArr[i].timeline.addTween(createjs.Tween.get( choiceArr[i]).to({ scaleX: 1.15, scaleY: 1.15 }, 24).to({ scaleX: 1.1, scaleY: 1.1 }, 25).wait(1));
        choiceMcArr[i].play();
        }
}
//===============================================//
function disablechoices() {
    for(i=1;i<=4;i++){
        choiceArr[i].removeEventListener("click", answerSelected);
        choiceArr[i].removeEventListener("mouseover", onRoll_over);
        choiceArr[i].removeEventListener("mouseout", onRoll_out)
        choiceArr[i].cursor = "default";
    }
     boardMc.boardMc.openMc.mouseEnabled = false
    boardMc.boardMc.openMc.alpha = .5
}

function clearTweens()
{
    
}

function onRoll_over(e) {
    e.currentTarget.alpha = .5;
    stage.update();
}
function onRoll_out(e) {  
     e.currentTarget.alpha = 1;
    stage.update();
}

function answerSelected(e) {
    e.preventDefault();  
    e.currentTarget.removeEventListener("click", answerSelected);
    e.currentTarget.cursor = "default";
  //  e.currentTarget.disablechoices=true;
    var dx=e.currentTarget.x;
    var dy=e.currentTarget.y;
    console.log(dx);

    clk++;
    uans = e.currentTarget.name.split("ch")[1];
 console.log("uans "+uans)
     clearTweens()
    gameResponseTimerStop();
   // pauseTimer();
    
    if(clk<=4)
    {
        console.log(anss[clk]+" =$$$$$$$$$= "+ uans)
        if(anss[clk]==uans)
        {
            correctCnt++;
            if(dx==390 && dy==257)
            {
            posArr[1].visible = true;
            posArr[1].gotoAndStop(clk-1) 
            }
            else if(dx==760 && dy==257)
            {
                posArr[2].visible = true;
            posArr[2].gotoAndStop(clk-1) 
            }
            else  if(dx==390 && dy==477)
            {
            posArr[3].visible = true;
            posArr[3].gotoAndStop(clk-1) 
            }
            else if(dx==760 && dy==477)
            {
                posArr[4].visible = true;
                posArr[4].gotoAndStop(clk-1) 
            }
            // else if(dx==590 && dy==347)
            // {
            //     posArr[5].visible = true;
            //     posArr[5].gotoAndStop(clk-1) 
            // }
            // else if(dx==110 && dy==347)
            // {
            //     posArr[6].visible = true;
            //     posArr[6].gotoAndStop(clk-1) 
            // }

        //    choiceArr[correctCnt].alpha =  .5;
            if(correctCnt == 4){
                console.log("All correct")
                disablechoices();
                getValidation("correct");	
            } 
        }else{
            console.log("test")
            disablechoices();
            getValidation("wrong");
        }
    }
    
}

//===============================================================================================//
