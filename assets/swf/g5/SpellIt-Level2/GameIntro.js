(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("ATIkXQgUILlqFoQliFgm0KUQoaqUlilgQlqlogUoLQgBgdAAgcQgBn4FQl0QCZirC4hhIBfgvIDchDQBygVB6AAQB8AAByAVIDbBDIBgAvQC3BhCaCrQFPF0AAH4QAAAcgBAdg");
	this.shape.setTransform(0,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ArMLNQkpkpAAmkQAAmjEpkpQEpkpGjAAQGkAAEpEpQEpEpAAGjQAAGkkpEpQkpEpmkAAQmjAAkpkpg");
	this.shape_1.setTransform(0,-46.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC3333").s().p("AtIIZQlqlpgUoKIgCg6QAAn4FQlzQCZirC3hiQiLBOh6B6QlSFSAAHdIABA0QARG8FAE9QFRFSIPKNQGqqNFSlSQE/k9ARm8IABg0QAAndlRlSQh6h6iMhOQC3BiCZCrQFQFzAAH4IgCA6QgTIKlqFpQliFfm1KUQoaqUlhlfg");
	this.shape_2.setTransform(0,6.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AsuHoQk/k/gRm7IgBg0QAAndFRlRQB7h6CLhOIBggvIDbhDQBzgYB6AAQB8AAByAYQhygVh8AAQh6AAhzAVQBzgVB6AAQB8AAByAVIDbBDIBgAvQCMBOB5B6QFSFRAAHdIgBA0QgRG7lAE/QlRFSmqKNQoPqNlSlSgArMwSQkpEpAAGjQAAGjEpEpQEpEpGjAAQGkAAEokpQEpkpAAmjQAAmjkpkpQkokpmkAAQmjAAkpEpg");
	this.shape_3.setTransform(0,-13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-123.9,-162.9,247.9,326.2);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("AgRDYQgIgEgGgGQgHgGgDgIQgEgIABgKQgBgJAEgJQADgIAHgGQAGgGAIgDQAIgEAKAAQAJAAAIAEQAJADAFAGQAHAGADAIQADAJABAJQgBAKgDAIQgDAIgHAGQgFAGgJAEQgIADgJAAQgKAAgIgDgAgXA+QgFgdgCgbQgDgZAAgbIAAisIBFAAIAACsQAAAbgDAZQgCAbgFAdg");
	this.shape.setTransform(66.5,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("ABOCaQgKAAgEgNIg3iuIgFgRIgEgSIgDASIgFASIg4CtQgEANgMAAIg5AAIhhkyIA7AAQAIAAAGADQAFAEACAGIAsCkIAGAbIAEAaIAHgaIAIgbIAzilQACgFAGgEQAFgEAHAAIAgAAQAIAAAFAEQAGAEABAFIAzCpIAHAZIAGAYIAFgaIAGgbIAuikQABgGAGgEQAGgDAHAAIA4AAIhhEyg");
	this.shape_1.setTransform(31.9,6.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Ag9CUQgdgMgTgUQgVgVgKgdQgMgeAAgkQAAgkAMgdQAKgdAVgVQATgUAdgMQAcgLAhAAQAjAAAcALQAcAMAUAUQATAVAMAdQALAdgBAkQABAkgLAeQgMAdgTAVQgUAUgcAMQgcALgjAAQghAAgcgLgAg4hKQgSAaAAAwQAAAyASAZQATAbAlAAQAnAAATgaQASgaAAgyQAAgxgSgaQgTgagnAAQglAAgTAbg");
	this.shape_2.setTransform(-8.8,6.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CCFF").s().p("ABmDZIhekjIgEgMIgDgOIgEAOIgDAMIhgEjIhJAAIiHmxIBFAAQAKAAAHAFQAHAFADAIIBJEEIAFAVIAEAYIAGgYIAGgVIBVkEQADgGAHgGQAGgGALAAIAWAAQALAAAHAFQAGAFADAIIBVEEQAHAUAEAXIAFgWIAEgVIBKkEQACgHAHgGQAIgFAKAAIA/AAIiHGxg");
	this.shape_3.setTransform(-57.3,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90.8,-38.9,170.2,76);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF9900").ss(3,1,1).p("AivhkIBXnKIA/AAIBCAAIBwHHAivhkIDTAAIB1AAIBMAAICYAAIl9KTIg/hvIk9okg");
	this.shape.setTransform(16.2,-86.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("AkwgsIDNAAIDSAAIB2AAIBMAAIkmIjgABvgsgAhjgsIBXnLIA+AAIA9HLg");
	this.shape_1.setTransform(8.6,-91.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF99").s().p("AjeHAIElojICYAAIl8KSgAh6hjIg9nLIBCAAIBxHHIAAAEg");
	this.shape_2.setTransform(32,-86.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.4,-143.5,79.2,114.8);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbolalphabetcopy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgWCcQgKgJABgUIAAj9QgBgUAKgJQAJgLANAAQAPAAAIALQAKAJgBAUIAAD9QABAUgKAJQgIAKgPAAQgNAAgJgKg");
	this.shape.setTransform(-0.4,-1.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AgWCcQgKgJABgUIAAj9QgBgUAKgJQAJgLANAAQAPAAAIALQAKAJgBAUIAAD9QABAUgKAJQgIAKgPAAQgNAAgJgKg");
	this.shape_1.setTransform(0.9,0.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3333").s().p("AjQD4QhagBAAhKIAAlZQAAhKBagBIGhAAQBaABAABKIAAFZQAABKhaABg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#663300").ss(3,1,1).p("AislCIFZAAQCWAAAACWIAAFZQAACWiWAAIlZAAQiWAAAAiWIAAlZQAAiWCWAAg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC0000").s().p("AisFCQiVAAAAiVIAAlZQAAiVCVAAIFZAAQCVAAAACVIAAFZQAACViVAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,255,255,0.027)").s().p("AjgF9QicABAAidIAAnAQAAieCcAAIHBAAQCcAAAACeIAAHAQAACdicgBg");
	this.shape_5.setTransform(0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbolalphabetcopy3, new cjs.Rectangle(-38,-37.9,76.3,76.3), null);


(lib.Symbolalphabetcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhxCaQgJgKAAgUIAAj3QAAgVAKgKQAKgJAVAAIBTAAQAmAAATAGQAUAFAPANQAOANAHASQAIATAAAXQAAAxgeAZQgfAZg8AAIg7AAIAABbQAAAUgJAKQgJAKgOAAQgPAAgJgKgAg5gPIAtAAQAWAAAPgFQAPgFAJgLQAHgLABgSQAAgVgNgOQgOgOgqAAIgtAAg");
	this.shape.setTransform(0.4,-1.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhxCaQgJgKAAgUIAAj3QAAgVAKgKQAKgJAVAAIBTAAQAmAAAUAGQATAFAPANQAOANAHASQAIATAAAXQAAAxgeAZQgfAZg8AAIg7AAIAABbQAAAUgJAKQgJAKgOAAQgPAAgJgKgAg5gPIAtAAQAWAAAPgFQAPgFAJgLQAHgLABgSQAAgVgNgOQgOgOgqAAIgtAAg");
	this.shape_1.setTransform(1.7,0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3333").s().p("AjQD4QhagBAAhKIAAlZQAAhKBagBIGhAAQBaABAABKIAAFZQAABKhaABg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#663300").ss(3,1,1).p("AislCIFZAAQCWAAAACWIAAFZQAACWiWAAIlZAAQiWAAAAiWIAAlZQAAiWCWAAg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC0000").s().p("AisFCQiVAAAAiVIAAlZQAAiVCVAAIFZAAQCVAAAACVIAAFZQAACViVAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,255,255,0.027)").s().p("AjgF9QicABAAidIAAnAQAAieCcAAIHBAAQCcAAAACeIAAHAQAACdicgBg");
	this.shape_5.setTransform(0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbolalphabetcopy2, new cjs.Rectangle(-38,-37.9,76.3,76.3), null);


(lib.Symbolalphabetcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhmCaQgJgKAAgUIAAj3QAAgOAEgJQAEgJAJgEQAJgEAOAAICYAAQAQAAAIAHQAHAHAAALQAAAMgHAHQgIAGgQAAIh/AAIAABTIBqAAQAPAAAIAHQAHAGAAALQAAALgIAGQgHAHgPAAIhqAAIAABqQAAAUgJAKQgJAKgPAAQgOAAgJgKg");
	this.shape.setTransform(0.6,-1.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#660000").s().p("AhmCaQgJgKAAgUIAAj3QAAgOAEgJQAEgJAJgEQAJgEAOAAICYAAQAQAAAIAHQAHAHAAALQAAAMgHAHQgIAGgQAAIh/AAIAABTIBqAAQAPAAAIAHQAHAGAAALQAAALgIAGQgHAHgPAAIhqAAIAABqQAAAUgJAKQgJAKgPAAQgOAAgJgKg");
	this.shape_1.setTransform(1.9,0.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3333").s().p("AjQD4QhagBAAhKIAAlZQAAhKBagBIGhAAQBaABAABKIAAFZQAABKhaABg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#663300").ss(3,1,1).p("AislCIFZAAQCWAAAACWIAAFZQAACWiWAAIlZAAQiWAAAAiWIAAlZQAAiWCWAAg");

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC0000").s().p("AisFCQiVAAAAiVIAAlZQAAiVCVAAIFZAAQCVAAAACVIAAFZQAACViVAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,255,255,0.027)").s().p("AjgF9QicABAAidIAAnAQAAieCcAAIHBAAQCcAAAACeIAAHAQAACdicgBg");
	this.shape_5.setTransform(0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbolalphabetcopy, new cjs.Rectangle(-38,-37.9,76.3,76.3), null);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape.setTransform(179,-0.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgPAwQgKgEgHgHQgHgHgEgJQgFgKAAgLQAAgFACgHQACgGADgGQADgFAFgFQAFgFAGgEQAFgDAHgCQAHgCAGAAQAIAAAHACQAGACAGADQAGAEAFAFIAIALIAAAAIgPAJIAAAAIgFgIIgIgGIgIgEIgKgCQgGAAgHADQgGADgEAFQgFAEgDAHQgDAGAAAGQAAAHADAHQADAGAFAFQAEAFAGACQAHADAGAAQAFAAAEgBQAEgBAEgDQAEgCADgDQAEgDACgEIAAgBIAQAJIgBAAQgDAGgFAFIgLAIIgMAFIgOABQgJAAgJgDg");
	this.shape_1.setTransform(169.3,1.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_2.setTransform(158.6,1.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgZBbIABgUQAFABAEgBIAHgDIAEgEIAEgGQACgGAAgIIABgXIAAgcIAAgnIASAAIAAAZIAAAUIAAAPIAAAMIAAASIgBAKQgBAFgCAFIgFAKQgDAFgFAEQgEAEgIACQgGACgIAAIgDAAgAAJhBIgEgDIgDgEIgBgFIABgFIADgEIAEgDIAFgBIAFABIAEADIACAEIABAFIgBAFIgCAEIgEADIgFAAIgFAAg");
	this.shape_3.setTransform(149.7,1.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHBDIgLgEQgGgDgFgFIAAAMIgRAAIgBiGIATgBIAAA0QAEgFAGgDIAKgEIAJgBIANABIALAFQAFADAFAFQAEAEADAGQADAFACAFQABAGAAAHQAAAIgCAHIgFAMIgIAKIgJAHIgKAFIgLACIgKgCgAgJgLIgJAFIgHAGQgDAEgBAEIAAAVIAEAKIAHAHIAJAEQADACAFAAQAGAAAFgCQAGgDAEgEQAEgFADgFQACgGAAgHQABgHgCgGQgCgGgEgEQgEgEgGgDQgGgDgGAAIgJACg");
	this.shape_4.setTransform(143.2,-0.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRAwQgJgEgHgGQgGgHgEgKQgDgJAAgMQAAgLADgJQAEgKAGgGQAHgHAJgEQAIgEAJAAQAKAAAIAEQAJAEAGAHQAGAHAFAKQADAJAAAKQAAALgDAKQgFAJgGAHQgGAHgJAEQgIAEgKAAQgJAAgIgEgAgMggQgGADgDAGQgEAFgBAGIgBAMQAAAGACAHQABAGADAFQAEAFAFAEQAGADAGAAQAIAAAEgDQAGgEADgFQAEgFACgGIACgNIgCgMQgCgGgDgFQgDgGgGgDQgGgDgHAAQgHAAgFADg");
	this.shape_5.setTransform(131.7,1.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_6.setTransform(116.4,1.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAXBDIAEgRIACgOIAAgOQgBgKgDgGQgCgGgEgEQgEgEgFgCQgFgCgFAAQgEABgFACQgFACgFAFQgEADgFAIIAAA6IgSABIgBiHIAUAAIAAA1QAEgFAGgDIAKgFIAKgCQAKAAAIADQAIAEAGAGQAGAHADAJQADAJABAMIAAAMIgBANIgBAMIgDAKg");
	this.shape_7.setTransform(105.2,-0.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_8.setTransform(94.9,-0.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_9.setTransform(81.7,-0.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAYAYIgGAKIgIAIIgJAFQgEACgGAAQgIAAgFgDQgGgDgEgFQgEgEgCgGIgEgMIgBgOIgBgMIABgSIACgUIATABIgCATIgBAQIAAAHIABAJIACAKQABAFACAEQACAEAEACQADACAFgBQAGAAAHgJQAIgJAIgPIgBgXIAAgNIAAgHIATgCIABAcIAAAUIABARIAAANIABASIgUABg");
	this.shape_10.setTransform(71.7,1.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgSAwQgIgEgGgGQgHgHgDgKQgEgJAAgMQAAgLAEgJQADgKAHgGQAGgHAIgEQAJgEAJAAQAJAAAJAEQAJAEAGAHQAGAHAEAKQAEAJAAAKQAAALgEAKQgEAJgGAHQgGAHgJAEQgJAEgJAAQgJAAgJgEgAgMggQgGADgDAGQgEAFgBAGIgCAMQAAAGADAHQABAGADAFQAEAFAFAEQAFADAHAAQAIAAAFgDQAFgEAEgFQADgFACgGIACgNIgCgMQgBgGgEgFQgDgGgGgDQgFgDgIAAQgHAAgFADg");
	this.shape_11.setTransform(60.8,1.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAFBMQgEgCgDgDQgDgDgDgEIgFgIQgDgKgCgNIADhvIARAAIgBAdIAAAYIgBATIAAAOIAAAYQAAAIACAGIACAGIADAEIAFAEIAHABIgDASQgFgBgGgCg");
	this.shape_12.setTransform(48.7,-1.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAFBMQgEgCgDgDQgEgDgCgEIgFgIQgDgKgCgNIADhvIARAAIgBAdIAAAYIgBATIAAAOIAAAYQAAAIACAGIACAGIADAEIAFAEIAHABIgDASQgGgBgFgCg");
	this.shape_13.setTransform(43.7,-1.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_14.setTransform(35.4,1.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgvhHIATAAIAAAMQAEgFAGgCIAKgEIAJgCIANABIALAGQAFADAEAEQAFAEADAGQADAFACAHQACAFAAAHQgBAIgCAHQgCAGgDAFQgEAGgEAEIgJAHIgKAGIgMABIgJgBIgLgFQgFgCgGgFIAAA7IgRABgAAAg2QgFAAgFACQgEACgEADQgEADgDAEQgDAEAAAEIgBARQABAGADAEQADAFAEACQAEADAEACQAFABAEAAQAFAAAGgCQAFgCAEgEQAEgEADgFQACgHAAgGQABgGgCgGQgCgGgEgFQgEgEgGgDQgEgCgGAAIgBAAg");
	this.shape_15.setTransform(24.5,3.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgMAxIgIgBIgHgCIgIgFIgIgEIAJgOIAKAFIAJAEIALADIALABIAIgBIAGgCIADgDIACgDIAAgEIgBgDIgDgDIgEgEIgHgCIgJgBIgPgBIgNgFQgFgDgEgDQgEgFgBgGQAAgHABgFQACgFADgEIAHgHQAEgEAFgBIALgEIAKgBIAIABIAKABIAKAEIAKAEIgGAQIgLgEIgKgDIgIgBQgNgBgIADQgIAFAAAHQAAAFADACQAEADAEABIAMABIANACQAJACAGACQAGACAEADQADADABAFQACAEAAAFQAAAIgDAGQgDAGgHADQgFADgHACIgQADQgGgBgJgCg");
	this.shape_16.setTransform(13.3,1.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgSAwQgIgEgHgGQgGgHgEgKQgDgJAAgMQAAgLADgJQAEgKAGgGQAHgHAIgEQAJgEAJAAQAJAAAJAEQAJAEAGAHQAHAHAEAKQADAJAAAKQAAALgDAKQgEAJgHAHQgGAHgJAEQgJAEgJAAQgJAAgJgEgAgMggQgGADgDAGQgDAFgCAGIgBAMQAAAGABAHQACAGAEAFQADAFAGAEQAEADAHAAQAHAAAFgDQAGgEADgFQAEgFACgGIABgNIgBgMQgCgGgDgFQgEgGgFgDQgFgDgIAAQgGAAgGADg");
	this.shape_17.setTransform(-1.7,1.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_18.setTransform(-11.4,-0.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgMAxIgIgBIgHgCIgIgFIgIgEIAJgOIAKAFIAJAEIALADIALABIAIgBIAFgCIAEgDIACgDIAAgEIgBgDIgDgDIgEgEIgHgCIgJgBIgPgBIgNgFQgFgDgEgDQgEgFgBgGQAAgHABgFQACgFADgEIAHgHQAEgEAFgBIALgEIAKgBIAIABIAKABIAKAEIAKAEIgGAQIgLgEIgKgDIgIgBQgNgBgIADQgIAFAAAHQAAAFADACQAEADAEABIAMABIANACQAJACAGACQAGACAEADQADADABAFQACAEAAAFQAAAIgDAGQgDAGgHADQgFADgHACIgQADQgGgBgJgCg");
	this.shape_19.setTransform(-25.4,1.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgnAVIgBgVIAAgRIgBgMIAAgSIAUgBIAAALIAAAKIAAAKIAGgIIAJgKIAJgHQAFgDAGgBQAIAAAFADIAFAEIAFAGIADAIIACANIgRAGIgBgIIgCgFIgCgEIgDgCQgDgDgDABIgGACIgFAFIgFAFIgGAHIgFAIIgEAFIAAAMIAAAMIABAKIAAAHIgTADIgBgcg");
	this.shape_20.setTransform(-34.8,1.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_21.setTransform(-45.4,1.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_22.setTransform(-55,-0.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_23.setTransform(-63.6,-0.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_24.setTransform(-73.1,1.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAFBMQgEgCgDgDQgDgDgDgEIgFgIQgDgKgCgNIADhvIARAAIgBAdIAAAYIgBATIAAAOIAAAYQAAAIACAGIACAGIADAEIAFAEIAHABIgDASQgFgBgGgCg");
	this.shape_25.setTransform(-80.4,-1.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_26.setTransform(-93.4,1.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAXBDIAEgRIACgOIAAgOQgBgKgCgGQgDgGgEgEQgEgEgFgCQgFgCgFAAQgEABgFACQgEACgFAFQgGADgDAIIgBA6IgRABIgCiHIAVAAIgBA1QAEgFAGgDIAKgFIAKgCQAKAAAIADQAIAEAGAGQAGAHADAJQADAJABAMIAAAMIgBANIgBAMIgCAKg");
	this.shape_27.setTransform(-104.6,-0.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_28.setTransform(-114.9,-0.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgKgJIgeAAIAAgQIAfgBIAAgmIARgBIgBAnIAigBIgBAQIghABIgBBMIgRAAg");
	this.shape_29.setTransform(-128.2,-0.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgPAwQgKgEgHgHQgHgHgEgJQgFgKAAgLQAAgFACgHQACgGADgGQADgFAFgFQAFgFAGgEQAFgDAHgCQAHgCAGAAQAIAAAHACQAGACAGADQAGAEAFAFIAIALIAAAAIgPAJIAAAAIgFgIIgIgGIgIgEIgKgCQgGAAgHADQgGADgEAFQgFAEgDAHQgDAGAAAGQAAAHADAHQADAGAFAFQAEAFAGACQAHADAGAAQAFAAAEgBQAEgBAEgDQAEgCADgDQAEgDACgEIAAgBIAQAJIgBAAQgDAGgFAFIgLAIIgMAFIgOABQgJAAgJgDg");
	this.shape_30.setTransform(-137.9,1.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_31.setTransform(-148.6,1.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAFBMQgFgCgCgDQgEgDgCgEIgFgIQgEgKgBgNIADhvIARAAIgBAdIAAAYIgBATIAAAOIAAAYQAAAIACAGIACAGQABACADACIAEAEIAHABIgCASQgGgBgGgCg");
	this.shape_32.setTransform(-155.9,-1.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgTAvQgJgDgGgHQgGgGgDgKQgEgIAAgLQAAgKAEgKQAEgKAGgGQAHgHAJgFQAIgEAKAAQAJABAIADQAIADAGAGQAGAHAFAIQAEAJABAKIhLAKQAAAGADAGQACAFAEADQAEADAFACQAFACAFAAIAJgCIAIgDIAHgIIAEgJIARADQgCAIgFAHQgEAGgGAFQgGAEgHADQgHADgIgBQgLABgJgEgAgGghQgFABgEADQgEADgDAFQgEAGgBAHIA0gGIgBgCQgDgJgGgFQgGgEgIAAIgHABg");
	this.shape_33.setTransform(-164.2,1.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgKBHQgHgBgGgDIgNgHIgMgIIANgOQAGAFAHAEIAMAFQAGACAFABQAGAAAGgBIAKgEIAGgGQACgDABgEQAAgDgBgDIgEgFIgHgEIgIgDIgIgDIgIgBIgKgCIgLgEIgKgGQgFgDgEgEQgDgFgCgHQgCgGABgJQAAgIADgFQACgFAEgFQAEgEAFgDQAFgEAGgCIALgDIALgBIARADIAHACIAIADIAIAFIAHAGIgKAPIgGgFIgGgFIgGgCIgGgCIgOgDQgHAAgGADQgGACgEAEQgFADgCAFQgCAFAAAEQAAAFADAEQACAEAFAEQAFADAGADQAHADAGAAIANACIAMAEIALAGQAFADADAEQADAFACAGQABAGgBAGQgBAGgCAFQgDAFgEADQgDAEgFADIgKADIgLACIgKABIgMgBg");
	this.shape_34.setTransform(-175.4,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#660202").ss(4,1,1).p("A6ojKMA1RAAAQBTAAA6A7QA7A6AABTIAAAFQAABTg7A6Qg6A7hTAAMg1RAAAQhTAAg6g7Qg7g6AAhTIAAgFQAAhTA7g6QA6g7BTAAg");
	this.shape_35.setTransform(1.4,0.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#CD0D0D").s().p("A6oDLQhTAAg6g7Qg7g6AAhTIAAgFQAAhTA7g6QA6g7BTAAMA1RAAAQBTAAA6A7QA7A6AABTIAAAFQAABTg7A6Qg6A7hTAAg");
	this.shape_36.setTransform(1.4,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-191.1,-21.7,385,44.6), null);


(lib.objectsnewcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// QuestionDisp
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#201F1F").s().p("AknGRQgYgbAAgyIAAqFQAAg5AagYQAZgYA4AAIDZAAQBhAAA1APQAzAPAmAhQAlAhAUAwQATAwAAA8QAACAhPBCQhPBCidAAIiaAAIAADtQAAAzgYAaQgYAbgkAAQgnAAgXgagAiVgqIBzAAQA6AAApgMQAogNAWgcQAUgdAAguQAAg4gggjQglglhwAAIhzAAg");
	this.shape.setTransform(188.2,0.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#201F1F").s().p("AEQGrQgOgIgKgMQgKgMgLgaIgTgtIgohpIlUAAIgpBsQgXA+gQAXQgRAWgmAAQggAAgZgYQgYgXAAgeQAAgRAFgTIATgzIDXofIAXg4QAMgiAPgVQAOgWAYgNQAYgOAjAAQAiAAAYAOQAXANAPAVQAPAVAKAZIAaBBIDaIcQAaA+AAAcQAAAdgYAZQgZAYgiAAQgUAAgOgHgAh/BbID5AAIh+lZg");
	this.shape_1.setTransform(104.8,-0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#201F1F").s().p("AhZGlQg2gNgsgaQgtgbglgrQgigogXgwQgXgygMg3QgMg4AAg+QAAhkAehQQAdhRA3g4QA3g5BKgeQBKgeBSAAQBmAABPApQBQAoAqA8QArA8AAA1QAAAdgVAXQgVAWgdAAQggAAgQgPQgRgQgUgmQghg+gtgfQgtgfhCAAQhnAAg+BQQg/BPAACSQAABiAcBBQAbBAAzAgQAyAhBDAAQBJAAAzglQAygkAahHQALghAQgUQAQgVAjAAQAeAAAWAVQAWAVAAAfQAAAogTAvQgUAvgrAtQgqAshCAdQhCAchXAAQhCAAg2gNg");
	this.shape_2.setTransform(19.2,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Bag
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(3,1,1).p("Al4oBQgQgQAAgVQAAgbAggNQAhgNAiAAQAjAAAQANQAQANAAAbQAAAEgBAEQgDAVgUAQQgBAAAAABQgYASgiAAQgTAAgQgGQgHgDgGgDQgGgDgFgEQgFgEgDgEQrHChBbMAQA2gbA1gaQBMgkBJghQCsHUI9kRQAGgDAGgDQAygaAwgXQDThlCigmQAAgBABAAQD5g8CEBZQAOAIAMALQBWBKAaCVQgGAIgGAHQhbBglRkvQgmghgognAqMDlQh2njFujbQASgLAUgKQACgBABgBQADgBADgBAjjoeQABAAABAAQDqgeD7DKQEFDRAlDoIAEADIFREIIATAOArkEmQAygWAwgVIABAAQAagLAagLQBYglBTgfQJsjoEfBxQAlAPAgAV");
	this.shape_3.setTransform(-134.7,-1.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#990000").s().p("AIFE0QAVgDAPgFQAtgPA1AAQByAABWBdQAjAmA8BjQgWAYglAAQhyAAkAjngAnGoaQlvDbB3HjIACAGIAIAQIhiArQhJAhhNAkQg1qCIbjCg");
	this.shape_4.setTransform(-129.6,5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC0000").s().p("AszgDIBigqIAAAAIA1gWQCPGbHEiYQjTBlidAAQkNAAhtkogAnxiIQJsjoEfByQAlAPAfAVIAFgEIFREHIgDAEQgMgLgOgJQiEhXj5A8IgBAAQiiAmjTBlQhTAWhJAAQkNAAhskng");
	this.shape_5.setTransform(-126.7,28.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF6666").s().p("AjOCKQhVg5AAhRQAAhQBVg5QBWg5B4AAQB5AABWA5QBWA5AABQQAABRhWA5QhWA5h5AAQh4AAhWg5g");
	this.shape_6.setTransform(-144.3,-25.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AOFGaQhVhdhzAAQg1AAgtAPQgPAFgUADQgmghgognIgBgHQD5g8CEBYQAOAJAMALQBWBJAaCVIgMAQQg8hjgjgmgAl4oiIAKATQgUAKgSALQobDCA2KCIhrA1QhbsALHihg");
	this.shape_7.setTransform(-134.7,1.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF3333").s().p("AoRDuQBYgmBTgfQCKF3GLhlQgwAWgyAaIgMAHQh5AphjAAQkNAAhpktgApNDzIgDgFQh2njFujbQASgLAUgKIADgCIAGgDIANAHQAQAFATAAQAiAAAYgSIABgBQAUgQADgUIACgBQDqgdD7DJQEFDSAlDnIAEADIgEAFQgggVglgPQkfhxpsDnQhTAfhYAmIg0AWgAjylxQhWA5AABRQAABRBWA5QBWA4B5AAQB4AABWg4QBVg5AAhRQAAhRhVg5QhWg5h4AAQh5AAhWA5g");
	this.shape_8.setTransform(-140.7,-2.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("AgiA1IgNgHIgLgHIgIgHQgQgRAAgUQAAgaAggNQAhgNAhAAQAjAAAQANQAQANAAAaIgBAIQgDAUgUAQIgBABQgYASgiAAQgSAAgQgFg");
	this.shape_9.setTransform(-165.7,-56);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 2
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#083756").ss(5,1,1).p("EghEgPzMBCJAAAQCxAAB+B9QB9B+AACxIAASPQAACxh9B+Qh+B9ixAAMhCJAAAQiyAAh9h9Qh9h+AAixIAAyPQAAixB9h+QB9h9CyAAg");
	this.shape_10.setTransform(0,-1.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("EghEAP0QiyAAh9h+Qh9h9AAixIAAyPQAAixB9h+QB9h9CyAAMBCJAAAQCxAAB+B9QB9B+AACxIAASPQAACxh9B9Qh+B+ixAAg");
	this.shape_11.setTransform(0,-1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10}]}).wait(1));

	// Layer 3
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(255,255,255,0.027)").s().p("EghmARrQjQAAiVhmQiUhnAAiQIAA4bQAAiRCUhmQCVhmDQAAMBDNAAAQDRAACUBmQCVBmAACRIAAYbQAACQiVBnQiUBmjRAAg");
	this.shape_12.setTransform(-0.7,-1.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

}).prototype = getMCSymbolPrototype(lib.objectsnewcopy2, new cjs.Rectangle(-266.3,-114.9,531.3,226.2), null);


(lib.objectsnew = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// QuestionDisp
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#201F1F").s().p("AkrAdIAAg5IJXAAIAAA5g");
	this.shape.setTransform(186.1,53.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#201F1F").s().p("AEQGrQgOgIgKgMQgKgMgLgaIgTgtIgohpIlVAAIgnBsQgYA+gRAXQgQAWgmAAQggAAgYgYQgZgXAAgeQAAgRAGgTIATgzIDWofIAWg4QANgiAPgVQAOgWAYgNQAXgOAkAAQAiAAAYAOQAYANAPAVQAOAVAKAZIAZBBIDcIcQAZA+AAAcQAAAdgYAZQgYAYgjAAQgUAAgOgHgAiABbID6AAIh9lZg");
	this.shape_1.setTransform(114.6,-0.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#201F1F").s().p("AhZGlQg2gNgsgaQgtgbglgrQgigogXgwQgXgygMg3QgMg4AAg+QAAhkAehQQAdhRA3g4QA3g5BKgeQBKgeBSAAQBmAABPApQBQAoAqA8QArA8AAA1QAAAdgVAXQgVAWgdAAQggAAgQgPQgRgQgUgmQghg+gtgfQgtgfhCAAQhnAAg+BQQg/BPAACSQAABiAcBBQAbBAAzAgQAyAhBDAAQBJAAAzglQAygkAahHQALghAQgUQAQgVAjAAQAeAAAWAVQAWAVAAAfQAAAogTAvQgUAvgrAtQgqAshCAdQhCAchXAAQhCAAg2gNg");
	this.shape_2.setTransform(29,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Bag
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(3,1,1).p("Al4oBQgQgQAAgVQAAgbAggNQAhgNAiAAQAjAAAQANQAQANAAAbQAAAEgBAEQgDAVgUAQQgBAAAAABQgYASgiAAQgTAAgQgGQgHgDgGgDQgGgDgFgEQgFgEgDgEQrHChBbMAQA2gbA1gaQBMgkBJghQCsHUI9kRQAGgDAGgDQAygaAwgXQDThlCigmQAAgBABAAQD5g8CEBZQAOAIAMALQBWBKAaCVQgGAIgGAHQhbBglRkvQgmghgognAqMDlQh2njFujbQASgLAUgKQACgBABgBQADgBADgBAjjoeQABAAABAAQDqgeD7DKQEFDRAlDoIAEADIFREIIATAOArkEmQAygWAwgVIABAAQAagLAagLQBYglBTgfQJsjoEfBxQAlAPAgAV");
	this.shape_3.setTransform(-134.7,-1.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#990000").s().p("AIFE0QAVgDAPgFQAtgPA1AAQByAABWBdQAjAmA8BjQgWAYglAAQhyAAkAjngAnGoaQlvDbB3HjIACAGIAIAQIhiArQhJAhhNAkQg1qCIbjCg");
	this.shape_4.setTransform(-129.6,5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC0000").s().p("AszgDIBigqIAAAAIA1gWQCPGbHEiYQjTBlidAAQkNAAhtkogAnxiIQJsjoEfByQAlAPAfAVIAFgEIFREHIgDAEQgMgLgOgJQiEhXj5A8IgBAAQiiAmjTBlQhTAWhJAAQkNAAhskng");
	this.shape_5.setTransform(-126.7,28.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF6666").s().p("AjOCKQhVg5AAhRQAAhQBVg5QBWg5B4AAQB5AABWA5QBWA5AABQQAABRhWA5QhWA5h5AAQh4AAhWg5g");
	this.shape_6.setTransform(-144.3,-25.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#330000").s().p("AOFGaQhVhdhzAAQg1AAgtAPQgPAFgUADQgmghgognIgBgHQD5g8CEBYQAOAJAMALQBWBJAaCVIgMAQQg8hjgjgmgAl4oiIAKATQgUAKgSALQobDCA2KCIhrA1QhbsALHihg");
	this.shape_7.setTransform(-134.7,1.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF3333").s().p("AoRDuQBYgmBTgfQCKF3GLhlQgwAWgyAaIgMAHQh5AphjAAQkNAAhpktgApNDzIgDgFQh2njFujbQASgLAUgKIADgCIAGgDIANAHQAQAFATAAQAiAAAYgSIABgBQAUgQADgUIACgBQDqgdD7DJQEFDSAlDnIAEADIgEAFQgggVglgPQkfhxpsDnQhTAfhYAmIg0AWgAjylxQhWA5AABRQAABRBWA5QBWA4B5AAQB4AABWg4QBVg5AAhRQAAhRhVg5QhWg5h4AAQh5AAhWA5g");
	this.shape_8.setTransform(-140.7,-2.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("AgiA1IgNgHIgLgHIgIgHQgQgRAAgUQAAgaAggNQAhgNAhAAQAjAAAQANQAQANAAAaIgBAIQgDAUgUAQIgBABQgYASgiAAQgSAAgQgFg");
	this.shape_9.setTransform(-165.7,-56);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

	// Layer 2
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#083756").ss(5,1,1).p("EghEgPzMBCJAAAQCxAAB+B9QB9B+AACxIAASPQAACxh9B+Qh+B9ixAAMhCJAAAQiyAAh9h9Qh9h+AAixIAAyPQAAixB9h+QB9h9CyAAg");
	this.shape_10.setTransform(0,-1.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("EghEAP0QiyAAh9h+Qh9h9AAixIAAyPQAAixB9h+QB9h9CyAAMBCJAAAQCxAAB+B9QB9B+AACxIAASPQAACxh9B9Qh+B+ixAAg");
	this.shape_11.setTransform(0,-1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10}]}).wait(1));

	// Layer 3
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(255,255,255,0.027)").s().p("EghmARrQjQAAiVhmQiUhnAAiQIAA4bQAAiRCUhmQCVhmDQAAMBDNAAAQDRAACUBmQCVBmAACRIAAYbQAACQiVBnQiUBmjRAAg");
	this.shape_12.setTransform(-0.7,-1.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

}).prototype = getMCSymbolPrototype(lib.objectsnew, new cjs.Rectangle(-266.3,-114.9,531.3,226.2), null);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbolalphabetcopy();
	this.instance.parent = this;
	this.instance.setTransform(-279.9,-0.2,2.826,2.826,0,0,0,0.2,0.1);

	this.instance_1 = new lib.Symbolalphabetcopy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(280.4,-0.2,2.826,2.826,0,0,0,0.1,0.1);

	this.instance_2 = new lib.Symbolalphabetcopy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.4,-0.2,2.826,2.826,0,0,0,0.2,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-387.9,-107.8,776,215.6);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("AggCiQgMgEgLgFQg9gegYg/QgYg/Aeg9QAcg/BBgXQA/gYA9AeQAtAUAYAn");
	this.shape.setTransform(403.4,119.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AgpD0QgXgGgXgLQhcgsgjhgQgjhfArhdQArhcBggjQAQgFAPgEQA2gMA0AMQAZAGAYAMQBLAiAlBC");
	this.shape_1.setTransform(403.2,119.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AgIk0QAIgIAKgGQALgFAMgGQBVgjBoAPQBrAQCJFiQhYCwhWAWQhqAbg0AEQgTABgYALQgYAKg2ApQg1AqgiAmQghAkgRgMQg0gmABhGQACg9Alg1QAlg1AvgQQAOgGAggJQAQgFAPgGQhthXhugtQgcgMgagRQgRgJgQgMQghgZgggfQgXgZg5hIQg5hKAchDQAdhEBXA/QBYA9BAA+QACADADACQAZAXAYARQAKAIAMAGQADACAEADQAdASAgALQBGAYANACAhqjVQAJgOAMgPQAUgYAZgTQAPgNARgKIAAAAQBHgTBbBH");
	this.shape_2.setTransform(445.4,153.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("Ah8GdQg0gmABhGQACg9Alg1QAlg1AvgQQAOgGAggJIAfgLQhthXhugtQgcgMgagRQgRgJgQgMQghgZgggfQgXgZg5hIQg5hKAchDQAdhEBXA/QBYA9BAA+IAFAFQAZAXAYARQAKAIAMAGIAHAFQAdASAgALQBGAYANACQgNgChGgYQgggLgdgSIgHgFIAFgKQAJgOAMgPQAUgYAZgTQAPgNARgKIAAAAQAIgIAKgGIAXgLQBVgjBoAPQBrAQCJFiQhYCwhWAWQhqAbg0AEQgTABgYALQgYAKg2ApQg1AqgiAmQgZAbgQAAQgFAAgEgDgACakAIgFgEQhFg0g6AAIAAAAIAAAAQgQAAgOAEQAOgEAQAAIAAAAIAAAAQA6AABFA0IAFAEg");
	this.shape_3.setTransform(445.4,153.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(426.3,130.7,1.452,1.452);
	this.instance.alpha = 0.852;
	this.instance.filters = [new cjs.BlurFilter(53, 53, 1)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(326.7,31.1,203,203);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(6.6,-8.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.95,scaleY:0.95,x:6.4,y:25.6},15).to({scaleX:1,scaleY:1,x:6.6,y:-8.1},19).wait(1));

	// Layer_3
	this.instance_1 = new lib.Tween10("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,35.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.95,scaleY:0.95,y:67.3},15).to({scaleX:1,scaleY:1,y:35.9},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122,-127.1,247.9,326.2);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Wow
	this.instance = new lib.Symbol6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(637.9,346.4,1.111,1.111);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(185).to({_off:false},0).wait(53));

	// hand
	this.instance_1 = new lib.Tween8("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(263.2,503.2);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({_off:false},0).to({alpha:1},7).to({startPosition:0},13).to({scaleX:1.1,scaleY:1.1},9).to({scaleX:1,scaleY:1},6).to({_off:true},31).wait(53));

	// arrow
	this.instance_2 = new lib.Tween7("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(635.6,434.9,1,1,0,0,0,15.4,-84.9);
	this.instance_2.alpha = 0.602;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(89).to({_off:false},0).to({y:464.9,alpha:1},13).to({y:434.9},9).to({y:464.9,alpha:0.801},15).to({_off:true},1).wait(111));

	// Layer_8
	this.instance_3 = new lib.objectsnewcopy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(622.9,313.6,1.519,1.519,0,0,0,-12,-14.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(155).to({_off:false},0).wait(25).to({alpha:0},4).to({_off:true},1).wait(53));

	// Layer_7
	this.instance_4 = new lib.Tween18("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(640,613.3);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(38).to({_off:false},0).to({alpha:1},5).to({startPosition:0},137).to({alpha:0},4).to({_off:true},1).wait(53));

	// Layer_6
	this.instance_5 = new lib.objectsnew();
	this.instance_5.parent = this;
	this.instance_5.setTransform(622.9,313.6,1.519,1.519,0,0,0,-12,-14.3);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(19).to({_off:false},0).to({alpha:1},5).wait(156).to({alpha:0},4).to({_off:true},1).wait(53));

	// Layer_5
	this.instance_6 = new lib.Symbol1copy();
	this.instance_6.parent = this;
	this.instance_6.setTransform(637.3,109.7,1.973,1.973);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(5).to({_off:false},0).to({alpha:1},5).wait(170).to({alpha:0},4).to({_off:true},1).wait(53));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(495.6,262.8,1563,915);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;