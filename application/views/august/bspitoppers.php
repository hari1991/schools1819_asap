 <header class="head">
                                <!-- /.search-bar -->
                            <div class="main-bar">
                                <h3>
              <i class="fa fa-dashboard"></i>&nbsp;
            BSPI Toppers
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
						
						
            </div>
	 <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">		
<div class="row">
  <div class="col-lg-12">
 
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>BSPI Toppers</h5>
            </header>
			<form id="srchreport" action="<?php echo base_url(); ?>index.php/admin/bspitoppers" method="POST">
			<div class="row">
				<div class="col-sm-3">
					<label>School</label>
					<select  class="form-control input-sm" name="schoolid" id="schoolid">
					<option value="">Select</option>
					<?php foreach($schools as $sc) { ?>
					<option value="<?php echo $sc['id']; ?>"  <?php if($_POST['schoolid']== $sc['id']) { ?>  selected='selected' <?php } ?>     ><?php echo $sc['school_name']; ?></option>
					<?php } ?>
					</select>
				</div>

					<div class="col-sm-3" style="display:none";>
			<label>Schools</label>
			<select  class="form-control input-sm" name="sklid" id="sklid">
			<option value="">Select</option>
			<?php foreach($skls as $skl) { ?>
			<option value="<?php echo $skl['sklname']; ?>" <?php if($_POST['sklid']== $skl['sklname']) { ?>  selected='selected' <?php } ?>  ><?php echo $skl['sklname']; ?></option>
			<?php } ?>
			</select>
			</div>
				
				<div class="col-sm-2">
					<label>Grade</label>
					<select  class="form-control input-sm" name="gradeid" id="gradeid" >
					<option value="">Select</option>
					<?php foreach($grades as $gd) { ?>
					<option value="<?php echo $gd['id']; ?>" <?php if($_POST['gradeid']== $gd['id']) { ?>  selected='selected' <?php } ?>  ><?php echo $gd['classname']; ?></option>
					<?php } ?>
					</select>
					<!--<input type="button" name="sbmtbtn" id="sbmtbtn" Value="Get result">-->
				</div>
				
				
				
				<div class="col-sm-2">
					<label>Top</label>
					<select  class="form-control input-sm" name="topid" id="topid" >
					<option value="">Select</option>
					<option value="1" <?php if($_POST['topid']== 1) { ?>  selected='selected' <?php } ?>>1</option>
					<option value="2" <?php if($_POST['topid']== 2) { ?>  selected='selected' <?php } ?>>2</option>
					<option value="3" <?php if($_POST['topid']== 3) { ?>  selected='selected' <?php } ?>>3</option>
					<option value="4" <?php if($_POST['topid']== 4) { ?>  selected='selected' <?php } ?>>4</option>
					<option value="5" <?php if($_POST['topid']== 5) { ?>  selected='selected' <?php } ?>>5</option>
					<option value="6" <?php if($_POST['topid']== 6) { ?>  selected='selected' <?php } ?>>6</option>
					<option value="7" <?php if($_POST['topid']== 7) { ?>  selected='selected' <?php } ?>>7</option>
					<option value="8" <?php if($_POST['topid']== 8) { ?>  selected='selected' <?php } ?>>8</option>
					<option value="9" <?php if($_POST['topid']== 9) { ?>  selected='selected' <?php } ?>>9</option>
					<option value="10" <?php if($_POST['topid']== 10) { ?>  selected='selected' <?php } ?>>10</option>
					</select>
				</div>
				
				<div class="col-sm-1">
			<label>Section</label>
			<select  class="form-control input-sm sectionid" name="sectionid"  id="sectionid" >
			<option value="">Select</option>
			<?php if(isset($_POST['gradeid'])){   	 ?>
			<option value="<?php echo $data; ?>" selected='selected'><?php echo $data; ?></option>
			 <?php } ?>
			</select>
			</div>
				
				
				<div class="col-sm-2">
					<input type="submit" name="sbmtbtn" id="sbmtbtn" Value="Get result">
				</div>
			</div>
			</form>
		</div>
            <div id="collapse4" class="body">
				<div class="row" <?php if(isset($GetTOPBSPIUser)) {  } else {?> style="display:none;" <?php } ?>>
      			<div class="col-lg-12 landingContainer">
				 <table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
					<thead>
					  <tr>
						<th>S.No.</th>
						<th>Username</th>
						<th>BSPI</th>
					   </tr>
					</thead>
					<tbody>	 
					<?php $inj=1; foreach($GetTOPBSPIUser as $row)
					 {  
					?>	
					<tr>
						<td><?php echo $inj; ?></td>
						<td><?php  echo $row['name']; echo "<br/>"; ?></td>
						<td><?php  echo ROUND(($row['finalscore']),2); ?></td> 
					</tr>
					<?php  
			$inj++; }
					?>
					</tbody>
				  </table>
      			</div>
				
				
 			</div>
		
			</div>         
        
		</div>
    </div>
</div>
</div>
</div>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
 <script type="text/javascript">

<?php //print_r($GradewiseScore_data1); ?> 
$(document).ready(function(){
	ajaxsectionload(schoolid,gradeid,'<?php echo $_POST['sectionid']; ?>');
	
$("#srchreport").validate({
		rules: {
			schoolid: {required: true},
			gradeid:{required: true},
            topid: {required: true} 
		   },
		   messages: {
			schoolid: {required: "Please choose school"},
			gradeid: {required: "Please choose grade"},
			topid: {required: "Please choose top value"}				
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},
	});
});


$('#gradeid').change(function(){
	
	ajaxsectionload(schoolid,gradeid,'');
});


function ajaxsectionload(schoolid,gradeid,selectID)
{
var schoolid = 	$('#schoolid').val();
var gradeid = $('#gradeid').val();

$.ajax({
		 url: "<?php echo base_url(); ?>index.php/admin/getsection",
		data:{schoolid:schoolid,gradeid:gradeid,defaultselect:selectID},
		success: function(result)
		{
		//alert(result);
		$('#sectionid').html(result);
		}
	});
	}

</script>
 <style>
 body{min-height:0 !important;}
 .nice-select span.current{font-size: 20px}
 .nice-select .option {font-size: 10px}
 .nice-select ul{height:200px;overflow-y:scroll !important}
 #sbmtbtn{padding: 10px 22px;margin: 5px auto;background: #e9e610;}
.error{color:red;}
 </style>