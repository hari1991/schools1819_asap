(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("ATIkXQgUILlqFoQliFgm0KUQoaqUlilgQlqlogUoLQgBgdAAgcQgBn4FQl0QCZirC4hhIBfgvIDchDQBygVB6AAQB8AAByAVIDbBDIBgAvQC3BhCaCrQFPF0AAH4QAAAcgBAdg");
	this.shape.setTransform(0,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ArMLNQkpkpAAmkQAAmjEpkpQEpkpGjAAQGkAAEpEpQEpEpAAGjQAAGkkpEpQkpEpmkAAQmjAAkpkpg");
	this.shape_1.setTransform(0,-46.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC3333").s().p("AtIIZQlqlpgUoKIgCg6QAAn4FQlzQCZirC3hiQiLBOh6B6QlSFSAAHdIABA0QARG8FAE9QFRFSIPKNQGqqNFSlSQE/k9ARm8IABg0QAAndlRlSQh6h6iMhOQC3BiCZCrQFQFzAAH4IgCA6QgTIKlqFpQliFfm1KUQoaqUlhlfg");
	this.shape_2.setTransform(0,6.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AsuHoQk/k/gRm7IgBg0QAAndFRlRQB7h6CLhOIBggvIDbhDQBzgYB6AAQB8AAByAYQhygVh8AAQh6AAhzAVQBzgVB6AAQB8AAByAVIDbBDIBgAvQCMBOB5B6QFSFRAAHdIgBA0QgRG7lAE/QlRFSmqKNQoPqNlSlSgArMwSQkpEpAAGjQAAGjEpEpQEpEpGjAAQGkAAEokpQEpkpAAmjQAAmjkpkpQkokpmkAAQmjAAkpEpg");
	this.shape_3.setTransform(0,-13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-123.9,-162.9,247.9,326.2);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("AgRDXQgIgDgGgGQgGgGgEgIQgDgJgBgIQABgKADgIQAEgJAGgGQAGgGAIgEQAIgDAJAAQAKAAAIADQAIAEAHAGQAFAGAEAJQADAIAAAKQAAAIgDAJQgEAIgFAGQgHAGgIADQgIAEgKAAQgJAAgIgEgAgXA+QgEgegDgaQgDgZAAgcIAAirIBFAAIAACrQAAAcgDAZQgCAagFAeg");
	this.shape.setTransform(69.4,-10.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("ABOCaQgKAAgEgNIg3iuIgFgSIgEgRIgDASIgFARIg4CuQgEANgMAAIg5AAIhhkzIA7AAQAIABAGADQAFAEACAGIAsClIAGAaIAEAaIAHgaIAIgaIAzimQACgFAGgFQAFgDAHAAIAgAAQAIAAAFADQAGAFABAFIAzCoIAHAaIAGAYIAFgaIAGgaIAuilQABgGAGgEQAGgDAHgBIA4AAIhhEzg");
	this.shape_1.setTransform(34.8,-4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CCFF").s().p("Ag9CUQgcgMgVgUQgTgVgLgdQgMgeAAgkQAAgkAMgdQALgdATgVQAVgUAcgMQAcgLAhAAQAjAAAcALQAcAMAUAUQAUAVAKAdQALAdAAAkQAAAkgLAeQgKAdgUAVQgUAUgcAMQgcALgjAAQghAAgcgLgAg4hKQgTAaAAAwQAAAyATAZQATAbAlAAQAnAAATgaQASgaAAgyQAAgxgSgaQgTgagnAAQglAAgTAbg");
	this.shape_2.setTransform(-5.9,-4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CCFF").s().p("ABmDZIhekjIgEgMIgEgOIgCAOIgEAMIhgEjIhIAAIiHmxIBDAAQALAAAHAFQAHAFACAIIBKEEIAFAVIAEAYIAGgYIAGgVIBVkEQADgGAGgGQAIgGAJAAIAYAAQAKAAAHAFQAGAFAEAIIBUEEQAHAUAEAXIAFgWIAEgVIBLkEQABgHAIgGQAHgFAKAAIBAAAIiHGxg");
	this.shape_3.setTransform(-54.4,-10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-87.9,-49.8,170.2,76);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#660000").ss(3,1,1).p("AiMhPIBGlvIAyAAIA1AAIBaFsAiMhPICpAAIBeAAIA8AAIB6AAIkxIOIgyhZIj+m1g");
	this.shape.setTransform(797.8,33.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFCC00").s().p("AjzgjICkAAICoAAIBfAAIA8AAIjrG2gABZgjgAhPgjIBFlvIAyAAIAxFvg");
	this.shape_1.setTransform(791.7,29);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF99").s().p("AixFmIDqm1IB5AAIkwIOgAhhhPIgylvIA2AAIBaFsIAAADg");
	this.shape_2.setTransform(810.5,33.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(765.8,-12.7,64,92.5);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlhFiQiSiTAAjPQAAjOCSiTQCTiSDOAAQDPAACTCSQCSCTAADOQAADPiSCTQiTCSjPAAQjOAAiTiSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-50,-50,100,100), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhQFaQgYgYAAgiQAAghAYgXQAXgYAhAAQAhAAAXAYQAXAXAAAhQAAAigXAYQgXAXghAAQghAAgXgXgAhLCOIgJgZQgIgVAAgRQAAgeAIgcQAJgcAQgYQARgZAwg2QAxg2AAgkQAAhEhYAAQgsAAgvArIgyhdQA/gyBmAAQBOAAA1AsQA1AsAABIQAAAygTAjQgTAjgxAtQgxAugOAfQgPAeAAAlQAAAIAHAhg");
	this.shape.setTransform(3.9,-6.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(4,1,1).p("A0+MwIAA5fMAp9AAAIAAZfg");
	this.shape_1.setTransform(1.7,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3333").s().p("A0+MwIAA5fMAp9AAAIAAZfg");
	this.shape_2.setTransform(1.7,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,255,255,0.02)").s().p("A10NRIAA6hMArpAAAIAAahg");
	this.shape_3.setTransform(1.6,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-138.1,-84.4,279.5,169.8), null);


(lib.QuestionText_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#663300").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape.setTransform(271.2,35.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#663300").s().p("AgYBLQgPgHgLgKQgMgLgHgPQgHgPAAgRQAAgJADgKQADgKAFgJQAFgJAIgIQAHgHAJgFQAJgGALgDQAKgDALAAQAMAAAKADQALADAJAFQAKAGAHAHQAHAIAFAKIABAAIgXAPIgBgBQgDgIgFgFQgFgGgHgEQgGgDgHgCQgIgCgHgBQgLABgKAEQgJAEgIAHQgHAIgFAKQgEAKAAAKQAAALAEALQAFAJAHAIQAIAHAJAEQAKAEALABIAOgCQAHgCAGgDQAGgEAFgFQAFgFAEgGIAAgCIAYAOIAAABQgFAJgIAIQgIAGgJAFQgJAGgLACQgLADgKAAQgPAAgOgFg");
	this.shape_1.setTransform(254.2,35.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#663300").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_2.setTransform(237.3,35.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#663300").s().p("AgRBoIABgjIABgsIABg6IAbgBIgBAjIAAAeIgBAYIAAATIAAAegAgHhBIgGgEQgDgCgBgEQgCgEAAgEQAAgEACgDQABgEADgDIAGgEQAEgCADAAQAEAAADACQAEACADACIAEAHQACADAAAEQAAAEgCAEIgEAGQgDADgEABQgDACgEAAQgDAAgEgCg");
	this.shape_3.setTransform(225.4,31.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#663300").s().p("AhLhvIAegBIAAAUQAHgIAKgEIAPgHQAJgCAGgBQAKAAAJADQAKADAHAFQAJAEAHAHQAHAHAFAJQAFAIACAKQADAKAAAKQgBAMgDALQgDAKgFAIQgGAJgGAHQgHAHgIAFQgHAFgJACQgJADgHAAIgRgDIgQgGQgJgEgJgIIgBBeIgaABgAgQhTQgHADgGAFQgGAFgEAHQgEAGgDAHIAAAaQACAJAFAHQAEAHAGAEQAGAEAHADQAHACAIAAQAIABAJgEQAIgEAHgFQAFgHAFgJQADgJABgKQABgKgDgJQgEgKgGgHQgGgHgJgEQgIgEgKAAQgIAAgIADg");
	this.shape_4.setTransform(213,38.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#663300").s().p("AgdBzIgOgFIgMgHIgLgLQgFgGgDgJIAagLQADAGAEAEQADAEAEADQAFADAEACQAFACAEAAQAKADAKgCQAKgCAHgEQAHgEAFgEQAFgFADgGIAFgKIADgJIAAgHIAAgSQgIAIgJAEQgJAFgIACIgQACQgQAAgOgFQgOgGgKgKQgKgLgGgOQgGgPABgSQAAgTAFgPQAHgOALgKQALgLANgFQANgFAOAAQAIABAJADIASAHQAJAFAJAJIAAgcIAbABIgBCdIgCASQgDAJgEAJQgEAIgHAHQgGAIgJAFQgIAGgKAEQgLADgMABIgDABQgOAAgNgEgAgUhTQgIAEgHAHQgFAHgEAKQgDAKAAALQAAALADAKQADAJAHAGQAGAHAJAEQAIAEAKAAQAIAAAIgDQAJgEAHgFQAHgFAFgIQAFgIACgKIAAgSQgCgJgFgIQgFgJgHgFQgHgGgJgDQgIgDgJAAQgKAAgIAEg");
	this.shape_5.setTransform(186.1,38.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#663300").s().p("AhAAiIgBgiIgBgaIgBgUIAAgdIAfgBIABAoQAEgIAGgHQAGgHAHgFQAGgGAHgDQAIgDAIgBQALgBAHADQAIACAGAFQAFAEAEAGQADAGACAHIADAMIACAMIAAAvIgBAxIgcgBIACgsQABgWgCgWIAAgGIgBgJIgEgIQgCgFgDgDQgDgDgFgCQgFgBgGAAQgLACgKAOQgMAPgNAaIABAkIABATIAAALIgdAEIgCgrg");
	this.shape_6.setTransform(168.5,35);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#663300").s().p("AgRBoIABgjIABgsIABg6IAbgBIgBAjIAAAeIgBAYIAAATIAAAegAgHhBIgGgEQgDgCgBgEQgCgEAAgEQAAgEACgDQABgEADgDIAGgEQAEgCADAAQAEAAADACQAEACADACIAEAHQACADAAAEQAAAEgCAEIgEAGQgDADgEABQgDACgEAAQgDAAgEgCg");
	this.shape_7.setTransform(156.5,31.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#663300").s().p("AgTBOIgMgDIgNgEIgMgGIgMgIIANgVIAQAIQAHAEAJACIARAFQAHACAKAAQAHAAAGgCIAIgDIAFgFIADgFIABgFIgCgGIgEgFIgHgFIgLgEQgGgBgJAAQgLgBgMgCQgLgCgJgEQgJgFgGgFQgGgHgBgLQgBgKACgIQACgIAFgHQAFgGAGgFQAHgFAIgDQAIgEAJgBQAJgCAIAAIANABIAPACIAQAFQAIADAHAFIgJAZQgJgFgIgCIgPgEIgOgDQgVgBgMAGQgMAGAAALQAAAJAFAEQAEADAIACQAIACALAAIAVACQANADAKAEQAJADAGAFQAFAGADAHQACAGAAAIQAAANgFAJQgFAIgJAGQgJAGgMACQgLADgNABQgLgBgNgCg");
	this.shape_8.setTransform(144.9,35.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#663300").s().p("AgTBOIgMgDIgNgEIgMgGIgMgIIANgVIAQAIQAHAEAJACIARAFQAHACAKAAQAHAAAGgCIAIgDIAFgFIADgFIABgFIgCgGIgEgFIgHgFIgLgEQgGgBgJAAQgLgBgMgCQgLgCgJgEQgJgFgGgFQgGgHgBgLQgBgKACgIQACgIAFgHQAFgGAGgFQAHgFAIgDQAIgEAJgBQAJgCAIAAIANABIAPACIAQAFQAIADAHAFIgJAZQgJgFgIgCIgPgEIgOgDQgVgBgMAGQgMAGAAALQAAAJAFAEQAEADAIACQAIACALAAIAVACQANADAKAEQAJADAGAFQAFAGADAHQACAGAAAIQAAANgFAJQgFAIgJAGQgJAGgMACQgLADgNABQgLgBgNgCg");
	this.shape_9.setTransform(129.2,35.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#663300").s().p("AgRBoIABgjIABgsIABg6IAbgBIgBAjIAAAeIgBAYIAAATIAAAegAgHhBIgGgEQgDgCgBgEQgCgEAAgEQAAgEACgDQABgEADgDIAGgEQAEgCADAAQAEAAADACQAEACADACIAEAHQACADAAAEQAAAEgCAEIgEAGQgDADgEABQgDACgEAAQgDAAgEgCg");
	this.shape_10.setTransform(118.1,31.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#663300").s().p("ABCAhIAAgeIAAgVQgBgMgDgFQgDgGgGAAQgDAAgDACQgEACgEAEIgGAHIgHAJIgGAJIgFAHIABAMIABARIAAAUIAAAaIgaACIAAgrIgBgeIgCgVQAAgMgDgFQgEgGgFAAQgEAAgDADIgIAGIgHAIIgHAKIgGAJIgFAHIABBIIgcACIgDiTIAdgEIAAAoIAJgLIALgLQAGgEAHgDQAGgEAIAAQAGAAAFACQAGACADAEQAFAEADAGQADAGABAIIAJgLIAKgKQAGgFAGgDQAGgDAIAAQAGAAAGACQAGADAFAEQAEAEADAHQADAHAAAJIACAaIABAgIAAAwIgdACIAAgrg");
	this.shape_11.setTransform(102.9,34.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#663300").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_12.setTransform(75.8,35.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#663300").s().p("AAlBpQAEgNACgNIACgWIABgWQgBgPgEgKQgFgKgGgGQgHgHgHgDQgIgDgIAAQgGABgIAEQgHADgIAHQgIAGgHANIgBBbIgbABIgCjUIAggBIgBBUQAHgIAJgFIAQgHQAJgDAHgBQAPAAANAFQANAGAJAKQAJALAFAOQAGAOAAATIAAAUIgBAUIgCASIgDAQg");
	this.shape_13.setTransform(58.2,31.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#663300").s().p("AgQgPIgwABIABgaIAwgBIABg9IAagBIgBA9IA2gBIgCAaIg0ABIgBB3IgcABg");
	this.shape_14.setTransform(42.1,32.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#663300").s().p("AgQgPIgwABIABgaIAwgBIABg9IAagBIgBA9IA2gBIgCAaIg0ABIgBB3IgcABg");
	this.shape_15.setTransform(21.3,32.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#663300").s().p("AgYBLQgPgHgLgKQgMgLgHgPQgHgPAAgRQAAgJADgKQADgKAFgJQAFgJAIgIQAHgHAJgFQAJgGALgDQAKgDALAAQAMAAAKADQALADAJAFQAKAGAHAHQAHAIAFAKIABAAIgXAPIgBgBQgDgIgFgFQgFgGgHgEQgGgDgHgCQgIgCgHgBQgLABgKAEQgJAEgIAHQgHAIgFAKQgEAKAAAKQAAALAEALQAFAJAHAIQAIAHAJAEQAKAEALABIAOgCQAHgCAGgDQAGgEAFgFQAFgFAEgGIAAgCIAYAOIAAABQgFAJgIAIQgIAGgJAFQgJAGgLACQgLADgKAAQgPAAgOgFg");
	this.shape_16.setTransform(6,35.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#663300").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_17.setTransform(-10.9,35.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#663300").s().p("AAJB4QgIgDgFgFQgFgGgEgGQgEgGgDgHQgHgQgCgUIAFitIAbAAIgBAtIgBAlIgBAeIAAAWIgBAmQABANADAJIADAJIAGAHIAIAGQAEACAGAAIgEAbQgJAAgIgDg");
	this.shape_18.setTransform(-22.5,30.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#663300").s().p("AgeBKQgOgFgKgKQgKgLgFgOQgFgOAAgRQAAgRAGgPQAGgPAKgLQAKgLAOgGQAOgHAPABQAOgBANAGQANAFAJALQAKAKAHANQAGANACAQIh3ARQABAKAEAHQAEAIAGAFQAGAGAIACQAIADAIAAQAHAAAHgCQAIgCAGgFQAGgEAEgHQAFgGACgIIAaAFQgDANgHAKQgIAKgJAHQgJAIgLAEQgMADgMAAQgRABgOgGgAgLg0QgHABgGAFQgHAFgFAIQgFAIgCAMIBSgKIgBgCQgFgPgJgHQgJgIgOAAQgFAAgHADg");
	this.shape_19.setTransform(-35.6,35.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#663300").s().p("AgRBvQgLgCgKgEQgKgEgJgGIgUgMIAUgYQALAJALAGQAKAFAJADQAKADAIABQAJABAJgCQAJgCAGgEQAGgEAEgFQAEgGABgGQAAgFgCgEQgCgFgEgDQgFgEgGgCIgMgFIgNgEIgNgDIgQgDQgJgCgJgEQgIgEgHgFQgIgFgFgIQgGgIgDgKQgCgKABgOQAAgLAEgIQAEgJAGgHQAGgIAIgEQAIgGAIgDQAJgDAKgCQAJgBAJAAQAMABAOADIALAEIANAFIAMAHIALAJIgPAYIgJgIIgKgHIgKgEIgKgEQgKgDgLAAQgLAAgKAEQgKAEgGAGQgHAGgEAHQgDAHAAAIQAAAHAEAGQAEAHAIAFQAHAGAKADQAKAEALACIAUADIATAGQAJAEAIAFQAHAFAGAIQAFAHACAJQADAIgCALQgBAKgEAHQgEAIgGAFQgGAGgIAEQgHAEgJACQgIACgIABIgQABQgKAAgKgCg");
	this.shape_20.setTransform(-53.2,32.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#CC9900").ss(3,1,1).p("A8LjMMA4XAAAQB3AAAAB7IAACjQAAB7h3AAMg4XAAAQh3AAAAh7IAAijQAAh7B3AAg");
	this.shape_21.setTransform(109.2,35);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFCC00").s().p("A8LDNQh3AAAAh7IAAijQAAh7B3AAMA4XAAAQB3AAAAB7IAACjQAAB7h3AAg");
	this.shape_22.setTransform(109.2,35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.QuestionText_mc, new cjs.Rectangle(-84.5,8.7,387.6,48.3), null);


(lib.questioncopy8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#171616").s().p("ApyA0QhkAAAAhkIAAgCIAAgBIWtAAIAABng");
	this.shape.setTransform(-73.8,109.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#726E6E").s().p("AodB6IDWjzINlAAIAAASItaAAIgKAMIgbAeIgaAeIgaAeIgaAdIgaAdIgbAfIgeAig");
	this.shape_1.setTransform(-55.4,65);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("ArWEKIHJoOIPhAAIAAgFIADAAIAAAQItmAAIjWD0IAbAAIQhAAIAAAvIkiAAIhJCyIFrAAIAAAug");
	this.shape_2.setTransform(-73.8,77.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#545151").s().p("AoQBrIAegiIQDAAIAAAigAnXAqIAbgdIPNAAIAAAdgAmigQIAageIOZAAIAAAegAluhMIAageINlAAIAAAeg");
	this.shape_3.setTransform(-54,66.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7C7B7B").s().p("ACXDiIBJizIEiAAIAACzgAoBghIAbgfIPoAAIAAAfgAnLheIAageIOzAAIAAAegAmXiZIAagfIN/AAIAAAfgAljjWIAKgLINbAAIAAALg");
	this.shape_4.setTransform(-52.5,77.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AsmHdIAAu5IZNAAIAAO5g");
	this.shape_5.setTransform(-81.9,99.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy8, new cjs.Rectangle(-162.6,51.2,161.5,96.2), null);


(lib.questioncopy7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("As0n0IZpAAIAAPpI5pAAg");
	this.shape.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#988F8F").s().p("Ah2AaQgNAAgKgHQgHgHgBgJIAAgDQAAgKAIgHQAKgIANAAIDsAAQAOAAAJAIQAJAHAAAKIAAADQgBAJgIAHQgJAHgOAAg");
	this.shape_1.setTransform(-85.9,-54.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AoQAVIAAgmIJwAAQAAAJAIAHQAJAGANAAIDuAAQANAAAJgGQAIgHABgJIBHAAQASAAAQgDIAdApg");
	this.shape_2.setTransform(-110.4,-52.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#4088B8").s().p("AoCAYIAAguIQFAAIAAAug");
	this.shape_3.setTransform(-111.8,-68.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0C0E05").s().p("AF4HjIAAgDQAAgLgJgHQgJgJgNABIjuAAQgNgBgJAJQgJAHAAALIABADIpwAAIAAh2IQFAAIAAgvIAAsgIBCAAIAANhQAABRhDAQQgQADgSAAg");
	this.shape_4.setTransform(-108.5,-102.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#62AEE0").s().p("AoBGQIAAg4IgDAAIAAmJIABAAQCmggBTgaQDzhKDGibQAmgeAlghIEKAAIAAMfg");
	this.shape_5.setTransform(-111.9,-111);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#71BCEE").s().p("Al/CvIAAldIL+AAQglAhgmAeQjHCajyBKQhTAaimAgg");
	this.shape_6.setTransform(-125.2,-133.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_9
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("As0H1IAAvoIZpAAIAAPog");
	this.shape_7.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer_8
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AtVIJIAAwRIarAAIAAQRg");
	this.shape_8.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy7, new cjs.Rectangle(-167.5,-152.2,170.8,104.2), null);


(lib.questioncopy6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("As0n0IZpAAIAAPpI5pAAg");
	this.shape.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#988F8F").s().p("Ah1AaQgOAAgKgIQgIgHAAgLIAAgCQABgJAHgHQAKgHAOAAIDrAAQAOAAAJAHQAIAHABAJIAAACQAAALgJAHQgJAIgOAAg");
	this.shape_1.setTransform(-72.4,-52.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AnxATIAgglIAPABIBIAAIAAACQAAALAIAHQAKAIANAAIDtAAQAOAAAIgIQAKgHgBgLIAAgCIJBAAIAAAfIgCAAIAAAFg");
	this.shape_2.setTransform(-49.4,-50.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#62AEE0").s().p("AnMDgIAAgbQAdgtAogrQBkhuCLhEQAtgWAxgSQCCgvD7gsICFgXIACgBIAAGIIACAAIAAA5g");
	this.shape_3.setTransform(-45.6,-91.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0C0E05").s().p("Ag0HjQgBgJgIgHQgJgHgOAAIjsAAQgOAAgKAHQgHAHgBAJIhHAAIgPgBQgOgBgMgDQg8gTAAhMIAAthIBRAAIAANOIPIAAIAAB3g");
	this.shape_4.setTransform(-52.1,-101);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#4088B8").s().p("AnjGoIAAtPIAvAAIAAMEIAAAbIOYABIAAAvg");
	this.shape_5.setTransform(-48,-107);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#71BCEE").s().p("AnKmBIOVAAIAAFeIgCABIiFAXQj7AriCAvQgxASgsAWQiMBEhkBvQgoArgcAtg");
	this.shape_6.setTransform(-45.8,-110.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_9
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("As0H1IAAvoIZpAAIAAPog");
	this.shape_7.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

	// Layer_8
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(255,255,255,0.02)").s().p("AtVIJIAAwRIarAAIAAQRg");
	this.shape_8.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy6, new cjs.Rectangle(-167.5,-152.2,170.8,104.2), null);


(lib.questioncopy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("As0n0IZpAAIAAPpI5pAAg");
	this.shape.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4088B8").s().p("AgXC5IAAlxIAvAAIAAFxg");
	this.shape_1.setTransform(-94.1,-68.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0C0E05").s().p("AoLD9IAAmVQAAhkBlAAIOyAAIAACIIuWAAIgwAAIAAFxg");
	this.shape_2.setTransform(-52.2,-75.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#71BCEE").s().p("AnLC5IAAlxIOWAAIAAFxg");
	this.shape_3.setTransform(-45.7,-68.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_9
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("As0H1IAAvoIZpAAIAAPog");
	this.shape_4.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

	// Layer_8
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,255,255,0.02)").s().p("AtVIJIAAwRIarAAIAAQRg");
	this.shape_5.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy5, new cjs.Rectangle(-167.5,-152.2,170.8,104.2), null);


(lib.questioncopy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(4,1,1).p("As0n0IZpAAIAAPpI5pAAg");
	this.shape.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#171616").s().p("ApyA0QhkAAAAhkIAAgDIAAAAIWtAAIAABng");
	this.shape_1.setTransform(-72.3,-91.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#545151").s().p("AoQBrIAegiIQDAAIAAAigAnXAqIAbgdIPNAAIAAAdgAmigQIAageIOZAAIAAAegAluhMIAageINlAAIAAAeg");
	this.shape_2.setTransform(-52.5,-134.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#726E6E").s().p("AodB6IDWjzINlAAIAAASItaAAIgKAMIgbAeIgaAeIgaAeIgaAdIgaAdIgbAfIgeAig");
	this.shape_3.setTransform(-53.8,-136.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7C7B7B").s().p("ACXDiIBJiyIEiAAIAACygAoBghIAbgfIPoAAIAAAfgAnLhdIAageIOzAAIAAAegAmXiaIAagdIN/AAIAAAdgAljjVIAKgMINbAAIAAAMg");
	this.shape_4.setTransform(-51,-123.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("ArWEKIHJoOIPhAAIAAgFIADAAIAAAQItmAAIjWD1IAbAAIQhAAIAAAuIkiAAIhJCyIFrAAIAAAug");
	this.shape_5.setTransform(-72.3,-123.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_9
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("As0H1IAAvoIZpAAIAAPog");
	this.shape_6.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(1));

	// Layer_8
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,255,255,0.02)").s().p("AtVIJIAAwRIarAAIAAQRg");
	this.shape_7.setTransform(-82.1,-100.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy4, new cjs.Rectangle(-167.5,-152.2,170.8,104.2), null);


(lib.questioncopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#330000").ss(2.5,1,1).p("A5lAAMAzLAAA");
	this.shape.setTransform(-0.4,50.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#330000").ss(2.5,1,1).p("A5lAAMAzLAAA");
	this.shape_1.setTransform(-0.4,-50);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#330000").ss(2.5,1,1).p("AAA3dMAAAAu7");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#330000").ss(2.5,1,1).p("A5l3dMAzLAAAMAAAAu7MgzLAAAg");
	this.shape_3.setTransform(-0.4,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 7
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#171616").s().p("ArHA0IAAhnIWOAAIABAAIAAADQAABkhkAAg");
	this.shape_4.setTransform(71.8,107.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("ArGEGIAAgtIF0AAIg/izIk1AAIAAguIQpAAIAWAAIi8j0IuDAAIAAgJIQfAAIFuILg");
	this.shape_5.setTransform(71.7,76.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#545151").s().p("AoUBrIAAgiIQPAAIAaAigAoUAqIAAgdIPgAAIAXAdgAoUgQIAAgeIOyAAIAXAegAoUhMIAAgeIOEAAIAXAeg");
	this.shape_6.setTransform(53.9,64.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#726E6E").s().p("AIKB6IgagiIgYgfIgXgdIgXgdIgXgeIgXgeIgXgeIgJgMIt7AAIAAgSIODAAIC8Dzg");
	this.shape_7.setTransform(55,63.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#7C7B7B").s().p("AoHDiIAAizIE1AAIA/CzgAoHghIAAgfIP3AAIAYAfgAoHheIAAgeIPJAAIAXAegAoHiZIAAgfIObAAIAXAfgAoHjWIAAgLIN7AAIAJALg");
	this.shape_8.setTransform(52.6,75.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#4088B8").s().p("AoCAYIAAgvIQFABIAAAug");
	this.shape_9.setTransform(51.9,32.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#333333").s().p("AoQAVIAAgmIJwAAQAAAJAIAHQAJAGANAAIDuAAQANAAAJgGQAIgHABgJIBHAAQASAAAQgDIAdApg");
	this.shape_10.setTransform(53.3,48.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#988F8F").s().p("Ah2AaQgNAAgJgHQgIgHAAgJIgBgDQAAgKAJgHQAJgIANAAIDtAAQANAAAJAIQAJAHAAAKIAAADQgCAJgHAHQgJAHgNAAg");
	this.shape_11.setTransform(77.9,46);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0C0E05").s().p("AF4HjIAAgDQAAgLgJgHQgJgIgNAAIjuAAQgNAAgJAIQgJAHAAALIABADIpwAAIAAh2IQFAAIAAgvIAAsgIBCAAIAANhQAABRhDAQQgQADgSAAg");
	this.shape_12.setTransform(55.2,-2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#62AEE0").s().p("AoBGQIAAg4IgCAAIAAmJIAAAAQCmggBTgaQDzhKDGibQAmgeAlghIEKAAIAAMfg");
	this.shape_13.setTransform(51.8,-10.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#71BCEE").s().p("Al+CvIAAldIL9AAQgkAhgnAeQjHCajxBKQhUAaimAgIAAAAIAAAAg");
	this.shape_14.setTransform(38.5,-32.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0C0E05").s().p("AHjD9IAAk3IAAg6IwHAAIAAiIIPkAAQBcAAAIBTIABARIAAGVg");
	this.shape_15.setTransform(55.1,-75.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#62AEE0").s().p("AiECcQBIhBBAhLQBIhSA5hZIAAE3g");
	this.shape_16.setTransform(90.1,-65.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#71BCEE").s().p("AoDC5IAAlxIQHAAIAAA6Qg5BZhIBSQhBBLhIBBg");
	this.shape_17.setTransform(51.8,-68.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#171616").s().p("ApyA0QhkAAAAhkIAAgCIAAgBIWtAAIAABng");
	this.shape_18.setTransform(-72.3,107.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#333333").s().p("ArWEKIHJoOIPhAAIAAgFIADAAIAAAQItmAAIjWD0IAbAAIQhAAIAAAvIkiAAIhJCzIFrAAIAAAtg");
	this.shape_19.setTransform(-72.3,76.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#726E6E").s().p("AodB6IDWjzINlAAIAAASItaAAIgKAMIgbAeIgaAeIgaAeIgaAdIgaAdIgbAfIgeAig");
	this.shape_20.setTransform(-53.8,63.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#545151").s().p("AoQBrIAegiIQDAAIAAAigAnXAqIAbgdIPNAAIAAAdgAmigQIAageIOZAAIAAAegAluhMIAageINlAAIAAAeg");
	this.shape_21.setTransform(-52.5,64.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#7C7B7B").s().p("ACXDiIBJizIEiAAIAACzgAoBghIAbgfIPoAAIAAAfgAnLheIAageIOzAAIAAAegAmXiZIAagfIN/AAIAAAfgAljjWIAKgLINbAAIAAALg");
	this.shape_22.setTransform(-51,75.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#988F8F").s().p("Ah1AaQgOAAgKgIQgIgHAAgLIAAgCQABgJAHgHQAKgHAOAAIDrAAQAOAAAJAHQAIAHABAJIAAACQAAALgJAHQgJAIgOAAg");
	this.shape_23.setTransform(-72.4,46.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#333333").s().p("AnxATIAgglIAPABIBIAAIAAACQAAALAIAHQAKAIANAAIDtAAQAOAAAIgIQAKgHgBgLIAAgCIJBAAIAAAfIgCAAIAAAFg");
	this.shape_24.setTransform(-49.4,48.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#62AEE0").s().p("AnMDhIAAgbQAdguAogrQBkhtCLhFQAtgWAxgRQCCgwD7grICFgYIACgBIAAGJIACAAIAAA4g");
	this.shape_25.setTransform(-45.6,7.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#0C0E05").s().p("Ag0HjQgBgJgIgHQgJgIgOAAIjsAAQgOAAgKAIQgHAHgBAJIhHAAIgPAAQgOgCgMgEQg8gRAAhNIAAthIBRAAIAANPIPIAAIAAB2g");
	this.shape_26.setTransform(-52.1,-2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#4088B8").s().p("AnjGoIAAtPIAvAAIAAMEIAAAbIOYABIAAAvg");
	this.shape_27.setTransform(-48,-7.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#71BCEE").s().p("AnKmCIOVAAIAAFfIgCAAIiFAZQj7AqiCAwQgxARgsAWQiMBFhkBtQgoAsgcAug");
	this.shape_28.setTransform(-45.8,-11.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#4088B8").s().p("AgXC5IAAlxIAvAAIAAFxg");
	this.shape_29.setTransform(-94.1,-68.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#0C0E05").s().p("AoLD9IAAmVQAAhkBlAAIOyAAIAACIIuWAAIgwAAIAAFxg");
	this.shape_30.setTransform(-52.2,-75.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#71BCEE").s().p("AnLC5IAAlxIOWAAIAAFxg");
	this.shape_31.setTransform(-45.7,-68.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#171616").s().p("AAAA0IAAhnIABAAIAABng");
	this.shape_32.setTransform(0.5,107.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#545151").s().p("AAABrIAAgiIABAAIAAAigAAAAqIAAgdIABAAIAAAdgAAAgQIAAgeIABAAIAAAegAAAhMIAAgeIABAAIAAAeg");
	this.shape_33.setTransform(0.5,64.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#7C7B7B").s().p("AAADiIAAizIABAAIAACzgAAAghIAAgfIABAAIAAAfgAAAheIAAgeIABAAIAAAegAAAiZIAAgfIABAAIAAAfgAAAjWIAAgLIABAAIAAALg");
	this.shape_34.setTransform(0.5,75.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#726E6E").s().p("AAAAJIAAgRIABAAIAAARg");
	this.shape_35.setTransform(0.5,52);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#333333").s().p("AAAEGIAAgtIABAAIAAAtgAAAAmIAAguIABAAIAAAugAAAj8IAAgJIABAAIAAAJg");
	this.shape_36.setTransform(0.5,76.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]}).wait(1));

	// Layer 4
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("rgba(255,255,255,0.047)").s().p("A6nYaMAAAgwzMA1PAAAMAAAAwzg");
	this.shape_37.setTransform(-0.4,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(1));

	// Layer 2
	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#993300").ss(5,1,1).p("A6A3+MA0BAAAMAAAAv9Mg0BAAAg");
	this.shape_38.setTransform(-0.4,0.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("A6AX/MAAAgv9MA0BAAAMAAAAv9g");
	this.shape_39.setTransform(-0.4,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38}]}).wait(1));

	// Layer 5
	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("rgba(255,255,255,0.008)").s().p("A7aZKMAAAgyTMA20AAAMAAAAyTg");
	this.shape_40.setTransform(0.7,-0.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_40).wait(1));

}).prototype = getMCSymbolPrototype(lib.questioncopy2, new cjs.Rectangle(-174.7,-161.5,350.9,321.9), null);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.questioncopy7();
	this.instance.parent = this;
	this.instance.setTransform(484,4.1,1.528,1.528,0,0,0,-78.3,-97.5);

	this.instance_1 = new lib.questioncopy6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(165.2,4.1,1.528,1.528,0,0,0,-78.3,-97.5);

	this.instance_2 = new lib.questioncopy5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-153.4,4.1,1.528,1.528,0,0,0,-78.2,-97.5);

	this.instance_3 = new lib.questioncopy4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-472.2,4.1,1.528,1.528,0,0,0,-78.2,-97.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-608.6,-79.5,1217.3,159.1);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,51,102,0.298)").ss(5,1,1).p("AgbCFQgJgDgJgEQgzgYgUg1QgTg0AYgyQAYg0A1gTQA0gUAzAZQAkAQAUAg");
	this.shape.setTransform(-30.2,113);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("rgba(0,51,102,0.298)").ss(3,1,1).p("AghDKQgUgFgTgJQhMgkgdhQQgdhOAkhNQAkhMBPgdQANgEANgDQAsgLArALQAVAFAUAJQA9AcAfA3");
	this.shape_1.setTransform(-30.4,113);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#CC6600").ss(3,1,1).p("AgGj/QAGgGAIgFQAJgEAKgFQBGgdBXAMQBYAOBxEkQhJCRhHATQhYAWgqADQgQABgUAJQgUAJgtAiQgrAigcAfQgbAegOgJQgsggACg6QABgyAegsQAfgsAngNQAMgFAagIQANgEANgEQhahIhbglQgYgKgVgOQgOgIgNgKQgcgUgagaQgTgVgvg7Qgvg9AXg4QAYg4BIA0QBIAzA1AzQACACADACQAUATAUAOQAJAGAJAGQADABADADQAYAOAaAJQA6AVALABAgGj/QA6gPBLA6AhXiwQAHgMAKgMQAQgUAVgQQAMgKAPgIIAAgB");
	this.shape_2.setTransform(4.5,141.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFCC99").s().p("AhmFWQgsggACg6QABgyAegsQAfgsAngNQAMgFAagIIAagIQhahIhbglQgYgKgVgOQgOgIgNgKQgcgUgagaQgTgVgvg7Qgvg9AXg4QAYg4BIA0QBIAzA1AzIAFAEQAUATAUAOIASAMIAGAEQAYAOAaAJQA6AVALABQgLgBg6gVQgagJgYgOIgGgEIAFgJQAHgMAKgMQAQgUAVgQQAMgKAPgIIAAgBIAOgLIATgJQBGgdBXAMQBYAOBxEkQhJCRhHATQhYAWgqADQgQABgUAJQgUAJgtAiQgrAigcAfQgVAXgNAAQgEAAgDgCgAB/jUIgBgBIgCgCIAAAAIgCgBQg5gpgugBIgBAAIAAAAQgNAAgLADQALgDANAAIAAAAIABAAQAuABA5ApIACABIAAAAIACACIABABIAAAAg");
	this.shape_3.setTransform(4.5,141.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-11.3,122.3,1.2,1.2);
	this.instance.alpha = 0.852;
	this.instance.filters = [new cjs.BlurFilter(53, 53, 1)];
	this.instance.cache(-52,-52,104,104);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-98.3,35.3,177,178);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Tween9("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(6.6,-8.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.95,scaleY:0.95,x:6.4,y:25.6},15).to({scaleX:1,scaleY:1,x:6.6,y:-8.1},19).wait(1));

	// Layer_3
	this.instance_1 = new lib.Tween10("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,35.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.95,scaleY:0.95,y:67.3},15).to({scaleX:1,scaleY:1,y:35.9},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122,-127.1,247.9,326.2);


// stage content:
(lib.GameIntro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Wow
	this.instance = new lib.Symbol6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(637.9,346.4,1.111,1.111);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(146).to({_off:false},0).wait(59));

	// hand
	this.instance_1 = new lib.Tween8("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(263.2,503.2);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(95).to({_off:false},0).to({alpha:1},8).to({scaleX:1.2,scaleY:1.2},11).to({scaleX:1,scaleY:1},8).to({startPosition:0},7).to({_off:true},17).wait(59));

	// Layer_5
	this.instance_2 = new lib.questioncopy8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(649.4,330.3,1.411,1.411,0,0,0,0.2,0.1);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(123).to({_off:false},0).to({regX:0.1,scaleX:1.18,scaleY:1.18,x:639.3,y:344.3},6).to({_off:true},17).wait(59));

	// arrow
	this.instance_3 = new lib.Tween7("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(161.4,456.5,1,1,0,0,0,798.2,36.1);
	this.instance_3.alpha = 0.602;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(69).to({_off:false},0).to({y:504.5,alpha:1},9).to({y:456.5},11).to({y:504.5,alpha:0.699},9).to({_off:true},1).wait(106));

	// Layer_2
	this.instance_4 = new lib.Tween15("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(640,623.8);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(46).to({_off:false},0).to({alpha:1},3).to({_off:true},97).wait(59));

	// Layer_3
	this.instance_5 = new lib.Symbol1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(542.8,461.1,0.708,0.706,0,0,0,0.3,0.3);
	this.instance_5.alpha = 0.109;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(21).to({_off:false},0).to({alpha:1},1).to({_off:true},124).wait(59));

	// Layer_1
	this.instance_6 = new lib.questioncopy2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(639.3,344.3,1.176,1.176,0,0,0,0.1,0.1);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(21).to({_off:false},0).to({alpha:1},1).to({_off:true},124).wait(59));

	// Layer_4
	this.questxt = new lib.QuestionText_mc();
	this.questxt.name = "questxt";
	this.questxt.parent = this;
	this.questxt.setTransform(640.1,67.8,1.8,1.8,0,0,0,109.2,11.4);
	this.questxt.alpha = 0;
	this.questxt._off = true;

	this.timeline.addTween(cjs.Tween.get(this.questxt).wait(5).to({_off:false},0).to({alpha:1},4).to({_off:true},137).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(568.2,280.3,1418,864);
// library properties:
lib.properties = {
	id: 'D044BEAA30B3F146B78DA97BB48504C8',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D044BEAA30B3F146B78DA97BB48504C8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;