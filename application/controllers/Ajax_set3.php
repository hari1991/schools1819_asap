<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_set3 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Assessment_model_set3');
				$this->load->library('session');			
				//$this->lang->load("menu_lang","french");		
			
        }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	
	public function ajaxstarsupdate()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		$data['SetDetails']=$this->Assessment_model_set3->getSetDetails($this->session->game_plan_id,$this->session->game_grade);
		/*
		$data['randomGames']=$this->Assessment_model_set3->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		
		$data['assignGames']=$this->Assessment_model_set3->getAssignGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid);
	
		
		$cur_day_skills = count($data['randomGames']);
		$assign_count = count($data['assignGames']);
		if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) {
				$this->fn_Rand_games($userid, $check_date_time, $cur_day_skills, $assign_count,$catid);
			}
		*/
		$data['randomGames']=$this->Assessment_model_set3->getRandomGames($check_date_time,$data['SetDetails'][0]['planid'],$data['SetDetails'][0]['gradeid'],$this->session->school_id);
		$where = "";
		foreach($data['randomGames'] as $brainData )
		{
			$brainIds[] = $brainData['gid'];
			$active_game_ids = @implode(',',$brainIds);
			$where = " and g.gid in ($active_game_ids)";
		}
			 
		$data['actualGames'] = $this->Assessment_model_set3->getActualGames($data['SetDetails'][0]['planid'],$data['SetDetails'][0]['gradeid'],$userid,$catid,$where);
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
 
	 $restrainCalendar= $this->Assessment_model_set3->getTrainCalendar($userid,date('Y-m-01'),date('Y-m-d'),$catid);
	 $data['arrtrainCalendar']=explode(',',$restrainCalendar[0]['updateDates']);
	 $data['trophies']=array();
	 $data['trophies']= $this->Assessment_model_set3->getMyCurrentTrophies($userid);

	/* echo "<pre>";
	 print_r($data['arrtrainCalendar'] );
	 exit;*/
		//$data['query'] = $this->Assessment_model_set3->getleftbardata($userid);
		
		//$data['mytrophy'] = $this->Assessment_model_set3->gettrophy($userid);
		//$this->in_array_r();
		 
		 
	 $data['controllername'] = "mypuzzleset3";
	 
	 $data['setname'] = "Reports";
	 $data['controllername2'] = "reports";
		 
	
	$this->load->view('home/dashboard_ajax', $data);
	
	 
	 
}


public function bspiajax()
{
	$userid = $this->session->user_id;
	
	$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	 $data['bspi'] = $tot;
	
	echo round($data['bspi'],2);
	 
	// $this->load->view('headerinner', $data);
}

public function gamecompletepopup()
	{
		$userid = $_REQUEST['userid'];
		$currentdate = $_REQUEST['currentdate'];
		
		$data['set1gamecount'] = $this->Assessment_model_set3->menudisable3($userid,$currentdate);
		
		echo $data['set1gamecount'][0]['countid'];
		
	}

}
?>