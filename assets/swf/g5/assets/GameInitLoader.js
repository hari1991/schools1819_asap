//////////////////////////////////////////////////////////////////======LOADER=======///////////////////////////////////////////////////////////////////////
var wrongSnd, gameOverSnd, timeOverSnd, correctSnd, BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var backGround0,getHowToPlayMc,clearHowToPlayInterval;

function createLoader() {

    loaderColor = createjs.Graphics.getRGB(255, 51, 51, 1);
    loaderBar = new createjs.Container();
    var txt = new createjs.Container();
    bar = new createjs.Shape();
    bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();
    loaderWidth = 600;

    //
    loadProgressLabel = new createjs.Text("", "30px lato-bold", "black");
    loadProgressLabel.lineWidth = 400;
    loadProgressLabel.textAlign = "center";
    txt.addChild(loadProgressLabel)
    txt.x = 260;
    txt.y = 35;

    //
    var bgBar = new createjs.Shape();
    var padding = 3
    bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);
    loaderBar.x = 1300 - loaderWidth >> 1;
    loaderBar.y = 720 - barHeight >> 1;
    loaderBar.addChild(bar, bgBar, txt);
    stage.addChild(loaderBar);

   
    
}
//////////////////////////////////////////////////////////////////======DEFAULT MANIFEST ASSETS=======////////////////////////////////////////////////////////
function createManifest() {

    /* Always specify the following terms as given  
         1. redirecturl.json path as "redirectJsonPath"
         2. Intro text image name as "IntroScreen.png""
    */

    manifest = [
        //parrot images
        { id: "parrot", src: assetsPath + "ParrotAnimationTesting.js" },
        { id: "howToPlay", src: assetsPath + "HowToPlay.js" },

        //audio
        
        { id: "correct", src: assetsPath + "wow_s.ogg" },
        { id: "wrong", src: assetsPath + "oops_s.ogg" },
        { id: "gameOver", src: assetsPath + "Game_over.ogg" },
        { id: "timeOver", src: assetsPath + "timeover_s.ogg" },
        //result
        //{ id: "resultLoading", src: assetsPath + "ResultLoading.png" },
        { id: "resultLoading", src: assetsPath + "ResultScreen.js" },
        { id: "domainPath", src: redirectJsonPath + "redirecturl.json" },

        //others
        { id: "startBtn", src: assetsPath + "StartButton.png" },
        
        { id: "hPanel", src: assetsPath + "helpPanel.js" },
        { id: "qPanel", src: assetsPath + "quickPanel.js" },

        //common game assets
        { id: "backGround", src: gameAssetsPath + "Background.png" },
        { id: "IntroStartBtn", src: assetsPath + "IntroStartBtn.png" }
    ];

    return 1;
}
//////////////////////////////////////////////////////////////////======PRELOADING OF ASSETS=======///////////////////////////////////////////////////////////
function preloadAllAssets() {
    createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin]);

    if (!createjs.Sound.initializeDefaultPlugins()) { return; }

    createjs.Sound.alternateExtensions = ["mp3"];

    createjs.WebAudioPlugin.playEmptySound();

    preload = new createjs.LoadQueue(true);

    preload.installPlugin(createjs.Sound);

    preload.addEventListener("complete", doneLoading); // add an event listener for when load is completed

    preload.addEventListener("fileload", fileLoaded);

    preload.addEventListener("progress", updateLoading);

    preload.loadManifest(manifest);

    stage.update();

}

function stop() {
    if (preload != null) {
        preload.close();
    }
    createjs.Sound.stop();
}

function updateLoading(event) {
    bar.scaleX = event.loaded * loaderWidth;

    progresPrecentage = Math.round(event.loaded * 100);
    loadProgressLabel.text = "              " + progresPrecentage + "% Game Loading...";
    stage.update();
}

function fileLoaded(e) {
    assets.push(e);
}



function doneLoading(event) {
    loaderBar.visible = false;
    stage.update();
 
    for (i = 0; i < assets.length; i++) {
        
        if (i < 13) {
            var event = assets[i];
            var id = event.item.id;
            console.log("test === id === "+id +" == "+i)
            if (id == "backGround") {
                bg = new createjs.Bitmap(preload.getResult('backGround'));
                container.parent.addChild(bg);
                bg.visible = true;
                continue;
            }
            if (id == "introScreen") {
                introScrn = new createjs.Bitmap(preload.getResult('introScreen'));
                container.parent.addChild(introScrn);
                introScrn.visible = false;
                continue;
            }

             
            if (id == "startBtn") {
                startBtn = new createjs.Bitmap(preload.getResult('startBtn'));
                container.parent.addChild(startBtn);
                startBtn.x = 520; startBtn.y = 450;
                //startBtn.scaleX = startBtn.scaleY = .6;
                startBtn.visible = false
                continue;
            }
            if (id == "IntroStartBtn") {
                introStartBtn = new createjs.Bitmap(preload.getResult('IntroStartBtn'));
                container.parent.addChild(introStartBtn);
                introStartBtn.visible = false;
                continue;
            }


            if (id == "parrot") {
                var comp1 = AdobeAn.getComposition("E02783C89B02E54D9400B73DF3C4FEBA");
                var lib2 = comp1.getLibrary();
                ParrotAnimationTesting = new lib2.ParrotAnimationTesting()
                container.parent.addChild(ParrotAnimationTesting);
                ParrotAnimationTesting.visible = false;
                continue;
            };

             if (id == "howToPlay") {
                var  howToPlay1 = AdobeAn.getComposition("509C8CAA7D4C684A8790B1A978774CA5");
                var howToPlayLib = howToPlay1.getLibrary();
                getHowToPlayMc = new howToPlayLib.HowToPlay()
                container.parent.addChild(getHowToPlayMc);
                getHowToPlayMc.visible = false;
                continue;
            };

            if (id == "qPanel") {
                var comp_QuickPanelMc = AdobeAn.getComposition("7A44EE712DF0D641981C6942D548419E");
                var getQuickPanelLib = comp_QuickPanelMc.getLibrary();
                boardMc = new getQuickPanelLib.quickPanel()
                container.parent.addChild(boardMc);
                boardMc.x = boardMc.x
                boardMc.y = boardMc.y;
                boardMc.visible = false;
                continue;
            }

            if (id == "hPanel") {
                var comp_HelpMc = AdobeAn.getComposition("CCEB8AB16488514283EDB755E5DFA164");
                var getHelpLib = comp_HelpMc.getLibrary();

                helpMc = new getHelpLib.helpPanel()
                container.parent.addChild(helpMc);
                helpMc.visible = false;

                continue;
            };

            if (id == "resultLoading") {
                var resScreen = AdobeAn.getComposition("69D87AFB09EC6F4C934222D9FD26164E");
                var getResScreen_lib = resScreen.getLibrary();

                resultLoading = new getResScreen_lib.ResultScreen()
                console.log("resultLoading--- " + resultLoading)
                container.parent.addChild(resultLoading);
                resultLoading.visible = true;

                continue;
            }





            if (id == "domainPath") {
                var json = preload.getResult("domainPath");
                console.log(json); // true
                url = json.path;
                nav = json.nav;
                continue;
            }
        }
        else {
            doneLoading1(event)

        }
    }

  //  bgSnd = playSound("begin", 9999)//createjs.Sound.play("begin",9999);
  //  bgSnd.volume = 0;
    correctSnd = createjs.Sound.play("correct");
    correctSnd.volume = 0;
    wrongSnd = createjs.Sound.play("wrong");
    wrongSnd.volume = 0;
    gameOverSnd = createjs.Sound.play("gameOver");
    gameOverSnd.volume = 0;
    timeOverSnd = createjs.Sound.play("timeOver");
    timeOverSnd.volume = 0;
  

    // start the music
    createjs.Ticker.setFPS(30);
    //if (!createjs.Ticker.hasEventListener("tick")) {
    createjs.Ticker.addEventListener("tick", tick);
    //}
    createjs.Touch.enable(stage, true, true)

    watchRestart();
}

function tick(e) {

    stage.update();
}


function watchRestart() {
    //watch for clicks
stop() 
    stage.addChild(messageField);


    boardMc.boardMc.secMc.txt.text = parseInt(time)//"180"
    boardMc.boardMc.qnsMc.txt.text = "1/" + String(totalQuestions)
    boardMc.boardMc.scoreMc.txt.text = "0";
    //
    // apply font family
    boardMc.boardMc.secMc.txt.font = "bold 33px Veggieburger-Bold";
    boardMc.boardMc.qnsMc.txt.font = "bold 33px Veggieburger-Bold";
    
    boardMc.boardMc.scoreMc.txt.font = "bold 33px Veggieburger-Bold";

    startBtn.cursor = "pointer";
    startMc = new createjs.MovieClip(null, 0, true);
    container.parent.addChild(startMc)
    startMc.addChild(startBtn)
    startMc.visible = true
    startBtn.regX = 160; startBtn.regY = 160;
    startMc.scaleX = startMc.scaleY = 1.2;

    gameIntroAnimMc.visible = false;
    gameIntroAnimMc.gotoAndStop(0);

    getHowToPlayMc.visible = true;
    container.parent.addChild(getHowToPlayMc)
    getHowToPlayMc.howToPlayMc.addEventListener("tick",createHowToPlayHandler)
    


    // startMc.timeline.addTween(createjs.Tween.get(startBtn).to({ scaleX: 0.95, scaleY: 0.95 }, 24).to({ scaleX: 1, scaleY: 1 }, 25).wait(1));
    // startBtn.addEventListener("click", handleClick);
    // gameIntroAnimMc.addEventListener("click", handleClick);
    // gameIntroAnimMc.cursor = "pointer";
    stage.update(); 	//update the stage to show text;

    this.addEventListener("custom_event", animationEndHandler)

// these are equivalent, 1000ms / 40fps = 25ms//

   createjs.Ticker.interval = 25;
   createjs.Ticker.framerate = 30;
   createjs.Ticker.setFPS(24);

}
function createHowToPlayHandler(evt){
   // console.log(evt.currentTarget.currentFrame +" == "+   evt.currentTarget.totalFrames)
    if(evt.currentTarget.currentFrame ==  evt.currentTarget.totalFrames-1){
        var totalFrame = evt.currentTarget.totalFrames
       
        evt.currentTarget.gotoAndStop(totalFrame-1)
        evt.currentTarget.removeEventListener("tick",createHowToPlayHandler)
        clearHowToPlayInterval = setInterval(gameHowToPlayAnimation, 1000);
    }
}
//===========================================================================================//
function gameHowToPlayAnimation(){
     
    clearInterval(clearHowToPlayInterval)
    getHowToPlayMc.visible = false;
    getHowToPlayMc.howToPlayMc.visible = false;
    container.parent.removeChild(getHowToPlayMc)
    getHowToPlayMc = null

     gameIntroAnimMc.visible = true;

    gameIntroAnimMc.gotoAndPlay(0);
}

///////////////////////////////OTHER COMMON FUNCTIONS//////////////////////////////////////////
function playSound(id, loop) {
    return createjs.Sound.play(id, loop);
}
//===========================================================================================//
function range(max, min) {
    return Math.floor(min + (max - min) * Math.random());
}
//===========================================================================================//
function randomSort(a, b) {
    if (Math.random() < 0.5) return -1;
    else return 1;
}
//------------------------------------------------------------------------------------------//
function between(startNumber, endNumber) {
    var baseNumber = []
    var randNumber = []
    for (j = startNumber; j <= endNumber; j++) {
        baseNumber[j] = j;
    }
    for (j = endNumber; j > startNumber; j--) {
        var tempRandom = startNumber + Math.floor(Math.random() * (j - startNumber));
        randNumber[j] = baseNumber[tempRandom];
        baseNumber[tempRandom] = baseNumber[j];
    }
    randNumber[startNumber] = baseNumber[startNumber];
    return randNumber;
}
//-----------------------------------------------------------------------------------------//

