<!DOCTYPE html>
<!--
	NOTES:
	1. All tokens are represented by '$' sign in the template.
	2. You can write your code only wherever mentioned.
	3. All occurrences of existing tokens will be replaced by their appropriate values.
	4. Blank lines will be removed automatically.
	5. Remove unnecessary comments before creating your template.
-->
<html>
<head>
<meta charset="UTF-8">
<meta name="authoring-tool" content="Adobe_Animate_CC">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<title>ResultNew</title>
<!-- write your code here -->
<style>
  #animation_container {
	position:absolute;
	margin:auto;
	left:0;right:0;
	top:0;bottom:0;
  }
</style>
<script src="<?php echo base_url(); ?>/assets/swf/<?php echo $this->session->userlang; ?>/assets/createjs-2015.11.26.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/Result.js?1482418925604"></script>
 

<script>


function getParamValue(paramName)
{
    var url = window.location.search.substring(1); //get rid of "?" in querystring
    var qArray = url.split('&'); //get key-value pairs
    for (var i = 0; i < qArray.length; i++) 
    {
        var pArr = qArray[i].split('='); //split key and value
        if (pArr[0] == paramName) 
		{
		//alert(pArr[1]);
		return pArr[1]; //return value
		}
            
			
    }
}
var tn;
		var aq;
		var cq;
		var sv;
		var rts;
		tn = <?php echo $_REQUEST["tqcnt1"]; ?>;//obj.tn;
			aq =  <?php echo $_REQUEST["aqcnt1"]; ?>;//obj.aq;
			cq =  <?php echo $_REQUEST["cqcnt1"]; ?>;//obj.cq;
			sv =  <?php echo $_REQUEST["gscore1"]; ?>;//obj.sv;
			rts =  <?php echo $_REQUEST["rtime1"]; ?>;//obj.rts;
			
			
			
			var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init() {
	canvas = document.getElementById("canvas");
	anim_container = document.getElementById("animation_container");
	dom_overlay_container = document.getElementById("dom_overlay_container");
	handleComplete();
}
function handleComplete() {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	exportRoot = new lib.ResultNew();
	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);	
	//Registers the "tick" event listener.
	fnStartAnimation = function() {
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas);		
		resizeCanvas();		
		function resizeCanvas() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = window.innerWidth, ih=window.innerHeight;			
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas.width = w*pRatio*sRatio;			
			canvas.height = h*pRatio*sRatio;
			canvas.style.width =    dom_overlay_container.style.width=  w*sRatio+'px';				
			canvas.style.height =   dom_overlay_container.style.height = h*sRatio+'px';
			stage.scaleX = pRatio*sRatio;			
			stage.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;		
		}
	}
	makeResponsive(false,'both',false,1);	
	fnStartAnimation();
}
			
			
			
	 
</script>
<!-- write your code here -->
</head>
<body onload="init();" style="margin:0px;">
	<div id="animation_container" style="text-align:center;overflow: hidden;">
		<canvas id="canvas" width="550" height="400" style=""></canvas>
		<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:550px; height:400px; position: absolute; left: 0px; top: 0px; display: block;">
		</div>
	</div>
</body>
<style>
 body{ margin:0; min-width:100%; max-width:100%; background:url("<?php echo base_url(); ?>/assets/swf/<?php echo $this->session->userlang; ?>/assets/bgtile.jpg") repeat;}

  :full-screen{background:url("<?php echo base_url(); ?>/assets/swf/<?php echo $this->session->userlang; ?>/assets/bgtile.jpg") repeat;}
  :-webkit-full-screen{background:url("<?php echo base_url(); ?>/assets/swf/<?php echo $this->session->userlang; ?>/assets/bgtile.jpg") repeat;}
  :-moz-full-screen{background:url("<?php echo base_url(); ?>/assets/swf/<?php echo $this->session->userlang; ?>/assets/bgtile.jpg") repeat;}
  #animation_container{height:400px;}
  @media only screen 
and (min-device-width : 375px) 
and (max-device-width : 667px) and (orientation : portrait) { #animation_container{height:240px;}}  
@media only screen 
and (min-device-width : 375px) 
and (max-device-width : 667px) and (orientation : landscape) { #animation_container{height:330px;}}
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 568px) and (orientation : portrait) { #animation_container{height:330px;}}
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 568px) and (orientation : landscape) { #animation_container{height:240px;}}
  
</style>
</html>
