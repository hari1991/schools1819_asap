(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(255,255,255,0.498)").ss(5,1,1).p("EhOWgzdMCctAAAQDPAACTCSQCSCTAADPMAAABXTQAADPiSCTQiTCSjPAAMictAAAQjPAAiTiSQiSiTAAjPMAAAhXTQAAjPCSiTQCTiSDPAAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.349)").s().p("EhOWAzeQjPAAiTiTQiSiSAAjPMAAAhXTQAAjPCSiSQCTiTDPAAMCctAAAQDPAACTCTQCSCSAADPMAAABXTQAADPiSCSQiTCTjPAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-554,-331.9,1108.1,663.8), null);


(lib.ScoreAni = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(11));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ak7E8QiCiDAAi5QAAi4CCiDQCDiDC4AAQC4AACDCDQCDCDAAC4QAAC5iDCDQiDCDi4AAQi4AAiDiDg");
	this.shape.setTransform(-0.6,0.4,0.987,0.987);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(11));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99CC00").s().p("AB7gFYAAAEAAADAAAEIj1gEYAAgCAAgCAAgDg");
	this.shape_1.setTransform(0.6,-55.4,1,1,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#99CC00").s().p("ACkjGYAACPgtCLhUBzIjGiRYA1hIAdhZAAhbg");
	this.shape_2.setTransform(20,-51.2,1,1,-90);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99CC00").s().p("AEQlCYAAEmi9EEkWBbIhMjrYCyg6B3ilAAi7g");
	this.shape_3.setTransform(32.3,-40.5,1,1,-90);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#99CC00").s().p("AG7lSYAADZhoDLivCAYiuB/jiAkjOhDIBMjrYCDArCQgXBuhRYBwhQBCiCAAiKg");
	this.shape_4.setTransform(34,-23.4,1,1,-90);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#99CC00").s().p("AJklSYAAEmi9EDkXBbYkVBbkxhkitjtIDHiQYBuCWDCA/Cwg6YCyg5B4ilAAi7g");
	this.shape_5.setTransform(34,-6.5,1,1,-90);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#99CC00").s().p("AKklSYAAF1kvEwl1AAYl0AAkvkwAAl1ID2AAYAADuDADBDtAAYDuAADAjBAAjug");
	this.shape_6.setTransform(34,0,1,1,-90);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#99CC00").s().p("AKkiLYAAEli8EFkXBbYkWBakxhjisjuYisjuAAlBCsjuIDHCRYhuCYAADNBuCWYBtCXDDBACwg6YCxg6B4imAAi6g");
	this.shape_7.setTransform(14,0,1,1,-90);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#99CC00").s().p("AKkgQYAAFbkFEjlYAkYlXAlk8jnhIlTYhIlTDDlUFJhrIBMDrYjRBEh9DYAuDXYAuDYDJCTDagXYDbgXCmi5AAjdg");
	this.shape_8.setTransform(1.7,0,1,1,-90);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#99CC00").s().p("AKkAAYAAEmi8EFkXBbYkWBakxhjisjuYisjuAAlBCsjuYCsjuExhjEWBaIhMDrYiwg6jDBAhtCXYhuCXAADNBuCXYBtCXDDBACwg6YCxg6B4imAAi7g");
	this.shape_9.setTransform(0,0,1,1,-90);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#99CC00").s().p("AKkAAYAAFOjyEclIA0YlIA1k9jEhnk9Yhnk9CPlZEoiYYEoiXFqBXDDEOIjHCRYh8isjmg3i8BgYi9BhhbDbBCDJYBBDLDKB8DQghYDSgiCZi1AAjUg");
	this.shape_10.setTransform(0,0,1,1,-90);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#99CC00").s().p("AqjAAQAAhdAYhWQAYhXAshMQAshLA+g+QA9g+BMgsQBLgtBWgYQBWgXBdgBQBeABBWAXQBWAYBLAtQBMAsA9A+QA+A+AsBLQAsBMAYBXQAYBWAABdQAABegYBWQgYBXgsBMQgsBLg+A+Qg9A+hMAsQhLAthWAYQhWAXheABQiMgBh6g1Qh6g0hdhdQhdhdg0h7Qg0h7gBiNgAmtAAQAAA8APA3QAPA3AcAwQAdAwAnAnQAnAoAwAcQAwAcA3AQQA2APA7AAQA8AAA2gPQA3gQAwgcQAwgcAngoQAngnAdgwQAcgwAPg3QAPg3AAg8QAAhZghhOQgihOg7g7Qg7g8hNghQhOghhagBQg7AAg2APQg3AQgwAcQgwAcgnAoQgnAngdAwQgcAwgPA3QgPA3AAA7g");
	this.shape_11.setTransform(0,0,1,1,-90);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).wait(1));

	// Layer_2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AqjAAQAAhdAYhWQAYhXAshMQAshLA+g+QA9g+BMgsQBLgtBWgYQBWgXBdgBQBeABBWAXQBWAYBLAtQBMAsA9A+QA+A+AsBLQAsBMAYBXQAYBWAABdQAABegYBWQgYBXgsBMQgsBLg+A+Qg9A+hMAsQhLAthWAYQhWAXheABQiMgBh6g1Qh6g0hdhdQhdhdg0h7Qg0h7gBiNgAmtAAQAAA8APA3QAPA3AcAwQAdAwAnAnQAnAoAwAcQAwAcA3AQQA2APA7AAQA8AAA2gPQA3gQAwgcQAwgcAngoQAngnAdgwQAcgwAPg3QAPg3AAg8QAAhZghhOQgihOg7g7Qg7g8hNghQhOghhagBQg7AAg2APQg3AQgwAcQgwAcgnAoQgnAngdAwQgcAwgPA3QgPA3AAA7g");
	this.shape_12.setTransform(0,0,0.99,0.99,-90);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-67.7,134.5,134.6);


(lib.ResponseAni = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(11));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ak7E8QiCiDAAi5QAAi4CCiDQCDiDC4AAQC4AACDCDQCDCDAAC4QAAC5iDCDQiDCDi4AAQi4AAiDiDg");
	this.shape.setTransform(-0.6,0.4,0.987,0.987);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(11));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3333").s().p("Ah6ABIAAgGID1AAIAAALg");
	this.shape_1.setTransform(0.6,-55.3,1,1,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3333").s().p("AijA2QBShxAAiLID1AAQAADbiACyg");
	this.shape_2.setTransform(20,-51.2,1,1,-90);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF3333").s().p("AkPBYQCFgsBShxQBRhxAAiMID3AAQAADciCCyQiBCyjPBFg");
	this.shape_3.setTransform(32.3,-40.5,1,1,-90);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF3333").s().p("Am6EyIBMjrQCFArCFgsQCEgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihpAAQhoAAhogig");
	this.shape_4.setTransform(34,-23.4,1,1,-90);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF3333").s().p("AkREyQjQhEiCizIDHiQQBSBwCFArQCFAsCEgsQCFgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihoAAQhoAAhpgig");
	this.shape_5.setTransform(34,-6.5,1,1,-90);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF3333").s().p("AndCMQjHjFABkZID1AAQABCyB9B/QB/B9CxAAQCxAAB/h9QB9h/ABiyID2AAQAAEZjGDFQjGDIkYgBQkXABjGjIg");
	this.shape_6.setTransform(34,0,1,1,-90);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF3333").s().p("AmtF/QjPisgikJQgikKCejaIDHCRQhkCLAVCpQAWCoCDBtQCEBtCogLQCpgKB1h9QB1h9AAipID3AAQAAELi4DEQi4DFkKARQgYABgXAAQjsAAi9icg");
	this.shape_7.setTransform(14,0,1,1,-90);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF3333").s().p("AmMIUQjSiZg2j/Qg2j+CBjhQCCjiD3hRIBMDsQidAzhTCPQhSCQAiChQAiCiCGBhQCGBiCjgSQCkgRBuh7QBvh7AAilID3AAQAAEEiuDBQiuDCkCAbQgnAFglAAQjVAAiyiDg");
	this.shape_8.setTransform(1.7,0,1,1,-90);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF3333").s().p("AlyI4QjUiNhEj1QhEj0BujmQBvjnDphkQDqhkDwBPIhNDrQiYgyiUA/QiUBAhHCTQhHCSAsCbQArCcCHBZQCHBZCegWQCggWBqh6QBqh6AAihID3AAQAAD+inDAQinDAj7AiQgyAHgwAAQjDAAiphwg");
	this.shape_9.setTransform(-0.1,0,1,1,-90);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF3333").s().p("AlgJDQjViDhNjuQhNjtBgjoQBfjnDehyQDfhxDxA6QDzA6CTDLIjICSQhdiBibglQiZgliNBIQiNBIg9CTQg9CTAxCWQAxCYCHBTQCIBTCbgZQCdgYBnh6QBnh5AAifID3AAQAAD7iiC+QiiC/j2AnQg5AJg3AAQi3AAikhlg");
	this.shape_10.setTransform(0,0,1,1,-90);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF3333").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_11.setTransform(0,0,1,1,-90);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).wait(1));

	// Layer_2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_12.setTransform(0,-0.1,0.99,0.99,-90);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-67.7,134.5,134.6);


(lib.QuestionAni = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(11));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ak7E8QiCiDAAi5QAAi4CCiDQCDiDC4AAQC4AACDCDQCDCDAAC4QAAC5iDCDQiDCDi4AAQi4AAiDiDg");
	this.shape.setTransform(-0.6,0.4,0.987,0.987);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(11));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF9900").s().p("Ah6ABIAAgGID1AAIAAALg");
	this.shape_1.setTransform(0.6,-55.3,1,1,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF9900").s().p("AijA2QBShxAAiLID1AAQAADbiACyg");
	this.shape_2.setTransform(20,-51.2,1,1,-90);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF9900").s().p("AkPBYQCFgsBShxQBRhxAAiMID3AAQAADciCCyQiBCyjPBFg");
	this.shape_3.setTransform(32.3,-40.5,1,1,-90);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF9900").s().p("Am6EyIBMjrQCFArCFgsQCEgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihpAAQhoAAhogig");
	this.shape_4.setTransform(34,-23.4,1,1,-90);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF9900").s().p("AkREyQjQhEiCizIDHiQQBSBwCFArQCFAsCEgsQCFgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihoAAQhoAAhpgig");
	this.shape_5.setTransform(34,-6.5,1,1,-90);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF9900").s().p("AndCMQjHjFABkZID1AAQABCyB9B/QB/B9CxAAQCxAAB/h9QB9h/ABiyID2AAQAAEZjGDFQjGDIkYgBQkXABjGjIg");
	this.shape_6.setTransform(34,0,1,1,-90);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF9900").s().p("AmtF/QjPisgikJQgikKCejaIDHCRQhkCLAVCpQAWCoCDBtQCEBtCogLQCpgKB1h9QB1h9AAipID3AAQAAELi4DEQi4DFkKARQgYABgXAAQjsAAi9icg");
	this.shape_7.setTransform(14,0,1,1,-90);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF9900").s().p("AmMIUQjSiZg2j/Qg2j+CBjhQCCjiD3hRIBMDsQidAzhTCPQhSCQAiChQAiCiCGBhQCGBiCjgSQCkgRBuh7QBvh7AAilID3AAQAAEEiuDBQiuDCkCAbQgnAFglAAQjVAAiyiDg");
	this.shape_8.setTransform(1.7,0,1,1,-90);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF9900").s().p("AlyI4QjUiNhEj1QhEj0BujmQBvjnDphkQDqhkDwBPIhNDrQiYgyiUA/QiUBAhHCTQhHCSAsCbQArCcCHBZQCHBZCegWQCggWBqh6QBqh6AAihID3AAQAAD+inDAQinDAj7AiQgyAHgwAAQjDAAiphwg");
	this.shape_9.setTransform(-0.1,0,1,1,-90);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF9900").s().p("AlgJDQjViDhNjuQhNjtBgjoQBfjnDehyQDfhxDxA6QDzA6CTDLIjICSQhdiBibglQiZgliNBIQiNBIg9CTQg9CTAxCWQAxCYCHBTQCIBTCbgZQCdgYBnh6QBnh5AAifID3AAQAAD7iiC+QiiC/j2AnQg5AJg3AAQi3AAikhlg");
	this.shape_10.setTransform(0,0,1,1,-90);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF9900").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_11.setTransform(0,0,1,1,-90);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).wait(1));

	// Layer_2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_12.setTransform(0,-0.1,0.99,0.99,-90);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-67.7,134.5,134.6);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(0.5,1,1).p("Aq9ikIgQAAQicFCFGgmQAQgBALgLQALBZBYgVQAsgLAigmQAxB3CKACQAQABAQgGIAQBVIA1AAIAWgVQgBAXALATQALAVAWAIQADAAADABIA7g4QBDAWA2grQAhgagKgsQCJAuB9g0IAHguQAwAPAugVQArgUAMgsQClAfgriiQgNgwg3ApIgPhQIidgDQAXhPg3g4QggghgtAVQAQhRhBgIQgtgIgmAhQgZg7hGAEQhQAKggBMIgngvQh/ASgSB9IglhMQhagZhAA+QgfAcgLAmQiSi1izCVQgRAOgNAS");
	this.shape.setTransform(273.6,-221.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],-76.8,0,76.8,0).s().p("AAKF6QgWgIgLgVQgLgTABgXIgWAVIg1AAIgQhVQgQAGgQgBQiKgCgxh3QgiAmgsALQhYAVgLhZQgLALgQABQlGAmCclCIAQAAIAAgEQANgSARgOQCziVCSC1QALgmAfgcQBAg+BaAZIAlBMQASh9B/gSIAnAvQAghMBQgKQBGgEAZA7QAmghAtAIQBBAIgQBRQAtgVAgAhQA3A4gXBPICdADIAPBQQA3gpANAwQArCiilgfQgMAsgrAUQguAVgwgPIgHAuQh9A0iJguQAKAsghAaQg2ArhDgWIg7A4IgGgBg");
	this.shape_1.setTransform(273.6,-221.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(0.5,1,1).p("A2IlNQn9gjCqGWQAWA2BSAAIg5A7QCUHHGjjAQBzECCqjaIBpBIIAvgKIAjhZQCAHrGMkKQAxCqCmASQAsAAAsgOIBAiKIAKAyQBnA1BYhEQA3gpgfhFIEQA+QAUieBbiAIBwC4IAahHIB8DUIAXiJICJBvIBEgLIATiXIBlBwIBHAAIAAiiIBSAkQBqgbAphdQAJgWAHgVIAqgWQAHj6jvg4IgdikIhnAZQgLk6lzg4QhqA4AJB0IgZAAQifh2iLCPIhIhkQhwgThmA4QhhmlljBxQjslujXGFIhWjFQm8gNgFG/QhyiviLB/QgrAogaA4");
	this.shape_2.setTransform(-305.3,-261.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],-179.8,0,179.8,0).s().p("AiRKMQmMEKiAnrIgjBZIgvAKIhphIQiqDahzkCQmjDAiUnHIA5g7QhSAAgWg2QiqmWH9AjIACgVQAag4ArgoQCLh/ByCvQAFm/G8ANIBWDFQDXmFDsFuQFjhxBhGlQBmg4BwATIBIBkQCLiPCfB2IAZAAQgJh0Bqg4QFzA4ALE6IBngZIAdCkQDvA4gHD6IgqAWQgHAVgJAWQgpBdhqAbIhSgkIAACiIhHAAIhlhwIgTCXIhEALIiJhvIgXCJIh8jUIgaBHIhwi4QhbCAgUCeIkQg+QAfBFg3ApQhYBEhng1IgKgyIhACKQgsAOgsAAQimgSgxiqg");
	this.shape_3.setTransform(-305.3,-261.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],9.1,-130.5,9.1,330.3).s().p("AAeRtQhDgYgig+Qggg6AChEIhBA+IifAAIgwj+QgxARgwgCQmdgGiUlmQhlByiEAiQkJA/ggkMQgiAhgwAEQvRByHUvIIAvAAIAAgNQAng2AzgpQIZm/G2IeQAihxBchVQDBi5EOBLIBuDjQA3l3F+g2IB2COQBfjlDwgeQDTgLBKCwQByhiCIAYQDCAYgwDyQCHg+BgBjQCmCohGDtIHXAIIAtDxQClh8AnCRQCCHonwheQgkCEiBA9QiKA+iQgsIgUCJQl4CdmaiKQAeCDhjBPQiiCBjJhDIixCoIgTgDg");
	this.shape_4.setTransform(214.5,254.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],9.1,-130.5,9.1,330.3).s().p("AAeRtQhDgYgig+Qggg6AChEIhBA+IifAAIgwj+QgxARgwgCQmdgGiUlmQhlByiEAiQkJA/ggkMQgiAhgwAEQvRByHUvIIAvAAIAAgNQAng2AzgpQIZm/G2IeQAihxBchVQDBi5EOBLIBuDjQA3l3F+g2IB2COQBfjlDwgeQDTgLBKCwQByhiCIAYQDCAYgwDyQCHg+BgBjQCmCohGDtIHXAIIAtDxQClh8AnCRQCCHonwheQgkCEiBA9QiKA+iQgsIgUCJQl4CdmaiKQAeCDhjBPQiiCBjJhDIixCoIgTgDg");
	this.shape_5.setTransform(-214.5,254.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],1.3,-88.1,1.3,306.7).s().p("AiOKBQmGEFh9njIgjBYIguAKIhohHQinDWhxj9QmcC9iRm/IA4g7QhQAAgWg1QinmPH0AjIACgVQAZg3ArgnQCIh9BxCsQAEm3G1ANIBUDBQDUl+DoFnQFdhvBfGeQBlg3BtASIBHBjQCIiMCdBzIAYAAQgIhyBog2QFtA2AKE1IBmgZIAdCiQDrA3gHD1IgqAWQgHAVgJAVQgoBchoAaIhRgjIAACfIhGAAIhjhuIgSCVIhDAKIiHhtIgXCHIh6jRIgZBGIhui1QhaB+gTCcIkMg9QAeBDg2ApQhWBDhmg0IgJgyIg/CIQgrAOgsAAQiigSgwing");
	this.shape_6.setTransform(39.5,196.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],6.1,-87,6.1,220.2).s().p("AAULzQgsgQgXgpQgVgnABgtIgrApIhqAAIggipQghALgggBQkTgEhjjvQhDBMhYAXQixAqgVizQgXAWggADQqLBME4qFIAfAAIAAgJQAagkAigbQFmkqEkFpQAXhLA9g5QCBh7C0AyIBJCXQAlj6D+gkIBPBfQA/iZCggUQCNgHAxB1QBMhBBbAQQCBAQggChQBagpBABCQBvBwgvCeIE6AFIAeChQBuhTAaBhQBXFFlLg/QgYBYhWApQhcAphggdIgNBbQj7BpkRhcQAUBXhCA1QhsBWiGgtIh2BwIgNgCg");
	this.shape_7.setTransform(-285.4,182.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.8,-55.1,0.8,191.7).s().p("AhZGRQjzCjhPkuIgVA3IgdAGIhBgsQhoCGhHifQkBB2hbkXIAjglQgyAAgOghQhoj5E4AWIACgNQAPgjAbgYQBVhOBHBsQACkTERAIIA1B5QCEjvCRDhQDahFA8ECQA+gjBFAMIAsA+QBVhYBiBIIAPAAQgFhHBBgiQDkAiAHDBIA/gPIASBkQCTAjgECZIgaAOQgEANgGANQgZA5hBARIgzgWIAABjIgsAAIg9hFIgMBdIgqAHIhUhFIgOBVIhNiDIgQAsIhEhxQg4BPgMBhIingmQASAqghAZQg2AqhAggIgGgfIgnBVQgbAIgbAAQhlgLgfhog");
	this.shape_8.setTransform(246.6,140.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.6,-38.2,0.6,133.2).s().p("Ag9EWQipByg3jSIgPAmIgUAFIgtgfQhIBdgxhuQizBSg/jCIAYgaQgjAAgJgXQhJisDZAPIABgJQALgYATgRQA7g2AxBKQACi+C9AFIAlBUQBcimBkCcQCXgwAqC0QArgYAwAIIAfArQA7g9BEAyIAKAAQgDgyAtgXQCeAXAFCGIAsgKIAMBFQBmAYgDBrIgSAJIgHASQgRAogtAMIgjgQIAABFIgfAAIgrgvIgIBAIgdAFIg6gwIgKA7Ig1hbIgLAfIgwhPQgnA3gIBDIh0gaQANAdgYASQglAdgsgXIgEgVIgcA7QgSAGgTAAQhGgIgVhJg");
	this.shape_9.setTransform(150.5,103.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.9,-57.4,0.9,199.7).s().p("AhcGhQj+CqhSk6IgWA5IgeAHIhEgvQhsCMhKilQkMB7hfkjIAlgnQg1AAgOgiQhtkDFGAWIABgNQARgkAcgaQBYhRBKBwQADkeEcAIIA3B+QCKj5CWDqQDjhIA/EOQBBgkBHAMIAvBAQBYhbBmBLIAQAAQgFhLBDgjQDuAjAHDJIBCgPIASBoQCZAkgECgIgbAOIgLAbQgaA8hDASIg1gYIAABoIguAAIhAhHIgMBgIgsAHIhXhHIgPBYIhQiIIgQAuIhIh2Qg6BSgNBlIiugoQAUAsgkAbQg4ArhCgiIgGggIgpBZQgcAJgcAAQhqgMgfhtg");
	this.shape_10.setTransform(-323.9,71.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.9,-57.4,0.9,199.7).s().p("AhcGhQj+CqhSk6IgWA5IgeAHIhEgvQhsCMhKilQkMB7hfkjIAlgnQg1AAgOgiQhtkDFGAWIABgNQARgkAcgaQBYhRBKBwQADkeEcAIIA3B+QCKj5CWDqQDjhIA/EOQBBgkBHAMIAvBAQBYhbBmBLIAQAAQgFhLBDgjQDuAjAHDJIBCgPIASBoQCZAkgECgIgbAOIgLAbQgaA8hDASIg1gYIAABoIguAAIhAhHIgMBgIgsAHIhXhHIgPBYIhQiIIgQAuIhIh2Qg6BSgNBlIiugoQAUAsgkAbQg4ArhCgiIgGggIgpBZQgcAJgcAAQhqgMgfhtg");
	this.shape_11.setTransform(-104.6,116.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],4.5,-65.3,4.5,165.1).s().p("AAPI2QghgMgRgeQgQgeABghIggAeIhQAAIgYh+QgZAIgYgBQjOgDhKizQgyA5hCARQiFAggQiHQgRARgYACQnoA5DqnjIAXAAIAAgHQATgbAagUQEMjgDbEPQASg4AtgrQBhhcCHAlIA3BxQAci7C+gbIA7BHQAvhzB4gPQBqgFAlBYQA5gxBEAMQBhAMgYB5QBDgfAwAyQBUBUgkB2IDsAEIAWB5QBTg/ATBJQBCD0j5gwQgSBChAAfQhFAfhIgWIgKBEQi8BPjNhFQAPBBgxAoQhRBBhlgiIhYBUIgKgCg");
	this.shape_12.setTransform(307.6,1.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.7,-45.9,0.7,159.8).s().p("AhJFOQjLCIhCj8IgSAuIgYAFIg2glQhXBwg6iEQjXBihMjpIAegeQgqAAgMgcQhXjPEFASIABgLQANgdAWgUQBHhBA7BaQACjlDkAHIArBkQBvjHB4C7QC2g5AyDXQA0gdA5AKIAlA0QBHhKBRA8IANAAQgEg7A2gcQC+AcAFChIA1gNIAPBUQB7AdgEB/IgWAMQgDALgFAKQgVAwg2AOIgqgSIAABSIglAAIgzg5IgKBOIgjAFIhGg5IgMBGIg/hsIgOAkIg5heQguBCgLBRIiLggQAQAjgcAVQgtAjg1gbIgFgaIghBHQgWAHgXAAQhUgJgZhXg");
	this.shape_13.setTransform(234.9,50.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.7,-45.9,0.7,159.8).s().p("AhJFOQjLCIhCj8IgSAuIgYAFIg2glQhXBwg6iEQjXBihMjpIAegeQgqAAgMgcQhXjPEFASIABgLQANgdAWgUQBHhBA7BaQACjlDkAHIArBkQBvjHB4C7QC2g5AyDXQA0gdA5AKIAlA0QBHhKBRA8IANAAQgEg7A2gcQC+AcAFChIA1gNIAPBUQB7AdgEB/IgWAMQgDALgFAKQgVAwg2AOIgqgSIAABSIglAAIgzg5IgKBOIgjAFIhGg5IgMBGIg/hsIgOAkIg5heQguBCgLBRIiLggQAQAjgcAVQgtAjg1gbIgFgaIghBHQgWAHgXAAQhUgJgZhXg");
	this.shape_14.setTransform(-153,50.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0.6,-38.2,0.6,133.2).s().p("Ag9EWQipByg3jSIgPAmIgUAFIgtgfQhIBdgxhuQizBSg/jCIAYgaQgjAAgJgXQhJisDZAPIABgJQALgYATgRQA7g2AxBKQACi+C9AFIAlBUQBcimBkCcQCXgwAqC0QArgYAwAIIAfArQA7g9BEAyIAKAAQgDgyAtgXQCeAXAFCGIAsgKIAMBFQBmAYgDBrIgSAJIgHASQgRAogtAMIgjgQIAABFIgfAAIgrgvIgIBAIgdAFIg6gwIgKA7Ig1hbIgLAfIgwhPQgnA3gIBDIh0gaQANAdgYASQglAdgsgXIgEgVIgcA7QgSAGgTAAQhGgIgVhJg");
	this.shape_15.setTransform(-222.9,7.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],3,-43.5,3,110.1).s().p("AAKF6QgWgIgLgVQgLgTABgXIgWAVIg1AAIgQhVQgQAGgQgBQiKgCgxh3QgiAmgsALQhYAVgLhZQgLALgQABQlGAmCclCIAQAAIAAgEQANgSARgOQCziVCSC1QALgmAfgcQBAg+BaAZIAlBMQASh9B/gSIAnAvQAghMBQgKQBGgEAZA7QAmghAtAIQBBAIgQBRQAtgVAgAhQA3A4gXBPICdADIAPBQQA3gpANAwQArCiilgfQgMAsgrAUQguAVgwgPIgHAuQh9A0iJguQAKAsghAaQg2ArhDgWIg7A4IgGgBg");
	this.shape_16.setTransform(-334.9,9.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("Eg+fgWMMAAAggxMB8/AAAMAAAAmWMAAABHlMh8/AAAg");
	this.shape_17.setTransform(-39,23.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(0.5,1,1).p("EA+ggJsQg3glg7AfQgIAFgJAFIgbAAIgVgpQgQAAgNALQgNAKgLAMIgUghQhaALhLA4QACgWgVgGQh4gnhoBJQgHgsgjgcQgHgFgHgFQhAAhgsA3QgKgggNghQgHgSgPgMQgEgDgEgDIinA6QhAgphIAXQgJADgJADIgSBLIgJgyQhpgKhVA4QgkAYgTAoQgVAtgHAwQgjgUgogIQgzAEgpAhQgtAjgGA6QgDAdAAAdIhGAMQgPAgAPAjQAIATAPANIgbAAQAFA/AGBAIg7AAQg1gygnhBQgOAAgJAHQgOALgDASQgwABgQAvQgGARABATQgegGgbAJQgGADgGADIgFBLQgsAhgRA4QgCAIgBAIIgGgLIgGgLQhhADg0BRQgvgngvAqQgQAPADAWQADAUALAQIgfgeQgwAGgRAtQgXA8ARA+QgigYgqgLQgUgFgRAOQgnAegJAxQgJA2AwAZQAnATAtACIgXAWIgggFIgGAyIAWAYIgrAAIAABVQgqgagpAhQglAfAEA0IhAgaQg0AQgLA3QgEAQgBAQQgYhLhLgXQgJgDgIgBQgDgBgDgBIAIgHQAUhKgzg0QgJgKgMgFQgmgWgtABQhQhXh0gIQhEABg7AeIgUgeQgTAEgUAIQgTAJgSAPQgchmhpgJIgOAUQAIgtgrgbQgPgJgPAHQgWAJgPASQgGgcgegMQgpgRgpANIgOAhQgEgogggYQgngeguAQQANgwgXgoQgbgwgjgrQgegmgdgmIglAAIgQALIgGggIg4AAIADhVIg1gPQAJhAgpgyQgMgOgSAAQgNAAgOABQAHg4gkgqQgOgPgTgFQg1gPg3ALQgLhJg2gxQgbgZgnABQgvADgqAWQgKhshShKQg1gwhIACQgpAFgjAVQgUAMgUANIgcggQiTAUhyBjIgSgYIg1AHIgYBgIhhg1QgsAUgcApQgKANgIAOQgwgDgoAZQgNAJgHASQgHARAAAVIgbgeQgDgBgCAAQgEAAgDAAQgEAAgDAAQgDABgDACQgDABgCACQgDACgDACQgOgSgVgMQgMgHgNgEIgWAJIgYgQIg1AAIAAA1QgbgKgWgTQgpgigzAJQgRAEgPAIQgRAJgQAMIgIgQIgaAEIgTBQIhjABIgSAqQgcgOgfAEQgLACgLACIgDAxIgIgKIgjARIgViWIggAAIgShBIAPgLQAMg1gngoQggghgugFQgvgEguAOQAOgugjgfQgOgNgTAAQgPACgPACQAOgugQguQgDgHgCgGQhBgYg8AgQgIAEgJAFIgKhWIhZgCIgRAeQg5gjg+AVQgOAGgOAIIgBA6IgOgsQgwgFghAiQgPAQgHAWQANg5gdgzQgTgiglgOQgOgFgPgFIAAhaIhKAFIgKhQQgngNgoAEQghAFgUAdQgNhNhDgnQgagOgeAKQgvAQgmAjARIPEIAPBGEg2FgJbIgFAY");
	this.shape_18.setTransform(-39,-20.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FFFFFF","#0BCDFD"],[0,1],0,-259.9,0,259.9).s().p("Eg+fAnBMAAAhNKQAmgjAvgQQAfgKAZAOQBDAnANBNQAUgdAhgFQApgEAmANIAKBQIBKgFIAABaIAeAKQAkAOATAiQAdAzgNA5QAHgWAPgQQAhgiAwAFIAOAsIACg6QANgIAPgGQA9gVA5AjIARgeIBaACIAKBWIARgJQA7ggBBAYIAFANQAQAugOAuIAfgEQASAAAOANQAkAfgOAuQAtgOAvAEQAvAFAfAhQAnAogMA1IgPALIASBBIAhAAIAUCWIAkgRIAHAKIADgxIAWgEQAfgEAcAOIASgqIBjgBIAThQIAagEIAIAQQAQgMASgJQAPgIAQgEQAzgJApAiQAXATAbAKIAAg1IA1AAIAXAQIAXgJQANAEAMAHQAUAMAPASIAFgEIAGgDIAFgDIAHAAIAHAAIAGABIAbAeQgBgVAHgRQAHgSAOgJQAngZAwADIASgbQAdgpArgUIBhA1IAYhgIA2gHIASAYQBxhjCUgUIAcAgQATgNAUgMQAkgVAogFQBIgCA1AwQBSBKALBsQApgWAvgDQAngBAbAZQA2AxALBJQA3gLA1APQATAFAOAPQAkAqgHA4IAbgBQASAAAMAOQApAygJBBIA1APIgDBVIA5AAIAFAgIAQgLIAlAAIA8BMQAiArAbAwQAXAogNAwQAugQAnAeQAgAYAEAoIAOghQApgNApARQAdAMAHAcQAPgSAWgJQAPgHAOAJQAsAbgJAtIAPgUQBoAJAdBmQASgPATgJQAUgIASgEIAUAeQA8geBDgBQB1AIBQBXQAtgBAmAWQAMAFAJAKQAzA0gVBKIgHAHIAGACIARAEQBLAXAYBLQABgQADgQQALg3A1gQIBAAaQgFg0AmgfQApghAqAaIAAhVIAqAAIgVgYIAFgyIAhAFIAXgWQgugCgmgTQgwgZAJg2QAJgxAmgeQARgOAUAFQArALAiAYQgRg+AWg8QASgtAwgGIAfAeQgLgQgDgUQgDgWAPgPQAvgqAwAnQA0hRBhgDIAGALIAFALIAEgQQAQg4AtghIAEhLIAMgGQAcgKAeAGQgBgTAFgRQAQgvAxgBQACgSAOgLQAKgHAOAAQAnBBA1AyIA7AAIgMh/IAbAAQgOgNgIgTQgPgjAPggIBGgMQAAgdADgdQAGg6AtgjQApghAzgEQAoAIAjAUQAHgwAUgtQAUgoAjgYQBWg4BoAKIAKAyIAShLIARgGQBJgXBAApICmg6IAJAGQAPAMAHASQAMAhAKAgQAtg3BAghIAOAKQAjAcAGAsQBphJB4AnQAUAGgBAWQBLg4BagLIAUAhQALgMAMgKQAOgLAQAAIAVApIAbAAIARgKQA7gfA3AlMAAABHkgARXmsIgPhGgEg2Kgf6IAFgYg");
	this.shape_19.setTransform(-39,125.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#0066FF","#0BCDFD"],[0,1],-388.9,-281.4,-143.2,-2.4).s().p("APleBIgRgEIABgJQAVhJgzg1QgJgJgMgGQgmgVgtAAQhQhXh1gIQhDABg8AfIgUgeQgSAEgUAIQgTAIgSAPQgdhlhogJIgPAUQAJgugsgaQgOgJgPAGQgWAKgPASQgHgdgdgLQgpgRgpANIgOAhQgEgogggYQgngeguAPQANgvgXgoQgbgwgigsIg8hMIglAAIgQALIgFggIg5AAIADhUIg1gQQAJhAgpgyQgMgOgSAAIgbABQAHg4gkgqQgOgPgTgGQg1gOg3ALQgLhKg2gxQgbgZgnABQgvAEgpAWQgLhthShJQg1gwhIABQgoAFgkAVQgUAMgTAOIgcggQiUAThxBjIgSgXIg2AGIgYBhIhhg1QgrAUgdAoIgSAbQgwgDgnAaQgOAJgHARQgHASABAUIgbgdIgGgCIgHAAIgHABIgFACIgGADIgFAFQgPgTgUgLQgMgHgNgFIgXAJIgXgQIg1AAIAAA1QgbgKgXgSQgpgigzAJQgQADgPAIQgSAKgQALIgIgPIgaAEIgTBQIhjAAIgSAqQgcgNgfAEIgWAEIgDAwIgHgJIgkARIgUiXIghAAIgShAIAPgLQAMg1gngpQgfgggvgFQgvgFgtAOQAOgtgkggQgOgNgSAAIgfAFQAOgvgQgtIgFgOQhBgXg7AfIgRAKIgKhWIhagDIgRAeQg5gig9AVQgPAGgNAIIgCA6IgOgtQgwgEghAhQgPARgHAWQANg6gdgzQgTgigkgOIgegJIAAhbIhKAGIgKhQQgmgOgpAEQghAGgUAdQgNhNhDgnQgZgOgfAKQgvAQgmAjMAAAggwMB8/AAAMAAAAmVQg3glg7AgIgRAKIgbAAIgVgqQgQABgOALQgMAKgLALIgUggQhaALhLA4QABgXgUgGQh4gmhpBIQgGgrgjgcIgOgKQhAAhgtA2QgKgggMggQgHgSgPgMIgJgGIimA5QhAgohJAWIgRAGIgSBMIgKgzQhogJhWA4QgjAYgUAoQgUAsgHAxQgjgVgogIQgzAFgpAgQgtAjgGA7QgDAcAAAdIhGAMQgPAhAPAiQAIAUAOAMIgbAAIAMB/Ig7AAQg1gxgnhCQgOAAgKAHQgOAMgCARQgxABgQAvQgFASABATQgegHgcALIgMAGIgEBKQgtAigQA4IgJAFIgGgMQhhAEg0BRQgwgogvArQgPAPADAVQADAUALAQIgfgeQgwAGgSAtQgWA9ARA+QgigZgrgLQgUgEgRANQgmAfgJAwQgJA3AwAYQAmATAuADIgXAVIghgEIgFAyIAVAXIgqAAIAABVQgqgagpAiQgmAfAFAzIhAgZQg1AQgLA2QgDAQgBARQgYhMhLgXg");
	this.shape_20.setTransform(-39,-126.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-486.1,-346.5,931.1,722.9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#348101").s().p("Eg+fAKaIAA0zIBMAAIBbA4QBKAtAuAmQgFgfATgVQAOgPAIAAQAMAAABABIARATIANAAQARgjALgNQAOgQARgCIAABJQAFAAArghQAqghAdAAQAmAAANAfQAKAXAZBbIAAAHIADgFQAJgLAFgOQAHgRADgEQANgPBFAAQBQAAAOATQAJANAFBcIAIgDQAGgDAKAAQAMAAAEAHQAOAUANANIANAAIAEgdQADgZAAgTQAaAAAGAHIAbAAQAAhCAGghQAZALAEAWIAMAuIAMAAQADgcAGgpQAFgfgBgNQA1AUAcAvQAXAyAPAQIAOAAQgFgMATguQASgzABgDIAWAbQAUAaAKAAIAzgYQA2gZAOgEIA1AAQAAAhgHAnIAAAHIACgBQAOgFAXgGQAXgGAEgCIAoAAIAABNQALgGAPgFIAigLQAJgCANAAQAEAAApATQApAUAdAAIAAguQALAGAPAOIAwAAQgLgkATgVQAOgPAQAAQACAAAWAKQAUAJACAAQAHAAAIgFQAMgKAHgEIANAAIAAAhQAOAAAngEIAugDQAdAAAoAQQApARBeAAIAAAuQAuAABDAXQBBAWAUAVQAEgiAAgbQgBgzgWgUIAAAAIAnAhQArAjARAFIANAAQgCgbAGgOQAEgJAMgKIATAAIAABWQANAAAagVQAdgXAKgDIAFAVQADATAAAHIAAAHIAEgDIAqggQArgZA4AAIB3AAQgSAkgPARIgHANQAZAABYgeQBYgeAxAAQgHAIhRA2Qg9AqgOAjQA1g2C+gEQDYAFBKAAQAAgLgNgjQgOgiAAgGIADgDQANAMAXAMQAcAOAFAFIAAhVQAYAOAPAqQAOAkAEAAQALAAAXgbQAageArgJIA7AAQAAA7AHAUQgBgXB3gKIEgAAIAAAoQgHAQgOATQgNARgFANQAcgBA7gIQBFgKAhgBQB2gEAJBGIAEAFQADAFAAAEQANgQARgsQAQgnAAgHQAjAAAbAYQAaAXAeAAQABgXAGglQAHgmAAgIQAGAAAAgGQANAKAPAbQAOAbAFAVIAGAEQAGAFAAAFIAAhCIAVAAQADAPAJASQAIAPgBAKQAHAAAAgNIAuAAQAaARAaAEIAfAAQAlAAA1gRQA1gRAVAAIBaAKQBYAJAeABQANAAB9AoQB/AoAMAAIAAADIABAAQAggPAbgCQARgCAUABQAOgDAMgSQANgTAWg6IAOAmQAIAXAMAMQAogfAAg3QAiAAAgAlQAPASALASQAPAEANgKQAbgTgDhDQAGAEAiApQAdghAUgUQAmgnAZAAQAMAIAGAPQAIARAIAHQAQgQAtgJQAtgJAGgGQgOAcAZAeQAWAcAagBIAAgnIAnAAQAaAcAeAIQANADAYAAIAAgtQgJgLgLgVIgUgjIADgFIAnAcQAlAYAHAAIAAhPQAeAJAOARQAIAKAOAXQAFgFAHgLQAHgLAOgNIA6AAQABBCgHAHIAXAAIAMgBQAPgEARgIIB2AAQgQAggVAEQgYAFgLASIB9AAQAJATgJAvIDFAAIAAhWIAcAAIAoANIAZAHQAWAAALgbQAHgTAAgaQAGAAAdAQQAdARAIAAIAggRQAggQAJAAQCHA7ADAAQAIAAAPgHQAPgGAIAAQAKAAA8AkQA8AkAJAAQALAAAFgNQAFgOASAAQAMAABHApQBRAuAPAGIAuAAIAAhCIA8AAIAdAPIAAOzgACXkhIAHAAIAAgBIgHABgEgmigGfIAIAAQAEAAACgDIAAgEIAHAAIAAgIQgPAHgGAIg");
	this.shape.setTransform(0,233.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6BB300").s().p("EA8ZAGIQgPgGhRguQhHgogMAAQgSAAgFANQgFAOgLAAQgJAAg8gkQg8glgKAAQgIAAgPAHQgPAGgIAAQgDAAiHg7QgJAAggARIggAQQgIAAgdgQQgdgRgGAAQAAAagHATQgLAbgWAAIgZgHIgogMIgcAAIAABVIjFAAQAJgvgJgTIh9AAQALgSAYgFQAVgEAQggIh2AAQgRAIgPAEIgMACIgXAAQAHgIgBhBIg6AAQgOAMgHALQgHAMgFAFQgOgYgIgKQgOgRgegJIAABPQgHAAglgYIgngbIgDgCIAAAHIAUAjQALAUAJALIAAAtQgYAAgNgDQgegIgagcIgnAAIAAAnQgaACgWgdQgZgeAOgbQgGAFgtAJQgtAKgQAPQgIgHgIgQQgGgQgMgIQgZAAgmAnQgUAVgdAgQgigpgGgEQADBEgbASQgNAKgPgEQgLgSgPgSQggglgiABQAAA2goAfQgMgMgIgXIgOgmQgWA6gNAUQgMARgOADQgUgBgRACQgbADggAOIgBgDQgMAAh/goQh9gogNAAQgegBhYgIIhagLQgVAAg1ARQg1ARglAAIgfAAQgagEgagQIguAAQAAAMgHAAQABgJgIgPQgJgTgDgPIgVAAIAABCQAAgFgGgFIgGgEQgFgVgOgbQgPgbgNgKQAAAGgGABQAAAHgHAmQgGAlgBAXQgeAAgagXQgbgYgjAAQAAAIgQAnQgRAsgNAPQAAgEgDgFIgEgEQgJhHh2AEQghABhFAKQg7AIgcACQAFgOANgRQAOgTAHgQIAAgnIkgAAQh3AJABAYQgHgUAAg8Ig7AAQgrAJgaAeQgXAcgLgBQgEAAgOgkQgPgqgYgOIAABVQgFgFgcgOQgXgMgNgMIgDgDIAAAHQAAAFAOAiQANAkAAAKQhKABjYgGQi+AFg1A1QAOgjA9gqQBRg2AHgHQgxAAhYAdQhYAegZABIAHgOQAPgRASgkIh3AAQg4AAgrAZIgqAgIgEgEQAAgHgDgTIgFgUQgKACgdAYQgaAUgNAAIAAhVIgTAAQgMAJgEAJQgGAOACAbIgNAAQgRgFgrgjIgnghIgHgGIAHAGQAWAVABAzQAAAbgEAhQgUgUhBgXQhDgXguAAIAAguQheAAgpgRQgogQgdAAIguADQgnAEgOAAIAAghIgNAAQgHAEgMAKQgIAFgHABQgCAAgUgKQgWgKgCAAQgQAAgOAPQgTAWALAjIgwAAQgPgOgLgFIAAAtQgdAAgpgUQgpgTgEAAQgNAAgJACIgiALQgPAFgLAGIAAhNIgoAAQgEADgXAFQgXAGgOAFIgCgFQAHgnAAgiIg1AAQgOAFg2AYIgzAYQgKAAgUgZIgWgcQgBAEgSAyQgTAvAFALIgOAAQgPgPgXgzQgcgvg1gTQABAMgFAfQgGApgDAcIgMAAIgMguQgEgWgZgLQgGAhAABCIgbAAQgGgHgaABQAAASgDAZIgEAdIgNAAQgNgNgOgTQgEgIgMAAQgKAAgGAEIgIADQgFhdgJgNQgOgThQAAQhFABgNAPQgDADgHARQgFAOgJALIgDgCQgZhbgKgWQgNgfgmAAQgdAAgqAgQgrAhgFAAIAAhHQgRACgOAPQgLANgRAiIgNAAIgRgTQgBgBgMAAQgIAAgOAQQgTAVAFAeQgugmhKgtIhbg3IhMAAIAAkxQAWgFAOAAQASABAPAHQANAIAbAYQAFgJAGgLQAIgPAAgFIAbAAIAaAoQANgbAHgGIAbAAQAAAHAKAoQAKAoAAAEQAEgJALg5QALg5gBgPQAcAQAHATQAFALAAAUQALgDAOgNIAPgQIAGgGIAABUQANgWAigSIBBggIBPAAIAAAoQANgBASgNQAXgPANgEIAnAAQAAAHAIAGIghAcQgZAagCASIAAAGIADgEQAVgeAPgEIEPAAQgYApgkAfQAbAAAvgKQAogIAYgJIAABWQABgDAGgLQAHgLAAgIQANABAAgIQALAMAVASQAbAXAHAAIAAgnQAGAJARALQASALAMABIAAgnIAnAkQAgAdAPAIIAAgbQgCgQgEgPQgIgegUgYIAAhpQAaAMAfA8QAeBEAMAYIAHAHQAAgeADgrIAEg0QAcAMAVAmIAQAjIAPAAIAAhVQAGgBAAgGQATAOAPAaIASAgIAOAHQAAgLADgbQAFgdAEgTQAYALARAoQASAyAVASIANAAIgBgsQAAgPAPgUICDAAIAAAnQACAmgcAcQALgFA3gEQAzgEAfAAQB/AABPAeQCXA4CNAhQAAgiAGgTIAbAAQAeAHAGAGIgCAHQAIgDgGgEQALgYAGgIQAKgOAMAAQAWAAATAKQATAKAHAAQADAAAsAVQAwAYAHAHQAFgmgOgPQgIgHg6gjIgBAEQgGAMgLAAQgJAAgGgKQgEgIABgHQgBgbAUAAQAQABACAVQCDANAvAfQAYAPAPAaQAHAKAVAwIAHAAIAAgBQAAgggbgtIghgwIBJAAQBAAVAoAbIAjAZIAAgvQAaAVAJAdQAFAPgBAVQA0gTAIgUIANAAIAAAuQAGgNAAgVQAHAAAKgCQAKgEAHAAQgBAHAEAQIABAHIABgHQAHggA2gKQBRgDALgEIAGABQgBAUgUAbIgTAcIABAEIAagHIBpAAQAFAHADAUIAHAeQAFABANgIQATgKAjgIIAbAAQAAA0AGAUQALgNBDAAIAAgoQAPAEAlAWQAiAUAIAHQAAgNgOhbIgNhgIACgCIADgDIAAgBQAEAXAeA3QAUAmAaArIAQgRQALgNgBgOIAbAAQAHAYAEAKQAFANALALQAAgEAGgKQAHgJgBgDIAKALQAHAIAKAAQARgKAVgJQArgTAgAAIAABjQANgOAqgWQA7ghAdgSIBIAAQAABdgHATQBkggBZgSQBNgQAsAAQAXABAHAFQAEAEANARIAAhCQAGAAAAgNQAjAWAEAbQACAYAZANIAAgoQAOAHAMALQAIAIAIAAQAFAAADgGQAHgLAIgFQAfgcBYgWIAoAAIAABWQANgYA/gOQAtgKAfABIAAAhQgEAYgGAKQgCAFgFAFIAIgGQAPgJAbgMQAsgRAbAAQAMAAABAEQABADAAANQADAkAyAyQASgRAqgpIAjgiQASAAApARQA+AaBRA3IAOAAIAAiEQAVAAASAUQAXAYAKADIAAhWQACgKAKAUIAOAdQAbgnAKgKQAOgPAVAAQAPAAANASQAMAUAEgBQABAABOgfQACAAAmAQQAnATArADQBFAGBBgCIAtgCQACgYAEgmIAFg0IABABIAHAGQARgTAQgwQANgoAAgLQAXAKAFASQADAIABAbQAAAqA2AHQgDgOANgkQAQgnAVgXIAABcQACABAGADQAFADAKAAQABAAAngSQApgTANgCIAABpQBRAAA/gqQAmgaAIgEQAagNAdAAIgCBIQAAAgAcAnIAbAAIAAhiQABAsAGAvQAFAAAEAFQADAEAAAGIAHAAQAAhcAIgVIAPAgQAMAggIAPIAOADQALAEAIAAQAnhXAAgFIAHAAIADAkQADAfAAASIAKAGQACABALAAIA8geQA8ggAJgDIAABvQAEgIAigMQA7gUAPgGIBCAAIAAB9QAMgQAxgEIA5AAQAviBAAgIQAEAVANAYQAMAXALAKIAAgiQgHgZgHgTQgFgQgBgIQAEAPAwAvIAGAAQABhvAGgTQAXALAKARIANAZIApAAQgHgdANgWQAEgHAQgPQAdASAaAlQATAdASABQgLg5AUgQQAHgGAYAAQAkABBKAxQBLAxAFAAQAfAAAEgEQADgGAWhLQgMA3A+AvQA3ApAyAAIAHAAIAAE4IgdgOIg8AAIAABBgAuNgjIAAgHIgCgKQAAAHACAKg");
	this.shape_1.setTransform(0,171.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#7ED200").s().p("AV8IFQhRg3g+gaQgpgRgSAAIgjAiQgqApgSAQQgygygDgjQAAgNgBgDQgBgEgMAAQgbAAgsARQgbALgPAKIgBgEQAGgLAEgXIAAgiQgfAAgtAKQg/AOgNAYIAAhXIgoAAQhYAWgfAcQgIAGgHAKQgDAHgFAAQgIAAgIgIQgMgLgOgHIAAAoQgZgNgCgYQgEgbgjgXQAAANgGAAIAABDQgNgRgEgEQgHgGgXAAQgsAAhNAQQhZAShkAgQAHgTAAhdIhIAAQgdASg7AgQgqAXgNANIAAhjQggAAgrATQgVAKgRAKQgKAAgHgIIgKgMQABAEgHAJQgGAKAAADQgLgKgFgNQgEgKgHgZIgbAAQABAOgLAOIgQARQgagsgUgmQgeg3gEgXIAAAAIAAAAIgDADIgCACIANBgQAOBdAAANQgIgHgigUQglgWgPgEIAAAoQhDAAgLANQgGgUAAg1IgbAAQgjAJgTAKQgNAIgFgBIgHgfQgDgUgFgIIhpAAIgaAHIgBgEIATgbQAUgbABgUIAAgBIgGAAQgLADhRAEQg2AJgHAhIgCgBQgEgPABgHQgHAAgKADQgKADgHAAQAAAVgGANIAAgvIgNAAQgIAVg0ATQABgVgFgPQgJgdgagVIAAAuIgjgZQgogahAgVIhJAAIAhAwQAbAsAAAhIgHAAQgVgvgHgKQgPgagYgQQgvgeiDgNQgCgVgQgBQgUAAABAbQgBAHAEAHQAGALAJAAQALAAAGgMIABgEQA6AiAIAIQAOAPgFAmQgHgIgwgXQgsgVgDAAQgHAAgTgKQgTgLgWAAQgMAAgKAPQgGAIgLAXQgGgFgegHIgbAAQgGATAAAiQiNghiXg4QhPgeh/AAQgfAAgzAEQg3AEgLAFQAcgcgCgmIAAgoIiDAAQgPAVAAAPIABAsIgNAAQgVgTgSgxQgRgpgYgKQgEATgFAdQgDAbAAALIgOgHIgSggQgPgagTgOQAAAGgGAAIAABWIgPAAIgQgkQgVgmgcgMIgEA1QgDArAAAdIgHgGQgMgYgehFQgfg7gagMIAABpQAUAYAIAdQAEAQACAQIAAAbQgPgIgggeIgngjIAAAnQgMgBgSgLQgRgLgGgJIAAAnQgHAAgbgXQgVgSgLgMQAAAHgNAAQAAAIgHALQgGALgBADIAAhWQgYAIgoAJQgvAKgbAAQAkgfAYgqIkPAAQgPAEgVAeIgDgBQACgTAZgZIAhgcQgIgGAAgGIgnAAQgNAEgXAOQgSAMgNABIAAgmIhPAAIhBAeQgiATgNAWIAAhUIgGAHIgPAQQgOAMgLADQAAgUgFgKQgHgTgcgQQABAPgLA4QgLA4gEAKQAAgEgKgoQgKgnAAgHIgbAAQgHAGgNAZIgagmIgbAAQAAAFgIANQgGAMgFAIQgbgXgNgHQgPgIgSAAQgOAAgWAFIAAn5QB5AxBvBBIErCvIAAhPIApAAQAVACAeAXQAdAVAMAAIAAgoQAJADARAHQAQAHAEADQAGAAAAgUIAhAAQESBeBcAaQCFAlD2AIQDogBBdAGQCdAMAbBGQAAgEADgKIADgNIBEAoQApAXAxAJQAHgaAAg1IBCAAIAlANIAHACQACAFAAAFIAFgIIArAPQArAOAPABIAAA6QAggbAJgaQAFgPAAglQAHgGAAgIQAZAOgBARQAAAWAIAIQgCASAIgRIANgjQABgGAGAAIAABCQAdgKAMgbIADgKIADAAQAPgOAHgKQAEgHAAgJIAGAAQAWAlgBAgIAAAeQAFAHAlAAICUgBICbABQAnAAAEgIIgBgiQAAgjAWgqQAEAAACgEIAGgJQANALAMAsQAIAlABAOIAAANIACgMIAFgTQAGgSAGgKQAGgNABgHQAAAbAMANIAAgbQACAAADgDQADgDAJAAQANAAAtAaIAsAbQAJAAAEgDQAEgGAKgZQA3BQBSAjQBBAcBfAFICngBQBhAAA7ARIgdglQgOgQgDgGICRBRIBNAsIAHAAQgThAgmgfQgqgZgSgSIAaAAQAOAFCRBHQCRBGARAGIgngjQgXgTgLgGQABgNgHAAIAXAAQCoAhA0AaQAVAKAEABQAMADAlAAQEEAADagyQBtgZJeivIAHABQgDASgIANQgIAKgOALQAAALgGABIAaAAQAIAABWgWQBWgXALAAQAAAzgGAAIAAAHIACgDQAcgcCTgKQBNgFBMABQADAABTAbQBTAZBEAAIAOAGIgNgJIgOgLIAAgGQANAABBAdQBEAeATAAIAAgnIB+AaIA0AAQAGgNAAgnQANAABEATQA9ASAdABQAAgugGgSIDhAAQD9AABJACQCFAFBnARQBYANCrAyIAAEzIgHAAQgyAAg3gqQg+gvAMg4QgWBMgDAFQgEAFgfAAQgFAAhLgxQhKgygkAAQgYAAgHAGQgUAPALA6QgSgBgTgdQgaglgdgSQgQAPgEAHQgNAVAHAdIgpAAIgNgZQgKgRgXgLQgGAUgBBwIgGAAQgwgwgEgPQABAIAFAQQAHATAHAaIAAAiQgLgKgMgXQgNgZgEgVQAAAHgvCDIg5AAQgxADgMARIAAh+IhCAAQgPAGg7AVQgiALgEAJIAAhwQgJADg8AgIg8AeQgLAAgCgBIgKgFQAAgTgDgfIgDgkIgHAAQAAAFgnBXQgIAAgLgDIgOgDQAIgQgMggIgPggQgIAVAABcIgHAAQAAgFgDgFQgEgEgFAAQgGgwgBgtIAABkIgbAAQgcgoAAggIAChJQgdAAgaAOQgIAEgmAZQg/ArhRAAIAAhpQgNACgpATQgnASgBAAQgKAAgFgDQgGgEgCAAIAAhcQgVAWgQAoQgNAjADAPQg2gHAAgrQgBgagDgIQgFgSgXgKQAAALgNAoQgQAwgRATIgHgGIAAgXIgBAWIgFA0QgEAngCAXIgtADQhBAChFgGQgrgDgngTQgmgRgCAAQhOAggBAAQgEAAgMgTQgNgUgPAAQgVAAgOAQQgKAKgbAoIgOgdQgKgVgCAKIAABXQgKgEgXgXQgSgUgVAAIAACEg");
	this.shape_2.setTransform(0,137.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#99FF00").s().p("AHRHEQgEgBgVgJQg0gaioghIgXAAQAHAAgBANQALAGAXATIAnAjQgRgHiRhGQiRhGgOgFIgaAAQASARAqAaQAmAeATBAIgHAAIhNgrIiRhSQADAHAOAQIAdAlQg7gRhhAAIinAAQhfgEhBgcQhSgkg3hQQgKAZgEAGQgEACgJAAIgsgaQgtgbgNAAQgJAAgDAEQgDADgCAAIAAAbQgMgNAAgbQgBAHgGANQgGAKgGASIgFASIgCAAQgBgPgIgkQgMgtgNgKIgGAJQgCAEgEAAQgWAqAAAjIABAiQgEAJgnAAIibgBIiUABQglAAgFgIIAAgeQABgggWglIgGAAQAAAIgEAIQgHAKgPAOIAAgdQAAAQgDAMIgDALQgMAagdALIAAhDQgGAAgBAHIgNAjIgGgBQgIgIAAgWQABgRgZgOQAAAIgHAGQAAAlgFAOQgJAaggAcIAAg6QgPgBgrgPIgrgOQATghA7g0QBMhBAagZQAJAIADAqQABAZAAA4IAAANQACgVAaglIAmgwIAaAAIAABQIAOAAQACgVAGgSQAKgkAUgRIAaAAQANA0AAB4QA1gKBagDQA1gCBsAAQgIgiAphDQAsg/ADgHQAOASAMAbQANAeAAASIANAAQAAgGAHgbQAGgbAAgHQAGABAIgHQAGgGAIAAIAABPIBBhDIAcAAIAABqQAwAADfA1QBwAaBoAbQAAgLgHgeQgGgdAAgXIAAgNIA5AsQA2ApAIAIQAPgPAAgLIgDgNQAfAGAwAbQAkATARANIANAAQAAg0gNAAIAAgNQAeAABjA+QBpBBANADQAiAIC1gGQCXgFAFAQQDBgFCtggQCVgcC3g6QDrhNAogJQCWgoCIAAQBlAACCAhQBAAQAwAQQAWAAAAgCQgHgNgJgYQANAABrAhQBZAbAXAEIgBgCQgNgmgGgLIAAgNQAcAEBNAhQAoASA1AZIAAhdICrAAQBEATD6AJIIPALQBqACDbgJIAwgBIAACiQirgxhYgPQhngQiFgFQhJgCj9AAIjhAAQAGASAAAvQgdgCg9gRQhEgUgNAAQAAAngGANIg0AAIh+gaIAAAoQgTAAhEgeQhBgdgNgBIAAAAIAAAHIAOALIgBACQhEAAhTgaQhTgagDAAQhMgBhNAFQiTAJgcAdIgCgDQAGAAAAg0QgLAAhWAXQhWAXgIgBIgaAAQAGAAAAgMQAOgLAIgLQAIgMADgSIAAgBIgHAAQpeCwhtAZQjaAxkEABQglAAgMgEgAIrFxIAmAAIAAgBIgmABgEAmUADSIgNAAIgGgBQALAaAIgZgA+CDoIhEgpIgDAOQgDAKAAADQgbhFidgMQhdgHjoACQj2gJiFgkQhcgakShdIghAAQAAATgGABQgEgEgQgHQgRgGgJgDIAAAnQgMAAgdgVQgegWgVgCIgpAAIAABNIkritQhvhBh5gxIAAjPQJHE+G8CCQGzB+HoAAQAiAAAWAnQAQAbAbAIIAAAEIAXAAQgNAAgKgEIAAhYIA1AAQAQAVAXAFQAEABAjAAQAxgIAfgTIAHAUQAGAbAAAgIAGAIIgCAGIglgNIhCAAQAAA1gHAaQgxgJgpgXg");
	this.shape_3.setTransform(0,110.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-400,65.2,800,234.9), null);


(lib.CorrectAnicopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(11));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ak7E8QiCiDAAi5QAAi4CCiDQCDiDC4AAQC4AACDCDQCDCDAAC4QAAC5iDCDQiDCDi4AAQi4AAiDiDg");
	this.shape.setTransform(-0.6,0.4,0.987,0.987);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(11));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0099FF").s().p("Ah6ABIAAgGID1AAIAAALg");
	this.shape_1.setTransform(0.6,-55.3,1,1,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0099FF").s().p("AijA2QBShxAAiLID1AAQAADbiACyg");
	this.shape_2.setTransform(20,-51.2,1,1,-90);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0099FF").s().p("AkPBYQCFgsBShxQBRhxAAiMID3AAQAADciCCyQiBCyjPBFg");
	this.shape_3.setTransform(32.3,-40.5,1,1,-90);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0099FF").s().p("Am6EyIBMjrQCFArCFgsQCEgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihpAAQhoAAhogig");
	this.shape_4.setTransform(34,-23.4,1,1,-90);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0099FF").s().p("AkREyQjQhEiCizIDHiQQBSBwCFArQCFAsCEgsQCFgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihoAAQhoAAhpgig");
	this.shape_5.setTransform(34,-6.5,1,1,-90);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0099FF").s().p("AndCMQjHjFABkZID1AAQABCyB9B/QB/B9CxAAQCxAAB/h9QB9h/ABiyID2AAQAAEZjGDFQjGDIkYgBQkXABjGjIg");
	this.shape_6.setTransform(34,0,1,1,-90);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0099FF").s().p("AmtF/QjPisgikJQgikKCejaIDHCRQhkCLAVCpQAWCoCDBtQCEBtCogLQCpgKB1h9QB1h9AAipID3AAQAAELi4DEQi4DFkKARQgYABgXAAQjsAAi9icg");
	this.shape_7.setTransform(14,0,1,1,-90);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0099FF").s().p("AmMIUQjSiZg2j/Qg2j+CBjhQCCjiD3hRIBMDsQidAzhTCPQhSCQAiChQAiCiCGBhQCGBiCjgSQCkgRBuh7QBvh7AAilID3AAQAAEEiuDBQiuDCkCAbQgnAFglAAQjVAAiyiDg");
	this.shape_8.setTransform(1.7,0,1,1,-90);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0099FF").s().p("AlyI4QjUiNhEj1QhEj0BujmQBvjnDphkQDqhkDwBPIhNDrQiYgyiUA/QiUBAhHCTQhHCSAsCbQArCcCHBZQCHBZCegWQCggWBqh6QBqh6AAihID3AAQAAD+inDAQinDAj7AiQgyAHgwAAQjDAAiphwg");
	this.shape_9.setTransform(-0.1,0,1,1,-90);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0099FF").s().p("AlgJDQjViDhNjuQhNjtBgjoQBfjnDehyQDfhxDxA6QDzA6CTDLIjICSQhdiBibglQiZgliNBIQiNBIg9CTQg9CTAxCWQAxCYCHBTQCIBTCbgZQCdgYBnh6QBnh5AAifID3AAQAAD7iiC+QiiC/j2AnQg5AJg3AAQi3AAikhlg");
	this.shape_10.setTransform(0,0,1,1,-90);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0099FF").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_11.setTransform(0,0,1,1,-90);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).wait(1));

	// Layer_2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_12.setTransform(0,-0.1,0.99,0.99,-90);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-67.7,134.5,134.6);


(lib.AttemptsAni = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop()
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(11));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ak7E8QiCiDAAi5QAAi4CCiDQCDiDC4AAQC4AACDCDQCDCDAAC4QAAC5iDCDQiDCDi4AAQi4AAiDiDg");
	this.shape.setTransform(-0.6,0.4,0.987,0.987);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(11));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6666FF").s().p("Ah6ABIAAgGID1AAIAAALg");
	this.shape_1.setTransform(0.6,-55.3,1,1,-90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6666FF").s().p("AijA2QBShxAAiLID1AAQAADbiACyg");
	this.shape_2.setTransform(20,-51.2,1,1,-90);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#6666FF").s().p("AkPBYQCFgsBShxQBRhxAAiMID3AAQAADciCCyQiBCyjPBFg");
	this.shape_3.setTransform(32.3,-40.5,1,1,-90);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#6666FF").s().p("Am6EyIBMjrQCFArCFgsQCEgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihpAAQhoAAhogig");
	this.shape_4.setTransform(34,-23.4,1,1,-90);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#6666FF").s().p("AkREyQjQhEiCizIDHiQQBSBwCFArQCFAsCEgsQCFgrBShwQBShyAAiMID3AAQAADdiCCxQiBCzjQBEQhpAihoAAQhoAAhpgig");
	this.shape_5.setTransform(34,-6.5,1,1,-90);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#6666FF").s().p("AndCMQjHjFABkZID1AAQABCyB9B/QB/B9CxAAQCxAAB/h9QB9h/ABiyID2AAQAAEZjGDFQjGDIkYgBQkXABjGjIg");
	this.shape_6.setTransform(34,0,1,1,-90);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#6666FF").s().p("AmtF/QjPisgikJQgikKCejaIDHCRQhkCLAVCpQAWCoCDBtQCEBtCogLQCpgKB1h9QB1h9AAipID3AAQAAELi4DEQi4DFkKARQgYABgXAAQjsAAi9icg");
	this.shape_7.setTransform(14,0,1,1,-90);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#6666FF").s().p("AmMIUQjSiZg2j/Qg2j+CBjhQCCjiD3hRIBMDsQidAzhTCPQhSCQAiChQAiCiCGBhQCGBiCjgSQCkgRBuh7QBvh7AAilID3AAQAAEEiuDBQiuDCkCAbQgnAFglAAQjVAAiyiDg");
	this.shape_8.setTransform(1.7,0,1,1,-90);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#6666FF").s().p("AlyI4QjUiNhEj1QhEj0BujmQBvjnDphkQDqhkDwBPIhNDrQiYgyiUA/QiUBAhHCTQhHCSAsCbQArCcCHBZQCHBZCegWQCggWBqh6QBqh6AAihID3AAQAAD+inDAQinDAj7AiQgyAHgwAAQjDAAiphwg");
	this.shape_9.setTransform(-0.1,0,1,1,-90);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#6666FF").s().p("AlgJDQjViDhNjuQhNjtBgjoQBfjnDehyQDfhxDxA6QDzA6CTDLIjICSQhdiBibglQiZgliNBIQiNBIg9CTQg9CTAxCWQAxCYCHBTQCIBTCbgZQCdgYBnh6QBnh5AAifID3AAQAAD7iiC+QiiC/j2AnQg5AJg3AAQi3AAikhlg");
	this.shape_10.setTransform(0,0,1,1,-90);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#6666FF").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_11.setTransform(0,0,1,1,-90);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).wait(1));

	// Layer_2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AndHgQjHjHABkZQgBkYDHjHQDGjHEXAAQEYAADGDHQDGDHAAEYQAAEZjGDHQjGDHkYAAQkXAAjGjHgAkwkwQh9B/gBCxQABCyB9B/QB/B+CxAAQCxAAB/h+QB9h/ABiyQgBixh9h/Qh/h+ixgBQixABh/B+g");
	this.shape_12.setTransform(0,-0.1,0.99,0.99,-90);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.2,-67.7,134.5,134.6);


(lib.result_option2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// txt
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#660000").s().p("AhqBxQgrgtAAhEQAAhCArgtQAtgvBEAAQBFAAAtAvQAsAtAABCQAABEgsAtQgtAuhFAAQhEAAgtgugAhRhYQghAjAAA1QAAA2AhAjQAiAkA2AAQA3AAAjgkQAggjAAg2QAAg1gggjQgjgkg3AAQg2AAgiAkgAnGBxQgogtAAhEQAAhDAogsQAqgvBEAAQA0AAAlAbQAkAaAQAsIglAAQgMgdgagRQgZgRghAAQghAAgWAKQgWAKgPAQQgfAjAAA1QAAA3AfAiQAgAkA8AAQAhAAAZgRQAagRAMgcIAlAAQgQArgkAaQglAbg0AAQhEAAgqgugAsGCEQgfgZgEgsIAlAAQACAfAdARQAZAPAqAAQBXAAAAg3QAAgTgQgLQgPgKgYgHQgYgHgbgHQgcgGgXgIQgYgKgQgQQgPgPAAgbQAAgmAdgYQAdgYA6AAQA8AAAgAbQAdAYACAmIgkAAQgFgngtgMQgPgFgYAAQgYAAgQAFQgQAGgJAHQgJAIgDAKQgDAJAAALQAAAKAJAJQAJAJAPAGQAPAGATAFIAoAKQATAEAUAHQATAFAPAKQAhAVAAAkQAAAsggAXQghAXg6AAQhBAAgigbgAJGCbIAAk1IDkAAIAAAiIjBAAIAABoICsAAIAAAhIisAAIAABpIDBAAIAAAhgAHOCbIhLh3IhsAAIAAB3IgjAAIAAk1ICVAAQAvAAAdAaQAcAaAAAtQAAA5gwAVQgOAGgNADIBSB9gAEXADIBsAAQAyAAASgeQAHgMAAgSQAAgegUgRQgUgRghAAIhuAAg");
	this.shape.setTransform(-6.4,51.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000033").s().p("AFbBfQgYgaABgnQgBglAYgaQAYgaAnAAQAfAAAWAQQAWAQAIAaIgcAAQgHgQgNgJQgNgJgRAAQgQAAgMAFQgMAFgIAJQgRASAAAcQAAAdARASQARAUAfAAQARAAANgJQANgKAHgPIAcAAQgIAagWAQQgWAQgfAAQgnAAgYgagACJBfQgYgaAAgnQAAgmAXgZQAYgaAoAAQAoAAAYAaQAXAZAAAmIAAALIiTAAQADAYARAQQAQAQAaAAQAkAAAPgdIAdAAQgJAXgUAPQgUAPgfAAQgoAAgZgagACggUQgQAOgDAWIB3AAQgEgWgPgOQgRgPgYAAQgYAAgQAPgAlbBfQgWgaAAgnQAAgmAWgZQAZgaAnAAQAoAAAZAaQAWAZAAAmQAAAngWAaQgZAagoAAQgnAAgZgagAlIgQQgPASAAAcQAAAdAPASQARAUAcAAQAdAAAQgUQARgSgBgdQABgcgRgSQgQgTgdAAQgcAAgRATgApSBWQgeghgBgzQABgyAeghQAfgjAzAAQAnAAAcAUQAbATAMAhIgcAAQgJgVgTgNQgTgNgZAAQgZAAgQAIQgRAHgLAMQgXAaAAAoQAAApAXAZQAYAbAtAAQAZAAATgMQATgNAJgVIAcAAQgMAhgbATQgcAUgnAAQgzAAgfgjgAIvB2IAAiXIgkAAIAAgYIAkAAIAAg/IAaAAIAAA/IAoAAIAAAYIgoAAIAACXgAgKB2IAAivIAZAAIAAAsQAIgcAdgNQALgFARAAIABAAIAAAaIgDAAQgcAAgTAXQgQAVAAAeIAABNgAiSB2IAAivIAbAAIAAAsQAHgcAdgNQAMgFAQAAIACAAIAAAaIgDAAQgdAAgSAXQgQAVAAAeIAABNg");
	this.shape_1.setTransform(369,328.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000033").s().p("AEYCaIAAj0IAaAAIAAAcQAVgfAqAAQAlAAAYAaQAYAbgBAmQABAlgYAaQgYAagnAAQgoAAgVgfIAABigAFAgxQgPASAAAdQAAAbAPATQARAUAcAAQAdAAARgUQAQgTAAgbQAAgdgQgSQgRgUgdAAQgcAAgRAUgAJ7BGQgSgPgCgaIAbAAQADAZAZAIQAJADAJAAQAJAAAHgBQAJgBAHgDQARgGAAgRQAAgLgJgFQgIgGgOgEQgNgEgPgDQgPgDgNgEQgNgFgJgJQgJgJABgRQAAgYARgNQARgNAiAAQAhAAASAQQAQANACAVIgbAAQgDgbgpAAQgoAAAAAZQAAALAJAFQAIAFAOAEIAbAGQAQADANAGQANAFAJAJQAIAJAAARQAAAagUANQgTANggAAQgkAAgVgRgAjsA9QgYgaAAglQAAgnAXgaQAZgaAnAAQAnAAAZAaQAXAaAAAnIAAAJIiTAAQADAZAQAQQARAQAbAAQAjAAAPgeIAdAAQgJAYgUAPQgUAOgfAAQgoAAgZgagAjWg2QgPAPgDAXIB3AAQgEgXgPgPQgRgPgYAAQgYAAgRAPgAIGBVIAAiYIgkAAIAAgXIAkAAIAAg/IAaAAIAAA/IApAAIAAAXIgpAAIAACYgADDBVIAAhpQAAgVgMgPQgNgNgSAAQgVAAgMANQgPANAAAYIAABoIgbAAIAAhpQAAgVgMgPQgMgNgTAAQgUAAgNANQgNANAAAYIAABoIgbAAIAAivIAbAAIAAAbQAQgeAkAAQASAAAPAJQAOAKAJARQAMgXAagJQAJgEANAAQANAAAMAFQALAFAKAKQATAVAAAgIAABpgAljBVIAAiYIgkAAIAAgXIAkAAIAAg/IAaAAIAAA/IApAAIAAAXIgpAAIAACYgAnkBVIAAiYIgkAAIAAgXIAkAAIAAg/IAaAAIAAA/IAoAAIAAAXIgoAAIAACYgAoxBVIgchEIh0AAIgcBEIgdAAIBjjoIAhAAIBjDogAq2gGIBfAAIgvhyg");
	this.shape_2.setTransform(120.9,331.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000033").s().p("AqvCFQgWgOgMgYQgygCgggjQgfghAAgwQAAgzAhghQAhgjA0AAQA0AAAiAjQAgAhAAAzQAAArgbAhQgaAhgrAHQAKAUAZAJQAJADAMAAQAXAAAUgQIAOATQgZAUggAAQgaAAgXgPgAsPhaQgZAbAAAoQAAAnAZAaQAaAbApAAQAqAAAZgbQAZgaAAgnQAAgogZgbQgZgbgqAAQgpAAgaAbgALEBNQgTgPgCgZIAcAAQACAYAZAJQAJADAJAAQAJAAAIgCQAIgBAHgCQARgHAAgQQAAgLgJgGQgIgGgNgDQgNgEgQgDQgPgDgNgFQgNgFgJgJQgIgJAAgQQAAgYARgNQASgNAiAAQAgAAATAPQAPANADAWIgbAAQgEgcgoAAQgpAAAAAZQAAALAJAFQAIAGAOAEIAcAGQAPADANAEQANAGAJAJQAIAKAAAQQAAAagTAOQgTANghAAQgkAAgUgSgAEeBFQgXgaAAgnQAAgmAXgZQAZgaAnAAQAoAAAYAaQAXAZAAAmQAAAngXAaQgYAagoAAQgnAAgZgagAExgqQgPATAAAbQAAAdAPASQARAUAcAAQAdAAAQgUQAQgSAAgdQAAgbgQgTQgQgTgdAAQgcAAgRATgAh0BNQgTgPgBgZIAbAAQADAYAZAJQAJADAJAAQAJAAAHgCQAIgBAIgCQAQgHAAgQQAAgLgIgGQgJgGgNgDQgNgEgPgDQgPgDgNgFQgNgFgJgJQgJgJAAgQQAAgYASgNQARgNAiAAQAhAAASAPQAPANACAWIgaAAQgDgcgpAAQgoAAAAAZQAAALAIAFQAJAGANAEIAcAGQAQADANAEQANAGAHAJQAJAKAAAQQAAAagTAOQgTANghAAQgjAAgVgSgAlBBFQgYgaAAgnQAAgmAXgZQAZgaAnAAQAoAAAYAaQAXAZAAAmIAAALIiTAAQADAYARAQQAQAQAbAAQAjAAAQgdIAcAAQgJAXgUAPQgUAPgfAAQgoAAgZgagAkqguQgQAOgDAXIB3AAQgDgXgQgOQgQgPgZAAQgYAAgQAPgAn5BaQgMgFgKgKQgWgVAAgjIAAhmIAbAAIAABlQAAAYANAPQAOAOAWAAQAWAAAPgPQAQgPAAgXIAAhlIAaAAIAACvIgaAAIAAgbQgKAMgNAJQgOAJgRAAQgSAAgNgFgAJnBcIAAhkQAAgZgOgOQgOgOgVAAQgXAAgPAOQgPAPAAAYIAABkIgbAAIAAivIAbAAIAAAcQAJgNAOgJQANgIASAAQASAAAMAFQANAFAJAJQAWAWAAAjIAABlgAC8BcIAAivIAaAAIAACvgABOBcIAAiXIgkAAIAAgYIAkAAIAAg/IAaAAIAAA/IAoAAIAAAYIgoAAIAACXgAC7hwIAAgjIAcAAIAAAjg");
	this.shape_3.setTransform(-132.4,331.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000033").s().p("Ap7CaIAAjzIAaAAIAAAcQAVgfAqAAQAmAAAXAaQAYAaAAAmQAAAlgYAaQgXAagoAAQgnAAgWgfIAABigApSgxQgQATAAAcQAAAcAQASQAQATAcABQAcgBASgTQAQgSAAgcQAAgcgQgTQgSgUgcABQgcgBgQAUgARdA9QgYgZAAgmQAAgnAXgZQAZgaAnAAQAnAAAZAaQAXAZAAAnIAAAJIiTAAQADAZAQAQQARAQAaAAQAjAAAQgeIAdAAQgKAYgTAPQgUAOgfAAQgoAAgZgagARzg1QgPAOgEAXIB3AAQgDgXgPgOQgRgQgYABQgYgBgRAQgADNA9QgYgZAAgmQAAgnAXgZQAZgaAnAAQAnAAAZAaQAXAZAAAnIAAAJIiTAAQADAZAQAQQARAQAaAAQAjAAAQgeIAdAAQgKAYgTAPQgUAOgfAAQgoAAgZgagADjg1QgPAOgEAXIB3AAQgDgXgPgOQgRgQgYABQgYgBgRAQgAASBGQgSgPgCgZIAbAAQACAXAZAJQAJADAJAAIARgBQAHgBAIgDQAQgHAAgPQAAgMgIgFQgJgGgNgEIgcgHQgPgCgNgFQgNgFgJgJQgIgJAAgQQAAgYARgNQARgNAiAAQAhAAASAPQAQANACAWIgbAAQgDgcgpAAQgoAAAAAaQAAAKAJAFQAIAGANADIAcAGQAQADANAGQAMAFAJAJQAJAKAAAQQAAAZgUAOQgTANghAAQgjAAgUgRgAmRA9QgXgZAAgmQAAgnAXgZQAZgaAnAAQAnAAAZAaQAXAZAAAnQAAAmgXAZQgZAagnAAQgnAAgZgagAl+gxQgQATAAAcQAAAcAQASQARATAcABQAcgBARgTQAQgSAAgcQAAgcgQgTQgRgUgcABQgcgBgRAUgAsqBGQgTgPgBgZIAbAAQADAXAYAJQAJADAJAAIARgBQAIgBAHgDQARgHAAgPQAAgMgJgFQgIgGgNgEIgcgHQgQgCgNgFQgNgFgIgJQgJgJAAgQQAAgYASgNQARgNAiAAQAgAAATAPQAPANADAWIgbAAQgEgcgoAAQgoAAAAAaQAAAKAIAFQAJAGANADIAcAGQAPADANAGQANAFAIAJQAJAKAAAQQAAAZgTAOQgTANghAAQgjAAgVgRgAv2A9QgYgZAAgmQAAgnAXgZQAZgaAnAAQAnAAAZAaQAXAZAAAnIAAAJIiTAAQADAZAQAQQARAQAaAAQAjAAAQgeIAdAAQgKAYgTAPQgUAOgfAAQgoAAgZgagAvgg1QgPAOgEAXIB3AAQgDgXgPgOQgRgQgYABQgYgBgRAQgAP8BUIAAhoQAAgVgMgOQgNgNgTAAQgTgBgOANQgOANAAAYIAABnIgaAAIAAhoQAAgVgMgOQgNgNgTAAQgUgBgNANQgOANAAAYIAABnIgaAAIAAitIAaAAIAAAaQASgdAjAAQASAAAPAJQAPAJAIARQAMgWAZgJQAKgEANAAQANAAAMAFQALAFAJAJQAUAVAAAgIAABogAK8BUIAAitIAaAAIAACtgAIpBUIAAjNIhRAAIAAgZIC8AAIAAAZIhRAAIAADNgAhKBUIAAhjQAAgZgOgOQgNgOgWAAQgWAAgPAOQgQAPAAAYIAABjIgaAAIAAitIAaAAIAAAbQAKgNANgJQAOgIARAAQASAAANAFQAMAFAKAJQAVAVAAAkIAABkgAxPBUIg4hYIhRAAIAABYIgaAAIAAjmIBvAAQAjAAAWATQAVATAAAiQAAAsgkAQQgKAEgKACIA9BcgAzYgdIBQAAQAmAAAOgWQAFgJAAgOQAAgWgPgNQgPgNgZABIhSAAgAK7h3IAAgiIAcAAIAAAig");
	this.shape_4.setTransform(-381.4,330.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// values
	this.cTxt = new cjs.Text("", "50px 'Questrial'", "#0099FF");
	this.cTxt.name = "cTxt";
	this.cTxt.textAlign = "center";
	this.cTxt.lineHeight = 46;
	this.cTxt.lineWidth = 121;
	this.cTxt.parent = this;
	this.cTxt.setTransform(367.1,191);

	this.aTxt = new cjs.Text("", "50px 'Questrial'", "#0099FF");
	this.aTxt.name = "aTxt";
	this.aTxt.textAlign = "center";
	this.aTxt.lineHeight = 46;
	this.aTxt.lineWidth = 120;
	this.aTxt.parent = this;
	this.aTxt.setTransform(119.2,191);

	this.qTxt = new cjs.Text("", "50px 'Questrial'", "#0099FF");
	this.qTxt.name = "qTxt";
	this.qTxt.textAlign = "center";
	this.qTxt.lineHeight = 46;
	this.qTxt.lineWidth = 119;
	this.qTxt.parent = this;
	this.qTxt.setTransform(-128.7,191);

	this.rTxt = new cjs.Text("", "50px 'Questrial'", "#0099FF");
	this.rTxt.name = "rTxt";
	this.rTxt.textAlign = "center";
	this.rTxt.lineHeight = 54;
	this.rTxt.lineWidth = 118;
	this.rTxt.parent = this;
	this.rTxt.setTransform(-375.5,191);

	this.sTxt = new cjs.Text("", "80px 'Questrial'", "#FF0000");
	this.sTxt.name = "sTxt";
	this.sTxt.textAlign = "center";
	this.sTxt.lineHeight = 76;
	this.sTxt.lineWidth = 200;
	this.sTxt.parent = this;
	this.sTxt.setTransform(-16,-173.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.sTxt},{t:this.rTxt},{t:this.qTxt},{t:this.aTxt},{t:this.cTxt}]}).wait(1));

	// score
	this.totalScoreMc = new lib.ScoreAni();
	this.totalScoreMc.name = "totalScoreMc";
	this.totalScoreMc.parent = this;
	this.totalScoreMc.setTransform(-14.3,-133.4,2.344,2.344);

	this.timeline.addTween(cjs.Tween.get(this.totalScoreMc).wait(1));

	// response
	this.responseTimeMc = new lib.ResponseAni();
	this.responseTimeMc.name = "responseTimeMc";
	this.responseTimeMc.parent = this;
	this.responseTimeMc.setTransform(-374.6,213.8,1.406,1.406);

	this.timeline.addTween(cjs.Tween.get(this.responseTimeMc).wait(1));

	// questions
	this.totalQuestionsMc = new lib.QuestionAni();
	this.totalQuestionsMc.name = "totalQuestionsMc";
	this.totalQuestionsMc.parent = this;
	this.totalQuestionsMc.setTransform(-127.7,211.3,1.406,1.406);

	this.timeline.addTween(cjs.Tween.get(this.totalQuestionsMc).wait(1));

	// attempts
	this.attemptScoreMc = new lib.AttemptsAni();
	this.attemptScoreMc.name = "attemptScoreMc";
	this.attemptScoreMc.parent = this;
	this.attemptScoreMc.setTransform(121.1,211.6,1.406,1.406);

	this.timeline.addTween(cjs.Tween.get(this.attemptScoreMc).wait(1));

	// correct
	this.correctScoreMc = new lib.CorrectAnicopy();
	this.correctScoreMc.name = "correctScoreMc";
	this.correctScoreMc.parent = this;
	this.correctScoreMc.setTransform(368.1,210.5,1.406,1.406);

	this.timeline.addTween(cjs.Tween.get(this.correctScoreMc).wait(1));

}).prototype = getMCSymbolPrototype(lib.result_option2, new cjs.Rectangle(-508,-292,970.7,639.3), null);


(lib.forest_bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// ground
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#348101").s().p("EiBgAKPIAA0dICdAAIC9A3QCYAsBhAmQgLgfAogUQAegPAQAAQAXAAAEABIAiATIAbAAQAkgjAXgMQAcgRAkgBIAABHQAKAABaghQBXgfA7gBQBRABAaAfQATAVA0BaIAAAHIAIgFQARgLAMgNQAOgRAFgEQAcgOCOAAQClAAAeASQATAMALBbIARgDQALgDAUAAQAZAAAJAIQAcATAbAMIAdAAQAAgEAHgYQAHgZgBgTQA2AAAOAIIA3AAQAAhCANghQAyAMALAWQATAqADADIAbAAQAFgcANgoQAJgfAAgMQBtATA6AvQAuAxAhAQIAdAAQgKgMAmgtQAngzAAgDIAuAbQAqAZAVAAIBqgXQBvgZAegEIBsAAQAAAhgNAnIAAAGIAEgBQAcgFAwgGQAwgFAJgCIBTAAIAABLQAVgGAhgFIBGgLQASgBAcAAQAHAABVATQBWASA7AAIAAgtQAYAHAeANIBjAAQgXgjAogVQAdgPAjAAQAEAAArAKQArAKAFAAQAOAAAPgGQAagKAOgEIAbAAIAAAhQAfgBBRgDIBdgDQA9AABTAQQBVARDCAAIAAAtQBgAACKAWQCGAWAsAUQAIgggBgbQgCgygugUIAAAAIBRAgQBZAiAjAGIAbAAQgFgbANgOQAIgIAZgLIAoAAIAABVQAcAAA2gUQA7gYAWgCQAAADAIASQAHASABAHIAAAHIAHgDIBXggQBbgYB0AAID1AAQglAkgfAQIgNANQAxAAC3gdQC4geBlAAQgPAHimA2QiBApgdAjQBvg1GJgFQHAAGCZAAQAAgLgbgiQgcgjAAgFIAHgDQAZAMAwAMQA6ANAMAGIAAhUQAwANAhAqQAcAjAJAAQAWAAAwgbQA2gdBYgJIB7AAQAAA7AOATQgCgXD4gKIJUAAIAAAoQgNAPgfASQgcASgJAOQA6gCB5gIQCRgJBFgBQDzgGAUBGIAIAEQAFAGABAEQAbgQAkgrQAggmAAgHQBIAAA5AWQA3AYA+AAQAAgWANgmQAOgkAAgJQANAAAAgFQAbAJAfAcQAeAZAJAVIANADQANAGABAEIAAhAIAqAAQAGAPASASQAQAOAAALQANAAAAgOIBgAAQA1ARA3ADIBAAAQBMAABvgPQBsgSAtABIC7AKQC2AJA+AAQAbgBEEAoQEFAoAaAAIAAADIADgBQBCgOA4gCQAhgCArABQAegDAYgRQAagTAvg5IAbAmQASAVAZAMQBTgeAAg2QBGABBBAjQAgASAWASQAgADAbgJQA3gTgEhBQALADBHApQA8ghAqgTQBOgnA1AAQAXAIAOAPQARAQAPAIQAhgQBegKQBdgJANgEQgcAbAxAdQAwAcA0gBIAAgnIBRAAQA3AbA8AIQAbADAyABIAAgsQgSgMgWgUIgpgiIAEgFIBSAbQBMAYAPAAIAAhOQA+AJAcARQASAJAeAYQAJgGAPgKQAPgLAcgOIB6AAQAABCgOAGIBJAAQAfgFAkgHID1AAQghAfgtAEQgxAFgYARIEEAAQATATgTAuIGZAAIAAhUIA4AAIBUAMIA1AHQAsAAAWgbQAQgSAAgaQALAAA9ARQA9ARAOAAQAEAABAgRQBDgRARAAQEaA7AFAAQARgBAegGQAggGAQAAQAVAAB9AjQB7AkAUAAQAVAAALgNQAMgOAkAAQAaABCSAoQCoAtAfAGIBfAAIAAhBIB7AAQAeAIAgAHIAAOjgEhP2gGZIAQAAQAIAAADgCIACgEIAOAAIAAgIQggAIgLAGg");
	this.shape.setTransform(92.2,354.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#99FF00").s().p("APEG9QgJgBgqgJQhugZlbghIgtAAQALAAAAANQAUAFAwATIBSAiQgkgGkrhFQkuhFgdgFIg2AAQAlASBYAZQBOAdApBAIgPAAIiggrIkthRQAHAHAcAQQAkAVAYAPQh5gQjMAAIlYAAQjGgEiGgcQipgjhzhPQgUAZgKAFQgGADgTAAIhcgaQhegbgaAAQgTAAgGAEQgFADgEAAIAAAaQgbgMAAgZQAAAEgOAOQgNAKgLARIgLATIgFgBQAAgOgSgkQgZgsgagKIgNAIQgFAEgJAAQgsAqABAiIACAiQgJAIhRABIlAgBIk0ABQhOgBgJgHIABgeQABgfgsgkIgOAAQAAAIgJAHQgNAKggAOIAAgcQgBAQgFAMIgHAKQgXAag8AKIAAhCQgOAAAAAHIgdAiIgMgBQgRgIACgVQABgRg1gNQAAAHgOAHQAAAjgKAPQgTAZhDAcIAAg6QgfgBhXgOIhcgOQApggB8g0QCbg/A3gZQARAIAHApQADAYAAA3IAAAOQADgVA3glIBPguIA2AAIAABOIAbAAQAFgVAMgSQAWgjAqgRIA2AAQAbAzAAB2QBugJC5gDQBugCDhAAQgTgiBVhCQBdg9AFgHQAdARAYAbQAcAdAAASIAbAAQAAgGAOgaQANgbAAgGQAOAAAOgHQAOgGAQAAIAABOICHhBIA5AAIAABoQBkAAHPA0QDoAaDXAaQAAgLgNgdQgOgdAAgXIAAgMIB1ArQBxApAQAHQAggPAAgKQgGgKABgEQA/AGBkAbQBKATAjANIAbAAQAAgzgbAAIAAgOQA/AADOA9QDaBAAbAEQBFAHF4gGQE4gFALARQGQgFFoggQEzgbF6g6QHphLBSgKQE4gnEZAAQDQAAENAgQCHAQBiAQQAuAAgBgBQgNgNgSgYQAbAADcAgQC5AbAvAEIgBgCQgbglgNgMIAAgMQA7AECfAhIDAApIAAhbIFjAAQCOATIEAIIRFAMQDaACHIgKIBjAAIAACgQlkgxi1gOQjWgRkUgFQiWgBoMAAInTAAQAMARACAvQg9gCh+gRQiNgUgcAAQAAAngNAMIhsAAIkFgZIAAAmQgmAAiMgdQiHgcgbgBIgCAAIAAAHIAfALIgEACQiMAAisgaQisgZgHAAQicgCihAFQkuAKg6AcIgHgDQAOAAAAg0QgXAAizAXQizAWgPAAIgzAAQAKAAAAgMQAdgLAPgKQARgMAHgRIAAgCIgOAAQzoCtjjAZQnCAwocAAQhMAAgZgDgAS5FrIAUAAIAAAAIgUAAgEBPZADPIgbAAIgNgBQAYAZAQgYgEg+QADkIiMgoQAAAEgHAKQgHAKAAADQg2hFlGgLQjBgHngACQoAgIkSglQi/gZo2hbIhGAAQAAATgOAAQgIgDghgHIg2gJIAAAmQgZAAg7gUQg/gWgrgDIhVAAIAABNIpsirQjlhAj7gvIAAjMQS4E4OYCAQOEB8P0AAQBHAAAuAmQAhAcA3AHIAAAEIAqAAQgWgBgUgDIAAhXIBvAAQAhAVAvAFQAKABBIAAQBlgIBAgTIAOAUQANAaAAAgQAIADAGAFIgFAFIhOgLIiJAAQAAA0gNAZQhngJhUgXg");
	this.shape_1.setTransform(92.2,234);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6BB300").s().p("EB9JAGCQgfgGiogtQiSgogagBQgkAAgMAOQgLANgVAAQgUAAh7gkQh9gjgVAAQgQAAggAGQgeAGgRABQgFAAkag7QgRAAhDARQhAARgEAAQgOAAg9gRQg9gRgLAAQAAAagQASQgWAbgsAAIg1gHIhUgMIg4AAIAABUImZAAQATgugTgTIkEAAQAYgRAxgFQAtgEAhgfIj1AAQgkAHgfAFIhJAAQAOgGAAhCIh6AAQgcAOgPALQgPAKgJAGQgegYgSgJQgcgRg+gJIAABOQgPAAhMgYIhSgbIgEgBIAAAGIApAiQAWAUASAMIAAAsQgygBgbgDQg8gIg3gbIhRAAIAAAnQg0ABgwgcQgxgdAcgbQgNAEhdAJQheAKghAQQgPgIgRgQQgOgPgXgIQg1AAhOAnQgqATg8AhQhHgpgLgDQAEBBg3ATQgbAJgggDQgWgSgggSQhBgjhGgBQAAA2hTAeQgZgMgSgVIgbgmQgvA5gaATQgYARgeADQgrgBghACQg4AChCAOIgDgCQgaAAkFgoQkEgogbABQg+AAi2gJIi7gKQgtgBhsASQhvAPhMAAIhAAAQg3gDg1gRIhgAAQAAAOgNAAQAAgLgQgOQgSgSgGgPIgqAAIAABAQgBgEgNgGIgNgDQgJgVgegZQgfgcgbgJQAAAFgNAAQAAAJgOAkQgNAmAAAWQg+AAg3gYQg5gWhIAAQAAAHggAmQgkArgbAQQgBgEgFgGIgIgEQgUhGjzAGQhFABiRAJQh5AIg6ACQAJgOAcgSQAfgSANgPIAAgoIpUAAQj4AKACAXQgOgTAAg7Ih7AAQhYAJg2AdQgwAbgWAAQgJAAgcgjQghgqgwgNIAABUQgMgGg6gNQgwgMgZgMIgHgDIAAAGQAAAFAcAjQAbAiAAALQiZAAnAgGQmJAFhvA1QAdgjCBgpQCmg2APgHQhlAAi4AeQi3AdgxAAIANgNQAfgQAlgkIj1AAQh0AAhbAYIhXAgIgHgEQgBgHgHgSQgIgSAAgDQgWACg7AYQg2AUgcAAIAAhVIgoAAQgZALgIAIQgNAOAFAbIgbAAQgjgGhZgiIhRggIgOgGIAOAGQAuAUACAyQABAbgIAgQgsgUiGgWQiKgWhgAAIAAgtQjCAAhVgRQhTgQg9AAIhdADQhRADgfABIAAghIgbAAQgOAEgaAKQgPAGgOAAQgFAAgrgKQgrgKgEAAQgjAAgdAPQgoAVAXAjIhjAAQgegNgYgHIAAAtQg7AAhWgSQhVgTgHAAQgcAAgSABIhGALQghAFgVAGIAAhLIhTAAQgJACgwAFQgwAGgcAFIgEgFQANgnAAghIhsAAQgeAEhvAZIhqAXQgVAAgqgZIgugbQAAADgnAzQgmAtAKAMIgdAAQghgQgugxQg6gvhtgTQAAAMgJAfQgNAogFAcIgbAAQgDgDgTgqQgLgWgygMQgNAhAABCIg3AAQgOgIg2AAQABATgHAZQgHAYAAAEIgdAAQgbgMgcgTQgJgIgZAAQgUAAgLADIgRADQgLhbgTgMQgegSilAAQiOAAgcAOQgFAEgOARQgMANgRALIgIgCQg0hagTgVQgagehRgBQg7ABhXAeQhaAhgKAAIAAhGQgkABgcAQQgXAMgkAjIgbAAIgigTQgEgBgXAAQgQAAgeAPQgoAUALAfQhhgmiYgsIi9g2IidAAIAAksQAugEAdAAQAmgBAeAJQAbAGA4AYQALgIANgMQAQgOABgEIA4AAIA2AmQAbgaANgGIA3AAQAAAHAWAoQATAnAAAEQAKgKAWg4QAWg3AAgPQA4APAQATQAKAKAAAVQAWgDAdgOIAfgPIAOgGIAABTQAagVBGgTICJgeICjAAIAAAmQAZgBAmgMQAvgPAcgEIBQAAQAAAGAQAGIhCAcQg1AagEASIAAAGIAHgFQAqgdAfgFIIxAAQgxAphKAfQA4AABjgKQBRgIAzgJIAABVQAAgCAOgMQAOgLAAgIQAbAAAAgGQAWAMArARQA4AXAQgBIAAgmQAMAJAjALQAmALAYABIAAgnIBQAjQBDAdAgAIIAAgbQgEgPgJgPQgPgdgqgYIAAhoQA2ANA+A5QBABEAaAYIANAGQAAgcAHgrQAGgqABgKQA7AMAsAmIAhAjIAdAAIAAhVQAOABAAgHQAnAOAgAZIAlAhIAbAGQAAgLAJgbQAJgcAJgSQAyAKAhAoQAnAwAqASIAdAAQABgEgDgoQgBgNAegWIERAAIAAApQADAkg5AcQAVgFB0gEQBpgEBAAAQEGAAClAeQE4A3ElAfQAAggAOgTIA3AAQA9AHAOAGIgGAGQARgDgLgDQAXgXALgJQAVgOAaAAQAsAAAoAKQAoAKAMAAQAHAABdAVQBhAXAQAIQALgmgegPQgSgIiagrIABgFQEvAMBmAgQAyAPAgAZQANALAuAuIANAAQAAggg4gsIhEgvICYAAQCDAVBTAZIBJAZIAAguQA2AWASAbQAKAPAAAVQBrgSAQgVIAbAAIAAAuQANgOAAgTQAOAAAUgDQAWgDANgBQAAAIAHAPIADAHQgBAHAEAJIAAgGIgDgKIABgHQAPgfBwgKQCogDAWgEIAOABQgEAUgpAaIguAgIgIABQgoAEgLAAIAAAgIAOAAQARgQAcgVIA/gIIDaAAQAJAIAHATQAIAWAFAIQAMABAbgIQAngKBIgHIA3AAQAAAyAOATQAXgMCMAAIAAgnQAeAEBLAVQBJAUAOAGQAAgLgbhbQgchaAAgIIAMgBQAIAXA9A1QAqAmA3AqIAegRQAYgNAAgOIA4AAQANAYAIAKQALANAWAKQAAgDANgKQAOgJAAgDIAUALQAOAHAVABQAhgKAtgJQBagTBBAAIAABhQAagMBZgYQB7gfA7gSICWAAQAABbgPAUQDRghC3gQQCfgRBcABQAwgBANAHQAJAEAaARIAAhCQAOAAAAgMQBJAVAHAaQAFAYAzANIAAgoQAdAHAZALQAQAIATgBQAIABAIgHQAOgJAPgGQBDgcC0gUIBTAAIAABUQAbgYCDgOQBdgJBAAAIAAAgQgJAXgKAMQgGAFgIAEIAQgGQAegJA4gLQBagRA6gBQAXABADADQACAEgBAMQAIAjBnAyQAmgQBWgpQBHgiADABQAkAABVAQQCAAaCoA2IAcAAIAAiBQAtAAAmASQAwAYATAEIAAhVQADgKAYAUQAMALAQASQA3gnAUgKQAdgPAtAAQAfAAAaASQAbATAGAAQAEAAChgfQAEAABOAQQBSASBYAEQCQAFCGgCIBegBQACgYAKglIALg0IACABIAOAHQAjgTAhgwQAcgnAAgLQAuAKAMASQAFAIABAaQABAqBvAGQgFgNAcgjQAggnAsgXIAABbQADAAAMAEQANADASAAQAFAABQgSQBUgTAcgBIAABnQCnAACDgqQBNgZARgEQA3gNA7AAQABABgEBGQgBAfA6AnIA4AAIAAhhQACAsAMAuQAKAAAJAEQAIAFAAAFIANAAQAAhbAQgTIAfAfQAaAfgQAPIAdADQAWAEAPAAQBShWAAgFIAOAAQAAAFAHAfQAGAfAAARIAVAFQAFACAWAAIB7geICQgjIAABuQAJgJBFgLQB8gUAfgFICIAAIAAB7QAagQBlgDIB3AAQBgiAAAgIQAJAVAbAXQAZAXAVAKIAAghQgNgYgOgUQgKgPgCgIQAHAOBkAvIANAAQAAhtAOgTQAwAKASARIAdAZIBUAAQgOgcAbgXQAIgGAigOQA7ARA1AlQAoAcAmABQgXg5AqgPQAOgFAygBQBLAACaAyQCbAwALAAQA/AAAJgFQAFgFAuhKQgaA2CBAuQBzApBnAAIAPAAIAAEzQgggHgegIIh7AAIAABBg");
	this.shape_2.setTransform(92.2,293.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#7ED200").s().p("EAtdAH8Qiog2iAgaQhVgQgkAAQgDgBhHAiQhWApgmAQQhngygIgjQABgMgCgEQgDgDgXgBQg6ABhaARQg4ALgeAJIgCgDQAKgMAJgXIAAggQhAAAhdAJQiDAOgbAYIAAhVIhTAAQi0AVhDAcQgPAGgOAJQgIAHgIgBQgTABgQgIQgZgLgdgHIAAAoQgzgNgFgYQgHgahJgWQAAANgOAAIAABCQgagRgJgEQgNgHgwABQhcgBifARQi3AQjRAhQAPgUAAhbIiWAAQg7ASh7AfQhZAYgaAMIAAhiQhBAAhaAUQgtAJghAKQgVgBgOgHIgUgLQAAADgOAJQgNAKAAADQgWgKgLgNQgIgKgNgZIg4AAQAAAOgYAOIgeARQg3grgqgmQg9g1gIgXQgCgHADgFQgNAAAAANQAAAIAcBaQAbBcAAALQgOgGhJgUQhLgVgegEIAAAnQiMAAgXAMQgOgTAAgzIg3AAQhIAIgnAKQgbAIgMgBQgFgIgIgXQgHgTgJgIIjaAAIg/AIIAuggQApgaAEgUIAAgBIgOAAQgWAEioADQhwAKgPAfIgEAAQgHgPAAgIQgNABgWADQgUADgOAAQAAATgNAOIAAguIgbAAQgQAVhrASQAAgVgKgPQgSgbg2gWIAAAuIhJgZQhTgZiDgVIiYAAIBEAvQA4AsAAAgIgNAAQgugugNgLQgggZgygPQhmggkvgMIgIAAIgSgCQAIABAIADIAJADQCaArASAIQAeAPgLAmQgQgIhhgXQhdgVgHAAQgMAAgogKQgogKgsAAQgaAAgVAOQgLAJgXAXQgOgGg9gHIg3AAQgOATAAAgQklgfk4g3QilgekGAAQhAAAhpAEQh0AEgVAFQA5gcgDgkIAAgpIkRAAQgeAWABANQADAogBAEIgdAAQgqgSgngwQghgogygKQgJASgJAcQgJAbAAALIgbgGIglghQgggZgngOQAAAHgOgBIAABVIgdAAIghgjQgsgmg7gMQgBAKgGAqQgHArAAAcIgNgGQgagYhAhEQg+g5g2gMIAABnQAqAYAPAdQAIAPAFAPIAAAbQgggIhDgdIhQgjIAAAnQgYgBgmgLQgjgLgMgJIAAAmQgQABg4gXQgrgRgWgMQAAAGgbAAQAAAIgOALQgOAMAAACIAAhVQgzAJhRAIQhjAKg4AAQBKgfAxgpIoxAAQgfAFgqAdIgHgBQAEgSA1gaIBCgcQgQgFAAgGIhQAAQgcAEgvAOQgmAMgZABIAAglIijAAIiJAdQhGATgaAVIAAhSIgOAGIgfAPQgdANgWADQAAgUgKgKQgQgTg4gPQAAAPgWA2QgWA4gKAKQAAgEgTgnQgWgnAAgHIg3AAQgNAGgbAZIg2glIg4AAQgBAEgQANQgNAMgLAIQg4gXgbgGQgegJgmABQgdAAguAEIAAnwQD7AvDlBBIJsCsIAAhPIBVAAQArADA/AWQA7AWAZgBIAAgnIA2AKQAhAHAIACQAOABAAgUIBGAAQI2BcC/AaQESAkIAAJQHggCDBAGQFGAMA2BEQAAgDAHgKQAHgKAAgEICMApQBUAXBnAIQANgZAAg0ICJAAIBOALIANACQAGAGgBAFIAJgHIBcANQBXAOAfACIAAA4QBDgaATgaQAKgOAAgkQAOgHAAgHQA1ANgBASQgCAUARAJQgEARAQgRIAdgiQAAgGAOgBIAABCQA8gKAXgaIAHgKIAGAAQAggOANgKQAJgHAAgIIAOAAQAsAkgBAfIgBAdQAJAHBOABIE0gBIFAABQBRgBAJgIIgCghQgBghAsgqQAJAAAFgFIANgIQAaALAZArQASAkAAAPIAAAMIAFgMIALgTQALgRANgKQAOgNAAgFQAAAZAbAMIAAgaQAEABAFgEQAGgEATAAQAaAABeAbIBcAaQATAAAGgDQAKgFAUgZQBzBOCpAjQCGAcDGAEIFYAAQDMAAB5ARQgYgQgkgVQgcgQgHgHIEtBRICgArIAPAAQgpg/hOgeQhYgZglgSIA2AAQAdAGEuBFQErBEAkAGIhSghQgwgTgUgGQAAgNgLAAIAtAAQFbAhBuAZQAqAKAJAAQAZADBMAAQIcAAHCgwQDjgZToisIAOACQgHARgRAMQgPAKgdALQAAALgKAAIAzAAQAPABCzgWQCzgXAXAAQAAAzgOAAIAAAHIAHgEQA6gbEugKQChgECcABQAHAACsAZQCsAZCMAAIAdAHIgZgJIgfgLIACgHQAbABCHAcQCMAdAmABIAAgnIEFAZIBsAAQANgMAAgmQAcAACNATQB+ARA9ACQgCgugMgRIHTAAQIMAACWABQEUAFDWARQC1ANFkAxIAAEuIgPAAQhnAAhzgqQiBguAag2QguBKgFAFQgJAFg/AAQgLAAibgwQiagyhLAAQgyABgOAFQgqAPAXA5QgmgBgogcQg1glg7gRQgiAOgIAGQgbAXAOAcIhUAAIgdgZQgSgRgwgKQgOATAABuIgNAAQhkgwgHgOQACAIAKAPQAOAUANAZIAAAhQgVgKgZgXQgbgYgJgVQAAAIhgCBIh3AAQhlADgaAQIAAh7IiIAAQgfAFh8AUQhFALgJAJIAAhvIiQAjIh7AfQgWAAgFgCIgVgFQAAgSgGgfQgHgfAAgFIgOAAQAAAFhSBXQgPAAgWgEIgdgDQAQgQgagfIgfgfQgQATAABcIgNAAQAAgFgIgFQgJgEgKAAQgMgvgCgsIAABiIg4AAQg6goABgfQAEhGgBgBQg7AAg3ANQgRAEhNAZQiDAqinAAIAAhnQgcABhUATQhQASgFAAQgSAAgNgDQgMgEgDAAIAAhbQgsAXggAnQgcAjAFANQhvgGgBgqQgBgagFgIQgMgSgugKQAAALgcAnQghAwgjATIgOgHIAAgPIgCAOIgLA0QgKAmgCAYIheABQiGACiQgFQhYgEhSgSQhOgRgEAAQihAggEAAQgGAAgbgTQgagTgfAAQgtAAgdAPQgUALg3AnQgQgSgMgLQgYgVgDALIAABVQgTgEgwgYQgmgSgtAAIAACBg");
	this.shape_3.setTransform(92.2,260.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// bground
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(29.2,-182.5,1.737,2.092,0,0,180,-0.1,-0.1);
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(-36, -1, 0, 138))];
	this.instance.cache(-402,63,804,239);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// sky
	this.instance_1 = new lib.Symbol4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(81.6,4,1.669,1.109,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.forest_bg, new cjs.Rectangle(-736.7,-379.5,1657.8,827.6), null);


// stage content:
(lib.ResultScreen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.resultScreenMc = new lib.result_option2();
	this.resultScreenMc.name = "resultScreenMc";
	this.resultScreenMc.parent = this;
	this.resultScreenMc.setTransform(634.8,323.8,0.999,0.996,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.resultScreenMc).wait(1));

	// Layer_5
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(635.9,355.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_4
	this.instance_1 = new lib.forest_bg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(624.3,330.3);
	this.instance_1.filters = [new cjs.BlurFilter(25, 25, 3), new cjs.ColorMatrixFilter(new cjs.ColorMatrix(-8, 1, -21, -1))];
	this.instance_1.cache(-739,-381,1662,832);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(493.6,276.8,1729,900);
// library properties:
lib.properties = {
	id: '69D87AFB09EC6F4C934222D9FD26164E',
	width: 1280,
	height: 720,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['69D87AFB09EC6F4C934222D9FD26164E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;