     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="contentInner">
 <div class="container">


<div class="bspiCalender">
 <h2> </h2> 

 <?php //print_r($bspicomparison);  ?>
 
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>



<script>
$(document).ready(function()
{
	bspichart();
});
 function bspichart()
 {
	 
	 Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });
	
	 var chart = Highcharts.chart('container1', {

    chart: {
        type: 'column',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: true},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: ['BSPI'],
		gridLineWidth: 0,
  minorGridLineWidth: 0
       
    },
  plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Score'
        },
		 max: 100 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0
		
    },
	


    series: [{
		
		showInLegend: false, 
        name: 'Puzzle set - 1',
        data: [{y:<?php echo round($bspicomparison[0]['avgbspiset1'], 2); ?>,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#ff6600"],[1, Highcharts.Color("#ff6600").brighten(-0.3).get('rgb')]]}}]
	},
	<?php if($this->session->set2planid!=0) { ?>
	{showInLegend: false, 
        name: 'Puzzle set - 2',
        data:  [{y:<?php echo round($bspicomparison[0]['avgbspiset2'], 2); ?>,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#067d00"],[1, Highcharts.Color("#067d00").brighten(-0.3).get('rgb')]]}}]
    
	<?php } ?>
	<?php if($this->session->set3planid!=0) { ?>
	{showInLegend: false, 
        name: 'Puzzle set - 3',
        data:  [{y:<?php echo round($bspicomparison[0]['avgbspiset3'], 2); ?>,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "#ffbe00"],[1, Highcharts.Color("#ffbe00").brighten(-0.3).get('rgb')]]}}]
    
	<?php } ?>
	 }] 
});
 }
	 
	 
	 
 
</script>
 
  <div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID1">BSPI comparison</h2>
 <br/>
 <div id="container1">
</div>
 </div>
  
 </div> 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 

 
 </style>