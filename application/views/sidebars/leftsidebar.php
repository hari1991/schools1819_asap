 <aside class="contentleft">
<section class="profileBg">
                		<!-- /#content --> 
								

   <!--<link rel="css/colorbox.css" type="text/css" />  
   <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>-->
<script type="text/javascript" src="plugins/jqplot.pieRenderer.min.js"></script>

<script language="javascript">
$(document).ready(function(){
	
  var data = [['Memory',1],['Visual Processing',1],['Focus And Attention',1],['Problem Solving',1],['Linguistics',1]];
  var totalgam = 0;
  var lblflg = false;
  if(totalgam>0){
  		//lblflg = true;
  }
  var plot1 = jQuery.jqplot ('piechart1', [data],
    {
      seriesDefaults: {
        // Make this a pie chart.
        renderer: jQuery.jqplot.PieRenderer,
        rendererOptions: {
          // Put data labels on the pie slices.
          // By default, labels show the percentage of the slice.
          showDataLabels: lblflg,
		   dataLabels: 'value',
		   padding: 2
        }
      },
	  seriesColors:['#00000','#00000','#00000','#00000','#00000'],
	  legend:{
            show:true,
            placement: 'inside',
            rendererOptions: {
                numberRows: 4
            },
            location:'s',
            marginTop: '15px'
        }  
      //legend: { show:true, location: 'e' }
    }
  );
  
  
  $('.closeArrow').click(function(){
	  window.location.href = window.location.href; 
	  });
  
});
</script>		

<?php //print_r($query); ?>

<img class="profileImg" src="<?php echo base_url(); ?>assets/<?php echo $query[0]->avatarimage; ?>" width="113" height="113" alt="Profile Image">

                       
<input type="button" id="btnchangeavatar" class="btnchangeavatar" name="btnchangeavatar" value="Change avatar">


        <div id="fade" class="black_overlay" style="display: none;"></div>
<script type="text/javascript">
      $('#btnchangeavatar').click(function(){
	  
	  $('#avatharimgs').css("top",'-80px').show().animate({'marginTop' : "+=150px" },800);
		//document.getElementById("fadeavatarpopup").style.display="block";
		$('#fade').show();
		
});//window.location.href = window.location.href;
$('.avatarprofileImg').click(function(){
	
	var avatarimg = $(this).attr('src');
	$.ajax
	({
			type:"POST",
			url:"avatarimage_upload.php",
			data:{Type:'A',avatarimg:avatarimg,id:1},
			success:function(result)
			{
				//alert (result);
				window.location.href = window.location.href;
				
			}
	
	});
	
	});
</script>
                   <h4><?php echo $this->lang->line("goodday"); ?><br> <?php echo $query[0]->fname; ?></h4> <p><?php echo $this->lang->line("lastplayed"); ?><br>  <?php echo $query[0]->pre_logindate; ?>.</p>
				   
				   
				   
<?php
$login_count = 3;
if($login_count<1)
{
echo '<p>'.$this->lang->line("greetingmessage1").'</p>';
}
elseif($login_count<=3)
{
 echo '<p>'.$this->lang->line("greetingmessage2").'</p>';
 
}
 
?>


				  		
	<!-- /#sidebar -->
	</section>
	
	<section class="myTrophies">
			      <div class="todayStatus" id="todayStatus">Your Overall <span class="hoverhelp">BSPI</span> as on <span><?php //echo $query[0]->login_date; ?>  : <span class="bspi_ratings">45.8</span></span><span class="bspi tool-tip slideIn right" id="bspi">BSPI - Brain Skill Power Index - Average of all skill scores</span></div>
             <div class="howtogetOuter"><a href="#" class="questionicon"></a><div class="howtoget_trophies" id="howtoget_trophies"><h3 class="gettrophiestext">How can I get stars and trophies</h3></div><div class="tool-tip slideIn right">
 
<p>Stars are awarded to the users based on the scores of each games played</p>
<p>Stars Awarded on a given day</p>
<ul>
<li>- based on score obtained in the game</li>
<li>- or based on average score in the game</li>
<li>- Every day the stars are refreshed </li>
</ul>
<p>Trophies are awarded based on the no of stars earned in each skill for the current month</p>
</div></div>

<h2>My trophies</h2>

                <?php  // 
				
				foreach($mytrophy as $tro)
				{
					//print_r($tro);
					$mainclass='';
					$innerclass='';
					
					if($tro['catid']==59)
					{ $mainclass = 'memoryOuter'; $innerclass = 'memory'; }
					elseif($tro['catid']==60)
					{ $mainclass = 'visualOuter'; $innerclass = 'visualProcessing'; }
					elseif($tro['catid']==61)
					{ $mainclass = 'focusOuter'; $innerclass = 'focus'; }
					elseif($tro['catid']==62)
					{ $mainclass = 'problemOuter'; $innerclass = 'problemSolving'; }
					elseif($tro['catid']==63)
					{ $mainclass = 'linguisticsOuter'; $innerclass = 'linguistics'; }
					
					
					
					
 echo	 '<div class="trophiesList '.$mainclass.'">
<div class="trophiesName">
<h5><span class='.$innerclass.'></span> '.$tro['name'].' </h5>
 </div>
<div class="trophiesCups">';

if($tro['diamond'] == 0)
{ echo '<span class="dimondCup"></span>'; } else { echo '<span class="defaultCup"></span>'; }

if($tro['gold'] > 0)
{ echo '<span class="goldcup"></span>'; } else { echo '<span class="defaultCup"></span>'; }

if($tro['silver'] > 0)
{ echo '<span class="silverCup"></span>'; } else { echo '<span class="defaultCup"></span>'; }

echo '</div>
</div>';
					
					
				}
				
				
				?>

								
</section>
</aside>