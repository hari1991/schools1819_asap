///////////////////////////////////////////////////-------Common variables--------------/////////////////////////////////////////////////////////////////////
var messageField;		//Message display field
var assets = [];
var cnt = -1, ans, uans, interval, time = 180, totalQuestions = 10, answeredQuestions = 0, choiceCnt = 3, quesCnt = 0, resTimerOut = 0, rst = 0, responseTime = 0;
var startBtn, introScrn, container, choice1, choice2, choice3, choice4, question, circleOutline, circle1Outline, boardMc, helpMc, quesMarkMc, questionText, quesHolderMc, resultLoading, preloadMc;
var mc, mc1, mc2, mc3, mc4, mc5, startMc, questionInterval = 0;
var parrotWowMc, parrotOopsMc, parrotGameOverMc, parrotTimeOverMc, gameIntroAnimMc;
var bgSnd, correctSnd, wrongSnd, gameOverSnd, timeOverSnd, tickSnd;
var tqcnt = 0, aqcnt = 0, ccnt = 0, cqcnt = 0, gscore = 0, gscrper = 0, gtime = 0, rtime = 0, crtime = 0, wrtime = 0, currTime = 0;
var bg
var BetterLuck, Excellent, Nice, Good, Super, TryAgain;
var rst1 = 0, crst = 0, wrst = 0, score = 0;
var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;
var borderPadding = 10, barHeight = 20;
var loadProgressLabel, progresPrecentage, loaderWidth;
/////////////////////////////////////////////////////////////////////////GAME SPECIFIC VARIABLES//////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////GAME SPECIFIC ARRAY//////////////////////////////////////////////////////////////
var qno = [];

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
    checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////
function init() {
    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    createLoader()
    createCanvasResize()

    stage.update();
    stage.enableMouseOver(40);
    ///////////////////////////////////////////////////////////////=========MANIFEST==========///////////////////////////////////////////////////////////////

    /*Always specify the following terms as given in manifest array. 
         1. choice image name as "ChoiceImages1.png"
         2. question text image name as "questiontext.png"
     */

    assetsPath = "assets/";
    gameAssetsPath = "WhatsInStore-Level1/";
    soundpath = "FA/"

    var success = createManifest();
    if (success == 1) {
        manifest.push(
            { id: "choice1", src: gameAssetsPath + "ChoiceImages1.png" },
            { id: "choice2", src: gameAssetsPath + "ChoiceImages2.png" },
            { id: "choice3", src: gameAssetsPath + "ChoiceImages3.png" },
            { id: "question", src: gameAssetsPath + "question.png" },
            { id: "questiontext1", src: gameAssetsPath + "questiontext1.png" },
            { id: "questionText", src: gameAssetsPath + "questiontext.png" },
            { id: "GameIntroAnimation", src: gameAssetsPath + "GameIntro.js" },
        )
        preloadAllAssets()
        stage.update();
    }
}

//=====================================================================//


function doneLoading1(event) {
    var event = assets[i];
    var id = event.item.id;
    console.log(" doneLoading ")
    loaderBar.visible = false;
    stage.update();


    if (id == "questiontext1") {
        questiontext1 = new createjs.Bitmap(preload.getResult('questiontext1'));
        container.parent.addChild(questiontext1);
        questiontext1.visible = false;
    }

    if (id == "choice1" || id == "choice2" || id == "choice3") {
        var spriteSheet1 = new createjs.SpriteSheet({
            framerate: 60,
            "images": [preload.getResult("choice1")],
            "frames": { "regX": 50, "height": 377, "count": 0, "regY": 50, "width": 377 }
        });

        choice1 = new createjs.Sprite(spriteSheet1);
        choice1.visible = false;
        container.parent.addChild(choice1);

        var spriteSheet4 = new createjs.SpriteSheet({
            framerate: 60,
            "images": [preload.getResult("choice2")],
            "frames": { "regX": 50, "height": 377, "count": 0, "regY": 50, "width": 377 }
        });

        choice2 = new createjs.Sprite(spriteSheet4);
        choice2.visible = false;
        container.parent.addChild(choice2);
        //
        var spriteSheet5 = new createjs.SpriteSheet({
            framerate: 60,
            "images": [preload.getResult("choice3")],
            "frames": { "regX": 50, "height": 377, "count": 0, "regY": 50, "width": 377 }
        });

        choice3 = new createjs.Sprite(spriteSheet5);
        choice3.visible = false;
        container.parent.addChild(choice3);
    }

    if (id == "question") {
        var spriteSheet3 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("question")],
            "frames": { "regX": 50, "height": 576, "count": 0, "regY": 50, "width": 1002 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        question = new createjs.Sprite(spriteSheet3);
        container.parent.addChild(question);
        question.visible = false;
    }

    if (id == "questionText") {
        var spriteSheet3 = new createjs.SpriteSheet({
            framerate: 30,
            "images": [preload.getResult("questionText")],
            "frames": { "regX": 50, "height": 80, "count": 0, "regY": 50, "width": 791 },
            // define two animations, run (loops, 1.5x speed) and jump (returns to run):
        });
        //
        questionText = new createjs.Sprite(spriteSheet3);
        container.parent.addChild(questionText);
        questionText.visible = false;
    }

    if (id == "GameIntroAnimation") {
        var comp = AdobeAn.getComposition("9B187A3141F2F044B0356886562D636B");
        var lib1 = comp.getLibrary();
        gameIntroAnimMc = new lib1.GameIntro()
        container.parent.addChild(gameIntroAnimMc);
        gameIntroAnimMc.visible = false;
        gameIntroAnimMc.addEventListener("tick", startAnimationHandler);
    }
}

function tick(e) {
    stage.update();
}

/////////////////////////////////////////////////////////////////=======GAME START========///////////////////////////////////////////////////////////////////
function handleClick(e) {
    for (i = 0; i < 12; i++) {
        qno.push(i)
    }
    qno.sort(randomSort)
    toggleFullScreen()
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements() {
    helpMc.helpMc.contentMc.helpTxt.text = "Match the smiley";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y + 65;

    bg.visible = true;

    container.parent.addChild(questiontext1);
    questiontext1.visible = true;
    //
    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].visible = false;
    }
    question.visible = true
    container.parent.addChild(question)
    question.x = 200; question.y = 185;

    questionText.x = 270
    questionText.y = 150

    container.parent.addChild(choice1, choice2, choice3);
    choice1.x = 100; choice1.y = 300;
    choice2.x = 500; choice2.y = 300;
    choice3.x = 900; choice3.y = 300;

    choice1.visible = choice2.visible = choice3.visible = false;

    pickques();
}
function helpDisable() {
    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].mouseEnabled = false;
    }
}

function helpEnable() {
    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].mouseEnabled = true;
    }
}
//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    clk = 0;
    correctCnt = 0;
    chpos = [];
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions;
    boardMc.boardMc.openMc.mouseEnabled = true;

    questiontext1.visible = true
    questionText.visible = false
    question.visible = true;
    var temp = qno[cnt] % 4

    if (temp == 0)
        question.gotoAndStop(0)
    else if (temp == 1)
        question.gotoAndStop(1)
    else if (temp == 2)
        question.gotoAndStop(2)
    else
        question.gotoAndStop(3)

    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].visible = false;
        this["choice" + i].gotoAndStop(qno[cnt])
    }
    clearquesInterval = setInterval(createChoices, 5000);
}


function createChoices() {
    clearInterval(clearquesInterval)
    clearquesInterval = 0;
    boardMc.boardMc.openMc.mouseEnabled = true;
    boardMc.boardMc.openMc.alpha = 1;

    question.visible = false;
    questiontext1.visible = false;

    questionText.gotoAndStop(qno[cnt]);
    questionText.visible = true

    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].visible = true;
        this["choice" + i].gotoAndStop(qno[cnt])
        chpos.push({ posx: this["choice" + i].x, posy: this["choice" + i].y })
    }

    chpos.sort(randomSort);
    var vCnt = 0
    for (i = 1; i <= choiceCnt; i++) {
        vCnt++;
        this["choice" + vCnt].x = chpos[i - 1].posx;
        this["choice" + vCnt].y = chpos[i - 1].posy;
    };
    ans = "ch1";
    rst = 0;
    gameResponseTimerStart();
    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
    enablechoices();
}

function enablechoices() {
    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].name = "ch" + i;
        this["choice" + i].cursor = "pointer";
        this["choice" + i].addEventListener("click", answerSelected);
     //   this["choice" + i].addEventListener("mouseover", onRoll_over);
    //    this["choice" + i].addEventListener("mouseout", onRoll_out)
        this["choice" + i].visible = true;
        this["choice" + i].alpha = 1;
    }
}

function disablechoices() {
    for (i = 1; i <= choiceCnt; i++) {
        this["choice" + i].removeEventListener("click", answerSelected);
     //   this["choice" + i].removeEventListener("mouseover", onRoll_over);
     //   this["choice" + i].removeEventListener("mouseout", onRoll_out)
        this["choice" + i].visible = false;
        this["choice" + i].alpha = .5;
    }
    boardMc.boardMc.openMc.mouseEnabled = false;
}

function onRoll_over(e) {
    e.currentTarget.alpha = .5;
    stage.update();
}

function onRoll_out(e) {
    e.currentTarget.alpha = 1;
    stage.update();
}

function answerSelected(e) {
    e.preventDefault();
    e.currentTarget.removeEventListener("click", answerSelected);
    e.currentTarget.cursor = "default";
    uans = e.currentTarget.name;
    console.log("answer" + uans);
    console.log(ans + " =correct= " + uans)
    gameResponseTimerStop();
    if (ans == uans) {
        getValidation("correct");
    } else {
        getValidation("wrong");
    }
    disablechoices();
}
//===========================================================================================//
