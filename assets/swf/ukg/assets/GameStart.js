function CreateGameStart() {
    base_image.visible = false;
    base_image.removeEventListener("click", speaksound);
    base_image.cursor = "default";

    base_image1.visible = false;
    base_image1.removeEventListener("click", speaksound);
    base_image1.cursor = "default";



    //base_image.removeEventListener("tick",makeup);
    startBtn.removeEventListener("click", handleClick)
    startBtn.removeEventListener("click", handleClick)
    container.parent.removeChild(startBtn);
    container.parent.removeChild(startMc);
    container.parent.removeChild(gameIntroAnimMc);
    startMc = null;
    container.parent.removeChild(introScrn);
    container.parent.removeChild(introStartBtn);
    

    document.getElementById("gameCanvas").style.background = 'white';
    boardMc.visible = true;
    container.parent.addChild(boardMc);

    boardMc.boardMc.setMc.name = "setMc";

    boardMc.boardMc.setMc.effectMc.name = "effectMc"
    //
    boardMc.boardMc.setMc.cursor = "pointer";

    boardMc.boardMc.setMc.effectMc.cursor = "pointer";


    boardMc.boardMc.setMc.buttonMode = true;
    boardMc.boardMc.secMc.txt.text = parseInt(time)//"180";
    boardMc.boardMc.scoreMc.txt.text = "0";
    boardMc.boardMc.setMc.addEventListener("click", settingBarSelected);


    helpMc.helpMc.visible = true;

    helpMc.helpMc.contentMc.visible = false;
    helpMc.helpMc.closeMc.visible = false;
    helpMc.helpMc.hitMc.visible = false;

    boardMc.boardMc.openMc.cursor = "pointer";
    boardMc.boardMc.openMc.addEventListener("click", helpMcSelected);
    boardMc.boardMc.openMc.addEventListener("mouseover", help_RollOver);
    boardMc.boardMc.openMc.addEventListener("mouseout", help_RollOut);
    boardMc.boardMc.fullScreenBtn.addEventListener("click", toggleFullScreen);
    boardMc.boardMc.openMc.visible = false


    boardMc.boardMc.fullScreenBtn.cursor = "pointer";

    if (!createjs.Ticker.hasEventListener("tick")) {
        createjs.Ticker.addEventListener("tick", tick);
    }
    /*
    if (bgSnd) {
        bgSnd.stop();
    }
    bgSnd = createjs.Sound.play("begin", { interrupt: createjs.Sound.INTERRUPT_NONE, loop: -1, volume: 0.4 });
    bgSnd.volume = 0.2;
    bgSnd.on("complete", bgSndPlaying, null, false);
    */
    stage.update();

}



function bgSndPlaying() {
   /* bgSnd.play();
    bgSnd.volume = 0.2;
    bgSnd.on("complete", bgSndPlaying, null, false);
    */
}



function helpMcSelected(e) {
    helpMc.helpMc.contentMc.visible = true;
    helpMc.helpMc.closeMc.visible = true;
    helpMc.helpMc.hitMc.visible = true;
    helpMc.helpMc.hitMc.mouseEnabled = false
    helpMc.helpMc.closeMc.cursor = "pointer";
    helpMc.helpMc.visible = true;
    container.parent.addChild(helpMc.helpMc)

    boardMc.boardMc.setMc.mouseEnabled = false;
    boardMc.boardMc.fullScreenBtn.mouseEnabled = false;
    helpDisable()


    helpMc.helpMc.closeMc.buttonMode = true;
    helpMc.helpMc.closeMc.addEventListener("click", hideContent);
    helpMc.helpMc.closeMc.addEventListener("mouseover", closeMc_onRollover);
    helpMc.helpMc.closeMc.addEventListener("mouseout", closeMc_onRollout);

}
function help_RollOver(e) {

    boardMc.boardMc.openMc.gotoAndStop(1);
}
//
function help_RollOut(e) {
    boardMc.boardMc.openMc.gotoAndStop(0);
}

function closeMc_onRollover(e) {
    helpMc.helpMc.closeMc.gotoAndStop(1);
}
function closeMc_onRollout(e) {
    helpMc.helpMc.closeMc.gotoAndStop(0);
}


function hideContent(e) {
    helpMc.helpMc.visible = true;

    boardMc.boardMc.openMc.visible = true;
    helpMc.helpMc.contentMc.visible = false;
    helpMc.helpMc.closeMc.visible = false;
    helpMc.helpMc.hitMc.visible = false;

    boardMc.boardMc.setMc.mouseEnabled = true;
    boardMc.boardMc.fullScreenBtn.mouseEnabled = true;

    helpEnable()

    helpMc.helpMc.closeMc.removeEventListener("click", hideContent);
    helpMc.helpMc.closeMc.removeEventListener("mouseover", closeMc_onRollover);
    helpMc.helpMc.closeMc.removeEventListener("mouseout", closeMc_onRollover);
}

function settingBarSelected(event) {
    event.preventDefault();
    if (event.currentTarget.currentFrame == 0) {
        event.currentTarget.gotoAndStop(1);
        event.currentTarget.effectMc.gotoAndStop(1)
   //     bgSnd.stop();
        correctSnd.stop();
        wrongSnd.stop();
        gameOverSnd.stop();
 
       // Good.stop();
 

        isEffSound = false;

    } else if (event.currentTarget.effectMc.currentFrame == 1) {
        event.currentTarget.effectMc.gotoAndStop(0);
        event.currentTarget.gotoAndStop(0);
     //   bgSnd.play();
        isEffSound = true;
    }


    stage.update();
}

//=======================================================END OF BOARD================================================//
this.countTime = function () {
    time--;
   // console.log("time= "+time)
    boardMc.boardMc.secMc.txt.text = String(time)

    if (time == 10) {
     //   tickSnd = createjs.Sound.play("tick", { interrupt: createjs.Sound.INTERRUPT_NONE, loop: -1, volume: 0.4 });
      //  tickSnd.volume = 1;
       // tickSnd.play();
     //   bgSnd.stop();
    }
    if (self.time == 0) {
        timeOverSnd.play();
        timeOverSnd.volume = 1;
       
        //container.parent.removeAllChildren();
        var container3 = new createjs.Container();
        stage.addChild(container3)
        ParrotAnimationTesting.visible = true;
        container3.parent.addChild(ParrotAnimationTesting)
        ParrotAnimationTesting["_mc"].gotoAndStop(9);

      

        stage.update()
        timeOverSnd.addEventListener("complete", handleComplete1);
     //   pickQuesInterval = setInterval(delayGameAndTimeOver, 1200)
        clearInterval(self.interval)
        rst1 = rst1 + rst;
        astatus = "U";
        uans = "NotAnswered";
        gameResponseTimerStop();
        correctSnd.stop();
        wrongSnd.stop();
        gameOverSnd.stop();
       // tickSnd.stop();
       // bgSnd.stop()
    }

}
function pauseTimer() {
    currTime = time;
    clearInterval(interval);
}
function restartTimer() {
    time = currTime;
    interval = setInterval(countTime, 1000);
}
//----------------------------RESPONSE TIMER-------------------------------------------//
function gameResponseTimerStart() {
    resTimerOut = setInterval(function () {
        rst++;
        //  console.log("rst= "+rst)
        //self.boardMc._txt.text = parseInt(rst);
    }, 1000);
}
function gameResponseTimerStop() {
    clearInterval(resTimerOut);
}
