<?php

if ($this->session->user_id)
        { 
            redirect('index.php/mypuzzleset2/dashboard');
        }

 ?>


<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<title>SkillAngels Super Admin for Assessment</title>
		<meta name="description" content="SkillAngels, An IIT Madras RTBI incubated company, Skillangels was conceptualized with the vision of improving cognitive skills and performance for students and adults through next generation brain enhancement programs.">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap Core CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-3.1.1.min.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-multiselect.css" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- MetisMenu CSS -->
		<link href="<?php echo base_url(); ?>assets/css/metisMenu.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
		<!-- Custom Fonts -->
		<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>assets/css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
		<!-- Web Fonts  -->
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<link rel="icon" type="<?php echo base_url(); ?>assets/image/png"  href="img/favicon.png">
		<script src="<?php echo base_url(); ?>assets/js/jquery_library.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.js" type="text/javascript"></script>
		<!--[if IE]>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="<?php echo base_url(); ?>assets/vendor/respond.js"></script>
		<![endif]-->
		
    </head>
	<body>
		<div id="wrapper">
		<!-- Codrops top bar --> 
		<!-- header starts here -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="pageheader">           
		
            <!-- /.navbar-header -->
			<span class="topHead">  Welcome to Skill Angels Admin Panel </span>
            
			</div>
				
		</nav>
<style>
.ed6 {
    color: rgb(0, 170, 255);
    font-family: "Shadows Into Light",cursive;
    font-size: 2em;
}
.LoginContainer
{
	    BORDER: 2px solid;
    width: 50%;
    margin-left: 25%;
}
.submitlabel{padding-top:20px;padding-bottom:20px;}
.LoginContainer .fields{text-align:right;}
</style>

	
	<!--[if lte IE 6]>
		<style type="text/css" media="screen">
			form label {
					background: #fff;
			}
		</style>
	<![endif]-->
	
  <script type="text/javascript" src="js/jquery.validate.js"></script>
<div id="login-page-wrapper">

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide" style="display: block;">
<div class="row">
      			<div class="col-lg-12 landingContainer">
  <div align="center"><strong><span class="ed6">EdSix Brain Lab<sup>TM</sup> Pvt Ltd</span><br>Incubated by IIT Madras' RTBI<br>
					Supported by IIM Ahmedabad's CIIE and Village Capital, U.S.A</strong>

					</div>
  </div>
  </div>
              <div class="row">
      			<div class="col-lg-12 landingContainer">
				<div class="LoginContainer" align="center">
        			<h1 class="">Login</h1>
                    <div class="loginform">
		
					<div class="green"></div>
					<div class="red loginerror" style=" padding-bottom: 20px;color:red;"></div>
				</div>
			<form class="cmxform" action="<?php echo base_url('Login/logincheck') ?>" method="POST" id="commentForm" accept-charset="utf-8">
				
			<div class="row" style="padding-bottom: 20px;">	 
			<div <?php if(($this->session->flashdata('message')!='')){ ?> class="postErrorMsg" <?php } ?> > <?php { echo $this->session->flashdata('message'); }?></div>
			    

<div class="form-group"><label class="fields col-lg-5" for="email">Email Id<span class="star">*</span></label><div class="col-lg-6"> <input type="text" name="txtUsername" value="" required="true" class="required email" id="email"></div>
						</div>
						</div>
						<div class="row">
					<div class="form-group"><label class="fields col-lg-5" for="pwd">Password<span class="star">*</span></label><div class="col-lg-6"><input type="password" name="txtPassword" value=""  required="true" class="required" id="pwd"></div></div>
						</div>
					<div class="submitlabel"><input type="submit" class="btn btn-success" id="submit" name="submit" value="Login"></div>
					
			</form>
		
      			</div>
 			</div></div>

</div>
</div>
 <!-- Footer starts here-->
</body>
	 
</html>	
	
