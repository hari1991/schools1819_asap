     <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
	     <link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="contentInner">
 <div class="container">


<div class="bspiCalender">
 <h2> </h2> 

 <?php //print_r($skillwiseaverage); 

//echo $total= $var1 + $var2;

 $totalsets;

//echo $skillwiseaverage[0]['skillscorem'] + $skillwiseaverage[0]['skillscore2m'] + $skillwiseaverage[0]['skillscore3m'] 

//echo $set1avg_M = ($skillwiseaverage[0]['skillscorem'] + $skillwiseaverage[0]['skillscore2m'] + $skillwiseaverage[0]['skillscore3m']) / $totalsets;
//echo '<br/>';
//echo $set1avg_V = ($skillwiseaverage[0]['skillscorev'] + $skillwiseaverage[0]['skillscore2v'] + $skillwiseaverage[0]['skillscore3v']) / $totalsets;

//echo round($set1avg_M, 2); echo '<br/>';
//echo round($set1avg_V, 2); echo '<br/>';
//echo round($set1avg_F, 2); echo '<br/>';
//echo round($set1avg_P, 2);  echo '<br/>';
//echo round($set1avg_L, 2);  echo '<br/>';

$memory = round($set1avg_M, 2);
$visual = round($set1avg_V, 2);
$focus = round($set1avg_F, 2);
$problem = round($set1avg_P, 2);
$linguistics = round($set1avg_L, 2);


$test = array("Memory"=>$memory, "Visual Processing"=>$visual, "Focus and Attention"=>$focus, "Problem Solving"=>$problem, "Linguistics"=>$linguistics);
arsort($test);
//print_r($test);


?>
 
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
 <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>



<script>
$(document).ready(function()
{
	bspichart();
});
 function bspichart()
 {
	 
	 Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });
	
	 var chart = Highcharts.chart('container1', {

    chart: {
        type: 'column',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: true},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: ['Skill wise average'],
		gridLineWidth: 0,
  minorGridLineWidth: 0
       
    },
  plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true
        }
            }
        },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Score'
        },
		 max: 100 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0
		
    },
	


   
    series: [
	
	<?php foreach($test as $key=>$value){ 
	$color='';
	if($key=='Memory'){$color='#ff6600';}
	else if($key=='Visual Processing'){$color='#067d00';}
	else if($key=='Focus and Attention'){$color='#be0000';}
	else if($key=='Problem Solving'){$color='#1e9ddf';}
	else if($key=='Linguistics'){$color='#ffbe00';}
	?> 
	
	{
		
		showInLegend: false, 
        name: '<?php echo $key; ?>',
        data: [{y:<?php echo $value; ?>,color:{radialGradient: {cx: 0.5,cy: 0.3,r: 0.7},stops: [[0, "<?php echo $color; ?>"],[1, Highcharts.Color("<?php echo $color; ?>").brighten(-0.3).get('rgb')]]}}]
	},
	
	<?php } ?>
	 ] 

});
 }
	 
	 
	 
 
</script>
 
  <div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID1">Skill Wise Report</h2>
 <br/>
 <div id="container1">
</div>
 </div>
  
 </div> 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 

 
 </style>