var messageField;		//Message display field
 
var assets = [];
var choiceArr = [];
var choiceMcArr = []
var posArr = []
var qno = [];
var anss = [,0,1,2,3];
var cnt=-1,ans,uans,interval,delayInterval,time=180,totalQuestions=10,answeredQuestions=0,choiceCnt=4,quesCnt=0,resTimerOut=0,rst=0,responseTime=0,clk=0,correctCnt = 0;
var startBtn,introScrn,container,question,circleOutline,choice1,choice2,choice3,boardMc,helpMc,ansPanelMc,numPosition,resultLoading,questionText;
var parrotWowMc,parrotOopsMc,parrotGameOverMc,parrotTimeOverMc,btnImages,isCorrect="";
var bgSnd,correctSnd,wrongSnd,gameOverSnd,timeOverSnd,tickSnd,currTime =0;
var tqcnt=0, aqcnt=0, ccnt=0, cqcnt=0, gscore=0, gscrper=0, gtime=0,rtime=0,crtime=0,wrtime=0;
var namePos = [0,1,2,3,0,2,3,1,0,1,2,3,3,0,1,2,1,2,0,3,0,1,3,2,2,1,0,3,1,3,2,0,1,2,3,0,2,0,1,3,2,0,3,1,0,1,3,2,1,2,3,0,1,2,3,0,1,0,3,2,1,2,3,0,2,3,0,1,0,2,1,3,
                3,0,1,2,1,2,3,0,0,1,2,3,0,2,3,1,0,2,3,1,0,1,2,3,1,2,3,0]
var qno = [0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96]
var nameArr = [];
var btnX = ["178.3","258.3","338.3","418.3","498.3","578.3","658.3","258.3","338.3","418.3","498.3","578.3"];
var btnY = ["497.1","497.1","497.1","497.1","497.1","497.1","497.1","567.1","567.1","567.1","567.1","567.1"];

var btnPaddArr = ["","","","365","335","305","275","245","215","185"]

var btnPadding = 50;

var rst1 = 0, crst = 0, wrst = 0, score = 0;


var isBgSound = true;
var isEffSound = true;

var url = "";
var nav = "";
var isResp = true;
var respDim = 'both'
var isScale = true
var scaleType = 1;

var lastW, lastH, lastS = 1;

var borderPadding = 10, barHeight = 20;

var loadProgressLabel, progresPrecentage, loaderWidth;

//register key functions
///////////////////////////////////////////////////////////////////
window.onload = function (e) {
  checkBrowserSupport();
}
///////////////////////////////////////////////////////////////////

function init() {
    
    canvas = document.getElementById("gameCanvas");
    stage = new createjs.Stage(canvas);
    container = new createjs.Container();
    stage.addChild(container)
    createjs.Ticker.addEventListener("tick", stage);

    loaderColor = createjs.Graphics.getRGB(255, 51, 51, 1);
    loaderBar = new createjs.Container();
    var txt = new createjs.Container();
    bar = new createjs.Shape();
    bar.graphics.beginFill(loaderColor).drawRect(0, 0, 1, barHeight).endFill();
    loaderWidth = 300;

    //
    loadProgressLabel = new createjs.Text("", "20px Veggieburger-Bold", "black");
    loadProgressLabel.lineWidth = 400;
    loadProgressLabel.textAlign = "center";
    txt.addChild(loadProgressLabel)
    txt.x = 120;
    txt.y = 35;
    //
    var bgBar = new createjs.Shape();
    var padding = 3
    bgBar.graphics.setStrokeStyle(1).beginStroke(loaderColor).drawRect(-padding / 2, -padding / 2, loaderWidth + padding, barHeight + padding);
    loaderBar.x = 800 - loaderWidth >> 1;
    loaderBar.y = 800 - barHeight >> 1;
    loaderBar.addChild(bar, bgBar, txt);
    stage.addChild(loaderBar);

    createCanvasResize();

    stage.update();
    stage.enableMouseOver(40);

    var assetsPath = "assets/";
    var gameAssetsPath = "ArrangeTheWords-Level4/";
    var soundpath = "LI/"
    manifest = [

        { id: "parrotWowMc", src: assetsPath + "Parrot-Wow.png" },
        { id: "parrotOopsMc", src: assetsPath + "Parrot-Oops.png" },
        { id: "parrotGameOverMc", src: assetsPath + "Parrot-GameOver.png" },
        { id: "parrotTimeOverMc", src: assetsPath + "Parrot-TimeOver.png" },
        { id: "resultLoading", src: assetsPath + "ResultLoading.png" },

        { id: "backGround", src: gameAssetsPath + "Background.png" },
        { id: "introScreen", src: gameAssetsPath + "IntroScreen.png" },
        {id: "choice1", src: gameAssetsPath+"question.png"},
		{id: "position", src: gameAssetsPath+"positionImage1.png"},
        {id: "questionText", src: gameAssetsPath+"questiontext.png"},
         {id: "chHolder", src: gameAssetsPath+"chHolder.png"},

        { id: "startBtn", src: assetsPath + "StartButton.png" },
		{ id: "hPanel", src: assetsPath + "helpPanel.js" },
        { id: "qPanel", src: assetsPath + "quickPanel.js" },
        { id: "domainPath", src: redirectJsonPath + "redirecturl.json" },
        { id: "begin", src: assetsPath + soundpath + "bg_snd.ogg" },
        { id: "correct", src: assetsPath + "wow_s.ogg" },
        { id: "wrong", src: assetsPath + "oops_s.ogg" },
        { id: "gameOver", src: assetsPath + "Game_over.ogg" },
        { id: "timeOver", src: assetsPath + "timeover_s.ogg" },
        { id: "tick", src: assetsPath + "TickSound.ogg" }

    ];

    // createjs.FlashAudioPlugin.swfPath = "src/soundjs/flashaudio/";
    //createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashAudioPlugin]);
    createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin]);
    if (!createjs.Sound.initializeDefaultPlugins()) { return; }

    createjs.Sound.alternateExtensions = ["mp3"];
    createjs.WebAudioPlugin.playEmptySound();
    preload = new createjs.LoadQueue(true);
    preload.installPlugin(createjs.Sound);
    preload.addEventListener("complete", doneLoading); // add an event listener for when load is completed
    preload.addEventListener("fileload", fileLoaded);
    preload.addEventListener("progress", updateLoading);
    preload.loadManifest(manifest);
    stage.update();
}


function updateLoading(event) {
    bar.scaleX = event.loaded * loaderWidth;

    progresPrecentage = Math.round(event.loaded * 100);
    loadProgressLabel.text = progresPrecentage + "% Game Loading...";
    stage.update();
}
function fileLoaded(e) {
    assets.push(e);
}

function doneLoading(event) {

    loaderBar.visible = false;
    stage.update();
    for (i = 0; i < assets.length; i++) {
        var event = assets[i];
        var id = event.item.id;

        if (id == "backGround") {
            var bg = new createjs.Bitmap(preload.getResult('backGround'));
            container.parent.addChild(bg);
             continue;
        }
              
        if (id == "introScreen") {
            introScrn = new createjs.Bitmap(preload.getResult('introScreen'));
            container.parent.addChild(introScrn);
           // introScrn.x = 180;
           // introScrn.y = 240;
             continue;
        }

         if (id == "startBtn") {
            startBtn =  new createjs.Bitmap(preload.getResult('startBtn'));
            container.parent.addChild(startBtn);
            startBtn.x = 380; startBtn.y = 480;
            continue;
        }
      if(id == "chHolder"){
				chHolderMc  = new createjs.Bitmap(preload.getResult('chHolder'));
                chHolderMc.x=80;chHolderMc.y=120;
				container.parent.addChild(chHolderMc);    
				chHolderMc.visible = false;
                 continue;
			}
        
        if (id == "resultLoading") {
            resultLoading = new createjs.Bitmap(preload.getResult('resultLoading'));
            container.parent.addChild(resultLoading);
            resultLoading.visible = false;
             continue;
        }
        if(id == "choice1")
        {
            var spriteSheet1 = new createjs.SpriteSheet({
                framerate: 60,
                "images": [preload.getResult("choice1")],
                "frames": {"regX": 50, "height": 92, "count": 100, "regY": 50, "width": 175}
            });
            
            choice1 = new createjs.Sprite(spriteSheet1);
            choice1.visible = false;
            container.parent.addChild(choice1);
             continue;
        }
        
        if(id == "position"){
            var spriteSheet3 = new createjs.SpriteSheet({
                framerate: 60,
                "images": [preload.getResult("position")],
                "frames": {"regX": 50, "height": 63, "count": 64, "regY": 50, "width": 63}
            });
            numPosition = new createjs.Sprite(spriteSheet3);
            numPosition.visible = false;
            container.parent.addChild(numPosition);
             continue;
        }
		 
       if(id == "questionText" ){
           
            questionText = new createjs.Bitmap(preload.getResult('questionText'));
            container.parent.addChild(questionText);
            questionText.visible = false;
             continue;
        }
         
        if (id == "parrotWowMc") {
            parrotWowMc = new createjs.Bitmap(preload.getResult('parrotWowMc'));
            container.parent.addChild(parrotWowMc)
            parrotWowMc.cache(-0, -0, 800, 600);
            parrotWowMc.visible = false;
             continue;
        }

        if (id == "parrotOopsMc") {
            parrotOopsMc = new createjs.Bitmap(preload.getResult('parrotOopsMc'));
            container.parent.addChild(parrotOopsMc)
            parrotOopsMc.cache(-0, -0, 800, 600);
            parrotOopsMc.visible = false;
             continue;
        }

        if (id == "parrotGameOverMc") {
            parrotGameOverMc = new createjs.Bitmap(preload.getResult('parrotGameOverMc'));
            container.parent.addChild(parrotGameOverMc)
            parrotGameOverMc.cache(-0, -0, 800, 600);
            parrotGameOverMc.visible = false;
 continue;
        }
        if (id == "parrotTimeOverMc") {
            parrotTimeOverMc = new createjs.Bitmap(preload.getResult('parrotTimeOverMc'));
            container.parent.addChild(parrotTimeOverMc)
            parrotTimeOverMc.cache(-0, -0, 800, 600);
            parrotTimeOverMc.visible = false;
 continue;
        }

        if (id == "qPanel") {
            boardMc = new lib.quickPanel()
            container.parent.addChild(boardMc);
            boardMc.visible = false;
            console.log("get board= " + boardMc)
             continue;
        }
        if (id == "hPanel") {
            helpMc = new lib.helpPanel()
            container.parent.addChild(helpMc);
            helpMc.visible = false;
             continue;
        };


        if (id == "domainPath") {
            var json = preload.getResult("domainPath");
            console.log(json); // true
            url = json.path;
            nav = json.nav;
             continue;
        }

    }

    bgSnd = playSound("begin", 9999)//createjs.Sound.play("begin",9999);
    bgSnd.volume = 0;
    correctSnd = createjs.Sound.play("correct");
    correctSnd.volume = 0;
    wrongSnd = createjs.Sound.play("wrong");
    wrongSnd.volume = 0;
    gameOverSnd = createjs.Sound.play("gameOver");
    gameOverSnd.volume = 0;
    timeOverSnd = createjs.Sound.play("timeOver");
    timeOverSnd.volume = 0;
    tickSnd = createjs.Sound.play("tick", { interrupt: createjs.Sound.INTERRUPT_NONE, loop: -1, volume: 0.4 });
    tickSnd.volume = 0;

    // start the music
    createjs.Ticker.setFPS(40);
    //if (!createjs.Ticker.hasEventListener("tick")) {
    createjs.Ticker.addEventListener("tick", tick);
    //}
    createjs.Touch.enable(stage, true, true)

    watchRestart();
}

function tick(e) {

    stage.update();
}


function watchRestart() {
    //watch for clicks

    stage.addChild(messageField);
    
    boardMc.boardMc.secMc.txt.text = "180"
    boardMc.boardMc.qnsMc.txt.text = "1/" + String(totalQuestions)
    boardMc.boardMc.scoreMc.txt.text = "0";

    // apply font family
    boardMc.boardMc.secMc.txt.font = "bold 35px Veggieburger-Bold";
    boardMc.boardMc.qnsMc.txt.font = "bold 25px Veggieburger-Bold";
    boardMc.boardMc.scoreMc.txt.font = "bold 35px Veggieburger-Bold";


    startBtn.cursor = "pointer";
    startMc = new createjs.MovieClip(null, 0, true);
    container.parent.addChild(startMc)
    startMc.addChild(startBtn)
    startBtn.regX = 160;startBtn.regY = 160;
    startMc.timeline.addTween(createjs.Tween.get(startBtn).to({ scaleX: 0.9, scaleY: 0.9 }, 24).to({ scaleX: 1, scaleY: 1 }, 25).wait(1));
    startBtn.addEventListener("click", handleClick);
    stage.update(); 	//update the stage to show text;

}

function handleClick(e) {
    qno.sort(randomSort)
     toggleFullScreen();
    CreateGameStart()
    CreateGameElements()
    interval = setInterval(countTime, 1000);
}

function CreateGameElements(){
    helpMc.helpMc.contentMc.helpTxt.text = "Select the words in the alphabetical order";
    helpMc.helpMc.contentMc.helpTxt.font = "bold 35px Veggieburger-Bold";  
    helpMc.helpMc.contentMc.helpTxt.y = helpMc.helpMc.contentMc.helpTxt.y +40
    
    container.parent.addChild(boardMc);
    container.parent.addChild(questionText);
    questionText.visible = true;
    questionText.scaleX = questionText.scaleY = .8
    questionText.x= 270;questionText.y = 80;
    container.parent.addChild(choice1);
    container.parent.addChild(numPosition);
    numPosition.visible = false;
    var j;
    for(i=0;i<2;i++){
         j=i+1;
        choiceArr[j] = choice1.clone();
        choiceArr[j].visible = true;
        choiceArr[j].x = (i*350)+220;
        choiceArr[j].y = 220
        choiceArr[j].scaleX =.8;
        choiceArr[j].scaleY =.8;
        container.parent.addChild(choiceArr[j]);
        console.log("xpos= "+ choiceArr[j].x)
        posArr[j] = numPosition.clone()
     posArr[j].scaleX=posArr[j].scaleY=.7;
        posArr[j].x = (i*340)+270;
        posArr[j].y = 290;
        posArr[j].visible = false;
        container.parent.addChild(posArr[j]);
    }

     for(i=0;i<2;i++){
         j=i+3;
        choiceArr[j] = choice1.clone();
        choiceArr[j].visible = true;
        choiceArr[j].x = (i*350)+220;
        choiceArr[j].y = 430
        choiceArr[j].scaleX =.8;
        choiceArr[j].scaleY =.8;
        container.parent.addChild(choiceArr[j]);
        console.log("xpos= "+ choiceArr[j].x)
        posArr[j] = numPosition.clone()
        posArr[j].scaleX=posArr[j].scaleY=.7;
        posArr[j].x = (i*340)+270;
        posArr[j].y = 500;
        posArr[j].visible = false;
        container.parent.addChild(posArr[j]);
    }
   
    j=0;
    for(i=1;i<=choiceCnt;i++)
    {
          choiceArr[i].x = choiceArr[i].x-30;
           posArr[i].x = posArr[i].x-30;
            choiceArr[i].y = choiceArr[i].y+17;
           posArr[i].y = posArr[i].y+17;
    }
    pickques();
}
  function helpEnable(){
      for(i=1;i<=4;i++){
        choiceArr[i].mouseEnabled = true;
      }
  }

  function helpDisable(){
      for(i=1;i<=4;i++){
        choiceArr[i].mouseEnabled = false;
      }
  }

//=================================================================================================================================//
function pickques() {
    cnt++;
    quesCnt++;
    clk=0;
    correctCnt = 0;
    chpos = [];
    console.log("count "+namePos.length)
    ansArr = [];
    boardMc.boardMc.qnsMc.txt.text = quesCnt + "/" + totalQuestions; 
     boardMc.boardMc.openMc.mouseEnabled = false;
     boardMc.boardMc.openMc.alpha = .5;
     qno.sort(randomSort)
     var qn=qno[cnt];
    for(i=1;i<=4;i++){
       console.log("qnoj:"+qn);
        choiceArr[i].gotoAndStop(qn);
       choiceArr[i].name = "ch"+namePos[qn];
       console.log("ch name "+choiceArr[i])
       nameArr[i]=namePos[qn];
       console.log(nameArr[i])
        choiceArr[i].alpha = 1;
        choiceArr[i].scaleX =.8;
        choiceArr[i].scaleY =.8;
        posArr[i].visible = false;
        chpos.push({posx:choiceArr[i].x,posy:choiceArr[i].y})
        qn++;
        ansArr.push(choiceArr[i].name);
    } 
     chpos.sort(randomSort);
     boardMc.boardMc.openMc.mouseEnabled = true;
     boardMc.boardMc.openMc.alpha = 1;    
    var vCnt = 0;
    for(i=1;i<=4;i++)
     {
         console.log("swap working");
         choiceArr[i].x = chpos[i-1].posx;
         choiceArr[i].y = chpos[i-1].posy;
         console.log("xpos= "+ choiceArr[i].x)
         console.log("ypos= "+ choiceArr[i].y)
    }
    rst = 0;
    gameResponseTimerStart();
    createjs.Ticker.addEventListener("tick", tick);
    stage.update();
    enablechoices();
}

function enablechoices() {
    helpMc.helpMc.visible = false;
     boardMc.boardMc.openMc.mouseEnabled = true
    boardMc.boardMc.openMc.alpha = 1
      for(i=1;i<=4;i++){
        choiceMcArr[i] = new createjs.MovieClip()
        container.parent.addChild(choiceMcArr[i] )
        choiceMcArr[i].addChild(choiceArr[i])
        choiceArr[i].addEventListener("click", answerSelected);
        choiceArr[i].addEventListener("mouseover", onRoll_over);
        choiceArr[i].addEventListener("mouseout", onRoll_out)
        choiceArr[i].cursor = "pointer";
        choiceArr[i].scaleX = choiceArr[i].scaleY = 1;	
        choiceArr[i].scaleX =.85;
        choiceArr[i].scaleY =.85;
        choiceMcArr[i].timeline.addTween(createjs.Tween.get( choiceArr[i]).to({ scaleX: 0.9, scaleY: 0.9 }, 24).to({ scaleX: .85, scaleY: .85 }, 25).wait(1));
        choiceMcArr[i].play();
        }
}
//===============================================//
function disablechoices() {
    for(i=1;i<=4;i++){
        choiceArr[i].removeEventListener("click", answerSelected);
        choiceArr[i].removeEventListener("mouseover", onRoll_over);
        choiceArr[i].removeEventListener("mouseout", onRoll_out)
        choiceArr[i].cursor = "default";
    }
     boardMc.boardMc.openMc.mouseEnabled = false
    boardMc.boardMc.openMc.alpha = .5
}

function clearTweens()
{
    
}

function onRoll_over(e) {
    e.currentTarget.alpha = .5;
    stage.update();
}
function onRoll_out(e) {  
     e.currentTarget.alpha = 1;
    stage.update();
}

function answerSelected(e) {
    e.preventDefault();  
    e.currentTarget.removeEventListener("click", answerSelected);
    e.currentTarget.cursor = "default";
    e.currentTarget.disablechoices=true;
    var dx=e.currentTarget.x;
    var dy=e.currentTarget.y;
    console.log(dx);

    clk++;
    uans = e.currentTarget.name.split("ch")[1];
 console.log("uans "+uans)
     clearTweens()
    gameResponseTimerStop();
   // pauseTimer();
    
    if(clk<=4)
    {
        console.log(anss[clk]+" =$$$$$$$$$= "+ uans)
        if(anss[clk]==uans)
        {
            correctCnt++;
            if(dx==190 && dy==237)
            {
            posArr[1].visible = true;
            posArr[1].gotoAndStop(clk-1) 
            }
            else if(dx==540 && dy==237)
            {
                posArr[2].visible = true;
            posArr[2].gotoAndStop(clk-1) 
            }
            else  if(dx==190 && dy==447)
            {
            posArr[3].visible = true;
            posArr[3].gotoAndStop(clk-1) 
            }
            else if(dx==540 && dy==447)
            {
                posArr[4].visible = true;
                posArr[4].gotoAndStop(clk-1) 
            }else if(dx==590 && dy==347)
            {
                posArr[5].visible = true;
                posArr[5].gotoAndStop(clk-1) 
            }
            else if(dx==110 && dy==347)
            {
                posArr[6].visible = true;
                posArr[6].gotoAndStop(clk-1) 
            }

            choiceArr[correctCnt].alpha =  .5;
            if(correctCnt == 4){
                console.log("All correct")
                disablechoices();
                getValidation("correct");	
            } 
        }else{
            console.log("test")
            disablechoices();
            getValidation("wrong");
        }
    }
    
}

//===============================================================================================//
function playSound(id, loop) {
    return createjs.Sound.play(id, loop);
}
//===========================================================================================//
function range(max, min) {
    return Math.floor(min + (max - min) * Math.random());
}
//===========================================================================================//
function randomSort(a, b) {
    if (Math.random() < 0.5) return -1;
    else return 1;
}
//------------------------------------------------------------------------------------------//
function between(startNumber, endNumber) {
    var baseNumber = []
    var randNumber = []
    for (j = startNumber; j <= endNumber; j++) {
        baseNumber[j] = j;
    }
    for (j = endNumber; j > startNumber; j--) {
        var tempRandom = startNumber + Math.floor(Math.random() * (j - startNumber));
        randNumber[j] = baseNumber[tempRandom];
        baseNumber[tempRandom] = baseNumber[j];
    }
    randNumber[startNumber] = baseNumber[startNumber];
    return randNumber;
}
//-----------------------------------------------------------------------------------------//
