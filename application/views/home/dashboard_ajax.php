<div class="container">
 
  <!-- Trigger the modal with a button -->
  
   <?php	if($this->session->set3gradeid!=0) { $var1 = 1; }
            if($this->session->set2gradeid!=0) { $var2 = 1; } 
			$totalsets = $var1 + $var2 + 1; ?>

  <!-- Modal -->
  <div class="modal fade" id="completepopupmodal" role="dialog">
    <div class="modal-dialog">
     <?php $Greeting0='Needs Attention';
    $Greeting1='Needs Improvement';
    $Greeting2='Can Do Better';
    $Greeting3='Average';
    $Greeting4='Fair';
    $Greeting5='Good';
    $Greeting6='Very Good';
    $Greeting7='Excellent';
    $Greeting8='Extraordinary';
    $Greeting9='Genius';
    $Greeting10='Prodigy';

    if(round($bspi,2)<1){ $dispbspimsg=$Greeting0;}
    else if(round($bspi,2)<11){ $dispbspimsg=$Greeting1;}
    else if(round($bspi,2)<21){ $dispbspimsg=$Greeting2;}
    else if(round($bspi,2)<31){ $dispbspimsg=$Greeting3;}
    else if(round($bspi,2)<41){ $dispbspimsg=$Greeting4;}
    else if(round($bspi,2)<51){ $dispbspimsg=$Greeting5;}
    else if(round($bspi,2)<61){ $dispbspimsg=$Greeting6;}
    else if(round($bspi,2)<71){ $dispbspimsg=$Greeting7;}
    else if(round($bspi,2)<81){ $dispbspimsg=$Greeting8;}
    else if(round($bspi,2)<91){ $dispbspimsg=$Greeting9;}
    else if(round($bspi,2)<101){ $dispbspimsg=$Greeting10;} ?>
      <!-- Modal content-->
      <div class="modal-content"  style="background:rgba(0, 0, 0, 0);box-shadow:none;border:0">
       
        <div class="modal-body" >
		
		<?php if($totalsets==3) { ?>
		
	<?php if($controllername2=='mypuzzleset1') { ?>
	<h2 class="modal-title" style="text-align:center;font-weight:border;">Puzzle Completion</h2>
	<div class="popupimg"><img src="<?php echo base_url(); ?>assets/images/space_small.png" align="middle"/></div>
        <p style="font-weight:border;color:#fff;text-align:center;font-size:25px;">Awesome effort !! You are just two steps away from the launch</p>
		<?php } }
		
		elseif($totalsets==2)
		{ ?>
			<?php if($controllername2=='mypuzzleset1') { ?>
	<h2 class="modal-title" style="text-align:center;font-weight:border;">Puzzle Completion</h2>
	<div class="popupimg"><img src="<?php echo base_url(); ?>assets/images/space_small.png" align="middle"/></div>
        <p style="font-weight:border;color:#fff;text-align:center;font-size:25px;">Awesome effort !! You are just one step away from the launch</p>
		<?php }
		} ?>
		
		<?php
		if($controllername2=='mypuzzleset3') { ?>
		  <h2 class="modal-title" style="text-align:center;font-weight:border;">Puzzle Completion</h2>
		  <div class="popupimg"><img src="<?php echo base_url(); ?>assets/images/space_small.png" align="middle"/></div>
        <p style="font-weight:border;color:#fff;text-align:center;font-size:25px;">Wow!! You are really close just one step to go</p>
		<?php } ?>
		  
		 <?php  if($controllername2=='reports') { ?>
		  <h2 class="modal-title" style="text-align:center;font-weight:border;">Puzzle Completion</h2>
		  <div class="popupimg"><img src="<?php echo base_url(); ?>assets/images/space_small.png" align="middle"/></div>
        <p style="font-weight:border;color:#fff;text-align:center;font-size:25px;">Launch successfull, Time to celebrate</p>
		  <div style="text-align:center">
<h4 style="font-weight:border;color:#fff;">BSPI Score : <span class="updatedBSPI"><?php echo round($bspi,2); ?>%</span></h4>
<div class="progress">
  <div class="progress-bar progress-bar-striped active massive-font" role="progressbar"
  aria-valuenow="<?php echo round($bspi,2); ?>" aria-valuemin="0" aria-valuemax="100" style="background-color: #ff9854;width:<?php echo round($bspi,2); ?>%">
     <span>   <?php echo $dispbspimsg; ?> </span>

  </div>
</div>
<div id="chart-container"></div>
 
</div>
		  <div style="text-align:right;" ><a class="btn btn-success blink_me1 ViewReport" href="<?php echo base_url()."index.php/".$controllername2."/reportslist#View"; ?>" >Click here to see your <?php echo $setname; ?> </a></div>
		 
		 <?php } else {  ?>
		  
		  <div style="text-align:center">
<h4 style="font-weight:border;color:#fff;">BSPI Score : <span class="updatedBSPI"><?php echo round($bspi,2); ?>%</span></h4>
<div class="progress">
  <div class="progress-bar progress-bar-striped active massive-font" role="progressbar"
  aria-valuenow="<?php echo round($bspi,2); ?>" aria-valuemin="0" aria-valuemax="100" style="background-color: #ff9854;width:<?php echo round($bspi,2); ?>%">
    <span>   <?php echo $dispbspimsg; ?> </span>
 
  </div>
</div>
<div id="chart-container"></div>
 
</div>

		  <div style="text-align:right;" ><a  class="btn btn-success blink_me1 ViewReport"  href="<?php echo base_url()."index.php/".$controllername2."/dashboard#View"; ?>" >Click here to play next puzzle set <?php //echo $setname; ?> </a></div>
		 <?php } ?>
        </div>
        
      </div>
	  
	 
      
    </div>
  </div>
  
</div>
 <div class="clear_both"></div>
 <div class="section_four">
 <div class="container">
 <h2>Puzzles </h2>
 <?php if (isset($this->session->fullname) && ($this->session->fullname!=''))
 { ?>
  <h4> Welcome, <?php echo $this->session->fullname; ?></h4>
 <?php } ?>
 <div class="panel panel-default">

<div class="panel-body">

<div class="tab-content responsive">
<div class="tab-pane active" id="BrainSkillsOn">
<div class="tab_sec_c1">
 <h3> </h3>
<div class="row">
<?php
 $myskillpie=array("59"=>"#cdcdcd","60"=>"#cdcdcd","61"=>"#cdcdcd","62"=>"#cdcdcd","63"=>"#cdcdcd");
 //$myskillpie=array("59"=>"#ff6600","60"=>"#067d00","61"=>"#ffbe00","62"=>"#be0000","63"=>"#1e9ddf");
 $myskillpie_orginal=array("59"=>"#da0404","60"=>"#ffc000","61"=>"#92d050","62"=>"#ff6600","63"=>"#00b0f0");
?>
<?php $inCC=0;foreach($actualGames as $games)
{
	?>
	<div class="col-md-3 col-sm-3 col-xs-12 bounce" >

<div class="box_c1 bounceIn animated" style="background-color:<?php echo $myskillpie_orginal[$games['skill_id']]; ?>">
<h3 style="color:#fff;"><?php echo $actualGameCategory[$games['skill_id']]; ?></h3>
<?php
if($games['tot_game_score']==""){$games['tot_game_score']=0;}
if($games['tot_game_played']=="" || $games['tot_game_played']==0 ){$tot_game_played=1;}else{$myskillpie[$games['skill_id']]=$myskillpie_orginal[$games['skill_id']];$tot_game_played=$games['tot_game_played'];}
 
$avg_game_score=$games['tot_game_score']/$tot_game_played;
	if($avg_game_score < 20) $filled_stars = 0;
	if($avg_game_score >= 20 && $avg_game_score <= 40)	$filled_stars = 1;
	if($avg_game_score >= 41 && $avg_game_score <= 60)	$filled_stars = 2;
	if($avg_game_score >= 61 && $avg_game_score <= 80)	$filled_stars = 3;
	if($avg_game_score >= 81 && $avg_game_score <= 90)	$filled_stars = 4;
	if($avg_game_score >= 91 && $avg_game_score <= 100)	$filled_stars = 5;
	
	?>
<?php if($games['tot_game_played']<$games['ncount']){ ?>	
<a class="fancybox fancybox.iframe" id="<?php echo $games['gid']; ?>" href="javascript:;" data-href="<?php echo base_url()."assets/swf/".$this->session->userlang."/".$games['game_html'].".html"; ?>"><img src="<?php echo base_url(); ?>assets/<?php echo $games['img_path']; ?>"/></a>
<?php  } else {?>        
<a  href="javascript:;"><img src="<?php echo base_url(); ?>assets/<?php echo $games['img_path']; ?>"/></a>  
<?php } ?>

<p><?php echo $games['gname']; ?></p>
<p  style="background-color: #fff;box-shadow: 2px 0px 8px 2px rgb(183, 180, 180);" id="stars<?php echo $games['gid']; ?>">
 
<?php

	 
	
 for($i=0;$i<$filled_stars;$i++){ ?>
	 <img  class="staractive" src="<?php echo base_url(); ?>assets/images/icon_StarActive.png">
 <?php } ?>
  <?php for($i=0;$i<5-$filled_stars;$i++){  ?>
	 <img  class="starinactive" src="<?php echo base_url(); ?>assets/images/icon_StarInActive.png">
 <?php } ?>
</p>
<?php if($games['tot_game_played']<$games['ncount']){ ?>
<a style="" class="btn btn-default in_btn fancybox fancybox.iframe btn-4 btnactive" id="<?php echo $games['gid']; ?>" href="javascript:;"  data-href="<?php echo base_url()."assets/swf/".$this->session->userlang."/".$games['game_html'].".html"; ?>"><span><?php if($games['tot_game_played']==0){ echo "Play"; } else{echo "Re-play";}?></span></a>         
<?php  } else {$inCC++;?>       
<a style="" class="btn btn-default in_btn gameBtnInactive" href="javascript:;">Completed</a>  
<?php } ?>
</div>
</div>
<?php 
}
?>




</div><!--/row -->

</div><!--/tab_sec_c1 -->

</div><!--/tab-pane -->



</div><!-- tab-content -->
</div><!-- panel-body -->
</div><!-- panel-default -->
 </div><!-- container -->
 </div><!-- section_four -->

 <div class="clear_both"></div>
	

 <style>
/* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 99999999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
  
 <style>
 .btn-4:hover {
  -webkit-transform: scale(1.1);
    background: #31708f;
}
#fireworksField{z-index:9999;}
.modal{z-index:99999;}
.blink_me {
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 1500ms;
    -webkit-animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;

    -moz-animation-name: blinker;
    -moz-animation-duration: 1500ms;
    -moz-animation-timing-function: linear;
    -moz-animation-iteration-count: infinite;

    animation-name: blinker;
    animation-duration: 1500ms;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
}

@-moz-keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 40px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}

@-webkit-keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 40px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}

@keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 40px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}
 svg{    background-color:rgba(0, 0, 0, 0) !important; }  body{min-height:0 !important;}
 .massive-font{
font-size: 30px;
text-align: center;
}
  
.progress{
height:50px;
text-align:center;
}

.progress-bar {
  padding:15px;
}
 .progress {
    position: relative;
}

.bar {
    z-index: 1;
    position: absolute;
}

.progress span {
    position: absolute;
    top: 12px;
    z-index: 2;
    text-align: center;
   
    color: black;
}
 .ViewReport{background: #1713a5;font-size: 26px;border: 3px solid #fff;}
 </style>