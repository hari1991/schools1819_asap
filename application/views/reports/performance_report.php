<div id="main">

<?php error_reporting(0); //echo "<pre>"; print_r($bspireport); 


 $start = $academicyear[0]['startdate'];

$start = $month = strtotime($academicyear[0]['startdate']);
$end = strtotime($academicyear[0]['enddate']); 
?>


  
    <h2>Performance report</h2>
     <form class="cmxform" method="POST" id="commentForm" accept-charset="utf-8"> 
	 <table align="center" border="0" cellpadding="0" cellspacing="0"> 
	 <tr>
	 <td class="selectGame">Select Puzzle : </td>
	 <td> 
	<select id="chkveg1" multiple="multiple">
	<?php	while ($month <= $end) {
										//echo "$date\n";
										
										$date =  date('M-Y', $month);
    									$month = strtotime("+1 month", $month);?>
					
				<option value="<?php echo $date; ?>" <?php if(date('M-Y')==$date) { echo "selected"; } ?>> <?php echo $date; ?></option>
				
			<?php } ?>
			
	 </select>		 
	 <input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" />    
	 </td>
	 </tr>	 
	 </table>
	 <input type="button" value="Show result" id="btnreport" name="btnreport">
	 </form> 
  

<script src="<?php echo base_url(); ?>assets/js/plugins/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/exporting.js"></script>

<script src="<?php echo base_url(); ?>assets/js/fusioncharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fusioncharts.theme.fint.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fusioncharts.widgets.js"></script>


<div id="chart-container"></div>
<div id="score"></div>

<script type="text/javascript">

$(document).ready(function(){
	
$('#btnreport').click();
});

$("#btnreport").click(function(){
var form=$("#commentForm");
$.ajax({
type:"POST",
url:"<?php echo base_url('index.php/reports/mybspi_report_ajax') ?>",
data:form.serialize(),
//dataType: 'json',
success:function(result)
{
alert(result);
if(result!=0)
{
	//alert('heello');
	guagechart(result);
	var sco = Math.round(result);
	$('#score').html(sco);
}

}
});
});




function guagechart(score)
			{
			
	FusionCharts.ready(function () {
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container",
        "width": "400",
        "height": "250",
        "dataFormat": "json",
            "dataSource": {
    "chart": {
        "caption": "",
        "lowerlimit": "0",
        "upperlimit": "100",
        "lowerlimitdisplay": "",
        "upperlimitdisplay": "",
        "palette": "1",
        "numbersuffix": "",
        "tickvaluedistance": "10",
        "showvalue": "0",
        "gaugeinnerradius": "0",
        "bgcolor": "FFFFFF",
        "pivotfillcolor": "333333",
        "pivotradius": "8",
        "pivotfillmix": "333333, 333333",
        "pivotfilltype": "radial",
        "pivotfillratio": "0,100",
        "showtickvalues": "1",
        "showborder": "0"
    },
    "colorrange": {
        "color": [
            {
                "minvalue": "0",
                "maxvalue": "20",
                "code": "e44a00"
            },
            {
                "minvalue": "20",
                "maxvalue": "75",
                "code": "f8bd19"
            },
            {
                "minvalue": "75",
                "maxvalue": "100",
                "code": "6baa01"
            }
        ]
    },
    "dials": {
        "dial": [
            {
                "value": score,
                "rearextension": "15",
                "radius": "100",
                "bgcolor": "333333",
                "bordercolor": "333333",
                "basewidth": "8"
            }
        ]
    }
}
      });

csatGauge.render();
});	
			}

	





$(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	       
			            	    });
								
								
			</script>
	

</script>
<center>  
<b>X-Axis :</b>  Date Played <br> 
<b>Y-Axis:</b> Scored Points   
</center>
</div>
</div>

    
    
    
    
    
    
    
    

