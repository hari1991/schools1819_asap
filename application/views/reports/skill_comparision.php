<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styleinner.css">
 <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
	
	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
<link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">
<div class="" style="">
 <div class="">


<div class="">


 <?php //print_r($bspicalendarskillScore);  ?>
 
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>

<script src="<?php echo base_url(); ?>assets/js/pattern-fill.js"></script>



<script>
$(document).ready(function()
{
	multiplechart();
});
 function multiplechart()
 {
	 
	 
	 var chart = Highcharts.chart('container1', {

    chart: {
        type: 'column',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: false},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: ['Memory', 'Visual Processing', 'Focus and Attention', 'Problem Solving', 'Linguistics'],
		 gridLineWidth: 0,
  minorGridLineWidth: 0 ,
	labels: {
            style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        }
       
    },
  plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true,
			style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
        }
            }
        },
    yAxis: {
        allowDecimals: false,
		  
        title: {
            text: 'Score',
			style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        },
		 max: 100 ,
    gridLineWidth: 0,
  minorGridLineWidth: 0,
  labels: {
            style: {
                fontSize: '25px',
				color: '#FF6600',
				fontFamily: 'Phenomena-Regular'
            }
        }
		
    },
	
legend:{
    useHTML: true,
	symbolWidth: 0,
    labelFormatter: function() {
		var img;
    var name = this.name;
	if(name=="Puzzle set - 2"){
     img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/p2.png" width = "20px" height = "20px">';
	}
	else if(name=="Puzzle set - 1")
	{
		img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/p1.png" width = "20px" height = "20px">';
	}
	else{
		img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/p3.png" width = "20px" height = "20px">';
	}
    return img + '  ' + name;
  }
},

    series: [
	
	<?php if($this->session->set3planid!=0) { ?>
	{showInLegend: true, 
        name: 'Puzzle set - 3',
        data: [{y:<?php echo ($bspicalendarskillScore3['SID59']=='')?0:$bspicalendarskillScore3['SID59']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p3.png', width: 40, height: 40 }}, {y:<?php echo ($bspicalendarskillScore3['SID60']=='')?0:$bspicalendarskillScore3['SID60']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p3.png', width: 40, height: 40 }}, {y:<?php echo ($bspicalendarskillScore3['SID61']=='')?0:$bspicalendarskillScore3['SID61']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p3.png', width: 40, height: 40 }},{y:<?php echo ($bspicalendarskillScore3['SID62']=='')?0:$bspicalendarskillScore3['SID62']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p3.png', width: 40, height:40 }}, {y:<?php echo ($bspicalendarskillScore3['SID63']=='')?0:$bspicalendarskillScore3['SID63']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p3.png', width: 40, height: 40 }}]
	},
    
	<?php } ?>
	
	{
		
		showInLegend: true, 
        name: 'Puzzle set - 2',  
		   
        data: [{y:<?php echo $bspicalendarskillScore['SID59']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}, {y:<?php echo $bspicalendarskillScore['SID60']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}, {y:<?php echo $bspicalendarskillScore['SID61']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }},{y:<?php echo $bspicalendarskillScore['SID62']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}, {y:<?php echo $bspicalendarskillScore['SID63']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}]
	},
	
	<?php if($this->session->set2planid!=0) { ?>
	{showInLegend: true, 
        name: 'Puzzle set - 1',
		 
	data: [{y:<?php echo ($bspicalendarskillScore2['SID59']=='')?0:$bspicalendarskillScore2['SID59']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p1.png', width: 40, height: 40 }}, {y:<?php echo ($bspicalendarskillScore2['SID60']=='')?0:$bspicalendarskillScore2['SID60']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p1.png', width: 40, height: 40 }}, {y:<?php echo ($bspicalendarskillScore2['SID61']=='')?0:$bspicalendarskillScore2['SID61']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p1.png', width:40, height: 40 }},{y:<?php echo ($bspicalendarskillScore2['SID62']=='')?0:$bspicalendarskillScore2['SID62']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p1.png', width: 40, height: 40 }}, {y:<?php echo ($bspicalendarskillScore2['SID63']=='')?0:$bspicalendarskillScore2['SID63']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p1.png', width: 40, height: 40 }}]
	},
    
	<?php } ?>
	
	
	
	 ] 
});
 }
	 
	 
	 
 
</script>
 
  <div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <div><h2 id="MonthID1"><span class="col-lg-10">Skill comparison</span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>


 <div id="container1" style="background:#fff;padding-top:20px;border: 1px solid #ccc;" >
</div>
 </div>
  
 </div> 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 body{overflow: hidden;}
.highcharts-legend-item .highcharts-point{display:none;}
 
 </style>