(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(51,51,51,0.8)").ss(5,1,1).p("AioopQEOAAB6CBQBoBtAMDiQABAdAAAgQAACggtB+QgtB/hTBIQhwBhj2AAQjeAAhxheQhghOgriJQgWhFgKhIQgLhGAAhIQAAkgCKh2QCDhtEOAAgAcDoPIggCsIilNvImSAAIhknBIgMAAIhiHBImNAAIh4p+IhNmdIFzAAIAwFeIAiD7IALAAIArkMIA1lNIEgAAIAsEFIA4FWIAJAAIAwlrIAfjwgAy0CXIjRAAIAABXIAAEeIl9AAIAAjXIAAtEIF9AAIAAF0IDRAAIAAl0IF9AAIAAKPIAAGMIl9AAIAAlFgAi3jDQg7AAgkAbQg3AoAAB4QAAAXABAVQAGB1ArAxQAiAoBDAAQBcAAAihJQAYg2AAhlQAAgRAAgRQgGhtgrgkQghgehFAAg");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FAEBAE").s().p("A8CmUIF9AAIAAF0IDRAAIAAl0IF9AAIAAKOIl9BHIAAgwIjRAAIAABXIl9BHgArDBUQAAkfCKh2QCDhtEOAAQEOAAB6CBQBoBtAMDhIl0BFQgGhsgrgkQghgehFAAQg7AAgkAbQg3AoAAB3QAAAXABAVIlsBEQgLhFAAhJgAGImUIFzAAIAwFeIlWA+gAO4mUIEgAAIAsEFImBBIgAWUmUIFvAAIggCsIluBEg");
	this.shape_1.setTransform(-10.5,-12.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("An9FoQhghNgriJQgWhFgKhJIFshDQAGB1ArAwQAiApBDAAQBcAAAihJQAYg3AAhjIAAgjIF0hFQABAdAAAgQAACfgtB/QgtB/hTBHQhwBij2AAQjeAAhxhfgAS8GpIhknBIgMAAIhiHBImNAAIh4p+IFWg/IAiD7IALAAIArkMIGBhIIA4FXIAJAAIAwlsIFuhEIilNvgAykGpIAAlGIF9hHIAAGNgA7yGpIAAjYIF9hHIAAEfg");
	this.shape_2.setTransform(-12.1,9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-192.4,-57.9,364,115.8);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(51,51,51,0.8)").ss(5,1,1).p("ANRjDQhlAAg/AbQhgAoAAB4QAAAeADAaQAOBsBFAuQA8AoB1AAQCfAAA8hJQAFgHAFgHQAgg1AAhYQAAiJhXgqQg5geh4AAgANropQHYAADTCBQDLB7AAERQAACghNB+QgIANgIAMQhNBtiCBBQjCBhmuAAQmCAAjEheQimhOhKiJQhKiHAAiUQAAgdACgbQAWj0DWhqQDkhtHUAAgAi7oPIAAFqIjFAAIkAAAIAAKxIqmAAIAAqxIm6AAIAAktIAAg9g");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("AEuGgQinhOhJiJQhKiHAAiTQAAgdACgbIKJCNQANBsBGAuQA7AoB1AAQCfAAA8hJIAKgOIJLCAQhNBtiBBBQjDBhmuAAQmBAAjEhegAz3HgIAAqxIm6AAIAAksIVgEsIkAAAIAAKxg");
	this.shape_1.setTransform(-15.2,4.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BDF1FE").s().p("AQ5EiQAfg1AAhXQABiKhXgpQg5geh4AAQhmAAg+AbQhhAnAAB4QABAeADAaIqJiOQAWjzDWhqQDjhtHUAAQHYAADUCBQDLB7AAEQQAACghNB/IgRAYgAmAgdI1gktIAAg9IYlAAIAAFqg");
	this.shape_2.setTransform(-10.5,-13.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-189.1,-57.9,357.2,115.8);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(51,51,51,0.8)").ss(5,1,1).p("AGZBNIgmiZIgliUIgJAAIglCgIggCNgAHFFrIjvAAIggCkIleAAIB9odIB2n/IILAAIBeGRICYKLIllAAgAXpDdIAAEyIlgAAIAAkyIiWmJIiHliIFtAAIBhExIAAABIAIAAIABgDIBikvIFlAAIhhD6gA8JoKIJFAAQAzAAAxARQAxAQAfAbQBmBdAADKQAADFhJBlQgYAhgfAWQg0AkhLAOQgkAGggAAIjJAAIAAEdIlTAAIAAkVgA22gHIBHAAQAyAAAZgTQAlgeAAhFQAAhCgggaQgTgNgsAAIhYAAgAjUDDIAAFMIp4AAIAAmlIAAp3IFcAAIAAJDIAACNg");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CC00").s().p("AS5GSIAAkyIiWmJIFHgxIAAACIAIAAIABgDIFmg2IjAHxIAAEygAIXGSIgiikIjvAAIggCkIldAAIB8ocIFLgyIggCNICZAAIgmiZIFBgxICYKLgAscGSIAAmkIFdg0IAACMIEbAAIAAFMgA7YGSIAAkVIMVh3QgXAhggAWQgzAkhMAOQgkAGggAAIjIAAIAAEdg");
	this.shape_1.setTransform(-15.3,12.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CFFAA5").s().p("A8Il/IJEAAQAzAAAxARQAxAQAfAbQBnBcAADKQgBDFhJBlIsVB3gA22CDIBHAAQAyAAAZgUQAlgdAAhGQAAhBgggZQgTgOgsAAIhYAAgAtMmCIFdAAIAAJCIldA0gABKmCIILAAIBfGPIlCAxIgkiUIgJAAIglCgIlLAygANrmDIFuAAIBgEwIlGAygAWkmDIFlAAIhgD5IlmA2g");
	this.shape_2.setTransform(-10.5,-13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-193.1,-55.1,365.3,110.3);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(5,1,1).p("ACNDhIm1AAQA5hSBFh8QBYidAAg4QAAhOh+AAQhjAAiAAgIAAnPQDlgyDBAAQBxAABOATQBMATA+AsQB4BWAAChQAABwgxBhQg7B1gEAIgAk/ISQAAAwATApQATApAgAdQAgAfAqARQAqARAwAAQAvAAApgRQApgRAggfQAggdASgpQASgpAAgwQAAgvgSgpQgSgpgggdQgggegpgTQgpgRgvAAQgwAAgqARQgqATggAeQggAdgTApQgTApAAAvg");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3333").s().p("AivLhQgqgRghgfQgfgegTgpQgSgpgBgvQABgvASgpQATgpAfgdQAhgeAqgTQAqgRAvgBQAxABAnARQApATAhAeQAgAdASApQASApAAAvQAAAvgSApQgSApggAeQghAfgpARQgnAQgxAAQgvAAgqgQgAknDhQA5hSBFh8QBXidABg3QAAhPh+AAQhkAAiAAhIAAnQQDmgxDAAAQBxAABOASQBMAUA+AsQB4BVAAChQAABwgxBiIg/B8Ii4E7g");
	this.shape_1.setTransform(-10.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.6,-77.8,92.2,155.7);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFCC").s().p("AmMGMQikikAAjoQAAjnCkilQClikDnAAQDoAACkCkQClClAADnQAADoilCkQikCljoAAQjnAAililg");
	this.shape.setTransform(304.6,-3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF66").s().p("AqUKVQkSkRAAmEQAAmCESkSQERkSGDAAQGEAAERESQESESAAGCQAAGEkSERQkRESmEAAQmDAAkRkSgAmMmMQikClAADnQAADoCkCkQClClDnAAQDoAACkilQClikAAjoQAAjnililQikikjoAAQjnAAilCkg");
	this.shape_1.setTransform(304.6,-3.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("As5M6QlXlWAAnkQAAnjFXlWQFWlXHjAAQHkAAFWFXQFXFWAAHjQAAHklXFWQlWFXnkAAQnjAAlWlXgAqUqUQkSESAAGCQAAGEESERQERESGDAAQGEAAERkSQESkRAAmEQAAmCkSkSQkRkSmEAAQmDAAkRESg");
	this.shape_2.setTransform(304.6,-3.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AtaNbQljlkAAn3QAAnrFUlfIAPgQQFkljH2AAQH3AAFkFjQFjFkAAH2QAAH3ljFkIgQAOQlfFVnsAAQn2AAlkljgAs5s5QlXFWAAHjQAAHkFXFWQFWFXHjAAQHkAAFWlXQFXlWAAnkQAAnjlXlWQlWlXnkAAQnjAAlWFXg");
	this.shape_3.setTransform(304.6,-3.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC00").s().p("As5M6QlXlWAAnkQAAnjFXlWQFWlXHjAAQHkAAFWFXQFXFWAAHjQAAHklXFWQlWFXnkAAQnjAAlWlXgAqVqUQkRERAAGDQAAGDERESQESESGDAAQGDAAESkSQESkSAAmDQAAmDkSkRQkSkSmDAAQmDAAkSESg");
	this.shape_4.setTransform(-304.5,3.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFCC").s().p("AmMGNQikikAAjpQAAjoCkikQCkikDoAAQDpAACkCkQCkCkAADoQAADpikCkQikCkjpAAQjoAAikikg");
	this.shape_5.setTransform(-304.5,3.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF66").s().p("AqVKVQkRkSAAmDQAAmDERkRQESkSGDAAQGDAAESESQESERAAGDQAAGDkSESQkSESmDAAQmDAAkSkSgAmMmMQikCkAADoQAADpCkCkQCkCkDoAAQDpAACkikQCkikAAjpQAAjoikikQikikjpAAQjoAAikCkg");
	this.shape_6.setTransform(-304.5,3.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AtaNaQljljAAn3QAAnrFVlfIAOgQQFkljH2AAQH3AAFjFjQFkFkAAH2QAAH3lkFjIgPAQQlfFUnsAAQn2AAlklkgAs5s5QlXFWAAHjQAAHkFXFWQFWFXHjAAQHkAAFWlXQFXlWAAnkQAAnjlXlWQlWlXnkAAQnjAAlWFXg");
	this.shape_7.setTransform(-304.5,3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-425.9,-125.1,851.9,250.2), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFCC").s().p("AjGDHQhShTAAh0QAAhzBShTQBThSBzAAQB0AABSBSQBSBTAABzQAAB0hSBTQhSBSh0AAQhzAAhThSg");
	this.shape.setTransform(182.5,-2.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF66").s().p("AlJFKQiKiJAAjBQAAjACKiJQCIiJDBAAQDBAACKCJQCICJAADAQAADBiICJQiKCJjBAAQjBAAiIiJgAjGjGQhSBTAABzQAAB0BSBTQBTBSBzAAQB0AABShSQBShTAAh0QAAhzhShTQhShSh0AAQhzAAhTBSg");
	this.shape_1.setTransform(182.5,-2.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AmdGdQiqirgBjyQABjxCqirQCsirDxAAQDyAACrCrQCsCrgBDxQABDyisCrQirCrjyAAQjxAAisirgAlJlJQiKCJAADAQAADBCKCJQCICJDBAAQDBAACKiJQCIiJAAjBQAAjAiIiJQiKiJjBAAQjBAAiICJg");
	this.shape_2.setTransform(182.5,-2.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AmsGtQiyixAAj8QAAj1CrivIAHgIQCxiyD7AAQD7AACyCyQCyCxAAD7QAAD8iyCxIgIAIQivCqj2AAQj7AAixiygAmdmcQiqCrgBDxQABDyCqCrQCsCrDxAAQDyAACrirQCsirgBjyQABjxisirQirirjyAAQjxAAisCrg");
	this.shape_3.setTransform(182.5,-2.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC00").s().p("AmcGdQisirABjyQgBjxCsisQCrirDxAAQDyAACsCrQCqCsABDxQgBDyiqCrQisCsjyAAQjxAAirisgAlKlKQiICJAADBQAADCCICJQCKCJDAAAQDCAACIiJQCKiJAAjCQAAjBiKiJQiIiJjCAAQjAAAiKCJg");
	this.shape_4.setTransform(-182.5,2.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFCC").s().p("AjFDGQhShSAAh0QAAhzBShTQBShRBzAAQB0AABTBRQBSBTAABzQAAB0hSBSQhTBSh0AAQhzAAhShSg");
	this.shape_5.setTransform(-182.5,2.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF66").s().p("AlKFLQiIiJAAjCQAAjBCIiJQCKiJDAAAQDCAACICJQCKCJAADBQAADCiKCJQiICJjCAAQjAAAiKiJgAjFjGQhSBTAABzQAAB0BSBSQBSBSBzAAQB0AABThSQBShSAAh0QAAhzhShTQhThRh0AAQhzAAhSBRg");
	this.shape_6.setTransform(-182.5,2.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AmsGtQiyiyAAj7QAAj1CqiwIAIgHQCyiyD6AAQD8AACxCyQCyCxAAD7QAAD7iyCyIgHAIQiwCqj2AAQj6AAiyiygAmcmdQisCsABDxQgBDyCsCrQCrCsDxAAQDyAACsisQCqirABjyQgBjxiqisQisirjyAAQjxAAirCrg");
	this.shape_7.setTransform(-182.5,2.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-243.2,-63.4,486.4,126.9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-0.8,243.5);
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, 33))];
	this.instance.cache(-245,-65,490,131);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFF00","#FF9900"],[0.718,0.976],0,0,0,0,0,295.7).s().p("EggSAgSQtXtXAAy7QAAy6NXtYQNYtXS6AAQS7AANXNXQNZNYgBS6QABS7tZNXQtXNZy7gBQy6ABtYtZgA5050QqtKsAAPIQAAPJKtKsQKsKtPIAAQPJAAKsqtQKtqsAAvJQAAvIqtqsQqsqtvJAAQvIAAqsKtg");
	this.shape.setTransform(-5.3,-12.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EghhAhiQt5t5AAzpQAAzNNTtuIAmgmQN5t5ToAAQTpAAN5N5QN5N5AAToQAATpt5N5IgmAmQtuNTzOAAQzoAAt5t5gEggSggSQtXNYAAS6QAAS7NXNXQNYNZS6gBQS7ABNXtZQNZtXgBy7QABy6tZtYQtXtXy7AAQy6AAtYNXg");
	this.shape_1.setTransform(-5.3,-12.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFCC").s().p("AvfPgQmbmbAApFQAApEGbmbQGbmbJEAAQJFAAGbGbQGbGbAAJEQAAJFmbGbQmbGbpFAAQpEAAmbmbg");
	this.shape_2.setTransform(-5.3,-12.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF66").s().p("A50Z1QqtqsAAvJQAAvIKtqsQKsqtPIAAQPJAAKsKtQKtKsAAPIQAAPJqtKsQqsKtvJAAQvIAAqsqtgAvfvfQmbGbAAJEQAAJFGbGbQGbGaJEAAQJFAAGbmaQGambAApFQAApEmambQmbmbpFAAQpEAAmbGbg");
	this.shape_3.setTransform(-5.3,-12.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,190.9);
	this.instance_1.filters = [new cjs.ColorFilter(0.48, 0.48, 0.48, 1, 132.6, 132.6, 132.6, 0), new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, 18))];
	this.instance_1.cache(-428,-127,856,254);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-425.9,-316,854,636.8), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EggRAgSQtZtXABy7QgBy6NZtXQNXtZS6ABQS7gBNXNZQNZNXgBS6QABS7tZNXQtXNZy7gBQy6ABtXtZg");
	mask.setTransform(0,31.5);

	// Layer_2
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(237,-87.4);
	this.instance.alpha = 0.328;
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(30).to({_off:false},0).to({y:138.5,alpha:1},4).to({y:63.2},3).to({y:138.5},3).to({startPosition:0},34).wait(1));

	// Layer_3
	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.2,138.5,2,2);
	this.instance_1.alpha = 0.191;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(24).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},6).to({startPosition:0},44).wait(1));

	// Layer_4
	this.instance_2 = new lib.Tween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.6,13.4,2,2,-25.2);
	this.instance_2.alpha = 0.191;
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0,x:-0.5,alpha:1},7).to({startPosition:0},50).wait(1));

	// Layer_5
	this.instance_3 = new lib.Tween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(2.9,-107.3,2,2,26.2);
	this.instance_3.alpha = 0.191;
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(10).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0,x:2.8,alpha:1},7).to({startPosition:0},57).wait(1));

	// Layer_6
	this.instance_4 = new lib.Symbol1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(5.3,44);
	this.instance_4.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, -58))];
	this.instance_4.cache(-428,-318,858,641);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-420.6,-272,858,640);


// stage content:
(lib.HowToPlay = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.howToPlayMc = new lib.Symbol4();
	this.howToPlayMc.name = "howToPlayMc";
	this.howToPlayMc.parent = this;
	this.howToPlayMc.setTransform(640.1,324.5,0.64,0.64,0,0,0,5.4,-11.6);

	this.timeline.addTween(cjs.Tween.get(this.howToPlayMc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1007.4,517.8,553,413);
// library properties:
lib.properties = {
	id: '509C8CAA7D4C684A8790B1A978774CA5',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#66FFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['509C8CAA7D4C684A8790B1A978774CA5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;