<footer>
<div class="container" id="footerpart">
<div class="row">
   
	  <div class="col-sm-6 col-lg-6 col-md-6">
	 <div class="row">
		<div class="col-sm-6 col-lg-6 col-md-6">
			<p style="text-align:center;color: #92278f;font-size: 20px;margin-bottom:10px"><strong>EDSIX BRAINLAB<sup>TM</sup> PRIVATE LTD</strong></p>
			<p style="text-align:center;margin-bottom:0px;margin-bottom:10px">Module #1, 3rd Floor, D Block,</p> 
			<p style="text-align:center;margin-bottom:0px;margin-bottom:10px">Phase 2, IITM Research Park,</p> 
			<p style="text-align:center;margin-bottom:0px;margin-bottom:10px">Kanagam Road, Taramani, Chennai - 600113 </p> 
		</div>
		<div class="col-sm-6 col-lg-6 col-md-6">
		 
			<p style="text-align:center;color: #92278f;font-size: 20px;margin-bottom:10px" class="callicon">044-66469877<br/>+91 95695 65454</p> 
			<p style="text-align:center;color: #92278f;font-size: 20px;margin-bottom:10px" class="msgicon"><a style="color: #92278f;" href="mailto:angel@skillangels.com">angel@skillangels.com</a></p>
  

		</div>
		</div>
		  
		  
     </div>    
				  
				 
               
				 
                <div class="col-sm-4 col-lg-6 col-md-6 ">
     <p style="text-align:center; margin-bottom:0px">  <a class="" href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/index_logo.png" width="150px" alt="logo"></a></p> 
	 <p style="text-align:center; margin-bottom:0px">EdSix Brain Lab<sup>TM</sup> Pvt Ltd</p> 
     <p style="text-align:center; margin-bottom:0px">Incubated by IIT Madras' RTBI</p> 
     <p style="text-align:center; margin-bottom:0px">Supported by IIM Ahmedabad's CIIE and Village Capital, U.S.A</p> 
                      
                </div>
            </div>
</div>

</footer>

  <script src="<?php echo base_url(); ?>assets/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('ready', function() {
      $(".regular").slick({
        dots: false,
		arrows: true,
		infinite: true,
		 autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1
      });
	  
$('.medialogo').slick({
  dots: true,
  arrows: false,
  autoplay: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
    });
  </script>
<script type="text/javascript">
  $(document).ready(function(e) { 
   $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 6000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
});
</script>
<script>

setInterval(LoginAjaxCall, 1000*60*2); //300000 MS == 5 minutes
LoginAjaxCall();
function LoginAjaxCall(){ //alert("KKKKKKKKKK");
   $.ajax({
			type:"POST",
			url:"<?php echo base_url('index.php/home/checkuserisactive') ?>",
			success:function(result)
			{	//alert(result);
				if(result==1)
				{ 
					
					window.location.href= "<?php echo base_url();?>index.php";
				}		
			}
		});
}
</script>

	 <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/commonscript.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url(); ?>assets/js/ie10-viewport-bug-workaround.js"></script>
	 
</body>
</html>