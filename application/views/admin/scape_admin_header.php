<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Schools admin</title>
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/admin/img/metis-tile.png" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.css">   
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css">   
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/metisMenu.css">    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/animate.css">
	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url(); ?>assets/admin/css/theme.less">
	
	


	
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>


</head>

 <body class="  ">
            <div class="bg-dark dk" id="wrap">
                <div id="top">
                    <!-- .navbar -->
                    <nav class="navbar navbar-inverse navbar-static-top">
                        <div class="container-fluid">
                    
                    
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <header class="navbar-header">
                    
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="javascript:;" class="navbar-brand"><img src="<?php echo base_url(); ?>assets/images/logo.png" width="150" alt=""></a>
                    
                            </header>
							
							<div class="collapse navbar-collapse navbar-ex1-collapse">
                    
                                <!-- .nav -->
                                <ul class="nav navbar-nav">
                                    <!--<li <?php if($this->uri->segment(2)=="dashboard"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/admin/dashboard">Dashboard</a></li>-->

										<li  <?php if($this->uri->segment(2)=="dashboard"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/scape/dashboard">Dashboard</a></li> 
										<?php //if($this->session->role==1){ ?>
										<li <?php if($this->uri->segment(2)=="report1"){echo 'class="active"';}?>><a href="<?php echo base_url(); ?>index.php/scape/report1" class="dropdown-toggle" data-toggle="dropdown">BSPI Report</a>
										</li>
										<?php //} ?>
                                        <!--<ul class="dropdown-menu">
                                            <li><a href="javascript:;">Skill Report</a></li>
                                            <li><a href="javascript:;">BSPI report</a></li>                                          
                                        </ul>-->
										
										<li class=""><a href="<?php echo base_url(); ?>index.php/scape/logout">Logout</a></li>    
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
							
							
							</div>
							</nav>
							</div>
							</div>



