
<link href="<?php echo base_url(); ?>assets/css/banner/parallax_mouseinteraction.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/testimonial/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/testimonial/owl.theme.css">
<div class="headerTop">
<div id="parallax_mouseinteraction_thumbs">
<div class="myloader"></div>
			<ul class="parallax_mouseinteraction_list">
                   <!-- <li data-text-id="#parallax_mouseinteraction_photoText1" data-bottom-thumb="images/thumbsFullWidth/thumbs/01_thumbs.png">
                    	<img src="<?php echo base_url(); ?>assets/images/livelinks/1.jpg" width="1920" height="389" alt="" />
                    </li>--> 				
                    
                    <li data-text-id="#parallax_mouseinteraction_photoText1" data-bottom-thumb="<?php echo base_url(); ?>assets/images/banner/skl-banner01_thumbs.jpg"><img src="<?php echo base_url(); ?>assets/images/banner/skl-banner01.jpg" width="1920" height="750" alt="" /></li>                 
                    
                    <li data-text-id="#parallax_mouseinteraction_photoText2" data-bottom-thumb="<?php echo base_url(); ?>assets/images/banner/skl-banner02_thumbs.jpg">
                    	<img src="<?php echo base_url(); ?>assets/images/banner/skl-banner02.jpg" width="1920" height="750" alt="" />
                    </li>
					
					<li data-text-id="#parallax_mouseinteraction_photoText3" data-bottom-thumb="<?php echo base_url(); ?>assets/images/banner/skl-banner03_thumbs.jpg"><img src="<?php echo base_url(); ?>assets/images/banner/skl-banner03.jpg" width="1920" height="750" alt="" /></li>	
          
            </ul>    
                
	<!-- TEXTS --> 

				<div id="parallax_mouseinteraction_photoText1" class="parallax_mouseinteraction_texts">

                    <div class="parallax_mouseinteraction_text_line textElement31_thumbsFW" data-initial-left="1300" data-initial-top="294" data-final-left="1300" data-final-top="334" data-duration="1.5" data-fade-start="0" data-delay="2.0" data-easing="easeOutElastic" data-exit-left="-200" data-exit-top="134" data-exit-duration="1.5" data-exit-delay="0.4"><span style="color:#d51220;"></span></div>
                    
                    <div class="parallax_mouseinteraction_text_line textElement30_thumbsFW" data-initial-left="1300" data-initial-top="283" data-final-left="220" data-final-top="320" data-duration="1.5" data-fade-start="0" data-delay="2.0" data-easing="easeOutElastic" data-exit-left="-200" data-exit-top="113" data-exit-duration="1.5" data-exit-delay="0.2">Game-based learning <br/> framework</div>
                    
                  <div class="parallax_mouseinteraction_text_line textElement32_thumbsFW" data-initial-left="0" data-initial-top="380" data-final-left="220" data-final-top="420" data-duration="1.5" data-fade-start="0" data-delay="2.3" data-easing="easeOutElastic" data-exit-left="-100" data-exit-top="-248" data-exit-duration="1.5" data-exit-delay="0.4"><span style="color:#000;"> - Competitive <br/> - interactive <br/> - fun filled </span></div>
                  
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="1500" data-initial-top="-320" data-final-left="1550" data-final-top="250" data-duration="1.7" data-fade-start="0.4" data-delay="1.5" data-exit-left="1500" data-exit-top="-1000" data-exit-duration="1.5" data-exit-delay="0.3"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb22.png" width="119" height="124" alt=""/></div>
                    
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="0" data-initial-top="20" data-final-left="450" data-final-top="20" data-duration="1.7" data-fade-start="0.4" data-delay="0.4" data-easing="swing" data-exit-left="446" data-exit-top="20" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb14.png" width="119" height="124" alt=""/></div>   
                                     
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="-860" data-initial-top="-120" data-final-left="1300" data-final-top="50" data-duration="1.1" data-fade-start="0" data-delay="0.8" data-easing="swing" data-exit-left="-300" data-exit-top="-70" data-exit-duration="1.5" data-exit-delay="0.6"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb24.png" width="130" height="130" alt=""/></div>
					
						<div  class="parallax_mouseinteraction_text_line" data-initial-left="150" data-initial-top="-520" data-final-left="1650" data-final-top="500" data-duration="1.7" data-fade-start="0.4" data-delay="2.2" data-easing="easeOutBounce" data-exit-left="1131" data-exit-top="-900" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb30.png" width="119" height="124" alt=""/></div>
					
					 <div  class="parallax_mouseinteraction_text_line" data-initial-left="1131" data-initial-top="-520" data-final-left="150" data-final-top="180" data-duration="1.7" data-fade-start="0.4" data-delay="2.2" data-easing="easeOutBounce" data-exit-left="1131" data-exit-top="-900" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb01.png" width="119" height="124" alt=""/></div>
					 
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="100" data-initial-top="-520" data-final-left="100" data-final-top="500" data-duration="1.7" data-fade-start="0.4" data-delay="2.2" data-easing="easeOutBounce" data-exit-left="1131" data-exit-top="-900" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb05.png" width="119" height="124" alt=""/></div>
                    
               </div><!-- End photoText3 -->
                <div id="parallax_mouseinteraction_photoText2" class="parallax_mouseinteraction_texts">
               		                  
                                       
                    <div class="parallax_mouseinteraction_text_line textElement30_thumbsFW" data-initial-left="1100" data-initial-top="283" data-final-left="950" data-final-top="130" data-duration="1.5" data-fade-start="0" data-delay="2.0" data-easing="easeOutElastic" data-exit-left="-200" data-exit-top="113" data-exit-duration="1.5" data-exit-delay="0.2"><span style="color:#eeff20;">Puzzle-based learning (PBL)</span></div>
                    
					<div class="parallax_mouseinteraction_text_line textElement32_thumbsFW" data-initial-left="0" data-initial-top="380" data-final-left="950" data-final-top="170" data-duration="1.5" data-fade-start="0" data-delay="2.3" data-easing="easeOutElastic" data-exit-left="-100" data-exit-top="-248" data-exit-duration="1.5" data-exit-delay="0.4"><span style="color:#fff;"> Emerging model of teaching Critical thinking and Problem solving</span></div>
                  
					<div  class="parallax_mouseinteraction_text_line" data-initial-left="1300" data-initial-top="20" data-final-left="300" data-final-top="20" data-duration="1.7" data-fade-start="0.4" data-delay="0.4" data-easing="swing" data-exit-left="446" data-exit-top="20" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb27.png" width="130" height="130" alt=""/></div>
					
					<div  class="parallax_mouseinteraction_text_line" data-initial-left="1300" data-initial-top="20" data-final-left="150" data-final-top="220" data-duration="1.7" data-fade-start="0.4" data-delay="0.4" data-easing="swing" data-exit-left="446" data-exit-top="20" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb25.png" width="130" height="130" alt=""/></div>
					
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="1200" data-initial-top="-320" data-final-left="850" data-final-top="40" data-duration="1.7" data-fade-start="0.4" data-delay="1.5" data-exit-left="1348" data-exit-top="-1000" data-exit-duration="1.5" data-exit-delay="0.3"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb07.png" width="130" height="130" alt=""/></div>
                    
                    
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="1324" data-initial-top="0" data-final-left="1600" data-final-top="150" data-duration="1.9" data-fade-start="0" data-delay="0.3" data-easing="swing" data-exit-left="165" data-exit-top="86" data-exit-duration="1.0" data-exit-delay="0.1" data-continuous-left="1600" data-continuous-top="94" data-continuous-duration="0.8" data-continuous-easing="linear" data-enableMouseInteraction="false"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb20.png" width="130" height="130" alt=""/></div>
                    
                </div>
			    <div id="parallax_mouseinteraction_photoText3" class="parallax_mouseinteraction_texts">
               		
                    <div class="parallax_mouseinteraction_text_line textElement31_thumbsFW" data-initial-left="1300" data-initial-top="294" data-final-left="1300" data-final-top="334" data-duration="1.5" data-fade-start="0" data-delay="2.0" data-easing="easeOutElastic" data-exit-left="-200" data-exit-top="134" data-exit-duration="1.5" data-exit-delay="0.4"><span style="color:#d51220;"></span></div>
                    
                    <div class="parallax_mouseinteraction_text_line textElement30_thumbsFW" data-initial-left="1450" data-initial-top="283" data-final-left="150" data-final-top="320" data-duration="1.5" data-fade-start="0" data-delay="2.0" data-easing="easeOutElastic" data-exit-left="-200" data-exit-top="113" data-exit-duration="1.5" data-exit-delay="0.2">Technology for Good</div>
                    
                  <div class="parallax_mouseinteraction_text_line textElement32_thumbsFW" data-initial-left="1450" data-initial-top="380" data-final-left="150" data-final-top="370" data-duration="1.5" data-fade-start="0" data-delay="2.3" data-easing="easeOutElastic" data-exit-left="-100" data-exit-top="-248" data-exit-duration="1.5" data-exit-delay="0.4"><span style="color:#000;"> Imparting 21st Century Skills</span></div>
                  
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="1550" data-initial-top="-320" data-final-left="1550" data-final-top="250" data-duration="1.7" data-fade-start="0.4" data-delay="1.5" data-exit-left="1348" data-exit-top="-1000" data-exit-duration="1.5" data-exit-delay="0.3"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb06.png" width="119" height="124" alt=""/></div>
                    
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="0" data-initial-top="20" data-final-left="450" data-final-top="20" data-duration="1.7" data-fade-start="0.4" data-delay="0.4" data-easing="swing" data-exit-left="446" data-exit-top="20" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb22.png" width="119" height="124" alt=""/></div>   
                     <div  class="parallax_mouseinteraction_text_line" data-initial-left="0" data-initial-top="20" data-final-left="1400" data-final-top="20" data-duration="1.7" data-fade-start="0.4" data-delay="0.4" data-easing="swing" data-exit-left="446" data-exit-top="20" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb15.png" width="119" height="124" alt=""/></div>            
                                 
				
					
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="150" data-initial-top="-520" data-final-left="1650" data-final-top="500" data-duration="1.7" data-fade-start="0.4" data-delay="2.2" data-easing="easeOutBounce" data-exit-left="1131" data-exit-top="-900" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb11.png" width="119" height="124" alt=""/></div>
					
					<div  class="parallax_mouseinteraction_text_line" data-initial-left="600" data-initial-top="-520" data-final-left="190" data-final-top="180" data-duration="1.1" data-fade-start="0" data-delay="0.8" data-easing="swing" data-exit-left="-300" data-exit-top="-70" data-exit-duration="1.5" data-exit-delay="0.6"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb08.png" width="130" height="130" alt=""/></div>.
					
                    <div  class="parallax_mouseinteraction_text_line" data-initial-left="150" data-initial-top="-520" data-final-left="150" data-final-top="500" data-duration="1.7" data-fade-start="0.4" data-delay="2.2" data-easing="easeOutBounce" data-exit-left="1131" data-exit-top="-900" data-exit-duration="1.5" data-exit-delay="0.2"><img src="<?php echo base_url(); ?>assets/images/banner/npngs/scrb03.png" width="119" height="124" alt=""/></div>
                    
                </div>
			   <!-- End photoText3 -->
</div>
</div>

<!--<video style="width:100%; position:relative;"  controls="none" id="bgvid" playsinline autoplay>
<source src="<?php echo base_url(); ?>assets/images/classroom.mp4" type="video/mp4" >
</video>-->
<!--<div class="swiper-slide caption1 swiper-slide-visible swiper-slide-active no-transform" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle" >
    <div class="video-wrap" style="width: 100%; height: auto;">
      <video style="width:100%; position:relative;"  controls="none" id="bgvid" playsinline autoplay>
        <source src="images/classroom.mp4" type="video/mp4" >
      </video>
    </div>
  </div>
  <div class="tagline">Be a part of revolution</div>-->
</div>
<div id="quoteslider">
	 <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="tcb-quote-carousel">
                    <div class="quote"><i class=" fa fa-quote-left fa-4x"></i></div>
                    <div class="carousel slide carousel-fade" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                        <!-- Carousel indicators -->
                        <!--<ol class="carousel-indicators">
                            <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                        </ol>-->
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="profile-circle"><img class="img-responsive " src="<?php echo base_url(); ?>assets/images/home/Albert.png" alt=""></div>
                                <blockquote>
                                    <p>“Play is the highest form of research” – Albert Einstein</p>
                                </blockquote>
                            </div>
                            <div class="item">
                                <div class="profile-circle"><img class="img-responsive " src="<?php echo base_url(); ?>assets/images/home/Diane.png" alt=""></div>
                                <blockquote>
                                    <p>“Play is our brain’s favourite way of learning.” – Diane Ackerman</p>
                                </blockquote>
                            </div>
							<div class="item">
                                <div class="profile-circle"><img class="img-responsive " src="<?php echo base_url(); ?>assets/images/home/Plato.png" alt=""></div>
                                <blockquote>
                                    <p>“Do not keep children to their studies by compulsion but by play.” – Plato</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="skillPuzzle">
	<div class="container">
		<h1><?php echo $this->lang->line("centertext"); ?></h1>
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2><?php echo $this->lang->line("countermessage1"); ?></h2>
				<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s"><div class="odometer"  id="odo1" ></div></div>
			</div>
			<div class="col-md-6 col-sm-12">
				<h2><?php echo $this->lang->line("countermessage2"); ?></h2>
				<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" ><div class="odometer" id="odo2" ></div></div>
			</div>
		</div>
	</div>
</div>
<div id="testimonial">
	<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div id="testimonial-slider" class="owl-carousel">
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi01a.png" width="50px" height="50px" alt="">
							</div>
							<p class="description">
								It’s an innovative methodology to inculcate cognitive skills among children.
							</p>
							<h3 class="testimonial-title">
								Ms R.Dheivanai 
								<small class="post">Primary School Computer Science Teacher</small>
							</h3>
						</div>
		 
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi02a.png" alt="">
							</div>
							<p class="description">
								It is a wonderful programme for the students which provokes their thought process and helps in enhancing their skill indirectly through gamified platform.
							</p>
							<h3 class="testimonial-title">
								Ms Tajamul 
								<small class="post">Secondary School Language Teacher</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi03a.png" alt="">
							</div>
							<p class="description">
								Though the children are excellent performers academically, they lag behind when it comes to competitive exams. These sessions will enhance their Cognitive skills which could mould them better to face the outside challenges.
							</p>
							<h3 class="testimonial-title">
								Ms Sowbakia  
								<small class="post">Secondary School Teacher</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi01a.png" alt="">
							</div>
							<p class="description">
								The Analytical reports are excellent as it helps in fetching the weaker skills of the students instantly and enhancing their capabilities gradually.
							</p>
							<h3 class="testimonial-title">
								Ms Prema Kodandan  
								<small class="post">Robotics and Artificial Intelligence Foundation</small>
							</h3>
						</div>
						
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi02a.png" alt="">
							</div>
							<p class="description">
								It is a stupendous, innovative, catchy learning initiative. Even the dullest child can be easily involved wherein a conventional learning needs to literally propel them.
							</p>
							<h3 class="testimonial-title">
								Ms Saraswathi  
								<small class="post">Math Teacher</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi03a.png" alt="">
							</div>
							<p class="description">
								Excellent and much needed initiative to raise the upcoming generation with innovation and competency.
							</p>
							<h3 class="testimonial-title">
								Mr Yogeshwaran  
								<small class="post">Parent</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi01a.png" alt="">
							</div>
							<p class="description">
								Brilliant training for the kids to test and develop their core skills.
							</p>
							<h3 class="testimonial-title">
								Ms Padma Mohan   
								<small class="post">Sun Network</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi02a.png" alt="">
							</div>
							<p class="description">
								It improves memory and focus attention skills which is very useful in my business. It also induces new ideas and create awareness in my business.
							</p>
							<h3 class="testimonial-title">
								Mr Suresh Kannan   
								<small class="post">Entrepreneur, Coir Fibre Manufacturing</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi03a.png" alt="">
							</div>
							<p class="description">
								We are very much happy that the products of SkillAngels is very much useful for our Children (Students) and very much appreciated by our parents. We can see drastic improvement in the cognitive skills of our students, when they are exposed to the SkillAngels product.
							</p>
							<h3 class="testimonial-title">
								Dr Danathiyagou   
								<small class="post">Director, Alpha Educational Institutions</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi01a.png" alt="">
							</div>
							<p class="description">
								Difference between other generic computer games and ‘SkillAngels’ games is that it develops the core brain skills. It will help not only in their studies, but will give a holistic development, increased speed of execution to approach a real life situation, which is necessary for today’s generation.
							</p>
							<h3 class="testimonial-title">
								Dr M.Shyamala Devi   
								<small class="post">Developmental & Behavioural Paediatrician, Apollo Children Hospital</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi02a.png" alt="">
							</div>
							<p class="description">
								This brain gym, we believe can also be used through intranet portals for continually exercising the brains of our associates (self-directed) whenever they find the time in between their work. It is done in a totally non-threatening fashion and we believe that these skills are the basic building blocks that form the foundation for any individual.
							</p>
							<h3 class="testimonial-title">
								Mr Vijayakumar Rangaraju    
								<small class="post">VP – Human Resources, Polaris Financial Technology</small>
							</h3>
						</div>
						
						<div class="testimonial">
							<div class="pic">
								<img src="<?php echo base_url(); ?>assets/images/home/testi03a.png" alt="">
							</div>
							<p class="description">
								A fun and engaging assessment tool that helps us understand the skill level of employees and prospective employees. The distinctive approach gave the candidates a pleasant experience during the interview process and helped us create positive branding for our organisation.
							</p>
							<h3 class="testimonial-title">
								Ms Nithya Rajasundaran    
								<small class="post">Director – Human Resources, Lister Technologies</small>
							</h3>
						</div>
						
						
					</div>
				</div>
			</div>
	</div>
</div>


<div class="indexContentMiddle">
  <div class="container">
  <div class="row taglines fadeanimate">
<div class="col-md-4 col-sm-4 alignleft tagline1 bounceIn animated"><h3><?php echo $this->lang->line("childrenmsg1"); ?> <?php echo $this->lang->line("childrenmsg2"); ?></h3></div>
<div class="col-md-4 col-sm-4 alignleft tagline2 bounceIn animated"><h3><?php echo $this->lang->line("academicmsg1"); ?> <?php echo $this->lang->line("academicmsg2"); ?></h3></div>
<div class="col-md-4 col-sm-4 alignleft tagline3 bounceIn animated"><h3><?php echo $this->lang->line("parentsmsg1"); ?> <?php echo $this->lang->line("parentsmsg2")."<sup>TM</sup>"; ?></h3></div>
  </div>
<div class="row  fadeanimate">
<div class="col-md-4 col-sm-4 pad5 bounceIn animated">
	<div class="vc_column-inner ">
		<div class="wpb_wrapper">
			<div class="welcome-img ">
				<img alt="" src="<?php echo base_url(); ?>assets/images/home/Academic.jpg" width="100%">
				<div class="welcome-content" style="background-color:rgba(141,198,63, .9);">
				<div class="svg-overlay" style="color:rgba(141,198,63, .9);">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
					<path d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
					</svg>
				</div>
				<h4>For School</h4><p>
								- Identify Young Genius<br/>
								- Help slow learners<br/>
								- Flexible Pricing<br/>
								 </p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-4 col-sm-4 pad5 bounceIn animated">
	<div class="vc_column-inner ">
		<div class="wpb_wrapper">
			<div class="welcome-img ">
				<img alt="" src="<?php echo base_url(); ?>assets/images/home/Parant.jpg" width="100%">
				<div class="welcome-content" style="background-color:rgba(146,39,143, .9);">
				<div class="svg-overlay" style="color:rgba(146,39,143, .9);">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
					<path d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
					</svg>
				</div>
				<h4>For Parents</h4><p> 
								- Early Talent Identification<br/>
								- Kids learn - Skills For Life<br/>
								- Personalized Reports<br/>
								</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-4 col-sm-4 pad5 bounceIn animated">
	<div class="vc_column-inner ">
		<div class="wpb_wrapper">
			<div class="welcome-img ">
				<img alt="" src="<?php echo base_url(); ?>assets/images/home/Children.jpg" width="100%">
				<div class="welcome-content" style="background-color:rgba(241,109,124, .9);">
				<div class="svg-overlay" style="color:rgba(241,109,124, .9);">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
					<path d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
					</svg>
				</div>
				<h4>For Students</h4><p>
								- Adopt novel and creative thinking<br/>
								- Better Decision making<br/>
								- Accelerate Academic learning<br/>
								</p>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
  
  </div>
</div>

<div class="awardsrecognition" parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url(); ?>assets/images/home/Awards-Banner2.jpg">
  <div class="container">
<h2><?php echo $this->lang->line("awardstitle"); ?></h2>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<ul>
			<li>Featured in Startup India – Launch event by Honorable Prime Minister Shri. Narendra Modi</li>
			<li>Finalists – CavinKare-MMA Chinnikrishnan Innovation Awards</li>
			<li>Best Startup–Department of Electronics & IT,Ministry of Comm& IT</li>
			<li>Winners of Hot100 Technology Awards </li>
			<li>People’s Choice Best Startup CII Startupreneurs</li>
			<li>NSDC Innovations for Skills Challenge Finalist</li>
			<li>Launched “SkillAngels Super Brains” – Computer Based Brain Skill Contest for 11000+ school children and created “Assist World Record”</li>
			<li>QPrizeFinalist</li>
			<li>Winners of Tech4Impact Accelerator Program conducted by IIM Ahmedabad’s CIIE and Village Capital</li>
			
		</ul>
	</div>
<div class="col-md-8 col-sm-8"><img src="<?php echo base_url(); ?>assets/images/home/awards1.png" class="img-responsive"></div>
</div>
</div>
</div>

<div class="medianews" >
<div class="container">
<div class="row">
<div class="mediaList col-md-12 col-sm-12 col-xs-12 ">
<h2>Featured In </h2>
<ul id="flexiselDemo3">
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/01.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/02.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/03.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/04.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/05.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/06.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/07.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/11.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/09.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/10.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/08.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/12.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/13.jpg" alt="" ></li>
<li><img src="<?php echo base_url(); ?>assets/images/Featuredin/14.jpg" alt="" ></li>
</ul>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/counter.js" type="text/javascript" charset="utf-8"></script>
<script>
window.odometerOptions = {
  format: '(ddd).dd'
};
$(document).ready(function(){	  	 
doAjax();	
});	
function doAjax()
{   
setTimeout(function(){
    odo1.innerHTML = 891565;
    odo2.innerHTML = 2791766;
}, 1000);		  		
}
setInterval(function(){doAjax();}, 10000);  // 1000 = 1 second, 3000 = 3 seconds	
$(".txthEmail").blur(function(){
$.ajax({
		type		: "POST",
		cache	: false,
		url		: '<?php echo base_url('index.php/home/language') ?>',
		data		: {Lemail:$(".txthEmail").val()},
		success: function(result) {
		//	alert(result);
			 $(".ddlHLanguage").html(result);
		}
});			
});
	
if (localStorage.chkbx && localStorage.chkbx != '') {
	$('#remember').attr('checked', 'checked');
	$('#email').val(localStorage.usrname);
	$('#pwd').val(localStorage.pass);
} else {
	$('#remember').removeAttr('checked');
	$('#email').val('');
	$('#pwd').val('');
}

 $('#remember').click(function() {

	if ($('#remember').is(':checked')) {
		// save username and password
		localStorage.usrname = $('#email').val();
		localStorage.pass = $('#pwd').val();
		localStorage.chkbx = $('#remember').val();
	} else {
		localStorage.usrname = '';
		localStorage.pass = '';
		localStorage.chkbx = '';
	}
});
</script>
<!-- Full Image Fade OUT --><script src="<?php echo base_url(); ?>assets/js/imagefade/parallax.min.js"></script>
<script>
jQuery(function() {
	jQuery('#parallax_mouseinteraction_thumbs').parallax_mouseinteraction({
		skin: 'bullets',
		width: 1920,
		height: 750,
		width100Proc:true,
		defaultEasing:'easeOutElastic',
		autoPlay:10,
		responsive:true,
		autoHideBottomNav:false,
		showPreviewThumbs:true,
		autoHideNavArrows:true,
		myloaderTime:1,
		scrollSlideDuration:1.8,
		scrollSlideEasing:'easeInQuint',
		maxInteractionYmove:37,
		thumbsOnMarginTop:12,				
		thumbsWrapperMarginBottom:-35	
	});			
});
</script>


<!-- Media Slider ---->
<link href="<?php echo base_url(); ?>assets/css/media/media.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/media/jquery.flexisel.js"></script>
<script type="text/javascript">
$("#flexiselDemo3").flexisel({
        visibleItems: 3,
        itemsToScroll: 1,         
        autoPlay: {
            enable: true,
            interval: 5000,
            pauseOnHover: true
        },
		  responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1,
                itemsToScroll: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2,
                itemsToScroll: 1
            },
            tablet: { 
                changePoint:768,
                visibleItems: 2,
                itemsToScroll: 1
            },
			landscape: { 
                changePoint:1024,
                visibleItems: 3,
                itemsToScroll: 1
            }
        },       
});
</script>
<!-- Banner JS Files Start--->
<script src="<?php echo base_url();?>assets/js/banner/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/banner/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/banner/jQueryRotateCompressed.2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/banner/parallax_mouseinteraction.js" type="text/javascript"></script>
<!-- Banner JS Files END--->
<!-- Testimonial  Slider Plugin -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/testimonial/owl.carousel.min.js"></script>
<script>
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        slideSpeed:1000,
        singleItem:true,
        autoPlay:true
    });
});
</script>
<!-- Testimonial  Slider Plugin -->
