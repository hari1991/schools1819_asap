<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scape_model extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
        }

        public function getschools()
        {
			$query = $this->db->query('select * from schools where status=1 and id in (24,25) and  active=1');
			
			//echo $this->db->last_query(); exit;
			return $query->result_array();
        }
		
		 public function getskls()
        {
			$query = $this->db->query('select Distinct(school_name) as sklname from users where school_name!=""');
			
			//echo $this->db->last_query(); exit;
			return $query->result_array();
        }
		
		 public function getgrades()
        {
			$query = $this->db->query('select * from class WHERE status=1 and id NOT IN(1,2,11,12,13,14,15)');
			
			//echo $this->db->last_query(); exit;
			return $query->result_array();
        }
		
		 public function getreport2($schoolid,$gradeid,$skillid,$sklid,$sectionid)
        {
			$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; } else if($sectionid!='') { $where =  "and section='".$sectionid."'"; }
			
				$query = $this->db->query('select id, fname, school_name, grade_id,(select classname from class where id=grade_id) as gradename,section,(select school_name from schools where id=sid) as schoolname,a2.score,a2.rstime,a3.score2,a3.rstime2 ,a4.score3,a4.rstime3  from users mu left join 
(select (AVG(score)) as score, (AVG(restime)) as rstime, gu_id, gs_id, (select name from category_skills where id =gs_id ) as skillname, (SELECT sid from users where id=gu_id) as schoolid from (SELECT (AVG(`game_score`)) as score , (AVG(rtime)) as restime, gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id ="'.$skillid.'" and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) a2 on a2.gu_id=mu.id left join 

(select (AVG(score)) as score2, (AVG(restime)) as rstime2,gu_id from (SELECT (AVG(`game_score`)) as score , (AVG(rtime)) as restime, gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id ="'.$skillid.'" and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) a3 on a3.gu_id=mu.id left join 

(select (AVG(score)) as score3, (AVG(restime)) as rstime3,gu_id from (SELECT (AVG(`game_score`)) as score , (AVG(rtime)) as restime, gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id ="'.$skillid.'" and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) a4 on a4.gu_id=mu.id 

where sid="'.$schoolid.'" and grade_id="'.$gradeid.'"  '.$where.' ORDER BY a2.score DESC' );			
			
			//echo $this->db->last_query(); exit;
			return $query->result_array();
			
		}
		
		
		 public function getreport1($schoolid,$gradeid,$sklid,$sectionid)
        {
			$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; } else if($sectionid!='') { $where =  "and section='".$sectionid."'"; } else { $where =  ''; }
			
				$query = $this->db->query('select id, fname,username, grade_id,sid,school_name,section,(select classname from class where id=grade_id) as gradename,(select max(lastupdate) from game_reports where gu_id=mu.id) as completeddate,(select school_name from schools where id=sid) as schoolname, s1.skillscore_M as skillscorem, skillscore_V as skillscorev,skillscore_F as skillscoref,skillscore_P as skillscorep,skillscore_L as skillscorel, skillscore2_M as skillscore2m, skillscore2_V as skillscore2v, skillscore2_F as skillscore2f, skillscore2_P as skillscore2p, skillscore2_L as skillscore2l, skillscore3_M as skillscore3m,
skillscore3_V as skillscore3v, skillscore3_F as skillscore3f, skillscore3_P as skillscore3p, skillscore3_L as skillscore3l, a3.finalscore as avgbspiset1,a3_set2.finalscore as avgbspiset2, COALESCE(a3.playedcount,0) as play, CASE  WHEN  playedcount = 5 THEN "Completed" WHEN  playedcount < 5 THEN "Incomplete" WHEN  playedcount=0 THEN "P"  END as status, a3_set3.finalscore as avgbspiset3 from users mu

left join 
 (SELECT SUM(score)/5 as finalscore,count(gu_id) as playedcount, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s1 on s1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s2 on s2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s3 on s3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s4 on s4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s5 on s5.gu_id=mu.id 
 
 left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1_set2 group by gs_id, gu_id ) a2_set2 group by gu_id) a3_set2   on a3_set2.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore2_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t1 on t1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t2 on t2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t3 on t3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t4 on t4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t5 on t5.gu_id=mu.id 
 
 
 left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1_set3 group by gs_id, gu_id ) a2_set3 group by gu_id) a3_set3   on a3_set3.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore3_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u1 on u1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u2 on u2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u3 on u3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u4 on u4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u5 on u5.gu_id=mu.id 
 
 
 
 where sid="'.$schoolid.'" and grade_id="'.$gradeid.'"  '.$where.' ORDER BY avgbspiset1 DESC');
 //echo $this->db->last_query(); exit;
		return $query->result_array();
		 
}

 public function getplanid($schoolid,$gradeid)
        {
			$query = $this->db->query('SELECT plan_id from skl_class_plan where school_id="'.$schoolid.'" AND class_id="'.$gradeid.'"');
			return $query->result_array();
		}
		
		 public function getgradeidsets($planid,$gradeid)
        {
			$query = $this->db->query('SELECT gradeid_set1, gradeid_set2,plan_id_set2,plan_id_set3,gradeid_set3 from assesment_class_set where plan_id="'.$planid.'" AND gradeid_set1="'.$gradeid.'"');
			return $query->result_array();
		}
		
		 public function getsection($schoolid,$gradeid)
        {
			$query = $this->db->query('select id,section from skl_class_section where class_id="'.$gradeid.'" and school_id="'.$schoolid.'"');
		//	echo $this->db->last_query(); exit;
			return $query->result_array();
			
		}
		
		 public function getClassPerformace_data($schoolid,$gradeid,$sectionid,$tablename,$rangefrom,$rangeto)
        {
			$query = $this->db->query('select  (@cnt := @cnt + 1) AS rowNumber,id, fname as name,lname,avatarimage, IF(avgbspiset1 IS NULL,0,avgbspiset1) as bspi from (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM '.$tablename.' WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid="'.$schoolid.'" and grade_id="'.$gradeid.'" and section="'.$sectionid.'" and a3.finalscore BETWEEN '.$rangefrom.' AND  '.$rangeto.' ORDER BY avgbspiset1 ASC ) as a5 CROSS JOIN (SELECT @cnt := 0) AS dummy');
 
			//echo $this->db->last_query(); 
			return $query->result_array();
		}
		
		public function getClassPerformace_data2($schoolid,$gradeid,$sectionid,$tablename,$rangefrom,$rangeto)
        {
			$query = $this->db->query('select  (@cnt := @cnt + 1) AS rowNumber,id, fname as name,lname,avatarimage, IF(avgbspiset1 IS NULL,0,avgbspiset1) as bspi from (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM '.$tablename.' WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid="'.$schoolid.'" and grade_id="'.$gradeid.'" and section="'.$sectionid.'" and a3.finalscore BETWEEN '.$rangefrom.' AND  '.$rangeto.'  ORDER BY avgbspiset1 ASC ) as a5 CROSS JOIN (SELECT @cnt := 0) AS dummy');
 
			//echo $this->db->last_query(); 
			return $query->result_array();
		}
		
		public function getClassPerformace_data3($schoolid,$gradeid,$sectionid,$tablename,$rangefrom,$rangeto)
        {
			$query = $this->db->query('select  (@cnt := @cnt + 1) AS rowNumber,id, fname as name,lname,avatarimage, IF(avgbspiset1 IS NULL,0,avgbspiset1) as bspi from (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM '.$tablename.' WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid="'.$schoolid.'" and grade_id="'.$gradeid.'" and section="'.$sectionid.'" and a3.finalscore	 BETWEEN '.$rangefrom.' AND  '.$rangeto.'  ORDER BY avgbspiset1 ASC ) as a5 CROSS JOIN (SELECT @cnt := 0) AS dummy');
 
			//echo $this->db->last_query(); 
			return $query->result_array();
		}
		
		public function getusercount($data0,$schoolid)
        {
			$query = $this->db->query("select count(*) as userCount from users where username LIKE (select concat('".str_replace('.','', str_replace(' ','', strtolower($data0)))."_%')) and sid='".$schoolid."'");
			 
			return $query->result_array();
		}
		
		public function userimport($shpassword,$salt1,$salt2,$data0,$data1,$data2,$data3,$data4,$schoolid,$ddlGradeType,$ceationkey,$data5)
        {
			$creationdate = date("Y-m-d H:i:s");
			
			$query = $this->db->query("INSERT INTO users(password,salt1,salt2,creation_date, fname, lname, gender, dob, status, gp_id, grade_id, username,  sid, section, academicyear, creationkey,school_name) VALUES ('$shpassword','$salt1','$salt2','$creationdate','$data0','$data1','$data2','$data3','1',(select id from g_plans where grade_id=$ddlGradeType limit 1),'$ddlGradeType',(select concat('".str_replace('.','', str_replace(' ','', strtolower($data0)))."','.',(select school_code from schools where id='$schoolid'))),'$schoolid','$data4',(select id from academic_year limit 1), '".$ceationkey."','$data5' )");
			
			//return $query->result_array();
		}
		
		public function userimport1($shpassword,$salt1,$salt2,$data0,$data1,$data2,$data3,$data4,$schoolid,$ddlGradeType,$ceationkey,$userexist,$data5)
        {
			$creationdate = date("Y-m-d H:i:s");
			 
			$query = $this->db->query("INSERT INTO users(password,salt1,salt2,creation_date,fname, lname, gender, dob, status, gp_id, grade_id, username,  sid, section, academicyear,creationkey,school_name) VALUES ('$shpassword','$salt1','$salt2','$creationdate','$data0','$data1','$data2','$data3','1',(select id from g_plans where grade_id=$ddlGradeType limit 1),'$ddlGradeType',(select concat('".str_replace('.','', str_replace(' ','', strtolower($data0)))."','_',($userexist+1),'.',(select school_code from schools where id='$schoolid'))),'$schoolid','$data4',(select id from academic_year limit 1), '".$ceationkey."','$data5')");
		}
		
		public function userdownload($ceationkey)
        {
			$query = $this->db->query("SELECT *, (SELECT classname FROM class WHERE id=user.grade_id) as gradename FROM users user WHERE creationkey = '".$ceationkey."'");
			return $query->result_array();
		}
		
		public function getClassPerformace_data_overall($schoolid,$gradeid,$sectionid,$planidset2,$planidset3,$rangefrom,$rangeto)
		 {
			 $subquery='((coalesce(set1.avgbspiset1,0)+coalesce(set2.avgbspiset1,0)+coalesce(set3.avgbspiset1,0))/3) ';
			 if($planidset2==0 && $planidset3==0){$subquery='(coalesce(set1.avgbspiset1,0)/1) ';}
			 else if($planidset2!=0 && $planidset3!=0){$subquery='((coalesce(set1.avgbspiset1,0)+coalesce(set2.avgbspiset1,0)+coalesce(set3.avgbspiset1,0))/3) ';}
			 else if($planidset2==0 && $planidset3!=0){$subquery='((coalesce(set1.avgbspiset1,0)+coalesce(set3.avgbspiset1,0))/2) ';}
			 else if($planidset2!=0 && $planidset3==0){$subquery='((coalesce(set1.avgbspiset1,0)+coalesce(set2.avgbspiset1,0))/2) ';}
			 
			 /*echo "select (@cnt := @cnt + 1) AS rowNumber,id, fname as name,lname,avatarimage, IF(avgscore IS NULL,0,avgscore) as bspi from (select set1.id,set1.fname,set1.lname,set1.avatarimage,".$subquery." as avgscore from (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM game_reports WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$sectionid."'   ORDER BY avgbspiset1 DESC ) as set1 join (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM game_reports_set2 WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$sectionid."'   ORDER BY avgbspiset1 DESC ) as set2 on set1.id=set2.id join (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM game_reports_set3 WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$sectionid."'   ORDER BY avgbspiset1 DESC ) as set3  on set1.id=set3.id  WHERE ".$subquery."  BETWEEN ".$rangefrom." AND ".$rangeto."  order by avgscore desc )as a5 CROSS JOIN (SELECT @cnt := 0) AS dummy"; */
			
		$query = $this->db->query("select (@cnt := @cnt + 1) AS rowNumber,id, fname as name,lname,avatarimage, IF(avgscore IS NULL,0,avgscore) as bspi from (select set1.id,set1.fname,set1.lname,set1.avatarimage,".$subquery." as avgscore from (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM game_reports WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$sectionid."'   ORDER BY avgbspiset1 DESC ) as set1 join (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM game_reports_set2 WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$sectionid."'   ORDER BY avgbspiset1 DESC ) as set2 on set1.id=set2.id join (select id as id, fname,lname,avatarimage, grade_id,(select classname from class where id=grade_id) as gradename,a3.finalscore as avgbspiset1 from users mu  left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM game_reports_set3 WHERE gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id where sid=".$schoolid." and grade_id='".$gradeid."' and section='".$sectionid."'   ORDER BY avgbspiset1 DESC ) as set3  on set1.id=set3.id  WHERE ".$subquery."  BETWEEN ".$rangefrom." AND ".$rangeto."  order by avgscore ASC )as a5 CROSS JOIN (SELECT @cnt := 0) AS dummy");
		//echo $this->db->last_query(); 
		
			return $query->result_array();
		 }
		 
	/* Dashboard Start*/
	public function GetGradebySection($school_id)
	{
		$query = $this->db->query("SELECT class_id,section,(select classname from class where id=class_id) as gradename,CONCAT(class_id,section) as rowval FROM skl_class_section WHERE school_id=".$school_id." ");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
		
	}
	public function GetRegisteredStudentCount()
	{
		$query = $this->db->query('SELECT COUNT(*) AS RegisteredCount FROM users');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}	
	public function GetAssessmentTakenStudentCount()
	{
		$query = $this->db->query('select count(distinct(gu_id)) as AssessmentTaken from(select gu_id from (SELECT gu_id from game_reports union all
SELECT gu_id from game_reports_set3 union all
SELECT gu_id from game_reports_set2 ) as a1 where gu_id!=0)as a2');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function GetFullyAssessmentTakenStudentCount()
	{
		$query = $this->db->query('select count(Distinct(gu_id)) as AssessmentFullyTaken from(select gu_id from(SELECT gu_id,set1 from(SELECT gu_id,count(id) as set1 FROM game_reports where gu_id!=0  group by gu_id)a1 where set1>=5)b1
union 
select gu_id from(SELECT gu_id,set2 from(SELECT gu_id,count(id) as set2 FROM game_reports_set2 where gu_id!=0 group by gu_id)a2 where set2>=5)b2
union
select gu_id from(SELECT gu_id,set3 from(SELECT gu_id,count(id) as set3 FROM game_reports_set3 where gu_id!=0 group by gu_id)a3 where set3>=5)b3) c1');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function GetRegisteredStudentCountbyschool($sid,$school_name)
	{		$where =  '';
			if($school_name!='') { $where =  " AND school_name='".$school_name."'"; } else { $where =  ''; }
			
		$query = $this->db->query('select count(Distinct(id)) as RegisteredCount,(select classname from class where id=grade_id) as gradename,section, sid,CONCAT(grade_id,section) as rowval  FROM users as u where sid='.$sid.' '.$where.' group by gradename,section');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}	
	public function GetAssessmentTakenStudentCountbyschool($sid,$school_name)
	{
			$where =  '';
			if($school_name!='') { $where =  " AND school_name='".$school_name."'"; } else { $where =  ''; }
		/*$query = $this->db->query('select id as AssessmentTaken,gradename,section,sid,school_name,CONCAT(grade_id,section) as rowval from(select count(distinct(gu_id)) as id,(select classname from class where id=(select grade_id from users where id=gu_id)) as gradename,(select section from users where id=gu_id) as section,(select sid from users where id=gu_id) as sid,(select grade_id from users where id=gu_id) as grade_id,(select school_name from users where id=gu_id) as school_name  from (SELECT gu_id from game_reports union all
SELECT gu_id from game_reports_set2 union all
SELECT gu_id from game_reports_set3 ) as a1  group by gradename,section)as a2 where sid='.$sid.' '.$where.' ');*/
$query = $this->db->query('select count(distinct(gu_id)) as AssessmentTaken,grade_id,sid,section,school_name,CONCAT(grade_id,section) as rowval,(select classname from class where id=u.grade_id) as gradename from game_reports gr join users u on gr.gu_id=u.id where u.sid='.$sid.' '.$where.'  group by u.grade_id,u.section');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
}
public function GetAssessmentTakenStudentBspibyschool($sid,$school_name)
{
$where =  '';
if($school_name!='') { $where =  " AND school_name='".$school_name."'"; } else { $where =  ''; }
$query = $this->db->query('select gu_id,set1,ROUND(avg(bspi),2) as avgbspi,grade_id,sid,section,school_name,CONCAT(grade_id,section) as rowval,(select classname from class where id=u.grade_id) as gradename  from(SELECT gu_id,count(id) as set1,sum(game_score)/5 as bspi FROM game_reports where gu_id!=0  group by gu_id)a1 join users u on u.id=a1.gu_id where set1>=5 and  u.sid='.$sid.' '.$where.' group by u.grade_id,u.section');
//echo $this->db->last_query(); exit;
return $query->result_array();
}

	public function GetFullyAssessmentTakenStudentCountbyschool($sid,$school_name)
	{	
		$where =  '';
		if($school_name!='') { $where =  " AND school_name='".$school_name."'"; } else { $where =  ''; }
		/*$query = $this->db->query('select AssessmentFullyTaken,gradename,section,sid,school_name,CONCAT(grade_id,section) as rowval from(select count(Distinct(gu_id)) as AssessmentFullyTaken,(select classname from class where id=(select grade_id from users where id=gu_id)) as gradename,(select section from users where id=gu_id) as section,(select sid from users where id=gu_id) as sid,(select grade_id from users where id=gu_id) as grade_id,(select school_name from users where id=gu_id) as school_name  from(select gu_id from(SELECT gu_id,set1 from(SELECT gu_id,count(id) as set1 FROM game_reports where gu_id!=0  group by gu_id)a1 where set1>=5)b1) c1  group by gradename,section) a5 where sid='.$sid.' '.$where.' ');*/
		$query = $this->db->query('select gu_id,set1,count(distinct(gu_id)) as AssessmentFullyTaken,grade_id,sid,section,school_name,CONCAT(grade_id,section) as rowval,(select classname from class where id=u.grade_id) as gradename  from(SELECT gu_id,count(id) as set1 FROM game_reports where gu_id!=0  group by gu_id)a1 join users u on u.id=a1.gu_id where set1>=5 and  u.sid='.$sid.' '.$where.' group by u.grade_id,u.section'); 
		
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	public function GetBestPuzzleSet($schoolid)
	{
$query = $this->db->query('select round(avg(avgbspiset1),2) as BSPISET1,round(avg(avgbspiset2),2) as BSPISET2,round(avg(avgbspiset3),2) as BSPISET3,grade_id,gradename from(select id, fname, grade_id,(select classname from class where id=grade_id) as gradename, s1.skillscore_M as skillscorem, skillscore_V as skillscorev,skillscore_F as skillscoref,skillscore_P as skillscorep,skillscore_L as skillscorel, skillscore2_M as skillscore2m, skillscore2_V as skillscore2v, skillscore2_F as skillscore2f, skillscore2_P as skillscore2p, skillscore2_L as skillscore2l, skillscore3_M as skillscore3m,
skillscore3_V as skillscore3v, skillscore3_F as skillscore3f, skillscore3_P as skillscore3p, skillscore3_L as skillscore3l, a3.finalscore as avgbspiset1,a3_set2.finalscore as avgbspiset2,a3_set3.finalscore as avgbspiset3 from users mu

left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s1 on s1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s2 on s2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s3 on s3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s4 on s4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s5 on s5.gu_id=mu.id 
 
 left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1_set2 group by gs_id, gu_id ) a2_set2 group by gu_id) a3_set2   on a3_set2.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore2_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t1 on t1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t2 on t2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t3 on t3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t4 on t4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore2_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set2` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) t5 on t5.gu_id=mu.id 
 
 
 left join 
 (SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1_set3 group by gs_id, gu_id ) a2_set3 group by gu_id) a3_set3   on a3_set3.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore3_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u1 on u1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u2 on u2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u3 on u3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u4 on u4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore3_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports_set3` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) u5 on u5.gu_id=mu.id 
 
 
 
 where sid="'.$schoolid.'") x1 group by grade_id');
 
		return $query->result_array();
		 
}
	public function GetTOPBSPIScores($school_id,$gradeid,$topid)
	{
		$query = $this->db->query('select finalscore,gu_id,schoolid from(SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT AVG(game_score) as score , gs_id , gu_id, lastupdate FROM game_reports group by gs_id , gu_id, lastupdate) as a1 group by gs_id, gu_id) a2 group by gu_id) as a3 where schoolid='.$school_id.' group by gu_id order by finalscore DESC limit 0,'.$topid.' ');
		return $query->result_array();
	}
	public function GetTOPBSPIUser($school_id,$gradeid,$topid,$sklid,$sectionid)
	{
		$where =  '';
			if($sklid!='') { $where =  "and sklname='".$sklid."'"; } else if($sectionid!='') { $where_section =  "and section='".$sectionid."'"; } else { $where = ""; }
		
		$query = $this->db->query('select finalscore,group_concat(gu_id) as gu_ids,GROUP_CONCAT(username SEPARATOR ", ") as name,schoolid,grade_id from(SELECT SUM(score)/5 as finalscore, gu_id, (SELECT sid from users where id=gu_id) as schoolid, (SELECT grade_id from users where id=gu_id '.$where_section.') as grade_id,(SELECT school_name from users where id=gu_id) as sklname,(select GROUP_CONCAT(CONCAT(fname," ",lname,"- Section ",section)) from `users` where id = gu_id) as username from (select (AVG(score)) as score, gu_id, gs_id from (SELECT AVG(game_score) as score , gs_id , gu_id, lastupdate FROM game_reports group by gs_id , gu_id, lastupdate) as a1 group by gs_id, gu_id) a2 group by gu_id) as a3 where schoolid='.$school_id.' and grade_id='.$gradeid.' '.$where.' group by finalscore order by finalscore DESC limit 0,'.$topid.' ');
		//echo $this->db->last_query(); exit;
		return $query->result_array();
	}
	
	public function GetSkill1TopScore($school_id,$gradeid,$topid,$sklid,$sectionid)
	{
		$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; } else if($sectionid!='') { $where =  "and ua.section='".$sectionid."'"; } else { $where =  ''; }
			
			$query = $this->db->query('select GROUP_CONCAT(id),GROUP_CONCAT(CONCAT(fname," ",lname,"- Section ",section) SEPARATOR ", ") as username,s1.gamescore from users ua left join (
SELECT (AVG(`game_score`)) as gamescore ,gs_id ,gu_id, lastupdate,DATE_FORMAT(`lastupdate`,"%m") as playedMonth  FROM `game_reports` WHERE gs_id in (59)  and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gu_id,gs_id)s1 on s1.gu_id=ua.id where sid='.$school_id.' and grade_id='.$gradeid.' '.$where.' group by s1.gamescore  order by s1.gamescore DESC LIMIT 0,'.$topid.'');
//echo $this->db->last_query(); exit;
return $query->result_array();
	}
	public function GetSkill2TopScore($school_id,$gradeid,$topid,$sklid,$sectionid)
	{
		$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; }  else if($sectionid!='') { $where =  "and ua.section='".$sectionid."'"; } else { $where =  ''; }
			
			$query = $this->db->query('select GROUP_CONCAT(id),GROUP_CONCAT(CONCAT(fname," ",lname,"- Section ",section) SEPARATOR ", ") as username,s2.gamescore from users ua
left join (
SELECT (AVG(`game_score`)) as gamescore ,gs_id ,gu_id, lastupdate,DATE_FORMAT(`lastupdate`,"%m") as playedMonth  FROM `game_reports` WHERE gs_id in (60)  and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gu_id,gs_id)s2 on s2.gu_id=ua.id where sid='.$school_id.' and grade_id='.$gradeid.' '.$where.' group by s2.gamescore  order by s2.gamescore DESC LIMIT 0,'.$topid.'');
return $query->result_array();
	}
	
	public function GetSkill3TopScore($school_id,$gradeid,$topid,$sklid,$sectionid)
	{
		$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; }  else if($sectionid!='') { $where =  "and ua.section='".$sectionid."'"; } else { $where =  ''; }
		
			$query = $this->db->query('select GROUP_CONCAT(id),GROUP_CONCAT(CONCAT(fname," ",lname,"- Section ",section) SEPARATOR ", ") as username,s3.gamescore from users ua
left join (
SELECT (AVG(`game_score`)) as gamescore ,gs_id ,gu_id, lastupdate,DATE_FORMAT(`lastupdate`,"%m") as playedMonth  FROM `game_reports` WHERE gs_id in (61)  and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gu_id,gs_id)s3 on s3.gu_id=ua.id  where sid='.$school_id.' and grade_id='.$gradeid.' '.$where.' group by s3.gamescore  order by s3.gamescore DESC LIMIT 0,'.$topid.'');
return $query->result_array();
	}
	
	public function GetSkill4TopScore($school_id,$gradeid,$topid,$sklid,$sectionid)
	{
		$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; }  else if($sectionid!='') { $where =  "and ua.section='".$sectionid."'"; } else { $where =  ''; }
		
			$query = $this->db->query('select GROUP_CONCAT(id),GROUP_CONCAT(CONCAT(fname," ",lname,"- Section ",section) SEPARATOR ", ") as username,s4.gamescore from users ua
left join (
SELECT (AVG(`game_score`)) as gamescore ,gs_id ,gu_id, lastupdate,DATE_FORMAT(`lastupdate`,"%m") as playedMonth  FROM `game_reports` WHERE gs_id in (62)  and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gu_id,gs_id)s4 on s4.gu_id=ua.id where sid='.$school_id.' and grade_id='.$gradeid.' '.$where.' group by s4.gamescore  order by s4.gamescore DESC LIMIT 0,'.$topid.'');
return $query->result_array();
	}
	public function GetSkill5TopScore($school_id,$gradeid,$topid,$sklid,$sectionid)
	{
		$where =  '';
			if($sklid!='') { $where =  "and school_name='".$sklid."'"; }  else if($sectionid!='') { $where =  "and ua.section='".$sectionid."'"; } else { $where =  ''; }
		
			$query = $this->db->query('select GROUP_CONCAT(id),GROUP_CONCAT(CONCAT(fname," ",lname,"- Section ",section) SEPARATOR ", ") as username,s5.gamescore from users ua
left join (
SELECT (AVG(`game_score`)) as gamescore ,gs_id ,gu_id, lastupdate,DATE_FORMAT(`lastupdate`,"%m") as playedMonth  FROM `game_reports` WHERE gs_id in (63)  and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gu_id,gs_id)s5  on s5.gu_id=ua.id where sid='.$school_id.' and grade_id='.$gradeid.' '.$where.' group by s5.gamescore  order by s5.gamescore DESC LIMIT 0,'.$topid.'');
return $query->result_array();
	}
	

	

}
