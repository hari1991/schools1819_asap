<div class="clear_both"></div>
 <div class="contentInner">
 <div class="container">
 <h2>My Trophies:</h2>
 <div class="form_sec">
<div class="row">
 <?php foreach($academicmonths as $months){ ?>
	   
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="mytrophies"><h3><?php echo date("F", mktime(null, null, null, $months['monthNumber']))." ".$months['yearNumber']; ?> </h3>

<div>
<?php if($months['yearNumber'].str_pad($months['monthNumber'], 2, '0', STR_PAD_LEFT)<=date("Y").str_pad(date("n"), 2, '0', STR_PAD_LEFT)){ ?>
<?php
$diamondtrophy=$goldtrophy=$silvertrophy=$totaltrphy=0;
if(isset($arrmyTrophies[str_pad($months['monthNumber'], 2, '0', STR_PAD_LEFT)])){
foreach($arrmyTrophies[str_pad($months['monthNumber'], 2, '0', STR_PAD_LEFT)] as $mytrophies)
{
				$totstarval = $mytrophies;
				$diamondtrophy += floor($totstarval/60);
				$goldtrophy += floor(($totstarval%60)/30);
				$silvertrophy += floor((($totstarval%60)%30)/15);
				$totaltrphy =$diamondtrophy+$goldtrophy+$silvertrophy;
}
}
?>
<div class="trophieslist trophy-DiamondBig"><span><?php echo $diamondtrophy; ?></span></div>
<div class="trophieslist trophy-GoldBig"><span><?php echo $goldtrophy; ?></span></div>
<div class="trophieslist trophy-SilverBig"><span><?php echo $silvertrophy; ?></span></div>
<?php } else { ?>
<img src="<?php echo base_url() ?>assets/images/myTrophies-Star.png" width="126" height="126"> 

<?php } ?>
</div>
</div>

</div>
  
	  <?php } ?>
 
</div>

 
 


  </div><!--/form_sec -->
 </div><!-- container -->
 </div>