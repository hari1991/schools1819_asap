<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appapi_model extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
			//	 $this->load->library('Multipledb');
        }
		
		public function checkUser($username,$password,$deviceid)
        {
			$query = $this->db->query('select *,(SELECT language_key FROM language_master WHERE ID=1) as languagekey FROM users a WHERE username="'.$username.'" AND password=SHA1(CONCAT(salt1,"'.$password.'",salt2))  AND status=1 AND (SELECT school_id FROM school_admin WHERE school_id=a.sid AND active=1)');
			
			//echo $this->db->last_query(); exit;
			return $query->result();
        }
		
		 public function update_loginDetails($userid,$session_id)
		 { 
		$query = $this->db->query("update users set pre_logindate = login_date,login_date = CURDATE(),login_count=login_count+1,session_id=".$session_id.",islogin=1,last_active_datetime=NOW() WHERE id =".$userid);
		//echo $this->db->last_query(); exit;
		 }	
		 
		 public function insert_login_log($userid,$sessionid,$ip,$country,$region,$city,$isp,$browser,$status)
		{
			$query = $this->db->query('INSERT INTO user_login_log(userid,sessionid,created_date,lastupdate,logout_date,ip,country,region,city,browser,isp,status)VALUES("'.$userid.'","'.$sessionid.'",now(),now(),now(), "'.$ip.'","'.$country.'","'.$region.'","'.$city.'","'.$browser.'","'.$isp.'","'.$status.'")');
			return $query;
			
		}
		
		public function updateDeviceID($username,$deviceid)
        {	
 			$query = $this->db->query('UPDATE users SET deviceid="'.$deviceid.'" where  username="'.$username.'"'); 
        }
		
		public function checkuserexist($username)
        {
 			$query = $this->db->query('select a.*,(select replace(classname,"Grade","") from class where id=grade_id) as gradename FROM users a  WHERE a.username="'.$username.'"  AND a.status=1');
  			return $query->result();
        }
		
		/* public function getRandomGames($game_plan_id,$game_grade,$school_id)
		 {
			 
			$query = $this->db->query("SELECT gid FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' GROUP BY school_id, gs_id");
			//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		 } */
		 
		 public function getRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id,$uid)
		 {
			 
			 $query = $this->db->query("SELECT gid FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and user_id='".$uid."' GROUP BY school_id, gs_id ORDER BY gs_id ASC");
			 
		 
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 
		 public function getActualGames($game_plan_id,$game_grade,$uid,$catid,$where,$schoolid)
		 {
			 
			
				 
				 $query = $this->db->query("SELECT (SELECT ntimes from game_limit where g_id=g.gid and gp_id=b.id and gs_id=e.skill_id limit 1) as number_of_playtime,  (select count(*) as tot_game_played from game_reports where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id) as tot_game_played ,(select coalesce(SUM(game_score), '-')  from game_reports where gu_id =  '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id) as tot_game_score , e.skill_id, j.name AS skill_name, g.gid, CASE WHEN g.game_html='Equate-Level2' THEN 'ArithmeticChallenge' ELSE g.game_html END  as gname FROM users AS a
				JOIN g_plans AS b ON   b.id = '".$game_plan_id."'
				JOIN class AS c ON   c.id = '".$game_grade."'
				JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id
				JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id
				JOIN category_skills AS j ON e.skill_id = j.id 
				JOIN skl_class_plan AS f ON a.sid = f.school_id
				JOIN games AS g ON d.game_id = g.gid
				JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$schoolid."'
				WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' $where
				GROUP BY g.gid");
				 
			 
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 
		 public function deleteSPLRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id)
		 {
			$query = $this->db->query("delete FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."'");
		 }
		 
		 public function getSkillsRandom($catid)
		 {
			 
		$query = $this->db->query("SELECT a.id AS category_id, b.id AS skill_id FROM g_category AS a JOIN category_skills AS b ON a.id = b.category_id WHERE a.id = '".$catid."'");
			return $query->result_array();
		 }
		 
		 public function assignRandomGame($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$uid)
		 {
			 
		$query = $this->db->query("SELECT j.id, e.skill_id, j.name AS skill_name, g.gid, g.gname FROM users AS a
				JOIN g_plans AS b ON   b.id = '".$game_plan_id."' JOIN class AS c ON   c.id = '".$game_grade."' JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id JOIN category_skills AS j ON e.skill_id = j.id JOIN skl_class_plan AS f ON a.sid = f.school_id JOIN games AS g ON d.game_id = g.gid JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$school_id."' WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' and j.id = '".$skill_id."' and g.gid not in (SELECT gid FROM rand_selection WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and user_id='".$uid."' and gs_id = '".$skill_id."') GROUP BY g.gid ORDER BY RAND() LIMIT 1");
			return $query->result_array();
		 }
		 
		 public function deleteRandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$del_where,$uid)
		 {
			 
		$query = $this->db->query("delete from rand_selection where gc_id = '".$catid."' and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' and user_id='".$uid."' ".$del_where);
			 
		 }
		 
		 public function insertRandomGames($catid,$game_plan_id,$game_grade,$skill_id,$school_id,$section,$gameid,$check_date_time,$userid)
		 {
			 
		$query = $this->db->query("INSERT INTO rand_selection SET gc_id = '".$catid."', gs_id = '".$skill_id."', gid = '".$gameid."', gp_id = '".$game_plan_id."', grade_id = '".$game_grade."', section = '".$section."', school_id = '".$school_id."',user_id='".$userid."', created_date = '".$check_date_time."'");
		
			 
		 }
		 
		/*  public function getActualGames($game_plan_id,$game_grade,$uid,$catid,$where,$schoolid)
		 {
			 $query = $this->db->query("SELECT (SELECT ntimes from game_limit where g_id=g.gid and gp_id=b.id and gs_id=e.skill_id limit 1) as number_of_playtime,  (select count(*) as tot_game_played from game_reports where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id) as tot_game_played ,(select coalesce(SUM(game_score), '-') from game_reports where gu_id =  '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id) as tot_game_score , e.skill_id, j.name AS skill_name, g.gid, CASE WHEN g.game_html='Equate-Level2' THEN 'ArithmeticChallenge' ELSE g.game_html END  as gname
		FROM users AS a
		JOIN g_plans AS b ON   b.id = '".$game_plan_id."'
		JOIN class AS c ON   c.id = '".$game_grade."'
		JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id
		JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id
		JOIN category_skills AS j ON e.skill_id = j.id 
		JOIN skl_class_plan AS f ON a.sid = f.school_id
		JOIN games AS g ON d.game_id = g.gid
		JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$schoolid."'
		WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' $where
		GROUP BY g.gid");
		//echo $this->db->last_query(); exit;
		return $query->result_array();
		 } */
		 
		public function getAssignGames($game_plan_id,$game_grade,$uid,$catid)
		 {
			 
		$query = $this->db->query("SELECT a.id, a.grade_id, d.skill_id FROM users AS a JOIN g_plans AS b ON   b.id = '".$game_plan_id."' JOIN class_plan_game AS c ON b.id = c.plan_id AND b.grade_id = c.class_id JOIN class_skill_game AS d ON c.class_id = d.class_id AND c.game_id = d.game_id AND d.class_id = '".$game_grade."' JOIN category_skills AS e ON e.id = d.skill_id WHERE a.id = '".$uid."' AND e.category_id = '".$catid."' GROUP BY d.skill_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 
		 public function getgameid($gamename)
		{
			$query = $this->db->query("select gid as gid from games where game_html='".$gamename."' limit 1 ");
			//echo $this->multipledb->db->last_query(); exit;
			return $query->result_array();
		}
		
			public function checkgame($gameid,$schoolid,$gradeid,$section,$userid)
	{
		$curdate=date('Y-m-d');
		$query = $this->db->query("select count(distinct gid) as gameexist,(select game_html from games where gid='".$gameid."' limit 1) as gpath,(select ntimes from game_limit where g_id='".$gameid."' limit 1) as game_limit,(select count(id) from game_reports where g_id='".$gameid."' and gu_id='".$userid."' and lastupdate='".$curdate."') as played_time from rand_selection where gid = '".$gameid."' and created_date='".$curdate."' and grade_id='".$gradeid."' and school_id='".$schoolid."'  ");
		//echo $this->multipledb->db->last_query(); exit;
		return $query->result_array();
	}
	
	 public function getresultGameDetails($userid,$gameid)
	{
		 //echo 'hello'; exit;
	$query = $this->db->query("select (select gs_id from games where gid='".$gameid."') as gameskillid,(select count(gu_id) from game_reports where gu_id=".$userid." and lastupdate=CURDATE() and gs_id IN(59,60,61,62,63)) as playedgamescount");
	 
		return $query->result_array();
	}
	
	public function getcurdayskillid($userid,$skillid)
		 {
		$query = $this->db->query("SELECT COUNT(gs_id) as skillcount from game_reports where gu_id='".$userid."' and gs_id='".$skillid."'");
		return $query->result_array();
		 }
		 
	public function insertone($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into game_reports (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."')");
		//echo $this->multipledb->db->last_query(); exit;
			 
		 }
		 
		  public function getSkillscores($uid)
		 {
			 $query = $this->db->query("SELECT id, s1.mem as memory,s2.vp as visual, s3.fa as focus, s4.ps as problem,s5.lin as ling from users mu left join 

(select AVG(game_score) as mem, gu_id FROM game_reports  where gu_id='".$uid."' and gs_id=59)s1 ON s1.gu_id=mu.id

left join (select AVG(game_score) as vp, gu_id FROM game_reports  where gu_id='".$uid."'  and gs_id=60)s2 ON s2.gu_id=mu.id

left join (select AVG(game_score) as fa, gu_id FROM game_reports  where gu_id='".$uid."'  and gs_id=61)s3 ON s3.gu_id=mu.id

left join (select AVG(game_score) as ps, gu_id FROM game_reports  where gu_id='".$uid."' and gs_id=62)s4 ON s4.gu_id=mu.id

left join (select AVG(game_score) as lin, gu_id FROM game_reports  where gu_id='".$uid."' and gs_id=63)s5 ON s5.gu_id=mu.id where mu.id='".$uid."' ");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
								
		 }
		 
		 	public function update_logout_log($userid,$sessionid)
	{
		$query = $this->db->query('update user_login_log set lastupdate=now(),logout_date=now() where userid="'.$userid.'" and sessionid="'.$sessionid.'"');
		return $query;
	}
	
	public function updateuserloginstatus($userid,$login_session_id)
	{
		$query = $this->db->query('Update users set islogin=0 WHERE id="'.$userid.'" AND status=1;');
	}
		
		
	
}