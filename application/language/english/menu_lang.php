<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['welcome_message'] = 'Welcome to CodexWorld';

$lang['home'] = 'Home';
$lang['brainwave'] = 'The Brain Wave';
$lang['Whyskillangels'] = 'Why Skill Angels';
$lang['ourstory'] = 'OUR STORY';
$lang['corevideo'] = 'CORE VIDEO';
$lang['howitworks'] = 'HOW IT WORKS';
$lang['partOfrevolution'] = 'WHY BE A PART OF REVOLUTION';
$lang['mustknowfacts'] = '50 Must Know Facts';
$lang['innews'] = 'In news';
$lang['inaction'] = 'In Action';
$lang['superbrain'] = 'SUPER BRAIN';
$lang['hearaboutus'] = 'HEAR ABOUT US';
$lang['research'] = 'Research';
$lang['industrylinks'] = 'INDUSTRY LINKS';
$lang['experttalk'] = 'Expert Talk';
$lang['login'] = 'Login';

$lang['msgdashboard'] = 'Welcome to home';

?>
