(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],-5.5,-25,5.6,25.1).s().p("AjNBdQgggagRhBQgQg5CBgdIgSgCQDcgxC3BRIgDAAIAPAGIABACQhCAmg0AZIgmARQhBAehoAcQgaAGgWAEQgWAEgSAAQgiAAgPgNg");
	this.shape.setTransform(30.7,25.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],15.5,-12.5,-15.5,12.7).s().p("AAfCZIgIgGQgygngUgVQgLgLgUgcQgWgeghgxQhChfAAg2QgBgyA9AMQAyAJBEAzIAhAcQBZBLAcAyQAdAvAWBJQAVBKgBABQgBALgCAGIABADIgCAJQgIADgKAAQgwAAhjhFg");
	this.shape_1.setTransform(40.6,-13.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],5.8,-20.3,-0.9,29.2).s().p("ACVC/QhIgLhNgPQjegpkehFQhnhWAJhXQAJhYCSgQQARgBAXABQChAKHCCoIAKADIDZBOIADABIAIACIgDgCQB2BTAvgRIgDAMIgDAMQgKAtgNAwIgNACIgJABQi3hSjdAxg");
	this.shape_2.setTransform(-0.2,-2.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.6,-36,121.2,72.1);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FA2B2B","#C10000"],[0,1],5,-1.5,-4.9,1.5).s().p("Ag5AKQAJhcAahaIACgHQAHADAHACQAGACAHAGIABADQABABAEACIAMAnQALAmAGAfQAXBxgHAeQgFAWgRATQgOAQgiAdIgTADQgjhSAJhYg");
	this.shape.setTransform(4.7,25.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().rs(["#FF0000","#440000"],[0,1],-8.2,-10.2,0,-8.2,-10.2,23).ss(2,1,1).p("AgqjqQg+DEB2CnQAiAwAQA6");
	this.shape_1.setTransform(-5.2,-27.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#FF0000","#860000"],[0,1],4.4,7.1,0,4.4,7.1,12.8).s().p("AgaDoQgnhFgMhKIgLAHIgCABIgXAIIgHghQgFhrAWhOQAGgSAIgTQATgtAkgwIAFACIACADIgCgEIABAAQACgDAvgCIgCgBIACAAIABAAQAIAEAKACQAIADAIAIIACADQACACAEACQAGAPAIAgQAPAvAHAoQAdCKgJAmQgGAagVAYQgSAUgqAjIgLACQAAAKgQAWQgQAXgGABIgKgRg");
	this.shape_2.setTransform(0,25.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF9900","#FF3300"],[0,1],-0.2,-2.3,0.5,2).s().p("AAgASQgKgCgIgEIAAAAIgDAAIADABQgvACgDACIgBABIgKgOQgBgIAWgLQARgJAJgBQAbgFAIAJQAFAHAHAlIABAFQgIgHgIgDg");
	this.shape_3.setTransform(0.8,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.6,-51.6,24.9,102.2);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FF9900","#FF3300","#800000"],[0,0.58,1],18.3,-35.3,0,18.3,-35.3,102.5).s().p("AoMFbIBYhvIANgTIACgCIA5hJIBGheQAxg9Axg0QADgFACgBQCkivC5hGQDdhTCSBaQg5A2hFAjQg/AehFAJQhRALhCAtQgmAbgdAmQgsAygOBAIgEghQgCgbAEgcQAMhZA9hFIgFAEQg9Avg6A1QghAigeAkQgmAwghA1QgnA8gtA2QguAyg2ApQg5AwhJAVQgKgEgIgGg");
	this.shape.setTransform(-1.5,-10.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#010031","#0155D8"],[0.278,1],-17,18,21,-14.7).s().p("AjNCKQFHhoBUi4QhpD+kKAuIgCABQgWgKgQgDg");
	this.shape_1.setTransform(7.2,27.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#010031","#0155D8"],[0.278,1],-19.3,20.2,22.8,-16.1).s().p("AjpCVQF3hpBbjHQh7EplXAOg");
	this.shape_2.setTransform(-16.5,12.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#010031","#0155D8"],[0.278,1],-11.9,13.3,14.9,-9.8).s().p("ACQhnQgwB5hiA4QhQAVg+AJQDphLA3iEg");
	this.shape_3.setTransform(24.7,35.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],44,-1.6,0,44,-1.6,69.5).s().p("Ag1G+IgmgTIgEgBIACgBQEKguBpj/QhUC5lHBoIgQgBIgBgBQhOgGhegYQhggZAKgUQALgWAAgWIgBgMQFYgNB6kpQhaDHl4BpIgCABIgKgBQhCgDgsgSQgKgEgJgFIBYhwIAOgTIABgBIA5hKIBGhcQAxg/Axg0QADgFADgBQCjiuC5hGQDdhUCSBbIAKAGIAHAFIANAJQgOAmgFAqQATgRgYAYQgFDgAKCqQAHBpgwBEQg8CAiOAYIgJAEIgXAGIgCAAQBjg4Awh6Qg4CFjpBLQgsAGgiAAQgyAAgbgOg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54,-46,108,92.1);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FF9900","#FF3300","#800000"],[0,0.58,1],21.6,18.3,0,21.6,18.3,51.5).s().p("AB1BaQgqgMglgWQhcg2hpAOIgCgCIgLgBQh0AGhaBHQAQgTAVgSQBLg+BDgTIAHgCQApgLAmAGQAjAEAjAFQAhAGAgAHIA0ANQBYAZBXgQQBagQAohcQADBBgnAuQgTAXgeAIQg/AOhBgBQAJACAJAFQAYAMAcADQAdAAAhgDQgzAZg3AAQgkAAgngKg");
	this.shape.setTransform(2.2,-22.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],0,0.1,0,0,0.1,0.4).s().p("AgCAAIAEAAIABABIgFgBg");
	this.shape_1.setTransform(5.8,21.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#0173DE","#010031"],[0,1],-13.2,0.6,13.6,-1.1).s().p("AiIhBQBQCNDBg9QgGAVgUAWQgkAIggAAQh+AAg1iDg");
	this.shape_2.setTransform(6.1,15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#0173DE","#010031"],[0,1],-19.2,0.7,20.1,-1.8).s().p("AjGhSQBvDEEehyIAAAAIgDACQABAjgFADQhiArhMAAQiVAAhDilg");
	this.shape_3.setTransform(14.7,-1.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],23.8,-4.3,0,23.8,-4.3,51.5).s().p("Ai7EPQgwhTgTgVQgKgIhXgxQhKgogEgcQgIhDAthCIABgCQAPgVAUgVQAQgTAVgSQBLg/BDgTIAHgCQApgLAmAGQAjADAjAGQAgAFAhAIIA0ANQBYAaBXgRQBagQAohdIADgIQAPAgAGAXQADAOACATIAAAIQAGA9gdBDQgGAOgHANQgUAlgZAcIgCACIgBAAQkeByhvjFQBlD8EhiCIgRAgQgWAfgpAeQgYAUgfAQQAAABgBAAQAAAAAAAAQAAAAAAAAQAAAAgBgBIgCADIgDAKQjCA9hPiOQBDClC0gpQgKAKgOAMQgUARgcASIgcAQQhgAwgjAFIgTABQg+gCgug+gAA2DZIAHACIgBgDIgGABg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-43.1,-33.5,86.3,67);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(51,51,51,0.8)").ss(5,1,1).p("AioopQEOAAB6CBQBoBtAMDiQABAdAAAgQAACggtB+QgtB/hTBIQhwBhj2AAQjeAAhxheQhghOgriJQgWhFgKhIQgLhGAAhIQAAkgCKh2QCDhtEOAAgAcDoPIggCsIilNvImSAAIhknBIgMAAIhiHBImNAAIh4p+IhNmdIFzAAIAwFeIAiD7IALAAIArkMIA1lNIEgAAIAsEFIA4FWIAJAAIAwlrIAfjwgAy0CXIjRAAIAABXIAAEeIl9AAIAAjXIAAtEIF9AAIAAF0IDRAAIAAl0IF9AAIAAKPIAAGMIl9AAIAAlFgAi3jDQg7AAgkAbQg3AoAAB4QAAAXABAVQAGB1ArAxQAiAoBDAAQBcAAAihJQAYg2AAhlQAAgRAAgRQgGhtgrgkQghgehFAAg");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FAEBAE").s().p("A8CmUIF9AAIAAF0IDRAAIAAl0IF9AAIAAKOIl9BHIAAgwIjRAAIAABXIl9BHgArDBUQAAkfCKh2QCDhtEOAAQEOAAB6CBQBoBtAMDhIl0BFQgGhsgrgkQghgehFAAQg7AAgkAbQg3AoAAB3QAAAXABAVIlsBEQgLhFAAhJgAGImUIFzAAIAwFeIlWA+gAO4mUIEgAAIAsEFImBBIgAWUmUIFvAAIggCsIluBEg");
	this.shape_1.setTransform(-10.5,-12.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("An9FoQhghNgriJQgWhFgKhJIFshDQAGB1ArAwQAiApBDAAQBcAAAihJQAYg3AAhjIAAgjIF0hFQABAdAAAgQAACfgtB/QgtB/hTBHQhwBij2AAQjeAAhxhfgAS8GpIhknBIgMAAIhiHBImNAAIh4p+IFWg/IAiD7IALAAIArkMIGBhIIA4FXIAJAAIAwlsIFuhEIilNvgAykGpIAAlGIF9hHIAAGNgA7yGpIAAjYIF9hHIAAEfg");
	this.shape_2.setTransform(-12.1,9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-192.4,-57.9,364,115.8);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(51,51,51,0.8)").ss(5,1,1).p("ANRjDQhlAAg/AbQhgAoAAB4QAAAeADAaQAOBsBFAuQA8AoB1AAQCfAAA8hJQAFgHAFgHQAgg1AAhYQAAiJhXgqQg5geh4AAgANropQHYAADTCBQDLB7AAERQAACghNB+QgIANgIAMQhNBtiCBBQjCBhmuAAQmCAAjEheQimhOhKiJQhKiHAAiUQAAgdACgbQAWj0DWhqQDkhtHUAAgAi7oPIAAFqIjFAAIkAAAIAAKxIqmAAIAAqxIm6AAIAAktIAAg9g");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CCFF").s().p("AEuGgQinhOhJiJQhKiHAAiTQAAgdACgbIKJCNQANBsBGAuQA7AoB1AAQCfAAA8hJIAKgOIJLCAQhNBtiBBBQjDBhmuAAQmBAAjEhegAz3HgIAAqxIm6AAIAAksIVgEsIkAAAIAAKxg");
	this.shape_1.setTransform(-15.2,4.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#BDF1FE").s().p("AQ5EiQAfg1AAhXQABiKhXgpQg5geh4AAQhmAAg+AbQhhAnAAB4QABAeADAaIqJiOQAWjzDWhqQDjhtHUAAQHYAADUCBQDLB7AAEQQAACghNB/IgRAYgAmAgdI1gktIAAg9IYlAAIAAFqg");
	this.shape_2.setTransform(-10.5,-13.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-189.1,-57.9,357.2,115.8);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(51,51,51,0.8)").ss(5,1,1).p("AGZBNIgmiZIgliUIgJAAIglCgIggCNgAHFFrIjvAAIggCkIleAAIB9odIB2n/IILAAIBeGRICYKLIllAAgAXpDdIAAEyIlgAAIAAkyIiWmJIiHliIFtAAIBhExIAAABIAIAAIABgDIBikvIFlAAIhhD6gA8JoKIJFAAQAzAAAxARQAxAQAfAbQBmBdAADKQAADFhJBlQgYAhgfAWQg0AkhLAOQgkAGggAAIjJAAIAAEdIlTAAIAAkVgA22gHIBHAAQAyAAAZgTQAlgeAAhFQAAhCgggaQgTgNgsAAIhYAAgAjUDDIAAFMIp4AAIAAmlIAAp3IFcAAIAAJDIAACNg");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#66CC00").s().p("AS5GSIAAkyIiWmJIFHgxIAAACIAIAAIABgDIFmg2IjAHxIAAEygAIXGSIgiikIjvAAIggCkIldAAIB8ocIFLgyIggCNICZAAIgmiZIFBgxICYKLgAscGSIAAmkIFdg0IAACMIEbAAIAAFMgA7YGSIAAkVIMVh3QgXAhggAWQgzAkhMAOQgkAGggAAIjIAAIAAEdg");
	this.shape_1.setTransform(-15.3,12.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CFFAA5").s().p("A8Il/IJEAAQAzAAAxARQAxAQAfAbQBnBcAADKQgBDFhJBlIsVB3gA22CDIBHAAQAyAAAZgUQAlgdAAhGQAAhBgggZQgTgOgsAAIhYAAgAtMmCIFdAAIAAJCIldA0gABKmCIILAAIBfGPIlCAxIgkiUIgJAAIglCgIlLAygANrmDIFuAAIBgEwIlGAygAWkmDIFlAAIhgD5IlmA2g");
	this.shape_2.setTransform(-10.5,-13.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-193.1,-55.1,365.3,110.3);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(5,1,1).p("ACNDhIm1AAQA5hSBFh8QBYidAAg4QAAhOh+AAQhjAAiAAgIAAnPQDlgyDBAAQBxAABOATQBMATA+AsQB4BWAAChQAABwgxBhQg7B1gEAIgAk/ISQAAAwATApQATApAgAdQAgAfAqARQAqARAwAAQAvAAApgRQApgRAggfQAggdASgpQASgpAAgwQAAgvgSgpQgSgpgggdQgggegpgTQgpgRgvAAQgwAAgqARQgqATggAeQggAdgTApQgTApAAAvg");
	this.shape.setTransform(-10.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF3333").s().p("AivLhQgqgRghgfQgfgegTgpQgSgpgBgvQABgvASgpQATgpAfgdQAhgeAqgTQAqgRAvgBQAxABAnARQApATAhAeQAgAdASApQASApAAAvQAAAvgSApQgSApggAeQghAfgpARQgnAQgxAAQgvAAgqgQgAknDhQA5hSBFh8QBXidABg3QAAhPh+AAQhkAAiAAhIAAnQQDmgxDAAAQBxAABOASQBMAUA+AsQB4BVAAChQAABwgxBiIg/B8Ii4E7g");
	this.shape_1.setTransform(-10.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.6,-77.8,92.2,155.7);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFCC").s().p("AmMGMQikikAAjoQAAjnCkilQClikDnAAQDoAACkCkQClClAADnQAADoilCkQikCljoAAQjnAAililg");
	this.shape.setTransform(304.6,-3.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF66").s().p("AqUKVQkSkRAAmEQAAmCESkSQERkSGDAAQGEAAERESQESESAAGCQAAGEkSERQkRESmEAAQmDAAkRkSgAmMmMQikClAADnQAADoCkCkQClClDnAAQDoAACkilQClikAAjoQAAjnililQikikjoAAQjnAAilCkg");
	this.shape_1.setTransform(304.6,-3.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("As5M6QlXlWAAnkQAAnjFXlWQFWlXHjAAQHkAAFWFXQFXFWAAHjQAAHklXFWQlWFXnkAAQnjAAlWlXgAqUqUQkSESAAGCQAAGEESERQERESGDAAQGEAAERkSQESkRAAmEQAAmCkSkSQkRkSmEAAQmDAAkRESg");
	this.shape_2.setTransform(304.6,-3.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AtaNbQljlkAAn3QAAnrFUlfIAPgQQFkljH2AAQH3AAFkFjQFjFkAAH2QAAH3ljFkIgQAOQlfFVnsAAQn2AAlkljgAs5s5QlXFWAAHjQAAHkFXFWQFWFXHjAAQHkAAFWlXQFXlWAAnkQAAnjlXlWQlWlXnkAAQnjAAlWFXg");
	this.shape_3.setTransform(304.6,-3.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC00").s().p("As5M6QlXlWAAnkQAAnjFXlWQFWlXHjAAQHkAAFWFXQFXFWAAHjQAAHklXFWQlWFXnkAAQnjAAlWlXgAqVqUQkRERAAGDQAAGDERESQESESGDAAQGDAAESkSQESkSAAmDQAAmDkSkRQkSkSmDAAQmDAAkSESg");
	this.shape_4.setTransform(-304.5,3.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFCC").s().p("AmMGNQikikAAjpQAAjoCkikQCkikDoAAQDpAACkCkQCkCkAADoQAADpikCkQikCkjpAAQjoAAikikg");
	this.shape_5.setTransform(-304.5,3.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF66").s().p("AqVKVQkRkSAAmDQAAmDERkRQESkSGDAAQGDAAESESQESERAAGDQAAGDkSESQkSESmDAAQmDAAkSkSgAmMmMQikCkAADoQAADpCkCkQCkCkDoAAQDpAACkikQCkikAAjpQAAjoikikQikikjpAAQjoAAikCkg");
	this.shape_6.setTransform(-304.5,3.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AtaNaQljljAAn3QAAnrFVlfIAOgQQFkljH2AAQH3AAFjFjQFkFkAAH2QAAH3lkFjIgPAQQlfFUnsAAQn2AAlklkgAs5s5QlXFWAAHjQAAHkFXFWQFWFXHjAAQHkAAFWlXQFXlWAAnkQAAnjlXlWQlWlXnkAAQnjAAlWFXg");
	this.shape_7.setTransform(-304.5,3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-425.9,-125.1,851.9,250.2), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFCC").s().p("AjGDHQhShTAAh0QAAhzBShTQBThSBzAAQB0AABSBSQBSBTAABzQAAB0hSBTQhSBSh0AAQhzAAhThSg");
	this.shape.setTransform(182.5,-2.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF66").s().p("AlJFKQiKiJAAjBQAAjACKiJQCIiJDBAAQDBAACKCJQCICJAADAQAADBiICJQiKCJjBAAQjBAAiIiJgAjGjGQhSBTAABzQAAB0BSBTQBTBSBzAAQB0AABShSQBShTAAh0QAAhzhShTQhShSh0AAQhzAAhTBSg");
	this.shape_1.setTransform(182.5,-2.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFCC00").s().p("AmdGdQiqirgBjyQABjxCqirQCsirDxAAQDyAACrCrQCsCrgBDxQABDyisCrQirCrjyAAQjxAAisirgAlJlJQiKCJAADAQAADBCKCJQCICJDBAAQDBAACKiJQCIiJAAjBQAAjAiIiJQiKiJjBAAQjBAAiICJg");
	this.shape_2.setTransform(182.5,-2.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AmsGtQiyixAAj8QAAj1CrivIAHgIQCxiyD7AAQD7AACyCyQCyCxAAD7QAAD8iyCxIgIAIQivCqj2AAQj7AAixiygAmdmcQiqCrgBDxQABDyCqCrQCsCrDxAAQDyAACrirQCsirgBjyQABjxisirQirirjyAAQjxAAisCrg");
	this.shape_3.setTransform(182.5,-2.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFCC00").s().p("AmcGdQisirABjyQgBjxCsisQCrirDxAAQDyAACsCrQCqCsABDxQgBDyiqCrQisCsjyAAQjxAAirisgAlKlKQiICJAADBQAADCCICJQCKCJDAAAQDCAACIiJQCKiJAAjCQAAjBiKiJQiIiJjCAAQjAAAiKCJg");
	this.shape_4.setTransform(-182.5,2.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFCC").s().p("AjFDGQhShSAAh0QAAhzBShTQBShRBzAAQB0AABTBRQBSBTAABzQAAB0hSBSQhTBSh0AAQhzAAhShSg");
	this.shape_5.setTransform(-182.5,2.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF66").s().p("AlKFLQiIiJAAjCQAAjBCIiJQCKiJDAAAQDCAACICJQCKCJAADBQAADCiKCJQiICJjCAAQjAAAiKiJgAjFjGQhSBTAABzQAAB0BSBSQBSBSBzAAQB0AABThSQBShSAAh0QAAhzhShTQhThRh0AAQhzAAhSBRg");
	this.shape_6.setTransform(-182.5,2.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AmsGtQiyiyAAj7QAAj1CqiwIAIgHQCyiyD6AAQD8AACxCyQCyCxAAD7QAAD7iyCyIgHAIQiwCqj2AAQj6AAiyiygAmcmdQisCsABDxQgBDyCsCrQCrCsDxAAQDyAACsisQCqirABjyQgBjxiqisQisirjyAAQjxAAirCrg");
	this.shape_7.setTransform(-182.5,2.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-243.2,-63.4,486.4,126.9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-0.8,243.5);
	this.instance.alpha = 0.801;
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, 33))];
	this.instance.cache(-245,-65,490,131);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFF00","#FF9900"],[0.718,0.976],0,0,0,0,0,295.7).s().p("EggSAgSQtXtXAAy7QAAy6NXtYQNYtXS6AAQS7AANXNXQNZNYgBS6QABS7tZNXQtXNZy7gBQy6ABtYtZgA5050QqtKsAAPIQAAPJKtKsQKsKtPIAAQPJAAKsqtQKtqsAAvJQAAvIqtqsQqsqtvJAAQvIAAqsKtg");
	this.shape.setTransform(-5.3,-12.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EghhAhiQt5t5AAzpQAAzNNTtuIAmgmQN5t5ToAAQTpAAN5N5QN5N5AAToQAATpt5N5IgmAmQtuNTzOAAQzoAAt5t5gEggSggSQtXNYAAS6QAAS7NXNXQNYNZS6gBQS7ABNXtZQNZtXgBy7QABy6tZtYQtXtXy7AAQy6AAtYNXg");
	this.shape_1.setTransform(-5.3,-12.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFCC").s().p("AvfPgQmbmbAApFQAApEGbmbQGbmbJEAAQJFAAGbGbQGbGbAAJEQAAJFmbGbQmbGbpFAAQpEAAmbmbg");
	this.shape_2.setTransform(-5.3,-12.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF66").s().p("A50Z1QqtqsAAvJQAAvIKtqsQKsqtPIAAQPJAAKsKtQKtKsAAPIQAAPJqtKsQqsKtvJAAQvIAAqsqtgAvfvfQmbGbAAJEQAAJFGbGbQGbGaJEAAQJFAAGbmaQGambAApFQAApEmambQmbmbpFAAQpEAAmbGbg");
	this.shape_3.setTransform(-5.3,-12.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,190.9);
	this.instance_1.filters = [new cjs.ColorFilter(0.48, 0.48, 0.48, 1, 132.6, 132.6, 132.6, 0), new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, 18))];
	this.instance_1.cache(-428,-127,856,254);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-425.9,-316,854,636.8), null);


(lib.Parrot_Winked2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// cap
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(0,88,175,0.102)","rgba(0,0,0,0.4)"],[0.4,1],4.4,7.6,0,4.4,7.6,29.7).s().p("AlBAdQAUhTBFg6QAYgUAegTQByhKDKgtIC4DsIhVAEIguALQi1AwiHBlQhFAzg6BBQgJAKgFAPg");
	this.shape.setTransform(25.2,-84.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#003366","#000F1E"],[0,1],8.7,-1.6,-8.7,1.7).s().p("AhYg0QAQgpAtgtQAbgbAjghIA2EaQhGAyg5BBg");
	this.shape_1.setTransform(4,-80.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#0278EE","#034689"],[0,1],2.5,-1.3,-2.8,1.8).s().p("AgGAlQhAhsgNgSIAbgWIAOgFIB+DeIguALIgshQg");
	this.shape_2.setTransform(41.2,-98.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#012548","#0360BD"],[0,1],-32.3,0,32.4,0).s().p("AlDATQAYhEBFg6QAYgUAegTQBxhKDAg3IBIBVIB7ChIhVAEIguALQi1AwiHBlQhFAzg6BBQgJAKgFAPg");
	this.shape_3.setTransform(25,-85.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#00152C","#022C5B"],[0,1],11.3,-10.1,0,11.3,-10.1,17.7).s().p("AhRBTIAhgSQALgGAWgSQAUgSAkgpQAjgpgigKQgigKgFgMIB+gdIgiBAQgTAlgdAeQgRARgUAQQgTAQgVAMQgkAXgnANIgXAOQAZgbAWgMg");
	this.shape_4.setTransform(62.5,-122);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#014180","#3496F7","#0F67BE"],[0,0.553,1],-25.2,0,25.3,0).s().p("AjrClQAOg6AkgvQAMgRANgPIADgCQA4g8BfhRQASgSAYgQQBYg9CBgtQhSAjh1BdQh1BdhEBVQgWAdgSAfQgmBAgTBOIgXADQgDgNAThOg");
	this.shape_5.setTransform(-13.4,-101.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#000F1E","#002E5D","#024C98"],[0.278,0.749,0.894],-4.4,10.9,0,-4.4,10.9,30.1).s().p("AoyFNIgBAAQgcgDgfgGQAahyBLhoQAMgSAOgQIABgCQCEilEEiKIA1AOIA2AJIABAAQDJAaDohNQBagdBegvQgEBqibCCQg8AwhRA1QgbASggATIhIhUQjAA2hxBKQgeATgYAUQhFA6gYBFIAMA2IgGADIgYAIQhhAahmAAQgqAAgrgFg");
	this.shape_6.setTransform(18.7,-107.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(75));

	// Layer_2
	this.instance = new lib.Tween8("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-42,-76.2,1,1,0,0,0,-8.4,-51.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({startPosition:0},2).to({regX:-8.6,regY:-51.5,rotation:6.2,x:-42.2,y:-76.4},11).to({regX:-8.4,regY:-51.4,rotation:0,x:-42,y:-76.2},9).to({regX:-8.6,regY:-51.5,rotation:6.2,x:-42.2,y:-76.4},12).to({regX:-8.4,regY:-51.4,rotation:0,x:-42,y:-76.2},9).to({regX:-8.6,regY:-51.5,rotation:6.2,x:-42.2,y:-76.4},9).to({regX:-8.4,regY:-51.4,rotation:0,x:-42,y:-76.2},9).wait(14));

	// wing_f
	this.instance_1 = new lib.Tween7("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(44.5,0.5,1,1,0,0,0,49.8,-39.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({startPosition:0},11).to({startPosition:0},6).to({regX:49.7,rotation:26.9,x:44.6},7).to({rotation:15.7,x:44.5},10).to({regX:49.8,rotation:0},8).to({regX:49.7,rotation:26.9,x:44.6},7).wait(26));

	// mouth
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FFFF00","#FFFFFF"],[0,0.992],-11.1,3.2,11.1,-3.2).s().p("AAyARQgpgbg+geQg+gfgkgkIBOAvQBJAdA+ArQAdATAYAeIAkAvQg+hBgngag");
	this.shape_7.setTransform(86.6,-42.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FF0000","#000000"],[0,1],0.7,0.1,0,0.7,0.1,7.5).s().p("Ag4APQgFgQAAgGQAAgLAGgKIAIgKIAAgBQAFgDAFAAQASACAIATIALAkQAMANAygNQgaAMgTAHIALgEIgcAKIgCABQgKACgIAAQgcAAgIgcg");
	this.shape_8.setTransform(67.3,-48.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#FFCC00","#FF3300"],[0,1],-8.6,3.7,10.9,-4.5).s().p("Ag/BOIgEgFQgKgRgGgUIgCgDQgMgjgVhOIgKgpIgLgrIACAKQAPA0AdAcIABADQABALADALIAAADQAFAXAGAUQAFAPAFAOQAMAYAPARQAoAtAsgOQAngMAHgxIABgIIABgpIAAgdIgBgOIAMgCIACgBIACAHIABAIQABAbgDApQAAATAEAUQAGAsAYA4IgDABQiPAAg5hXg");
	this.shape_9.setTransform(73,-23.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#660000","#000000"],[0,1],3.1,-4.3,0,3.1,-4.3,11.8).s().p("AgpAoQgQgRgLgXQgGgOgEgQQgHgUgEgXQASAYBHgEQBIgGASAUIgBApIAAAIQgHAxgnAMQgKADgLAAQggAAgfgig");
	this.shape_10.setTransform(73.1,-20.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FF0000","#000000"],[0,1],4.8,6.4,0,4.8,6.4,10.3).s().p("AACARQhHAEgSgXIgBgDQgCgLgBgLIgBgDQANAMAOAGQAmASBCgIIA0gJIABANIAAAdQgSgUhIAGg");
	this.shape_11.setTransform(72.9,-28);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FFCC00","#FF3300"],[0,1],-0.9,0.6,0,-0.9,0.6,29.1).s().p("AAVB+IgBgGIgDABIgMABIg0AJQhCAJgmgTQgPgGgMgMQgdgcgPg0IgDgKIALAsQgMgXgJgdIgDgKIgBgGQgDgLgBgIQgGgWAAgKIAAgNQACgQAGgNQAHgQAOgNIAAAEIAxgBQAsgBAqAIQDLAiB/DOIAEAIIABABIAAAAIgDgCQgggOgxABQgTAAgVADIg4ALIgUAFIgZAEIgDAAIgBgIg");
	this.shape_12.setTransform(81.6,-41.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#FF0000","#000000"],[0,1],0,0.9,0,0,0.9,27.3).s().p("ADlCEQh+jOjLgjQgqgHgtAAIgwACIAAgFIABgBIAugKQEmg3BzEWIAPAtIgCACIgFgIg");
	this.shape_13.setTransform(83.1,-42.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]}).wait(75));

	// eyes
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(243,243,243,0.502)").s().p("AgBA3QgVgLgPgZQgQgWACgXQABgXAPgIQAQgIAUAJIAEADQAUALAOAYQAOAWgBAXQAAAXgQAJQgGADgHAAQgLAAgNgHg");
	this.shape_14.setTransform(28.5,-42.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F3F3F3").s().p("AApAyQgEgBgCgDIgDgEQgEgGAAgFQABgGACgCQAFgCAFACQAAAAAAAAQAAAAAAABQABAAAAAAQAAAAABAAQACABACAEIADAEQADAFAAAGQAAAGgEACIgCAAQgDAAgDgCgAgbAEQgKgEgGgLQgJgLAAgLQACgLAHgFQAHgEALAFIABABQAKAGAGALQAIAMgBAKQABAKgIAEQgEACgEAAQgFAAgGgEg");
	this.shape_15.setTransform(36.1,-40);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000033").s().p("AgBA1QgUgKgPgXQgOgXAAgWQACgWAQgJQAOgHAVALIACACQAUAJANAWQAOAXgBAWQAAAGgBAGQgCgEgDgBQAAAAAAAAQgBAAAAAAQAAAAAAAAQgBgBAAAAQgFgCgEACQgCACgBAGQgBAFAEAGIADAEIgCABQgHAEgHAAQgLAAgLgHgAgpgoQgGAFgCALQgBALAJALQAHALAJAEQAMAHAHgFQAHgEAAgLQAAgJgGgMQgHgLgJgGIgCgBQgGgDgFAAQgEAAgDACg");
	this.shape_16.setTransform(35.9,-40.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0066FF").s().p("AgDBoQgpgUgagtQgeguACgrQABgpAfgSQAegPApAVIAEADIAAgBQAlAVAbArQAbAsgCAqQAAAtgeAQQgMAGgPAAQgVAAgXgMgAAvA3QAFADAEgBQADgCABgGQAAgGgDgFIgDgEQABgGAAgGQABgWgOgXQgNgWgUgJIgCgCQgVgLgOAHQgQAJgCAWQAAAWAOAXQAPAXATAKQAWALAPgIIACgBQADADADABg");
	this.shape_17.setTransform(35.5,-40.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAPCRQgngagmgmIgYgcQgyg7ACg+QAAg/AqgWQAqgYA3AhQArAYAfAsQAKANAJAQQAmBAgBA7QgBA5ggAXQgMAIgRAAQgZAAghgTg");
	this.shape_18.setTransform(32.6,-45.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#999999","#666666"],[0,1],-15.8,-4.6,16.2,4.1).s().p("AAQCYQgpgagogpIgagdQgzg+AChBQgBhCAsgYQAsgYA6AhQAsAaAiAtQALAOAJASQAoBCgBA+QgBA9ghAXQgOAJgRAAQgaAAgjgUgAhciaQgqAXAAA/QgCA+AyA6IAYAcQAmAnAnAZQA3AgAggVQAggXABg5QABg6gmhAQgJgQgKgOQgfgsgrgYQgggSgcAAQgUAAgRAJg");
	this.shape_19.setTransform(32.6,-45.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FFCC00","#FF6600"],[0,1],-18.2,-5.3,18.6,4.8).s().p("AASCvQgygggvgyQgNgNgMgPIgBgBQg7hIABhKQABhIAsgbIAGgEQAzgcBCAmQA2AfAoA3QALAPAIARIABAAQAuBOgCBHQgBBFgnAbQgPAKgTAAQgeAAgpgXgAhhihQgsAYABBCQgCBBAzA+IAaAdQAoApApAaQA6AhAigWQAhgXABg9QABg+gohCQgJgSgLgOQgigtgsgaQghgTgdAAQgVAAgTAKg");
	this.shape_20.setTransform(32.6,-45.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(243,243,243,0.502)").s().p("AgDA7QgUgMgOgbQgQgZADgYQACgaAPgHQARgKATALIADADQAVAMANAcQANAYgCAYQAAAagRAIQgHAEgHAAQgLAAgMgJg");
	this.shape_21.setTransform(63.7,-61.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#F3F3F3").s().p("AAbApIgDgEIgCgDQgDgFAAgEQAAgEADgCQAEgBADABIABABQACABABAEIADADQACAEAAAFQAAAEgEABIgBAAIgGgBgAgTADQgJgDgEgKQgEgHgBgIIAAgDQACgIAEgEQAHgDAGAEIACABQAHAEAEAKQAFAJgBAIQAAAIgFAEIgFABQgEAAgEgDg");
	this.shape_22.setTransform(67.9,-58.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#0066FF").s().p("AgEBTQgegRgTglQgTglABgiQACgiAXgNQAXgMAdASIADACQAcATASAiQASAjgBAjQgCAjgXANQgJAFgKAAQgPAAgRgMgAAgAtQAFACACgBQAEgBAAgEQAAgFgCgEIgDgDIACgKQABgSgJgSQgJgSgPgJIgCAAQgOgKgLAFQgMAIgBARIAAADQAAARAJAQQAJASAOAJQAPAKAMgHIACgBIADAEg");
	this.shape_23.setTransform(67.4,-58.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000033").s().p("AgCArQgPgJgKgTQgJgQAAgQQABAHAEAHQAFAJAIAFQAHAFAGgDQAFgEAAgJQABgHgEgKQgFgJgGgFIgCAAQgHgFgGAEQgFADgCAIQACgRAMgHQAKgGAPAKIABABQAQAIAIASQAJATAAASIgCAJQgCgDgCgBIAAgBQgEgCgDACQgDABAAAFQAAADACAFIACAEIgBABQgFACgGAAQgHAAgIgFg");
	this.shape_24.setTransform(67.7,-59.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAIB8QgegWgdgjIgSgYQgmg1ADg0QAEg2AggSQAjgTAqAdQAgAWAYAmQAIAMAHAOQAbA4gDAyQgBAxgbATQgJAGgMAAQgUAAgbgSg");
	this.shape_25.setTransform(67.4,-61.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.lf(["#999999","#666666"],[0,1],-12.4,-4.5,12.6,3.9).s().p("AAICCQgggXgeglIgTgZQgng3ADg4QACg4AjgTQAkgTAsAeQAiAXAaAoQAIAMAHAPQAdA6gDA1QgCA1gbATQgKAGgNAAQgVAAgcgTgAhFiGQggARgDA2QgDA1AmA1IASAYQAdAiAeAWQAqAeAagSQAagSACgxQADgzgcg3QgGgOgIgNQgYgmghgWQgYgRgXAAQgQAAgOAIg");
	this.shape_26.setTransform(67.5,-61.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#FFCC00","#FF6600"],[0,1],-14.3,-5.1,14.5,4.6).s().p("AAKCWQgngcgkgsIgSgaIgBAAQgtg/AEhBQACg9AkgWIAFgDQApgXAzAjQApAcAeAwQAIAMAHAQIAAAAQAiBDgDA9QgEA8gfAWQgMAIgOAAQgZAAgfgWgAhIiNQgjATgBA4QgDA4AmA3IAUAZQAeAlAfAXQAtAfAbgSQAbgTADg1QADg1gdg6QgIgPgIgMQgZgogigXQgbgSgYAAQgQAAgOAHg");
	this.shape_27.setTransform(67.4,-61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14}]}).wait(75));

	// body
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(255,255,255,0.2)").s().p("AAKDgQhaguhDhiQhChggEheQgGhcA+ghQA9ghBbAuQBYAsBDBiQBEBhAEBcQAEBfg9AgQgaAOgfAAQgrAAgzgag");
	this.shape_28.setTransform(33.7,-41);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#0BE0AB").s().p("AAAAAQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAAAABAAIgBADIAAgCg");
	this.shape_29.setTransform(8.1,57);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],64.7,-34,0,64.7,-34,102.5).s().p("AjtG1QhHgFhLgRIgxgMQgzgPg2gUQAfgnAcgoIAAAAIAIgMIACgCIArhBQCLjVBCjRIgEASQISjdDogXQgVAHgRALQgVANgRASQgRATgOAaQgRAfgMApQgWBMgRBNQgSBUgbBQQgYBKgnBBIgJAOQgJAOgMANIhKBQIgNAOIhbBdQhQAUhSAGQgdABgdAAQggAAgggCgAiqCZQABAGAAgIQgBAAAAAAQAAABAAAAQgBAAAAAAQAAABABAAg");
	this.shape_30.setTransform(25.2,41.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.rf(["#FFFF00","#FF9900"],[0,1],16.4,11.5,0,16.4,11.5,28.3).s().p("AjzFOIANgOIBKhQQAMgNAJgOIAJgOQAnhBAYhKQAbhQAShUQARhNAVhMQAMgpARgfQAOgaARgTQARgSAVgNQARgLAVgHQCigRAQBRIABAeQAAAlgCAlQgMB9g2B1QhBCLh0BgIgPAMQggAZgkAVQidBhi0AyIBbhdg");
	this.shape_31.setTransform(63.4,40.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#003366").ss(1,1,1).p("AA7ggQg1AjhAAe");
	this.shape_32.setTransform(33.8,76.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CC6600").s().p("AgBAAIADgCIAAAAIgCAFIgBgDg");
	this.shape_33.setTransform(53,98.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#993300").s().p("AgBAAIADAAIAAABg");
	this.shape_34.setTransform(51.3,81.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#FFFF00","#FF9900"],[0,1],16.4,12,0,16.4,12,28.3).s().p("AjzFTIANgOIBKhQQAMgNAJgOIAJgOQAnhBAYhKQAbhQAShUQARhNAVhMQAWhMAmgpQARgSAVgNQAcgSAlgIQAagGAdACQArADAxAXQAJBOgGBMQgMB9g2B1QhBCLh0BgIgPAMQggAZgkAVQidBhi0AyIBbhdg");
	this.shape_35.setTransform(63.4,39.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#0BE0AB").s().p("AkrEnQAAgBAAAAQAAAAAAgBQAAAAABAAQAAAAAAAAIAAAEIgBgCgAErkoIABABIgBACIAAgDg");
	this.shape_36.setTransform(38,27.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#51FD00","#47E202","#074A02"],[0,0.42,1],-6.6,-0.4,0,-6.6,-0.4,102.5).s().p("AlEOEQhHgFhLgRIgxgNQgzgPg2gUQAfgnAcgnIAAgBIAIgLIACgDIArhAQC3kZA5kSQBDlIh1k/QgShYAHgzQAIgzAzhPQA0hPBlgRQBlgSBiAcQBiAbBAA2QA/A2AgAkQAfAkAmA6QAnA5ApBIQBcC9AhDjIAJBOQAFAQACASQgxgXgrgDQgdgDgaAGQglAJgcARQgVANgRASQgmAqgWBMQgWBLgRBOQgSBTgbBSQgYBKgnBBIgJANQgJAPgMAMIhKBRIgNANIhaBdQhRAVhSAFQgdACgdAAQggAAgggCgAkBJoQABAGAAgIQgBAAAAAAQAAAAAAABQAAAAAAAAQAAABAAAAgAFUAbIABgCIgBgBIAAADg");
	this.shape_37.setTransform(33.9,-4.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#003366").ss(1,1,1).p("AA7ggQg1AjhAAe");
	this.shape_38.setTransform(33.8,76.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#CC6600").s().p("AgBAAIADgCIAAAAIgCAFIgBgDg");
	this.shape_39.setTransform(53,98.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#993300").s().p("AgBAAIADAAIAAABg");
	this.shape_40.setTransform(51.3,81.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28}]}).wait(75));

	// tail
	this.instance_2 = new lib.Tween9("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-18.4,57.5,1,1,0,0,0,60.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({startPosition:0},16).to({rotation:-7},10).to({rotation:0},10).to({rotation:-7},9).to({rotation:0},10).wait(20));

	// wing_BK
	this.instance_3 = new lib.Tween6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(64.4,-9.3,1,1,75,0,0,-44.9,-13.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({startPosition:0},16).to({regX:-45,regY:-13.2,rotation:0,x:64.3},8).to({regX:-44.9,regY:-13.3,rotation:75,x:64.4},18).to({regX:-45,regY:-13.2,rotation:0,x:64.3},8).wait(25));

	// legs
	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#431600").s().p("AAEAKQg7gQgoAIIAsgvQAUgVARAGIgDAAIgEAHIgPAWIgTAVQAVgGAXAIIAoAPQAWAGAVASQANALALAOIgEAGQgaghg+gTg");
	this.shape_41.setTransform(52.6,87);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#CC6600").s().p("AA8BPIgagRQgngWgfgNQgbgLgwgCIhLgBQAAAAABAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIAkgeQAQgOAbgUQATAUBXAxQBDAmAhAkQgFACgEAAQgNAAgSgMgACbgbIhOg4IAEgHIADAAIAEACQAnAcAVARQAYASAPASQgQgIgQgMg");
	this.shape_42.setTransform(42.7,90);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF9900").s().p("AABACQhXgxgTgUIAMgIIAFACIBnBBQBCAqAZAnIgBAAIgEADQghgkhDgmg");
	this.shape_43.setTransform(42.6,91.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#993300").s().p("AgwAZQhmgZhEANIBLhDQAjggAfALIgMAIQgbATgRAOIgjAgQAAAAAAABQAAAAgBABQAAAAAAAAQAAAAAAAAIBLAAQAvACAcAMQAeAMAoAWIAZASQATALANAAQADAAAFgCIADADQgFAHgLANQgsgvhrgagAC7gMQgVgSgWgGIgpgQQgXgHgVAFIATgUIAPgXIBOA5QARALAPAIQAKAMAGAKQAAAAAAABQAAAAgBABQAAAAAAAAQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAgBAAAAQAAgBAAAAQgBADgFAIQgLgNgNgLg");
	this.shape_44.setTransform(41.1,91.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41}]}).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-139.2,-141.6,246,242.8);


// stage content:
(lib.HowToPlay = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EggRAgSQtYtXAAy7QAAy6NYtXQNXtZS6ABQS7gBNXNZQNZNXgBS6QABS7tZNXQtXNZy7gBQy6ABtXtZg");
	mask.setTransform(640,391.5);

	// Layer_4
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(877,272.6);
	this.instance.alpha = 0.328;
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(30).to({_off:false},0).to({y:498.5,alpha:1},4).to({y:423.2},3).to({y:498.5},3).to({startPosition:0},34).wait(1));

	// Layer_3
	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(642.2,498.5,2,2);
	this.instance_1.alpha = 0.191;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(24).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},6).to({startPosition:0},44).wait(1));

	// Layer_1
	this.instance_2 = new lib.Tween3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(639.4,373.4,2,2,-25.2);
	this.instance_2.alpha = 0.191;
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0,x:639.5,alpha:1},7).to({startPosition:0},50).wait(1));

	// Layer_2
	this.instance_3 = new lib.Tween4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(642.9,252.7,2,2,26.2);
	this.instance_3.alpha = 0.191;
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(10).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:0,x:642.8,alpha:1},7).to({startPosition:0},57).wait(1));

	// parrot
	this.instance_4 = new lib.Parrot_Winked2("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(1005.7,597.3,1.012,1.012,0,0,180,6.7,-20.1);

	this.instance_5 = new lib.Parrot_Winked2("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(284.6,597.3,1.012,1.012,0,0,0,6.7,-20.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5},{t:this.instance_4}]}).wait(75));

	// Layer_5
	this.instance_6 = new lib.Symbol1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(645.3,404);
	this.instance_6.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(0, 0, 0, -58))];
	this.instance_6.cache(-428,-318,858,641);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(776.8,448,1016.6,640);
// library properties:
lib.properties = {
	id: '509C8CAA7D4C684A8790B1A978774CA5',
	width: 1280,
	height: 720,
	fps: 30,
	color: "#66FFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['509C8CAA7D4C684A8790B1A978774CA5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;