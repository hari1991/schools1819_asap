<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assessment_model_set2 extends CI_Model {

        
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				 $this->load->database();
        }

        public function checkUser($username,$password,$langid)
        {
			$query = $this->db->query('select *,(SELECT language_key FROM language_master WHERE ID="'.$langid.'") as languagekey FROM users a WHERE username="'.$username.'" AND password="'.md5($password).'" AND status=1 AND (SELECT school_id FROM school_admin WHERE school_id=a.sid AND active=1)');
			
			//echo $this->db->last_query(); exit;
			return $query->result();
        }
		
		 public function getsetdatas($planid,$gradeid)
        {
			$query = $this->db->query('select gradeid_set2,gradeid_set3 FROM  assesment_class_set WHERE plan_id="'.$planid.'" AND gradeid_set1="'.$gradeid.'"');
			//echo $this->db->last_query(); exit;
			return $query->result();
		}
		      	
		public function getdates($userid)
		 {
			 
		$query = $this->db->query("select AY.startdate,AY.enddate from users UG,academic_year AY where AY.id=UG.academicyear and UG.id='".$userid."' limit 0,1");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }	
		 
		  public function getbspireport($userid)
		 {
			 
		$query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth  FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."' and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1)    group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 
		 
		 
		 public function languagechange($email)
		 {
		$query = $this->db->query("select * from language_master where status='Y' and ID in (select languageID from schools_language where schoolID=(select sid from users where username='".$email."' and status=1) and status='Y')");
		//echo $this->db->last_query(); exit;
			return $query->result();
		 }
		 public function update_loginDetails($userid)
		 {
		$query = $this->db->query("update users set pre_logindate = login_date,login_date = CURDATE(),login_count=login_count+1 WHERE id =".$userid);
		//echo $this->db->last_query(); exit;
		 }	

		public function getRandomGames($check_date_time,$game_plan_id,$game_grade,$school_id)
		 {
			 
		$query = $this->db->query("SELECT gid FROM rand_selection_set2 WHERE DATE(created_date) = '".$check_date_time."' AND gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' GROUP BY school_id, gs_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getAssignGames($game_plan_id,$game_grade,$uid,$catid)
		 {
			 
		$query = $this->db->query("SELECT a.id, a.grade_id, d.skill_id FROM users AS a JOIN g_plans AS b ON   b.id = '".$game_plan_id."' JOIN class_plan_game AS c ON b.id = c.plan_id AND b.grade_id = c.class_id JOIN class_skill_game AS d ON c.class_id = d.class_id AND c.game_id = d.game_id AND d.class_id = '".$game_grade."' JOIN category_skills AS e ON e.id = d.skill_id WHERE a.id = '".$uid."' AND e.category_id = '".$catid."' GROUP BY d.skill_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getSkillsRandom($catid)
		 {
			 
		$query = $this->db->query("SELECT a.id AS category_id, b.id AS skill_id FROM g_category AS a JOIN category_skills AS b ON a.id = b.category_id WHERE a.id = '".$catid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getSK_SkillsRandom($uid)
		 {
			 
		$query = $this->db->query("SELECT b.id AS skill_id  from category_skills AS b WHERE FIND_IN_SET ( b.id , ( select weakSkills from sk_user_game_list where 	userID='".$uid."' and status=0))");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function assignRandomGame($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id)
		 {
			 
		$query = $this->db->query("SELECT j.id, e.skill_id, j.name AS skill_name, g.gid, g.gname FROM users AS a
				JOIN g_plans AS b ON   b.id = '".$game_plan_id."' JOIN class AS c ON   c.id = '".$game_grade."' JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id JOIN category_skills AS j ON e.skill_id = j.id JOIN skl_class_plan AS f ON a.sid = f.school_id JOIN games AS g ON d.game_id = g.gid JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$school_id."' WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' and j.id = '".$skill_id."' and g.gid not in (SELECT gid FROM rand_selection_set2 WHERE gp_id = '".$game_plan_id."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' and gs_id = '".$skill_id."') GROUP BY g.gid ORDER BY RAND() LIMIT 1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function assignSK_RandomGame($check_date_time,$game_plan_id,$game_grade,$uid,$skill_id,$school_id)
		 {
			 
		$query = $this->db->query("SELECT gid FROM sk_rand_selection_set2 WHERE DATE(created_date) = '".$check_date_time."' AND gp_id = '".$game_plan_id."' AND userID = '".$uid."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' AND gs_id='".$skill_id."' GROUP BY school_id, gs_id,gid order by gs_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getAssignSK_RandomGame($check_date_time,$game_plan_id,$game_grade,$uid,$school_id)
		 {
			 
		$query = $this->db->query("SELECT gid FROM sk_rand_selection_set2 WHERE DATE(created_date) = '".$check_date_time."' AND gp_id = '".$game_plan_id."' AND userID = '".$uid."' AND grade_id = '".$game_grade."' AND school_id = '".$school_id."' GROUP BY school_id, gs_id,gid order by gs_id");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function assignSK_assignGameCount($game_plan_id,$skill_id,$school_id)
		 {
			 
		$query = $this->db->query("select * from sk_personalized_game where sk_planSkillCountID in (select ID from sk_plan_skillcount where school_ID='".$school_id."' and plan_ID='".$game_plan_id."' ) and skillID = '".$skill_id."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getSK_randomGames($game_plan_id,$game_grade,$uid,$skill_id,$school_id,$assign_count,$cur_day_skills)
		 {
			 
		$query = $this->db->query("SELECT g.skill_ID as skill_id,(select name from category_skills where id=g.skill_ID) as skill_name,g.name as gname,g.ID as gid FROM sk_games g join sk_games_plan gp on g.ID=gp.sk_game_ID  join sk_personalized_game pg on gp.plan_ID=from_GradeID WHERE g.skill_ID='".$skill_id."' and gp.school_ID='".$school_id."' and  pg.skillID=g.skill_ID and pg.sk_planSkillCountID in (select ID from sk_plan_skillcount where school_ID='".$school_id."' and plan_ID='".$game_plan_id."' ) and g.ID not in (SELECT gid FROM sk_rand_selection_set2  WHERE gp_id = '".$game_plan_id."' AND userID = '".$uid."' AND grade_id = '".$game_grade."'  AND school_id = '".$school_id."' and gs_id = '".$skill_id."') order by rand() limit ".($assign_count-$cur_day_skills)." ;");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 } 
		 public function getSK_mindatePlay($game_plan_id,$game_grade,$uid,$skill_id,$school_id,$catid)
		 {
			 
		$query = $this->db->query("select min(created_date) as mindate from sk_rand_selection_set2 where gc_id = '".$catid."' AND userID = '".$uid."'  and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function deleteSK_OldGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$mindate)
		 {
			 
		$query = $this->db->query("delete from sk_rand_selection_set2 where gc_id = '".$catid."' AND userID = '".$uid."'  and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."'  and created_date='".$mindate."'");
		//echo $this->db->last_query(); exit;
			 
		 }public function deleteRandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$del_where)
		 {
			 
		$query = $this->db->query("delete from rand_selection_set2 where gc_id = '".$catid."' and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' ".$del_where);
		//echo $this->db->last_query(); exit;
			 
		 }
		 public function deleteSK_RandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$del_where)
		 {
			 
		$query = $this->db->query("delete from sk_rand_selection_set2 where gc_id = '".$catid."' AND userID = '".$uid."'  and gs_id = '".$skill_id."' and gp_id = '".$game_plan_id."' and grade_id = '".$game_grade."'  and school_id = '".$school_id."' ".$del_where);
		//echo $this->db->last_query(); exit;
			 
		 }
		 public function insertRandomGames($catid,$game_plan_id,$game_grade,$skill_id,$school_id,$section,$gameid,$check_date_time)
		 {
			 
		$query = $this->db->query("INSERT INTO rand_selection_set2 SET gc_id = '".$catid."', gs_id = '".$skill_id."', gid = '".$gameid."', gp_id = '".$game_plan_id."', grade_id = '".$game_grade."', section = '".$section."', school_id = '".$school_id."', created_date = '".$check_date_time."'");
		
			 
		 } 
		 public function insertSK_RandomGames($catid,$game_plan_id,$game_grade,$uid,$skill_id,$school_id,$section,$gameid,$check_date_time)
		 {
			 
		$query = $this->db->query("INSERT INTO sk_rand_selection_set2 SET userID='".$uid."', gc_id = '".$catid."', gs_id = '".$skill_id."', gid = ".$gameid.", gp_id = '".$game_plan_id."', grade_id = '".$game_grade."', section = '".$section."', school_id = '".$school_id."', created_date = '".$check_date_time."';");
		
			 
		 }
		 public function getActualGames($game_plan_id,$game_grade,$uid,$catid,$where)
		 {
		$query = $this->db->query("SELECT j.id, (SELECT ntimes from game_limit where g_id=g.gid and gp_id=b.id and gs_id=e.skill_id limit 1) as ncount, (select count(*) as tot_game_played from game_reports_set2 where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id AND lastupdate = '".date('Y-m-d')."') as tot_game_played ,(select SUM(game_score)  from game_reports_set2 where gu_id =  '".$uid."'  AND gc_id = '".$catid."' AND gs_id = e.skill_id AND gp_id = b.id AND lastupdate = '".date('Y-m-d')."') as tot_game_score , e.skill_id, j.name AS skill_name, g.gid, g.gname, g.img_path,g.game_html, j.icon 
		FROM users AS a
		JOIN g_plans AS b ON   b.id = '".$game_plan_id."'
		JOIN class AS c ON   c.id = '".$game_grade."'
		JOIN class_plan_game AS d ON c.id = d.class_id AND d.plan_id = b.id
		JOIN class_skill_game AS e ON e.class_id = c.id AND  d.game_id = e.game_id
		JOIN category_skills AS j ON e.skill_id = j.id 
		JOIN skl_class_plan AS f ON a.sid = f.school_id
		JOIN games AS g ON d.game_id = g.gid
		JOIN skl_class_plan AS h ON h.plan_id = b.id AND h.class_id = c.id AND h.school_id = '".$_SESSION['school_id']."'
		WHERE a.id = '".$uid."' AND g.gc_id = '".$catid."' $where
		GROUP BY g.gid");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	 
		 public function getSK_ActualGames($game_plan_id,$game_grade,$uid,$catid,$where)
		 {
		$query = $this->db->query("SELECT  g.skill_ID as skill_id,game_html,(select count(*) as tot_game_played from sk_game_reports_set2 where gu_id = '".$uid."'  AND gc_id = '".$catid."' AND g_id = g.ID AND gp_id = '".$game_plan_id."' AND lastupdate = '".date('Y-m-d')."') as tot_game_played,(select name from category_skills where id=g.skill_ID) AS skill_name, g.ID as gid, g.name as gname, g.image_path as img_path FROM sk_games AS g where 1 $where order by skill_id ");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 public function getTrainCalendar($userid,$startdate,$enddate,$catid)
		 {
		$query = $this->db->query("select group_concat(distinct(lastupdate)) as updateDates  from game_reports_set2 WHERE gu_id = '".$userid."' and (lastupdate between '".$startdate."' and '".$enddate."')  and gc_id = '".$catid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function getBSPI($userid)
		 {
		$query = $this->db->query("SELECT (AVG(`game_score`)) as score ,gs_id , lastupdate FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."'  group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 
		 public function getleftbardata($userid)
		 {
			 
		$todaydate=date('Y-m-d');
		$query = $this->db->query("select avatarimage,fname,DATEDIFF(CURDATE(), login_date) as noofdays,pre_logindate from users where status=1 and id='".$userid."'");
		//echo $this->db->last_query(); exit;
			return $query->result();
		 }			 
		 
		 public function getMyCurrentTrophies($userid)
		 {
		$query = $this->db->query("select cs.id as catid,cs.name as name ,(select sum(diamond) from popuptrophys pt where gu_id=".$userid." and pt.catid=cs.id) as diamond ,(select sum(gold) from popuptrophys pt where gu_id=".$userid." and pt.catid=cs.id) as gold ,(select sum(silver) from popuptrophys pt where gu_id=".$userid." and pt.catid=cs.id) as silver from category_skills cs where category_id=1 ");
		//echo $this->db->last_query(); //exit;
		//echo "<pre>";
		//print_r($query->result_array());
		//exit;
			return $query->result_array();
		 }		

		public function getmyprofile($userid)
		 {
			 
		$query = $this->db->query("SELECT *, (SELECT enddate FROM academic_year LIMIT 1) as planenddate, (SELECT school_name FROM schools WHERE id=us.sid) as schoolname  FROM users us where id='".$userid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }			

		public function getplandetais($planid)
		 {
			 
		$query = $this->db->query("select * FROM g_plans where id='".$planid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	

		public function getgradedetais($gradeid)
		 {
			 
		$query = $this->db->query("select distinct(classname),id from class where id='".$gradeid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	

		public function updateprofile($sfname,$gender,$dob,$fathername,$mothename,$address,$sphoneno,$id,$newpass,$confirmpass)
		
		 {
			 
			 if($newpass !='' && $confirmpass!='')
{
	 $newpassword=md5($newpass); 		 
			 
		$query = $this->db->query("update users set fname = '".$sfname."',password='".$newpassword."',father='".$fathername."',mother='".$mothename."',gender='".$gender."',dob='".$dob."',mobile='".$sphoneno."',address='".$address."' where id = '".$id."'");
}
else{
	
	$query = $this->db->query("update users set fname = '".$sfname."',father='".$fathername."',mother='".$mothename."',gender='".$gender."',dob='".$dob."',mobile='".$sphoneno."',address='".$address."' where id = '".$id."'");
	
}
		//echo $this->db->last_query(); exit;
			//return $query->result_array();
		 }


	   public function getgameplanid($userid)
		 {
			 
		$query = $this->db->query("select gp_id from users where id = '".$userid."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }

		public function getgamenames($userid,$pid)
		 {
			 
		$query = $this->db->query("SELECT a.gid,a.gname FROM  games a, game_reports_set2 b where a.gid=b.g_id and b.gu_id='".$userid."' and b.gp_id = '".$pid."'  and a.gc_id = 1 group by a.gid");
		//echo $this->db->last_query(); exit;
			return $query->result();
		 }

		public function getgamedetails($userid,$gameid,$pid)
		 {
			 
		$query = $this->db->query("select g_id, avg(game_score) as game_score,lastupdate from game_reports_set2 where gp_id = '".$pid."' and gu_id='".$userid."' and g_id='".$gameid."'  group by lastupdate  ORDER BY id DESC LIMIT 10");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }		 
		 public function insertone($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into game_reports_set2 (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."')");
		//echo $this->db->last_query(); exit;
			 
		 }	 
		 public function insertone_SK($userid,$cid,$sid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into sk_game_reports_set2 (gu_id,gc_id,gs_id,gp_id,g_id,total_question,attempt_question,answer,game_score,gtime,rtime,crtime,wrtime,lastupdate) values('".$userid."','".$cid."','".$sid."','".$pid."','".$gameid."','".$total_ques."','".$attempt_ques."','".$answer."','".$score."','".$a6."','".$a7."','".$a8."','".$a9."','".$lastup_date."')");
		//echo $this->db->last_query(); exit;
			 
		 }	
		 
		 public function insertlang($gameid,$userid,$userlang,$skillkit)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("INSERT INTO game_language_track( gameID, userID, languageID, skillkit, createddatetime) VALUES ('".$gameid."','".$userid."','".$userlang."','".$skillkit."',NOW())");
		//echo $this->db->last_query(); exit;
			 
		 }	
		 
		  public function getresultGameDetails($userid,$gameid)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("select AY.id as academicid, AY.startdate,AY.enddate,(select gs_id from games where gid='".$gameid."') as gameskillid from users UG,academic_year AY where AY.id=UG.academicyear and UG.id='".$userid."' limit 0,1");
		 
			return $query->result_array();
		 }	
		 
		  public function insertthree($userid,$gameid,$acid,$lastup_date,$st)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("insert into user_games (gu_id,played_game,last_update,date,status,academicyear) values('".$userid."','".$gameid."','".$lastup_date."','".$lastup_date."','".$st."','".$acid."')");
		 
		 }	
		 public function getacademicyear()
		 {
			 
		$query = $this->db->query("select startdate,enddate from academic_year order by id desc limit 1");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }		 
		 
		public function getacademicmonths($startdate,$enddate)
		 {
			 
		$query = $this->db->query("select DATE_FORMAT(m1, '%m') as monthNumber,DATE_FORMAT(m1, '%Y') as yearNumber,DATE_FORMAT(m1, '%b') as monthName from (select ('".$startdate."' - INTERVAL DAYOFMONTH('".$startdate."')-1 DAY) +INTERVAL m MONTH as m1 from (select @rownum:=@rownum+1 as m from(select 1 union select 2 union select 3 union select 4) t1,(select 1 union select 2 union select 3 union select 4) t2,(select 1 union select 2 union select 3 union select 4) t3,(select 1 union select 2 union select 3 union select 4) t4,(select @rownum:=-1) t0) d1) d2 where m1<='".$enddate."' order by m1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 
		 public function getskills()
		 {
			 
		$query = $this->db->query("select name,id from category_skills where category_id = 1 order by id");
		//echo $this->db->last_query(); 
			return $query->result_array();
		 }
		 
		 public function getcalendar($uid,$start_date,$last_date)
		 {
	
		$query = $this->db->query("SELECT id, date_format(lastupdate, '%d/%m/%Y') as created_date FROM game_reports_set2 WHERE gu_id = '".$uid."' and lastupdate between '".$start_date."' and '".$last_date."'  and gc_id = 1");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		 
		 public function getskpreport($userid,$skillsid,$month)
		 {
	
		$query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth FROM `game_reports_set2` WHERE gs_id in (".$skillsid.") and gu_id='".$userid."' and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and DATE_FORMAT(lastupdate, '%Y-%m')=\"".$month."\"   group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }
		 public function mybspicalendar($school_id,$uid,$dateQry)
		 {
			 $query = $this->db->query('select (sum(a.game_score)/5) as game_score, lastupdate,playedDate from
								(
									SELECT avg(gr.game_score) as game_score,count(*) as cnt, lastupdate,DATE_FORMAT(`lastupdate`,"%d") as playedDate FROM game_reports_set2 gr join category_skills sk join users u WHERE gr.gu_id = u.id and u.sid = '.$school_id.' and gr.gu_id='.$uid.' and sk.id = gr.gs_id and gr.gs_id in (SELECT id FROM category_skills where category_id=1) and DATE_FORMAT(lastupdate, "%Y-%m")=\''.$dateQry.'\' group by lastupdate, gr.gs_id, gr.gu_id order by gr.gs_id
								) a group by lastupdate');
								//echo $this->db->last_query(); exit;
								return $query->result_array();
								
		 }

		 public function mybspicalendarSkillChart($skillsid,$uid,$dateQry)
		 {
			 $query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth  FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id='".$uid."' and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and DATE_FORMAT(lastupdate, '%Y-%m')=\"".$dateQry."\"   group by gs_id ");
								//echo $this->db->last_query(); exit;
								return $query->result_array();
								
		 }
		   public function myTrophiesAll($userid,$startdate,$enddate)
		 {
			 $query = $this->db->query("select trophystar.gu_id AS gu_id,extract(month from trophystar.lastupdate) AS month,sum(trophystar.ct) as totstar,trophystar.id as category from trophystar where (trophystar.lastupdate>='".$startdate."' and trophystar.lastupdate<='".$enddate."') and  trophystar.gu_id='".$userid."' group by month,trophystar.id ");
								//echo $this->db->last_query(); exit;
								return $query->result_array();
								
		 }
		  
		 function getPlaysCountPrior($r)
		 {
			 $query = $this->db->query("select playedGamesCount as max_playedGamesCount from (SELECT  (select count(distinct(lastupdate)) from game_reports_set2 gr where gr.gs_id=g.gs_id and gr.gu_id='".$r['id']."' and gr.g_id=cpg.game_id and lastupdate between (select startdate from academic_year where id=19) and (select enddate from academic_year where id=19) ) as playedGamesCount FROM class_plan_game cpg join games g on g.gid=cpg.game_id WHERE cpg.class_id='".$r['grade_id']."' and cpg.plan_id='".$r['gp_id']."' and g.gs_id in (59,60,61,62,63)) as a1 order by playedGamesCount desc limit 1");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }		 
		 	function getPlayCounts($r,$max_playedGamesCount)
		 {
			 $query = $this->db->query("select (select IFNULL(max(SessionID),0) from sk_user_game_list where userID='".$r['id']."' and planID='".$r['gp_id']."') as maxSession, (select skill_count from sk_plan_skillcount where school_ID='".$r['sid']."' and plan_ID='".$r['gp_id']."') as skillCount, (select count(*) FROM class_plan_game cpg join games g on g.gid=cpg.game_id WHERE cpg.class_id='".$r['grade_id']."' and cpg.plan_id='".$r['gp_id']."' and g.gs_id in (59,60,61,62,63) ) as acualGamesCount,count(*) as playedGamesCount  from (SELECT game_id,gs_id,(select count(distinct(lastupdate)) from game_reports_set2 gr where gr.gs_id=g.gs_id and gr.gu_id='".$r['id']."' and gr.g_id=cpg.game_id and lastupdate between (select startdate from academic_year where id=19) and (select enddate from academic_year where id=19) ) as playedGamesCount FROM class_plan_game cpg join games g on g.gid=cpg.game_id WHERE cpg.class_id='".$r['grade_id']."' and cpg.plan_id='".$r['gp_id']."' and g.gs_id in (59,60,61,62,63)) as a1 where a1.playedGamesCount>=".$max_playedGamesCount."");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }		 
		 	function getSKBspi($r)
		 {
			 $query = $this->db->query("select avg(gamescore) as gamescore,gs_id from (select avg(gamescore) as gamescore ,playedMonth,gs_id from ( SELECT (AVG(`game_score`)) as gamescore ,gs_id , lastupdate,DATE_FORMAT(`lastupdate`,'%m') as playedMonth  FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id='".$r['id']."' and  lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gs_id , lastupdate) as a1 group by gs_id,playedMonth) as a2 group by gs_id order by gamescore asc");
			//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }	
		  public function updateSKGameList($r)
		 {
			 //echo 'hello'; exit;
		$query = $this->db->query("update sk_user_game_list set status=1 where userID='".$r['id']."' and planID='".$r['gp_id']."'");
		//echo $this->db->last_query(); exit;
			 
		 }	
		  public function insertSKGameList($r,$maxsession,$month_array_skill)
		 {
			 //echo 'hello'; exit;

		$query = $this->db->query("insert into sk_user_game_list(userID,planID,SessionID,weakSkills,status,created_date) values ('".$r['id']."','".$r['gp_id']."','".($maxsession)."','".implode (",", $month_array_skill)."',0,curdate())");
		//echo $this->db->last_query(); exit;
			 
		 }	
		 	 
		 
		 public function getbspireport1($userid,$mnths)
		 {
	
		$query = $this->db->query("SELECT (AVG(`game_score`)) as gamescore,gs_id , lastupdate FROM `game_reports_set2` WHERE gs_id in (59,60,61,62,63) and gu_id='".$userid."' and DATE_FORMAT(lastupdate,'%b-%Y') in (".$mnths.") group by gs_id , lastupdate");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }

		 	 
		 
		  
		public function getstudentplay($userid,$txtFDate,$txtTDate)
		 {
	
		$query = $this->db->query("SELECT count(*) as PalyCount,lastupdate,gs_id ,(select concat(fname,' ',lname) from users u where u.id=gu_id) as Name,(select username from users u where u.id=gu_id) as Username,(select concat((select classname from class c where c.id=u.grade_id),' - ',section) from users u where u.id=gu_id) as Class from game_reports_set2 where gu_id='".$userid ."' and  date(lastupdate) between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) and  date(lastupdate) between '".$txtFDate."' and '".$txtTDate."' group by date(lastupdate),gs_id order by date(lastupdate)");
		//echo $this->db->last_query(); exit;
			return $query->result_array();
		 }		
		 
		 public function getSetDetails($game_plan_id,$game_grade)
		 {
			
		$query = $this->db->query("SELECT gradeid_set2 as gradeid,plan_id_set2 as planid from assesment_class_set where plan_id='".$game_plan_id."'  and gradeid_set1='".$game_grade."'");
		//echo $this->db->last_query(); exit;
			return $query->result_array(); 
			 
		 }
}
