<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<title>Skillangels</title>
<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styleinner.css">
 <link href="<?php echo base_url(); ?>assets/fonts/Roboto.css" rel="stylesheet">
 <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">

	 <!-- font CSS -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url(); ?>assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/navbar-static-top.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>assets/js/ie-emulation-modes-warning.js"></script>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stylenew.css">

<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url(); ?>assets/js/star-rating.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<style>
.modal{z-index: 9999;}
.btnfdk{background: #2b13a5;border: 2px solid #fff;border-radius: 12px !important;color: #fff;}
</style>
</head>

<body>
 <!--
<nav class="navbar navbar-default navbar-fixed-top">
    	<div class="section_one">
		<div class="container">
		
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  
          <div class="logo"><a class="navbar-brand" href="#"><img src ="<?php echo base_url(); ?>assets/images/skillAngels-Logo.png"/></a></div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
         
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url(); ?>">Home </a></li> 
        <li><a href="#">The Brain Wave</a></li>
          <li><a href="#">Why skill Wave</a></li>
        <li><a href="#">50 must know facts</a></li>
        <li><a href="#">In news</a></li>
		 <li><a href="#">In Action</a></li>
          <li><a href="#">Research</a></li>
        <li><a href="#">Expert Talk</a></li>
       <?php if(!isset($this->session->user_id))
			{ ?>
            <li><a href="#" data-toggle="modal" data-target="#login-modal" class="loginLink"><?php echo $this->lang->line("login"); ?></a></li> <?php 
			} else { ?>
				
				<li><a href="<?php echo base_url('index.php/home/dashboard#View') ?>" class="loginLink">Dashboard</a></li>
			<?php } ?>
          </ul>
		  
        </div> 
	
  </div>
		
	</div> 

  </nav>  -->	

<!--<a href="#" data-toggle="modal" data-target="#login-modal">Login</a>-->

<div class="modal fade"  id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    	  <div class="modal-dialog">
		 
				<div class="loginmodal-container">
				 <button type="button" class="close" data-dismiss="modal"><img src="<?php echo base_url();?>/assets/images/close.png" /></button>
					<h1>Login to Your Account</h1><br>
					
					<form  class="cmxform form-inline" method="POST" id="form-login" accept-charset="utf-8" > <ul><li><div id="errormsg"></div></li><li><label for="email">Email or Username</label><input required="required" type="text" name="email" value="" class="input-small logTxt" id="email"></li><li><label for="pwd">Password</label><input required="required" type="password" name="pwd" value="" class="input-small logTxt" id="pwd"></li>
					<li style="display:none">
					<select style="height:28px" name="ddlLanguage" class="input-small logTxt ddlHLanguage" id="ddlHLanguage">
								<option value="1">English</option>
								</select>
					</li><li><input type="button" class="btn btn-primary logSubmit" id="submit" name="submit" value="Login"></li> </ul></form>
					
				 
				  <div class="login-help">
					<label><input type="checkbox" id="remember" name="remember"><span class="spanRememberme">Remember me</span></label>
				  </div>
				</div>
			</div>
		  </div>
		  <div class="clear_both"></div>
		  
		  
		  
<!--<div class="section_two" id="View">
<div class="container">
<div class="row">

  <div class="col-md-4 col-sm-4 col-xs-12 main_sec">
  <div class="sec_in_sec">
 
  <div class="sec_in_L">
  <ul>
  <li > <img src="<?php echo base_url(); ?>assets/images/avatar.png" alt="" /></li>
  <li class="pad_v1"><p>Good day <br><?php echo $this->session->fname; ?></p>	</li>
  </ul>

  </div>

  </div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-12">
  <div class="sec_in_sec">
  <div class="mid_sec">
  <p><?php echo $this->session->greetings_content; ?></p>
  </div>

  </div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="sec_in_sec sec_in1">
  <div class="mid_sec">
  <p>Your Overall BSPI as on<br>
<?php echo date('d-m-Y'); ?>  : <span class="block" id="bspiid"><?php echo round($bspi,2); ?></span></p>
  </div>


  </div>
  </div>
  </div>---><!--/row -->
  </div><!--/container -->
</div><!--/section_two -->
  
	<div class="clear_both"></div>
	<div class="section_three" id="header">
	
	<div class="container">
	<span style="float: left;"><a  href="index.php"><img src="<?php echo base_url(); ?>assets/images/logo.png" width="150" /></a></span>
	 <nav class="navbar navbar-default navbar-fixed-top">
	       <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" aria-controls="navbar1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  
        </div>
		
	      <div id="navbar1" class="navbar-collapse collapse">
       	
          <ul class="nav navbar-nav">
		   <?php //print_r($disablemenus); ?>
            <li <?php if($this->uri->segment(2)=="myprofile" || $this->uri->segment(2)=="profile"){echo 'class="secondActive"';}?> ><a href="<?php echo base_url("index.php/home/profile#View") ?>">Profile</a></li> 
       
		
		<?php $planidset2 = $this->session->set2planid;
			  $grdeidset2 = $this->session->set2gradeid;
				if($planidset2!=0)
				{
					//if($disablemenus[0]['countid']>=5)
						{
			  ?>
		<li <?php if($this->uri->segment(1)=="mypuzzleset2"){echo 'class="secondActive"';}?> ><a href="<?php echo base_url("index.php/mypuzzleset2/dashboard#View") ?>">Puzzles set1</a></li>
					<?php }
					
					
					
					} ?>
					
					<?php if($disablemenus2[0]['countid']>=5)
						{ ?>
				<li <?php if($this->uri->segment(1)=="mypuzzleset1"){echo 'class="secondActive"';}?> ><a href="<?php echo base_url("index.php/mypuzzleset1/dashboard#View") ?>">Puzzles</a></li>
				
						<?php } 
						else {?>
							<li <?php if($this->uri->segment(1)=="mypuzzleset1"){echo 'class="secondActive"';}?> ><a href="<?php echo base_url("index.php/mypuzzleset1/dashboard#View") ?>">Puzzles</a></li>
							
					<?php	} ?>
				<?php $planidset3 = $this->session->set3planid;
			  $grdeidset3 = $this->session->set3gradeid;
			 
				if($planidset3!=0)
				{
					if($disablemenus[0]['countid']>=5) {
			  ?>
		<li <?php if($this->uri->segment(1)=="mypuzzleset3"){echo 'class="secondActive"';}?> ><a href="<?php echo base_url("index.php/mypuzzleset3/dashboard#View") ?>">Puzzles set3</a></li>
					<?php } else { ?>
		<li <?php if($this->uri->segment(1)=="mypuzzleset3"){echo 'class="secondActive"';}?> ><a class="puzzleset3url" href="javascript:;" style="color: #FFF; background-color:transparent;" onmouseover='this.style.cursor="none"'>Puzzles set3</a></li>
					<?php } } ?>
					
					
			<?php 
			$gradeid_set3 = $this->session->set3gradeid;
			  
			if($gradeid_set3!=0) { ?>
			
        <?php	if($disablemenus[0]['countid']>=5 && $disablemenus3[0]['countid']>=5) { ?>
        <li <?php if($this->uri->segment(1)=="reports"){echo 'class="secondActive"';}?> > <a href="<?php echo base_url("index.php/reports/reportslist#View") ?>">Reports</a></li>
				<?php } 
				else { ?>
					
			<li <?php if($this->uri->segment(1)=="reports"){echo 'class="secondActive"';}?> > <a class="reporturl" href="javascript:;" style="color: #FFF; background-color:transparent;" onmouseover='this.style.cursor="none"'>Reports</a></li>		
			<?php } } 
			
			else {	?>
			
			<?php	if($disablemenus[0]['countid']>=5) { ?>
        <li <?php if($this->uri->segment(1)=="reports"){echo 'class="secondActive"';}?> > <a href="<?php echo base_url("index.php/reports/reportslist#View") ?>">Reports</a></li>
				<?php } else { ?>
			
			<li <?php if($this->uri->segment(1)=="reports"){echo 'class="secondActive"';}?> > <a class="reporturl" href="javascript:;" style="color: #FFF; background-color:transparent;" onmouseover='this.style.cursor="none"'>Reports</a></li>	
			
			<?php } } ?>
			
        <?php if(!isset($this->session->user_id))
			{ ?>
             <?php 
			} else { ?>
				
								<li><a href="javascript:;" class="loginLink">Logout</a></li>

			<?php } ?>
        
          </ul>
		  
        </div><!--/.nav-collapse -->
		</nav>
	</div><!--/container -->
	</div><!--/section_three -->
<style>svg{    background-color:rgba(0, 0, 0, 0) !important; }  body{min-height:0 !important;}

.set2menu {
	
	position: relative;
    display: block;
    padding: 22px 30px;
    font-size: 22px;
    font-family: 'Phenomena-Regular';
    color: #FFF;
    letter-spacing: 1px;
}

.set2menu:hover {
	text-decoration: none;
	color: #000;
	cursor: pointer;
	
	textarea {
    resize: none;
}
}
.fancybox-overlay{z-index: 99999 !important;}
.fancybox-opened{z-index: 999999 !important;}
</style>

<div class="container">
 
  <!-- Trigger the modal with a button -->
  
 
  <!-- Modal -->
  <div class="modal fade" data-easein="bounceIn" id="feedbackmodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body"  style="border: 10px solid rgba(0, 0, 0, 0.41);" >
	
			<h2 class="modal-title" style="text-align:center;padding-bottom:0"> Feedback</h2>
			<div style="text-align:center">
          <p class="fdbkcontent">Take a moment to let us know your experience</p>
			  <form id="frmfeedback" method='POST'>
       
        <input id="starfdbk" value="" type="text" data-min=0 data-max=5 data-step=1 data-size="md" title="Stars">
		<br/>
       <textarea name="usercmnt" id="usercmnt" placeholder="Your comments here...." style="width:77%; height:110px;border:1px solid #ccc;"></textarea>
        
        <div class="form-group" style="margin-top:10px">
           
           
        </div>
    </form>
	</div>
				 <div style="text-align:right;"><button type="button" class="btn btn-success blink_me1 btnfdk" id="fbksubmit" data-dismiss="modal">Submit Feedback</button></div>
        </div>
         
      </div>
	  
	 
      
    </div>
  </div>
  
   
 
  
</div>
	
	
	<script>

$(document).ready(function()
{
	 $("#starfdbk").rating({
                 showClear: false,
				 showCaption: true,
				 min: 0, max: 5, step:1, size: "xl", stars: "5",
				 
				 step: 1,
        starCaptions: {1: 'Poor', 2: 'Neutral', 3: 'Good', 4: 'Very Good', 5: 'Awesome'},
				
            });
			
			
});

var userid = '<?php echo $this->session->user_id; ?>';
var currentdate = '<?php echo date('Y-m-d'); ?>';

$('.loginLink').click(function()
{
 $.ajax({
    type: "POST",
    url: "<?php echo base_url()."index.php/home/userfeedback"; ?>",
    data: {userid:userid,currentdate:currentdate},
    success: function(result){
		 
		 if(result==1)
		 {
			window.location.href = "<?php echo base_url('index.php/home/logout') ?>";
		 }
		 else{
			 
			$('#feedbackmodal').modal('show');
		 }
    }
});
});

$('#fbksubmit').click(function()
{
	var stars = $('#starfdbk').val();
	var userid = '<?php echo $this->session->user_id; ?>';
	var currentdate = '<?php echo date('Y-m-d'); ?>';
	var usercmnt = $('#usercmnt').val();
	
	$.ajax({
    type: "POST",
    url: "<?php echo base_url()."index.php/home/userfeedbackinsert"; ?>",
    data: {userid:userid,currentdate:currentdate,stars:stars,usercmnt:usercmnt},
    success: function(result){	 
		 //alert(result);
		 
		  if(result==1)
		 {
			window.location.href = "<?php echo base_url('index.php/home/logout') ?>";
		 }
    }
});
	
});


</script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/menu/jquery.sticky.js"></script>
<script>
$(window).load(function(){
      $("#header").sticky({ topSpacing: 0 });
	  $('.btnactive').sparkleHover(); 
});
</script>-->		 
<script src="<?php echo base_url(); ?>assets/js/hover/sparkleHover.js"></script>
<!-- ********** Checking Login User ********** -->

