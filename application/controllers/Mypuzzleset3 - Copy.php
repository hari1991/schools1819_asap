<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mypuzzleset3 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Assessment_model_set3');
				$this->load->library('session');			
				//$this->lang->load("menu_lang","french");		
			
        }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	
	public function userlogin()
	{
		//print_r($_POST); 
		
		if (!empty($_POST))
		{
			 
			$data['query'] = $this->Assessment_model_set3->checkUser($this->input->post('email'),$this->input->post('pwd'), $this->input->post('ddlLanguage'));
			
			 
			if(isset($data['query'][0]->id)){
				// update prelogin and login date
				// get login count
				// update login count by 1
				// based on login count set welcome message
				//
				$r=$array = json_decode(json_encode($data['query'][0]), True);
			$r_getpalyCounts_prior = $this->Assessment_model_set3->getPlaysCountPrior($r);
			$r_getpalyCounts = $this->Assessment_model_set3->getPlayCounts($r,$r_getpalyCounts_prior[0]['max_playedGamesCount']);
			if($r_getpalyCounts[0]['playedGamesCount']>=$r_getpalyCounts[0]['acualGamesCount'])
			{
			$SKBSPI = $this->Assessment_model_set3->getSKBspi($r);
			 
			$month_array_skill=array();
$inini=0;$ininj=1;$inink=0;
foreach($SKBSPI as $get_res){
	$inink++;
	if($ininj<=$r_getpalyCounts[0]['skillCount'] && $inink<=$r_getpalyCounts[0]['skillCount'])
	{
if($inini!=$get_res['gamescore']){$ininj++;}
$month_array_skill[]=$get_res['gs_id'];
$inini=$get_res['gamescore'];
	}
	
	}
	if($r_getpalyCounts[0]['maxSession']==0){$maxsession=$r_getpalyCounts_prior[0]['max_playedGamesCount'];} else{$maxsession=$r_getpalyCounts[0]['maxSession']+1;}
		
		//echo $r_getpalyCounts_prior['max_playedGamesCount']."==".$maxsession; exit;
if($r_getpalyCounts_prior[0]['max_playedGamesCount']==$maxsession)
{
	$this->Assessment_model_set3->updateSKGameList($r);
	$this->Assessment_model_set3->insertSKGameList($r,$maxsession,$month_array_skill);
}

			}
				$this->Assessment_model_set3->update_loginDetails($data['query'][0]->id);
				
				$login_count=$data['query'][0]->login_count;
				$prelogin_date=$data['query'][0]->pre_logindate;
				$fname=$data['query'][0]->fname;
				 $todaydate=date('Y-m-d');
 $login_date=$data['query'][0]->login_date;
 $start_ts = strtotime($login_date);
 $end_ts = strtotime($todaydate);
 $diff = $end_ts - $start_ts;
 $datediff = round($diff / 86400);
 
				if((isset($login_count) && $login_count<=1) || $prelogin_date == "0000-00-00")
{
$greetings_content = 'Thanks for subscribing to SkillAngels. I am sure you will enjoy the learning process.<br>
Good Luck
';
}

elseif((isset($login_count) && $login_count>1) && $datediff <3 )
{
$greetings_content ='You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.
<br> Nice to see you pay so much attention';
}

elseif((isset($login_count) && $login_count>1) && ($datediff >= 3 && $datediff < 7 ))
{
$greetings_content =' You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.</br>
You were missing for the last '.$datediff.'</br>
You missed '.$datediff.' training </br>
Everything Okay?';
}

elseif((isset($login_count) && $login_count>1) && ($datediff >= 7 && $datediff < 14 ))
{
$greetings_content ='You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.
<br>You were missing for the last '.$datediff.' 
<br>Please make it a habit to use SkillAngels regularly';
}
elseif((isset($login_count) && $login_count>1) && ($datediff > 30))
{
$greetings_content ='You last played on '.date("d-m-Y", strtotime($prelogin_date)).'.
<br>You were missing for the last '.$datediff.' 
<br>That seems to be a long break. Please resume your training
<br>Good Luck
';
}

			$this->session->set_userdata(array(
                            'userlang'       => 1,
                            'gp_id'       => $data['query'][0]->gp_id,
                            'id'       => $data['query'][0]->id,
                            'user_id'       => $data['query'][0]->id,
                            'fname'      => $data['query'][0]->fname,
                            'username'      => $data['query'][0]->username,
                            'login_count'      => $data['query'][0]->login_count,
                            'ltime'      => time(),
                            'fullname'      => $data['query'][0]->fname.' '.$data['query'][0]->lname,
                            'game_plan_id'      => $data['query'][0]->gp_id,
                            'game_grade'      => $data['query'][0]->grade_id,
                            'school_id'      => $data['query'][0]->sid,
                            'section'      => $data['query'][0]->section,
                            'greetings_content'      => $greetings_content
							
							
                    ));
					
					
		$language = $data['query'][0]->languagekey;
		$language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
					
			 echo	$result=1; exit;
		//redirect('index.php/home/dashboard');
		
		
			
		}
		}
	}
	
	public function dashboard()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		$data['randomGames']=$this->Assessment_model_set3->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		
		$data['assignGames']=$this->Assessment_model_set3->getAssignGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid);
	
		
		$cur_day_skills = count($data['randomGames']);
		$assign_count = count($data['assignGames']);
		if($cur_day_skills <= 0 || $assign_count > $cur_day_skills ) {
				$this->fn_Rand_games($userid, $check_date_time, $cur_day_skills, $assign_count,$catid);
			}
		$data['randomGames']=$this->Assessment_model_set3->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$where = "";
		foreach($data['randomGames'] as $brainData )
		{
			$brainIds[] = $brainData['gid'];
			$active_game_ids = @implode(',',$brainIds);
			$where = " and g.gid in ($active_game_ids)";
		}
			 
		$data['actualGames'] = $this->Assessment_model_set3->getActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
 
	 $restrainCalendar= $this->Assessment_model_set3->getTrainCalendar($userid,date('Y-m-01'),date('Y-m-d'),$catid);
	 $data['arrtrainCalendar']=explode(',',$restrainCalendar[0]['updateDates']);
	 $data['trophies']=array();
	 $data['trophies']= $this->Assessment_model_set3->getMyCurrentTrophies($userid);

	 $data['ajaxurl'] = "ajax_set3";
	 $data['controllername'] = "mypuzzleset3";
	/* echo "<pre>";
	 print_r($data['arrtrainCalendar'] );
	 exit;*/
		//$data['query'] = $this->Assessment_model_set3->getleftbardata($userid);
		
		//$data['mytrophy'] = $this->Assessment_model_set3->gettrophy($userid);
		//$this->in_array_r();
		 
		 
	
		//print_r($data['mytrophy']);  
		$this->load->view('headerinner', $data);
        $this->load->view('home/dashboard', $data);
		$this->load->view('footerinner');
	
	//redirect('index.php/home/dashboard');
	
	}
	
public function fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid) {
//echo "fn_Rand_games";

$arrSkills=$this->Assessment_model_set3->getSkillsRandom($catid);
	
	  foreach($arrSkills as $gs_data)
	  {
		  $rand_sel = $this->Assessment_model_set3->assignRandomGame($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
		   
		  $rand_count = count($rand_sel);	
		  if($rand_count <=0) {
					$del_where = "";
					if($assign_count <> $cur_day_skills && $cur_day_skills > 0)
						$del_where = " and created_date = '$check_date_time'";
					 $this->Assessment_model_set3->deleteRandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$del_where);
					 
				} 
				if($rand_count > 0) {
					$rand_data = $this->Assessment_model_set3->insertRandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$gs_data['skill_id'],$this->session->school_id,$this->session->section,$rand_sel[0]['gid'],$check_date_time);
					 
				}
	  }
	  $data['randomGames']=$this->Assessment_model_set3->getRandomGames($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$this->session->school_id);
		$cur_day_skills = count($data['randomGames']);
		if($cur_day_skills == 0)
				$this->fn_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count);
		 
			
	 
}
	
	public function language()
	{
	
	 $email = $this->input->post('Lemail'); 
		
		$data['query'] = $this->Assessment_model_set3->languagechange($email);
		
		//print_r($data['query']);
		$str_language='';
		foreach($data['query'] as $data)
		{
			
			$str_language.='<option value="'.$data->ID.'">'.$data->name.'</option>';
	
		}	

	
	 if($str_language==''){$str_language='<option value="1">English</option>';}
		echo $str_language;exit;

	}
	
	public function logout()
	{
 if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
    $this->session->sess_destroy();
   redirect(base_url());
	}
	public function gamesajax()
	{
		$this->session->set_userdata(array( 'currentgameid'=> $_REQUEST['gameid'] ));
		$this->session->set_userdata(array( 'isskillkit'=> $_REQUEST['skillkit'] ));
		echo $this->session->currentgameid;exit;
	}
	public function result()
	{
			if($this->session->user_id=="" || !isset($this->session->user_id)){exit;}
	
		$total_ques=$_REQUEST["tqcnt1"];
$attempt_ques=$_REQUEST["aqcnt1"];
$answer=$_REQUEST["cqcnt1"];
$score=$_REQUEST["gscore1"];
$a6=$_REQUEST["gtime1"];
$a7=$_REQUEST["rtime1"];
$a8=$_REQUEST["crtime1"];
$a9=$_REQUEST["wrtime1"];	
$gameid=$this->session->currentgameid;
$isskillkit=$this->session->isskillkit;
if($isskillkit=="Y"){$skillkit=1;}
else{$skillkit=0;}
/*
echo "<pre>";
print_r($_SESSION);
exit;

		/*echo 'hai'; exit;*/
		
		$userlang = $this->session->userlang;
		$userid = $this->session->user_id; 
		$lastup_date = date("Y-m-d");
		$cid = 1;
				$data['gameDetails'] = $this->Assessment_model_set3->getresultGameDetails($userid,$gameid);

		$skillid =$data['gameDetails'][0]['gameskillid'] ; 
		  
		//$createddate = NOW();
		 
		$pid =  $this->session->gp_id; 
		if($skillkit==0){
		$data['insert1'] = $this->Assessment_model_set3->insertone($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		else if($skillkit==1){
		$data['insert1'] = $this->Assessment_model_set3->insertone_SK($userid,$cid,$skillid,$pid,$gameid,$total_ques,$attempt_ques,$answer,$score,$a6,$a7,$a8,$a9,$lastup_date);
		}
		$data['insert2'] = $this->Assessment_model_set3->insertlang($gameid,$userid,$userlang,$skillkit);
 		
		$acid = $data['gameDetails'][0]['academicid'];
		$st = 1;
		$data['insert3'] = $this->Assessment_model_set3->insertthree($userid,$gameid,$acid,$lastup_date,$st);
		
		 
		$this->load->view('gameresult/Result');
		
	}
	
	public function myprofile()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$data['updatemsG']="";
		if($_REQUEST)
		{
		$sfname =  $this->input->post('sfname');
	$gender = $this->input->post('gender');
	$dob = $this->input->post('dob');
	$fathername = $this->input->post('fathername');
	$mothename = $this->input->post('mname');
	$address = $this->input->post('saddress');
	$emailid = $_POST['emailid'];
	$sphoneno = $this->input->post('sphoneno');
	$id = $this->input->post('id');
	$newpass = $this->input->post('newpass');
	$confirmpass = $this->input->post('cpass');
		
		
		$userid = $this->session->user_id;
		$data['updateprofile'] = $this->Assessment_model_set3->updateprofile($sfname,$gender,$dob,$fathername,$mothename,$address,$sphoneno,$id,$newpass,$confirmpass);
		$data['updatemsG']="Updated Successfully !!!";
		}
		
		
		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
		$userid = $this->session->user_id;
		//$data['query'] = $this->Assessment_model_set3->getleftbardata($userid);
		//$data['mytrophy'] = $this->Assessment_model_set3->gettrophy($userid);
		$data['myprofile'] = $this->Assessment_model_set3->getmyprofile($userid);
		$planid = $data['myprofile'][0]['gp_id'];
		$schoolid = $data['myprofile'][0]['sid'];
		$gradeid = $data['myprofile'][0]['grade_id']; 
		$data['getplandetails'] = $this->Assessment_model_set3->getplandetais($planid);
		$data['getgrade'] = $this->Assessment_model_set3->getgradedetais($gradeid);
		
		$this->load->view('headerinner', $data);
        $this->load->view('myprofile/myprofile', $data);
		$this->load->view('footerinner');
	
	}
	public function mybrainprofile()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
	
	$data['academicyear'] = $this->Assessment_model_set3->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model_set3->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model_set3->getskills();	
	
	
		$this->load->view('headerinner', $data);
        $this->load->view('mybrainprofile/mybrainprofile', $data);
		$this->load->view('footerinner');
		
	}
	public function ajaxcalendar()
	{
				 

		$yearMonthQry=$_REQUEST['yearMonth'];
		$userid = $this->session->user_id;
		
	
	$data['academicyear'] = $this->Assessment_model_set3->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model_set3->getacademicmonths($startdate,$enddate);
	$data['skills'] = $this->Assessment_model_set3->getskills();	
	$bspicalendardays= $this->Assessment_model_set3->mybspicalendar($this->session->school_id,$userid,$yearMonthQry);
	$mybspiCalendar=array();	
	foreach($bspicalendardays as $days)
	{
		$mybspiCalendar[$days['playedDate']]=$days['game_score'];
	}
	 $data['mybspiCalendar']=$mybspiCalendar;
	  
	$data['yearMonthQry']=$yearMonthQry;
	
		 
        $this->load->view('mybrainprofile/ajaxcalendar', $data);
 
		
	}
	public function ajaxcalendarSkillChart()
	{
				 

		$yearMonthQry=$_REQUEST['yearMonth'];
		$userid = $this->session->user_id;
		
 
	$bspicalendarskillScore= $this->Assessment_model_set3->mybspicalendarSkillChart("",$userid,$yearMonthQry);
  
	 $mybspiCalendarSkillScore=array("SID59"=>0,"SID60"=>0,"SID61"=>0,"SID62"=>0,"SID63"=>0);
	 foreach($bspicalendarskillScore as $score)
	{
		$mybspiCalendarSkillScore["SID".$score['gs_id']]=round($score['gamescore'],2);
	}
	 $data['bspicalendarskillScore']=$mybspiCalendarSkillScore;
	  
	$data['yearMonthQry']=$yearMonthQry;
	
		 
        $this->load->view('mybrainprofile/ajaxcalendarSkillChart', $data);
 
		
	}
	 
	 public function mytrophies()
	{
				if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}

		$userid = $this->session->user_id;
		$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
	$data['academicyear'] = $this->Assessment_model_set3->getacademicyear(); 
	$startdate = $data['academicyear'][0]['startdate'];
	$enddate = $data['academicyear'][0]['enddate'];
	$data['academicmonths'] = $this->Assessment_model_set3->getacademicmonths($startdate,$enddate);
	$mytrophies= $this->Assessment_model_set3->myTrophiesAll($userid,$startdate,$enddate);
	$arrmyTrophies=array();
	 foreach($mytrophies as $trophies)
	 {
		 $arrmyTrophies[str_pad($trophies['month'], 2, '0', STR_PAD_LEFT)][$trophies['category']]=$trophies['totstar'];
	 }
	 $data['arrmyTrophies'] =$arrmyTrophies;
		$this->load->view('headerinner', $data);
        $this->load->view('mytrophies/mytrophies', $data);
		$this->load->view('footerinner');
		
	}
	
	public function skillkit()
	{
		if($this->session->user_id=="" || !isset($this->session->user_id)){redirect('index.php');}
		
	//echo "hello"; exit;	
		$userid = $this->session->user_id;
		$check_date_time = date('Y-m-d');
		$catid=1;
		 $this->fn_SK_Rand_games($userid, $check_date_time, '',100,$catid);
		$brain_rs = $this->Assessment_model_set3->getAssignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$userid,$this->session->school_id);
		$where = "";
		$brainIds='';
		$data['actualGames']='';
			foreach( $brain_rs as $brainData)
			{
					$brainIds[] = $brainData['gid'];
			}
			if($brainIds)
			{			
					$active_game_ids = @implode(',',$brainIds);
					$where = " and g.ID in ($active_game_ids)";

			}
	if($brainIds)
			{				
			$data['actualGames'] = $this->Assessment_model_set3->getSK_ActualGames($this->session->game_plan_id,$this->session->game_grade,$userid,$catid,$where);
			}
		$data['actualGameCategory']=array('59'=>'Memory','60'=>'Visual Processing','61'=>'Focus and Attention','62'=>'Problem Solving','63'=>'Linguistics');
		$get_bspi_rows =$this->Assessment_model_set3->getBSPI($userid);

		 
$res_tot_memory=$res_tot_vp=$res_tot_fa= $res_tot_ps=$res_tot_lang=0;
$res_tot_memory_i=$res_tot_vp_i=$res_tot_fa_i= $res_tot_ps_i=$res_tot_lang_i=0;
 
	foreach($get_bspi_rows as $get_res){
		
		if(($get_res['gs_id']=='59')){$res_tot_memory_i++; $res_tot_memory += $get_res['score'];}
		else{$res_tot_memory += 0.00;}
		if(($get_res['gs_id']=='60')){$res_tot_vp_i++;$res_tot_vp += $get_res['score'];}
		else{$res_tot_vp += 0.00;}
		if(($get_res['gs_id']=='61')){$res_tot_fa_i++;$res_tot_fa += $get_res['score'];}
		else{$res_tot_fa += 0.00;}
		if(($get_res['gs_id']=='62')){$res_tot_ps_i++;$res_tot_ps += $get_res['score'];}
		else{$res_tot_ps += 0.00;}
		if(($get_res['gs_id']=='63')){$res_tot_lang_i++;$res_tot_lang += $get_res['score'];}
		else{$res_tot_lang += 0.00;}
		 
	}
	if($res_tot_memory_i==0){$res_tot_memory_i=1;}
	if($res_tot_vp_i==0){$res_tot_vp_i=1;}
	if($res_tot_fa_i==0){$res_tot_fa_i=1;}
	if($res_tot_ps_i==0){$res_tot_ps_i=1;}
	if($res_tot_lang_i==0){$res_tot_lang_i=1;}
	$tot = (($res_tot_memory/$res_tot_memory_i)+($res_tot_vp/$res_tot_vp_i)+($res_tot_fa/$res_tot_fa_i)+($res_tot_ps/$res_tot_ps_i)+($res_tot_lang/$res_tot_lang_i))/5;

	
	$data['bspi'] = $tot;
  
		$this->load->view('headerinner', $data);
        $this->load->view('home/skillkit', $data);
		$this->load->view('footerinner');
	
	//redirect('index.php/home/dashboard');
	
	}
	public function fn_SK_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,$catid) {
//echo "fn_Rand_games";

$arrSkills=$this->Assessment_model_set3->getSK_SkillsRandom($uid);
 
	  foreach($arrSkills as $gs_data)
	  {
		  $rand_sel = $this->Assessment_model_set3->assignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
		  
		  $cur_day_skills = count($rand_sel);	
		   
			  		  $rand_sel_limit = $this->Assessment_model_set3->assignSK_assignGameCount($this->session->game_plan_id,$gs_data['skill_id'],$this->session->school_id);
$assign_count = $rand_sel_limit[0]['gameCount']; 


if($cur_day_skills<$assign_count){
	$rand_sel_rs=$this->Assessment_model_set3->getSK_randomGames($this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$assign_count,$cur_day_skills);
 
 

$rand_count = count($rand_sel_rs);	

if($rand_count <=0) {
					$del_where = "";
					if($assign_count <> $cur_day_skills && $cur_day_skills > 0)
						$del_where = " and created_date = '$check_date_time'";
					 $this->Assessment_model_set3->deleteSK_RandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$del_where);
} 
				 
				if($rand_count > 0) {
					foreach($rand_sel_rs as $rand_data1) { 
					$rand_data = $this->Assessment_model_set3->insertSK_RandomGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$this->session->section,$rand_data1['gid'],$check_date_time);
					}
					 
				}
	  }
	  
	 		  $rand_sel = $this->Assessment_model_set3->assignSK_RandomGame($check_date_time,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id);
  $cur_day_skills = count($rand_sel);	
  if($cur_day_skills <$assign_count){
	  
	   $min_date_arr = $this->Assessment_model_set3->getSK_mindatePlay($this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id ,$catid);
		$this->Assessment_model_set3->deleteSK_OldGames($catid,$this->session->game_plan_id,$this->session->game_grade,$uid,$gs_data['skill_id'],$this->session->school_id,$min_date_arr[0]['mindate']);
			
				
								$this->fn_SK_Rand_games($uid, $check_date_time, $cur_day_skills, $assign_count,1);
				
			}
	  
	  
	  }
	   
			
	 
}
}
