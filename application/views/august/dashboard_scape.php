<link href = "<?php echo base_url(); ?>assets/css/jquery-ui.css" rel = "stylesheet">
<script src = "//code.jquery.com/jquery-1.10.2.js"></script>
<script src = "//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
#tabs-1{font-size: 14px;}
.ui-widget-header {
background:#b9cd6d;
border: 1px solid #b9cd6d;
color: #FFFFFF;
font-weight: bold;
}
.error { color: red;}
#srchreport .btn-success{margin-top: 24px;}
</style>	
<script src="<?php echo base_url(); ?>assets/js/counter.js" type="text/javascript" charset="utf-8"></script>
<script src = "<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
 <header class="head">
 <!-- /.search-bar -->
<div class="main-bar">
<h3><i class="fa fa-dashboard"></i>&nbsp;Dashboard</h3>
</div>
<!-- /.main-bar -->
</header>
</div>


<div id="content">
<div class="outer">
<div class="inner bg-light lter">		
<div class="row">
<div class="col-lg-12">

<div class="box">
<header>
<div class="icons"><i class="fa fa-table"></i></div>
<h5>Dashboard</h5>
</header>
<br/>

<!--<div class="row">
	<div class="col-lg-12">

		<div class="col-sm-4">
			<div class="noofstu">Total Users : <?php echo $Registered[0]['RegisteredCount'];?></div>
		</div>
		<div class="col-sm-4">
			<div class="noofstuta">User Taken Assessment : <?php echo $Taken[0]['AssessmentTaken'];?></div>
		</div>
		<div class="col-sm-4">
			<div class="noofstutfa">User Completed Assessment : <?php echo $Fully[0]['AssessmentFullyTaken'];?></div>
		</div>
	</div>
</div> -->
<br/>
<form id="srchreport" method="POST">
<div class="row">
<div class="col-sm-3">
<label>School</label>
<select  class="form-control input-sm" name="schoolid" id="schoolid">
<option value="">Select</option>
<?php foreach($schools as $sc) { ?>
<option value="<?php echo $sc['id']; ?>"><?php echo $sc['school_name']; ?></option>
<?php } ?>
</select>
</div>
<div class="col-sm-3" style="display:none";>
<label>Schools</label>
<select  class="form-control input-sm" name="sklid" id="sklid">
<option value="">Select</option>
<?php foreach($skls as $skl) { ?>
<option value="<?php echo $skl['sklname']; ?>"><?php echo $skl['sklname']; ?></option>
<?php } ?>
</select>
</div>
<div class="col-sm-3">
<input type="button" name="btnsearch" class="btn btn-success" value="Search" id="btnsearch" >
</div>

</div>
</form><br/>
<div id="tbldatapart1">
	

	
</div>
</div>
</div>
</div>
</div>
</div>
<style>
.noofstu{padding: 20px 30px;background: #98cb51;color: #fff;}
.noofstuta{padding: 20px 30px;background: #9c3c9a;color: #fff;}
.noofstutfa{padding: 20px 30px;background: #f27b88;color: #fff;}
</style>
<script>
$(document).ready(function(){
	$("#srchreport").validate({
		rules: {
			schoolid: {required: true} 
		   },
		   messages: {
			schoolid: {required: "Please select school"}		
		},errorElement: 'span',
		errorPlacement: function(error, element) {
		 error.insertAfter(element)
		},
	});	
	$("#btnsearch").click(function(){
		if($("#srchreport").valid()==true)
		{GetCountReport();}
	});
});
function GetCountReport()
{
	var schoolid = 	$('#schoolid').val();
	var school_name=$("#sklid").val();
	$.ajax({
		type: "GET",
		url: "<?php echo base_url(); ?>index.php/scape/GetCountReport",
		data: {schoolid:schoolid,school_name:school_name},		
		success: function(result){
			//alert(result);	 
			 $('#tbldatapart1').html(result);
		}
	});	
}
</script>
	