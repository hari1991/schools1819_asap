<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styleinner.css">
 <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
<link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="" style="">
 <div class="">



<div class="">
  
<div class="clearfix"></div>
  
  
 <script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/highcharts-3d.js"></script>
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>
 
<script src="<?php echo base_url(); ?>assets/js/highcharts-more.js"></script>
 <script type="text/javascript">

function bubblechart1(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		
		  events: {
            load: function(){
				       // this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  

				//this.tooltip.refresh();
				this.tooltip.refresh(this.series[0].data[<?php echo array_search($cus1['bspi'],$su1); ?>]);
 				// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
         
		gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false,style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
}
    },

    yAxis: {
		labels: {
            style: {
                fontSize: '20px',
				color: '#fff',
				fontFamily: 'Phenomena-Regular'
            }
        },
		title: {text: 'Score',style: {fontSize: '25px',color: '#ccc',fontFamily: 'Phenomena-Regular'}},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
	useHTML: true,
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;" ><img img width="40px" height="40px" src="'+this.point.myData+'" /><br/><b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b></div>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '';
            }
        }
    }
},
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport1 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",myData : '".base_url().$myavatarimage."',marker:{symbol: 'url(".base_url()."assets/images/starsmall.png)' } },";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).", myData : '".base_url().$myavatarimage."',marker:{ symbol: 'url(".base_url()."assets/images/starani2.gif)',radius:5} },";
			}
		}
		?>
            
        ] 
    }]

});
	
}
</script>
 <script type="text/javascript">
 function bubblechart2(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
        
		  events: {
            load: function(){
				//this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  
				//this.tooltip.refresh();
				 this.tooltip.refresh(this.series[0].data[<?php echo array_search($cus2['bspi'],$su2); ?>]);
 				 //this.tooltip.refresh(this.series[0].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false,style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
}
    },

    yAxis: {
		labels: {
            style: {
                fontSize: '20px',
				color: '#fff',
				fontFamily: 'Phenomena-Regular'
            }
        },
		title: {text: 'Score',style: {fontSize: '25px',color: '#ccc',fontFamily: 'Phenomena-Regular'}},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
	useHTML: true,
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;" ><img img width="40px" height="40px" src="'+this.point.myData+'" /><br/><b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b></div>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '';
            }
        }
    }
}, 
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport2 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",myData : '".base_url().$myavatarimage."',marker:{symbol: 'url(".base_url()."assets/images/starsmall.png)' } },";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).", myData : '".base_url().$myavatarimage."',marker:{ symbol: 'url(".base_url()."assets/images/starani2.gif)',radius:5} },";
			}
		}
		?>
            
        ] 
    } ]

});
	
}
</script>
 <script type="text/javascript">
function bubblechart3(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		  events: {
            load: function(){
				//this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  
				//this.tooltip.refresh();
					this.tooltip.refresh(this.series[0].data[<?php echo array_search($cus3['bspi'],$su3); ?>]); 				
					// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false,style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
}
    },

    yAxis: {
		labels: {
            style: {
                fontSize: '20px',
				color: '#fff',
				fontFamily: 'Phenomena-Regular'
            }
        },
		title: {text: 'Score',style: {fontSize: '25px',color: '#ccc',fontFamily: 'Phenomena-Regular'}},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
	useHTML: true,
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;" ><img img width="40px" height="40px" src="'+this.point.myData+'" /><br/><b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b></div>';
    }
},
 
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '';
            }
        }
    }
},	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport3 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",myData : '".base_url().$myavatarimage."',marker:{symbol: 'url(".base_url()."assets/images/starsmall.png)' } },";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).", myData : '".base_url().$myavatarimage."',marker:{ symbol: 'url(".base_url()."assets/images/starani2.gif)',radius:5} },";
			}
		}
		?>
            
        ]
         
    } ]

});
	
}

</script>
 <script type="text/javascript">
function bubblechart_overall(renderelement)
{
	
	var chart = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		  events: {
            load: function(){
				//this.renderer.image('http://schools.skillangels.com/images/bg.jpg').add();  
				//this.tooltip.refresh();
				this.tooltip.refresh(this.series[0].data[<?php echo array_search($cus_overall['bspi'],$su_overall); ?>]);
				// this.tooltip.refresh(this.series[1].data[0]);
			 
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
        gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false,style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
}
    },

    yAxis: {
		labels: {
            style: {
                fontSize: '20px',
				color: '#fff',
				fontFamily: 'Phenomena-Regular'
            }
        },
		title: {text: 'Score',style: {fontSize: '25px',color: '#ccc',fontFamily: 'Phenomena-Regular'}},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
	useHTML: true,
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;" ><img img width="40px" height="40px" src="'+this.point.myData+'" /><br/><b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b></div>';
    }
},
 
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '';
            }
        }
    }
},	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport_overall as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",myData : '".base_url().$myavatarimage."',marker:{symbol: 'url(".base_url()."assets/images/starsmall.png)' } },";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).", myData : '".base_url().$myavatarimage."',marker:{ symbol: 'url(".base_url()."assets/images/starani2.gif)',radius:5} },";
			}
		}
		?>
            
        ]
         
    } ]

});
	
}
</script>
 <script type="text/javascript">

$(document).ready(function(){
	bubblechart1('container1'); 
<?php if(isset($GradeWiseReport2) && $this->session->set2planid!=0){ ?>
	bubblechart2('container2');
<?php } ?>
<?php 	if(isset($GradeWiseReport3) && $this->session->set3planid!=0){ ?>
	bubblechart3('container3'); 
<?php } ?>
 bubblechart_overall('container_overall'); 
		});

</script>

<div class="row">

 <?php if($this->session->set2planid!=0){ ?>
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID2">Puzzle set 1 - Rank : <?php echo array_search($cus2['bspi'],$su2_r)+1; ?></h2>
 <br/>
 <div id="container2"  style="padding-top:20px;border:1px solid #008fa9;background:url('<?php echo base_url(); ?>assets/images/chart_patterns/bcp1.png')">
</div>
 </div>
 <?php } ?> 
  <div class="col-12 col-md-12 col-sm-12 col-xs-12">
  <div><h2 id="MonthID1"><span class="col-lg-10">Puzzle set 2 - Rank : <?php echo array_search($cus1['bspi'],$su1_r)+1; ?></span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>

  
 
 
 <div id="container1" style="padding-top:20px;border:1px solid #c5811a;background:url('<?php echo base_url(); ?>assets/images/chart_patterns/acp1.png')">
</div>
 </div> 
 
  <?php if($this->session->set3planid!=0){ ?>
  <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID3">Puzzle set 3 - Rank : <?php echo array_search($cus3['bspi'],$su3_r)+1; ?></h2>
 <br/>
 <div id="container3"  style="padding-top:20px;border:1px solid #71941b;background:url('<?php echo base_url(); ?>assets/images/chart_patterns/ccp1.png')">
</div>
 </div> 
  <?php } ?> 
  
  <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <h2 id="MonthID_overall">Overall - Rank : <?php echo array_search($cus_overall['bspi'],$su_overall_r)+1; ?></h2>
 <br/>
 <div id="container_overall"  style="padding-top:20px;border:1px solid #c14c19;background:url('<?php echo base_url(); ?>assets/images/chart_patterns/dcp1.png')">
</div>
 </div> 
 
 </div> 
 
 
 
 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
 body{min-height:0 !important;}
 body{overflow:hidden}
 .nice-select span.current{font-size: 20px}
 .nice-select .option {font-size: 10px}
 .nice-select ul{height:200px;overflow-y:scroll !important}
 </style>