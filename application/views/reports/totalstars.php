<?php //echo 'hello'; exit; ?>
<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/styleinner.css">
 <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">


<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nice-select.js"></script>
<link href="<?php echo base_url();?>assets/css/nice-select.css" rel="stylesheet">

<div class="">
 <div class="container">


<div class="">
 

 <?php  /*style="background:url('<?php echo base_url(); ?>assets/images/chart_patterns/bg1.png') repeat" */
 //print_r($bspicomparison);  ?>
 
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
 <script src="https://code.highcharts.com/highcharts.js"></script>
 
<script src="https://highcharts.github.io/pattern-fill/pattern-fill.js"></script>



<script>
$(document).ready(function()
{
	totalstars();
});
 function totalstars()
 {
	 
	  
	
	 var chart = Highcharts.chart('container1', {

    chart: {
        type: 'column',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: true},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: [''],
		gridLineWidth: 0,
  minorGridLineWidth: 0,
       labels: {
            style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        }
    },
  plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true,
			style: {
                fontSize: '15px',
				color: '#0c315b',
				fontFamily: 'Phenomena-Regular'
            }
        }
            }
        },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Stars count',
       
            style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        
        },
       labels: {
            style: {
                fontSize: '25px',
				color: '#FF6600',
				fontFamily: 'Phenomena-Regular'
            }
        },
		 max: 25 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0
		
    },
	

legend:{
    useHTML: true,
	symbolWidth: 0,
    labelFormatter: function() {
		var img;
    var name = this.name;
	if(name=="Puzzle set - 1"){
     img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/b.png" width = "20px" height = "20px">';
	}
	else if(name=="Puzzle set - 2")
	{
		img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/a.png" width = "20px" height = "20px">';
	}
	else{
		img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/c.png" width = "20px" height = "20px">';
	}
    return img + '  ' + name;
  }
},
    series: [
	<?php if($this->session->set2planid!=0) { ?>
	{showInLegend: true, 
        name: 'Puzzle set - 1',
		 
        data:  [{y:<?php if($startwo=='' || $startwo==0) { $startwo=0; }  echo  $startwo; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/b.png', width: 20, height: 20 }}]
	},
    
	<?php } ?>
	{
		
		showInLegend: true, 
        name: 'Puzzle set - 2',
		 
        data: [{y:<?php if($starone=='' || $starone==0) { $starone=0; } echo $starone; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/a.png', width: 20, height: 20 }}]
	},
	<?php if($this->session->set3planid!=0) { ?>
	{showInLegend: true, 
        name: 'Puzzle set - 3',
		 
        data:  [{y:<?php if($starthree=='' || $starthree==0) { $starthree=0; } echo $starthree; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/c.png', width: 20, height: 20 }}]
	},
    
	<?php } ?>
	 ] 
});
 }
	 
	 
	 
 
</script>
 
  <div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <div><h2 id="MonthID1"><span class="col-lg-10">Total stars</span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>
 
 <div id="container1" style="background:#fff;padding-top:20px;border: 1px solid #ccc;">
</div>
 </div>
  
 </div> 
 </div>
   

 

  </div><!--/form_sec -->
 </div>
  <style>
  body{min-height:0 !important;}
 body{overflow: hidden;}
.highcharts-legend-item .highcharts-point{display:none;}

 
 </style>