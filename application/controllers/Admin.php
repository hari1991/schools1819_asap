<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Admin_model');
				$this->load->library('session');
				//$this->load->library('zip');				
				$this->load->helper('download');
			
        }
		
	public function index()
	{	
		$this->load->view('admin/login');
		
	}
		
	public function login()
	{			
		
		$this->load->view('admin/login');
		
	}
	
	public function logincheck()
	{	
	
	if($_POST['username']=='admin@skillangels.com' && $_POST['password']=='assessadmin')
	{	$this->session->set_userdata(array(
				'username'      => $_POST['username'],
				'role'			=>'1'
				));
		redirect('index.php/admin/dashboard');
	}
	else if($_POST['username']=='skillangels' && $_POST['password']=='assessadmin')
	{
		$this->session->set_userdata(array(
				'username'      => $_POST['username'],
				'role'			=>'2'
		));
		redirect('index.php/admin/dashboard');
	}
	else
	{		
		$data['error'] = "Invalid credentials";
		$this->load->view('admin/login', $data);		
	}
	}
	public function logout()
	{
		if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		redirect('index.php/admin/');
	}
	public function dashboard()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
		$data['skls'] = $this->Admin_model->getskls();
		
		$data['Registered'] = $this->Admin_model->GetRegisteredStudentCount();
		$data['Taken'] = $this->Admin_model->GetAssessmentTakenStudentCount();
		$data['Fully'] = $this->Admin_model->GetFullyAssessmentTakenStudentCount();
		//echo "<pre>";print_r($data);exit;
		$this->load->view('admin/admin_header');
		$this->load->view('admin/dashboard',$data);
	}
	public function GetCountReport()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$schoolid = $_REQUEST['schoolid'];
		$school_name = $_REQUEST['school_name'];
		$data['gradesection'] = $this->Admin_model->Getgradebysection($schoolid);
		$RegisteredUserCount = $this->Admin_model->GetRegisteredStudentCountbyschool($schoolid,$school_name);
		$AssessmentTakenCount= $this->Admin_model->GetAssessmentTakenStudentCountbyschool($schoolid,$school_name);
		$AssessmentTakenBSPI= $this->Admin_model->GetAssessmentTakenStudentBspibyschool($schoolid,$school_name);
		$FullyAssessmentTakenCount = $this->Admin_model->GetFullyAssessmentTakenStudentCountbyschool($schoolid,$school_name);
		$ClassSkillScore=$this->Admin_model->getClassSkillScore($schoolid);
		
		$arrClassSkillScore=array();
		foreach($ClassSkillScore as $row)
		{
			$arrClassSkillScore[$row['rowval'].'-M']=$row['M1'];
			$arrClassSkillScore[$row['rowval'].'-VP']=$row['VP1'];
			$arrClassSkillScore[$row['rowval'].'-FA']=$row['FA1'];
			$arrClassSkillScore[$row['rowval'].'-PS']=$row['PS1'];
			$arrClassSkillScore[$row['rowval'].'-LI']=$row['LI1'];
		}
		$data['ClassSkillScore']=$arrClassSkillScore;
		
		$Registered=array();
		foreach($RegisteredUserCount as $row)
		{
			$Registered[$row['rowval']]=$row['RegisteredCount'];
		}
		$data['RegisteredUserCount']=$Registered;
		
		$taken=array();
		foreach($AssessmentTakenCount as $row)
		{
			$taken[$row['rowval']]=$row['AssessmentTaken'];
		}
		$data['AssessmentTakenCount']=$taken;
		
		$takenbspi=array();
		foreach($AssessmentTakenBSPI as $row)
		{
			$takenbspi[$row['rowval']]=$row['avgbspi'];
		}
		$data['AssessmentTakenBSPI']=$takenbspi;
		
		$Fully=array();
		foreach($FullyAssessmentTakenCount as $row)
		{
			$Fully[$row['rowval']]=$row['AssessmentFullyTaken'];
		}
		$data['FullyAssessmentTakenCount']=$Fully;
		
		/*--------*//*--------*//*--------*//*--------*//*--------*//*--------*//*--------*/
		$BestPuzzleSet=$this->Admin_model->GetBestPuzzleSet($schoolid);
		$Best=array();
		
		foreach($BestPuzzleSet as $key=>$row)
		{
			foreach($row as $key1=>$row1)
		{
			
			if( $key1=='BSPISET1' ||  $key1=='BSPISET2' ||  $key1=='BSPISET3')
			{
			$Best[$row['gradename']][$key1]=$row1;
			}
		}
				
		}
		$data['BestPuzzleSet']=$Best;
		//echo "<pre>";print_r($data);exit;
		$this->load->view('admin/dashboard_count_ajax', $data);
	}
	public function GetBestReport()
	{	
		$schoolid = $_POST['schoolid'];
		
	}
	public function userupload()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
		$this->load->view('admin/admin_header');
		$this->load->view('admin/user_upload', $data);
	
	}
	
	public function salt_my_pass($password)
{
// Generate two salts (both are numerical)
$salt1 = mt_rand(1000,9999999999);
$salt2 = mt_rand(100,999999999);

// Append our salts to the password
$salted_pass = $salt1 . $password . $salt2;

// Generate a salted hash
$pwdhash = sha1($salted_pass);

// Place into an array
$hash['Salt1'] = $salt1;
$hash['Salt2'] = $salt2;
$hash['Hash'] = $pwdhash;

// Return the hash and salts to whatever called our function
return $hash;

}
	
	public function userupload_list()
	{ if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		
		if(isset($_POST['sbmtbtn'])) 
{
	 
	$ceationkey = date('YmdHis');
	$schoolid=$_REQUEST['schoolid'];
	$ddlGradeType=$_REQUEST['ddlGradeType'];

	
	$handle = fopen($_FILES['txtUpload']['tmp_name'], "r");
	
	
$csvfilename = "uploads/Userslist_".$ceationkey.".csv";
 $file = fopen($csvfilename,"w");
 fputcsv($file,array('Firstname','Lastname','Gender','DOB','Grade','Section','Schoolname','Username','Password'));
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		
		
	/*$data[0] = strtolower($data[0]);
	$data[0] = str_replace(' ','', $data[0]);
	*/
	$data['query1'] = $this->Admin_model->getusercount(str_replace(' ','', strtolower($data[0])), $schoolid);
	$userexist = $data['query1'][0]['userCount']; 
	 
	$password = 'skillangels';
				$hashpass = $this->salt_my_pass($password);
				 
				$shpassword = $hashpass['Hash']; 
				$salt1 = $hashpass['Salt1']; 
				$salt2 = $hashpass['Salt2'];
		
		if($userexist==0)
		{
			$data['import'] = $this->Admin_model->userimport($shpassword,$salt1,$salt2,$data[0],$data[1],$data[2],$data[3],$data[4],$schoolid,$ddlGradeType,$ceationkey,$data[5]);
		}
		else
		{
			$data['import'] = $this->Admin_model->userimport1($shpassword,$salt1,$salt2,$data[0],$data[1],$data[2],$data[3],$data[4],$schoolid,$ddlGradeType,$ceationkey,$userexist,$data[5]);
		}

    }
	
	
	$data['download'] = $this->Admin_model->userdownload($ceationkey);

 foreach($data['download'] as $downloaddata)
 {
	 fputcsv($file,array($downloaddata['fname'],$downloaddata['lname'],$downloaddata['gender'],$downloaddata['dob'],$downloaddata['gradename'],$downloaddata['section'],$downloaddata['school_name'],$downloaddata['username'],'skillangels' ));
 }
 
 

    fclose($handle);
	
$data['filename'] = $csvfilename; 
$this->load->view('admin/admin_header');
$this->load->view('admin/user_upload', $data);

}
	}
	
	public function report1()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
		$data['skls'] = $this->Admin_model->getskls();
		$this->load->view('admin/admin_header');
		$this->load->view('admin/report1', $data);
	
	}
	
	public function report2()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
		$data['skls'] = $this->Admin_model->getskls();
		$this->load->view('admin/admin_header');
		$this->load->view('admin/report2', $data);
	
	}
	
	public function report3()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
	//	$data['sectionid'] = 'D';
		$this->load->view('admin/admin_header');
			
			if(isset($_POST['schoolid']) || isset($_POST['gradeid']) || isset($_POST['sectionid']) ||  isset($_POST['rangefrom']) || isset($_POST['rangeto']) )
			{
		$schoolid = $_POST['schoolid'];
		$gradeid = $_POST['gradeid'];
		$sectionid = $_POST['sectionid'];
		$rangefrom = $_POST['rangefrom'];
		$rangeto = $_POST['rangeto'];
		
		
		$data['GradewiseScore_data1'] = $this->Admin_model->getClassPerformace_data($schoolid,$gradeid,$sectionid,'game_reports',$rangefrom,$rangeto);
		
		$data['GradewiseScore_data2'] = $this->Admin_model->getClassPerformace_data2($schoolid,$gradeid,$sectionid,'game_reports_set2',$rangefrom,$rangeto);
		
		$data['GradewiseScore_data3'] = $this->Admin_model->getClassPerformace_data3($schoolid,$gradeid,$sectionid,'game_reports_set3',$rangefrom,$rangeto);
		
		$data['getplan'] = $this->Admin_model->getplanid($schoolid,$gradeid);
		$planid = $data['getplan'][0]['plan_id'];
		$data['gradesetid'] = $this->Admin_model->getgradeidsets($planid,$gradeid);
		$planidset2 = $data['gradesetid'][0]['plan_id_set2'];
		$planidset3 = $data['gradesetid'][0]['plan_id_set3'];
		
		
		$data['GradewiseScore_data_overall'] = $this->Admin_model->getClassPerformace_data_overall($schoolid,$gradeid,$sectionid,$planidset2,$planidset3,$rangefrom,$rangeto);
		
		$data['data'] = $sectionid;
		$data['bspirange'] = $bspirange;
		//print_r($data['GradewiseScore_data1']);
			}

		$this->load->view('admin/report3', $data);
	
	}
	
	public function getschools()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
	//	$assestype = $_POST['val'];
		$data['schools'] = $this->Admin_model->getschools();
		$this->load->view('admin/report2', $data);
	}
	
	public function getsection()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$schoolid = $_REQUEST['schoolid'];
		$gradeid = $_REQUEST['gradeid'];
		$data['defaultselect']=$_REQUEST['defaultselect'];
		$data['section'] = $this->Admin_model->getsection($schoolid,$gradeid);
		$this->load->view('admin/section_ajax', $data);
		
	}
	public function getreport2()
	{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
	$schoolid = $_POST['schoolid'];
	$gradeid = $_POST['gradeid'];
	$skillid = $_POST['skillid'];
	$sklid = $_POST['sklid'];
	$sectionid = $_POST['sectionid'];
	$data['getplan'] = $this->Admin_model->getplanid($schoolid,$gradeid);
	$planid = $data['getplan'][0]['plan_id'];
	$data['gradesetid'] = $this->Admin_model->getgradeidsets($planid,$gradeid);
	
		$data['report2'] = $this->Admin_model->getreport2($schoolid,$gradeid,$skillid,$sklid,$sectionid);
		$this->load->view('admin/report2_ajax', $data);
	}
	
	public function getreport1()
	{  if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
	$schoolid = $_POST['schoolid'];
	$gradeid = $_POST['gradeid'];
	$sklid = $_POST['sklid'];
	$sectionid = $_POST['sectionid'];
	//$skillid = $_POST['skillid'];
		$data['sid']=$schoolid;
		$data['gid']=$gradeid;
		
		$data['getplan'] = $this->Admin_model->getplanid($schoolid,$gradeid);
		$planid = $data['getplan'][0]['plan_id'];
		$data['gradesetid'] = $this->Admin_model->getgradeidsets($planid,$gradeid);
		$data['report1'] = $this->Admin_model->getreport1($schoolid,$gradeid,$sklid,$sectionid);
		//echo "<pre>";print_r($data['report1']);exit;


$this->load->view('admin/report1_ajax', $data);
// Report End
}


public function generatereport()
	{ 
 

	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
	$schoolid = $_POST['schoolid'];
	$gradeid = $_POST['gradeid'];
	$sklid = $_POST['sklid'];
	$sectionid = $_POST['sectionid'];
	//$skillid = $_POST['skillid'];
		$data['sid']=$schoolid;
		$data['gid']=$gradeid;
		
		$data['getplan'] = $this->Admin_model->getplanid($schoolid,$gradeid);
		$planid = $data['getplan'][0]['plan_id'];
		$data['gradesetid'] = $this->Admin_model->getgradeidsets($planid,$gradeid);
		$data['report1'] = $this->Admin_model->getreport1($schoolid,$gradeid,$sklid,$sectionid);
		//echo "<pre>";print_r($data['report1']);exit;
if(count($data['report1'])>0)
{
	
foreach($data['report1'] as $r1) {
	//header('Content-Type: image/png');
	$uid=$r1['id'];
	$username=$r1['fname'];
	$grade_name=$r1['gradename'];
	//$section = $r1['section'];
	$school_name=$r1['school_name'];
	if($school_name=='')
	{
		$school_name=$r1['schoolname'];
	}
	$BSPIScore=$r1['avgbspiset1'];
	/* Skill Scores */
	$fMemoryScore=round($r1['skillscorem'], 2);
	$fVisualProcessingScore=round($r1['skillscorev'], 2);
	$fFocusAndAttentionScore=round($r1['skillscoref'], 2);
	$fProblemSolvingScore=round($r1['skillscorep'], 2);
	$fLinguisticsScore=round($r1['skillscorel'], 2);
	/* Skill Wise Scores */
	/* Stars */	
	$filled_stars=0;
	if($r1['skillscorem'] < 20){$filled_stars =$filled_stars+0;}
	if($r1['skillscorem'] >= 20 && $r1['skillscorem'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorem'] >= 41 && $r1['skillscorem'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorem'] >= 61 && $r1['skillscorem'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorem'] >= 81 && $r1['skillscorem'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorem'] >= 91 && $r1['skillscorem'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorev'] < 20){$filled_stars =$filled_stars+0;}
	if($r1['skillscorev'] >= 20 && $r1['skillscorev'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorev'] >= 41 && $r1['skillscorev'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorev'] >= 61 && $r1['skillscorev'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorev'] >= 81 && $r1['skillscorev'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorev'] >= 91 && $r1['skillscorev'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscoref'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscoref'] >= 20 && $r1['skillscoref'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscoref'] >= 41 && $r1['skillscoref'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscoref'] >= 61 && $r1['skillscoref'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscoref'] >= 81 && $r1['skillscoref'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscoref'] >= 91 && $r1['skillscoref'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorep'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscorep'] >= 20 && $r1['skillscorep'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorep'] >= 41 && $r1['skillscorep'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorep'] >= 61 && $r1['skillscorep'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorep'] >= 81 && $r1['skillscorep'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorep'] >= 91 && $r1['skillscorep'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorel'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscorel'] >= 20 && $r1['skillscorel'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorel'] >= 41 && $r1['skillscorel'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorel'] >= 61 && $r1['skillscorel'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorel'] >= 81 && $r1['skillscorel'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorel'] >= 91 && $r1['skillscorel'] <= 100){$filled_stars =$filled_stars+5;}
		
	/* Star */
	$Greeting1='Wow !!!';
	$Greeting2='Good !!!';
	$Greeting3='Nice !!!';
	$Greeting4='Super !!!';
	$Greeting5='Great !!!';
	$Greeting6='Excellent !!!';
	$Greeting7='Wonderful !!!';
	$Greeting8='Awesome !!!';
	$Greeting9='Terrific !!!';
	$Greeting10='Fantasticc !!!';
	//$BackgroundImage= base_url().'assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';
	$BackgroundImage= '/mnt/vol1/sites/nschools/assessments/assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';

	$img = $this->LoadPNG($BackgroundImage,$username,$grade_name,$school_name,$filled_stars,$BSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$Greeting1,$Greeting2,$Greeting3,$Greeting4,$Greeting5,$Greeting6,$Greeting7,$Greeting8,$Greeting9,$Greeting10);
	 
		$foldername="report_image/".trim($_POST['schoolid'])."/".trim($_POST['gradeid'])."/".$r1['section']."/";
		if (!file_exists($foldername)) {
			mkdir($foldername, 0777, true);
		}
		imagepng($img,$foldername.''.$uid.'_'.$username.".png");
		imagedestroy($img);
}

   $this->load->library('zip');
   $path = FCPATH."/report_image/".$_POST['schoolid']."/";
   $this->zip->read_dir($path,FALSE);
   $this->zip->archive(FCPATH."/report_image/".$_POST['schoolid']."_".$_POST['gradeid'].".zip");  
   
   

	
}
 $arrResult=array("response"=>"success","schoolid"=>$_POST['schoolid'], "gradeid"=>$_POST['gradeid']);
 echo json_encode($arrResult);
//echo 1; exit;
}


public function LoadPNG($fBackgroundImage,$name,$grade_name,$school_name,$fTotalScore,$fBSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$fGreeting1,$fGreeting2,$fGreeting3,$fGreeting4,$fGreeting5,$fGreeting6,$fGreeting7,$fGreeting8,$fGreeting9,$fGreeting10)
	{ //echo $fBSPIScore;exit;
		 /* Attempt to open */
    $im = @imagecreatefrompng($fBackgroundImage);
    /* See if it failed */
    if(!$im)
    {
      echo "Loading bg has issue ".$fBackgroundImage;
    }
	 
	// Create some colors
	$white = imagecolorallocate($im, 255, 255, 255);
	$grey = imagecolorallocate($im, 128, 128, 128);
	$black = imagecolorallocate($im, 0, 0, 0);
$grey1 = imagecolorallocate($im, 228, 228, 228);

	// The text to draw
	// Replace path by your own font path
	$font = APPPATH."../assets/fonts/OpenSansBold.ttf";
//	$font = "/mnt/vol1/sites/gtec/assess/assets/fonts/OpenSansBold.ttf";
	// Add some shadow to the text

	$text_color = imagecolorallocate($im, 0, 0, 0);
	/* imagestring($im, 5, 140, 66, str_pad($name,50," ",STR_PAD_BOTH), $text_color);
	imagestring($im, 5, 140, 87, str_pad($grade_name,50," ",STR_PAD_BOTH), $text_color);
	imagestring($im, 5, 212, 105, str_pad($school_name,50," ",STR_PAD_BOTH), $text_color); */
	
	/* imagestring($im, 5, 320, 66,$name, $text_color);
	imagestring($im, 5, 320, 87,$grade_name, $text_color);
	imagestring($im, 5, 320, 105,$school_name, $text_color); */
	
/*	imagestring($im, 5,300, 75,"Name:", $black);
	imagestring($im, 5, 350, 75,$name, $text_color);
	imagestring($im, 5,292, 95,"Grade:", $black);
	imagestring($im, 5, 341, 95,str_replace('Grade','',$grade_name), $text_color); */
	
	imagestring($im, 3, 90, 68, $name, $text_color);
	imagestring($im, 3, 90, 87, str_replace("Class-",'',$grade_name), $text_color);
	//imagestring($im, 3, 90, 106, $school_name, $text_color);
	imagefilledrectangle($im, 0,106, 100, 120, $grey1);
	
	/* BSPIScore */ 
	$text_color = imagecolorallocate($im, 255, 255, 255);
	if (strlen($fBSPIScore) == 1)
    {
     imagettftext ($im, 25, 0, 195, 350, $text_color, $font, $fBSPIScore);
    }

	 elseif (strlen($fBSPIScore) > 2)
    {
     imagettftext ($im, 25, 0, 170, 350, $text_color, $font, $fBSPIScore);
    }
	
		elseif (strlen($fBSPIScore) > 3)
    {
     imagettftext ($im, 25, 0, 100, 350, $text_color, $font, $fBSPIScore);
    }
	else
    {
     imagettftext ($im, 25, 0, 180, 350, $text_color, $font, $fBSPIScore);
    }

/* TotalScore */ 
    if (strlen($fTotalScore) == 1)
    {
    imagettftext ($im, 50, 0, 570, 300, $text_color, $font, $fTotalScore);
    }
    elseif (strlen($fTotalScore) > 2)
    {
     imagettftext ($im, 50, 0, 530, 300, $text_color, $font, $fTotalScore);
    }
	else
    {
     imagettftext ($im, 50, 0, 550, 300, $text_color, $font, $fTotalScore);
    }
	
	
	
	/* MemoryScore */ 
	$text_color = imagecolorallocate($im, 255, 0, 0);
	
	if (strlen($fMemoryScore) == 1)
    {
      imagettftext ($im, 30, 0, 263, 510, $text_color, $font, $fMemoryScore);
    }
    elseif (strlen($fMemoryScore) > 2)
    {
     imagettftext ($im, 30, 0, 243, 510, $text_color, $font, $fMemoryScore);
    }
	else
    {
      imagettftext ($im,  30, 0, 253, 510, $text_color, $font, $fMemoryScore);
    }
	/* VisualProcessingScore */ 
	$text_color = imagecolorallocate($im, 255, 153, 51);
	
	if (strlen($fVisualProcessingScore) == 1)
	{
   
    imagettftext ($im, 30, 0, 373, 510, $text_color, $font, $fVisualProcessingScore);
   }
    elseif (strlen($fVisualProcessingScore) > 2)
    {
      imagettftext ($im, 30, 0, 353, 510, $text_color, $font, $fVisualProcessingScore);
    }
	else
    {
       imagettftext ($im, 30, 0, 363, 510, $text_color, $font, $fVisualProcessingScore);
    }
   /* FocusAndAttentionScore */ 
	$text_color = imagecolorallocate($im, 102, 204, 0);
	
	if (strlen($fFocusAndAttentionScore) == 1)
   {
    imagettftext ($im, 30, 0, 484, 510, $text_color, $font, $fFocusAndAttentionScore);
   }
    elseif (strlen($fFocusAndAttentionScore) > 2)
    {
      imagettftext ($im, 30, 0, 464, 510, $text_color, $font, $fFocusAndAttentionScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 474, 510, $text_color, $font, $fFocusAndAttentionScore);
    }
   /* ProblemSolvingScore */ 
	$text_color = imagecolorallocate($im, 230, 107, 25);
	
	if (strlen($fProblemSolvingScore) == 1)
  {
   imagettftext ($im, 30, 0, 594, 510, $text_color, $font, $fProblemSolvingScore);
   }
  elseif (strlen($fProblemSolvingScore) > 2)
    {
      imagettftext ($im,30, 0, 574, 510, $text_color, $font, $fProblemSolvingScore);
    }
	else
    {
       imagettftext ($im,30, 0, 584, 510, $text_color, $font, $fProblemSolvingScore);
    }
   /* LinguisticsScore */ 
	$text_color = imagecolorallocate($im, 51, 204, 255);
	
	if (strlen($fLinguisticsScore) == 1)
  {
  imagettftext ($im, 30, 0, 705, 510, $text_color, $font, $fLinguisticsScore);
  }
  elseif (strlen($fLinguisticsScore) > 2)
    {
      imagettftext ($im, 30, 0, 685, 510, $text_color, $font, $fLinguisticsScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 695, 510, $text_color, $font, $fLinguisticsScore);
    }
	/* Greetings Range */
	$text_color = imagecolorallocate($im, 0, 0, 0);
	
  if ($fBSPIScore < 11)
  {
  imagestring($im, 5, 0, 85, str_pad($fGreeting1,150," ",STR_PAD_BOTH), $text_color);
   
  }
  elseif ($fBSPIScore < 21)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting2,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	elseif ($fBSPIScore < 31)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting3,150," ",STR_PAD_BOTH), $text_color);
	  
    }
	 elseif ($fBSPIScore < 41)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting4,150," ",STR_PAD_BOTH), $text_color);
	
    }
	 elseif ($fBSPIScore < 51)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting5,150," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 61)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting6,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	 elseif ($fBSPIScore < 71)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting7,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	 elseif ($fBSPIScore < 81)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting8,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	elseif ($fBSPIScore < 91)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting9,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	else
    {
imagestring($im, 5, 0, 77, str_pad($fGreeting10,150," ",STR_PAD_BOTH), $text_color);

    }


	
    return $im;
	}

/*	
function add_data($name, $data) {
	
	$this->zip->archive("./report_image/".$_POST['schoolid']."/".$_POST['gradeid']."/".$_POST['schoolid']."_".$_POST['gradeid'].".zip"); 
	$this->zip->download($_POST['gradeid'].".zip");
}*/

/*
	
function create_zip($src= array(), $dst='', $overwrite = false)
{
	
	//print_r($dst);
    if (!extension_loaded('zip') || !file_exists($src)) {
       // return false;
		
		//echo 'hai'; exit;
    }

    $zip = new ZipArchive();
    if (!$zip->open($dst, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE)) {
        return false;
    }

    $src = str_replace('\\', '/', realpath($src));

    if (is_dir($src) === true)
    {
		
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($src), RecursiveIteratorIterator::SELF_FIRST);
		//$files = glob($src . '/*');
        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
				
                $zip->addEmptyDir(str_replace($src . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
				
                $zip->addFromString(str_replace($src . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($src) === true)
    {
	
        $zip->addFromString(basename($src), file_get_contents($src));
    }

    return $zip->close();
}

*/

/* function create_zip($zip,$dir){
 if (is_dir($dir)){

  if ($dh = opendir($dir)){
   while (($file = readdir($dh)) !== false){
 
    
     // If directory
     if(is_dir($dir.$file) ){

      if($file != '' && $file != '.' && $file != '..'){

       // Add empty directory
       $zip->addEmptyDir($dir.$file, basename($file));

       $folder = $dir.$file.'/';
 
       // Read data of the folder
       create_zip($zip,$folder);
	   
	 //  $zip->addFile($file, basename($file));
      }
     }
 
    
 
   }
   closedir($dh);
  }
 }
} 

function addzip($source, $destination) {
$src = glob($source . '/*');
$this->create_zip($src, $dst);

}*/
	
	public function report2_excel()
	{
	$schoolid = $_POST['schoolid'];
	$gradeid = $_POST['gradeid'];
	$skillid = $_POST['skillid'];
	$sklid = $_POST['sklid'];
	$sectionid = $_POST['sectionid'];
	$data['getplan'] = $this->Admin_model->getplanid($schoolid,$gradeid);
	$planid = $data['getplan'][0]['plan_id'];
	$gradesetid = $this->Admin_model->getgradeidsets($planid,$gradeid);
	
	$data['report2'] = $this->Admin_model->getreport2($schoolid,$gradeid,$skillid,$sklid,$sectionid);
	
	
	//print_r($data['report2']);
	
	
	$this->load->library('Excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
//name the worksheet
$this->excel->getActiveSheet()->setTitle('Skillscore Report');
$objPHPExcel=$this->excel;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(65);



$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
$objPHPExcel->getActiveSheet()->mergeCells('A3:E3');

	



$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
/* $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(26);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(22);*/
$sections = '';
if($sectionid=='') { $sections = ''; } else { $sections = $sectionid; }

$objPHPExcel->getActiveSheet()->SetCellValue('A2',"Skillscore Report - ".$data['report2'][0]['schoolname']."");
$objPHPExcel->getActiveSheet()->SetCellValue('A3',$data['report2'][0]['gradename'] ."-". $sections);
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '00b0f0')
        ),'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffedb0')
        ),'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffedb0')
        ),'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A4:E4')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '5cb85c')
        ),'font'  => array(
        'bold'  => true)
    )
);

	$objPHPExcel->getActiveSheet()->SetCellValue('A4','S.No.' );
	$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Username');
	$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Grade');
	
/*	 if($gradesetid[0]['gradeid_set2']!='' && $gradesetid[0]['gradeid_set2']!='0') { 
	$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Skill Score Set1');
	$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'AVG.Response time');
	 }
	*/
	$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Skill Score');
	$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'AVG.Response time');
	
	/* if($gradesetid[0]['gradeid_set3']!='' && $gradesetid[0]['gradeid_set3']!='0') {
	$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Skill Score Set3');
	$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'AVG.Response time');
	} */
	
	
$ini=4;
			

			foreach($data['report2'] as $result){ $ini++; $total=0;
			
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ini, $ini-4);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$ini, $result['fname']);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$ini, $result['gradename']);
	
/*	if($gradesetid[0]['gradeid_set2']!='' && $gradesetid[0]['gradeid_set2']!='0') {
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$ini, round($result['score2'], 2));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$ini, round($result['rstime2'], 2));
	}
	*/
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$ini, round($result['score'], 2));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$ini, round($result['rstime'], 2));
	
/*	if($gradesetid[0]['gradeid_set3']!='' && $gradesetid[0]['gradeid_set3']!='0') {
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$ini, round($result['score3'], 2));
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$ini, round($result['rstime3'], 2));
	}*/
			}

$objPHPExcel->getActiveSheet()->getStyle("A1:E".$ini)->applyFromArray(
    array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => '000000')
            )
        )
    )
);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$logo =APPPATH."../assets/admin/images/excellogo.png"; // Provide path to your logo file
$objDrawing->setPath($logo);
$objDrawing->setOffsetX(8);    // setOffsetX works properly
$objDrawing->setOffsetY(300);  //setOffsetY has no effect
$objDrawing->setCoordinates('A1');
$objDrawing->setHeight(75); // logo height
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); 
echo $filename='Reports/Skillscore_report'.date('Ymdhmis').'.xls'; //save our workbook as this file name

            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save($filename);
	
	}
	
	public function report1_excel()
	{
		
		
	$schoolid = $_POST['schoolid'];
	$gradeid = $_POST['gradeid'];
	$sklid = $_POST['sklid'];
	$sectionid = $_POST['sectionid'];
		$data['getplan'] = $this->Admin_model->getplanid($schoolid,$gradeid);
		$planid = $data['getplan'][0]['plan_id'];
		$data['gradesetid'] = $this->Admin_model->getgradeidsets($planid,$gradeid);	
	$data['report1'] = $this->Admin_model->getreport1($schoolid,$gradeid,$sklid,$sectionid);	
		
		
		//echo 'hello'; exit;
$this->load->library('excel');
//activate worksheet number 1
$this->excel->setActiveSheetIndex(0);
//name the worksheet
$this->excel->getActiveSheet()->setTitle("BSPI Report");
$objPHPExcel=$this->excel;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(65);
if(($data['gradesetid'][0]['gradeid_set2']=='' || $data['gradesetid'][0]['gradeid_set2']=='0') && ($data['gradesetid'][0]['gradeid_set3']=='' || $data['gradesetid'][0]['gradeid_set3']=='0')){

$objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
$objPHPExcel->getActiveSheet()->mergeCells('A3:L3');

}




$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(26);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(22);

$sections = '';
if($sectionid=='') { $sections = ''; } else { $sections = $sectionid; }

$objPHPExcel->getActiveSheet()->SetCellValue('A2',"BSPI Report - ".$data['report1'][0]['schoolname']."");
$objPHPExcel->getActiveSheet()->SetCellValue('A3',$data['report1'][0]['gradename'] ."-". $sections );
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '00b0f0')
        ),'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffedb0')
        ),'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffedb0')
        ),'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A4:L4')->applyFromArray(
    array(
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '5cb85c')
        ),'font'  => array(
        'bold'  => true)
    )
);

	$objPHPExcel->getActiveSheet()->SetCellValue('A4','S.No.' );
	$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Name');
	$objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Username');
	$objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Section');
	
	$objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Memory');
	$objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Visual Processing');
	$objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Focus and Attention');
	$objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Problem Solving');
	$objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Linguistics');
	$objPHPExcel->getActiveSheet()->SetCellValue('J4', 'BSPI');
	$objPHPExcel->getActiveSheet()->SetCellValue('K4', 'Status');
	$objPHPExcel->getActiveSheet()->SetCellValue('L4', 'Completed Date');
	

	
	
$ini=4;
			

			foreach($data['report1'] as $result){ $ini++; $total=0; $var1 =0; $var2 = 0; $flag=0;
			
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$ini, $ini-4);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$ini, $result['fname']);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$ini, $result['username']);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$ini, $result['section']);
	
	if($result['skillscorem']==''){$skillscorem='-';$flag=1;}else{$skillscorem=round($result['skillscorem'], 2);}
	if($result['skillscorev']==''){$skillscorev='-';$flag=1;}else{$skillscorev=round($result['skillscorev'], 2);}
	if($result['skillscoref']==''){$skillscoref='-';$flag=1;}else{$skillscoref=round($result['skillscoref'], 2);}
	if($result['skillscorep']==''){$skillscorep='-';$flag=1;}else{$skillscorep=round($result['skillscorep'], 2);}
	if($result['skillscorel']==''){$skillscorel='-';$flag=1;}else{$skillscorel=round($result['skillscorel'], 2);}
	if($result['avgbspiset1']==''){$avgbspiset1='-';}else{if($flag!=1){$avgbspiset1=round($result['avgbspiset1'], 2);}else{$avgbspiset1='-';}}
	if($result['status']=="") {  $status = "Pending"; } else { $status = $result['status']; }
	
	if($result['status']=="") { $compdate = ''; } else { $compdate = date('d-m-Y',strtotime($result['completeddate']));  }
	
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$ini, $skillscorem);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$ini, $skillscorev);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$ini, $skillscoref);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$ini, $skillscorep);
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$ini, $skillscorel);
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$ini, $avgbspiset1);
	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$ini, $status);
	$objPHPExcel->getActiveSheet()->SetCellValue('L'.$ini, $compdate);
	
	 

	
			}

$objPHPExcel->getActiveSheet()->getStyle("A1:L".$ini)->applyFromArray(
    array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => '000000')
            )
        )
    )
);

$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$logo =APPPATH."../assets/admin/images/excellogo.png"; // Provide path to your logo file
$objDrawing->setPath($logo);
$objDrawing->setOffsetX(8);    // setOffsetX works properly
$objDrawing->setOffsetY(300);  //setOffsetY has no effect
$objDrawing->setCoordinates('A1');
$objDrawing->setHeight(75); // logo height
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet()); 
echo $filename='Reports/BSPI_Report'.date('Ymdhmis').'.xls'; //save our workbook as this file name

            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
$objWriter->save($filename);

		
	}
	
public function getbubblereport1()
{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		
		$this->load->view('admin/report3', $data);
}
public function bspitoppers()
{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
		$data['skls'] = $this->Admin_model->getskls();
		if(isset($_POST['schoolid']) || isset($_POST['topid']))
		{
			$schoolid = $_POST['schoolid'];
			$gradeid = $_POST['gradeid'];
			$topid = $_POST['topid'];
			$sklid = $_POST['sklid'];
			$sectionid = $_POST['sectionid'];
			
				$data['GetTOPBSPIUser'] = $this->Admin_model->GetTOPBSPIUser($schoolid,$gradeid,$topid,$sklid,$sectionid);
				$data['data'] = $sectionid;
		}
		$this->load->view('admin/admin_header');
		$this->load->view('admin/bspitoppers', $data);
}
public function skilltoppers()
{	if($this->session->username=="" || !isset($this->session->username)){redirect('index.php/admin/');}
		$data['schools'] = $this->Admin_model->getschools();
		$data['grades'] = $this->Admin_model->getgrades();
		$data['skls'] = $this->Admin_model->getskls();
			
		if(isset($_POST['schoolid']) || isset($_POST['topid']))
		{
			$schoolid = $_POST['schoolid'];
			$gradeid = $_POST['gradeid'];
			$topid = $_POST['topid'];
			$sklid = $_POST['sklid'];
			$sectionid = $_POST['sectionid'];
			
			$data['data'] = $sectionid;
			//$data1['section'] = $this->Admin_model->getsection($schoolid,$gradeid);
			
				$Skill1TopScore = $this->Admin_model->GetSkill1TopScore($schoolid,$gradeid,$topid,$sklid,$sectionid);
				$Skill2TopScore = $this->Admin_model->GetSkill2TopScore($schoolid,$gradeid,$topid,$sklid,$sectionid);
				$Skill3TopScore = $this->Admin_model->GetSkill3TopScore($schoolid,$gradeid,$topid,$sklid,$sectionid);
				$Skill4TopScore = $this->Admin_model->GetSkill4TopScore($schoolid,$gradeid,$topid,$sklid,$sectionid);
				$Skill5TopScore = $this->Admin_model->GetSkill5TopScore($schoolid,$gradeid,$topid,$sklid,$sectionid);
				$i=0;
				
				for($i=0;$i<$_POST['topid'];$i++)
				{	
					$data['skilltoppers']['memory'][$i]=$Skill1TopScore[$i]['username'];
					$data['skilltoppers']['memoryscore'][$i]=$Skill1TopScore[$i]['gamescore'];
					$data['skilltoppers']['visual'][$i]=$Skill2TopScore[$i]['username'];
					$data['skilltoppers']['visualscore'][$i]=$Skill2TopScore[$i]['gamescore'];
					$data['skilltoppers']['focus'][$i]=$Skill3TopScore[$i]['username'];
					$data['skilltoppers']['focusscore'][$i]=$Skill3TopScore[$i]['gamescore'];
					$data['skilltoppers']['problem'][$i]=$Skill4TopScore[$i]['username'];
					$data['skilltoppers']['problemscore'][$i]=$Skill4TopScore[$i]['gamescore'];
					$data['skilltoppers']['linguistics'][$i]=$Skill5TopScore[$i]['username'];
					$data['skilltoppers']['linguisticsscore'][$i]=$Skill5TopScore[$i]['gamescore'];
				}
				//echo "<pre>";print_r($data);exit;
				/* $j=1;
				foreach($Skill2TopScore as $row)
				{	$data['visual'][$j]=$row['username'];
					$j++;
				}
				$k=1;
				foreach($Skill3TopScore as $row)
				{	$data['focus'][$k]=$row['username'];
					$k++;
				}
				$l=1;
				foreach($Skill4TopScore as $row)
				{	$data['Problem'][$l]=$row['username'];
					$l++;
				}
				$m=1;
				foreach($Skill5TopScore as $row)
				{	$data['Linguistics'][$n]=$row['username'];
					$n++;
				} */
						
				//echo "<pre>";print_r($data);exit;
		}
		$this->load->view('admin/admin_header');
		$this->load->view('admin/skilltoppers', $data);
}
public function demouser()
{
	$data['NithyaRefUsers'] = $this->Admin_model->GetNithyaRefUsers();//echo "<pre>";print_r($data['report1']);exit;
	$this->load->view('admin/demouser', $data);
}
public function getreportcard()
{	
	$userid=$_POST['userid'];
	$sname=$_POST['sname'];
	$this->Admin_model->UpdateUsername($userid,$sname); 
	$r=$this->Admin_model->getUserReportCard($userid);
	$r1=$r[0];
	$uid=$r1['id'];
	$username=$r1['fname'];
	$uname=$r1['username'];
	$grade_name=$r1['gradename'];
	//$section = $r1['section'];
	$school_name=$r1['school_name'];
	if($school_name=='')
	{
		$school_name=$r1['schoolname'];
	}
	$BSPIScore=$r1['avgbspiset1'];
	/* Skill Scores */
	$fMemoryScore=round($r1['skillscorem'], 2);
	$fVisualProcessingScore=round($r1['skillscorev'], 2);
	$fFocusAndAttentionScore=round($r1['skillscoref'], 2);
	$fProblemSolvingScore=round($r1['skillscorep'], 2);
	$fLinguisticsScore=round($r1['skillscorel'], 2);
	/* Skill Wise Scores */
	/* Stars */	
	$filled_stars=0;
	if($r1['skillscorem'] < 20){$filled_stars =$filled_stars+0;}
	if($r1['skillscorem'] >= 20 && $r1['skillscorem'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorem'] >= 41 && $r1['skillscorem'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorem'] >= 61 && $r1['skillscorem'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorem'] >= 81 && $r1['skillscorem'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorem'] >= 91 && $r1['skillscorem'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorev'] < 20){$filled_stars =$filled_stars+0;}
	if($r1['skillscorev'] >= 20 && $r1['skillscorev'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorev'] >= 41 && $r1['skillscorev'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorev'] >= 61 && $r1['skillscorev'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorev'] >= 81 && $r1['skillscorev'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorev'] >= 91 && $r1['skillscorev'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscoref'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscoref'] >= 20 && $r1['skillscoref'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscoref'] >= 41 && $r1['skillscoref'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscoref'] >= 61 && $r1['skillscoref'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscoref'] >= 81 && $r1['skillscoref'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscoref'] >= 91 && $r1['skillscoref'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorep'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscorep'] >= 20 && $r1['skillscorep'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorep'] >= 41 && $r1['skillscorep'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorep'] >= 61 && $r1['skillscorep'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorep'] >= 81 && $r1['skillscorep'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorep'] >= 91 && $r1['skillscorep'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorel'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscorel'] >= 20 && $r1['skillscorel'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorel'] >= 41 && $r1['skillscorel'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorel'] >= 61 && $r1['skillscorel'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorel'] >= 81 && $r1['skillscorel'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorel'] >= 91 && $r1['skillscorel'] <= 100){$filled_stars =$filled_stars+5;}
		
	/* Star */
	$Greeting1='Wow !!!';
	$Greeting2='Good !!!';
	$Greeting3='Nice !!!';
	$Greeting4='Super !!!';
	$Greeting5='Great !!!';
	$Greeting6='Excellent !!!';
	$Greeting7='Wonderful !!!';
	$Greeting8='Awesome !!!';
	$Greeting9='Terrific !!!';
	$Greeting10='Fantasticc !!!';
	//$BackgroundImage= base_url().'assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';
	//echo $BSPIScore."==".$BackgroundImage;exit;
	$BackgroundImage= '/mnt/vol1/sites/nschools/assessments/assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';

	$img = $this->LoadPNGnew($BackgroundImage,$username,$grade_name,$school_name,$filled_stars,$BSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$Greeting1,$Greeting2,$Greeting3,$Greeting4,$Greeting5,$Greeting6,$Greeting7,$Greeting8,$Greeting9,$Greeting10,$uname);	
	
	$foldername="report_image/".trim($userid)."/";
		if (!file_exists($foldername)) {
			mkdir($foldername, 0777, true);
		}
	$username=str_replace(' ', '', $username);
	imagepng($img,$foldername.''.$userid.'_'.$username.".png");
	imagedestroy($img);
	$file_name=$userid.'_'.$username.".png";
	$file_url=base_url()."".$foldername.''.$userid.'_'.$username.".png";
	echo $file_url;exit;
}
public function downloadreport($uid)
{	
	$userid=$uid;
	$r=$this->Admin_model->getUserReportCard($userid);
	$r1=$r[0];
	$uid=$r1['id'];
	$username=$r1['fname'];
	$uname=$r1['username'];
	$grade_name=$r1['gradename'];
	//$section = $r1['section'];
	$school_name=$r1['school_name'];
	if($school_name=='')
	{
		$school_name=$r1['schoolname'];
	}
	$BSPIScore=$r1['avgbspiset1'];
	/* Skill Scores */
	$fMemoryScore=round($r1['skillscorem'], 2);
	$fVisualProcessingScore=round($r1['skillscorev'], 2);
	$fFocusAndAttentionScore=round($r1['skillscoref'], 2);
	$fProblemSolvingScore=round($r1['skillscorep'], 2);
	$fLinguisticsScore=round($r1['skillscorel'], 2);
	/* Skill Wise Scores */
	/* Stars */	
	$filled_stars=0;
	if($r1['skillscorem'] < 20){$filled_stars =$filled_stars+0;}
	if($r1['skillscorem'] >= 20 && $r1['skillscorem'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorem'] >= 41 && $r1['skillscorem'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorem'] >= 61 && $r1['skillscorem'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorem'] >= 81 && $r1['skillscorem'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorem'] >= 91 && $r1['skillscorem'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorev'] < 20){$filled_stars =$filled_stars+0;}
	if($r1['skillscorev'] >= 20 && $r1['skillscorev'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorev'] >= 41 && $r1['skillscorev'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorev'] >= 61 && $r1['skillscorev'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorev'] >= 81 && $r1['skillscorev'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorev'] >= 91 && $r1['skillscorev'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscoref'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscoref'] >= 20 && $r1['skillscoref'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscoref'] >= 41 && $r1['skillscoref'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscoref'] >= 61 && $r1['skillscoref'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscoref'] >= 81 && $r1['skillscoref'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscoref'] >= 91 && $r1['skillscoref'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorep'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscorep'] >= 20 && $r1['skillscorep'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorep'] >= 41 && $r1['skillscorep'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorep'] >= 61 && $r1['skillscorep'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorep'] >= 81 && $r1['skillscorep'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorep'] >= 91 && $r1['skillscorep'] <= 100){$filled_stars =$filled_stars+5;}
	
	if($r1['skillscorel'] < 20){$filled_stars = $filled_stars+0;}
	if($r1['skillscorel'] >= 20 && $r1['skillscorel'] <= 40){$filled_stars =$filled_stars+1;}
	if($r1['skillscorel'] >= 41 && $r1['skillscorel'] <= 60){$filled_stars =$filled_stars+2;}
	if($r1['skillscorel'] >= 61 && $r1['skillscorel'] <= 80){$filled_stars =$filled_stars+3;}
	if($r1['skillscorel'] >= 81 && $r1['skillscorel'] <= 90){$filled_stars =$filled_stars+4;}
	if($r1['skillscorel'] >= 91 && $r1['skillscorel'] <= 100){$filled_stars =$filled_stars+5;}
		
	/* Star */
	$Greeting1='Wow !!!';
	$Greeting2='Good !!!';
	$Greeting3='Nice !!!';
	$Greeting4='Super !!!';
	$Greeting5='Great !!!';
	$Greeting6='Excellent !!!';
	$Greeting7='Wonderful !!!';
	$Greeting8='Awesome !!!';
	$Greeting9='Terrific !!!';
	$Greeting10='Fantasticc !!!';
	//$BackgroundImage= base_url().'assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';
	//echo $BSPIScore."==".$BackgroundImage;exit;
	$BackgroundImage= '/mnt/vol1/sites/nschools/assessments/assets/images/report_card/asap'.str_pad(ROUND(($BSPIScore/2),0)*2,3,"0",STR_PAD_LEFT).'.png';

	$img = $this->LoadPNGnew($BackgroundImage,$username,$grade_name,$school_name,$filled_stars,$BSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$Greeting1,$Greeting2,$Greeting3,$Greeting4,$Greeting5,$Greeting6,$Greeting7,$Greeting8,$Greeting9,$Greeting10,$uname);	
	
	$foldername="report_image/".trim($userid)."/";
		if (!file_exists($foldername)) {
			mkdir($foldername, 0777, true);
		}
	$username=str_replace(' ', '', $username);
	imagepng($img,$foldername.''.$userid.'_'.$username.".png");
	imagedestroy($img);
	$file_name=$userid.'_'.$username.".png";
	$file_url=base_url()."".$foldername.''.$userid.'_'.$username.".png";
	echo $file_url;exit;
	header('Connection: keep-alive');
 ob_clean(); 
$data = file_get_contents($file_url); echo "DDDDD";exit;
$name = $file_name; 
force_download($name,$data); echo "ccccccc";exit; 

	/*header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"".$file_name."\""); 
	readfile($file_url);exit;*/
}

public function LoadPNGnew($fBackgroundImage,$name,$grade_name,$school_name,$fTotalScore,$fBSPIScore,$fMemoryScore,$fVisualProcessingScore,$fFocusAndAttentionScore,$fProblemSolvingScore,$fLinguisticsScore,$fGreeting1,$fGreeting2,$fGreeting3,$fGreeting4,$fGreeting5,$fGreeting6,$fGreeting7,$fGreeting8,$fGreeting9,$fGreeting10,$uname)
{ //echo $fBSPIScore;exit;
		 /* Attempt to open */
    $im = @imagecreatefrompng($fBackgroundImage);
    /* See if it failed */
    if(!$im)
    {
      echo "Loading bg has issue ".$fBackgroundImage;
    }
	 
	// Create some colors
	$white = imagecolorallocate($im, 255, 255, 255);
	$grey = imagecolorallocate($im, 128, 128, 128);
	$grey1 = imagecolorallocate($im, 228, 228, 228);
	$black = imagecolorallocate($im, 0, 0, 0);


	// The text to draw
	// Replace path by your own font path
	$font = APPPATH."../assets/fonts/OpenSansBold.ttf";
	//$font = "/mnt/vol1/sites/gtec/assess/assets/fonts/OpenSansBold.ttf";
	// Add some shadow to the text

	$text_color = imagecolorallocate($im, 0, 0, 0);
	/* imagestring($im, 5, 140, 66, str_pad($name,50," ",STR_PAD_BOTH), $text_color);
	imagestring($im, 5, 140, 87, str_pad($grade_name,50," ",STR_PAD_BOTH), $text_color);
	imagestring($im, 5, 212, 105, str_pad($school_name,50," ",STR_PAD_BOTH), $text_color); */
	
	/* imagestring($im, 5, 320, 66,$name, $text_color);
	imagestring($im, 5, 320, 87,$grade_name, $text_color);
	imagestring($im, 5, 320, 105,$school_name, $text_color); */
	
/*	imagestring($im, 5,300, 75,"Name:", $black);
	imagestring($im, 5, 350, 75,$name, $text_color);
	imagestring($im, 5,292, 95,"Grade:", $black);
	imagestring($im, 5, 341, 95,str_replace('Grade','',$grade_name), $text_color); */
	
	imagestring($im, 3, 90, 68, $name, $text_color);
	imagestring($im, 3, 90, 87, str_replace("Grade ",'',$grade_name), $text_color);
	//imagestring($im, 3, 90, 106,'', $text_color);
	imagefilledrectangle($im, 0,106, 100, 120, $grey1);
	//imagestring($im, 2, 25, 106,'(Userid : '.$uname.')',$text_color);
	
	/* BSPIScore */ 
	$text_color = imagecolorallocate($im, 255, 255, 255);
	if (strlen($fBSPIScore) == 1)
    {
     imagettftext ($im, 25, 0, 195, 350, $text_color, $font, $fBSPIScore);
    }

	 elseif (strlen($fBSPIScore) > 2)
    {
     imagettftext ($im, 25, 0, 170, 350, $text_color, $font, $fBSPIScore);
    }
	
		elseif (strlen($fBSPIScore) > 3)
    {
     imagettftext ($im, 25, 0, 100, 350, $text_color, $font, $fBSPIScore);
    }
	else
    {
     imagettftext ($im, 25, 0, 180, 350, $text_color, $font, $fBSPIScore);
    }

/* TotalScore */ 
    if (strlen($fTotalScore) == 1)
    {
    imagettftext ($im, 50, 0, 570, 300, $text_color, $font, $fTotalScore);
    }
    elseif (strlen($fTotalScore) > 2)
    {
     imagettftext ($im, 50, 0, 530, 300, $text_color, $font, $fTotalScore);
    }
	else
    {
     imagettftext ($im, 50, 0, 550, 300, $text_color, $font, $fTotalScore);
    }
	
	
	
	/* MemoryScore */ 
	$text_color = imagecolorallocate($im, 255, 0, 0);
	
	if (strlen($fMemoryScore) == 1)
    {
      imagettftext ($im, 30, 0, 263, 510, $text_color, $font, $fMemoryScore);
    }
    elseif (strlen($fMemoryScore) > 2)
    {
     imagettftext ($im, 30, 0, 243, 510, $text_color, $font, $fMemoryScore);
    }
	else
    {
      imagettftext ($im,  30, 0, 253, 510, $text_color, $font, $fMemoryScore);
    }
	/* VisualProcessingScore */ 
	$text_color = imagecolorallocate($im, 255, 153, 51);
	
	if (strlen($fVisualProcessingScore) == 1)
	{
   
    imagettftext ($im, 30, 0, 373, 510, $text_color, $font, $fVisualProcessingScore);
   }
    elseif (strlen($fVisualProcessingScore) > 2)
    {
      imagettftext ($im, 30, 0, 353, 510, $text_color, $font, $fVisualProcessingScore);
    }
	else
    {
       imagettftext ($im, 30, 0, 363, 510, $text_color, $font, $fVisualProcessingScore);
    }
   /* FocusAndAttentionScore */ 
	$text_color = imagecolorallocate($im, 102, 204, 0);
	
	if (strlen($fFocusAndAttentionScore) == 1)
   {
    imagettftext ($im, 30, 0, 484, 510, $text_color, $font, $fFocusAndAttentionScore);
   }
    elseif (strlen($fFocusAndAttentionScore) > 2)
    {
      imagettftext ($im, 30, 0, 464, 510, $text_color, $font, $fFocusAndAttentionScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 474, 510, $text_color, $font, $fFocusAndAttentionScore);
    }
   /* ProblemSolvingScore */ 
	$text_color = imagecolorallocate($im, 230, 107, 25);
	
	if (strlen($fProblemSolvingScore) == 1)
  {
   imagettftext ($im, 30, 0, 594, 510, $text_color, $font, $fProblemSolvingScore);
   }
  elseif (strlen($fProblemSolvingScore) > 2)
    {
      imagettftext ($im,30, 0, 574, 510, $text_color, $font, $fProblemSolvingScore);
    }
	else
    {
       imagettftext ($im,30, 0, 584, 510, $text_color, $font, $fProblemSolvingScore);
    }
   /* LinguisticsScore */ 
	$text_color = imagecolorallocate($im, 51, 204, 255);
	
	if (strlen($fLinguisticsScore) == 1)
  {
  imagettftext ($im, 30, 0, 705, 510, $text_color, $font, $fLinguisticsScore);
  }
  elseif (strlen($fLinguisticsScore) > 2)
    {
      imagettftext ($im, 30, 0, 685, 510, $text_color, $font, $fLinguisticsScore);
    }
	else
    {
        imagettftext ($im, 30, 0, 695, 510, $text_color, $font, $fLinguisticsScore);
    }
	/* Greetings Range */
	$text_color = imagecolorallocate($im, 0, 0, 0);
	
  if ($fBSPIScore < 11)
  {
  imagestring($im, 5, 0, 85, str_pad($fGreeting1,150," ",STR_PAD_BOTH), $text_color);
   
  }
  elseif ($fBSPIScore < 21)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting2,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	elseif ($fBSPIScore < 31)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting3,150," ",STR_PAD_BOTH), $text_color);
	  
    }
	 elseif ($fBSPIScore < 41)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting4,150," ",STR_PAD_BOTH), $text_color);
	
    }
	 elseif ($fBSPIScore < 51)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting5,150," ",STR_PAD_BOTH), $text_color);
    }
	 elseif ($fBSPIScore < 61)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting6,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	 elseif ($fBSPIScore < 71)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting7,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	 elseif ($fBSPIScore < 81)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting8,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	elseif ($fBSPIScore < 91)
    {
      imagestring($im, 5, 0, 85, str_pad($fGreeting9,150," ",STR_PAD_BOTH), $text_color);
	 
    }
	else
    {
imagestring($im, 5, 0, 77, str_pad($fGreeting10,150," ",STR_PAD_BOTH), $text_color);

    }


	
    return $im;
	}

}
