<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
				//$this->lang->load("menu_lang","english");
				$this->load->model('Api_model');
			  
				//$this->lang->load("menu_lang","french");		
			
        }
		
	public function index()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	public function index1()
	{			
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
		
	}
	public function Login1()
	{
		$arrResult=array("response"=>"failure");
		{
			 
			$data['query'] = $this->Api_model->checkUser($_REQUEST['username'],$_REQUEST['password']);
			
			if(isset($data['query'][0]->id))
			{
				$arrResult=array("response"=>"success");
			}
			echo json_encode($arrResult);exit;
			
		}
	}
	public function Login()
	{
		$arrResult=array("response"=>"failure");
		{
			 //$_REQUEST['username'] = 'Aishu.C';
		//	 $_REQUEST['password'] = 123456;
		//	 $_REQUEST['deviceid'] = 'Device1283device';
			 
			  
			$data['query'] = $this->Api_model->checkUser($_REQUEST['username'],$_REQUEST['password'],$_REQUEST['platform'],$_REQUEST['model'],$_REQUEST['deviceid'],$_REQUEST['appversion']);
			
			$deviceid = $data['query'][0]->deviceid;
			
			if($deviceid=='')
			{
				$this->Api_model->insertdeviceid($data['query'][0]->id,$_REQUEST['deviceid']);	
				$success = 0;
			}
			else if($deviceid==$_REQUEST['deviceid'])
			{
				$success = 0;
			}
			else{
				$success = 1;
				
			}
			
			
			
			if(isset($data['query'][0]->id))
			{
				$arrResult=array("response"=>"success","deviceid_mismatch"=>$success,"usermasterid"=>$data['query'][0]->id,"firstname"=>$data['query'][0]->fname,"lastname"=>$data['query'][0]->lname,"plan"=>$data['query'][0]->planname,"grade"=>$data['query'][0]->gradename);
			}
			echo json_encode($arrResult);exit;
			
		}
	}
	
	public function getgamedatas()
	{
			//	$_REQUEST['userid'] = 7441;
			//    $_REQUEST['deviceid'] = '123457device1234';
				
				$data['newdeviceid'] = $this->Api_model->insertdeviceid($_REQUEST['userid'],$_REQUEST['deviceid']);	
				
				//echo $data['newdeviceid'];
				
				$data['query'] = $this->Api_model->getgamedatas($_REQUEST['userid']);
				
				// echo json_encode(array("syncid"=>$array1));exit;
				
				echo json_encode(array("usergamedatas"=>$data['query']));exit;
		
	}
	
	public function Sync()
	{
		
$datas = $_REQUEST['data'];
		 

$data1 =  json_decode($datas);

//print_r($data1); exit;

foreach($data1 as $res)
{
	//$arrayres[] = $res->id;
	//echo  $res->id;
	
	$data['getsyncid'] = $this->Api_model->getsyncid($res->id);
	
//	echo '<pre>';
//	print_r($data['getsyncid']); exit;
	$countid = $data['getsyncid'][0]['synid']; 
	
	if($countid!=0)
	{
	$data['gamedatas'] = $this->Api_model->gamedatasupdate($res->id,$res->userid,$res->gameid,$res->skillid,$res->gtime1,$res->aqcnt1,$res->rtime1,$res->cqcnt1,$res->crtime1,$res->gscore1,$res->tqcnt1,$res->wrtime1,$res->datetime,$res->sync_status);
	}
	else{
	$data['gamedatas'] = $this->Api_model->gamedatas($res->id,$res->userid,$res->gameid,$res->skillid,$res->gtime1,$res->aqcnt1,$res->rtime1,$res->cqcnt1,$res->crtime1,$res->gscore1,$res->tqcnt1,$res->wrtime1,$res->datetime,$res->sync_status);
	}
	
	//print_r($data['gamedatas']);
	
	$array1[] =array("id"=> $data['gamedatas']);
	//print_r($array1);
	
}
	 echo json_encode(array("syncid"=>$array1));exit;
	
	}
}