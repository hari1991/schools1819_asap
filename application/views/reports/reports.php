<style type="text/css">
@import url(//fonts.googleapis.com/css?family=Roboto+Condensed:400,700);
/* written by riliwan balogun http://www.facebook.com/riliwan.rabo*/
.board{
    width: 100%;
 
height: 500px;
background: #fff;
/*box-shadow: 10px 10px #ccc,-10px 20px #ddd;*/
}
.board .nav-tabs {
    position: relative;
    /* border-bottom: 0; */
    /* width: 80%; */
    margin: 40px auto;
    margin-bottom: 0;
    box-sizing: border-box;

}

.board > div.board-inner{
   //background:url('<?php echo base_url(); ?>assets/images/chart_patterns/bg1.png') repeat;
   background:#fafafa;
}

p.narrow{
    width: 60%;
    margin: 10px auto;
}

.liner{
    height: 2px;
    background: #ddd;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    
	
}

span.round-tabs{
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: white;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}

span.round-tabs.one{
    color: rgb(34, 194, 34);border: 2px solid rgb(34, 194, 34);
}

li.active span.round-tabs.one{
    background: #fff !important;
    border: 2px solid #ddd;
    color: rgb(34, 194, 34);
}

span.round-tabs.two{
    color: #febe29;border: 2px solid #febe29;
}

li.active span.round-tabs.two{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #febe29;
}

span.round-tabs.three{
    color: #3e5e9a;border: 2px solid #3e5e9a;
}

li.active span.round-tabs.three{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #3e5e9a;
}

span.round-tabs.four{
    color: #f1685e;border: 2px solid #f1685e;
}

li.active span.round-tabs.four{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #f1685e;
}

span.round-tabs.five{
    color: #999;border: 2px solid #999;
}

li.active span.round-tabs.five{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #999;
}

.nav-tabs > li.active > a span.round-tabs{
    background: #fafafa;
}

.nav-tabs > li {
   
}
/*li.active:before {
    content: " ";
    position: absolute;
    left: 45%;
    opacity:0;
    margin: 0 auto;
    bottom: -2px;
    border: 10px solid transparent;
    border-bottom-color: #fff;
    z-index: 1;
    transition:0.2s ease-in-out;
}*/
.nav-tabs > li:after {
    content: " ";
    position: absolute;
    left: 45%;
   opacity:0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #ddd;
    transition:0.1s ease-in-out;
    
}

.nav-tabs > li img {border-radius:15%;width:35%;}

.nav-tabs > li a{
   width: 100% !important;
   height: 100% !important;
   margin: 5px auto;
   padding: 0;
   text-align:center;
}

.nav-tabs > li a:hover{
    background: transparent;
	border-color:transparent;
}

.tab-content{
}
.tab-pane{
   position: relative;
 
}
.tab-content .head{
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 25px;
    text-transform: uppercase;
    padding-bottom: 10px;
}
.btn-outline-rounded{
    padding: 10px 40px;
    margin: 20px 0;
    border: 2px solid transparent;
    border-radius: 25px;
}

.btn.green{
    background-color:#5cb85c;
    /*border: 2px solid #5cb85c;*/
    color: #ffffff;
}



@media( max-width : 585px ){
    
.board {
width: 100%;
height:auto !important;
}
.nav-tabs>li{width:100%}
    span.round-tabs {
        font-size:16px;
width: 50px;
height: 50px;
line-height: 50px;
    }
    .tab-content .head{
        font-size:20px;
        }
    .nav-tabs > li a {
width: 50px;
height: 50px;
line-height:50px;
}
.nav-tabs > li img {width:18%;}

.nav-tabs > li.active:after {
content: " ";
position: absolute;
left: 35%;
}

.btn-outline-rounded {
    padding:12px 20px;
    }
}
.blink_me {
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 1500ms;
    -webkit-animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;

    -moz-animation-name: blinker;
    -moz-animation-duration: 1500ms;
    -moz-animation-timing-function: linear;
    -moz-animation-iteration-count: infinite;

    animation-name: blinker;
    animation-duration: 1500ms;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
}

@-moz-keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 20px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}

@-webkit-keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 20px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}

@keyframes blinker {  
    0% { opacity: 1.0;box-shadow: 0 0 3px }
    50% { opacity: 0.5;box-shadow: 0 0 20px }
    100% { opacity: 1.0;box-shadow: 0 0 3px }
}
#settingsh3,#mybspih3,#cperformh3{padding-bottom: 27px;}
#myTab h3{color:#ff6600;text-align:center;}
.nav-tabs>li{border-right:1px solid #ccc;}
#myTab{border: 1px solid #ccc;}
/* .nav>li:last-child>a{padding:22px 30px;} */
.nav>li:last-child{border-right:none;}
.activereport{background-color:#465247;}
</style>

<script type="text/javascript">
$(function(){
$('a[title]').tooltip();
});

$(document).ready(function()
{
setTimeout(function(){  bspichart(); }, 3000);

});





</script>
<section class="outerheight" style="background:#fff;min-height:900px;">
        <div class="container">
            <div class="row">
                <div class="board">
                    <!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
					
			<div class="board-inner">
			<ul class="nav nav-tabs" id="myTab">
				<div id="loadingimage1"  class="loading1"></div>
				<li class="col-sm-3 col-md-3 col-lg-3  bounceIn animated">
					<a href="javascript:;" id="home" class="reportLink" >
					<h3 style="text-align:center; color:orange;">Skill Comparison</h3>
					<img class="blink_me"  src="<?php echo base_url(); ?>assets/images/myreports1.png" /> 
					</a>
				</li>
				<li class="col-sm-3 col-md-3 col-lg-3  bounceIn animated">					
					<a href="javascript:;" id="profile" class="reportLink" >
						<h3 style="text-align:center; color:orange;">BSPI</h3>
						<img class="blink_me"  src="<?php echo base_url(); ?>assets/images/myreports2.png" />
					</a>
				</li>
				<li class="col-sm-3 col-md-3 col-lg-3  bounceIn animated">
					<a href="javascript:;" id="messages" class="reportLink" >
						<h3 style="text-align:center; color:orange;">Skill Performance</h3>
						<img class="blink_me"  src="<?php echo base_url(); ?>assets/images/myreports3.png" /> 
					</a>
				</li>
				<li class="col-sm-3 col-md-3 col-lg-3  bounceIn animated">
					<a href="javascript:;" id="settings" class="reportLink" >					
					<h3 style="text-align:center; color:orange;">Class Performance</h3>
						<img class="blink_me" src="<?php echo base_url(); ?>assets/images/myreports4New.png" /> 
					</a>
				</li>
                                      
                     </ul></div>

                 
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pattern-fill.js"></script>

<script>
function getReport(reportname)
{
		$('#loadingimage1').css("display", "block");
		/* Check User Login */
		LoginAjaxCall();
		/* Check User Login */
		$(".reportdiv").css("display","none");
		$("#d"+reportname).css("display","block");
		 multiplechart();
		 bspichart();
		 skillwisechart();
		 //	bubblechart1('containerone'); 
		 leaderboard();
		 //alert("reportname"+reportname);
		 var timer = setInterval(function(){
			if($("#dsettings").is(':visible'))
			{
				leaderboard();
			}
			 
			 }, 30000);

		 if(reportname=='home' || reportname=='profile' || reportname=='messages')
		 {
			//alert(timer);
			clearInterval(timer);
		 }
		 

		 
			$(".bounceIn").removeClass('activereport'); 
			$("#"+reportname).parent().addClass('activereport'); 
	
	
}





$(document).ready(function()
{
	
	$(".reportLink").click(function(){
		getReport($(this).attr('id'));
	});
	getReport("home");
});

//setInterval(function(){leaderboard();}, 30000);

/* $("#settings").click(function(){
		setInterval(function(){leaderboard();}, 30000);
	}); */
	
	/* if($("#bchart").is(':visible'))
        {
			setInterval(function(){leaderboard();}, 30000);
		} */


 function multiplechart()
 {
	  
	 
	 var chart1 = Highcharts.chart('container1', {

    chart: {
        type: 'column',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: false},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: ['Memory', 'Visual Processing', 'Focus and Attention', 'Problem Solving', 'Linguistics'],
		 gridLineWidth: 0,
  minorGridLineWidth: 0 ,
	labels: {
            style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        }
       
    },
  plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true,
			style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
        }
            }
        },
    yAxis: {
        allowDecimals: false,
		  
        title: {
            text: 'Score',
			style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        },
		 max: 100 ,
    gridLineWidth: 0,
  minorGridLineWidth: 0,
  labels: {
            style: {
                fontSize: '25px',
				color: '#FF6600',
				fontFamily: 'Phenomena-Regular'
            }
        }
		
    },
	
legend:{
    useHTML: true,
	symbolWidth: 0,
    labelFormatter: function() {
		var img;
    var name = this.name;
	if(name=="Puzzle set - 2"){
     img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/p2.png" width = "20px" height = "20px">';
	}
	
    return img + '  ' + name;
  }
},

    series: [{
		showInLegend: false, 
        name: 'Puzzle set - 2', 
        data: [{y:<?php echo $bspicalendarskillScore['SID59']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}, {y:<?php echo $bspicalendarskillScore['SID60']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}, {y:<?php echo $bspicalendarskillScore['SID61']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }},{y:<?php echo $bspicalendarskillScore['SID62']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}, {y:<?php echo $bspicalendarskillScore['SID63']; ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/p2.png', width: 40, height: 40 }}]
	}] 
});
$('#loadingimage1').css("display", "none");
 }
	 
	 
	 
 
</script>
 <div class="tab-content" style="height:100%">
  <div class="row reportdiv" id="dhome">
   
                       
	
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <div><h2 id="MonthID1"><span class="col-lg-10">Skill comparison</span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>


 <div id="container1" style="background:#fff;padding-top:20px;border: 1px solid #ccc;" >
</div>
 
  
 </div> 
 </div>
 
<script>
 function bspichart()
 {
	 
	  
	
	 var chart2 = Highcharts.chart('container2', {

    chart: {
        type: 'column',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: true},exporting:false,credits: {
      enabled: false
  },
	
			
	

    

    xAxis: {
        categories: ['BSPI'],
		gridLineWidth: 0,
  minorGridLineWidth: 0,
       labels: {
            style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        }
    },
  plotOptions: {
			  
            column: {
                depth: 50,
				dataLabels: {
            enabled: true,
			style: {
                fontSize: '15px',
				color: '#0c315b',
				fontFamily: 'Phenomena-Regular'
            }
        }
            }
        },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Score',
       
            style: {
                fontSize: '25px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        
        },
       labels: {
            style: {
                fontSize: '25px',
				color: '#FF6600',
				fontFamily: 'Phenomena-Regular'
            }
        },
		 max: 100 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0
		
    },
	

legend:{
    useHTML: true,
	symbolWidth: 0,
    labelFormatter: function() {
		var img;
    var name = this.name;
	
	if(name=="BSPI")
	{
		img= '<img src = "<?php echo base_url(); ?>assets/images/chart_patterns/a.png" width = "20px" height = "20px">';
	}
	
    return img + '  ' + name;
  }
},
    series: [{
		
		showInLegend: false, 
        name: 'BSPI',
		 
        data: [{y:<?php echo round($userbspiscore[0]['bspi'], 2); ?>,color: {pattern:   '<?php echo base_url(); ?>assets/images/chart_patterns/a.png', width: 20, height: 20 }}]
	}] 
});
$('#loadingimage1').css("display", "none");
 }
	 
	 
	 
 
</script>  
<div class="row reportdiv" id="dprofile">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12">
 <div><h2 id="MonthID1"><span class="col-lg-10">BSPI</span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>
 <div id="container2" style="background:#fff;padding-top:20px;border: 1px solid #ccc;">
</div>
 </div>
 </div>
                      
 <?php //print_r($skillwiseaverage); 

 $totalsets;

/* $memory = round($set1avg_M, 2);
$visual = round($set1avg_V, 2);
$focus = round($set1avg_F, 2);
$problem = round($set1avg_P, 2);
$linguistics = round($set1avg_L, 2); */

$memory = $bspicalendarskillScore['SID59'];
$visual = $bspicalendarskillScore['SID60'];
$focus = $bspicalendarskillScore['SID61'];
$problem = $bspicalendarskillScore['SID62'];
$linguistics = $bspicalendarskillScore['SID63'];

$test = array("Memory"=>$memory, "Visual Processing"=>$visual, "Focus and Attention"=>$focus, "Problem Solving"=>$problem, "Linguistics"=>$linguistics);
arsort($test);
//print_r($test);
?>
 


<script>
$(document).ready(function()
{
	
});
 function skillwisechart()
 {
	 
	 
	
	 var chart3 = Highcharts.chart('container3', {

    chart: {
        type: 'bar',
		backgroundColor:'transparent'
	
	 
    },

    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },
	tooltip: {enabled: true},exporting:false,credits: {
      enabled: false
  },

    xAxis: {
        categories: [<?php foreach($test as $key=>$value){ 
	 echo "'".$key."',";
		}
	?> ],
		gridLineWidth: 0,
  minorGridLineWidth: 0,
  labels: {
            style: {
                fontSize: '25px',
				color: '#FF6600',
				fontFamily: 'Phenomena-Regular'
            }
        }
       
    },
  
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Score',style: {fontSize: '25px',color: '#000',fontFamily: 'Phenomena-Regular'}
        },
		 max: 100 ,
  gridLineWidth: 0,
  minorGridLineWidth: 0,
  labels: {
            style: {
                fontSize: '20px',
				color: '#000',
				fontFamily: 'Phenomena-Regular'
            }
        }
		
    },
	
 plotOptions: {
	  
    bar: {
        dataLabels: {
            enabled: true,
			formatter: function() {
 
    var Greeting0='Needs Attention !!!';
    var Greeting1='Needs Improvement !!!';
    var Greeting2='Can Do Better !!!';
    var Greeting3='Average !!!';
    var Greeting4='Fair !!!';
    var Greeting5='Good !!!';
    var Greeting6='Very Good !!!';
    var Greeting7='Excellent !!!';
    var Greeting8='Extraordinary !!!';
    var Greeting9='Genius !!!';
    var Greeting10='Prodigy !!!';

    if(this.y<1){ return Greeting0;}
    else if(this.y<11){ return Greeting1;}
    else if(this.y<21){ return Greeting2;}
    else if(this.y<31){ return Greeting3;}
    else if(this.y<41){ return Greeting4;}
    else if(this.y<51){ return Greeting5;}
    else if(this.y<61){ return Greeting6;}
    else if(this.y<71){ return Greeting7;}
    else if(this.y<81){ return Greeting8;}
    else if(this.y<91){ return Greeting9;}
    else if(this.y<101){ return Greeting10;}
   },
                        inside: true,
			style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
             
        }
    }
},	

   
    series: [
	
	{
		showInLegend: false,
        name:"Skill Wise Average",
        data: [<?php foreach($test as $key=>$value){  if($key=='Memory'){$color='#da0404';}
	else if($key=='Visual Processing'){$color='#ffc000';}
	else if($key=='Focus and Attention'){$color='#92d050';}
	else if($key=='Problem Solving'){$color='#ff6600';}
	else if($key=='Linguistics'){$color='#00b0f0';} echo'{y:'.$value.',color:"'.$color.'"},'; }?>]
	}
	 ] 

});
$('#loadingimage1').css("display", "none");
 }
	 
	 
	 
 
</script>
 
<div class="row reportdiv" id="dmessages">
<div class="col-12 col-md-12 col-sm-12 col-xs-12">
<div><h2 id="MonthID1"><span class="col-lg-10">Skill Wise Report</span>  <span class="col-lg-2" style="text-decoration: none;float: none;"></span></h2></div>
<div id="container3"  style="background:#fff;padding-top:20px;border: 1px solid #ccc;" >
</div>
</div>
</div>
   
<script src="<?php echo base_url(); ?>assets/js/highcharts-more.js"></script>
<script type="text/javascript">

function leaderboard()
{
	$('#loadingimage1').show();
	$.ajax({
			type:"POST",
			data:{},
			url:"<?php echo base_url('index.php/reports/bubblechart') ?>",
			success:function(result)
			{		
			//alert(result);
			/* if(result==1)
			{
				$('[data-id='+id+']').remove();
				$('#myModalapprove').modal('hide');
				window.location.href="<?php echo base_url('index.php/home/license') ?>";
			} */
				$('#loadingimage1').hide();
				$('#container_bubble').html(result);
			
			}
		}); 	
	
}
/*
function bubblechart1(renderelement)
{
	
	var chart4 = new Highcharts.chart({

    chart: {
		 renderTo: renderelement,
        type: 'scatter',
         
		
		  events: {
            load: function(){
			this.tooltip.refresh(this.series[0].data[<?php echo array_search($cus1['bspi'],$su1); ?>]);
                // show tooltip on 4th point
                  
            }
        },
		backgroundColor:'transparent'
    },
exporting:false,credits: {
      enabled: false
  },
    title: {
        text: ''
    },

    xAxis: {
         
		gridLineWidth: 0,
  lineWidth: 0,
   minorGridLineWidth: 0,
   lineColor: 'transparent',
		labels:
{
  enabled: false,style: {fontSize: '15px',color: '#0c315b',fontFamily: 'Phenomena-Regular'}
}
    },

    yAxis: {
		labels: {
            style: {
                fontSize: '20px',
				color: '#fff',
				fontFamily: 'Phenomena-Regular'
            }
        },
		title: {text: 'Score',style: {fontSize: '25px',color: '#ccc',fontFamily: 'Phenomena-Regular'}},
		min: 0, max: 100,
		gridLineWidth: 0,
  minorGridLineWidth: 0,
        startOnTick: false,
        endOnTick: false
    },
tooltip: {
	useHTML: true,
   backgroundColor: 'rgba(0, 0, 0, 0.16)',
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    formatter: function() {
        //return 'The value for <b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
		return '<div style="text-align:center;color:white;" ><img width="40px" height="40px" src="'+this.point.myData+'" /><br/><b>Name : '+ this.point.name +'</b><br/><b>Score : '+ this.y +'</b></div>';
    }
},
 plotOptions: {
    scatter: {
        dataLabels: {
            enabled: true,
            formatter: function() {
                return '';
            }
        }
    }
},
	
    series: [{showInLegend: false, 
        data: [
	 
		<?php 
		
		foreach($GradeWiseReport1 as $datascore)
		{
			$myavatarimage=$datascore['avatarimage'];
			if($datascore['avatarimage']==""){$myavatarimage="assets/images/avatar.png";} 
  if(file_exists(base_url()."".$datascore['avatarimage'])){$myavatarimage="assets/images/avatar.png";} 
  
  
			if($datascore['id']!=$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).",myData : '".base_url().$myavatarimage."',marker:{symbol: 'url(".base_url()."assets/images/starsmall.png)' } },";
			}
			else if($datascore['id']==$this->session->user_id)
			{
			echo "{name: '".$datascore['name']."',y:".round($datascore['bspi'],2).", myData : '".base_url().$myavatarimage."',marker:{ symbol: 'url(".base_url()."assets/images/starani2.gif)',radius:5} },";
			}
		}
		?>
            
        ] 
    }]

});
$('#loadingimage1').css("display", "none");	
} */
</script>
<div class="row reportdiv"  id="dsettings"> 
                      
 <div class="row">
 <div class="col-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;font-weight:bold;padding-top: 5px;"><img src="<?php echo base_url()."assets/images/starani2.gif"; ?>" />  <span style="font-size:22px;"> - <?php echo $this->session->fname; ?></span></div>
 </div>
<div id="bchart">
  <div class="col-12 col-md-12 col-sm-12 col-xs-12" id="container_bubble">
 </div> </div>
 </div>
<div class="clearfix"></div>
</div>

</div>
</div>
</div>
</section>
               
<style>
.reportdiv{display:none;}
iframe body{overflow: hidden;}
.section_three{	
height: inherit;}
.highcharts-legend-item .highcharts-point{display:none;}

.nice-select span.current{font-size: 20px}
 .nice-select .option {font-size: 10px}
 .nice-select ul{height:200px;overflow-y:scroll !important}

</style>			   