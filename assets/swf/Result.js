(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(239,255,255,0.859)","rgba(137,254,253,0)"],[0,0.416,1],0,0,0,0,0,573.2).s().p("EgJoBYmMAC8g6LMgOxA4OQm/hqmiiyMATeg2DMgdsAw1QlxjelWkcMAg3gs/MgohAl4QkmkujxlDMAqXghyMgviAZ8QjFlQiRllMAwKgV7Mgy8AN4Qhlleg4lxMAywgKLMgzlACTQgNjLAAjQQAAiYAHiWMAzZABEMgyvgIwQAulnBZlWMAyqAMMMgwTgTvQB+lYCslFMAvoAXUMgrTgekQDSk6ECknMAqCAidMgjlgpLQEukfFEjqMAhVAtLMgYhgyxQF+jTGaiSMAUrA14MgJng49QG2haHRgXMAEmA6ZMAHeg6RQHWAhG6BoMgMvA5lMAYQg1/QGiCkGFDrMgciAzIMAl1gsoQEQDZEBEAQA0A1AzA1MgokAopMAvTgg0QD5FEDDFbMgwoAcnMA0wgUMQCUFfBhF0Mg0+AQKMA2ugHtQA4FmAMF3MguNADaMAuPACQQgIFygyFhMg2VgGOMA00AOTQhWFniEFTMg0WgR9MAw2AZvQirFOjdE6MgvkgdbMAp6AkqQiWCuioCoQh2B2h6BuMgn/gojMAgKAu5QlcD8l1C/Mgc9gycMASuA21QmiCSm/BOMgODg4vMACTA6BQiUAHiXAAQk6AAkvgeg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-570.3,-570,1140.7,1140);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(5,1,1).p("Eg0TgClQAAAHAAAIIAAEtQAADPCSCTQCTCSDPAAMBY/AAAQDPAACTiSQCSiTAAjPIAAktQAAgIAAgHQgFjFiNiOQiTiSjPAAMhY/AAAQjPAAiTCSQiNCOgFDFg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EgsfAGYQjPAAiTiSQiSiTAAjOIAAkuIAAgOMBonAAAIAAAOIAAEuQAADOiSCTQiTCSjPAAg");
	this.shape_1.setTransform(0,24.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF3300").s().p("Eg0TADzQAEjGCOiMQCTiTDOAAMBZAAAAQDPAACSCTQCOCMAFDGg");
	this.shape_2.setTransform(0,-40.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-337.3,-67.6,674.7,135.2), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#99FFFF").s().p("EApqArMMhTTAAAQmKAAkWkXQkWkVAAmKMAAAg4rQAAmJEWkXIAAAAQEWkWGKAAMBTTAAAQGKAAEWEWQEWEWAAGKMAAAA4rQAAGJkWEWIgBAAQkVEXmKAAIAAAAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EgppAr+QmfAAkkklQklklAAmeMAAAg4rIAAgLQAEmXEhkhQEkklGfAAMBTTAAAQGeAAElElQEhEhAEGXIAAALMAAAA4rQAAGeklElQklElmeAAgEg0Jgm1IAAAAQkWEXAAGJMAAAA4rQAAGKEWEVQEWEXGKAAMBTTAAAQGKAAEVkXIABAAQEWkWAAmJMAAAg4rQAAmKkWkWQkWkWmKAAMhTTAAAQmKAAkWEWg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-366.6,-281.3,733.2,562.8), null);


(lib.Silver_trophy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// shine
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(102,102,102,0.502)").s().p("AA1g/Qg4AogZBUQgFACgFgBQgFACgJAAQAZhYBQgng");
	this.shape.setTransform(-22.6,-34.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.502)").s().p("AAiA7QgEACgEgDQgchOgugpQBFApAcBSQgMAAgDgDg");
	this.shape_1.setTransform(5.1,-34.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E6E6E6").s().p("AgYBYQhBh+gFiwQAJAGAUAGQAFClA8CEQAoBbA3AcQhEgagzhkg");
	this.shape_2.setTransform(-23.6,-5.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAABgQA6iEAFilQAUgGAKgGQgFCwhDB+QgwBghBAdQA0geAohYgAgcAaQArhcAGh+IAIgCIAMgCQgMBtgoBVIgQAdg");
	this.shape_3.setTransform(5.9,-5.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#D9D9D9").s().p("AASBSQgohUgLhtIALACIAJACQAFB9AqBdIAAABIgQgeg");
	this.shape_4.setTransform(-24.5,-13.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// top
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.rf(["#CCCCCC","#666666"],[0,0.839],8.5,9,0,8.5,9,18).s().p("Ai0ALIgQgDIgZgFQgSgFgIgFIgCgBIgEgFQgEgJAKAAQAJAFARAFIAZAGIAQACQBLAMBqAAQBqAABKgMIASgCIAZgGQAPgFAIgFQALAEgEAGIgFAFQgIAFgRAEIgZAGIgSADQhKAMhqAAQhqAAhLgMg");
	this.shape_5.setTransform(-9,-26.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.522,0.839],1.1,-2,0,1.1,-2,4.2).s().p("AgNAeIgJgGQgHgEADgOQADgNAIgOQAHgOAJAAQAJAAAHAOQAHAOADAPQACAPgGADIgJAEQgGAGgIAAQgHAAgGgGg");
	this.shape_6.setTransform(-8.9,-48.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.522,0.839],4.7,1.5,0,4.7,1.5,22.3).s().p("Ai0AeIgQgCIgYgGQgSgFgIgFQAmgcAGgaIAEAEIABABQAHAFAOAEIATAEIAOACQA7AKBVAAQBVAAA7gKIAPgCIAUgFQANgEAGgFIAEgDQADAeApAXQgIAFgPAFIgYAGIgSACQhLANhqAAQhqAAhLgNg");
	this.shape_7.setTransform(-9,-30);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.522,0.839],-9,0.4,0,-9,0.4,24.7).s().p("AiPAnIgOgCIgTgEQgOgFgHgEIgBgBIgEgEQAXgvAxgUQAAADADADIACAAQAEADAJACIANADIAIACQAmAGA2AAQA2AAAmgGIAKgCIAMgDIANgFQADgCABgCQAvAXAYArIgEADQgGAFgNAEIgUAFIgPACQg7AKhVAAQhVAAg7gKg");
	this.shape_8.setTransform(-9,-36.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.522,0.839],3.1,-2.8,0,3.1,-2.8,15.3).s().p("AhbAdIgJgCIgNgCQgIgDgFgDIgBAAQgDgCgBgEIAPgKQAzgiA1gDQAFAGAIAAQAIAAAGgGQAxAEAzAhIARAMQgBACgEACIgMAGIgNACIgKACQglAGg3AAQg1AAgmgGg");
	this.shape_9.setTransform(-8.9,-42.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5}]}).wait(1));

	// handle
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FAFAFA","#CCCCCC","#999999"],[0,0.816,1],-11.7,-2.1,0,-11.9,-2.1,23.5).s().p("AAmDAQgRgPAAgXQAAgIADgIIACAAQAAgFADgFIAFgFQAIgGALAAQAMAAAIAGQAIAGAAAJQAAAJgIAGQgIAHgMAAIgIgBQACAJAHAHQAJAIAMAAQAMAAAJgIQAJgIAAgLIgBgIQgHgigrgUIgQgIQgqgUgogZQgpgYgbgmQgcgrABgyQABgxApgcQAqgcAxAVQArASAPAtIACARIAAAEIgBANQgGAagTgJIABgKQAAgdgUgVQgSgUgcAAQgcAAgUAUQgTAVAAAdQAAASARAmQASAnBEAkQBRAlAkAWIAAAAIADAHIAMAXIABACIAAAAIAEAGIAAACQAEAIAAAKQAAAXgSAPQgRAQgZAAQgYAAgSgQg");
	this.shape_10.setTransform(-38.6,-3.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FAFAFA","#CCCCCC","#999999"],[0,0.816,1],9.4,-4.3,0,9.4,-4.3,16.7).s().p("Ah5DAQgSgPAAgXQAAgLAEgJIADgGIABAAIABgCIAMgXIADgHQAkgWBRglQBEgkASgnQARgmAAgSQAAgdgTgVQgUgUgcAAQgcAAgSAUQgUAVAAAdIABAKQgTAJgGgaIgBgNIAAgEIACgRQAPgtArgSQAxgVAqAcQApAcABAxQABAygcArQgbAmgpAYQgoAZgqAUIgQAIQgrAUgHAiIgBAIQAAALAJAIQAJAIAMAAQAMAAAJgIQAHgHACgJIgIABQgMAAgIgHQgIgGAAgJQAAgJAIgGQAIgGAMAAQALAAAIAGIAFAFQADAFAAAFIACAAQADAIAAAIQAAAXgRAPQgSAQgYAAQgZAAgRgQg");
	this.shape_11.setTransform(20.6,-3.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10}]}).wait(1));

	// Layer 6
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.569,0.902],0,0,0,0,0,9.5).s().p("AiMBOQBZgdAGiOIABAAQAPAIATAAIAXAAQATAAAOgIQAGCOBZAdQhDAQhHAAQhFAAhKgQg");
	this.shape_12.setTransform(-8.7,37.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.569,0.902],0,0,0,0,0,4.1).s().p("AglgKQARAFASAAQAUAAATgGIAAAXQgKgEgOAAIgXAAQgOAAgNAEg");
	this.shape_13.setTransform(-8.7,22.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer 7
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],18.4,-20.8,0,18.4,-20.8,73.9).s().p("AhnBJQgrheAAiHIAAgLQBCAJBVAAQBPAAA/gIIAAAKQAACHgrBeQgrBfg9AAQg8AAgrhfg");
	this.shape_14.setTransform(-9.5,-7.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(1));

	// base
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.396,0.839],0.1,-0.6,0,0.1,-0.6,5.5).s().p("AiNAfQgVAAgOgKQgOgJAAgNIAAgBQAAgOAOgKQAOgJAVAAIACAAQCUAgCFggQAVAAAOAJQAOAKAAAOIAAABQAAANgOAJQgOAKgVAAQhJAFhGAAQhHAAhFgFg");
	this.shape_15.setTransform(-8.9,48.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.569,0.902],7.6,-4.7,0,7.6,-4.7,44.8).s().p("AgiD0QhGgTg3heIAAgBIgEgGIAAgBIgBgCIgMgXIgDgGIgBAAQhCiNgDjFQACAEAHADQAJAGATAFIAYAGIALACIAJABQBHAMBhAAQBiAABIgMIAIgBIAMgCIAWgGQAUgFAKgGQAGgEACgEQgBA4gGA0IgCAQQgQB4gtBfIgEAGIgLAXIgCACIAAABIgDAGQg1BchDAUQgUAGgUAAQgRAAgRgEgAB0AIIAEgHIAAAAIgEAHgAh4ABIAAAAIABABIgBgCg");
	this.shape_16.setTransform(-9,-2.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.396,0.839],0,0,0,0,0,6).s().p("AgLAbQgTAAgPgIIgBgBIAAAAQgNgHAAgLIAAAAQAAgKAOgIIAHgDQANgFAOAAIAXAAQAOAAAKAEQAGABADADQAPAIAAAKIAAAAQAAALgPAIQgOAIgTAAg");
	this.shape_17.setTransform(-8.6,26.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.396,0.839],0,0,0,0,0,22.6).s().p("AiaAaIgOgDIgVgFQgPgEgIgFIgBgBQgGgDAAgFQAAgIAcgJQAOgEAXgEQBAgKBaAAQBbAABAAKQAYAFAPAEQANAEAGAEQAHAEAAAEQAAAFgHAEQgHAEgOAFIgVAFIgQADQhAAKhbAAQhaAAhAgKg");
	this.shape_18.setTransform(-9,-28.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#FFFFFF","#CCCCCC","#999999"],[0,0.396,0.839],0,0,0,0,0,25.5).s().p("AioAhIgIgCIgMgBIgXgGQgTgFgKgGQgGgEgDgEIgBgFIABgEQAJgOAxgKIAPgDQBJgNBnAAQBoAABLANIAQADQAkAIANAJQAFAEACAEIAAAHQgCAFgGAEQgKAGgUAFIgXAGIgLABIgIACQhIAMhjAAQhhAAhHgMgAiagdQgWAEgPAEQgbAJAAAJQgBAEAGADIABABQAIAFAPAEIAVAFIAOADQBAAKBaAAQBbAABAgKIAQgDIAVgFQAOgFAIgEQAGgEAAgEQAAgFgGgEQgHgEgMgEQgQgEgYgFQhAgKhbAAQhaAAhAAKg");
	this.shape_19.setTransform(-9,-27.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.5,-52.5,87.1,105);


(lib.sec = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.txt = new cjs.Text("", "bold 35px 'Veggieburger'", "#FFFFFF");
	this.txt.name = "txt";
	this.txt.textAlign = "center";
	this.txt.lineHeight = 31;
	this.txt.lineWidth = 75;
	this.txt.parent = this;
	this.txt.setTransform(78.1,-19.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC3300").s().p("AgUA0QgLgEgIgHIgDgEIgBgFIABgFIAEgEIAEgDIAGgBIAFABIAFADQADADAGACQAHADAFAAQAGAAADgCQADgDAAgDIgBgDQgBgDgFgCQgEgDgKgDQgQgDgJgIQgJgJAAgMQAAgKAGgHQAFgHAIgDQAJgEAKAAQAKAAAIACQAIACAIAEQAEABABAEQACACAAAEIAAADIgBADQgCAEgEABQgDACgEAAIgDAAIgEgCIgKgEIgIgBQgFAAgDACQgDABAAADQAAAEAEABIAHADQAWAFALAKQALAIAAAPQgBAJgFAIQgFAHgJAEQgJAEgMAAQgLAAgMgDg");
	this.shape.setTransform(9.7,-2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC3300").s().p("AgDBHQgMgGgFgLQgGgMABgOIAAgfIgHAAQgHgBgDgDQgFgEAAgGQAAgFAFgEQADgDAHAAIAHAAIAAgiQAAgGADgEQAEgDAHAAQAGAAAEADQADAEAAAGIAAAiIAdAAQAFAAAFADQAEAEAAAFQAAAGgEAEQgFADgFABIgdAAIAAAfQAAAMAFAGQAGAFAMAAIAEAAIACAAIABAAIABgBQAHABAEADQAEAEAAAGQAAAHgHADQgFADgMAAQgRAAgKgGg");
	this.shape_1.setTransform(-1.3,-4.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC3300").s().p("Ag5BJQgEgEAAgGIAAhvQAAgDACgDQACgEAEgBQARgIAQgFQAQgEANAAQAOABAMAEQAMAEAHAJQAIAKAAANQAAAQgJAMQgKAKgPAJQgOAJgQAEQgQAGgPABIAAAfQAAAGgEAEQgEADgGABQgGgBgEgDgAgJgvQgLACgNAFIAAAwIATgFQALgDAKgFQALgHAIgHQAHgHABgJQAAgGgEgDQgFgDgFgCIgLAAQgIgBgKADg");
	this.shape_2.setTransform(-13.9,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC3300").s().p("ABGAzQgEgEAAgFIAAgsQAAgMgEgGQgEgFgEgCIgIgBQgIAAgJAGQgJAGgGALIAAAvQAAAFgEAEQgEAEgGAAQgFAAgEgEQgEgEAAgFIAAgsQAAgMgEgGQgEgFgEgCIgJgBQgFAAgGADQgGACgFAFQgFAGgEAHIAAAvQAAAFgEAEQgEAEgGAAQgGAAgEgEQgEgEgBgFIAAhTQABgFAEgEQAEgDAGgBQAGABAEADQAEAEAAAFIAAABQAGgGAIgEQAIgEAJAAQAOAAALAGQAJAFAGAJQAHgJAKgFQAKgFANgBQAXABANANQAMAOABAYIAAAsQgBAFgEAEQgEAEgGAAQgGAAgEgEg");
	this.shape_3.setTransform(-32.7,-2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC3300").s().p("AgnAyQgMgFgHgJQgHgKAAgOQAAgPAJgMQAJgNAOgIQAOgJARgFQAPgEAPAAQAKAAAIADQAJADAFAFQAGAGAAAKQAAALgIAJQgHAHgMAFQgNAFgOADQgMADgNABIgXABQAEAHAHADQAHADAIAAQAJAAALgEQANgEAQgHQADgCADAAQAGABAEADQAEAEAAAFQAAAEgCADQgCADgDACQgSAJgQAEQgQADgNAAQgPAAgNgFgAAGgaQgLADgKAGQgKAGgGAIIAXgCIAUgGQALgDAGgEQAGgEABgEQAAAAAAAAQgBgBAAAAQAAAAAAgBQgBAAAAAAIgGgBIAAAAQgLAAgLADg");
	this.shape_4.setTransform(-51.4,-2.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC3300").s().p("AgDBHQgLgGgGgLQgGgMAAgOIAAgfIgGAAQgHgBgDgDQgFgEAAgGQAAgFAFgEQADgDAHAAIAGAAIAAgiQABgGAEgEQADgDAHAAQAGAAAEADQADAEAAAGIAAAiIAdAAQAGAAAEADQAEAEAAAFQAAAGgEAEQgEADgGABIgdAAIAAAfQAAAMAFAGQAGAFAMAAIAEAAIADAAIAAAAIABgBQAHABAEADQAEAEAAAGQgBAHgGADQgFADgMAAQgRAAgKgGg");
	this.shape_5.setTransform(-64,-4.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC3300").s().p("AgDBHQgMgGgFgLQgGgMABgOIAAgfIgIAAQgFgBgEgDQgEgEgBgGQABgFAEgEQAEgDAFAAIAIAAIAAgiQAAgGADgEQAFgDAGAAQAGAAAEADQADAEAAAGIAAAiIAcAAQAHAAADADQAFAEAAAFQAAAGgFAEQgDADgHABIgcAAIAAAfQAAAMAGAGQAFAFAMAAIAEAAIACAAIABAAIABgBQAHABAEADQAEAEAAAGQAAAHgHADQgFADgMAAQgRAAgKgGg");
	this.shape_6.setTransform(-74.9,-4.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC3300").s().p("AA5BQQgEgCgCgEIgOgfIhIAAIgPAfQgCAEgEACQgEACgEAAQgHAAgEgEQgFgEAAgGIABgDIABgDIBAiGQACgFAEgCQAEgCAEAAQAFAAAEACQAEACACAEIBBCHIAAADIABADQAAAGgFAEQgEAEgHAAQgEAAgEgCgAAYARIgYgyIgXAyIAvAAg");
	this.shape_7.setTransform(-88.3,-4.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#7C562C").ss(2,1,1).p("AmPiIQgmAxAABAIAAAvQAABAAmAxQAMAPAOANQBBA7BaAAIG0AAQBbAABAg7QBBg6AAhTIAAgvQAAhThBg6QhAg7hbAAIm0AAQhaAAhBA7QgOANgMAPg");
	this.shape_8.setTransform(77.5,-0.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#422E17","#422E17","#A5723B"],[0,0.071,1],0.2,-19.6,0,19.7).s().p("AjZDgQhbAAhBg7QgOgNgLgPQgngxAAhAIAAgvQAAhAAngxQALgPAOgNQBBg7BbAAIGzAAQBbAABBA7QBAA6AABTIAAAvQAABThAA6QhBA7hbAAg");
	this.shape_9.setTransform(77.5,-0.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#A5723B").ss(2,1,1).p("AynirQgvA+AABQIAAA7QAABQAvA9QAPATASAQQBRBKBxAAIeJAAQBxAABRhKQBQhJAAhnIAAg7QAAhnhQhKQhRhJhxAAI+JAAQhxAAhRBJQgSARgPASg");
	this.shape_10.setTransform(11.3,-0.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FF6600","#FF9900","#FFCC00"],[0,0.071,1],0,29,0,-28.9).s().p("AvEEYQhxAAhRhKQgSgQgPgTQgvg9AAhQIAAg7QAAhQAvg+QAPgSASgRQBRhJBxAAIeJAAQBxAABQBJQBRBKAABnIAAA7QAABnhRBJQhQBKhxAAg");
	this.shape_11.setTransform(11.3,-0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.txt}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.sec, new cjs.Rectangle(-114.8,-29.9,251,58), null);


(lib.score = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.txt = new cjs.Text("", "bold 35px 'Veggieburger'", "#FFFFFF");
	this.txt.name = "txt";
	this.txt.textAlign = "center";
	this.txt.lineHeight = 31;
	this.txt.lineWidth = 75;
	this.txt.parent = this;
	this.txt.setTransform(68.8,-18.5);

	this.txt_1 = new cjs.Text("Correct", "bold 25px 'Veggieburger'", "#CC3300");
	this.txt_1.name = "txt_1";
	this.txt_1.textAlign = "center";
	this.txt_1.lineHeight = 20;
	this.txt_1.lineWidth = 132;
	this.txt_1.parent = this;
	this.txt_1.setTransform(-47.7,-17.1,1.096,1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7C562C").ss(2,1,1).p("ADajfImzAAQhbAAhAA7QgPANgLAPQgnAxAABAIAAAvQAABAAnAxQALAPAPANQBAA7BbAAIGzAAQBaAABBg7QBBg6AAhTIAAgvQAAhThBg6QhBg7haAAg");
	this.shape.setTransform(70,-0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#422E17","#422E17","#A5723B"],[0,0.071,1],0.2,-19.6,0,19.7).s().p("AjZDgQhbAAhAg7QgPgNgLgPQgmgxAAhAIAAgvQAAhAAmgxQALgPAPgNQBAg7BbAAIGzAAQBaAABBA7QBBA6AABTIAAAvQAABThBA6QhBA7haAAg");
	this.shape_1.setTransform(70,-0.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#A5723B").ss(2,1,1).p("AyEjOQgSARgPASQgvA+AABQIAAA7QAABQAvA9QAPATASAQQBQBKByAAIeFAAQByAABQhKQBQhJAAhnIAAg7QAAhnhQhKQhQhJhyAAI+FAAQhyAAhQBJg");
	this.shape_2.setTransform(3.9,-0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FF6600","#FF9900","#FFCC00"],[0,0.071,1],0,29,0,-28.9).s().p("AvCEYQhxAAhRhKQgSgQgOgTQgwg9AAhQIAAg7QAAhQAwg+QAOgSASgRQBRhJBxAAIeFAAQByAABPBJQBRBKAABnIAAA7QAABnhRBJQhPBKhyAAg");
	this.shape_3.setTransform(3.9,-0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.txt_1},{t:this.txt}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.score, new cjs.Rectangle(-122.4,-29.9,251,58), null);


(lib.quest = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.txt = new cjs.Text("", "bold 25px 'Veggieburger'", "#FFFFFF");
	this.txt.name = "txt";
	this.txt.textAlign = "center";
	this.txt.lineHeight = 20;
	this.txt.lineWidth = 75;
	this.txt.parent = this;
	this.txt.setTransform(71.2,-13.3);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC3300").s().p("AgUA0QgLgEgIgHIgDgEIgBgFIABgFIAEgEIAEgDIAGgBIAFABIAFADQADADAHACQAGADAFAAQAGAAADgCQADgDAAgDIAAgDQgCgDgEgCQgFgDgKgDQgQgDgJgIQgJgJAAgMQABgKAEgHQAFgHAKgDQAIgEALAAQAJAAAJACQAHACAJAEQADABACAEQACACAAAEIgBADIgBADQgCAEgDABQgDACgFAAIgDAAIgDgCIgKgEIgJgBQgFAAgDACQgDABAAADQAAAEAEABIAHADQAWAFALAKQALAIAAAPQAAAJgFAIQgGAHgIAEQgKAEgMAAQgLAAgMgDg");
	this.shape.setTransform(5.8,1.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC3300").s().p("AAfAzQgEgEAAgFIAAgsQAAgMgEgGQgEgFgFgCIgIgBQgFAAgFADQgHACgGAFQgFAGgEAHIAAAvQAAAFgEAEQgEAEgHAAQgGAAgEgEQgEgEAAgFIAAhTQAAgFAEgEQAEgDAGgBQAHABAEADQAEAEAAAFIAAABQAGgGAJgEQAIgEAJAAQAYABANANQAMAOABAYIAAAsQgBAFgEAEQgEAEgGAAQgGAAgEgEg");
	this.shape_1.setTransform(-6.7,1.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC3300").s().p("AglAyQgKgFgFgJQgFgKgBgLQAAgNAHgLQAGgNAKgKQAKgKAOgGQAMgGAOAAQAOAAAJAFQAKAGAFAJQAGAJAAAMQAAANgGALQgGAMgLALQgKAJgNAGQgNAHgOgBQgNAAgKgFgAgFgWQgKAIgHAJQgGAJgBAKQAAAHAEAEQADAFAIgBQALABAKgIQAKgGAGgKQAHgJAAgKQAAgHgDgEQgEgEgIAAQgLAAgJAGg");
	this.shape_2.setTransform(-20.8,1.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC3300").s().p("AgJBOQgEgDAAgGIAAhSQAAgFAEgEQAEgDAFgBQAHABADADQAEAEAAAFIAABSQAAAGgEADQgDAEgHAAQgFAAgEgEgAgKg4QgEgFgBgGQABgGAEgEQAEgEAGAAQAHAAAEAEQAFAEAAAGQAAAGgFAFQgEAEgHAAQgGAAgEgEg");
	this.shape_3.setTransform(-30.7,-1.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC3300").s().p("AgDBHQgMgGgFgLQgGgMAAgOIAAgfIgHAAQgGgBgEgDQgEgEAAgGQAAgFAEgEQAEgDAGAAIAHAAIAAgiQAAgGAFgEQADgDAHAAQAGAAAEADQADAEAAAGIAAAiIAdAAQAGAAAEADQAEAEAAAFQAAAGgEAEQgEADgGABIgdAAIAAAfQAAAMAGAGQAFAFANAAIACAAIAEAAIABAAIABgBQAGABAEADQAEAEAAAGIAAAAQAAAHgGADQgGADgLAAQgRAAgLgGg");
	this.shape_4.setTransform(-39.5,-0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC3300").s().p("AgUA0QgLgEgIgHIgDgEIgBgFIABgFIAEgEIAEgDIAGgBIAFABIAFADQADADAHACQAGADAFAAQAGAAADgCQADgDAAgDIAAgDQgCgDgEgCQgFgDgKgDQgQgDgJgIQgJgJAAgMQABgKAEgHQAFgHAKgDQAIgEALAAQAJAAAJACQAHACAJAEQADABACAEQACACAAAEIgBADIgBADQgCAEgDABQgDACgFAAIgDAAIgEgCIgJgEIgJgBQgFAAgDACQgDABAAADQAAAEAEABIAHADQAWAFALAKQALAIAAAPQAAAJgFAIQgGAHgIAEQgKAEgMAAQgLAAgMgDg");
	this.shape_5.setTransform(-50.4,1.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC3300").s().p("AgnAyQgMgFgHgJQgIgKAAgOQAAgPAJgMQAJgNAPgIQAOgJARgFQAPgEAQAAQAJAAAJADQAIADAGAFQAGAGAAAKQAAALgIAJQgIAHgMAFQgMAFgOADQgNADgNABIgXABQADAHAIADQAHADAIAAQAJAAAMgEIAcgLQADgCAEAAQAGABAEADQAEAEAAAFQAAAEgCADQgCADgDACQgTAJgQAEQgQADgNAAQgPAAgNgFgAAGgaQgLADgKAGQgKAGgGAIIAXgCIAVgGQAKgDAGgEQAHgEAAgEQAAAAAAAAQAAgBgBAAQAAAAAAgBQAAAAgBAAIgFgBIgBAAQgLAAgLADg");
	this.shape_6.setTransform(-63.5,1.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC3300").s().p("AgqApQgNgNAAgZIAAgrQAAgGAEgEQAEgDAGgBQAHABAEADQAEAEAAAGIAAArQAAANAEAFQAEAFAEACQAFABADAAQAGAAAFgDQAGgCAGgFQAGgGAEgHIAAguQAAgGAEgEQAEgDAGgBQAGABAFADQAEAEAAAGIAABRQAAAGgEAEQgFAEgGAAQgGAAgEgEQgEgEAAgGIAAAAQgHAGgIAEQgJAEgJAAQgXAAgNgOg");
	this.shape_7.setTransform(-78.3,1.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC3300").s().p("ABBBYIgFgDIgcgbQgLAJgOAFQgMAEgPAAQgVAAgOgJQgPgJgHgPQgHgQAAgRQgBgRAIgRQAHgSANgOQANgPARgIQASgJAUAAQAVAAAOAJQAPAKAHAPQAIAPAAARQgBARgGAPQgGAQgMANIAeAcQAFAEAAAFQgBAGgEAEQgEADgGAAIgGgBgAgOg2QgMAHgJALQgJAKgEANQgGAMAAALQABAKADAIQADAJAIAGQAHAFAMAAQAIAAAJgCQAGgDAHgFIgLgMIgDgEIgBgFQAAgGAEgDQADgEAGAAIAGABIAEADIAMAKQAGgJAFgKQADgLAAgKQABgJgEgJQgDgJgIgFQgHgGgMAAQgNAAgMAGg");
	this.shape_8.setTransform(-94.2,-0.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#7C562C").ss(2,1,1).p("AmTiIQgmAxAABAIAAAvQAABAAmAxQAMAPAPANQBBA7BcAAIG3AAQBcAABBg7QBBg6AAhTIAAgvQAAhThBg6QhBg7hcAAIm3AAQhcAAhBA7QgPANgMAPg");
	this.shape_9.setTransform(71.8,-0.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#422E17","#422E17","#A5723B"],[0,0.071,1],0.2,-19.6,0,19.7).s().p("AjcDgQhbAAhBg7QgOgNgMgPQgngxAAhAIAAgvQAAhAAngxQAMgPAOgNQBBg7BbAAIG4AAQBcAABBA7QBBA6AABTIAAAvQAABThBA6QhBA7hcAAg");
	this.shape_10.setTransform(71.8,-0.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#A5723B").ss(2,1,1).p("AyxirQgwA+AABQIAAA7QAABQAwA9QAOATATAQQBRBKBzAAIeZAAQBzAABRhKQBRhJAAhnIAAg7QAAhnhRhKQhRhJhzAAI+ZAAQhzAAhRBJQgTARgOASg");
	this.shape_11.setTransform(5,-0.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FF6600","#FF9900","#FFCC00"],[0,0.071,1],0,29,0,-28.9).s().p("AvMEYQhzAAhRhKQgSgQgPgTQgwg9AAhQIAAg7QAAhQAwg+QAPgSASgRQBRhJBzAAIeZAAQBzAABQBJQBSBKAABnIAAA7QAABnhSBJQhQBKhzAAg");
	this.shape_12.setTransform(5,-0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.txt}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.quest, new cjs.Rectangle(-121,-29.9,252,58), null);


(lib.Parrot_Winked = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// hair
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CCFF").s().p("AiUA7QgGgGgEgDIgRgNIBygpQAwgTAwgcQAugdAuAJQAcAEAVAfIgQgBQhoAChWBVIgQAHQgaARgaAAQgZAAgZgPg");
	this.shape.setTransform(10.8,-86.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0099CC").s().p("AjWACQAXgDAWgBQC8gaBQgwQAegDAfAWQAdATAbApIgLAAQgWgegcgEQgtgJguAdQgxAbgwATIhyAqQgcgagngxg");
	this.shape_1.setTransform(8,-90.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#006699").s().p("ACMByIgLgSQgmg+g1gmQg1gphDgRIgWgGQgXgZgSgeICjBLIAAAEQAGAEgFgHQAkAeAgAnQAeApAXAyIAGALIgGgKg");
	this.shape_2.setTransform(21.3,-101.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00FF99").s().p("AFyBnQgdgogigaQgigZgngPQgrgXgxgCQgvgDgwAFQgzAEg9AkQhBAmhDgLQhKgNhDgrIgphUIAKgTQAbAeAPAJQAVAYAxAOIAMgCQDjhECSAfIAWAFQBDARA2AqQA1AlAmA/IALASIAGAKIABAHIgKgQg");
	this.shape_3.setTransform(-2,-99.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#009966").s().p("AFtBUIgHgGQgqgfgbgBQgbgrgdgSQgegVgfACQhPAwi9AZQgWACgXADQgDgBgEABQhLAAg2gaQgsgUgcgkIgPgfQBDAsBKANQBDALBBgmQA9glAzgEQAwgFAvAEQAxABArAXQAnAQAiAZQAiAZAdAoIAKAQIgBgHIAGANIAEAHIACAGg");
	this.shape_4.setTransform(0.6,-94.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0066FF").s().p("ACuEPIgCgGIgEgHIgGgNIgGgLQgXgzgegoQgggpgkgdQAFAHgGgEIAAgEIijhLIAAAAIgBgDQgRgegMgjQgmh2A5hRIADAIQgTAtAEAVQABAHASAkQAmBLBjA3QBYAvAoBaQAZA3ADAMQAJAcAGA8IAAACg");
	this.shape_5.setTransform(19.7,-113.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// wing_f
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FF9900","#FF3300","#800000"],[0,0.58,1],18.3,-35.3,0,18.3,-35.3,102.6).s().p("AoMFbIBYhvIANgTIACgCIA5hJIBGheQAxg9Axg0QADgFACgBQCkivC5hGQDdhTCSBaQg5A2hFAjQg/AehFAJQhRALhCAtQgmAbgdAmQgsAygOBAIgEghQgCgbAEgcQAMhZA9hFIgFAEQg9Avg6A1QghAigeAkQgmAwghA1QgnA8gtA2QguAyg2ApQg5AwhJAVQgKgEgIgGg");
	this.shape_6.setTransform(-6.8,29.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#010031","#0155D8"],[0.278,1],-17,18,21,-14.7).s().p("AjNCKQFHhoBVi4QhqD+kKAuIgCABQgWgKgQgDg");
	this.shape_7.setTransform(1.9,67.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#010031","#0155D8"],[0.278,1],-19.3,20.2,22.9,-16.1).s().p("AjpCVQF3hpBbjHQh7EplXAOg");
	this.shape_8.setTransform(-21.8,52.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#010031","#0155D8"],[0.278,1],-12,13.3,14.9,-9.9).s().p("ACRhnQgxB5hiA4QhQAVg+AJQDohLA5iEg");
	this.shape_9.setTransform(19.4,74.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],44,-1.6,0,44,-1.6,69.5).s().p("Ag1G+IgmgTIgEgBIACgBQEKguBqj/QhVC5lHBoIgQgBIgBgBQhOgGhegYQhggZAKgUQALgWAAgWIgBgMQFYgNB6kpQhaDHl4BpIgCABIgKgBQhBgDgtgSQgKgEgJgFIBYhwIAOgTIABgBIA5hKIBGhcQAxg/Axg0QADgFADgBQCjiuC6hGQDdhUCRBbIAKAGIAHAFIANAJQgOAmgFAqQASgRgXAYQgEDgAJCqQAHBpgwBEQg8CAiNAYIgKAEIgXAGIgCAAQBig4Axh6Qg4CFjpBLQgsAGgiAAQgyAAgbgOg");
	this.shape_10.setTransform(-5.3,39.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// mouth
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FFFF00","#FFFFFF"],[0,0.992],-11.1,3.2,11.1,-3.2).s().p("AAyARQgpgbg+geQg+gfgkgkIBOAvQBJAdA+ArQAdATAYAeIAkAvQg+hBgngag");
	this.shape_11.setTransform(86.6,-42.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FF0000","#000000"],[0,1],0.7,0.1,0,0.7,0.1,7.5).s().p("Ag4APQgFgQAAgGQAAgLAGgKIAIgKIAAgBQAFgDAFAAQASACAIATIALAkQAMANAygNQgaAMgTAHIALgEIgcAKIgCABQgKACgIAAQgcAAgIgcg");
	this.shape_12.setTransform(67.3,-48.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FFCC00","#FF3300"],[0,1],-8.6,3.7,10.9,-4.5).s().p("Ag/BOIgEgFQgKgRgGgUIgCgDQgMgjgVhOIgKgpIgLgrIACAKQAPA0AdAcIABADQABALADALIAAADQAFAXAGAUQAFAPAFAOQAMAYAPARQAoAtAsgOQAngMAHgxIABgIIABgpIAAgdIgBgOIAMgCIACgBIACAHIABAIQABAbgDApQAAATAEAUQAGAsAYA4IgDABQiPAAg5hXg");
	this.shape_13.setTransform(73,-23.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#FF0000","#000000"],[0,1],3.1,-4.3,0,3.1,-4.3,11.8).s().p("AgpAoQgQgRgLgXQgGgOgEgQQgHgUgEgXQASAYBHgEQBIgGASAUIgBApIAAAIQgHAxgnAMQgKADgLAAQggAAgfgig");
	this.shape_14.setTransform(73.1,-20.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#FF0000","#000000"],[0,1],4.8,6.4,0,4.8,6.4,10.3).s().p("AACARQhHAEgSgXIgBgDQgCgLgBgLIgBgDQANAMAOAGQAmASBCgIIA0gJIABANIAAAdQgSgUhIAGg");
	this.shape_15.setTransform(72.9,-28);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#FFCC00","#FF3300"],[0,1],-0.9,0.6,0,-0.9,0.6,29.1).s().p("AAVB+IgBgGIgDABIgMABIg0AJQhCAJgmgTQgPgGgMgMQgdgcgPg0IgDgKIALAsQgMgXgJgdIgDgKIgBgGQgDgLgBgIQgGgWAAgKIAAgNQACgQAGgNQAHgQAOgNIAAAEIAxgBQAsgBAqAIQDLAiB/DOIAEAIIABABIAAAAIgDgCQgggOgxABQgTAAgVADIg4ALIgUAFIgZAEIgDAAIgBgIg");
	this.shape_16.setTransform(81.6,-41.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#FF0000","#000000"],[0,1],0,0.9,0,0,0.9,27.3).s().p("ADlCEQh+jOjLgjQgqgHgtAAIgwACIAAgFIABgBIAugKQEmg3BzEWIAPAtIgCACIgFgIg");
	this.shape_17.setTransform(83.1,-42.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	// Layer 3
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#0B2901").ss(1,1,1).p("AC9imQAFACAEAAQASAAANgXQAGgMAKgWADYibQACAFADABQADABAJAAQATAAAMgLQAHgHABgHAEahWQAHACAGgDQAEgCADgDAEBh9QAGACAHAAQAAAFAQgLACYigQAugEAAhfAhjDPQgUArgNAJAhSC2QgBAbgBACAhCCXQAEAJAAAQAiGDaQgFANgTAQQgPAMgFABAijDkQgXATAAAAQgVAOgtgCAiwDeQgMAHgbAAQgeAAgHgFQgEgDgtgXAgrB4QADAHAAAEQAAAGAAAB");
	this.shape_18.setTransform(45.3,-59.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

	// eyes
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#37E708","#BEFF7B"],[0,1],-6.1,1.1,6.5,0.5).s().p("AgTBHIgEgFIgNgQQgagiABgiQAAgkAWgNQAWgNAcASQAXAOAQAYQAGAIAEAKIACADQgRBMgvAAQgIAAgJgCg");
	this.shape_19.setTransform(27.6,-58.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#215F00","#12D405"],[0,1],-16.5,-5.5,8.8,-6.6).s().p("AhEBQIAAgBIgOgPQB7AgAoi1IACAGQgXClhZAAQgSAAgVgGg");
	this.shape_20.setTransform(34.2,-44);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.lf(["#17BB00","#37E708","#86F02E","#BEFF7B"],[0,0.549,0.839,1],-12.2,2.2,13.1,1.1).s().p("AgnCOIgIgKIgbgfQgzhFAChFQAAhIArgaQAtgaA6AkQAsAcAiAxQAKAQAKATIADAGQgiCaheAAQgRAAgSgFg");
	this.shape_21.setTransform(29.8,-51.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("rgba(0,0,0,0.2)").s().p("Ag+BWIgPgQQB7AnAcjFIAEAMQgXClhaAAQgNAAgOgDg");
	this.shape_22.setTransform(35.1,-43.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(243,243,243,0.502)").s().p("AgBA7QgVgLgPgbQgQgYACgZQABgZAPgJQAQgJAUAKIAEADQAUAMAOAaQAOAYgBAZQAAAZgQAKQgGADgHAAQgLAAgNgIg");
	this.shape_23.setTransform(27,-53.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#F3F3F3").s().p("AAeAoIgFgDIgCgDQgDgFABgEQAAgFACgBQADgCAEABIABABQACABACADIACADQACAEAAAFQAAAFgDABIgCABIgEgCgAgUADQgHgDgFgJQgHgJABgJQABgIAFgEQAFgEAIAFIABABQAHAEAFAJQAGAJgBAJQABAIgGADQgCABgDAAQgEAAgFgDg");
	this.shape_24.setTransform(31,-48.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.rf(["#00FFFF","#0072FF","#0033CC"],[0,0.839,1],-0.3,-1.7,0,-0.3,-1.7,6.2).s().p("AgCBUQgegRgUgkQgWglACgiQAAgiAXgOQAXgMAdARIADACIABAAQAbARAUAiQAUAkgBAiQAAAjgXANQgJAGgLAAQgPAAgRgKgAAjAsQAEADACgCQADgBAAgFQAAgFgCgEIgCgDQABgEgBgFQABgSgKgTQgJgRgQgIIgBgBQgPgJgLAGQgMAHgBASQAAARALASQALATANAJQARAIALgGIABgBIAFADg");
	this.shape_25.setTransform(30.5,-48.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000033").s().p("AgBArQgOgIgLgTQgLgSAAgSQABgRAMgIQALgGAPAJIABACQAPAHAKASQAKASgBASQABAFgBAFQgCgDgCgBIgBgBQgEgBgDABQgCABAAAFQgBAEADAFIACAEIgBAAQgFADgGAAQgHAAgJgFgAgegfQgFADgBAJQgBAJAHAIQAFAJAHAEQAIAFAGgEQAFgDgBgJQABgHgFgJQgFgKgHgEIgBgBQgEgCgDAAQgDAAgDACg");
	this.shape_26.setTransform(30.8,-48.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#999999","#666666"],[0,1],-15.8,-5,16.2,4.5).s().p("AAQClQgpgcgogtIgagfQgzhEAChGQgBhIAsgaQAsgaA6AkQAsAcAiAxQALAQAJATQAoBIgBBDQgBBDghAYQgOAKgRAAQgaAAgjgWgAhcinQgqAZAABEQgCBDAyBAIAYAfQAmApAnAcQA3AjAggXQAggZABg+QABhAgmhFQgJgSgKgOQgfgwgrgaQgggVgbAAQgVAAgRALg");
	this.shape_27.setTransform(31.6,-47.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.rf(["#FFFFFF","#F9F9F9","#E5E5E5","#B6B1B1","#626262"],[0,0.122,0.443,0.729,1],0.2,-0.2,0,0.2,-0.2,11.6).s().p("AAPCeQgngcgmgqIgYgeQgyhAAChEQAAhEAqgYQAqgaA3AjQArAaAfAwQAKAPAJARQAmBGgBBAQgBA+ggAZQgMAIgRAAQgYAAgigUg");
	this.shape_28.setTransform(31.6,-47.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFCC00","#FF6600"],[0,1],-18.2,-5.8,18.6,5.2).s().p("AASC+Qgygigvg3QgNgOgMgRIgBAAQg7hPABhRQABhNAsgeQACgDAEgBQAzgfBCApQA2AiAoA8QALAQAIASIABABQAuBUgCBOQgBBLgnAdQgPALgUAAQgdAAgpgZgAhhivQgsAZABBIQgCBHAzBDIAaAgQAoAtApAcQA6AkAigYQAhgZABhDQABhDgohIQgJgTgLgQQgigxgsgbQghgVgdAAQgVAAgTALg");
	this.shape_29.setTransform(31.6,-47.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("rgba(243,243,243,0.502)").s().p("AgCAvQgQgKgLgVQgNgTADgUQABgUAMgGQAOgIAOAJIADACQAQAKALAWQAKATgBATQAAAUgOAHQgFADgGAAQgJAAgJgHg");
	this.shape_30.setTransform(63,-62.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#F3F3F3").s().p("AAWAgIgDgDIgCgCQgCgEAAgDQAAgEADgBQACgBADABIABABIACADIACADQACADAAAEQAAADgDABIgBABIgEgCgAgPACQgHgCgDgIQgEgGAAgGIAAgCQABgHAEgDQAFgCAFADIACABQAFADADAIQAEAHAAAHQgBAGgEADIgDABQgEAAgDgDg");
	this.shape_31.setTransform(66.4,-60.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.rf(["#00FFFF","#0072FF","#0033CC"],[0,0.839,1],-0.2,-1.4,0,-0.2,-1.4,4.9).s().p("AgDBCQgYgOgPgdQgPgdABgbQACgbASgKQASgKAXAPIACABQAWAPAPAbQAOAcgBAcQgCAbgSAKQgHAEgIAAQgMAAgNgJgAAaAkQADACACgBQADgBAAgEQAAgDgCgEIgCgCIACgIQAAgOgHgOQgHgPgMgGIgBgBQgLgIgJAFQgJAGgBANIAAADQAAANAHAMQAHAPALAHQAMAIAKgFIABgBIADADg");
	this.shape_32.setTransform(66,-60.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000033").s().p("AgCAiQgMgHgHgPQgHgNAAgNQAAAGAEAGQADAHAHADQAFAEAFgCQAEgDAAgHQAAgGgDgHQgDgIgFgDIgCgBQgFgDgFACQgEADgBAHQABgOAJgGQAJgEALAIIABAAQAMAHAHAOQAHAPAAAOIgCAHIgCgDIgBgBQgDgBgCABQgDABAAAEQAAADACAEIACACIgBABQgEACgEAAQgGAAgHgEg");
	this.shape_33.setTransform(66.3,-61.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#FFFFFF","#F9F9F9","#E2E2E2","#ADADAD"],[0,0.42,0.78,1],0.1,-0.2,0,0.1,-0.2,9.2).s().p("AAIB8QgegWgdgjIgSgYQgmg1ADg0QAEg2AggSQAjgTAqAdQAgAWAYAmQAIAMAHAOQAbA4gDAyQgBAxgbATQgJAGgMAAQgUAAgbgSg");
	this.shape_34.setTransform(67.4,-61.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.lf(["#999999","#666666"],[0,1],-12.4,-4.5,12.6,3.9).s().p("AAICCQgggXgeglIgTgZQgng3ADg4QACg4AjgTQAkgTAsAeQAiAXAaAoQAIAMAHAPQAdA6gDA1QgCA1gbATQgKAGgNAAQgVAAgcgTgAhFiGQggARgDA2QgDA1AmA1IASAYQAdAiAeAWQAqAeAagSQAagSACgxQADgzgcg3QgGgOgIgNQgYgmghgWQgYgRgXAAQgQAAgOAIg");
	this.shape_35.setTransform(67.5,-61.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.lf(["#FFCC00","#FF6600"],[0,1],-14.3,-5.1,14.5,4.6).s().p("AAKCWQgngcgkgsIgSgaIgBAAQgtg/AEhBQACg9AkgWIAFgDQApgXAzAjQApAcAeAwQAIAMAHAQIAAAAQAiBDgDA9QgEA8gfAWQgMAIgOAAQgZAAgfgWgAhIiNQgjATgBA4QgDA4AmA3IAUAZQAeAlAfAXQAtAfAbgSQAbgTADg1QADg1gdg6QgIgPgIgMQgZgogigXQgbgSgYAAQgQAAgOAHg");
	this.shape_36.setTransform(67.4,-61.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19}]}).wait(1));

	// body
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("rgba(255,255,255,0.451)").s().p("AAIDgQhIgug1hiQg1hggDheQgFhcAyghQAwghBJAuQBGAsA2BiQA2BhADBcQADBfgwAgQgVAOgZAAQgiAAgpgag");
	this.shape_37.setTransform(29.4,-48.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#0BE0AB").s().p("AAAAAQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAAAABAAIgBADIAAgCg");
	this.shape_38.setTransform(8.1,57);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],64.7,-34,0,64.7,-34,102.5).s().p("AjtG1QhHgFhLgRIgxgMQgzgPg2gUQAfgnAcgoIAAAAIAIgMIACgCIArhBQCLjVBCjRIgEASQISjdDogXQgVAHgRALQgVANgRASQgRATgOAaQgRAfgMApQgWBMgRBNQgSBUgbBQQgYBKgnBBIgJAOQgJAOgMANIhKBQIgNAOIhbBdQhQAUhSAGQgdABgdAAQggAAgggCgAiqCZQABAGAAgIQgBAAAAAAQAAABAAAAQgBAAAAAAQAAABABAAg");
	this.shape_39.setTransform(25.2,41.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.rf(["#FFFF00","#FF9900"],[0,1],16.4,11.5,0,16.4,11.5,28.3).s().p("AjzFOIANgOIBKhQQAMgNAJgOIAJgOQAnhBAYhKQAbhQAShUQARhNAVhMQAMgpARgfQAOgaARgTQARgSAVgNQARgLAVgHQCigRAQBRIABAeQAAAlgCAlQgMB9g2B1QhBCLh0BgIgPAMQggAZgkAVQidBhi0AyIBbhdg");
	this.shape_40.setTransform(63.4,40.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#003366").ss(1,1,1).p("AA7ggQg1AjhAAe");
	this.shape_41.setTransform(33.8,76.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#CC6600").s().p("AgBAAIADgCIAAAAIgCAFIgBgDg");
	this.shape_42.setTransform(53,98.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#993300").s().p("AgBAAIADAAIAAABg");
	this.shape_43.setTransform(51.3,81.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.rf(["#FFFF00","#FF9900"],[0,1],16.4,12,0,16.4,12,28.3).s().p("AjzFTIANgOIBKhQQAMgNAJgOIAJgOQAnhBAYhKQAbhQAShUQARhNAVhMQAWhMAmgpQARgSAVgNQAcgSAlgIQAagGAdACQArADAxAXQAJBOgGBMQgMB9g2B1QhBCLh0BgIgPAMQggAZgkAVQidBhi0AyIBbhdg");
	this.shape_44.setTransform(63.4,39.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#0BE0AB").s().p("AkrEnQAAgBAAAAQAAAAAAgBQAAAAABAAQAAAAAAAAIAAAEIgBgCgAErkoIABABIgBACIAAgDg");
	this.shape_45.setTransform(38,27.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#51FD00","#47E202","#074A02"],[0,0.42,1],-6.6,-0.4,0,-6.6,-0.4,102.5).s().p("AlEOEQhHgFhLgRIgxgNQgzgPg2gUQAfgnAcgnIAAgBIAIgLIACgDIArhAQC3kZA5kSQBDlIh1k/QgShYAHgzQAIgzAzhPQA0hPBlgRQBlgSBiAcQBiAbBAA2QA/A2AgAkQAfAkAmA6QAnA5ApBIQBcC9AhDjIAJBOQAFAQACASQgxgXgrgDQgdgDgaAGQglAJgcARQgVANgRASQgmAqgWBMQgWBLgRBOQgSBTgbBSQgYBKgnBBIgJANQgJAPgMAMIhKBRIgNANIhaBdQhRAVhSAFQgdACgdAAQggAAgggCgAkBJoQABAGAAgIQgBAAAAAAQAAAAAAABQAAAAAAAAQAAABAAAAgAFUAbIABgCIgBgBIAAADg");
	this.shape_46.setTransform(33.9,-4.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#003366").ss(1,1,1).p("AA7ggQg1AjhAAe");
	this.shape_47.setTransform(33.8,76.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#CC6600").s().p("AgBAAIADgCIAAAAIgCAFIgBgDg");
	this.shape_48.setTransform(53,98.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#993300").s().p("AgBAAIADAAIAAABg");
	this.shape_49.setTransform(51.3,81.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37}]}).wait(1));

	// tail
	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],5.8,-20.3,-0.9,29.2).s().p("ACVC/QhHgLhOgOQjegpkehFQhnhXAJhXQAJhYCSgPQARgCAXACQChAJHCCoIAKAEIDZBNIADABIAIADIgLgJQB9BaAwgSIgDAMIgDANQgKAtgNAwIgNABIgJAAQi3hQjdAwg");
	this.shape_50.setTransform(-78.8,55.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],-5.6,-25,5.6,25.1).s().p("AjNBdQgggagRhBQgQg5CBgdIgTgCQDcgxC4BRIgDAAIAPAGIABACQhCAmg0AZIgmARQhBAehoAcQgaAGgWAEQgWAEgSAAQgiAAgPgNg");
	this.shape_51.setTransform(-47.9,83);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.lf(["#DD1000","#FF6600","#0066CC","#000066"],[0,0.494,0.518,0.984],15.5,-12.5,-15.5,12.7).s().p("AAXCTQgygngUgVQgLgLgUgcQgWgeghgxQhChfAAg2QgBgyA9AMQAyAJBEAzIAhAcQBZBLAcAyQAdAvAWBJQAVBKgBABQgBALgCAGIABADIgCAJQgIADgKAAQgzAAhohLg");
	this.shape_52.setTransform(-38,43.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_52},{t:this.shape_51},{t:this.shape_50}]}).wait(1));

	// wing_BK
	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.rf(["#FF9900","#FF3300","#800000"],[0,0.58,1],21.6,18.3,0,21.6,18.3,51.5).s().p("AB1BaQgqgMglgWQhcg2hpAOIgCgCIgLgBQh0AGhaBHQAQgTAVgSQBLg+BDgTIAHgCQApgLAmAGQAjAEAjAFQAhAGAgAHIA0ANQBYAZBXgQQBagQAohcQADBBgnAuQgTAXgeAIQg/AOhBgBQAJACAJAFQAYAMAcADQAdAAAhgDQgzAZg3AAQgkAAgngKg");
	this.shape_53.setTransform(111.5,-18.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],0,0.1,0,0,0.1,0.4).s().p("AgDAAIAFAAIABABIgGgBg");
	this.shape_54.setTransform(115.1,25.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.lf(["#0173DE","#010031"],[0,1],-13.2,0.6,13.6,-1.1).s().p("AiIhBQBQCNDBg9QgGAVgUAWQgkAIggAAQh+AAg1iDg");
	this.shape_55.setTransform(115.3,18.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.lf(["#0173DE","#010031"],[0,1],-19.3,0.7,20,-1.8).s().p("AjGhSQBvDFEehzIgBAAIgDACQABAjgEADQhiArhNAAQiUAAhDilg");
	this.shape_56.setTransform(124,2.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.rf(["#02B8FF","#0294E6","#0100C6"],[0,0.451,1],23.8,-4.3,0,23.8,-4.3,51.5).s().p("Ai7EOQgwhSgTgVQgKgIhXgxQhKgogEgcQgIhEAthBIABgCQAPgVAUgVQAQgTAVgSQBLg/BDgTIAHgCQApgLAmAGQAjADAjAGQAgAGAhAHIA0ANQBYAZBXgQQBagQAohdIADgIQAPAfAGAYQADAOACAUIAAAHQAGA+gdBCQgGAPgHAMQgUAlgZAcIgCABQkfB0hvjGQBlD7EhiAIgRAfQgWAfgpAeQgYAUgfAQQAAABgBAAQAAAAAAAAQAAAAAAAAQAAAAgBgBIgCADIgDAKQjCA9hPiOQBDClC0gqQgKALgOANQgUAQgcASIgcAPQhgAygjADIgTACQg+gBguhAgAA2DZIAHACIgBgDIgGABg");
	this.shape_57.setTransform(109.3,3.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53}]}).wait(1));

	// legs
	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#431600").s().p("AAEAKQg7gQgoAIIAsgvQAUgVARAGIgDAAIgEAHIgPAWIgTAVQAVgGAXAIIAoAPQAWAGAVASQANALALAOIgEAGQgaghg+gTg");
	this.shape_58.setTransform(52.6,87);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#CC6600").s().p("AA8BPIgagRQgngWgfgNQgbgLgwgCIhLgBQAAAAABAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIAkgeQAQgOAbgUQATAUBXAxQBDAmAhAkQgFACgEAAQgNAAgSgMgACbgbIhOg4IAEgHIADAAIAEACQAnAcAVARQAYASAPASQgQgIgQgMg");
	this.shape_59.setTransform(42.7,90);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#993300").s().p("AgwAZQhmgZhEANIBLhDQAjggAfALIgMAIQgbATgRAOIgjAgQAAAAAAABQAAAAgBABQAAAAAAAAQAAAAAAAAIBLAAQAvACAcAMQAeAMAoAWIAZASQATALANAAQADAAAFgCIADADQgFAHgLANQgsgvhrgagAC7gMQgVgSgWgGIgpgQQgXgHgVAFIATgUIAPgXIBOA5QARALAPAIQAKAMAGAKQAAAAAAABQAAAAgBABQAAAAAAAAQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAgBAAAAQAAgBAAAAQgBADgFAIQgLgNgNgLg");
	this.shape_60.setTransform(41.1,91.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FF9900").s().p("AABACQhXgxgTgUIAMgIIAFACIBnBBQBCAqAZAnIgBAAIgEADQghgkhDgmg");
	this.shape_61.setTransform(42.6,91.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Parrot_Winked, new cjs.Rectangle(-139.2,-140.3,291.6,241.5), null);


(lib.Gold_trophy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// shine
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(204,102,0,0.502)").s().p("AA1g/Qg4AogZBUQgFACgFgBQgFACgJAAQAZhYBQgng");
	this.shape.setTransform(-22.6,-34.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.502)").s().p("AAiA7QgEACgEgDQgchOgugpQBFApAcBSQgMAAgDgDg");
	this.shape_1.setTransform(5.1,-34.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFD90D").s().p("AASBSQgohUgLhtIALACIAJACQAFB9AqBdIAAABIgQgeg");
	this.shape_2.setTransform(-24.5,-13.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DB8402").s().p("AgYBYQhBh+gFiwQAJAGAUAGQAFClA8CEQAoBbA3AcQhEgagzhkg");
	this.shape_3.setTransform(-23.6,-5.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFCC").s().p("AgEDIQAshdAGh+IAIgBIALgCQgMBsgpBUIgPAfgAhAjHIAAgBIADADIAAAAIgDgCg");
	this.shape_4.setTransform(3.5,-22.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFF99").s().p("AAABgQA6iEAFilQAUgGAKgGQgFCwhDB+QgwBghBAdQA0geAohYg");
	this.shape_5.setTransform(5.9,-5.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// top
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FFFF33","#FFCC00","#9E4103"],[0,0.396,0.839],8.5,9,0,8.5,9,18).s().p("Ai0ALIgQgDIgZgFQgSgFgIgFIgCgBIgEgFQgEgJAKAAQAJAFARAFIAZAGIAQACQBLAMBqAAQBqAABKgMIASgCIAZgGQAPgFAIgFQALAEgEAGIgFAFQgIAFgRAEIgZAGIgSADQhKAMhqAAQhqAAhLgMg");
	this.shape_6.setTransform(-9,-26.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],-9,0.4,0,-9,0.4,24.7).s().p("AiPAnIgOgCIgTgEQgOgFgHgEIgBgBIgEgEQAXgvAxgUQAAADADADIACAAQAEADAJACIANADIAIACQAmAGA2AAQA2AAAmgGIAKgCIAMgDIANgFQADgCABgCQAvAXAYArIgEADQgGAFgNAEIgUAFIgPACQg7AKhVAAQhVAAg7gKg");
	this.shape_7.setTransform(-9,-36.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],1.1,-2,0,1.1,-2,4.2).s().p("AgNAeIgJgGQgHgEADgOQADgNAIgOQAHgOAJAAQAJAAAHAOQAHAOADAPQACAPgGADIgJAEQgGAGgIAAQgHAAgGgGg");
	this.shape_8.setTransform(-8.9,-48.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],3.1,-2.8,0,3.1,-2.8,15.3).s().p("AhbAdIgJgCIgNgCQgIgDgFgDIgBAAQgDgCgBgEIAPgKQAzgiA1gDQAFAGAIAAQAIAAAGgGQAxAEAzAhIARAMQgBACgEACIgMAGIgNACIgKACQglAGg3AAQg1AAgmgGg");
	this.shape_9.setTransform(-8.9,-42.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],4.7,1.5,0,4.7,1.5,22.3).s().p("Ai0AeIgQgCIgYgGQgSgFgIgFQAmgcAGgaIAEAEIABABQAHAFAOAEIATAEIAOACQA7AKBVAAQBVAAA7gKIAPgCIAUgFQANgEAGgFIAEgDQADAeApAXQgIAFgPAFIgYAGIgSACQhLANhqAAQhqAAhLgNg");
	this.shape_10.setTransform(-9,-30);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// handle
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],-11.7,-2.1,0,-11.9,-2.1,23.5).s().p("AAmDAQgRgPAAgXQAAgIADgIIACAAQAAgFADgFIAFgFQAIgGALAAQAMAAAIAGQAIAGAAAJQAAAJgIAGQgIAHgMAAIgIgBQACAJAHAHQAJAIAMAAQAMAAAJgIQAJgIAAgLIgBgIQgHgigrgUIgQgIQgqgUgogZQgpgYgbgmQgcgrABgyQABgxApgcQAqgcAxAVQArASAPAtIACARIAAAEIgBANQgGAagTgJIABgKQAAgdgUgVQgSgUgcAAQgcAAgUAUQgTAVAAAdQAAASARAmQASAnBEAkQBRAlAkAWIAAAAIADAHIAMAXIABACIAAAAIAEAGIAAACQAEAIAAAKQAAAXgSAPQgRAQgZAAQgYAAgSgQg");
	this.shape_11.setTransform(-38.6,-3.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],9.4,-4.3,0,9.4,-4.3,16.7).s().p("Ah5DAQgSgPAAgXQAAgLAEgJIADgGIABAAIABgCIAMgXIADgHQAkgWBRglQBEgkASgnQARgmAAgSQAAgdgTgVQgUgUgcAAQgcAAgSAUQgUAVAAAdIABAKQgTAJgGgaIgBgNIAAgEIACgRQAPgtArgSQAxgVAqAcQApAcABAxQABAygcArQgbAmgpAYQgoAZgqAUIgQAIQgrAUgHAiIgBAIQAAALAJAIQAJAIAMAAQAMAAAJgIQAHgHACgJIgIABQgMAAgIgHQgIgGAAgJQAAgJAIgGQAIgGAMAAQALAAAIAGIAFAFQADAFAAAFIACAAQADAIAAAIQAAAXgRAPQgSAQgYAAQgZAAgRgQg");
	this.shape_12.setTransform(20.6,-3.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	// Layer 6
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],0,4.2,0,0,4.2,16.2).s().p("AiMBvQBZgeAGiNIABAAQAPAIATAAIAXAAQATAAAOgIQAGCNBZAdQhDARhHAAQhFAAhKgQgAglh8QARAEASAAQAUAAATgGIAAAYQgKgEgOAAIgXAAQgOAAgNAFg");
	this.shape_13.setTransform(-8.7,34.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(1));

	// Layer 7
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["rgba(255,255,255,0.933)","rgba(255,255,255,0)"],[0,1],7.6,-14.3,0,7.6,-14.3,31.2).s().p("AhnBJQgrheAAiHIAAgLQBCAJBVAAQBPAAA/gIIAAAKQAACHgrBeQgrBfg9AAQg8AAgrhfg");
	this.shape_14.setTransform(-9.5,-7.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(1));

	// base
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],0.1,-0.6,0,0.1,-0.6,5.5).s().p("AiNAfQgVAAgOgKQgOgJAAgNIAAgBQAAgOAOgKQAOgJAVAAIACAAQCUAgCFggQAVAAAOAJQAOAKAAAOIAAABQAAANgOAJQgOAKgVAAQhJAFhGAAQhHAAhFgFg");
	this.shape_15.setTransform(-8.9,48.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#FFFF33","#FFCC00","#EC8E02"],[0,0.396,0.839],7.6,-4.7,0,7.6,-4.7,44.8).s().p("AgiD0QhGgTg3heIAAgBIgEgGIAAgBIgBgCIgMgXIgDgGIgBAAQhCiNgDjFQACAEAHADQAJAGATAFIAYAGIALACIAJABIARADQBCAJBVAAQBPAAA/gIIAcgEIAIgBIAMgCIAWgGQAUgFAKgGQAGgEACgEQgBA4gGA0IgCAQQgGAsgKApQgQBFgdA9IgEAGIgLAXIgCACIAAABIgDAGQg1BchDAUQgUAGgUAAQgRAAgRgEgAB0AIIAEgHIAAAAIgEAHgAh4ABIAAAAIABABIgBgCg");
	this.shape_16.setTransform(-9,-2.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFCC00").s().p("AgLAbQgTAAgPgIIgBgBIAAAAQgNgHAAgLIAAAAQAAgKAOgIIAHgDQANgFAOAAIAXAAQAOAAAKAEQAGABADADQAPAIAAAKIAAAAQAAALgPAIQgOAIgTAAg");
	this.shape_17.setTransform(-8.6,26.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#993300").s().p("AiWAaIgEAAIgOgDIgUgEIgBgBQgPgEgIgFIgBgBQgGgDAAgFQAAgIAcgJIADgBQAOgDAUgEIAHgBQA/gJBUAAQBPAAA7AIIARACQAYAFAPAEQANAEAGAEQAHAEAAAEQAAAFgHAEQgHAEgOAFIgVAFIgQADIgNABQg8AJhSAAQhXAAg/gKg");
	this.shape_18.setTransform(-9,-28.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF6600").s().p("AiWAkIgSgDIgIgCIgMgBIgXgGQgTgFgKgGQgGgEgDgEIgBgFIABgEQAJgOAxgKIADgBIAMgCIAfgFQA/gIBSAAQBNAAA9AHIApAGIAQADQAkAIANAJQAFAEACAEIAAAHQgCAFgGAEQgKAGgUAFIgXAGIgLABIgIACIgcAEQg/AIhQAAQhVAAhBgJgAiSgeIgIABQgUAEgOADIgDABQgbAJAAAJQgBAEAGADIABABQAIAFAPAEIABABIAUAEIAOADIAEAAQBAAKBWAAQBSAAA8gJIANgBIAQgDIAVgFQAOgFAIgEQAGgEAAgEQAAgFgGgEQgHgEgMgEQgQgEgYgFIgRgCQg6gIhQAAQhUAAg+AJg");
	this.shape_19.setTransform(-9,-27.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.5,-52.5,87.1,105);


(lib.Bronze_trophy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// shine
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(108,36,36,0.502)").s().p("AA1g/Qg4AogZBUQgFACgFgBQgFACgJAAQAZhYBQgng");
	this.shape.setTransform(-22.6,-34.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.502)").s().p("AAiA7QgEACgEgDQgchOgugpQBFApAcBSQgMAAgDgDg");
	this.shape_1.setTransform(5.1,-34.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#993333").s().p("AgYBYQhBh+gFiwQAJAGAUAGQAFClA8CEQAoBbA3AcQhEgagzhkg");
	this.shape_2.setTransform(-23.6,-5.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF9966").s().p("AASBSQgohUgLhtIALACIAJACQAFB9AqBdIAAABIgQgeg");
	this.shape_3.setTransform(-24.5,-13.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FDCC9B").s().p("AAABgQA6iEAFilQAUgGAKgGQgFCwhDB+QgwBghBAdQA0geAohYg");
	this.shape_4.setTransform(5.9,-5.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFCC").s().p("AgiBvQArhdAGh+IAIgBIAMgCQgMBtgoBTIgQAfg");
	this.shape_5.setTransform(6.5,-13.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// top
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#FF6633","#662222"],[0,0.914],8.5,9,0,8.5,9,18).s().p("Ai0ALIgQgDIgZgFQgSgFgIgFIgCgBIgEgFQgEgJAKAAQAJAFARAFIAZAGIAQACQBLAMBqAAQBqAABKgMIASgCIAZgGQAPgFAIgFQALAEgEAGIgFAFQgIAFgRAEIgZAGIgSADQhKAMhqAAQhqAAhLgMg");
	this.shape_6.setTransform(-9,-26.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],1.1,-2,0,1.1,-2,4.2).s().p("AgNAeIgJgGQgHgEADgOQADgNAIgOQAHgOAJAAQAJAAAHAOQAHAOADAPQACAPgGADIgJAEQgGAGgIAAQgHAAgGgGg");
	this.shape_7.setTransform(-8.9,-48.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],4.7,1.5,0,4.7,1.5,22.3).s().p("Ai0AeIgQgCIgYgGQgSgFgIgFQAmgcAGgaIAEAEIABABQAHAFAOAEIATAEIAOACQA7AKBVAAQBVAAA7gKIAPgCIAUgFQANgEAGgFIAEgDQADAeApAXQgIAFgPAFIgYAGIgSACQhLANhqAAQhqAAhLgNg");
	this.shape_8.setTransform(-9,-30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],-9,0.4,0,-9,0.4,24.7).s().p("AiPAnIgOgCIgTgEQgOgFgHgEIgBgBIgEgEQAXgvAxgUQAAADADADIACAAQAEADAJACIANADIAIACQAmAGA2AAQA2AAAmgGIAKgCIAMgDIANgFQADgCABgCQAvAXAYArIgEADQgGAFgNAEIgUAFIgPACQg7AKhVAAQhVAAg7gKg");
	this.shape_9.setTransform(-9,-36.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],3.1,-2.8,0,3.1,-2.8,15.3).s().p("AhbAdIgJgCIgNgCQgIgDgFgDIgBAAQgDgCgBgEIAPgKQAzgiA1gDQAFAGAIAAQAIAAAGgGQAxAEAzAhIARAMQgBACgEACIgMAGIgNACIgKACQglAGg3AAQg1AAgmgGg");
	this.shape_10.setTransform(-8.9,-42.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]}).wait(1));

	// handle
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],-11.9,-2.1,0,-11.9,-2.1,23.5).s().p("AAmDAQgRgPAAgXQAAgIADgIIACAAQAAgFADgFIAFgFQAIgGALAAQAMAAAIAGQAIAGAAAJQAAAJgIAGQgIAHgMAAIgIgBQACAJAHAHQAJAIAMAAQAMAAAJgIQAJgIAAgLIgBgIQgHgigrgUIgQgIQgqgUgogZQgpgYgbgmQgcgrABgyQABgxApgcQAqgcAxAVQArASAPAtIACARIAAAEIgBANQgGAagTgJIABgKQAAgdgUgVQgSgUgcAAQgcAAgUAUQgTAVAAAdQAAASARAmQASAnBEAkQBRAlAkAWIAAAAIADAHIAMAXIABACIAAAAIAEAGIAAACQAEAIAAAKQAAAXgSAPQgRAQgZAAQgYAAgSgQg");
	this.shape_11.setTransform(-38.6,-3.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],9.4,-4.3,0,9.4,-4.3,16.7).s().p("Ah5DAQgSgPAAgXQAAgLAEgJIADgGIABAAIABgCIAMgXIADgHQAkgWBRglQBEgkASgnQARgmAAgSQAAgdgTgVQgUgUgcAAQgcAAgSAUQgUAVAAAdIABAKQgTAJgGgaIgBgNIAAgEIACgRQAPgtArgSQAxgVAqAcQApAcABAxQABAygcArQgbAmgpAYQgoAZgqAUIgQAIQgrAUgHAiIgBAIQAAALAJAIQAJAIAMAAQAMAAAJgIQAHgHACgJIgIABQgMAAgIgHQgIgGAAgJQAAgJAIgGQAIgGAMAAQALAAAIAGIAFAFQADAFAAAFIACAAQADAIAAAIQAAAXgRAPQgSAQgYAAQgZAAgRgQg");
	this.shape_12.setTransform(20.6,-3.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11}]}).wait(1));

	// Layer 6
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],0,4.2,0,0,4.2,16.2).s().p("AiMBvQBZgeAGiNIABAAQAPAIATAAIAXAAQATAAAOgIQAGCNBZAdQhDARhHAAQhFAAhKgQgAglh8QARAEASAAQAUAAATgGIAAAYQgKgEgOAAIgXAAQgOAAgNAFg");
	this.shape_13.setTransform(-8.7,34.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(1));

	// Layer 8
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["rgba(255,255,255,0.933)","rgba(255,255,255,0)"],[0,1],14.5,-17.3,0,14.5,-17.3,42.4).s().p("AhnBJQgrheAAiHIAAgLQBCAJBVAAQBPAAA/gIIAAAKQAACHgrBeQgrBfg9AAQg8AAgrhfg");
	this.shape_14.setTransform(-9.5,-7.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(1));

	// base
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],0.1,-0.6,0,0.1,-0.6,5.5).s().p("AiNAfQgVAAgOgKQgOgJAAgNIAAgBQAAgOAOgKQAOgJAVAAIACAAQCUAgCFggQAVAAAOAJQAOAKAAAOIAAABQAAANgOAJQgOAKgVAAQhJAFhGAAQhHAAhFgFg");
	this.shape_15.setTransform(-8.9,48.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF6633").s().p("AgLAbQgTAAgPgIIgBgBIAAAAQgNgHAAgLIAAAAQAAgKAOgIIAHgDQANgFAOAAIAXAAQAOAAAKAEQAGABADADQAPAIAAAKIAAAAQAAALgPAIQgOAIgTAAg");
	this.shape_16.setTransform(-8.6,26.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#FF9933","#FF6633","#993333"],[0,0.486,0.839],7.6,-4.7,0,7.6,-4.7,44.8).s().p("AgiD0QhGgTg3heIAAgBIgEgGIAAgBIgBgCIgMgXIgDgGIgBAAQhCiNgDjFQACAEAHADQAJAGATAFIAYAGIALACIAJABQBHAMBhAAQBiAABIgMIAIgBIAMgCIAWgGQAUgFAKgGQAGgEACgEQgBA4gGA0IgCAQQgQB4gtBfIgEAGIgLAXIgCACIAAABIgDAGQg1BchDAUQgUAGgUAAQgRAAgRgEgAB0AIIAEgHIAAAAIgEAHgAh4ABIAAAAIABABIgBgCg");
	this.shape_17.setTransform(-9,-2.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#993300").s().p("AioAhIgIgCIgMgBIgXgGQgTgFgKgGQgGgEgDgEIgBgFIABgEQAJgOAxgKIAPgDQBJgNBnAAQBoAABLANIAQADQAkAIANAJQAFAEACAEIAAAHQgCAFgGAEQgKAGgUAFIgXAGIgLABIgIACQhIAMhjAAQhhAAhHgMg");
	this.shape_18.setTransform(-9,-27.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.5,-52.5,87.1,105);


(lib.bg_result_new = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC6600").s().p("Eg+xAGzIAAtlIALAAILBACQC6A6AsALQArAMDGAqIGWAFQBNAuAVADQA3gCB4AAIBmgDQAfgdBCggQBPgoBEgKIIMACQAlAIDKA+QC3A5AXAJQANAFCVAMQDBAPCuAcQErAsAIAAQBlAADAhYQBmgtB4hAINYAAQAPAOAuAYQA7AfAqAAQBUAAAkg9IHbgGIAjgPQAggPAVgHIEYADQB9AtG7BxIJcCaIFcgBIACAAIAAHig");
	this.shape.setTransform(-5.6,239.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#009966").s().p("EAvzAIzQm8hxh8gtIkZgDQgVAHgfAPIgjAPInbAGQgkA9hUAAQgqAAg8gfQgtgYgPgOItYAAQh5BAhlAtQjABYhmAAQgHAAksgsQitgcjCgPQiVgMgNgFQgWgJi4g5QjJg+glgIIoMgCQhEAKhQAoQhCAggfAdIhmADQh4AAg2ACQgWgDhMguImWgFQjGgqgrgMQgtgLi6g6IrBgCIAAohQBghCB2gKQAUgBAUAAQC9AACFCGQCGCFAAC7IgBAaQCChZCogBQDCAACQB7QAfgnAlgkQDtjtFQAAQD/ABDGCIIADgEQCxivD7AAQBwAABhAjQA2hnBZhZQDhjhE9AAQE+AADhDhQCHCIA2CpIANgBQBcABBPAdQAtgbAygPQAJj7C1i1QC/i/EOAAQDzAACzCcQAhg7AygzQCOiMDHAAQCygBCDBwQAQAOAQAPQAoApAeAtQBIBxAACOIgBASQDeAPChChQBHBGArBTQCGhtCzAAQC7AACKB3QAZgpAkgkQAjgjAogXIAALVIgCAEIlcABIpciag");
	this.shape_1.setTransform(-5.1,162.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#006699").s().p("EgwoAKDQioAAiCBaIABgaQAAi9iFiFQiGiGi8AAQgVAAgUACQGKl9IoAAQHzAAFyE4QAzhEA/g/QDkjjEohCQAzirCIiIQDZjZEzAAQEzAADZDZQC3C3AdD1QA9gUBGAAQApAAAlAHQDbiwEjAAQCEAAB1AkQAog3AygyQD6j6FhAAQFhAAD6D6QCOCOA9CvQiDhvixAAQjIAAiNCNQgzAyghA6QiziajzAAQkOAAi/C9Qi1C1gJD8QgyAPgtAbQhPgdhbAAIgOAAQg2ipiHiIQjgjhk+AAQk+AAjhDhQhZBZg2BnQhhgkhwAAQj6AAixCxIgEADQjGiJj/AAQlQAAjtDtQglAkgeAnQiRh6jCAAgEA11AIkQizAAiGBtQgrhThHhHQihihjegPIABgRQAAiPhIhwIAGgHQBohnCTAAQCTAABoBnQBoBoAACTIgCAgQAZANAYAQQASgWAVgVQCiihDkAAQAoAAAmAFQAZg6AxgxQAuguA4gYIAAIjQgoAYgjAjQgkAkgZApQiJh3i8AAgEgjHgCUQgogoAAg5QAAg5AogpQApgoA5AAQA5AAAoAoQAoApAAA5QAAA5goAoQgoAog5AAQg5AAgpgogEg3TgDoQgSgSAAgbQAAgaASgTQATgSAaAAQAbAAASASQATATAAAaQAAAbgTASQgSATgbAAQgaAAgTgTgEgsxgEDQgZgZAAgjQAAgjAZgZQAZgZAjAAQAjAAAZAZQAZAZAAAjQAAAjgZAZQgZAZgjAAQgjAAgZgZgAC6mRQgxgxAAhGQAAhGAxgyQAygyBGAAQBGAAAyAyQAyAyAABGQAABGgyAxQgyAyhGAAQhGAAgygygAOBndQgJgJAAgNQAAgOAJgJQAKgJANAAQAOAAAJAJQAJAJAAAOQAAANgJAJQgJAKgOAAQgNAAgKgKg");
	this.shape_2.setTransform(5.6,108.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0099CC").s().p("EAzRAM5IACggQAAiThohoQhphoiTAAQiTAAhoBoIgFAGQgegtgogpQgQgPgQgOQg9iviOiOQj6j5lhAAQlhAAj6D5QgyAygoA3Qh2gkiEAAQkjAAjaCvQglgHgpAAQhGABg+AUQgdj2i2i2QjZjYkzAAQkzAAjaDYQiHCIgzCrQkoBBjlDlQg+A/gzBEQlyk6nzABQooAAmKF9Qh2AKhgBCIAAtzQA6hYBPhPQEdkeGUABQGTgBEdEeQBfBeA/BrQEjjpF5gcQBEieCEiEQETkSGEgBQGEABESESQBiBiA+BwQDOh9ECAAQEVAADaCRIAegfQE8k7G9AAQG9AAE7E7QElElAUGVQAxAIAuAOQA0iABqhpQDRjQEoAAQDtAAC2CFIAAG8Qg4AYgvAvQgwAwgZA7QgmgFgpAAQjjAAiiChQgVAVgSAWQgYgQgZgNgEghcACWQgoAoAAA6QAAA4AoAoQApApA5AAQA5AAAogpQAogoAAg4QAAg6gogoQgogog5AAQg5AAgpAogEg1oACrQgSATgBAaQABAbASASQASASAbAAQAbAAASgSQASgSAAgbQAAgagSgTQgSgTgbABQgbgBgSATgEgrHAByQgZAZAAAiQAAAkAZAYQAZAaAkAAQAiAAAagaQAYgYAAgkQAAgigYgZQgagZgiAAQgkAAgZAZgAEliSQgyAxABBGQgBBFAyAyQAyAyBGAAQBGAAAxgyQAzgyAAhFQAAhGgzgxQgxgyhGAAQhGAAgyAygAPsgcQgJAJAAANQAAANAJAJQAKAKANAAQAOAAAIgKQAKgJAAgNQAAgNgKgJQgIgKgOABQgNgBgKAKgEAqkgCoQhDhDAAheQAAheBDhCQBDhDBeAAQBeAABCBDQBEBCAABeQAABehEBDQhCBDheAAQheAAhDhDgEA0rgDTQgPgPAAgXQAAgVAPgQQAQgPAWgBQAWABAQAPQAPAQAAAVQAAAXgPAPQgQAPgWAAQgWAAgQgPgEglUgDzQgkgjAAgzQAAgyAkgkQAkgjAyAAQAzAAAkAjQAjAkABAyQgBAzgjAjQgkAlgzgBQgyABgkglgA/VmdQgQgQAAgVQAAgXAQgPQAPgQAWABQAWgBAPAQQAQAPAAAXQAAAVgQAQQgPAQgWAAQgWAAgPgQgEg9SgImQgagaAAgmQAAgmAagaQAagaAmAAQAlAAAaAaQAbAaAAAmQAAAmgbAaQgaAaglAAQgmAAgagagAQzqyQgRgRAAgYQAAgYARgSQASgQAYgBQAYABARAQQARASAAAYQAAAYgRARQgRARgYAAQgYAAgSgRg");
	this.shape_3.setTransform(-5.1,59);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7DD9F0").s().p("AdJLqQiLg7AAhTQAAhUCLg7QCMg6DFAAQDGAACLA6QCMA7AABUQAABTiMA7QiLA7jGAAQjFAAiMg7gEAxhALzQgOgIAAgLQAAgLAOgIQAOgHAUAAQAUAAAOAHQAOAIAAALQAAALgOAIQgOAIgUAAQgUAAgOgIgEg3tALEQihg2AAhNQAAhNChg2QCig2DkAAQDkAAChA2QChA2AABNQAABNihA2QihA3jkAAQjkAAiig3gEA0PAJeQgogSAAgbQAAgaAogTQApgSA5AAQA5AAAoASQAoATAAAaQAAAbgoASQgoATg5AAQg5AAgpgTgEgkoAGUQglgJAAgOQAAgNAlgKQAlgJA1AAQA0AAAlAJQAmAKAAANQAAAOgmAJQglAJg0AAQg1AAglgJgEAjQAEIQg4gigkgmQhgAKhoAAQluAAkDh1QkDhzAAilQAAikEDh1QEDh0FuAAQEnAADiBMQAvhXBlhHQC8iCEJAAQEKAAC7CCQC8CDAAC4QAAC5i8CCQiZBpjMAUQgYBpiDBOQifBgjiAAQjiAAighggEAzoADsQghgTAAgaQAAgbAhgSQAhgTAuAAQAuAAAgATQAhASAAAbQAAAaghATQggASguAAQguAAghgSgEgorAB5QhVgogqgxIgYAJQiXA3jUAAQjVAAiWg3QiXg1AAhNQAAhNCXg2QAsgQAxgLQhqhIAAhdQAAh7C6hXQC7hWEHAAQEHAAC6BWQC6BXAAB7QAAAigNAeIAsAAQDyAACqBQQCrBRAAByQAABxirBRQiqBRjyAAQjxAAirhRg");
	this.shape_4.setTransform(8.7,-213.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00CCFF").s().p("Eg+qAvDMAAAheFMB9VAAAMAAABeFg");
	this.shape_5.setTransform(-4.1,-18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_result_new, new cjs.Rectangle(-407.4,-319.5,804.5,602.3), null);


(lib.bevel2_q = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#B58F50").ss(15,1,1).p("EAdIg0UMg6PAAAQl0AAkIEHQkIEIAAF1MAAABMhQAAF1EIEHQEIEIF0AAMA6PAAAQF0AAEIkIQEIkHAAl1MAAAhMhQAAl1kIkIQkIkHl0AAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bevel2_q, new cjs.Rectangle(-283.8,-342.4,567.8,684.8), null);


(lib.star_glow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(35.3,41.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:90},89).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-535,-528.8,1140.7,1140);


(lib.base_q1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bevel2_q();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.276,0.193);

	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#F8E9B0","#EACE8D"],[0,1],0,0,0,0,0,109.9).s().p("AoBKHQhmAAhJgzQhIgzAAhIIAAuxQAAhJBIgzQBJgyBmAAIQDAAQBmAABJAyQBIAzAABJIAAOxQAABIhIAzQhJAzhmAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.base_q1, new cjs.Rectangle(-78.2,-66.2,156.4,132.4), null);


(lib.Game_Panel1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.attemptMc = new lib.sec();
	this.attemptMc.parent = this;
	this.attemptMc.setTransform(3.8,-18.5,0.525,0.525,0,0,0,0,-0.5);

	this.questionMc = new lib.quest();
	this.questionMc.parent = this;
	this.questionMc.setTransform(5.8,-59.4,0.525,0.525,0,0,0,0,-0.5);

	this.correctMc = new lib.score();
	this.correctMc.parent = this;
	this.correctMc.setTransform(5.8,22.2,0.525,0.525,0,0,0,0,-0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.correctMc},{t:this.questionMc},{t:this.attemptMc}]}).wait(1));

	// Layer 1
	this.instance = new lib.base_q1();
	this.instance.parent = this;
	this.instance.setTransform(7.1,-19);
	this.instance.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(-9, 49, 9, 147))];
	this.instance.cache(-80,-68,160,136);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Game_Panel1, new cjs.Rectangle(-71.1,-85.2,160,136), null);


(lib.result_option2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// parrot
	this.instance = new lib.Parrot_Winked();
	this.instance.parent = this;
	this.instance.setTransform(-259.8,26.5,0.816,0.816,0,0,0,6.7,-19.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// gold
	this.instance_1 = new lib.Gold_trophy("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1,182.9,1.5,1.5,0,0,0,-9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// silver
	this.instance_2 = new lib.Silver_trophy("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-1.4,182.7,1.5,1.5,0,0,0,-9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// bronze
	this.instance_3 = new lib.Bronze_trophy("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(-1,182.8,1.5,1.5,0,0,0,-9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// rays
	this.instance_4 = new lib.star_glow();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-4.7,171,0.155,0.146,0,0,0,11.3,-9.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// circle
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(5,1,1).p("AO5AAQAAGLkXEXQkXEXmLAAQmJAAkYkXQkXkXAAmLQAAmKEXkXQEYkXGJAAQGLAAEXEXQEXEXAAGKg");
	this.shape.setTransform(-1.2,178.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0099FF").s().p("AqhKiQkXkXAAmLQAAmKEXkXQEXkXGKAAQGKAAEYEXQEXEXAAGKQAAGLkXEXQkYEXmKAAQmKAAkXkXg");
	this.shape_1.setTransform(-1.2,178.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// result
	this.panelMc = new lib.Game_Panel1();
	this.panelMc.parent = this;
	this.panelMc.setTransform(-16.5,-44.8,2.686,2.686);

	this.timeline.addTween(cjs.Tween.get(this.panelMc).wait(1));

	// score time
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgqA8QgNgGgIgLQgIgMAAgRQAAgSAKgPQAJgPAQgKQAPgKASgGQARgFARAAQAKAAAJADQAKAEAGAHQAGAHAAALQAAAOgIAKQgIAJgOAGQgNAGgPAEQgPADgOABIgYACQADAIAIAEQAIADAJAAQAKAAAMgFQAOgEARgJQAEgCADAAQAHAAAEAFQAFAEAAAHQAAAEgCAEQgCAEgEACQgUAKgRAFQgSAEgPAAQgQAAgNgGgAAGgfQgLAEgLAHQgLAHgHAJQAMAAANgDQAMgCALgEQALgEAHgFQAHgEAAgFQAAgBAAAAQgBgBAAAAQAAAAAAgBQgBAAAAAAIgGgBIgBAAQgLAAgNAEg");
	this.shape_2.setTransform(235.7,141.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgsA9QgEgEAAgHIAAhiQAAgHAEgEQAFgFAHAAQAGAAAEAFQAFAEAAAHQAEgHAIgFQAHgFAKAAQAKAAAIAEQAIAEAHAIIADAEQABADAAADQAAAGgFAFQgEAEgHABQgDAAgDgCIgGgEIgEgEQgDgBgFAAQgFAAgFAFQgFAFgFAIQgEAJgCAKIAAAvQAAAHgFAEQgEAFgGAAQgHAAgFgFg");
	this.shape_3.setTransform(223.8,141.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgpA8QgKgHgGgLQgFgKgBgOQAAgQAHgOQAGgOAMgNQALgMAOgHQAOgIAQAAQAOABAKAGQALAHAGALQAFALABAPQgBAPgGANQgHAPgLAMQgLAMgOAHQgOAIgQgBQgOAAgLgGgAgGgaQgLAIgHAMQgHALAAAMQgBAIAEAFQAEAGAIAAQANgBAKgIQALgIAHgMQAHgLAAgMQAAgIgDgFQgEgFgIAAQgNAAgKAIg");
	this.shape_4.setTransform(209.8,141.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmA9QgNgGgJgKQgIgLgBgQQABgQAIgMQAIgNAOgKQANgKAQgGQAQgHAQgEIAdgFIABAAIABAAQAGAAAEAEQAFAFAAAGQAAAHgEAEQgEAEgGABIgSADQgLADgNAEQgNAFgLAGQgLAGgIAHQgHAIAAAKQAAAHAEADQAFAEAGACQAHABAFAAQAKAAAOgEQAPgEASgJIAHgCQAGAAAFAEQAEAFABAHQAAAEgDAEQgCADgEADQgTAKgTAFQgSAFgPAAQgPAAgNgFg");
	this.shape_5.setTransform(194.3,141.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgYBgQgOgFgNgIQgEgCgCgDQgCgEAAgFIABgFIABgDQADgFAEgCQADgBAFAAIAEAAIAFACQAIAFAJADQAJADAHAAQAGAAAHgDQAGgCAEgFQAFgFAAgHQAAgHgEgFQgDgGgFgEQgFgEgGgDIgJgFIgMgEQgNgDgKgGQgKgIgGgKQgFgLAAgNQAAgQAJgMQAIgMANgHQAOgGAPAAIABAAQAJAAANACQANADAOAHQAFADADADQACAEAAAFQgBAIgEAEQgFAEgHABQgEAAgEgCQgIgEgKgDQgJgCgHAAQgGAAgGACQgHADgEAEQgDAEgBAHQABAIAFAFQAHAEAHACIANAGIANAEQAMAHAJAJQAJAIAHAKQAFAMAAAOQAAAQgJANQgIANgNAGQgOAIgQgBQgMAAgNgDg");
	this.shape_6.setTransform(179.2,138.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgqA8QgNgGgIgLQgIgMAAgRQAAgSAKgPQAJgPAQgKQAPgKASgGQARgFARAAQAKAAAJADQAKAEAGAHQAGAHAAALQAAAOgIAKQgIAJgOAGQgNAGgPAEQgPADgOABIgYACQADAIAIAEQAIADAJAAQAKAAAMgFQAOgEARgJQAEgCADAAQAHAAAEAFQAFAEAAAHQAAAEgCAEQgCAEgEACQgUAKgRAFQgSAEgPAAQgQAAgNgGgAAGgfQgLAEgLAHQgLAHgHAJQAMAAANgDQAMgCALgEQALgEAHgFQAHgEAAgFQAAgBAAAAQgBgBAAAAQAAAAAAgBQgBAAAAAAIgGgBIgBAAQgLAAgNAEg");
	this.shape_7.setTransform(-128.2,140.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("ABMA9QgEgEAAgHIAAg0QgBgQgEgGQgEgHgFgBQgEgCgEABQgJAAgKAGQgJAHgHAOIAAA4QABAHgFAEQgEAFgHAAQgFAAgFgFQgEgEAAgHIAAg0QAAgQgFgGQgEgHgFgBQgFgCgEABQgGAAgGADQgGADgGAGQgGAGgEAJIAAA4QgBAHgEAEQgEAFgGAAQgIAAgEgFQgEgEgBgHIAAhjQABgHAEgEQAEgEAIgBQAGABAEAEQAEAEABAHIAAABQAHgIAIgEQAJgFAKAAQAQAAAKAHQALAGAHALQAHgLALgGQALgGAOgBQAZABAOAQQAOAQAAAeIAAA0QAAAHgEAEQgFAFgGAAQgHAAgFgFg");
	this.shape_8.setTransform(-148.4,140.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKBeQgEgFgBgGIAAhiQABgHAEgEQAEgFAGAAQAHAAAEAFQAEAEABAHIAABiQgBAGgEAFQgEAEgHABQgGgBgEgEgAgLhEQgFgFAAgHQAAgIAFgFQAFgFAGAAQAHAAAFAFQAFAFAAAIQAAAHgFAFQgFAFgHAAQgGAAgFgFg");
	this.shape_9.setTransform(-163.5,137.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgLBdQgEgGgBgHIAAiPIgnAAQgIAAgFgFQgEgFAAgHQAAgHAEgFQAFgEAIAAIBvAAQAHAAAGAEQAEAFAAAHQAAAHgEAFQgGAFgHAAIgnAAIAACPQAAAHgGAGQgEAEgHAAQgGAAgFgEg");
	this.shape_10.setTransform(-175.1,137.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgqA8QgNgGgIgLQgIgMAAgRQAAgSAKgPQAJgPAQgKQAPgKASgGQARgFARAAQAKAAAJADQAKAEAGAHQAGAHAAALQAAAOgIAKQgIAJgOAGQgNAGgPAEQgPADgOABIgYACQADAIAIAEQAIADAJAAQAKAAAMgFQAOgEARgJQAEgCADAAQAHAAAEAFQAFAEAAAHQAAAEgCAEQgCAEgEACQgUAKgRAFQgSAEgPAAQgQAAgNgGgAAGgfQgLAEgLAHQgLAHgHAJQAMAAANgDQAMgCALgEQALgEAHgFQAHgEAAgFQAAgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAIgGgBIgBAAQgLAAgNAEg");
	this.shape_11.setTransform(-197.5,140.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgWA+QgMgFgIgIQgDgCgBgDIgBgFIABgGIAEgFQACgDADgBQADgBADAAIAGABQADABACADQAEADAGADQAHADAGAAQAGAAAEgDQADgCAAgEIgBgFQgBgCgFgDQgFgEgLgDQgRgEgKgKQgJgKgBgPQABgMAFgIQAGgIAJgFQAKgEALAAQALAAAIACQAJACAJAFQAEACACAEQACAEAAAEIgBAEIgBADQgCAEgEACQgEACgEAAIgEAAIgDgBQgGgEgFgBQgFgCgEAAQgGAAgDACQgDACAAADQAAAEAEADIAIADQAYAGAMAMQAMAKAAASQgBALgFAJQgGAIgKAFQgKAFgNAAQgMAAgNgEg");
	this.shape_12.setTransform(-211.6,140.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAiA9QgFgEAAgHIAAg0QAAgQgEgGQgEgHgFgBQgFgCgEABQgFAAgHADQgGADgHAGQgGAGgEAJIAAA4QAAAHgFAEQgEAFgHAAQgGAAgFgFQgEgEgBgHIAAhjQABgHAEgEQAFgEAGgBQAHABAEAEQAFAEAAAHIAAABQAHgIAJgEQAKgFAJAAQAZABAOAQQAOAQABAeIAAA0QgBAHgEAEQgFAFgGAAQgHAAgEgFg");
	this.shape_13.setTransform(-224.9,140.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgpA8QgKgHgGgLQgFgLgBgOQAAgPAHgOQAGgPAMgMQALgMAOgHQAOgHAQgBQAOAAAKAIQALAGAGALQAFAMABAOQgBAPgGAOQgHAOgLAMQgLAMgOAHQgOAIgQAAQgOgBgLgGgAgGgaQgLAIgHAMQgHALAAAMQgBAIAEAFQAEAGAIgBQANAAAKgIQALgIAHgMQAHgLAAgMQAAgIgDgFQgEgFgIAAQgNAAgKAIg");
	this.shape_14.setTransform(-240,140.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Ag+BXQgFgEAAgHIAAiFQAAgFACgDQADgEADgCQATgKASgFQASgFAOAAQAPAAANAGQANAFAIALQAJAKAAAQQAAATgLAPQgKANgQAKQgQAKgSAGQgRAHgPACIAAAlQgBAHgEAEQgFAEgGABQgHgBgEgEgAgKg5QgLADgOAHIAAA5IAUgGQAMgEALgHQAMgHAIgJQAIgJABgLQAAgGgFgEQgEgEgHgBIgLgCQgKAAgKADg");
	this.shape_15.setTransform(-254.9,143.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgWA+QgMgFgIgIQgDgCgBgDIgBgFIABgGIAEgFQACgDADgBQADgBADAAIAGABQADABACADQAEADAGADQAHADAGAAQAGAAAEgDQADgCAAgEIgBgFQgBgCgFgDQgFgEgLgDQgRgEgKgKQgJgKgBgPQABgMAFgIQAGgIAJgFQAKgEALAAQALAAAIACQAJACAJAFQAEACACAEQACAEAAAEIgBAEIgBADQgCAEgEACQgEACgEAAIgEAAIgDgBQgGgEgFgBQgFgCgEAAQgGAAgDACQgDACAAADQAAAEAEADIAIADQAYAGAMAMQAMAKAAASQgBALgFAJQgGAIgKAFQgKAFgNAAQgMAAgNgEg");
	this.shape_16.setTransform(-269.3,140.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgqA8QgNgGgIgLQgIgMAAgRQAAgSAKgPQAJgPAQgKQAPgKASgGQARgFARAAQAKAAAJADQAKAEAGAHQAGAHAAALQAAAOgIAKQgIAJgOAGQgNAGgPAEQgPADgOABIgYACQADAIAIAEQAIADAJAAQAKAAAMgFQAOgEARgJQAEgCADAAQAHAAAEAFQAFAEAAAHQAAAEgCAEQgCAEgEACQgUAKgRAFQgSAEgPAAQgQAAgNgGgAAGgfQgLAEgLAHQgLAHgHAJQAMAAANgDQAMgCALgEQALgEAHgFQAHgEAAgFQAAgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAIgGgBIgBAAQgLAAgNAEg");
	this.shape_17.setTransform(-283.3,140.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAxBiQgDgBgCgDIhHhOIgDgGIgBgGQAAgGADgFQAEgFAHgBIAOgEQAHgDAIgEQAHgEAFgGQAFgGAAgIQAAgIgHgEQgHgFgMAAQgIAAgKADQgLADgMAGIAACGQgBAIgEAEQgFAFgHAAQgHAAgFgFQgFgEAAgIIAAiRQAAgEACgEIAGgGQATgKARgFQARgFAOAAQAQAAANAGQAOAFAIAMQAJALAAARQAAAOgHAMQgIAMgLAIQgMAHgOAEIAHAHIAVAWIAdAgIAEAFIABAGQAAAIgFAEQgFAFgHAAQgEAAgDgBg");
	this.shape_18.setTransform(-298.8,137.5);

	this.responseTxt = new cjs.Text(" ", "bold 50px 'Veggieburger'", "#FF9900");
	this.responseTxt.name = "responseTxt";
	this.responseTxt.textAlign = "center";
	this.responseTxt.lineHeight = 54;
	this.responseTxt.lineWidth = 117;
	this.responseTxt.parent = this;
	this.responseTxt.setTransform(-221.5,173);

	this.scoreTxt = new cjs.Text("", "bold 60px 'Veggieburger'", "#00CC00");
	this.scoreTxt.name = "scoreTxt";
	this.scoreTxt.textAlign = "center";
	this.scoreTxt.lineHeight = 65;
	this.scoreTxt.lineWidth = 123;
	this.scoreTxt.parent = this;
	this.scoreTxt.setTransform(211.6,169.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.scoreTxt},{t:this.responseTxt},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2}]}).wait(1));

	// bt_tab
	this.instance_5 = new lib.Symbol3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-4,180.1);
	this.instance_5.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(75, 0, 0, 0))];
	this.instance_5.cache(-339,-70,679,139);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// holder
	this.instance_6 = new lib.Symbol2();
	this.instance_6.parent = this;
	this.instance_6.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

	// bg
	this.instance_7 = new lib.bg_result_new();
	this.instance_7.parent = this;
	this.instance_7.setTransform(10.6,22.1,1.015,1.021);
	this.instance_7.filters = [new cjs.ColorMatrixFilter(new cjs.ColorMatrix(22, -3, 4, -11))];
	this.instance_7.cache(-409,-321,809,606);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1));

}).prototype = getMCSymbolPrototype(lib.result_option2, new cjs.Rectangle(-403.1,-303.9,820,617), null);


// stage content:
(lib.Result= function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var self = this
		 
		var i, j, k;
		
		displayvalue();
		
		function displayvalue() 
		{
			//var value;
			//var obj: Object = LoaderInfo(root.loaderInfo).parameters;
		
			//for (value in obj) 
			//{
				
			//}
			
			 
			
			/*for (i = 1; i <= tn; i++) {
				this["t" + String(i)].gotoAndStop(1);
		
			}
			for (j = 1; j <= aq; j++) {
				this["a" + String(j)].gotoAndStop(2);
				this["ca" + String(j)].gotoAndStop(2);
		
			}
			for (k = 1; k <= cq; k++) {
				this["ca" + String(k)].gotoAndStop(3);
		
			}
			*/
			self.containerMc.responseTxt.text = rts;
			self.containerMc.scoreTxt.text = sv;
			self.containerMc.panelMc.questionMc.txt.text = tn;
			self.containerMc.panelMc.attemptMc.txt.text = aq;
			self.containerMc.panelMc.correctMc.txt.text = cq;
			
			//txtrstime.text = rts;
			//txtscore.text = sv;
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 1
	this.containerMc = new lib.result_option2();
	this.containerMc.parent = this;
	this.containerMc.setTransform(200,150,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.containerMc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(198.5,148,411,311);
// library properties:
lib.properties = {
	width: 400,
	height: 300,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;