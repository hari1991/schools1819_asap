<div class="headerTop">
<img src="<?php echo base_url(); ?>assets/images/banner.jpg" class="img-responsive">
<!--<div class="swiper-slide caption1 swiper-slide-visible swiper-slide-active no-transform" data-bg-alignment="center" data-color-scheme="light" data-x-pos="centered" data-y-pos="middle" >
    <div class="video-wrap" style="width: 100%; height: auto;">
      <video style="width:100%; position:relative;"  controls="none" id="bgvid" playsinline autoplay>
        <source src="images/classroom.mp4" type="video/mp4" >
      </video>
    </div>
  </div>
  <div class="tagline">Be a part of revolution</div>-->
</div>
<div class="skillPuzzle">
  <div class="container">
<h1>You are at India's number game-icon Brain Skills Puzzles Suite!</h1>

<div class="row">
<div class="col-md-4 col-sm-12">
<h2>Puzzles Attempted</h2>
<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s"><div class="odometer"  id="odo1" ></div></div>
</div>
<div class="col-md-4 col-sm-12">
<h2>Puzzles Solved</h2>
<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" ><div class="odometer" id="odo2" ></div></div>
</div>
<div class="col-md-4 col-sm-12">
<h2>Minutes Trained</h2>
<div class="wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" ><div class="odometer" id="odo3" ></div></div>
</div>

</div>

</div>
</div>
<div class="indexContentMiddle">
  <div class="container">
  <div class="row taglines">
<div class="col-md-4 col-sm-4">Talent hits a target<br>
No one else can hit</div>
<div class="col-md-4 col-sm-4">Genius hits a target<br>
No one else can see.</div>
<div class="col-md-4 col-sm-4">Awaken the Genius <br>
in you</div>
  </div>
  <div class="row indexMiddle">
<div class="col-md-4 col-sm-4"><img src="<?php echo base_url(); ?>assets/images/children-icon.jpg" class="img-responsive">
  <h2>Children</h2></div>
<div class="col-md-4 col-sm-4"><img src="<?php echo base_url(); ?>assets/images/academic-icon.jpg" class="img-responsive">
  <h2>Academic</h2></div>
<div class="col-md-4 col-sm-4"><img src="<?php echo base_url(); ?>assets/images/parents-icon.jpg" class="img-responsive">
  <h2>Parents</h2></div>
  </div>
  
  </div>
</div>

<div class="awardsrecognition">
  <div class="container">
<h1>Awards & Recognition</h1>
<div class="row">
<div class="col-md-6 col-sm-6">
<ul>
<li>Winners of Hot 100 Technology Awards Dec 2014</li>
<li>Best Startup Award - Development of Entrepreneurs (TIDE) Award at Department of Electronics & IT New Delhi</li>
<li>CII Startupreneurs - Best Startup - People's Choice Award</li>
<li>Qualcomm – Qprize Finalist</li>
<li>Assist World Record - Largest Computer Based Skill Competition</li>
<li>Winners of Tech4Impact Accelerator Program, 2013</li>
</ul>
</div>
<div class="col-md-6 col-sm-6"><img src="<?php echo base_url(); ?>assets/images/awards-logo.jpg" class="img-responsive"></div>
</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="mediaList col-md-6 col-sm-6">
<h2>In The Media</h2>
<span>Thanks to Media for featuring us</span>
<div class="medialogo">
<div><img src="<?php echo base_url(); ?>assets/images/hindu.png" class="img-responsive"></div>
<div><img src="<?php echo base_url(); ?>assets/images/timesOfindia.png" class="img-responsive"></div>
<div><img src="<?php echo base_url(); ?>assets/images/economicTimes.png" class="img-responsive"></div>
<div><img src="<?php echo base_url(); ?>assets/images/yoursStories.png" class="img-responsive"></div>
</div>
</div>
<div class="latestNews col-md-6 col-sm-6">
<h2>Latest News</h2>

  <section class="regular slider">
    <div>
Top 2 winner of Tech4 Impact Accelereator Program 2013 conducted by CIIEIIM Ahmedabad, Village Capital.     </div>
    <div>
Top 2 winner of Tech4 Impact Accelereator Program 2013 conducted by CIIEIIM Ahmedabad, Village Capital.     </div>
    <div>
Top 2 winner of Tech4 Impact Accelereator Program 2013 conducted by CIIEIIM Ahmedabad, Village Capital.     </div>
    <div>
Top 2 winner of Tech4 Impact Accelereator Program 2013 conducted by CIIEIIM Ahmedabad, Village Capital.     </div>
    <div>
Top 2 winner of Tech4 Impact Accelereator Program 2013 conducted by CIIEIIM Ahmedabad, Village Capital.     </div>
    <div>
Top 2 winner of Tech4 Impact Accelereator Program 2013 conducted by CIIEIIM Ahmedabad, Village Capital.     </div>
  </section>
</div>
</div>
</div>